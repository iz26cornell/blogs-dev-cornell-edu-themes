<?php /*
 * Template Name: slider-three-colum
 *
 * @package Cryout Creations
 * @subpackage parabola
 * @since parabola 0.5
 */
get_header(); ?>

		<section id="container" class="three-columns-sided">
	
			<div id="content" role="main">
			
            <div class="slider-container-3column">
          	<!-- Header Carousel -->
            
            <header id="myCarousel" class="carousel slide">

                <?php

                    //The slider will have a maximum of $MAX_SLIDES
                    $MAX_SLIDES = 4;
                    //Control whether or not to show caption
                    $show_caption = true;
                    
                    $posts = get_posts( array('category_name'  => 'homeslider', 'posts_per_page' => $MAX_SLIDES) );

                    if (null != $posts)
                    {
                       

                        echo "<p><!-- Wrapper for slides --></p>\n";
                        echo "<div class='carousel-inner' style='width: auto;'>\n";
                        echo "<!-- Slides -->\n";

                        for ($i=0; $i < count($posts); $i++)
                        {
                            $p = $posts[$i];
    						//print_r($p);
                            $content = $p->post_content;
    
                            //get the url of the first image that appears in the content
                            //see   http://stackoverflow.com/questions/18093990/php-regex-to-get-all-image-urls-on-the-page
                            $pattern = '~src="(http.*\.)(jpe?g|png|[tg]iff?|svg)~i';
                            $m = preg_match_all($pattern,$content,$matches);
    
                            //The entire match will be in $matches[0][0], which contains the starting     src="    substring.  Make sure
                            //to strip that off 
    
                            $image_url = $matches[0][0];
                            $image_url = str_replace('src="', '', $image_url);

                            $image_url = "'$image_url'";  //put single quotes around url because this will go in CSS background-image:url property
    
                            $item_class = 0 == $i ? 'item active':'item';
                            $caption = $p->post_title;

                            echo "<div class='$item_class'>\n";
                            //echo '    <div><img src=' . $image_url . ' width="200" /></div>' . "\n";
                            //echo "    <div>" . $caption ."</div>\n";
                            echo "    <div style='padding: 0px;'>". $content. "</div>\n";
                            echo "</div>\n";
                        }
						echo "<div style='clear: both;'></div>\n";
						echo "<div class='btn-indicators-container'>\n";
								echo "<div style='width: auto; float: left;'>\n";
									echo "<!-- Indicators -->\n";
									echo "<ol class='carousel-indicators'>\n";
									
									for ($i=0; $i < count($posts); $i++)
									{
										$p = $posts[$i];
										echo "<li data-target='#myCarousel' data-slide-to='$i' " . (0 == $i ? "class='active'":'') . "></li>\n";
									}
									
									echo "</ol>\n";
								echo "</div>\n";
								echo "<div class='btn-controls-container'>\n";
									echo "<a href='#myCarousel' data-slide='prev' class='prevbtn'><i class='fa fa-angle-left' aria-hidden='true'></i></a>\n";
									echo "<a href='#myCarousel' data-slide='next' class='nextbtn'><i class='fa fa-angle-right' aria-hidden='true'></i></a>\n";            
									echo "<div style='clear: left;'></div>\n";
								echo "</div>\n";
						echo "</div>\n";
                        //reset posts so another loop over the posts can take place below normally
                        wp_reset_postdata();
                        
                    }
                ?>
 
            <!-- Controls -->
			
       <!-- </div>-->
        
		</header>
        

            </div>
           
				
			<?php cryout_before_content_hook(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
				
                <h1 class="entry-title"><?php //the_title(); ?></h1>
                <div class="entry-content"><?php the_content(); ?></div>
		</div>		
			<?php cryout_after_content_hook(); ?>

			</div><!-- #content -->
			<?php get_sidebar('left'); get_sidebar('right'); ?>
		</section><!-- #container -->


<?php get_footer(); ?>