// Custom JavaScript

jQuery(document).ready(function() {
	
	jQuery('#cu-search').hide();

	jQuery('#search-button a').click(function(e) {
		e.preventDefault();
		jQuery(this).toggleClass('open');
		jQuery('#cu-search').slideToggle(300);
		if ( jQuery('#cu-search').css('visibility','visible') ) {
			jQuery('#search-form-query').focus();
		}
	});
	
	jQuery('#access .menu').each(function(){
		jQuery(this).addClass('group');
	});
	
	jQuery('.et_pb_slides .et_pb_slide_title a').each(function(){
		jQuery(this).attr("onclick", "window.open(this.href,'_blank');return false;");
	});
	
	jQuery('#sfooter .socialicons').each(function(){
		jQuery(this).attr("onclick", "window.open(this.href,'_blank');return false;");
	});
	
	jQuery('#prime_nav li:first span span').unwrap();
	
	jQuery('#footer2-inner').append('<hr class="footer-rule">');
	

});

