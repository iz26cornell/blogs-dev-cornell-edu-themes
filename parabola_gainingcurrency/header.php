<?php
/**
 * The Header
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Cryout Creations
 * @subpackage parabola
 * @since parabola 0.5
 */
 ?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php  cryout_meta_hook(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<script src="https://use.typekit.net/fao8jzu.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<?php
 	cryout_header_hook();
	wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<!-- Cornell Search -->
    <div id="cu-search" class="options">
    	<div id="cu-search-wrapper" class="options">
            <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                <div id="search-form" class="no-label">
                    <div id="search-input">
                        <label for="search-form-query">Search:</label>
                        <input type="text" id="search-form-query" name="s" value="" size="20" />
                        <input type="submit" id="search-form-submit" value="go" />
                    </div>
                    <div id="search-filters">
                        <input type="radio" name="site" id="search-filters1" value="this" checked="checked" />
                        <label for="search-filters1">This Site</label>
                        <input type="radio" name="site" id="search-filters2" value="cornell" />
                        <label for="search-filters2">Cornell</label>
                    </div>	
                </div>
            </form>
    	</div>
    </div>
	<!-- End Cornell Search -->
    
	<!-- Cornell Identity Banner -->
	<div id="cu-identity" class="home theme-red45">
		<div id="cu-identity-content">
            <div id="search-button">
                <p><a href="#" title="Search This Site">Search This Site</a></p>
            </div>
		</div>
	</div>
	<!-- End Cornell Identity Banner -->

	<?php cryout_body_hook(); ?>

	<div id="wrapper" class="hfeed">

		<?php cryout_wrapper_hook(); ?>

		<div id="header-full">

			<header id="header">

				<?php cryout_masthead_hook(); ?>

				<div id="masthead">

                    <div id="branding" role="banner" >
        
                        <?php cryout_branding_hook();?>
                        <?php cryout_header_widgets_hook(); ?>
                        <div style="clear:both;"></div>
        
                    </div><!-- #branding -->
                    
                    <a id="nav-toggle"><span>&nbsp;</span></a>
                    
                    <nav id="access" role="navigation">
                        <?php cryout_access_hook();?>
                    </nav><!-- #access -->
    
            	</div><!-- #masthead -->

                <div style="clear:both;height:1px;width:1px;"> </div>
            
            </header><!-- #header -->
            
		</div><!-- #header-full -->
        
        <div id="slider">
        	
            <div id="slider_content">
            
				<img id="currency_image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/project/money2.png" alt=""/>
                		   
        	</div>
            
		<?php if (is_front_page()) { ?>
            <div class="et_builder_outer_content" id="et_builder_outer_content">
                <div class="et_builder_inner_content et_pb_gutters3">
                    <?php echo do_shortcode('[et_pb_section admin_label="Section" fullwidth="on" specialty="off"][et_pb_fullwidth_post_slider admin_label="Fullwidth Post Slider" posts_number="10" include_categories="46247" orderby="date_desc" show_arrows="on" show_pagination="on" show_more_button="off" content_source="off" use_manual_excerpt="on" show_meta="off" background_color="rgba(255,255,255,0.91)" background_layout="light" show_image="on" image_placement="left" parallax="off" parallax_method="off" use_bg_overlay="off" use_text_overlay="off" remove_inner_shadow="off" background_position="default" background_size="default" auto="on" auto_speed="5000" auto_ignore_hover="off" hide_content_on_mobile="off" hide_cta_on_mobile="off" show_image_video_mobile="off" meta_letter_spacing="0" custom_button="on" button_letter_spacing="0" button_use_icon="on" button_icon_placement="right" button_on_hover="off" button_letter_spacing_hover="0" dot_nav_custom_color="#e02b20" button_icon="%%58%%" arrows_custom_color="#e02b20" header_font_size="30" header_text_color="#e02b20" body_font_size="15" button_icon_color="#e02b20"]'); ?>
                </div>
           </div>           
        <?php } ?>
        </div>
            
            
            
            
            
        </div>
        
        <div id="main">
            <div  id="forbottom" >
                <?php cryout_forbottom_hook(); ?> 
        
                <div style="clear:both;"> </div>
        
                <?php cryout_breadcrumbs_hook();?>
