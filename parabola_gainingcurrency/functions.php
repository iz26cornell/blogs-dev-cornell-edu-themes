<?php
/****************** remove cryout action hooks *********************/
function remove_parabola_hooks() {
    remove_action('cryout_footer_hook','parabola_site_info',12);
}
add_action('init','remove_parabola_hooks');

/****************** load child styles and scripts *********************/
function load_child_scripts() {
	wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'modernizr', get_stylesheet_directory_uri() . '/js/modernizr.js', array( 'jquery' ), '1.0', true );
	wp_enqueue_style( 'roboto', '//fonts.googleapis.com/css?family=Open+Sans:400,300,700|Bitter' );
}
add_action('wp_enqueue_scripts', 'load_child_scripts');

/*********** Replace mobile styles with a child version ***********/
function child_load_mobile_css() {
	global $parabolas;
	foreach ($parabolas as $key => $value) {
		${"$key"} = $value ;
	}
	if ($parabola_mobile=="Enable") {
		echo "<link rel='stylesheet' id='parabola_style_mobile'  href='".get_stylesheet_directory_uri() . '/styles/style-mobile.css?ver=' . _CRYOUT_THEME_VERSION . "' type='text/css' media='all' />";
	}
}
function child_remove_theme_actions() {
	remove_action('wp_head', 'parabola_load_mobile_css', 30);
	add_action('wp_head', 'child_load_mobile_css', 30);
}
add_action('wp_head','child_remove_theme_actions');


//Search template redirect
function search_template_redirect() {

	$selected_radio = $_GET['site'];
	
	if ($selected_radio == 'cornell') {
		$search_terms = urlencode($_GET['s']);
		$location = "http://www.cornell.edu/search/" . "?q=" . $search_terms;
		wp_redirect($location);
		exit();
	}
}
add_action( 'template_redirect', 'search_template_redirect' );


/*********************** Adds title and description to header *************************/
function child_title_and_description() {
	
	$parabolas = parabola_get_theme_options();
	foreach ($parabolas as $key => $value) { ${"$key"} = $value ; }
	// Header styling and image loading
	// Check if this is a post or page, if it has a thumbnail, and if it's a big one
	global $post;

	if (get_header_image() != '') { $himgsrc=get_header_image(); }
	if ( is_singular() && has_post_thumbnail( $post->ID ) && $parabola_fheader == "Enable" &&
		( $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'header' ) ) &&
		$image[1] >= HEADER_IMAGE_WIDTH ) : $himgsrc= $image[0];
	endif;

	if (isset($himgsrc) && ($himgsrc != '')) : echo '<img id="bg_image" alt="" title="" src="'.$himgsrc.'"  />';  endif;
	
} 

add_action ('cryout_branding_hook','child_title_and_description');

/************************ END CHILD TITLE AND DESCRIPTION ***************************/

/*
Plugin Name: Hide Title
Plugin URI: http://dojodigital.com
Description: Allows authors to hide the title tag on single pages and posts via the edit post screen.
Version: 1.0.4
Author: Brandon Kraft & Randall Runnels
Author URI: http://dojodigital.com
*/

if ( !class_exists( 'DojoDigitalHideTitle' ) ) {

    class DojoDigitalHideTitle {

    	private $slug = 'dojodigital_toggle_title';
    	private $selector = '.entry-title';
    	private $title;
    	private $afterHead = false;

        /**
        * PHP 5 Constructor
        */
        function __construct(){

	        add_action( 'add_meta_boxes', array( $this, 'add_box' ) );
			add_action( 'save_post', array( $this, 'on_save' ) );
			add_action( 'delete_post', array( $this, 'on_delete' ) );
			add_action( 'wp_head', array( $this, 'head_insert' ), 3000 );
			add_action( 'the_title', array( $this, 'wrap_title' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts' ) );

        } // __construct()


		private function is_hidden(  ){

			if( is_singular() ){

				global $post;

				$toggle = get_post_meta( $post->ID, $this->slug, true );

				if( (bool) $toggle ){
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}

		} // is_hidden()


    	public function head_insert(){

			if( $this->is_hidden() ){ ?>
<!-- Dojo Digital Hide Title -->
<script type="text/javascript">
	jQuery(document).ready(function($){

		if( $('<?php echo $this->selector; ?>').length != 0 ) {
			$('<?php echo $this->selector; ?> span.<?php echo $this->slug; ?>').parents('<?php echo $this->selector; ?>:first').hide();
		} else {
			$('h1 span.<?php echo $this->slug; ?>').parents('h1:first').hide();
			$('h2 span.<?php echo $this->slug; ?>').parents('h2:first').hide();
		}

	});
</script>
<noscript><style type="text/css"> <?php echo $this->selector; ?> { display:none !important; }</style></noscript>
<!-- END Dojo Digital Hide Title -->

			<?php }

			// Indicate that the header has run so we can hopefully prevent adding span tags to the meta attributes, etc.
			$this->afterHead = true;

		} // head_insert()


		public function add_box(){

			$posttypes = array( 'post', 'page' );

			foreach ( $posttypes as $posttype ){
				add_meta_box( $this->slug, 'Hide Title', array( $this, 'build_box' ), $posttype, 'side' );
			}

		} // add_box()


		public function build_box( $post ){

			$value = get_post_meta( $post->ID, $this->slug, true );

			$checked = '';

			if( (bool) $value ){ $checked = ' checked="checked"'; }

			wp_nonce_field( $this->slug . '_dononce', $this->slug . '_noncename' );

			?>
			<label><input type="checkbox" name="<?php echo $this->slug; ?>" <?php echo $checked; ?> /> Hide the title on singular page views.</label>
			<?php

		} // build_box()


		public function wrap_title( $content ){

			if( $this->is_hidden() && $content == $this->title && $this->afterHead ){
				$content = '<span class="' . $this->slug . '">' . $content . '</span>';
			}

			return $content;

		} // wrap_title()


		public function load_scripts(){


			// Grab the title early in case it's overridden later by extra loops.
			global $post;
			$this->title = $post->post_title;

			if( $this->is_hidden() ){
				wp_enqueue_script( 'jquery' );

			}

		} // load_scripts()


		public function on_save( $postID ){

			if ( ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
				|| !isset( $_POST[ $this->slug . '_noncename' ] )
				|| !wp_verify_nonce( $_POST[ $this->slug . '_noncename' ], $this->slug . '_dononce' ) ) {
				return $postID;
			}

			$old = get_post_meta( $postID, $this->slug, true );
			$new = $_POST[ $this->slug ] ;

			if( $old ){
				if ( is_null( $new ) ){
					delete_post_meta( $postID, $this->slug );
				} else {
					update_post_meta( $postID, $this->slug, $new, $old );
				}
			} elseif ( !is_null( $new ) ){
				add_post_meta( $postID, $this->slug, $new, true );
			}

			return $postID;

		} // on_save()


		public function on_delete( $postID ){
			delete_post_meta( $postID, $this->slug );
			return $postID;
		} // on_delete()


		public function set_selector( $selector ){

			if( isset( $selector ) && is_string( $selector ) ){
				$this->selector = $selector;
			}

		} // set_selector()


    } // DojoDigitalHideTitle

    $DojoDigitalHideTitle = new DojoDigitalHideTitle;

} // !class_exists( 'DojoDigitalHideTitle' )

/**
Plugin Name: TW Recent Posts Widget
Plugin URI: http://vuckovic.biz/wordpress-plugins/tw-recent-posts-widget
Description: TW Recent Posts Widget is advanced version of the WordPress Recent Posts widget allowing increased customization to display recent posts from category you define.
Author: Igor Vuckovic
Author URI: http://vuckovic.biz
Version: 1.0.5
*/

//	Set the wp-content and plugin urls/paths
if(!defined('WP_CONTENT_URL'))
    define('WP_CONTENT_URL', get_option('siteurl') . '/wp-content');
if(!defined('WP_CONTENT_DIR'))
    define('WP_CONTENT_DIR', ABSPATH . 'wp-content');
if(!defined('WP_PLUGIN_URL'))
    define('WP_PLUGIN_URL', WP_CONTENT_URL . '/plugins');
if(!defined('WP_PLUGIN_DIR'))
    define('WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins');

/**
 * Class TW_Recent_Posts
 */
class TW_Recent_Posts extends WP_Widget
{

    //	@var string(The plugin version)
    var $version = '1.0.5';
    //	@var string(Domain used for localization)
    var $localization_domain = 'tw-recent-posts';
    //	@var string(The url to this plugin)
    var $plugin_url = '';
    //	@var string(The path to this plugin)
    var $plugin_path = '';


    /**
     * Constructor
     */
    public function __construct()
    {
        $name = dirname(plugin_basename(__FILE__));
        $this->plugin_url = WP_PLUGIN_URL . "/$name/";
        $this->plugin_path = WP_PLUGIN_DIR . "/$name/";
        add_action('wp_print_styles', array(&$this, 'tw_recent_posts_css'));

        $widget_ops = array('classname' => 'tw-recent-posts', 'description' => __('Show recent posts from selected category. Includes advanced options.', $this->localization_domain));

        parent::__construct('tw-recent-posts', __('TW Recent Posts ', $this->localization_domain), $widget_ops);
    }

    /**
     * Inserts CSS style
     */
    public function tw_recent_posts_css()
    {
        $name = "tw-recent-posts-widget.css";
        if (false !== file_exists(TEMPLATEPATH . "/$name")) {
            $css = get_template_directory_uri() . "/$name";
        } else {
            $css = $this->plugin_url . $name;
        }

        wp_enqueue_style('tw-recent-posts-widget', $css, false, $this->version, 'screen');
    }

    /**
     * Truncates input text to the defined length
     *
     * @param int $amount
     * @param string $allowed
     * @return string
     */
    private function _truncate_post($amount, $allowed = '')
    {
        global $post;

        $post_excerpt = $post->post_excerpt;

        if($post_excerpt != '') {

            if(strlen($post_excerpt) <= $amount) {
                $echo_out = '';
            } else {
                $echo_out = '...';
            }

            $post_excerpt = strip_tags($post_excerpt, $allowed);

            if($echo_out == '...') {
                $post_excerpt = substr($post_excerpt, 0, strrpos(substr($post_excerpt, 0, $amount), ' '));
            } else {
                $post_excerpt = substr($post_excerpt, 0, $amount);
            }

            return($post_excerpt . $echo_out);

        } else {

            $truncate = $post->post_content;
            $truncate = preg_replace('@\[caption[^\]]*?\].*?\[\/caption]@si', '', $truncate);

            if(strlen($truncate) <= $amount) {
                $echo_out = '';
            } else {
                $echo_out = '...';
            }

            $truncate = apply_filters('the_content', $truncate);
            $truncate = preg_replace('@<script[^>]*?>.*?</script>@si', '', $truncate);
            $truncate = preg_replace('@<style[^>]*?>.*?</style>@si', '', $truncate);
            $truncate = strip_tags($truncate, $allowed);

            if($echo_out == '...') {
                $truncate = substr($truncate, 0, strrpos(substr($truncate, 0, $amount), ' '));
            } else {
                $truncate = substr($truncate, 0, $amount);
            }

            return $truncate . $echo_out;
        }
    }

    /**
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        extract($args);
        
        $title = apply_filters('title', isset($instance['title']) ? esc_attr($instance['title']) : '');
        $category = apply_filters('category', isset($instance['category']) ? esc_attr($instance['category']) : '');
        $moretext = apply_filters('moretext', isset($instance['moretext']) ? esc_attr($instance['moretext']) : '');
        $count = apply_filters('count', isset($instance['count']) && is_numeric($instance['count']) ? esc_attr($instance['count']) : '');
        $orderby = apply_filters('orderby', isset($instance['orderby']) ? $instance['orderby'] : '');
        $order = apply_filters('order', isset($instance['order']) ? $instance['order'] : '');
        $width = apply_filters('width', isset($instance['width']) && is_numeric($instance['width']) ?(int)$instance['width'] : 60);
        $height = apply_filters('height', isset($instance['height']) && is_numeric($instance['height']) ?(int)$instance['height'] : 60);
        $length = apply_filters('length', isset($instance['length']) && is_numeric($instance['length']) ?(int)$instance['length'] : 100);
        $show_post_title = apply_filters('show_post_title', isset($instance['show_post_title']) ?(bool)$instance['show_post_title'] : false);
        $show_post_time = apply_filters('show_post_time', isset($instance['show_post_time']) ? (bool)$instance['show_post_time'] : false);
        $show_post_thumb = apply_filters('show_post_thumb', isset($instance['show_post_thumb']) ?(bool)$instance['show_post_thumb'] : false);
        $show_post_excerpt = apply_filters('show_post_excerpt', isset($instance['show_post_excerpt']) ?(bool)$instance['show_post_excerpt'] : false);

        echo $before_widget;
        if(!empty($title)) {
            echo $before_title . $title . $after_title;
        }
?>

<div class="featured-posts textwidget">
<?php
$wp_query = new WP_Query(array(
    'cat' => $category,
    'posts_per_page' => $count,
    'orderby' => $orderby,
    'order' => $order,
    'nopagging' => true
));

while($wp_query->have_posts()) : $wp_query->the_post(); ?>
	<div class="featured-post">

	<?php if ($show_post_time) { ?>
		<div class="post-time">
			<?php the_time(get_option('date_format')); ?>
		</div>
	<?php } ?>

	<?php if ($show_post_thumb && has_post_thumbnail()) { ?>
		<a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail(array($width,$height), array('title' => '', 'class' => 'alignleft')); ?>
        </a>
	<?php } ?>

	<?php if($show_post_excerpt) { ?>
		<div class="excerpt">
            <?php echo $this->_truncate_post($length)
                . ($moretext != '' ? ' <a href="' . get_permalink() . '" class="read-more">' . $moretext . '</a>' : ''); ?>
		</div>
	<?php } ?>

	<?php if ($show_post_title) { ?>
		<h4>
        	<?php the_title() ?>
        </h4>
	<?php } ?>

		<div class="clear"></div>
	</div>
<?php
endwhile;
wp_reset_query();
wp_reset_postdata();
		?>
</div>
<?php
		echo $after_widget;
	}

    /**
     * @param array $new_instance
     * @param arrau $old_instance
     * @return mixed
     */
    public function update($new_instance, $old_instance)
    {
        return $new_instance;
    }

    /**
     * @param array $instance
     */
    public function form($instance) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$category = isset($instance['category']) ? esc_attr($instance['category']) : '';
		$moretext = isset($instance['moretext']) ? esc_attr($instance['moretext']) : 'more&raquo;';
		$count = isset($instance['count']) && is_numeric($instance['count']) ? esc_attr($instance['count']) : 4;
		$orderby = isset($instance['orderby']) ? $instance['orderby'] : '';
		$order = isset($instance['order']) ? $instance['order'] : '';
		$width = isset($instance['width']) && is_numeric($instance['width']) ? (int)$instance['width'] : 60;
		$height = isset($instance['height']) && is_numeric($instance['height']) ? (int)$instance['height'] : 60;
		$length = isset($instance['length']) && is_numeric($instance['length']) ? (int)$instance['length'] : 100;
		$show_post_title = isset($instance['show_post_title']) ? (bool)$instance['show_post_title'] : false;
		$show_post_time = isset($instance['show_post_time']) ? (bool)$instance['show_post_time'] : false;
		$show_post_thumb = isset($instance['show_post_thumb']) ? (bool)$instance['show_post_thumb'] : false;
		$show_post_excerpt = isset($instance['show_post_excerpt']) ? (bool)$instance['show_post_excerpt'] : false;
?>

<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', $this->localization_domain); ?> <input
	class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
	name="<?php echo $this->get_field_name('title'); ?>" type="text"
	value="<?php echo $title; ?>" /></label></p>

<p><label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category:', $this->localization_domain); ?></label><select
	id="<?php echo $this->get_field_id('category'); ?>"
	name="<?php echo $this->get_field_name('category'); ?>">
	<?php
	echo '<option value="0" ' .('0' == $category ? 'selected="selected"' : ''). '>'. __('All categories', $this->localization_domain).'</option>';
	$cats = get_categories(array('hide_empty' => 0, 'taxonomy' => 'category', 'hierarchical' => 1));
	foreach($cats as $cat) {
		echo '<option value="' . $cat->term_id . '" ' .($cat->term_id == $category ? 'selected="selected"' : ''). '>' . $cat->name . '</option>';
	} ?>
	</select></p>

<p><label for="<?php echo $this->get_field_id('orderby'); ?>"><?php _e('Order by:', $this->localization_domain); ?></label><select
	id="<?php echo $this->get_field_id('orderby'); ?>"
	name="<?php echo $this->get_field_name('orderby'); ?>">
	<option value="date"
		<?php echo 'date' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Date', $this->localization_domain); ?></option>
	<option value="ID"
		<?php echo 'ID' == $orderby ? 'selected="selected"' : '' ?>><?php _e('ID', $this->localization_domain); ?></option>
	<option value="title"
		<?php echo 'title' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Title', $this->localization_domain); ?></option>
	<option value="author"
		<?php echo 'author' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Author', $this->localization_domain); ?></option>
	<option value="comment_count"
		<?php echo 'comment_count' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Comment count', $this->localization_domain); ?></option>
	<option value="rand"
		<?php echo 'rand' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Random', $this->localization_domain); ?></option>
</select></p>

<p><label for="<?php echo $this->get_field_id('order'); ?>"><?php _e('Order:', $this->localization_domain); ?></label><select
	id="<?php echo $this->get_field_id('order'); ?>"
	name="<?php echo $this->get_field_name('order'); ?>">
	<option value="DESC"
		<?php echo 'DESC' == $order ? 'selected="selected"' : '' ?>><?php _e('DESC:', $this->localization_domain); ?></option>
	<option value="ASC"
		<?php echo 'ASC' == $order ? 'selected="selected"' : '' ?>><?php _e('ASC:', $this->localization_domain); ?></option>
</select></p>

<p><label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Number of posts to show:', $this->localization_domain); ?> <input
	id="<?php echo $this->get_field_id('count'); ?>"
	name="<?php echo $this->get_field_name('count'); ?>" type="text"
	size="3" value="<?php echo $count; ?>" /></label></p>

<p><input id="<?php echo $this->get_field_id('show_post_title'); ?>"
	name="<?php echo $this->get_field_name('show_post_title'); ?>"
	type="checkbox" <?php checked($show_post_title); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_title'); ?>"><?php _e('Show post title', $this->localization_domain); ?></label>
</p>

<p><input id="<?php echo $this->get_field_id('show_post_time'); ?>"
	name="<?php echo $this->get_field_name('show_post_time'); ?>"
	type="checkbox" <?php checked($show_post_time); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_time'); ?>"><?php _e('Show post time', $this->localization_domain); ?></label>
</p>

<p><input id="<?php echo $this->get_field_id('show_post_thumb'); ?>"
	name="<?php echo $this->get_field_name('show_post_thumb'); ?>"
	type="checkbox" <?php checked($show_post_thumb); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_thumb'); ?>"><?php _e('Show post thumb', $this->localization_domain); ?></label><br />
<small><?php _e('Thumbnail size(W-H):', $this->localization_domain); ?></small>
<input type="text" size="3"
	name="<?php echo $this->get_field_name('width'); ?>"
	value="<?php echo $width; ?>" />px <input type="text" size="3"
	name="<?php echo $this->get_field_name('height'); ?>"
	value="<?php echo $height; ?>" />px</p>

<p><input id="<?php echo $this->get_field_id('show_post_excerpt'); ?>"
	name="<?php echo $this->get_field_name('show_post_excerpt'); ?>"
	type="checkbox" <?php checked($show_post_excerpt); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_excerpt'); ?>"><?php _e('Show post excerpt', $this->localization_domain); ?></label><br />
<small><?php _e('Post excerpt length(characters)', $this->localization_domain); ?></small>
<input id="<?php echo $this->get_field_id('length'); ?>"
	name="<?php echo $this->get_field_name('length'); ?>" type="text"
	size="3" value="<?php echo $length; ?>" /><br />
<small><?php _e('Read more text', $this->localization_domain); ?></small>
<input name="<?php echo $this->get_field_name('moretext'); ?>"
	type="text" size="12" value="<?php echo $moretext; ?>" /></p>

<?php
    }

} // end class TW_Recent_Posts

add_action('widgets_init', function () {
    register_widget('TW_Recent_Posts');
});

/**
 * Plugin Name: Display Posts Shortcode
 * Plugin URI: http://www.billerickson.net/shortcode-to-display-posts/
 * Description: Display a listing of posts using the [display-posts] shortcode
 * Version: 2.4
 */
 
 
/**
 * To Customize, use the following filters:
 *
 * `display_posts_shortcode_args`
 * For customizing the $args passed to WP_Query
 *
 * `display_posts_shortcode_output`
 * For customizing the output of individual posts.
 * Example: https://gist.github.com/1175575#file_display_posts_shortcode_output.php
 *
 * `display_posts_shortcode_wrapper_open` 
 * display_posts_shortcode_wrapper_close`
 * For customizing the outer markup of the whole listing. By default it is a <ul> but
 * can be changed to <ol> or <div> using the 'wrapper' attribute, or by using this filter.
 * Example: https://gist.github.com/1270278
 */ 
 
// Create the shortcode
add_shortcode( 'display-posts', 'be_display_posts_shortcode' );
function be_display_posts_shortcode( $atts ) {

	// Original Attributes, for filters
	$original_atts = $atts;

	// Pull in shortcode attributes and set defaults
	$atts = shortcode_atts( array(
		'title'              => '',
		'author'              => '',
		'category'            => '',
		'date_format'         => '(n/j/Y)',
		'display_posts_off'   => false,
		'exclude_current'     => false,
		'id'                  => false,
		'ignore_sticky_posts' => false,
		'image_size'          => false,
		'include_title'       => true,
		'include_author'      => false,
		'include_content'     => false,
		'include_date'        => false,
		'include_excerpt'     => false,
		'meta_key'            => '',
		'meta_value'          => '',
		'no_posts_message'    => '',
		'offset'              => 0,
		'order'               => 'DESC',
		'orderby'             => 'date',
		'post_parent'         => false,
		'post_status'         => 'publish',
		'post_type'           => 'post',
		'posts_per_page'      => '10',
		'tag'                 => '',
		'tax_operator'        => 'IN',
		'tax_term'            => false,
		'taxonomy'            => false,
		'wrapper'             => 'ul',
		'wrapper_class'       => 'display-posts-listing',
		'wrapper_id'          => false,
	), $atts, 'display-posts' );
	
	// End early if shortcode should be turned off
	if( $atts['display_posts_off'] )
		return;

	$shortcode_title = sanitize_text_field( $atts['title'] );
	$author = sanitize_text_field( $atts['author'] );
	$category = sanitize_text_field( $atts['category'] );
	$date_format = sanitize_text_field( $atts['date_format'] );
	$exclude_current = be_display_posts_bool( $atts['exclude_current'] );
	$id = $atts['id']; // Sanitized later as an array of integers
	$ignore_sticky_posts = be_display_posts_bool( $atts['ignore_sticky_posts'] );
	$image_size = sanitize_key( $atts['image_size'] );
	$include_title = be_display_posts_bool( $atts['include_title'] );
	$include_author = be_display_posts_bool( $atts['include_author'] );
	$include_content = be_display_posts_bool( $atts['include_content'] );
	$include_date = be_display_posts_bool( $atts['include_date'] );
	$include_excerpt = be_display_posts_bool( $atts['include_excerpt'] );
	$meta_key = sanitize_text_field( $atts['meta_key'] );
	$meta_value = sanitize_text_field( $atts['meta_value'] );
	$no_posts_message = sanitize_text_field( $atts['no_posts_message'] );
	$offset = intval( $atts['offset'] );
	$order = sanitize_key( $atts['order'] );
	$orderby = sanitize_key( $atts['orderby'] );
	$post_parent = $atts['post_parent']; // Validated later, after check for 'current'
	$post_status = $atts['post_status']; // Validated later as one of a few values
	$post_type = sanitize_text_field( $atts['post_type'] );
	$posts_per_page = intval( $atts['posts_per_page'] );
	$tag = sanitize_text_field( $atts['tag'] );
	$tax_operator = $atts['tax_operator']; // Validated later as one of a few values
	$tax_term = sanitize_text_field( $atts['tax_term'] );
	$taxonomy = sanitize_key( $atts['taxonomy'] );
	$wrapper = sanitize_text_field( $atts['wrapper'] );
	$wrapper_class = sanitize_html_class( $atts['wrapper_class'] );
	if( !empty( $wrapper_class ) )
		$wrapper_class = ' class="' . $wrapper_class . '"';
	$wrapper_id = sanitize_html_class( $atts['wrapper_id'] );
	if( !empty( $wrapper_id ) )
		$wrapper_id = ' id="' . $wrapper_id . '"';

	
	// Set up initial query for post
	$args = array(
		'category_name'       => $category,
		'order'               => $order,
		'orderby'             => $orderby,
		'post_type'           => explode( ',', $post_type ),
		'posts_per_page'      => $posts_per_page,
		'tag'                 => $tag,
	);
	
	// Ignore Sticky Posts
	if( $ignore_sticky_posts )
		$args['ignore_sticky_posts'] = true;
	
	// Meta key (for ordering)
	if( !empty( $meta_key ) )
		$args['meta_key'] = $meta_key;
	
	// Meta value (for simple meta queries)
	if( !empty( $meta_value ) )
		$args['meta_value'] = $meta_value;
		
	// If Post IDs
	if( $id ) {
		$posts_in = array_map( 'intval', explode( ',', $id ) );
		$args['post__in'] = $posts_in;
	}
	
	// If Exclude Current
	if( $exclude_current )
		$args['post__not_in'] = array( get_the_ID() );
	
	// Post Author
	if( !empty( $author ) )
		$args['author_name'] = $author;
		
	// Offset
	if( !empty( $offset ) )
		$args['offset'] = $offset;
	
	// Post Status	
	$post_status = explode( ', ', $post_status );		
	$validated = array();
	$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
	foreach ( $post_status as $unvalidated )
		if ( in_array( $unvalidated, $available ) )
			$validated[] = $unvalidated;
	if( !empty( $validated ) )		
		$args['post_status'] = $validated;
	
	
	// If taxonomy attributes, create a taxonomy query
	if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {
	
		// Term string to array
		$tax_term = explode( ', ', $tax_term );
		
		// Validate operator
		if( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) )
			$tax_operator = 'IN';
					
		$tax_args = array(
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'slug',
					'terms'    => $tax_term,
					'operator' => $tax_operator
				)
			)
		);
		
		// Check for multiple taxonomy queries
		$count = 2;
		$more_tax_queries = false;
		while( 
			isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) && 
			isset( $original_atts['tax_' . $count . '_term'] ) && !empty( $original_atts['tax_' . $count . '_term'] ) 
		):
		
			// Sanitize values
			$more_tax_queries = true;
			$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
	 		$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
	 		$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts['tax_' . $count . '_operator'] : 'IN';
	 		$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
	 		
	 		$tax_args['tax_query'][] = array(
	 			'taxonomy' => $taxonomy,
	 			'field' => 'slug',
	 			'terms' => $terms,
	 			'operator' => $tax_operator
	 		);
	
			$count++;
			
		endwhile;
		
		if( $more_tax_queries ):
			$tax_relation = 'AND';
			if( isset( $original_atts['tax_relation'] ) && in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) ) )
				$tax_relation = $original_atts['tax_relation'];
			$args['tax_query']['relation'] = $tax_relation;
		endif;
		
		$args = array_merge( $args, $tax_args );
	}
	
	// If post parent attribute, set up parent
	if( $post_parent ) {
		if( 'current' == $post_parent ) {
			global $post;
			$post_parent = get_the_ID();
		}
		$args['post_parent'] = intval( $post_parent );
	}
	
	// Set up html elements used to wrap the posts. 
	// Default is ul/li, but can also be ol/li and div/div
	$wrapper_options = array( 'ul', 'ol', 'div' );
	if( ! in_array( $wrapper, $wrapper_options ) )
		$wrapper = 'ul';
	$inner_wrapper = 'div' == $wrapper ? 'div' : 'li';

	
	$listing = new WP_Query( apply_filters( 'display_posts_shortcode_args', $args, $original_atts ) );
	if ( ! $listing->have_posts() )
		return apply_filters( 'display_posts_shortcode_no_results', wpautop( $no_posts_message ) );
		
	$inner = '';
	while ( $listing->have_posts() ): $listing->the_post(); global $post;
		
		$image = $date = $author = $excerpt = $content = '';
		
		if ( $image_size && has_post_thumbnail() )  
			$image = '<a class="image" href="' . get_permalink() . '">' . get_the_post_thumbnail( get_the_ID(), $image_size ) . '</a> ';
			
		if ( $include_date ) 
			$date = ' <span class="date">' . get_the_date( $date_format ) . '</span>';
			
		if( $include_author )
			$author = apply_filters( 'display_posts_shortcode_author', ' <span class="author">by ' . get_the_author() . '</span>' );
		
		if ( $include_excerpt ) 
			$excerpt = '<div class="excerpt">' . get_the_excerpt() . '</div>';
			
		if( $include_content ) {
			add_filter( 'shortcode_atts_display-posts', 'be_display_posts_off', 10, 3 );
			$content = '<div class="content">' . apply_filters( 'the_content', get_the_content() ) . '</div>'; 
			remove_filter( 'shortcode_atts_display-posts', 'be_display_posts_off', 10, 3 );
		}
		
		if ( $include_title )
			$title = '<div class="the_title">' . get_the_title() . '</div>';
			
		$class = array( 'listing-item' );
		$class = sanitize_html_class( apply_filters( 'display_posts_shortcode_post_class', $class, $post, $listing, $original_atts ) );
		$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $image . $date . $author . $excerpt . $content . $title . '</' . $inner_wrapper . '>';
		
		// If post is set to private, only show to logged in users
		if( 'private' == get_post_status( get_the_ID() ) && !current_user_can( 'read_private_posts' ) )
			$output = '';
		
		$inner .= apply_filters( 'display_posts_shortcode_output', $output, $original_atts, $image, $title, $date, $excerpt, $inner_wrapper, $content, $class );
		
	endwhile; wp_reset_postdata();
	
	$open = apply_filters( 'display_posts_shortcode_wrapper_open', '<' . $wrapper . $wrapper_class . $wrapper_id . '>', $original_atts );
	$close = apply_filters( 'display_posts_shortcode_wrapper_close', '</' . $wrapper . '>', $original_atts );
	
	$return = $open;

	if( $shortcode_title ) {

		$title_tag = apply_filters( 'display_posts_shortcode_title_tag', 'h2', $original_atts );

		$return .= '<' . $title_tag . ' class="display-posts-title">' . $shortcode_title . '</' . $title_tag . '>' . "\n";
	}

	$return .= $inner . $close;

	return $return;
}

/**
 * Turn off display posts shortcode 
 * If display full post content, any uses of [display-posts] are disabled
 *
 * @param array $out, returned shortcode values 
 * @param array $pairs, list of supported attributes and their defaults 
 * @param array $atts, original shortcode attributes 
 * @return array $out
 */
function be_display_posts_off( $out, $pairs, $atts ) {
	$out['display_posts_off'] = true;
	return $out;
}

/**
 * Convert string to boolean
 * because (bool) "false" == true
 *
 */
function be_display_posts_bool( $value ) {
	return !empty( $value ) && 'true' == $value ? true : false;
}


// Add allowed tags
add_action( 'init', 'cwd_allow_onclick_on_a' );
function cwd_allow_onclick_on_a() {
    global $allowedposttags;
 
    $tags = array( 'a' );
    $new_attributes = array( 'onclick' => array() );
 
    foreach ( $tags as $tag ) {
        if ( isset( $allowedposttags[ $tag ] ) && is_array( $allowedposttags[ $tag ] ) )
            $allowedposttags[ $tag ] = array_merge( $allowedposttags[ $tag ], $new_attributes );
    }
}
add_filter('tiny_mce_before_init', 'cwd_filter_tiny_mce_before_init');
function cwd_filter_tiny_mce_before_init( $options ) {
 
    if ( ! isset( $options['extended_valid_elements'] ) ) {
        $options['extended_valid_elements'] = '';
    } else {
        $options['extended_valid_elements'] .= ',';
    }
 
    if ( ! isset( $options['custom_elements'] ) ) {
        $options['custom_elements'] = '';
    } else {
        $options['custom_elements'] .= ',';
    }
 
    $options['extended_valid_elements'] .= 'a[onclick|title|href|target|download|class|id|style]';
    $options['custom_elements']         .= 'a[onclick|title|href|target|download|class|id|style]';
    return $options;
}