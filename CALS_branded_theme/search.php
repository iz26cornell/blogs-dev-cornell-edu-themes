<?php
/**
 * The template for displaying Search Results pages
 */

get_header(); 

?>

<div id="content">

	<div id="main">
    
    	<div id="main-top"></div>
        
        <div id="main-body">
			<header class="entry-header">
				<h1 class="entry-title"><?php printf( __( 'Search Results for: %s', 'cornell_base' ), get_search_query() ); ?></h1>
			</header><!-- .entry-header -->
		<?php if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content' ); ?>
			<?php endwhile; ?>

			<?php if(function_exists('tw_pagination')) tw_pagination(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #main-body -->
        
		<?php if ( is_active_sidebar( 'sidebar-3' ) ) { ?>
            <div id="secondary-nav">
                <div class="main-body">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        <?php } ?>
            
		<?php if ( is_active_sidebar( 'sidebar-4' ) ) { ?>
            <div id="secondary">
                <div class="main-body">
                    <?php get_sidebar('secondary'); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-bottom"></div>
        
	</div><!-- #main -->
    
</div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- end #wrap -->



<?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
    <div class="colorband tint-one">
        <div class="colorband-content">
            <?php if ( get_theme_mod('section_one') != '' ) : ?><h2 class="section-title"><span><?php echo get_theme_mod('section_one', 'Section One'); ?></span></h2><?php endif; ?>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-5' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'sidebar-6' ) ) : ?>
    <div class="colorband tint-two">
        <div class="colorband-content">
            <?php if ( get_theme_mod('section_two') != '' ) : ?><h2 class="section-title"><span><?php echo get_theme_mod('section_two', 'Section Two'); ?></span></h2><?php endif; ?>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-6' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'sidebar-7' ) ) : ?>
    <div class="colorband tint-three">
        <div class="colorband-content">
            <?php if ( get_theme_mod('section_three') != '' ) : ?><h2 class="section-title"><span><?php echo get_theme_mod('section_three', 'Section Three'); ?></span></h2><?php endif; ?>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-7' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>