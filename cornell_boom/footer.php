<?php
/**
 * @package WordPress
 * @subpackage Cornell
 * @since Cornell 1.0
 */
?>
	<div id="footer-wrap"><div id="footer" role="contentinfo">
		<?php get_sidebar( 'footer' ); ?>

		<div id="colophon">
        <p><?php wp_loginout(); ?> -- <?php wp_footer();?></p>
		</div><!-- #colophon -->
	</div></div><!-- #footer -->


</body>
</html>