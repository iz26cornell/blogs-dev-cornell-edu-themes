<?php
// child functions


/* Shortcode: Render Custom Field */
function customfields_shortcode($atts, $text) {
	global $post;
	return get_post_meta($post->ID, $text, true);
}
@add_shortcode('cf','customfields_shortcode');


/* remove Parabola branding */
function remove_cryout_footer() {
	remove_action('cryout_footer_hook','parabola_site_info',12);
}
add_action('init','remove_cryout_footer');


/* add custom menu shortcode --- example: [menu name="your menu name" class="your class"] */
function print_menu_shortcode($atts, $content = null) {
extract(shortcode_atts(array( 'name' => null, 'class' => null ), $atts));
return wp_nav_menu( array( 'menu' => $name, 'menu_class' => $class, 'echo' => false ) );
}
add_shortcode('menu', 'print_menu_shortcode');


?>