<?php
    $options[] = array( "name" => "Testimonials",
    					"sicon" => "portfolio-32x32.png",
						"type" => "heading");

    $options[] = array( "name" => "Choose the defined Testimonials page",
                        "desc" => "The testimonials item page will use the same title description, if defined, as the selected page.",
                        "id" => $shortname."_portfoliodesc",
                        "type" => "select",
                        "options" => $options_pages);
    $options[] = array( "name" => "Choose the filtering type on Testimonials page",
                        "desc" => "",
                        "id" => $shortname."_portfoliofilters",
                        "type" => "select",
                        "std"  => "regular",
                        "options" => array(
                        	'regular'=>'Regular filtering (with page reload)',
                        	'javascript'=>'Javascript Filtering (without page reload)'
                        	)
                        );
    $options[] = array( "name" => "Testimonials Items per Page",
                        "desc" => "Set the number of items that appear on the Testimonials page.",
                        "id" => $shortname."_portfolioitemsperpage",
                        "std" => "6",
                        "type" => "text");
?>