/* IWS Dynamic Components
   ------------------------------------------- */  

	
	// Window Size Tracking
	function resizeChecks() {
		
		// Restore Standard Main Navigation
		if (jQuery(window).width() >= 767) {
			jQuery('#cu-search').removeAttr('style');
		}
	}
	
	jQuery(window).load(function() {
		
		resizeChecks();
		
		jQuery('.sub-menu').addClass('top-127');

		
	});
	
	jQuery(document).ready(function() {
		
		jQuery(window).resize(resizeChecks);
		resizeChecks();		
		
		// Mobile Form
		jQuery('#search_icon').click(function(){
			 jQuery('#cu-search').slideToggle(200);
		});
		
		
		
	});