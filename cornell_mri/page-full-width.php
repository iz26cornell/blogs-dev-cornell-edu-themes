<?php
/**
 * Template Name: Full Width (No Sidebar)
 * @package WordPress
 * @subpackage Cornell
 */

get_header(); ?>
<div id="content">
<div id="content-wrap">

  <div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post();
    /*
    <div id="breadcrumb" class="widget_pages">
		<ul>
			<li class="breadcrumb-home"><a href="<?php echo home_url( '/' ); ?>">Home</a></li>
			<?php wp_list_pages('title_li=&depth=8'); ?>
		</ul>
	</div>
	*/
	?>
    <h2><?php the_title(); ?></h2>
    <?php the_content(__('Read more'));?>
    <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php echo('<p class="post-nav">'); posts_nav_link(' ', __('<span class="post-nav-back">&laquo; Previous</span>'), __('<span class="post-nav-next">More &raquo;</span>')); echo('</p>'); ?>
  </div>
</div>
</div>
<?php get_footer(); ?>
