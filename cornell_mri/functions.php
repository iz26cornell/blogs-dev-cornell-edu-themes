<?php

function mobile_screen() {
	
	
	$useragent=$_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) 
	{
	
		//we ARE a mobile device
		
		/*
		//let's take care of any cookie changes from when user clicks
		if (isset($_GET["killMobileSession"])) { $_SESSION['iws_mobileSession'] = ""; $_SESSION["iws_amMobileDevice"] = "TRUE"; }
		//if (isset($_GET["killMobileSession"])) { unset($_SESSION['iws_mobileSession']); $_SESSION["iws_amMobileDevice"] = "TRUE"; }//setcookie("mobileSession", "", time()-3600); }
		if (isset($_GET["startMobileSession"])) { $_SESSION["iws_mobileSession"] = "TRUE"; }//setcookie("mobileSession", "TRUE"); }
		//echo "\niws_mobileSession:".$_SESSION["iws_mobileSession"]."\n";
		//echo "iws_amMobileDevice:".$_SESSION["iws_amMobileDevice"]."\n";
		
		//Are we already in a mobile session?
		//if (!isset($_SESSION["iws_mobileSession"])) echo "Session is not set\n";
		//if(isset($_SESSION["iws_amMobileDevice"])) echo "I am a mobile device\n";
		if(isset($_SESSION["iws_amMobileDevice"]) && $_SESSION["iws_amMobileDevice"] != "" && $_SESSION["iws_mobileSession"] === "")
		{
			//Then they switched to regular web version
			echo "<link rel=\"stylesheet\" href=\""; echo bloginfo('stylesheet_directory'); echo "/style.css\" type=\"text/css\" media=\"screen\" />";
		}
		else 
		{
			//set session
			$_SESSION["iws_amMobileDevice"] = "TRUE"; // setcookie("amMobileDevice","TRUE");
			$_SESSION["iws_mobileSession"] = "TRUE"; //setcookie("mobileSession","TRUE");
			//echo $_SESSION["iws_amMobileDevice"];
			echo "<meta name=\"viewport\" content=\"width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\"/>";
			echo "<link rel=\"stylesheet\" href=\""; echo bloginfo('stylesheet_directory'); echo "/styles/mobile.css\" type=\"text/css\" />";
		}*/
	
		
		
		/* Mobile CSS DISABLED ************************************************************ */
		
		//echo "<meta name=\"viewport\" content=\"width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\"/>";
		//echo "<link rel=\"stylesheet\" href=\""; echo bloginfo('stylesheet_directory'); echo "/styles/mobile.css\" type=\"text/css\" />";
		
		echo "<link rel=\"stylesheet\" href=\""; echo bloginfo('stylesheet_directory'); echo "/style.css\" type=\"text/css\" media=\"screen\" />";
		
		/* ******************************************************************************** */
	} 
	else {
		// then we're NOT on a mobile device
		echo "<link rel=\"stylesheet\" href=\""; echo bloginfo('stylesheet_directory'); echo "/style.css\" type=\"text/css\" media=\"screen\" />";
	}
	
}


if ( function_exists('register_sidebar') )	
    
	register_sidebar( array(
		'name' => __( 'Left Column (homepage)', 'Cornell' ),
		'id' => 'sidebar-home',
		'description' => __( 'The left widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Right Column (homepage)', 'Cornell' ),
		'id' => 'rightbar-home',
		'description' => __( 'The right widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Gallery (homepage)', 'Cornell' ),
		'id' => 'gallery-home',
		'description' => __( 'The campaign gallery area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (secondary)', 'Cornell' ),
		'id' => 'sidebar-1',
		'description' => __( 'The primary widget area for secondary pages.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Links (all pages)', 'Cornell' ),
		'id' => 'footer-widget-area',
		'description' => __( 'A customizable menu of footer links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Text (all pages)', 'Cornell' ),
		'id' => 'footer-message',
		'description' => __( 'This content appears under the footer copyright and links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );



/**
 *  Returns the Cornell options with defaults as fallback
 *
 */
function Cornell_get_theme_options() {
	$defaults = array(
		'color_scheme' => 'light',
		'theme_layout' => 'content-sidebar',
	);
	$options = get_option( 'Cornell_theme_options', $defaults );

	return $options;
}

/**
 *  Add a section class to the body tag
 *
 */
add_filter('body_class','body_class_section');
function body_class_section ($classes) {
	global $wpdb, $post;
	if (is_page()) {
		if ($post->post_parent) {
			$parent = end(get_post_ancestors($current_page_id));
		}
		else {
			$parent = $post->ID;
		}
	$post_data = get_post($parent, ARRAY_A);
	$classes[] = 'section-' . $post_data['post_name'];
	}
	return $classes;
}



/**
 *  Disable the website field in visitor comments
 *  -- now handled by comments.php
 
add_filter('comment_form_default_fields', 'comment_form_no_website');
function comment_form_no_website($fields) {
	if(isset($fields['url'])) {
		unset($fields['url']);
	}
	return $fields;
}
*/

/**
 *  Strip any commenter websites already in the database
 *
 */
add_filter('get_comment_author_url','remove_comment_author_url');
function remove_comment_author_url($url) {
    $url = '';
    return $url;
}

add_theme_support( 'post-thumbnails' );


function Cornell_excerpt_length( $length ) {
	return 12; /* word count */
}
add_filter( 'excerpt_length', 'Cornell_excerpt_length' );

/**
 *  Custom "Read More" link for homepage posts
 *
 */
function new_excerpt_more($more) {
	global $post;
	return ' ... <a class="more" href="'. get_permalink($post->ID) . '">Read More</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

/* Taken from a post by Spitzerg http://wordpress.org/support/topic/blank-search-sends-you-to-the-homepage */
add_filter( 'request', 'my_request_filter' );
function my_request_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}

add_theme_support( 'post-thumbnails' );
?>

<?php
function wordpress_breadcrumbs() {
 
  $delimiter = ''; //'&raquo;';
  $name = 'Home'; //text for the 'Home' link
  $currentBefore = '<li class="breadcrumb-active">';
  $currentAfter = '</li>';
 
  if ( !is_home() && !is_front_page() || is_paged() ) {
 
    echo '<ul id="breadcrumbs">';
 
    global $post;
    $home = get_bloginfo('url');
    echo '<li><a href="' . $home . '">' . $name . '</a></li>' . $delimiter . '';
 
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, '' . $delimiter . ''));
      echo $currentBefore . 'Archive by category <b>';
      single_cat_title();
      echo '</b>' . $currentAfter;
 
    } elseif ( is_day() ) {
      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li>' . $delimiter . '';
      echo '<li><a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a></li>' . $delimiter . '';
      echo $currentBefore . get_the_time('d') . $currentAfter;
 
    } elseif ( is_month() ) {
      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li>' . $delimiter . '';
      echo $currentBefore . get_the_time('F') . $currentAfter;
 
    } elseif ( is_year() ) {
      echo $currentBefore . get_the_time('Y') . $currentAfter;
 
    } elseif ( is_single() ) {
      $cat = get_the_category(); $cat = $cat[0];
      echo '<li>';
      echo get_category_parents($cat, TRUE, '' . $delimiter . '');
      echo '</li>';
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . '' . $delimiter . '';
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_search() ) {
      echo $currentBefore . 'Search results for &#39;' . get_search_query() . '&#39;' . $currentAfter;
 
    } elseif ( is_tag() ) {
      echo $currentBefore . 'Posts tagged &#39;';
      single_tag_title();
      echo '&#39;' . $currentAfter;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $currentBefore . 'Articles posted by ' . $userdata->display_name . $currentAfter;
 
    } elseif ( is_404() ) {
      echo $currentBefore . 'Error 404' . $currentAfter;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo '<span class="breadcrumb-active">(';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')</span>';
    }
 
    echo '</ul>';
 
  }
}
?>

