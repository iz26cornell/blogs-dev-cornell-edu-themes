<?php get_header(); ?>
<div id="content">
<div id="content-wrap">

  <div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
    <?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?>
    <h2><a href="<?php the_permalink() ?>" rel="bookmark">
      <?php the_title(); ?>
      </a></h2>
      <?php the_content(__('Read more'));?>
    <div class="postmeta">
      <div class="postmetaleft">
      
        <p>
        <span class="avatar-box"><?php echo get_avatar( get_the_author_email(), '25' ); ?><br/><?php the_author_posts_link(); ?></span>
          category: 
          <?php the_category(', ') ?><br />
          posted: <?php the_time('m/j/y g:i A') ?> by  <?php the_author_posts_link(); ?><br />
          <?php if ('open' == $post->comment_status) : ?><?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?> <?php else : ?>
<?php endif; ?><span class="edit-post""><?php edit_post_link('edit', '', ''); ?></span>
        </p>
      </div>
    </div>

    <h3>Comments</h3>
    <?php comments_template(); // Get wp-comments.php template ?>
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php echo('<p class="post-nav">'); posts_nav_link(' ', __('<span class="post-nav-back">&laquo; Previous</span>'), __('<span class="post-nav-next">More &raquo;</span>')); echo('</p>'); ?>
  </div>
  <div id="secondary">
	
	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<div id="sidebar-content">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div>
	<?php endif; ?>
	
	<?php if ( get_post_meta($post->ID, 'Related Posts', false) ) : ?>
    <div id="related">
		<div id="pages" class="widget-container">
			<h3 class="widget-title">See also:</h3>
			<ul>
				<?php
					global $post;
					$postID = $post->ID;
					$related = get_post_meta($post->ID, 'Related Posts', true);
				?>
				<?php query_posts('posts_per_page=6&orderby=title&order=ASC&meta_key=Related Posts&meta_value=' . $related); ?>
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php if ($post->ID != $postID) : ?>
						<li>
							<a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
						</li>
						<?php endif; ?>
					<?php endwhile; else : ?>
					<?php endif; ?>
				<?php wp_reset_query(); ?>
			</ul>

		</div>
	</div>
	<?php endif; ?>
	
	</div>
  
</div>
</div>
<?php get_footer(); ?>