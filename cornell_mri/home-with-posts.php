<?php
/**
 * Template Name: Homepage, with posts
 * @package WordPress
 * @subpackage Cornell
 */
get_header(); ?>
<div id="content">
<div id="content-wrap">
  <div id="secondary">
  	<?php if ( is_active_sidebar( 'sidebar-home' ) ) : ?>
		<?php dynamic_sidebar( 'sidebar-home' ); ?>
	<?php endif; ?>
  </div>
  
  <div id="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php if ( is_front_page() ) { ?>
				<?php /*<h2 class="entry-title"><?php the_title(); ?></h2> */?>
			<?php } else { ?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php } ?>

			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
				<?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
			</div><!-- .entry-content -->
		</div><!-- #post-## -->

		<?php ?> <?php /*comments_template( '', true ); */?> <?php ?>

	<?php endwhile; ?>
	
	<?php /*query_posts('category_name=news&showposts=4');*/ ?>
	<?php query_posts('cat=213&showposts=4'); ?>
	<?php /*<h2 class="latest-title">Latest News</h2> */?>

		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				
				<?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail('thumbnail', array('class' => 'dropshadow-light')); } ?>
				<?php if ( is_front_page() ) { ?>
					<h4 class="entry-title"><?php the_title(); ?></h4>
				<?php } else { ?>
					<h4 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'Cornell' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
				<?php } ?>
				<div class="entry-date"><?php the_time('M d, Y'); ?></div>
				
				<div class="entry-content">
					<?php the_excerpt(); ?>
					<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
					<?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
				</div><!-- .entry-content -->
			</div><!-- #post-## -->

			<?php /*?> <?php comments_template( '', true ); ?> <?php */?>

		<?php endwhile; ?>
		<a href="<?php echo get_settings('home'); ?>/category/news/" class="home-more">More</a>
	
  </div>
  
  <div id="tertiary">
  	<?php if ( is_active_sidebar( 'rightbar-home' ) ) : ?>
		<?php dynamic_sidebar( 'rightbar-home' ); ?>
	<?php endif; ?>
  </div>
  
</div>
</div>
<?php get_footer(); ?>
