<?php get_header(); ?>
<div id="content">
<div id="content-wrap">
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
  <div id="main">
    <?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?>
    <h2>Archive</h2>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail('thumbnail', array('class' => 'dropshadow-light')); } ?>
		<?php if ( is_front_page() ) { ?>
			<h4 class="entry-title"><?php the_title(); ?></h4>
		<?php } else { ?>
			<h4 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'Cornell' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
		<?php } ?>
		<div class="entry-date"><?php the_time('M d, Y'); ?></div>
		
		<div class="entry-content">
			<?php the_excerpt(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
			<?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
    </div>
    

    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php echo('<p class="post-nav">'); posts_nav_link(' ', __('<span class="post-nav-back">&laquo; Previous</span>'), __('<span class="post-nav-next">More &raquo;</span>')); echo('</p>'); ?>
  </div>
  <div id="secondary">
	  <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
      <?php endif; ?>
  </div>
  
</div>
</div>
<?php get_footer(); ?>
