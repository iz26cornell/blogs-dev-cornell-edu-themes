<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity" class="theme-gray75">

<!-- 75 px -->
<!-- A. cornell -->
	<div id="cu-logo">
		<a href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/cornell_identity/cu_logo_white75.gif" alt="Cornell University" /></a> 
	</div>

<!-- B. search cornell (75px 88px) -->
	<div id="search-form">
		<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" >
			<div id="search-input">
				<label for="search-form-query">SEARCH:</label>
				<input type="text" value="" name="s" id="search-form-query" size="20" />
				<input type="submit" id="search-form-submit" name="btnG" value="go" />
			</div>
			<div id="search-filters">
				<input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
				<label for="search-filters1">This Site</label>
				<input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
				<label for="search-filters2">Cornell</label>
				<a href="http://www.cornell.edu/search/">more options</a>
			</div>
		</form>
	</div>
</div>