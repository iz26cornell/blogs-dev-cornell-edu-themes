<?php
/**
 * @package WordPress
 * @subpackage Cornell
 */

get_header(); ?>
<div id="content">
<div id="content-wrap">

  <div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php if (function_exists('wordpress_breadcrumbs')) wordpress_breadcrumbs(); ?>
    <h2><?php the_title(); ?></h2>
    <?php the_content(__('Read more'));?>
    <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php echo('<p class="post-nav">'); posts_nav_link(' ', __('<span class="post-nav-back">&laquo; Previous</span>'), __('<span class="post-nav-next">More &raquo;</span>')); echo('</p>'); ?>
  </div>
  <div id="secondary">
		<div id="pages" class="widget_pages">
		  <h3>In this section:</h3>
		<ul>
		  <?php wp_list_pages('title_li=&depth=4&link_before=<span>&link_after=</span>'); ?>
		</ul>
	    </div>
	  <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<div id="sidebar-content">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</div>
      <?php endif; ?>
      
      <?php if ( get_post_meta($post->ID, 'Related Posts', false) ) : ?>
		 <div id="related">
			<div id="pages" class="widget-container">
					<?php
						global $post;
						$post_id = $post->ID;
						//$related = get_post_meta($post->ID, 'Related Posts', false);
						$mypages = array();
						$linkcount = 0;
						$maxlinks = 6;
						foreach( get_post_meta($post_id, 'Related Posts', false) as $tag ) { // for each tag
							foreach( get_pages( array('meta_value' => $tag ) ) as $page_found ) { // add each page
								array_push($mypages, $page_found);
							}
						}
						if ( count($mypages) > 1 ) { ?>
						
						<h3 class="widget-title">See also:</h3>
						<ul>
						
							<?php foreach( $mypages as $page ) {
								if ($page->ID != $post_id && $linkcount < $maxlinks) { ?>
									<li><a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a></li>
									<?php $linkcount++;
								} // end if
							} // end each ?>
						</ul>
						<?php } // end if ?>
	
			</div>
		</div>
		<?php endif; ?>  
      
  </div>
</div>
</div>
<?php get_footer(); ?>
