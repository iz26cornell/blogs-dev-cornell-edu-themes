<?php
/**
 * Template Name: Homepage
 * @package WordPress
 * @subpackage Cornell
 */
get_header(); ?>
<div id="content">
<div id="content-wrap">
  <div id="secondary">
  	<?php if ( is_active_sidebar( 'sidebar-home' ) ) : ?>
		<?php dynamic_sidebar( 'sidebar-home' ); ?>
	<?php endif; ?>
  </div>
  <div id="main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php if ( is_front_page() ) { ?>
				<h2 class="entry-title"><?php the_title(); ?></h2>
			<?php } else { ?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php } ?>

			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
				<?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
			</div><!-- .entry-content -->
		</div><!-- #post-## -->

		<?php ?> <?php comments_template( '', true ); ?> <?php ?>

	<?php endwhile; ?>
	
  </div>
  
  <div id="tertiary">
  	<?php if ( is_active_sidebar( 'rightbar-home' ) ) : ?>
		<?php dynamic_sidebar( 'rightbar-home' ); ?>
	<?php endif; ?>
  </div>
  
</div>
</div>
<?php get_footer(); ?>
