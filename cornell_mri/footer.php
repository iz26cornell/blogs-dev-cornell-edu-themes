</div>
<!-- begin footer -->
<div id="footer">
	<div id="footer-content">
	<div id="footer-content-wrap">
		<div id="footer-left">
			<ul>
				<li>&copy;<?php echo date("Y");?> <a href="http://www.cornell.edu/">Cornell University</a></li>
				<?php wp_register(); ?>
				<li class="last"><?php wp_loginout(); ?></li>
			</ul>
		</div>
		<div id="footer-right">
		<?php if ( is_active_sidebar( 'footer-widget-area' ) ) : ?>
			<?php dynamic_sidebar( 'footer-widget-area' ); ?>
		<?php endif; ?>
		</div>
	</div>
	<div id="footer-message-wrap">
		<?php if ( is_active_sidebar( 'footer-message' ) ) : ?>
			<?php dynamic_sidebar( 'footer-message' ); ?>
		<?php endif; ?>
	</div>
	</div>
</div>
<div id="wp-footer"><?php wp_footer(); ?></div>

<?php
if (isset($_SESSION["iws_amMobileDevice"]) && $_SESSION["iws_amMobileDevice"] === "TRUE") {
	echo "<div id=\"mobileswitch\">";
    echo "<h3>Switch to:<br />";
    echo "<a href=\"?startMobileSession=1\">Mobile Version</a>";
    echo "<a class=\"last\" href=\"?killMobileSession=1\">Web Version</a>";
    echo "</h3>";
    echo "</div>";
}
?>

</body></html>