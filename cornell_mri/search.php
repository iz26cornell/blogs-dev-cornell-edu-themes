<?php
if (isset($_GET['btnG'])) {
	session_start();
	$selected_radio = $_GET['sitesearch'];
	
	if ($selected_radio == 'cornell') {
		$search_terms = urlencode($_GET['s']);
		$URL="http://www.cornell.edu/search" . "?q=" . $search_terms . "&client=default_frontend&proxystylesheet=default_frontend";
		print $URL;
		header ("Location: $URL");
	}
}
?>
<?php get_header(); ?>
<div id="content">
<div id="content-wrap">

  <div id="main">
	<h2>Search Results</h2>
	<?php
		$searchq = new WP_Query("s=$s&showposts=-1");
		$key = wp_specialchars($s, 1);
		$count = $searchq->post_count;
	?>
	<p class="search-results-msg">Showing results for: <strong>"<?php echo $key; ?>"</strong><?php if (have_posts() && $_GET['s'] != "") : ?> <span class="search-results-count">(<?php echo $count; ?> found)</span><?php endif; ?></p>
    
    <?php if (have_posts() && $_GET['s'] != "") : while (have_posts()) : the_post(); ?>
    <h4 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark">
      <?php the_title(); ?>
      </a></h4>
      <?php the_excerpt();?>

    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php if (have_posts() && $_GET['s'] != "") : echo('<p class="post-nav">'); posts_nav_link(' ', __('<span class="post-nav-back">&laquo; Previous</span>'), __('<span class="post-nav-next">More &raquo;</span>')); echo('</p>'); endif; ?>
  </div>
  <div id="secondary">
	
	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	<?php endif; ?>
	</div>
  
</div>
</div>
<?php get_footer(); ?>