<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="distribution" content="global" />
<meta name="robots" content="follow, all" />
<meta name="language" content="en, sv" />
<title>
<?php wp_title(''); ?>
<?php if(wp_title('', false)) { echo ' |'; } ?>
 <?php bloginfo('name');
 	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	?>
</title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<!-- leave this for stats please -->
<link rel="Shortcut Icon" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_get_archives('type=monthly&format=link'); ?>
<?php wp_head(); ?>
<?php if (function_exists('mobile_screen') ) { mobile_screen(); } ?>

<!--[if lte IE 8]>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/styles/ie.css" media="all" />
<![endif]-->


<link rel="stylesheet" rev="stylesheet" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/scripts/colorbox/colorbox.css" media="all" />
<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/scripts/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#campaign-gallery a').colorbox({rel:'gal',transition:"none",opacity:0.8});
	});
</script>

</head>
<body <?php body_class(); ?>>
<?php include (TEMPLATEPATH . '/banner.php'); ?>
<div id="wrap" class="twocolumn-left<?php if ( is_home() || is_front_page() ) echo ' home'; ?>">
	<div id="header">
		<div id="campaign" class="dropshadow-light">
	<?php if ( is_home() || is_front_page() ) : ?>
		<div id="campaign-content">
	<?php endif; ?>
			<div id="navigation" class="dropshadow-glow">
				<ul>
					<li class="nav1 first"><a href="<?php echo home_url( '/' ); ?>"><span>Home</span></a></li>
					<li class="nav2"><a href="<?php echo home_url( '/' ); ?>about/"><span>About</span></a></li>
					<li class="nav3"><a href="<?php echo home_url( '/' ); ?>people/"><span>People</span></a></li>
					<li class="nav4"><a href="<?php echo home_url( '/' ); ?>research/"><span>Research</span></a></li>
					<li class="nav5"><a href="<?php echo home_url( '/' ); ?>education/"><span>Education-Training</span></a></li>
					<li class="nav6 last"><a href="<?php echo home_url( '/' ); ?>contact/"><span>Contact</span></a></li>
				</ul>
			</div>
			<div id="identity">
				<a href="<?php echo home_url( '/' ); ?>">
					<h1><?php bloginfo('name'); ?></h1>
				</a>
				<h2>
				<?php if ( is_home() || is_front_page() ) : 
						echo $site_description;
					else : 
						echo 'Cornell MRI Facility';
				endif; ?>
				<?php /* dynamic section title -->
				if ( is_home() || is_front_page() ) : 
						echo $site_description;
					elseif (is_category()) :
						single_cat_title();
					elseif (is_page()) :
						echo get_page(array_pop(get_post_ancestors($post->ID)))->post_title;
					else : 
						//bloginfo('name');
				endif; */ ?>
				</h2>
			</div>
		<?php if ( is_home() || is_front_page() ) : ?>
			<div id="campaign-gallery" class="dropshadow-glow-light">
				<?php if ( is_active_sidebar( 'gallery-home' ) ) : ?>
					<?php dynamic_sidebar( 'gallery-home' ); ?>
				<?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
		</div>
	</div>
