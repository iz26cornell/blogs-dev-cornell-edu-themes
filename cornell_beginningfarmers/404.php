<?php get_header(); ?>
<div id="wrap" class="twocolumn-left">
<div id="content">
<div id="content-wrap">
  <div id="main">
    <h2>Not Found, Error 404</h2>
    <p>The page you are looking for no longer exists.</p>
  </div>
  <div id="secondary">
	  <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
      <?php endif; ?>
  </div>
</div>
</div>
<!-- The main column ends  -->
</div>
<?php get_footer(); ?>
