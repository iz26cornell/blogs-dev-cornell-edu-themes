<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="distribution" content="global" />
<meta name="robots" content="follow, all" />
<meta name="language" content="en, sv" />
<title>
<?php wp_title(''); ?>
<?php if(wp_title('', false)) { echo ' |'; } ?>
 <?php bloginfo('name');
 	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	?>
</title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<!-- leave this for stats please -->
<link rel="Shortcut Icon" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_get_archives('type=monthly&format=link'); ?>
<?php wp_head(); ?>
<?php if (function_exists('mobile_screen') ) { mobile_screen(); } ?>

<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws_js/framework/iws_light.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws_js/slidedeck/slidedeck.jquery.lite.js?2"></script>
<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws_js/slidedeck/skin.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws_js/slidedeck/slidedeck.skin.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws_js/slidedeck/skin.css" media="screen" />
<!--[if lte IE 8]>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/styles/ie.css" media="all" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws_js/slidedeck/slidedeck.skin.ie.css" media="screen,handheld" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws_js/slidedeck/skin.ie7.css" media="screen,handheld" />
<![endif]-->

<!-- Initialize -->
<script type="text/javascript">
	var js_path = "<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws_js/framework/";
	iws_init();
</script>
</head>
<body <?php body_class("bfp"); ?>>
<?php include (TEMPLATEPATH . '/banner.php'); ?>
<div id="header">
	<div id="navigation">
		<ul>
			<li class="nav1 first"><a href="<?php echo home_url( '/' ); ?>">Home</a></li>
			<li class="nav2"><a href="<?php echo home_url( '/' ); ?>events/">Events</a></li>
			<li class="nav3"><a href="<?php echo home_url( '/' ); ?>map/">Who Can Help?</a></li>
			<li class="nav4"><a href="<?php echo home_url( '/' ); ?>online-courses/">Online Courses</a></li>
			<li class="nav5"><a href="<?php echo home_url( '/' ); ?>publications/">Publications</a></li>
			<li class="nav6"><a href="<?php echo home_url( '/' ); ?>videos/">Videos</a></li>
			<li class="nav7"><a href="<?php echo home_url( '/' ); ?>farmers/">Plan Your Farm</a></li>
			<li class="nav8 last"><a href="<?php echo home_url( '/' ); ?>contact/">Contact</a></li>
		</ul>
	</div>
	<div id="identity">
		<h1>Northeast Beginning Farmer Project</h1>
		<div id="campaign">
			<a href="<?php echo home_url( '/' ); ?>">
				<img src="<?php header_image(); ?>" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" alt="" />
			</a>
			<div id="section-title"></div>
		</div>
	</div>
</div>
