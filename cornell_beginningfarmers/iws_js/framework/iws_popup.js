/* Popup Launcher 0.1b (ama39)
	-- added close button and temporary "small" and "medium" popup options (4/4/12)
	-- fixed IE iframe caching bug (4/5/12)
   ------------------------------------------- */   

/* Global Options -------------- */
var popup_shadow = true;
var popup_fadein_speed = 200;

/* Global Variables ------------ */
var popup_count = 0;
var popup_type = "none";


/* -----------------------------------------------------------------------------------------
   Initialize Tooltips
   -----------------------------------------------------------------------------------------
   - 
-------------------------------------------------------------------------------------------- */
function popups() {
	
	// Create #popup node ---------- 
	jQuery("body").append("<div id=\"popup-background\"></div>");
	jQuery("#popup-background").css({
		"position": "fixed",
		"display": "none",
		"width": "100%",
		"height": "100%",
		"left": "0",
		"top": "0",
		"z-index": "2000" // resolves conflict with jQuery tabs()
	})
	jQuery("#popup-background").click(function(e) {
		jQuery("#popup").hide();
		jQuery("#popup-background").hide();
	});
	
	jQuery("body").append("<div id=\"popup\"></div>");
	jQuery("#popup").css({
		"position": "fixed",
		"display": "none",
		"z-index": "2001" // resolves conflict with jQuery tabs()
	});
	if (popup_shadow) {
		jQuery("#popup").addClass("dropshadow"); // apply dropshadow preference		
	}
	
	// Setup click events ----------
	jQuery(".popup").each(function() {
		popup_count++;
		jQuery(this).data("popupID",popup_count);
		
		var popup_content = jQuery(this).attr("href");
		var popup_caption = jQuery(this).attr("title");
		
		jQuery(this).click(function(e) {
			
			e.preventDefault();
			
			if (popup_content != "" && popup_content != undefined) {	
				
				var popup_store = document.getElementById("popup-store");
				if (popup_store == null) {
					jQuery("body").append("<div id=\"popup-store\"></div>");
					jQuery("#popup-store").css("display","none");
				}
				if (jQuery("#popup").children().size() > 0 && popup_type == "id") {
					jQuery("#popup-store").append(jQuery("#popup").children());
				}
				jQuery("#popup").empty();
				jQuery("#popup").unbind();
				
				var filetype = popup_content.substr(popup_content.lastIndexOf(".")).toLowerCase();
				if (filetype == ".jpg" || filetype == ".jpeg" || filetype == ".gif" || filetype == ".png") {
					popup_type = "image";
					var img = new Image();
					img.onload = function() {
						jQuery("#popup").html("<img id=\"popup-image\" src=\""+popup_content+"\" />");
						
						if (popup_caption != "" && popup_caption != undefined) {
							//jQuery("#log").append("<div>caption detected</div>");
							jQuery("#popup").append("<p class=\"caption\">"+popup_caption+"</p>");
						}
						
						jQuery("#popup").css({
							"left": (jQuery(window).width()/2 - jQuery("#popup").width()/2).toString()+"px",
							"top": (jQuery(window).height()/2 - jQuery("#popup").height()/2).toString()+"px"
						}).click(function(e) {
							jQuery("#popup").hide();
							jQuery("#popup-background").hide();
						}).fadeIn(popup_fadein_speed);
					};
					img.src = popup_content;
					
					jQuery("#popup-background").show();
					
					// Loading message could go here (for slow connections)
					
				}
				else {
					if (popup_content.indexOf("#") == 0) {
						popup_type = "id";
						//jQuery("#log").append("<div>anchor detected</div>");
						jQuery("#popup").append(jQuery(popup_content).show()).css({
							"left": (jQuery(window).width()/2 - jQuery("#popup").width()/2).toString()+"px",
							"top": (jQuery(window).height()/2 - jQuery("#popup").height()/2).toString()+"px"
						}).fadeIn(popup_fadein_speed);
						
						jQuery("#popup-background").show();
					}
					else {
						popup_type = "iframe";
												
						jQuery("#popup").html("<iframe src=\"" + popup_content + "\" frameborder=\"0\" scrolling=\"auto\" />");
						jQuery("#popup iframe").attr("src",jQuery("#popup iframe").attr("src")); // clears IE iframe caching bug
						
						if (jQuery(this).hasClass("popup-small")) {
							jQuery("#popup iframe").width(400);
							jQuery("#popup iframe").height(250);
						}
						else if (jQuery(this).hasClass("popup-medium")) {
							jQuery("#popup iframe").width(600);
							jQuery("#popup iframe").height(400);
						}
						else {
							jQuery("#popup iframe").width(parseInt(jQuery(window).width()*0.8)); // 80% window width (temporary default)
							jQuery("#popup iframe").height(parseInt(jQuery(window).height()*0.7)); // 70% window height (temporary default)
						}
						
						jQuery("#popup").css({
							"left": (jQuery(window).width()/2 - jQuery("#popup").width()/2).toString()+"px",
							"top": (jQuery(window).height()/2 - jQuery("#popup").height()/2).toString()+"px"
						}).fadeIn(popup_fadein_speed);
						
						jQuery("#popup-background").show();
					}
				}
				// Add Close Button
				jQuery("#popup").append("<div id=\"popup-close\"></div>");
				jQuery("#popup-close").click(function(e) {
					jQuery("#popup").hide();
					jQuery("#popup-background").hide();
				});
				// Preload Rollover Image ("_on")
				var on_state = jQuery("#popup-close").css("background-image").split("url(")[1].split(")")[0];
				var on_state_part1 = on_state.substr(0,on_state.lastIndexOf("."));
				var on_state_part2 = on_state.substr(on_state.lastIndexOf("."));
				jQuery(new Image()).attr("src",on_state_part1+'_on'+on_state_part2);
			}
		});
	});
}