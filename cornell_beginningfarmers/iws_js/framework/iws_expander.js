var global_toggle = 0;

function expander() {
	jQuery(".iws-expander .expand-all").show();
	jQuery(".iws-expander dd").hide();
	jQuery(".iws-expander dd").data("ex",false);
	jQuery(".iws-expander dt").click(function() {
		jQuery(this).next("dd").slideToggle(300);
		if (jQuery(this).next("dd").data("ex") == false) {
			jQuery(this).next("dd").data("ex",true);
			jQuery(this).addClass("expanded");
		}
		else {
			jQuery(this).next("dd").data("ex",false);
			jQuery(this).removeClass("expanded");
		}
	});
	
	jQuery(".iws-expander .expand-all a").click(function() {
		if (window.global_toggle == 0) {
			window.global_toggle = 1;
			jQuery(".iws-expander dt").addClass("expanded");
			jQuery(".iws-expander dd").slideDown(300);
			jQuery(".iws-expander dd").data("ex",true);
			jQuery(".iws-expander .expand-all a").text("close all");
		}
		else {
			window.global_toggle = 0;
			jQuery(".iws-expander dt").removeClass("expanded");
			jQuery(".iws-expander dd").slideUp(300);
			jQuery(".iws-expander dd").data("ex",false);
			jQuery(".iws-expander .expand-all a").text("expand all");
		}
	});
}