<div class="post">
		<h2 class="post-title">Oooops...Not found !</h2>
		<p class="post-info">As outputted by the <em>Server</em> on <em><?php echo date('d M Y h:i a'); ?></em> | Tagged as: <em>Unavailable</em></p>
		<div class="post-content">
			<p>
			The Server can not find the page you are looking for. 
			<br/> 
			You may try to locate it yourself by doing a search or browsing through the archives
			</p>
		</div>
</div>