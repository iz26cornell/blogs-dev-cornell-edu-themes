<?php get_header(); ?>
<div id="main">
		<div id="content">
		<?php if ($posts) : foreach ($posts as $post) : the_post(); ?>
		<div class="post">
        <span class="post-date">
          <?php the_time('l d M Y'); ?>
        </span>
        <h2 class="post-title">          
          <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a>
        </h2>				
			<div class="post-content">
				<?php the_content('Continue Reading &raquo;'); ?>
				<?php wp_link_pages(); ?>
				<p class="post-tags">
					<?php if (function_exists('the_tags')) the_tags('Tags: ', ', ', '<br/>'); ?>
				</p>
				<!--
					<?php trackback_rdf(); ?>
				-->
			</div>
        <p class="post-info">
          <span class="post-comments">
            <?php comments_popup_link('0 comments', '1 comment', '% comments','','comments off'); ?>
          </span><em class="user"><?php the_author_posts_link() ?></em> | <em class="cat"><?php the_category(', ') ?></em>
				<?php edit_post_link(); ?></p>
			<?php comments_template(); ?>
		</div>
			<?php endforeach; else: ?>
			<?php include_once(TEMPLATEPATH.'/notfound.php');?>
			<?php  endif; ?>
		<p align="center"><?php posts_nav_link(' - ','&#171; Prev','Next &#187;') ?></p>


	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>