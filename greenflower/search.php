<?php get_header(); ?>
		<div id="content">
		<?php if ($posts) { ?>	
		<?php $post = $posts[0]; /* Hack. Set $post so that the_date() works. */ ?>
		<div class="post">
			<h2 class="post-title">Search Results for '<?php echo $s; ?>'</h2>			
			<p class="post-info">Did you find what you wanted ?</p>		
		</div>
		<?php } ?>
			<?php if ($posts) : foreach ($posts as $post) : start_wp(); ?>
			<div class="post">
        <span class="post-date">
          <?php the_time('l d M Y'); ?>
        </span>
        <h2 class="post-title">          
          <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a>
        </h2>				
				<div class="post-content"><?php the_content('Continue Reading &#187;'); ?>
          
				<!--
				<?php trackback_rdf(); ?>
				-->
			</div>
        <p class="post-info">
          <span class="post-comments">
            <?php comments_popup_link('0 comments', '1 comment', '% comments','','comments off'); ?>
          </span><em class="user"><?php the_author_posts_link() ?></em> | <em class="cat"><?php the_category(', ') ?></em>
				<?php edit_post_link(); ?></p>
				<p class="post-tags">
					<?php if (function_exists('the_tags')) the_tags('Tags: ', ', ', '<br/>'); ?>
				</p>
		</div>
			<?php endforeach; else: ?>
			<?php include_once(TEMPLATEPATH.'/notfound.php');?>
			<?php  endif; ?>
		<p align="center"><?php posts_nav_link() ?></p>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>