<?php get_header()?>
<div id="content">
<!-- primary content start -->
	<?php 
		/* The following code includes the intro.php to to have an introduction area .
		You can change the contents of intro.php, as you wish. */
	?>
	<?php include_once(TEMPLATEPATH."/intro.php"); ?>
			
	<?php if ($posts) {
		if (get_settings('greenflower_asideid') != "")
			$AsideId = get_settings('greenflower_asideid');		
		function stupid_hack($str)
		{
			return preg_replace('|</ul>\s*<ul class="asides">|', '', $str);
		}
		ob_start('stupid_hack');
		foreach($posts as $post)
		{
			the_post();
	?>
	<?php if ( in_category($AsideId) && !is_single() ) : ?>
		<ul class="asides">
			<li id="p<?php the_ID(); ?>">
				<?php echo wptexturize($post->post_content); ?>							
				<br/>
				<?php comments_popup_link('(0)', '(1)','(%)')?>  | <a href="<?php the_permalink(); ?>" title="Permalink: <?php echo wptexturize(strip_tags(stripslashes($post->post_title), '')); ?>" rel="bookmark">#</a> <?php edit_post_link('(edit)'); ?>
			</li>						
		</ul>
	<?php else: // If it's a regular post or a permalink page ?>
		<div class="post">
			<span class="post-date"><?php the_time('l d M Y'); ?></span>
			<h2 class="post-title">          
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a>
			</h2>				
			<div class="post-content">
				<?php the_content('Continue Reading &raquo;'); ?>
				<?php wp_link_pages(); ?>
				<p class="post-tags">
					<?php if (function_exists('the_tags')) the_tags('Tags: ', ', ', '<br/>'); ?>
				</p>
				<!--
					<?php trackback_rdf(); ?>
				-->
			</div>
			<p class="post-info">
				<span class="post-comments">
					<?php comments_popup_link('0 comments', '1 comment', '% comments','','comments off'); ?>
				</span>
				<em class="user"><?php the_author_posts_link() ?></em> | 
				<em class="cat"><?php the_category(', ') ?></em>
				<?php edit_post_link(); ?>
			</p>			
		</div>
	<?php endif; // end if in category ?>
	<?php
		}
	} // end if (posts)
	else include_once(TEMPLATEPATH.'/notfound.php');			
	?>
	<p align="center">
		<?php posts_nav_link(' - ','&#171; Prev','Next &#187;') ?>
	</p>
</div><!-- CLOSE content -->
<?php get_sidebar();?>
<?php get_footer();?>