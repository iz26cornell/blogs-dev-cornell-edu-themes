<div id="sidebar">
<ul>
<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar() ) : else : ?>
<?php global $gf_pages; if ($gf_pages <> "" ){ ?>
<li>
		<h2>Sub Pages</h2>
		<ul><?php echo $gf_pages ?></ul>
</li>
	<?php }?>		
<li>
  <h2><?php _e('Search'); ?></h2>
	<form id="searchform" method="get" action="<?php bloginfo('siteurl')?>/index.php">
		<input type="text" name="s" id="s" class="textbox" value="<?php echo wp_specialchars($s, 1); ?>" size="15" />
		<input id="btnSearch" type="submit" name="submit" value="<?php _e('Go'); ?>" />
	</form>
  </li>
  <li>
    <h2>
      <?php _e('Tags'); ?>
    </h2>
    <ul>
      <?php wp_list_cats('optioncount=1&hierarchical=1');    ?>
    </ul>
  </li>
  <li>
    <h2>
      <?php _e('Archives'); ?>
    </h2>
    <ul>
      <?php wp_get_archives('type=monthly'); ?>
    </ul>
  </li>
  <li>
    <h2>Pages</h2>
    <ul>
      <?php wp_list_pages('title_li=' ); ?>
    </ul>
  </li>
<?php if (function_exists('wp_tag_cloud')) {	?>
<li>
	<h2><?php _e('Tags'); ?></h2>
	<p><?php wp_tag_cloud(); ?></p>
</li>
<?php } ?>
<?php if(is_home()) {?>
  <?php wp_list_bookmarks(); ?>      
  <li>
		<h2>Meta</h2>
		<ul>
			<?php wp_register(); ?>
			<li><?php wp_loginout(); ?></li>
			<li><a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional">Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a></li>
			<li><a href="http://gmpg.org/xfn/"><abbr title="XHTML Friends Network">XFN</abbr></a></li>
			<li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
			<?php wp_meta(); ?>
		</ul>			
   </li>
  <?php }?>
  <?php endif; ?>
</ul>

</div>
<!-- CLOSE sidebar-->
