<?php get_header(); ?>
<div id="main">
	<div id="content">
	<?php if (have_posts()) : ?>
	<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
    
		<?php /* If this is a category archive */ if (is_category()) { ?>				
		<h2 class="pagetitle">Category Archive for '<?php echo single_cat_title(); ?>'</h2>
	
		<?php /* If this is a Tag archive */ } elseif (function_exists('is_tag')&& is_tag()) { ?>				
		<h2 class="pagetitle">Tag Archive '<?php echo single_tag_title(); ?>'</h2>
		
 		<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h2 class="pagetitle">Daily Archive for <?php the_time('F jS, Y'); ?></h2>
		
		<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h2 class="pagetitle">Monthly Archive for <?php the_time('F, Y'); ?></h2>

		<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h2 class="pagetitle">Yearly Archive for <?php the_time('Y'); ?></h2>
		<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h2 class="pagetitle">Blog Archives</h2>

		<?php } ?>
		<?php while (have_posts()) : the_post(); ?>
		<div class="post">
        <span class="post-date">
          <?php the_time('l d M Y'); ?>
        </span>
        <h2 class="post-title">          
          <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a>
        </h2>				
			<div class="post-content">
				<?php the_excerpt(); ?>
				<p class="post-tags">
					<?php if (function_exists('the_tags')) the_tags('Tags: ', ', ', '<br/>'); ?>
				</p>
				<!--
					<?php trackback_rdf(); ?>
				-->
			</div>
        <p class="post-info">
          <span class="post-comments">
            <?php comments_popup_link('0 comments', '1 comment', '% comments','','comments off'); ?>
          </span><em class="user"><?php the_author_posts_link() ?></em> | <em class="cat"><?php the_category(', ') ?></em>
				<?php edit_post_link(); ?></p>
			<?php comments_template(); ?>
		</div>
	
		<?php endwhile; ?>

		<p align="center"><?php posts_nav_link(' - ','&#171; Prev','Next &#187;') ?></p>
		
	<?php else : include_once(TEMPLATEPATH.'/notfound.php');?>
	<?php endif; ?>

	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>