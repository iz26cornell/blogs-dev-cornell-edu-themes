<?php get_header()?>
<div id="content">
<?php if ($posts) : foreach ($posts as $post) : start_wp(); ?>
	<div class="post">
        <h2 class="post-title">          
          <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a>
        </h2>				
		<div class="post-content">
			<?php the_content('Continue Reading &#187;'); ?>
			<?php $gf_pages = wp_list_pages( 'sort_column=menu_order&depth=1&title_li=&echo=0&child_of=' . $id );?>
					<?php if ($gf_pages <> "" ){?>
					<small>Please see the sidebar for other sub pages.</small>
					<?php }?>
    		<!--
				<?php trackback_rdf(); ?>
			-->
		</div>
        <p class="post-info">
          <span class="post-comments">
            <?php comments_popup_link('0 comments', '1 comment', '% comments','','comments off'); ?>
          </span><em class="user"><?php the_author_posts_link() ?></em> <?php edit_post_link(); ?></p>
			<?php comments_template(); ?>
	</div>
			<?php endforeach; else: ?>
			<?php include_once(TEMPLATEPATH.'/notfound.php'); ?>
			<?php  endif; ?>
		<p align="center"><?php posts_nav_link(' - ','&#171; Prev','Next &#187;') ?></p>


</div><!-- CLOSE content -->
<?php get_sidebar();?>
<?php get_footer();?>