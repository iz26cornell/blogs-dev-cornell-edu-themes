<hr />
<div id="footer">
  <div id="footer-content">
        <p><span>&copy;<?php echo date("Y");?> <a href="http://www.cornell.edu/">Cornell University</a></span> Powered by <a href="http://edublogs.org/campus">Edublogs Campus</a> and running on <a href="http://blogs.cornell.edu">blogs.cornell.edu</a></p>
    <div class="feeds"><a class="rss-entries" href="<?php bloginfo('rss2_url'); ?>">Entries</a> <a class="rss-comments" href="<?php bloginfo('comments_rss2_url'); ?>">Comments</a></div>
  </div>
</div>

<?php do_action('wp_footer'); ?>


</body></html>