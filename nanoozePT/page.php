<?php get_header(); ?>
<?php get_sidebar(); ?>

     <div id="main-body">

			<?php /* The loop */ ?>
            <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
			<?php while ( have_posts() ) : the_post(); ?>
                <h1 class="entry-title"><?php if ( is_page('Artigos') ) { echo 'Artigos @ Nanooze!'; } elseif ( is_page('Blog') ) { echo 'Nanooze Blog'; } else { the_title(); } ?></h1>
                <div class="entry-content">
                	<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
					<?php the_content(); ?>
        			<?php if ( is_page('Bem-vindo') ) : ?>
						<div id="col1">
							<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Meet a Scientist') ) : ?><?php endif; ?>
        				</div>
						<div id="col2">
							<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('What\'s New') ) : ?><?php endif; ?>
        				</div>
					<?php endif; ?>
                </div><!-- .entry-content -->
                <?php if (is_page('Pesquisa')) { get_search_form(); } ?>
				<?php edit_post_link( __( 'Edit', 'nanooze' ), '<span class="edit-link">', '</span>' ); ?>
				<?php comments_template(); ?>
 			<?php endwhile; ?>

	</div>
    
<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>
