<header class="page-header">
    <h1 class="page-title"><?php _e( 'Nada Foi Encontrado', 'nanooze' ); ?></h1>
</header>

<div class="page-content">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

	<p><?php printf( __( 'Pronto para publicar o seu primeiro post? <a href="%1$s">Comece aqui</a>.', 'nanooze' ), admin_url( 'post-new.php' ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

	<p><?php _e( 'Desculpe, mas nada foi encontrado termos da sua pesquisa. Por favor, tente novamente com palavras-chave diferentes.', 'nanooze' ); ?></p>
	<?php get_search_form(); ?>

	<?php elseif ( is_archive() ) : ?>

	<p><?php _e( 'Desculpe, nenhum post nesta categoria.', 'nanooze' ); ?></p>

	<?php else : ?>

	<p><?php _e( 'Parece que n�o consegue encontrar o que voc� est� procurando. Talvez a pesquisa pode ajudar.', 'nanooze' ); ?></p>
	<?php get_search_form(); ?>

	<?php endif; ?>
</div><!-- .page-content -->
