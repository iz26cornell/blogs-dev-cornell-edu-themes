<?php
/**
 * The template for displaying search forms.
 *
 * @package WordPress
 * @subpackage Total
 * @since Total 1.0
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<label>
					<span class="screen-reader-text">Procurar:</span>
	<input type="search" class="search-field" name="s" title="Procurar:" value="<?php _e( 'Pesquisa...', 'nanooze' ); ?>" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" />
				</label>
				<input type="submit" class="search-submit" value="Pesquisa">
</form>