<?php get_header(); ?>
<?php get_sidebar(); ?>

	<div id="main-body">
    
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
				<?php nanooze_post_nav(); ?>
				<?php comments_template(); ?>

			<?php endwhile; ?>

	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>