<?php if ( is_active_sidebar( 'sidebar-10' ) ) : ?>

	<div id="secondary"<?php if ( !is_page('Bem-vindo') ) { echo ' class="secondary_page"'; } ?>>

			<?php dynamic_sidebar( 'sidebar-10' ); ?>

	</div>

<?php endif; ?>