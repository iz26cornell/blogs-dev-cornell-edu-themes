<?php get_header(); ?>
<?php get_sidebar(); ?>
           
	<div id="main-body" class="content-area">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
 
                <h1 class="archive-title"><?php printf( __( 'Posts com a tag: %s', 'nanooze' ), single_tag_title( '', false ) ); ?></h1>

				<?php if ( tag_description() ) : // Show an optional tag description ?>
				<div class="archive-meta"><?php echo tag_description(); ?></div>
				<?php endif; ?>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
					<?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) the_post_thumbnail(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php nanooze_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>