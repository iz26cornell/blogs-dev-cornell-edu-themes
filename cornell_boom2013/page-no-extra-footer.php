<?php
/**
 * Template Name: Default, no extra footer
 * @package WordPress
 * @subpackage Cornell
 * @since Cornell 1.0
 */

get_header(); ?>

		<div id="content-container">
			<div id="content" role="main">

			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<?php
					// query for "Related Links" in sidebar (ama39 2/29/12)
					global $wp_query;
					global $post_id;
					$post_id = $wp_query->post->ID;
				?>
				
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php } ?>

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

				<?php /*?> <?php comments_template( '', true ); ?> <?php */?>

			<?php endwhile; ?>

			</div><!-- #content -->
		</div><!-- #content-container -->

<?php get_sidebar(); ?>
	</div><!-- #content-box -->
</div><!-- #container -->
</div></div><!-- #wrap -->
<?php get_footer(); ?>