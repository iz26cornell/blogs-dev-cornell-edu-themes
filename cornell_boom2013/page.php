<?php
/**
 * @package WordPress
 * @subpackage Cornell
 */

get_header(); ?>

		<div id="content-container">
			<div id="content" role="main">

			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<?php
					// query for "Related Links" in sidebar (ama39 2/29/12)
					global $wp_query;
					global $post_id;
					$post_id = $wp_query->post->ID;
				?>
				
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php } ?>

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

				<?php /*?> <?php comments_template( '', true ); ?> <?php */?>

			<?php endwhile; ?>

			</div><!-- #content -->
		</div><!-- #content-container -->

<?php get_sidebar(); ?>
	</div><!-- #content-box -->
</div><!-- #container -->
</div></div><!-- #wrap -->
<div id="home-extra-wrap">
  <div id="content-secondary">
    <ul class="widget-list1">
      <?php // A first featured sidebar for widgets. Cornell uses the secondary widget area for three column layouts.
			if ( ! dynamic_sidebar( 'secondary-widget-area1' ) ) : ?>
      <?php endif; ?>
    </ul>
    <ul class="widget-list2">
      <?php // A second sidebar for widgets. Cornell uses the secondary widget area for three column layouts.
			if ( ! dynamic_sidebar( 'secondary-widget-area2' ) ) : ?>
      <?php endif; ?>
    </ul>
  </div>
</div>
<?php get_footer(); ?>