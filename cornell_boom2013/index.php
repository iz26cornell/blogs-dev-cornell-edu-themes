<?php
/**
 * @package WordPress
 * @subpackage Cornell
 */

get_header(); ?>

		<div id="content-container">
			<div id="content" role="main">
			<?php get_template_part( 'loop', 'index' ); ?> 
			</div><!-- #content -->
		</div><!-- #content-container -->

<?php get_sidebar(); ?>
	</div><!-- #content-box -->
</div><!-- #container -->
</div></div><!-- #wrap -->
<?php get_footer(); ?>