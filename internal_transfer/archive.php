<?php get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-body">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php
					if ( is_day() ) :
						printf( __( 'Daily Archives: %s', 'internal_transfer' ), get_the_date() );
					elseif ( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'internal_transfer' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'internal_transfer' ) ) );
					elseif ( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'internal_transfer' ), get_the_date( _x( 'Y', 'yearly archives date format', 'internal_transfer' ) ) );
					else :
						_e( 'Archives', 'internal_transfer' );
					endif;
				?></h1>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php internal_transfer_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>