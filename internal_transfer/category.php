<?php get_header(); ?>
<?php get_sidebar(); ?>

	<div id="main-body">

		<?php if ( have_posts() ) : ?>
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            
			<header class="entry-header">
				<h1 class="entry-title"><?php echo single_cat_title(); ?></h1>

				<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
				<?php endif; ?>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php internal_transfer_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>