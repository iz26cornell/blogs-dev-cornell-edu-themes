<?php

if ( ! isset( $content_width ) )
	$content_width = 604;


function internal_transfer_setup() {

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Switches default core markup for search form, comment form,
	 * and comments to output valid HTML5.
	 */
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );


	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Navigation Menu', 'internal_transfer' ) );

	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 604, 270, true );

}
add_action( 'after_setup_theme', 'internal_transfer_setup' );


/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function internal_transfer_scripts_styles() {
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	// Adds Masonry to handle vertical alignment of footer widgets.
	if ( is_active_sidebar( 'sidebar-1' ) )
		wp_enqueue_script( 'jquery-masonry' );

}
add_action( 'wp_enqueue_scripts', 'internal_transfer_scripts_styles' );

/**
 * Filter the page title.
 *
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 * @return string The filtered title.
 */
function internal_transfer_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'internal_transfer' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'internal_transfer_wp_title', 10, 2 );

/**
 * Register two widget areas.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function internal_transfer_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar Top', 'internal_transfer' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Appears on posts and pages in the top of sidebar.', 'internal_transfer' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Sidebar Bottom', 'internal_transfer' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears on posts and pages in the bottom of the sidebar.', 'internal_transfer' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Home Page Widgets', 'internal_transfer' ),
		'id'            => 'sidebar-4',
		'description'   => __( 'Appears in the main content area of the Home page.', 'internal_transfer' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Area One', 'nanooze' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears on the left side of the footer section.', 'nanooze' ),
		'before_widget' => '<div id="footer1">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="section">',
		'after_title'   => '</h2>',

	) );
	register_sidebar( array(
		'name'          => __( 'Footer Area Two', 'nanooze' ),
		'id'            => 'sidebar-5',
		'description'   => __( 'Appears on the right side of the footer section.', 'nanooze' ),
		'before_widget' => '<div id="footer2">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="section">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'internal_transfer_widgets_init' );

if ( ! function_exists( 'internal_transfer_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function internal_transfer_paging_nav() {
	global $wp_query;

	// Don't print empty markup if there's only one page.
	if ( $wp_query->max_num_pages < 2 )
		return;
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'internal_transfer' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'internal_transfer' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'internal_transfer' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'internal_transfer_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
*
* @since Twenty Thirteen 1.0
*
* @return void
*/
function internal_transfer_post_nav() {
	global $post;

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
	<nav class="navigation post-navigation" role="navigation">
		<div class="nav-links">

			<div class="nav-previous"><?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'internal_transfer' ) ); ?></div>
			<div class="nav-next"><?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'internal_transfer' ) ); ?></div>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'internal_transfer_entry_meta' ) ) :
/**
 * Print HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own internal_transfer_entry_meta() to override in a child theme.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */

function internal_transfer_entry_meta() {
	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post">' . __( 'Sticky', 'internal_transfer' ) . '</span>';

	if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
		internal_transfer_entry_date();

	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'internal_transfer' ) );
	if ( $categories_list ) {
		echo '<span class="categories-links">' . $categories_list . '</span>';
	}

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'internal_transfer' ) );
	if ( $tag_list ) {
		echo '<span class="tags-links">' . $tag_list . '</span>';
	}

	// Post author
	if ( 'post' == get_post_type() ) {
		printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'internal_transfer' ), get_the_author() ) ),
			get_the_author()
		);
	}
}
endif;

if ( ! function_exists( 'internal_transfer_entry_date' ) ) :
/**
 * Print HTML with date information for current post.
 *
 * Create your own internal_transfer_entry_date() to override in a child theme.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param boolean $echo (optional) Whether to echo the date. Default true.
 * @return string The HTML-formatted post date.
 */
function internal_transfer_entry_date( $echo = true ) {
	if ( has_post_format( array( 'chat', 'status' ) ) )
		$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'internal_transfer' );
	else
		$format_prefix = '%2$s';

	$date = sprintf( '<span class="date"><time class="entry-date" datetime="%3$s">%4$s</time></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'internal_transfer' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);

	return $date;
}
endif;

if ( ! function_exists( 'internal_transfer_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function internal_transfer_the_attached_image() {
	/**
	 * Filter the image attachment size to use.
	 *
	 * @since Twenty thirteen 1.0
	 *
	 * @param array $size {
	 *     @type int The attachment height in pixels.
	 *     @type int The attachment width in pixels.
	 * }
	 */
	$attachment_size     = apply_filters( 'internal_transfer_attachment_size', array( 724, 724 ) );
	$next_attachment_url = wp_get_attachment_url();
	$post                = get_post();

	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID'
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		// get the URL of the next image attachment...
		if ( $next_id )
			$next_attachment_url = get_attachment_link( $next_id );

		// or get the URL of the first image attachment.
		else
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
	}

	printf( '<a href="../internal_transfer/%1$s" title="%2$s" rel="attachment">%3$s</a>',
		esc_url( $next_attachment_url ),
		the_title_attribute( array( 'echo' => false ) ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;

/*
 *  show page top parent title
 	for use as section title by inserting <?php get_top_level_parent_title(); ?> on the template
 */
function get_top_level_parent_title() {
global $post;
	if ( empty($post->post_parent) )
		{ the_title(); }
	else {
		$ancestors = get_post_ancestors($post->ID);
		echo get_the_title(end($ancestors));
	}
}

/**
 * Return the post URL.
 *
 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or
 * the first link found in the post content.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return string The Link format URL.
 */
function internal_transfer_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );

	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function internal_transfer_body_class( $classes ) {
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	if ( ! is_front_page() )
		$classes[] = the_slug() . ' secondary-page';

	if ( is_front_page() ) {
		$classes[] = 'sidebar-right';
	}
	else  {
		$classes[] = 'sidebar-left';
	}

	return $classes;
}
add_filter( 'body_class', 'internal_transfer_body_class' );


/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 * @return void
 */
function internal_transfer_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'internal_transfer_customize_register' );

/**
 * Enqueue Javascript postMessage handlers for the Customizer.
 *
 * Binds JavaScript handlers to make the Customizer preview
 * reload changes asynchronously.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function internal_transfer_customize_preview_js() {
	wp_enqueue_script( 'internal_transfer-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20130226', true );
}
add_action( 'customize_preview_init', 'internal_transfer_customize_preview_js' );

/*
Plugin Name: Hierarchical Pages
*/

if (!class_exists('SRCS_WP_Widget')) {
  class SRCS_WP_Widget extends WP_Widget
  {
    function form_html($instance) {
      $option_menu = $this->known_params(1);
      foreach (array_keys($option_menu) as $param) {
	$param_display[$param] = htmlspecialchars($instance[$param]);
      }

      foreach ($option_menu as $option_name => $option) {
	$checkval='';
	$desc = '';
	if ($option['desc'])
	  $desc = "<br /><small>{$option['desc']}</small>";
	switch ($option['type']) {
	case 'checkbox':
	  if ($instance[$option_name]) // special HTML and override value
	    $checkval = 'checked="yes" ';
	  $param_display[$option_name] = 'yes';
	  break;
	case '':
	  $option['type'] = 'text';
	  break;
	}
	print '<p style="text-align:right;"><label for="' . $this->get_field_name($option_name) . '">' . 
	  __($option['title']) . 
	  ' <input style="width: 200px;" id="' . $this->get_field_id($option_name) . 
	  '" name="' . $this->get_field_name($option_name) . 
	  "\" type=\"{$option['type']}\" {$checkval}value=\"{$param_display[$option_name]}\" /></label>$desc</p>";
      }
    }
  }
}

class HierPageWidget extends SRCS_WP_Widget
{
  /**
   * Declares the HierPageWidget class.
   *
   */
  function __construct(){
    $widget_ops = array('classname' => 'widget_hier_page', 'description' => __( "Hierarchical Page Directory Widget") );
    $control_ops = array('width' => 300, 'height' => 300);
    $this->WP_Widget('hierpage', __('Hierarchical Pages'), $widget_ops, $control_ops);
  }

  /**
   * Helper function
   *
   */
  function hierpages_list_pages($args = '') {
    global $post;
    global $wp_query;

    if ( !isset($args['echo']) )
      $args['echo'] = 1;

    $output = '';
    
    if(!$args['post_type'])$args['post_type'] = 'page';

    // Query pages.  NOTE: The array is sorted in alphabetical, or menu, order.
    $pages = & get_pages($args);

    $page_info = Array();

    if ( $pages ) {
      $current_post = $wp_query->get_queried_object_id();

      foreach ( $pages as $page ) {
	$page_info[$page->ID]['parent'] = $page->post_parent;
	$page_info[$page->post_parent]['children'][] = $page->ID;
      }

      // Display the front page?
      $front_page = -1; // assume no static front page
      if ('page' == get_option('show_on_front')) {
	$front_page = get_option('page_on_front');
	// Regard flag: always show front page?  Otherwise: Show front page only if it has children
	if (($args['show_home'] == 'yes') || (sizeof($page_info[$front_page]['children']))) {
	  $page_info[$front_page]['show'] = 1;	// always show front page
	}
      }
	
      // add all children of the root node, but only to single depth.
      if ($args['show_root'] == 'yes') {
	foreach ( $page_info[0]['children'] as $child ) {
	  if ($child != $front_page) {
	    $page_info[$child]['show'] = 1;
	  }
	}
      }
      
      if (is_post_type_hierarchical($args['post_type'])) {
	if ($post->ID != $front_page ) {
	  // The current page is always shown, unless it is the static front page (see above)
	  $page_info[$post->ID]['show'] = 0;
	}

	// show the current page's children, if any.
	if (is_array($page_info[$current_post]['children'] )) {
	   foreach ( $page_info[$current_post]['children'] as $child ) {
	      $page_info[$child]['show'] = 1;
	   }
	}

	$post_parent = $page_info[$current_post]['parent'];
	if ($post_parent && ($args['show_siblings'] == 'yes')) {
	  // if showing siblings, add the current page's parent's other children.
	  foreach ( $page_info[$post_parent]['children'] as $child ) {
	    if ($child != $front_page) {
	      $page_info[$child]['show'] = 1;
	    }
	  }

	  // Also show parent node's siblings.
	  $post_grandparent = $page_info[$post_parent]['parent'];
	  if ($post_grandparent) {
	    foreach ( $page_info[$post_grandparent]['children'] as $child ) {
	      if ($child != $front_page) {
		$page_info[$child]['show'] = 1;
	      }
	    }
	  }
	}

	// add all ancestors of the current page.
	while ($post_parent) {
	  $page_info[$post_parent]['show'] = 0;
          // show that page's children, if any.                                                            
          if (is_array($page_info[$post_parent]['children'] )) {
            foreach ( $page_info[$post_parent]['children'] as $child ) {
              $page_info[$child]['show'] = 1;
            }
          }
	  $post_parent = $page_info[$post_parent]['parent'];
	}
      }
      
      // Add pages that were selected
      $my_includes = Array();

      foreach ( $pages as $page ) {
	if ($page_info[$page->ID]['show']) {
	  $my_includes[] = $page->ID;
	}
      }
      if ($args['child_of']) {
        $my_includes[] = $args['child_of'];
      }
      
      if (!empty($my_includes)) {
        // List pages, if any. Blank title_li suppresses unwanted elements.
        $output .= wp_list_pages( Array('title_li' => '',
					'sort_column' => $args['sort_column'],
					'sort_order' => $args['sort_order'],
					'include' => $my_includes,
                                        'post_type'=> $args['post_type']
                                        ) );
      }
    }

    $output = apply_filters('wp_list_pages', $output);
    
    if ( $args['echo'] )
      echo $output;
    else
      return $output;
  }

  /**
   * Displays the Widget
   *
   */
  function widget($args, $instance){

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $known_params = $this->known_params(0);
    foreach ($known_params as $param) {
      if (strlen($instance[$param])) {
	$page_options[$param] = $instance[$param];
      }
    }
      
    if ($instance['menu_order'] == 'yes') { // Deprecated, eliminated upon form display (see below)
      $page_options['sort_column']='menu_order,post_title';
    }

    print $args['before_widget'];
    if ( $title )
      print "{$args['before_title']}{$title}{$args['after_title']}";
    print '<ul>';
    $this->hierpages_list_pages($page_options);
    print "</ul>{$args['after_widget']}";
  }

  function known_params ($options = 0) {
    $option_menu = array('title' => array('title' => 'Title:'),
			 'show_siblings' => array('title' => 'Show siblings to the current page?',
						  'type' => 'checkbox'),
			 'show_root' => array('title' => 'Always show top-level pages?',
					      'type' => 'checkbox'),
			 'show_home' => array('title' => 'Show the static home page?',
					      'desc' => '(always shown if it has child pages)',
					      'type' => 'checkbox'),
			 'child_of' => array('title' => 'Root page ID:'),
			 'exclude' => array('title' => 'Exclude pages:',
					    'desc' => 'List of page IDs to exclude'),
			 'sort_column' => array('title' => 'Sort field:',
						'desc' => 'Comma-separated list: <em>post_title, menu_order, post_date, post_modified, ID, post_author, post_name</em>'),
			 'sort_order' => array('title' => 'Sort direction:',
					       'desc' => '(default: ASC)'),
			 'meta_key' => array('title' => 'Meta Key:'),
			 'meta_value' => array('title' => 'Meta-key Value:',
					     'desc' => 'for selecting pages by custom fields'),
			 'authors' => array('title' => 'Authors:'),
			 'post_status' => array('title' => 'Post status:',
						'desc' => '(default: publish)'),
			 'post_type' => array('title' => 'Post type:',
						'desc' => '(default: page)'),
			 );
    return ($options ? $option_menu : array_keys($option_menu));
  }

  /**
   * Saves the widget's settings.
   *
   */
  function update($new_instance, $old_instance){
    $instance = $old_instance;
    $known_params = $this->known_params();
    unset($instance['menu_order']);
    foreach ($known_params as $param) {
      $instance[$param] = strip_tags(stripslashes($new_instance[$param]));
    }
    $instance['sort_order'] = strtolower($instance['sort_order']) == 'desc'?'DESC':'ASC';
    return $instance;
  }

  /**
   * Creates the edit form for the widget.
   *
   */
  function form($instance){
    $instance = wp_parse_args( (array) $instance, array('title'=>'') );
    if ($instance['menu_order']) {
      $instance['sort_column'] = 'menu_order,post_title';
    }
    if (empty($instance['sort_column'])) {
      $instance['sort_column'] = 'post_title';
    }

    $this->form_html($instance);
  }

}// END class

/**
 * Register Hierarchical Pages widget.
 *
 * Calls 'widgets_init' action after the widget has been registered.
 */
function HierPageInit() {
  register_widget('HierPageWidget');
}

  /*
   * Plugin Name: Hierarchical Categories (combined with Hierarchical Pages)
   */

class HierCatWidget extends SRCS_WP_Widget
{
  /**
   * Declares our class.
   *
   */
  function __construct(){
    $widget_ops = array('classname' => 'widget_hier_cat', 'description' => __( "Hierarchical Category Widget") );
    $control_ops = array('width' => 300, 'height' => 300);
    $this->WP_Widget('hiercat', __('Hierarchical Categories'), $widget_ops, $control_ops);
  }

  /**
   * Helper function
   *
   */
  function hiercat_list_cats($args) {
    global $post;
    global $wp_query;

    if ( !isset($args['echo']) )
      $args['echo'] = 1;
    $sort_column = $args['sort_column'];

    $output = '';

    // Query categories.
    $cats = & get_categories($args);
    if ($cats['errors']) {
      print "<pre>"; print_r($cats); print "</pre>";
      return;
    }
    $cat_info = Array();

    if ( !empty ($cats) ) {
      $current_cat = $wp_query->get_queried_object_id();

      foreach ( $cats as $cat ) {
	$cat_info[$cat->term_id]['parent'] = $cat->category_parent;
	$cat_info[$cat->category_parent]['children'][] = $cat->term_id;
      }

      // add all children of the root node, but only to single depth.
      foreach ( $cat_info[0]['children'] as $child ) {
	$cat_info[$child]['show'] = 1;
      }
      
      // If currently displaying a category, taxonomy, or tag; AND
      // if it is the same as the one in this widget...
      if ((is_category() || is_tax() || is_tag()) && 
	  ($args['taxonomy'] == get_queried_object()->taxonomy ) ) {
	// show the current category's children, if any.
	if (is_array($cat_info[$current_cat]['children'] )) {
	   foreach ( $cat_info[$current_cat]['children'] as $child ) {
	      $cat_info[$child]['show'] = 1;
	   }
	}

	$cat_parent = $cat_info[$current_cat]['parent'];
	if ($cat_parent && ($args['show_siblings'] == 'yes')) {
	  // if showing siblings, add the current category's parent's other children.
	  foreach ( $cat_info[$cat_parent]['children'] as $child ) {
	    $cat_info[$child]['show'] = 1;
	  }

	  # Also show parent node's siblings.
	  $cat_grandparent = $cat_info[$cat_parent]['parent'];
	  if ($cat_grandparent) {
	    foreach ( $cat_info[$cat_grandparent]['children'] as $child ) {
		$cat_info[$child]['show'] = 1;
	    }
	  }
	}
	
	// add all ancestors of the current category.
	while ($cat_parent) {
	  $cat_info[$cat_parent]['show'] = 1;
	  $cat_parent = $cat_info[$cat_parent]['parent'];
	}
      }
      
      $my_includes = Array();
      // Add categories that were selected
      foreach ( $cats as $cat ) {
	if ($cat_info[$cat->term_id]['show']) {
	  $my_includes[] =$cat->term_id;
	}
      }
      
      if (!empty($my_includes)) {
        // List categories, if any. Blank title_li suppresses unwanted elements.
        $qargs = Array('title_li' => '', 'hide_empty' => 0, 'include' => $my_includes,
                      'order' => $args['order'], 'orderby' => $args['orderby'],
                      'show_count' => $args['show_count']);
        if (!empty($args['taxonomy'])) {
          $qargs['taxonomy'] = $args['taxonomy'];
        }
        $output .= wp_list_categories( $qargs );
      }
    }

    $output = apply_filters('wp_list_categories', $output);
    
    if ( $args['echo'] )
      echo $output;
    else
      return $output;
  }

  function known_params ($options = 0) {
    $option_menu = array('title' => array('title' => 'Title:'),
			 'show_siblings' => array('title' => 'Show siblings to the current category?',
						  'type' => 'checkbox'),
			 'include' => array('title' => 'Include:',
					    'desc' => 'Comma-delimited list of category IDs, or blank for all'),
			 'exclude' => array('title' => 'Exclude:'),
			 'orderby' => array('title' => 'Sort field:',
					    'desc' => 'Enter one of: <em>name, count, term_group, slug</em> or a custom value. Default: name'),
			 'order' => array('title' => 'Sort direction:',
					  'desc' => '(default: ASC)'),
			 'child_of' => array('title' => 'Only display Categories below this ID'),
			 'hide_empty' => array('title' => 'Hide empty categories?', 
						  'type' => 'checkbox'),
			 'show_count' => array('title' => 'Show count of category entries?', 
						  'type' => 'checkbox'),
			 'taxonomy' => array('title' => 'Custom taxonomy:'),
			 );
    if ($options) {
      $taxons = get_taxonomies();
      $option_menu['taxonomy']['desc'] = 'Enter one of: <em>' . 
	implode(', ',array_keys($taxons)) . '</em> or blank for post categories.';
    }
    return ($options ? $option_menu : array_keys($option_menu));
  }
  /**
   * Displays the Widget
   *
   */
  function widget($args, $instance){
    $known_params = $this->known_params(0);
    foreach ($known_params as $param) {
      if (strlen($instance[$param]))
	$cat_options[$param] = $instance[$param];
    }
    $cat_options['title'] = apply_filters('widget_title', $cat_options['title']);
    // WordPress defaults to hiding: thus, always specify.
    $cat_options['hide_empty'] = $cat_options['hide_empty'] == 'yes' ? 1 : 0;
      
    print $args['before_widget'];
    if ( strlen($cat_options['title']) )
      print "{$args['before_title']}{$cat_options['title']}{$args['after_title']}";
    print "<ul>";

    $this->hiercat_list_cats($cat_options);
    print "</ul>{$after_widget}";
  }

  /**
   * Saves the widget's settings.
   *
   */
  function update($new_instance, $old_instance){
    $instance = $old_instance;

    $known_params = $this->known_params();
    foreach ($known_params as $param) {
      $instance[$param] = strip_tags(stripslashes($new_instance[$param]));
    }
    return $instance;
  }

  /**
   * Creates the edit form for the widget.
   *
   */
  function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('title'=>'') );

    $this->form_html($instance);
  }

}// END class

/**
 * Register Hierarchical Categories widget.
 *
 * Calls 'widgets_init' action after the widget has been registered.
 */
function HierCatInit() {
  register_widget('HierCatWidget');
}

/*
 * Initialize both widgets
 */

add_action('widgets_init', 'HierCatInit');
add_action('widgets_init', 'HierPageInit');


/*Dimox Breadcrumbs*/
function dimox_breadcrumbs() {

	/* === OPTIONS === */
	$text['home']     = 'Home'; // text for the 'Home' link
	$text['category'] = 'Archive by Category "%s"'; // text for a category page
	$text['search']   = 'Search Results for "%s" Query'; // text for a search results page
	$text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
	$text['author']   = 'Articles Posted by %s'; // text for an author page
	$text['404']      = 'Error 404'; // text for the 404 page

	$show_current   = 1; // 1 - show current post/page/category title in breadcrumbs, 0 - don't show
	$show_on_home   = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$show_home_link = 1; // 1 - show the 'Home' link, 0 - don't show
	$show_title     = 1; // 1 - show the title for the links, 0 - don't show
	$delimiter      = ' &raquo; '; // delimiter between crumbs
	$before         = '<span class="current">'; // tag before the current crumb
	$after          = '</span>'; // tag after the current crumb
	/* === END OF OPTIONS === */

	global $post;
	$home_link    = home_url('/');
	$link_before  = '<span typeof="v:Breadcrumb">';
	$link_after   = '</span>';
	$link_attr    = ' rel="v:url" property="v:title"';
	$link         = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;
	$parent_id    = $parent_id_2 = $post->post_parent;
	$frontpage_id = get_option('page_on_front');

	if (is_home() || is_front_page()) {

		if ($show_on_home == 1) echo '<div class="breadcrumbs"><a href="' . $home_link . '">' . $text['home'] . '</a></div>';

	} else {

		echo '<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">';
		if ($show_home_link == 1) {
			echo '<a href="' . $home_link . '" rel="v:url" property="v:title">' . $text['home'] . '</a>';
			if ($frontpage_id == 0 || $parent_id != $frontpage_id) echo $delimiter;
		}

		if ( is_category() ) {
			$this_cat = get_category(get_query_var('cat'), false);
			if ($this_cat->parent != 0) {
				$cats = get_category_parents($this_cat->parent, TRUE, $delimiter);
				if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
				$cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
				$cats = str_replace('</a>', '</a>' . $link_after, $cats);
				if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
				echo $cats;
			}
			if ($show_current == 1) echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

		} elseif ( is_search() ) {
			echo $before . sprintf($text['search'], get_search_query()) . $after;

		} elseif ( is_day() ) {
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
			echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
			echo $before . get_the_time('d') . $after;

		} elseif ( is_month() ) {
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
			echo $before . get_the_time('F') . $after;

		} elseif ( is_year() ) {
			echo $before . get_the_time('Y') . $after;

		} elseif ( is_single() && !is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
				if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, $delimiter);
				if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
				$cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
				$cats = str_replace('</a>', '</a>' . $link_after, $cats);
				if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
				echo $cats;
				if ($show_current == 1) echo $before . get_the_title() . $after;
			}

		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			echo $before . $post_type->labels->singular_name . $after;

		} elseif ( is_attachment() ) {
			$parent = get_post($parent_id);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			$cats = get_category_parents($cat, TRUE, $delimiter);
			$cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);
			$cats = str_replace('</a>', '</a>' . $link_after, $cats);
			if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);
			echo $cats;
			printf($link, get_permalink($parent), $parent->post_title);
			if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;

		} elseif ( is_page() && !$parent_id ) {
			if ($show_current == 1) echo $before . get_the_title() . $after;

		} elseif ( is_page() && $parent_id ) {
			if ($parent_id != $frontpage_id) {
				$breadcrumbs = array();
				while ($parent_id) {
					$page = get_page($parent_id);
					if ($parent_id != $frontpage_id) {
						$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
					}
					$parent_id = $page->post_parent;
				}
				$breadcrumbs = array_reverse($breadcrumbs);
				for ($i = 0; $i < count($breadcrumbs); $i++) {
					echo $breadcrumbs[$i];
					if ($i != count($breadcrumbs)-1) echo $delimiter;
				}
			}
			if ($show_current == 1) {
				if ($show_home_link == 1 || ($parent_id_2 != 0 && $parent_id_2 != $frontpage_id)) echo $delimiter;
				echo $before . get_the_title() . $after;
			}

		} elseif ( is_tag() ) {
			echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

		} elseif ( is_author() ) {
	 		global $author;
			$userdata = get_userdata($author);
			echo $before . sprintf($text['author'], $userdata->display_name) . $after;

		} elseif ( is_404() ) {
			echo $before . $text['404'] . $after;
		}

		if ( get_query_var('paged') ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
			echo __('Page') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}

		echo '</div><!-- .breadcrumbs -->';

	}
} // end dimox_breadcrumbs()



/**
 * Remove twenty thirteen post format types
 */
function customize_meta_boxes() {
  /* Removes meta boxes from Posts */
  remove_meta_box('formatdiv','post','normal');
}

add_action('admin_init','customize_meta_boxes');



function the_slug() {
	$post_data = get_post($post->ID, ARRAY_A);
	$slug = $post_data['post_name'];
	return $slug;
}


//prevent the visual editor from stripping div tags
add_filter('tiny_mce_before_init', 'change_mce_options');
function change_mce_options( $init ) {
$init['extended_valid_elements'] = 'div[*]';
return $init;
}


/**
 * Create HTML list of nav menu items.
 * Replacement for the native Walker, using the description.
 *
 * @see    http://wordpress.stackexchange.com/q/14037/
 * @author toscho, http://toscho.de
 */
class Description_Walker extends Walker_Nav_Menu
{
    /**
     * Start the element output.
     *
     * @param  string $output Passed by reference. Used to append additional content.
     * @param  object $item   Menu item data object.
     * @param  int $depth     Depth of menu item. May be used for padding.
     * @param  array $args    Additional strings.
     * @return void
     */
    function start_el(&$output, $item, $depth, $args)
    {
        $classes     = empty ( $item->classes ) ? array () : (array) $item->classes;

        $class_names = join(
            ' '
        ,   apply_filters(
                'nav_menu_css_class'
            ,   array_filter( $classes ), $item
            )
        );

        ! empty ( $class_names )
            and $class_names = ' class="'. esc_attr( $class_names ) . '"';

        $output .= "<li id='menu-item-$item->ID' $class_names>";

        $attributes  = '';

        ! empty( $item->attr_title )
            and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
        ! empty( $item->target )
            and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
        ! empty( $item->xfn )
            and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
        ! empty( $item->url )
            and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';

        // insert description for top level elements only
        // you may change this
        $description = ( ! empty ( $item->description ) and 0 == $depth )
            ? '<p class="nav_desc">' . esc_attr( $item->description ) . '</p>' : '';

        $title = apply_filters( 'the_title', $item->title, $item->ID );

        $item_output = $args->before
            . "<span class='list_wrap'><a $attributes>"
            . $args->link_before
            . $title
            . $description
            . '</a></span> '
            . $args->link_after
            . $args->after;

        // Since $output is called by reference we don't need to return anything.
        $output .= apply_filters(
            'walker_nav_menu_start_el'
        ,   $item_output
        ,   $item
        ,   $depth
        ,   $args
        );
    }
}

/**
 * Create HTML list of nav menu items.
 * Replacement for the native Walker, using the description.
 *
 * @see    http://wordpress.stackexchange.com/q/14037/
 * @author toscho, http://toscho.de
 */
class Home_Nav_Walker extends Walker_Nav_Menu
{
    /**
     * Start the element output.
     *
     * @param  string $output Passed by reference. Used to append additional content.
     * @param  object $item   Menu item data object.
     * @param  int $depth     Depth of menu item. May be used for padding.
     * @param  array $args    Additional strings.
     * @return void
     */
    function start_el(&$output, $item, $depth, $args)
    {
        $classes     = empty ( $item->classes ) ? array () : (array) $item->classes;

        $class_names = join(
            ' '
        ,   apply_filters(
                'nav_menu_css_class'
            ,   array_filter( $classes ), $item
            )
        );

        ! empty ( $class_names )
            and $class_names = ' class="'. esc_attr( $class_names ) . '"';

        $output .= "<li id='menu-item-$item->ID' $class_names>";

        $attributes  = '';

        ! empty( $item->attr_title )
            and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
        ! empty( $item->target )
            and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
        ! empty( $item->xfn )
            and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
        ! empty( $item->url )
            and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';

        $title = apply_filters( 'the_title', $item->title, $item->ID );

		
        // add list wrap span tag.
        $item_output = $args->before
            . "<span class='list_wrap'><a $attributes>"
            . $args->link_before
            . $title
            . '</a></span> '
            . $args->link_after
            . $args->after;

        // Since $output is called by reference we don't need to return anything.
        $output .= apply_filters(
            'walker_nav_menu_start_el'
        ,   $item_output
        ,   $item
        ,   $depth
        ,   $args
        );
    }
}

function html_widget_title( $title ) {
	//HTML tag opening/closing brackets
	$title = str_replace( '[', '<', $title );
	$title = str_replace( '[/', '</', $title );

	//<a></a>
	$title = str_replace( 'a]', 'a>', $title );

	return $title;
}
add_filter( 'widget_title', 'html_widget_title' );

/**
 * Plugin Name: Display Posts Shortcode
 * Plugin URI: http://www.billerickson.net/shortcode-to-display-posts/
 * Description: Display a listing of posts using the [display-posts] shortcode
 * Version: 2.3
 * Author: Bill Erickson
 * Author URI: http://www.billerickson.net
 *
 */
 
 
/**
 * To Customize, use the following filters:
 *
 * `display_posts_shortcode_args`
 * For customizing the $args passed to WP_Query
 *
 * `display_posts_shortcode_output`
 * For customizing the output of individual posts.
 * Example: https://gist.github.com/1175575#file_display_posts_shortcode_output.php
 *
 * `display_posts_shortcode_wrapper_open` 
 * display_posts_shortcode_wrapper_close`
 * For customizing the outer markup of the whole listing. By default it is a <ul> but
 * can be changed to <ol> or <div> using the 'wrapper' attribute, or by using this filter.
 * Example: https://gist.github.com/1270278
 */ 
 
// Create the shortcode
add_shortcode( 'display-posts', 'be_display_posts_shortcode' );
function be_display_posts_shortcode( $atts ) {

	// Original Attributes, for filters
	$original_atts = $atts;

	// Pull in shortcode attributes and set defaults
	$atts = shortcode_atts( array(
		'author'              => '',
		'category'            => '',
		'date_format'         => '(n/j/Y)',
		'id'                  => false,
		'ignore_sticky_posts' => false,
		'image_size'          => false,
		'include_content'     => false,
		'include_date'        => false,
		'include_excerpt'     => false,
		'meta_key'            => '',
		'no_posts_message'    => '',
		'offset'              => 0,
		'order'               => 'DESC',
		'orderby'             => 'date',
		'post_parent'         => false,
		'post_status'         => 'publish',
		'post_type'           => 'post',
		'posts_per_page'      => '10',
		'tag'                 => '',
		'tax_operator'        => 'IN',
		'tax_term'            => false,
		'taxonomy'            => false,
		'wrapper'             => 'ul',
	), $atts );

	$author = sanitize_text_field( $atts['author'] );
	$category = sanitize_text_field( $atts['category'] );
	$date_format = sanitize_text_field( $atts['date_format'] );
	$id = $atts['id']; // Sanitized later as an array of integers
	$ignore_sticky_posts = (bool) $atts['ignore_sticky_posts'];
	$image_size = sanitize_key( $atts['image_size'] );
	$include_content = (bool)$atts['include_content'];
	$include_date = (bool)$atts['include_date'];
	$include_excerpt = (bool)$atts['include_excerpt'];
	$meta_key = sanitize_text_field( $atts['meta_key'] );
	$no_posts_message = sanitize_text_field( $atts['no_posts_message'] );
	$offset = intval( $atts['offset'] );
	$order = sanitize_key( $atts['order'] );
	$orderby = sanitize_key( $atts['orderby'] );
	$post_parent = $atts['post_parent']; // Validated later, after check for 'current'
	$post_status = $atts['post_status']; // Validated later as one of a few values
	$post_type = sanitize_text_field( $atts['post_type'] );
	$posts_per_page = intval( $atts['posts_per_page'] );
	$tag = sanitize_text_field( $atts['tag'] );
	$tax_operator = $atts['tax_operator']; // Validated later as one of a few values
	$tax_term = sanitize_text_field( $atts['tax_term'] );
	$taxonomy = sanitize_key( $atts['taxonomy'] );
	$wrapper = sanitize_text_field( $atts['wrapper'] );

	
	// Set up initial query for post
	$args = array(
		'category_name'       => $category,
		'order'               => $order,
		'orderby'             => $orderby,
		'post_type'           => explode( ',', $post_type ),
		'posts_per_page'      => $posts_per_page,
		'tag'                 => $tag,
	);
	
	// Ignore Sticky Posts
	if( $ignore_sticky_posts )
		$args['ignore_sticky_posts'] = true;
	
	// Meta key (for ordering)
	if( !empty( $meta_key ) )
		$args['meta_key'] = $meta_key;
	
	// If Post IDs
	if( $id ) {
		$posts_in = array_map( 'intval', explode( ',', $id ) );
		$args['post__in'] = $posts_in;
	}
	
	// Post Author
	if( !empty( $author ) )
		$args['author_name'] = $author;
		
	// Offset
	if( !empty( $offset ) )
		$args['offset'] = $offset;
	
	// Post Status	
	$post_status = explode( ', ', $post_status );		
	$validated = array();
	$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
	foreach ( $post_status as $unvalidated )
		if ( in_array( $unvalidated, $available ) )
			$validated[] = $unvalidated;
	if( !empty( $validated ) )		
		$args['post_status'] = $validated;
	
	
	// If taxonomy attributes, create a taxonomy query
	if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {
	
		// Term string to array
		$tax_term = explode( ', ', $tax_term );
		
		// Validate operator
		if( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) )
			$tax_operator = 'IN';
					
		$tax_args = array(
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'slug',
					'terms'    => $tax_term,
					'operator' => $tax_operator
				)
			)
		);
		
		// Check for multiple taxonomy queries
		$count = 2;
		$more_tax_queries = false;
		while( 
			isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) && 
			isset( $original_atts['tax_' . $count . '_term'] ) && !empty( $original_atts['tax_' . $count . '_term'] ) 
		):
		
			// Sanitize values
			$more_tax_queries = true;
			$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
	 		$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
	 		$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts['tax_' . $count . '_operator'] : 'IN';
	 		$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
	 		
	 		$tax_args['tax_query'][] = array(
	 			'taxonomy' => $taxonomy,
	 			'field' => 'slug',
	 			'terms' => $terms,
	 			'operator' => $tax_operator
	 		);
	
			$count++;
			
		endwhile;
		
		if( $more_tax_queries ):
			$tax_relation = 'AND';
			if( isset( $original_atts['tax_relation'] ) && in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) ) )
				$tax_relation = $original_atts['tax_relation'];
			$args['tax_query']['relation'] = $tax_relation;
		endif;
		
		$args = array_merge( $args, $tax_args );
	}
	
	// If post parent attribute, set up parent
	if( $post_parent ) {
		if( 'current' == $post_parent ) {
			global $post;
			$post_parent = $post->ID;
		}
		$args['post_parent'] = intval( $post_parent );
	}
	
	// Set up html elements used to wrap the posts. 
	// Default is ul/li, but can also be ol/li and div/div
	$wrapper_options = array( 'ul', 'ol', 'div' );
	if( ! in_array( $wrapper, $wrapper_options ) )
		$wrapper = 'ul';
	$inner_wrapper = 'div' == $wrapper ? 'div' : 'li';

	
	$listing = new WP_Query( apply_filters( 'display_posts_shortcode_args', $args, $original_atts ) );
	if ( ! $listing->have_posts() )
		return apply_filters( 'display_posts_shortcode_no_results', wpautop( $no_posts_message ) );
		
	$inner = '';
	while ( $listing->have_posts() ): $listing->the_post(); global $post;
		
		$image = $date = $excerpt = $content = '';
		
		if  ( in_category('Successful Transfers') )
			$title = '<h3>' . apply_filters( 'the_title', get_the_title() ) . '</h3>';
		
		else
			$title = '<h3><a class="title" href="' . apply_filters( 'the_permalink', get_permalink() ) . '">' . apply_filters( 'the_title', get_the_title() ) . '</a></h3>';
		
		if ( $image_size && has_post_thumbnail() )  
			$image = '<a class="image" href="' . get_permalink() . '">' . get_the_post_thumbnail( $post->ID, $image_size ) . '</a> ';
			
		if ( $include_date ) 
			$date = ' <span class="date">' . get_the_date( $date_format ) . '</span>';
		
		if ( $include_excerpt ) 
			$excerpt = ' <span class="excerpt-dash">-</span> <span class="excerpt">' . get_the_excerpt() . '</span>';
			
		if( $include_content )
			$content = '<div class="content">' . apply_filters( 'the_content', get_the_content() ) . '</div>'; 
		
		$class = array( 'listing-item' );
		$class = apply_filters( 'display_posts_shortcode_post_class', $class, $post, $listing );
		$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $image . $title . $date . $excerpt . $content . '</' . $inner_wrapper . '>';
		
		// If post is set to private, only show to logged in users
		if( 'private' == get_post_status( $post->ID ) && !current_user_can( 'read_private_posts' ) )
			$output = '';
		
		$inner .= apply_filters( 'display_posts_shortcode_output', $output, $original_atts, $image, $title, $date, $excerpt, $inner_wrapper, $content, $class );
		
	endwhile; wp_reset_postdata();
	
	$open = apply_filters( 'display_posts_shortcode_wrapper_open', '<' . $wrapper . ' class="display-posts-listing">', $original_atts );
	$close = apply_filters( 'display_posts_shortcode_wrapper_close', '</' . $wrapper . '>', $original_atts );
	$return = $open . $inner . $close;

	return $return;
}
