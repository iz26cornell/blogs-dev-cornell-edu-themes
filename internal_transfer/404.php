<?php get_header(); ?>
<?php get_sidebar(); ?>

	<div id="main-body">

			<header class="entry-header">
				<h1 class="entry-title"><?php _e( 'Not found', 'internal_transfer' ); ?></h1>
			</header>

			<div class="entry-content subpage">
    
					<p><?php _e( 'This is somewhat embarrassing, isn&rsquo;t it? It looks like nothing was found at this location. Maybe try a search?', 'internal_transfer' ); ?></p>
					<?php get_search_form(); ?>

			</div>
	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>