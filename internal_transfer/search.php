<?php

if (isset($_GET['btnG'])) {
	session_start();
	$selected_radio = $_GET['sitesearch'];
	
	if ($selected_radio == 'people') {
		$search_terms = urlencode($_GET['s']);
		$URL="http://www.cornell.edu/search/" . "?q=" . $search_terms . "&submit=go&tab=people";
		print $URL;
		header ("Location: $URL");
	}
	else {
		$search_terms = urlencode($_GET['s']);
		$URL="http://www.cornell.edu/search/" . "?q=" . $search_terms . "&submit=go&tab=";
		print $URL;
		header ("Location: $URL");
	}
}

get_header(); ?>

<?php get_sidebar(); ?>

	<div id="main-body">

		<?php if ( have_posts() ) : ?>

			<header class="entry-header">
				<h1 class="entry-title"><?php printf( __( 'Search Results for: %s', 'internal_transfer' ), get_search_query() ); ?></h1>
			</header>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php internal_transfer_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>