<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>        
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" media="print" href="<?php echo get_template_directory_uri(); ?>/style/print.css" />
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico?v=2" />
	<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/style/ie.css" />
	<![endif]-->

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
    
	<script>window.jQuery || document.write('<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/jquery-1.9.1.min.js">\x3C/script><script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/jquery-migrate-1.1.1.min.js">\x3C/script>')</script>

	<?php wp_head(); ?>
    
	<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/modernizr.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/framework/iws.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/jquery.isotope.min.js"></script>

	<!-- Initialize -->
	<script type="text/javascript">
		var js_path = '<?php echo get_stylesheet_directory_uri('template_directory'); ?>' + '/js/framework/';
		iws_init();
	</script>

</head>

<body <?php body_class(); ?>>

	<div class="flexible nav-centered">

		<div id="skipnav"><a href="#content">Skip to main content</a></div>

        <div id="search-form">
            <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                <div id="search-input">
                    <label for="search-form-query">SEARCH CORNELL:</label>
                    <input type="text" id="search-form-query" name="s" value="" size="20">
                    <input type="submit" id="search-form-submit" name="btnG" value="go">
                </div>
    
                <div id="search-filters">
                    
                    <input type="radio" name="sitesearch" id="search-filters1" value="pages" checked="checked">
                    <label for="search-filters1">Pages</label>
                    <input type="radio" name="sitesearch" id="search-filters2" value="people">
                    <label for="search-filters2">People</label>

                    <a href="http://www.cornell.edu/search/">more options</a>

                </div>	
            </form>
        </div>
        
		<div id="cu-identity" class="theme-white45">
			<div id="cu-identity-content">

                <div class="divSearchIcon"></div>
				<div id="cu-logo">
                    <a href="<?php echo home_url( '/' ); ?>" title="Cornell University"><img id="cu_logo_img" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/cornell_identity/cu_logo_white45.gif" alt="Cornell University" /></a>
                </div>
			</div>       
		</div>

    <div id="site-identity">
        <div class="site-identity-wrap">
              <a href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a>
        </div>
    </div>
      
    <div id="wrap">
		<div id="header">
    
            <div id="navigation-bar">
                <div id="navigation-wrap">
                <div id="navigation">
                    <h3><em>Navigate</em></h3>
                    <?php //if(is_front_page()) { 
						  	wp_nav_menu(array('menu' => 'home-page-menu', 'walker' => new Home_Nav_Walker)); 
						  //} 
						  //else { 
						  	//wp_nav_menu(array ('menu' => 'subpage-menu', 'walker' => new Description_Walker));
						  //} 
					?>
                </div>
                </div>
            </div>
        
        </div>
    
        <?php if(!is_front_page()) { ?>
        
        <form role="search" method="get" class="smallForm" action="<?php echo home_url( '/' ); ?>">
            <div id="search-input2">
                <input type="text" id="search-form-query2" name="s" value="" size="40">
                <input type="submit" id="search-form-submit2" name="btnG" value="go">
            </div>

            <div id="search-filters4">
                
                <input type="radio" name="sitesearch" id="search-filters3" value="pages" checked="checked">
                <label for="search-filters1">Pages</label>
                <input type="radio" name="sitesearch" id="search-filters4" value="people">
                <label for="search-filters2">People</label>

                <a href="http://www.cornell.edu/search/">more options</a>

            </div>	
        </form>        
        
        <div id="header-band">
            <div id="identity">
                <div id="identity-content">
                   <div id="identity-image"></div>
                   <div id="identity-mobile-image"></div>
				   <h1 class="entry-title"><?php $categories = get_the_category(); foreach($categories as $category) { $cat_name = $category->name; } if(is_single() || is_archive()) { echo $cat_name; } elseif (is_404()) { echo 'Not Found'; } else { echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); } ?></h1>
                </div>
                <div id="campaign-feature"></div>
            </div>
        </div>
        <?php } ?>

    <div id="midband-wrap" class="">
        <div id="midband"></div>
    </div>
    
    <div id="content-wrap">
        <div id="content">

        
            <div id="main">
                
           <div id="main-top">
			   <?php if(is_front_page()) { ?>
        
                    <form role="search" method="get" class="smallForm_home" action="<?php echo home_url( '/' ); ?>">
                        <div id="search-input3">
                            <input type="text" id="search-form-query3" name="s" value="" size="40">
                            <input type="submit" id="search-form-submit3" name="btnG" value="go">
                        </div>
            
                        <div id="search-filters5">
                            
                            <input type="radio" name="sitesearch" id="search-filters6" value="pages" checked="checked">
                            <label for="search-filters1">Pages</label>
                            <input type="radio" name="sitesearch" id="search-filters7" value="people">
                            <label for="search-filters2">People</label>
        
                            <a href="http://www.cornell.edu/search/">more options</a>
        
                        </div>	
                    </form>
                    
                    <p><img id="home-header" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/photos/cornell-land.png" alt="" /></p>
        
			   <?php } ?>
           </div>