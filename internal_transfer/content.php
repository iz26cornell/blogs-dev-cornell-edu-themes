<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<div class="entry-thumbnail">
			<?php the_post_thumbnail(); ?>
		</div>
		<?php endif; ?>

		<?php if ( is_single() || is_category('Successful Transfers') ) : ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
            <?php if ( is_category('Key Dates') ) { echo '<div id="date">Posted ' . internal_transfer_entry_date() . '</div>'; } ?>
		</h1>
		<?php endif; // is_single() ?>

		<!--<div class="entry-meta">
			<?php //internal_transfer_entry_meta(); ?>
			<?php //edit_post_link( __( 'Edit', 'internal_transfer' ), '<span class="edit-link">', '</span>' ); ?>
		</div> .entry-meta -->
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-content subpage">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content <?php if(!is_page('Home')) { echo 'subpage'; } ?>">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'internal_transfer' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'internal_transfer' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	</div><!-- .entry-content -->
    <br />
	<?php endif; ?>

	<footer class="entry-meta">
		<?php if ( comments_open() && ! is_single() ) : ?>
			<div class="comments-link">
				<?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a comment', 'internal_transfer' ) . '</span>', __( 'One comment so far', 'internal_transfer' ), __( 'View all % comments', 'internal_transfer' ) ); ?>
			</div><!-- .comments-link -->
		<?php endif; // comments_open() ?>

		<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
			<?php get_template_part( 'author-bio' ); ?>
		<?php endif; ?>
	</footer><!-- .entry-meta -->
</article><!-- #post -->
