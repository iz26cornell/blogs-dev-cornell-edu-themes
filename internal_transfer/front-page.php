<?php get_header(); ?>
<?php get_sidebar(); ?>

	  <div id="main-body">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>
					</header>

					<div class="entry-content home">
						<div id="the_content"><?php the_content(); ?></div>
                        
						<div id="secondary_home">
							<div class="secondary-section">
								<?php dynamic_sidebar( 'Home Page Widgets' ); ?>
							</div>
						</div>

						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'internal_transfer' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div>

					<footer class="entry-meta">
						<?php //edit_post_link( __( 'Edit', 'internal_transfer' ), '<span class="edit-link">', '</span>' ); ?>
					</footer>
				</article>

				<?php comments_template(); ?>
			<?php endwhile; ?>
            
	 </div>
     
<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>