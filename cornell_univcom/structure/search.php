<?php
/**
 * Controls output elements in search form.
 *
 * @category   Genesis
 * @package    Structure
 * @subpackage Search
 * @author     StudioPress
 * @license    http://www.opensource.org/licenses/gpl-license.php GPL-2.0+
 * @link       http://www.studiopress.com/themes/genesis
 */
