<?php get_header(); ?>

<div id="content">

	<div id="main">
    
    	<div id="main-top"></div>
        
		<?php if ( is_active_sidebar( 'sidebar-1' ) ) { ?>
            <div id="secondary-nav">
                <div class="main-body">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-body">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php
					if ( is_day() ) :
						printf( __( 'Daily Archives: %s', 'smallfarms' ), get_the_date() );
					elseif ( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'smallfarms' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'smallfarms' ) ) );
					elseif ( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'smallfarms' ), get_the_date( _x( 'Y', 'yearly archives date format', 'smallfarms' ) ) );
					else :
						_e( 'Archives', 'smallfarms' );
					endif;
				?></h1>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>

			<?php echo('<p class="post-nav">'); posts_nav_link(' ', __('<span class="post-nav-back">&laquo; Previous</span>'), __('<span class="post-nav-next">More &raquo;</span>')); echo('</p>'); ?>

		<?php else : ?>
			<?php the_content(); ?>
		<?php endif; ?>

		</div><!-- end #main-body -->
        
		<?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
            <div id="secondary">
                <div class="main-body">
                    <?php get_sidebar('secondary'); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-bottom"></div>
        
	</div><!-- #main -->
    
</div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- end #wrap -->





<?php get_footer(); ?>