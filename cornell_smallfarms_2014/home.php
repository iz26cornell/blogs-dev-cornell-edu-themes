<?php
/**
 * Template Name: Homepage, without posts
 * @package WordPress
 * @subpackage Cornell
 */
get_header(); ?>

	<div id="content">

		<div id="main">
  
			<div id="main-top"></div>

            <?php if ( is_active_sidebar( 'sidebar-1' ) ) { ?>
                <div id="secondary-nav">
                    <div class="main-body color-box green">
                        <?php get_sidebar( 'sidebar-1' ); ?>
                    </div>
                </div>
            <?php } ?>
                            
			<div id="main-body">
            
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if ( is_front_page() ) { ?>
                    
                    <?php } else { ?>
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php } ?>
        
                    <div class="entry-content">
                        <?php the_content(); ?>
                        <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
                        <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
                    </div><!-- .entry-content -->
                </div><!-- #post-## -->
        
                <?php ?> <?php /*comments_template( '', true ); */?> <?php ?>
        
            <?php endwhile; ?>
                
			</div>
              
            <?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
                <div id="secondary">
                    <div class="main-body">
                        <?php get_sidebar('secondary'); ?>
                    </div>
                </div>
            <?php } ?>
            
            <div id="main-bottom"></div>
            
        </div><!-- #main -->
        
    </div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- #wrap -->


<?php get_footer(); ?>
