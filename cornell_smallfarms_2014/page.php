<?php
/**
 * The template for displaying all pages
 *
 */

get_header(); ?>

    <div id="content">
    
        <div id="main">
        
            <div id="main-top"></div>
            
            <?php if ( is_active_sidebar( 'sidebar-1' ) ) { ?>
                <div id="secondary-nav">
                    <div class="main-body color-box green">
                    	<span id="section-nav-icon"></span>
                        <?php get_sidebar( 'sidebar-1' ); ?>
                    </div>
                </div>
            <?php } ?>
                            
            <div id="main-body">
                <?php /* The loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>
                
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    
                        <header class="entry-header">
                            <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
                            <div class="entry-thumbnail">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <?php endif; ?>
                            <h2 class="entry-title"><?php the_title(); ?></h2>
                        </header><!-- .entry-header -->
    
                        <div class="entry-content">
                            <?php the_content(); ?>
                            <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'smallfarms' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
                        </div><!-- .entry-content -->
    
                    </article><!-- #post -->
    
                    <footer class="entry-meta">
                        <?php edit_post_link( __( 'Edit', 'smallfarms' ), '<span class="edit-link">', '</span>' ); ?>
                    </footer><!-- .entry-meta -->
                        
                    <?php //comments_template(); ?>
                    
                <?php endwhile; ?>
            </div><!-- end #main-body -->
        
            <?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
                <div id="secondary">
                    <div class="main-body">
                        <?php get_sidebar('secondary'); ?>
                    </div>
                </div>
            <?php } ?>
            
            <div id="main-bottom"></div>
            
        </div><!-- #main -->
        
    </div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- end #wrap -->





<?php get_footer(); ?>



