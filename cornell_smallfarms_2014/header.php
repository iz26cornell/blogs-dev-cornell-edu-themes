<!DOCTYPE html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    
    <link rel="stylesheet" type="text/css" media="print" href="<?php echo get_template_directory_uri(); ?>/style/print.css" />
    
    <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/style/ie.css" />
        <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->
    
    <script type="text/javascript">window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js">\x3C/script><script src="<?php echo get_template_directory_uri(); ?>/js/jquery-migrate-1.1.1.min.js">\x3C/script>')</script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/brwsniff.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.fitvids.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/framework/iws.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/framework/iws_events.js"></script>
    <!--<script src="<?php echo get_template_directory_uri(); ?>/js/framework/iws_rotator.js"></script>-->

    <!-- Slide Deck -->
    <script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/slidedeck/slidedeck.jquery.lite.js?2"></script>
    <script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/slidedeck/skin.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/slidedeck/slidedeck.skin.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/slidedeck/skin.css" media="screen" />
   
    <!-- Initialize -->
    <script type="text/javascript">
        var js_path = "<?php echo get_template_directory_uri(); ?>/js/framework/";
        iws_init();
    </script>
    
    <?php wp_head(); ?>

</head>

<body <?php body_class('theme-gray75'); ?>>
<div id="skipnav"><a href="#content">Skip to main content</a></div>

<div id="cu-identity">
    <div id="cu-brand">

        <div id="cu-logo">
            <a href="http://www.cornell.edu/"><img src="<?php echo get_template_directory_uri(); ?>/images/cornell_identity/cu_logo_gray75.gif" width="240" height="75" alt="Cornell University" /></a>
        </div>
        
		<h3 id="mobile-nav"><em>menu</em></h3>
        <div id="search-button"></div>
        <div id="cu-search">
            <div id="search-form">
                <form method="get" action="<?php echo home_url( '/' ); ?>">
                  <label for="search-form-query">SEARCH:</label>
                  <input type="hidden" name="cx" value="011895235015245787991:gf_zlivn_9i" />
                  <input type="hidden" name="cof" value="FORID:11" />
                  <input type="hidden" name="ie" value="UTF-8" />
                  <input type="text" id="search-form-query" name="s" value="" size="20" />
                  <input type="submit" id="search-form-submit" name="btnG" value="go" />
                  <div id="search-filters">
                    <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
                    <label for="search-filters1">This Site</label>
                    <input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
                    <label for="search-filters2">Cornell</label>
                    <a href="http://www.cornell.edu/search/">more options</a> </div>
                </form>
            </div>
        </div>
        
    </div><!-- #cu-brand -->
</div><!-- #cu-identity -->


<div id="header">

    <div id="header-band">
    
        <div id="navigation-bar">
            <div id="navigation-wrap">
                <div id="navigation">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
                </div>
            </div>
        </div>
        
        <div id="identity">
        
            <div id="identity-content">
            </div>
            
            <div id="campaign-feature">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                    <h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
                    <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
                    <img src="<?php header_image(); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
                </a>
            </div>
            
        </div>
        
    </div>
    
</div>
	
<div id="wrap" <?php if ( is_page_template('page-full-width.php') ) { echo 'class="onecolumn"'; } if ( is_page_template('home-with-posts.php') || is_page_template('home.php') ) { echo 'class="twocolumn-right"'; } ?>>

	<div id="midband-wrap">
		<div id="midband"></div>
	</div>
	
	<div id="content-wrap">