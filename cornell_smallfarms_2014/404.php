<?php get_header(); ?>

<div id="content">

	<div id="main">
    	<div id="main-top"></div>
        
		<?php if ( is_active_sidebar( 'sidebar-1' ) ) { ?>
            <div id="secondary-nav">
                <div class="main-body">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-body">

			<header class="page-header">
				<h1 class="page-title"><?php _e( 'Not Found', 'smallfarms' ); ?></h1>
			</header>
            
			<div class="page-wrapper">
				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'smallfarms' ); ?></p>
					<?php get_search_form(); ?>
				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->
            
		</div><!-- #main-body -->
        
		<?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
            <div id="secondary">
                <div class="main-body">
                    <?php get_sidebar('secondary'); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-bottom"></div>
        
	</div><!-- #main -->
    
</div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- #wrap -->





<?php get_footer(); ?>