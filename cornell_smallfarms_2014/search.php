<?php
/**
 * The template for displaying Search Results pages
 */

get_header(); ?>

<div id="content">

	<div id="main">
    
    	<div id="main-top"></div>
        
		<?php if ( is_active_sidebar( 'sidebar-1' ) ) { ?>
            <div id="secondary-nav">
                <div class="main-body">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-body">

	<h2>Search Results</h2>
	<?php
		$searchq = new WP_Query("s=$s&showposts=-1");
		$key = wp_specialchars($s, 1);
		$count = $searchq->post_count;
	?>
	<p class="search-results-msg">Showing results for: <strong>"<?php echo $key; ?>"</strong><?php if (have_posts() && $_GET['s'] != "") : ?> <span class="search-results-count">(<?php echo $count; ?> found)</span><?php endif; ?></p>
    
    <?php if (have_posts() && $_GET['s'] != "") : while (have_posts()) : the_post(); ?>
    <h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark">
      <?php the_title(); ?>
      </a></h3>
      <?php the_excerpt();?>

    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php if (have_posts() && $_GET['s'] != "") : echo('<p class="post-nav">'); posts_nav_link(' ', __('<span class="post-nav-back">&laquo; Previous</span>'), __('<span class="post-nav-next">More &raquo;</span>')); echo('</p>'); endif; ?>

		</div><!-- #main-body -->
        
		<?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
            <div id="secondary">
                <div class="main-body">
                    <?php get_sidebar('secondary'); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-bottom"></div>
        
	</div><!-- #main -->
    
</div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- end #wrap -->


<?php get_footer(); ?>