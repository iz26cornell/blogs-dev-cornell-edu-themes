<?php

/**
 * Small Farms setup.
 *
 */
if ( function_exists('register_sidebar') )	
    
	register_sidebar( array(
		'name' => __( 'Primary Sidebar', 'Cornell' ),
		'id' => 'sidebar-1',
		'description' => __( 'The primary widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );	
	
	register_sidebar( array(
		'name' => __( 'Secondary Sidebar', 'Cornell' ),
		'id' => 'sidebar-2',
		'description' => __( 'The primary widget area for secondary pages.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Links (all pages)', 'Cornell' ),
		'id' => 'sidebar-4',
		'description' => __( 'A customizable menu of footer links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Text (all pages)', 'Cornell' ),
		'id' => 'sidebar-5',
		'description' => __( 'This content appears under the footer copyright and links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );








add_action( 'after_setup_theme', 'smallfarms_setup' );

if ( ! function_exists( 'smallfarms_setup' ) ):

function smallfarms_setup() {	
	
	// Your changeable header business starts here
	define( 'HEADER_TEXTCOLOR', 'F8F7EB' );
	// No CSS, just an IMG call. The %s is a placeholder for the theme template directory URI.
	define( 'HEADER_IMAGE', '%s/images/sfp/banner_sfp1.jpg' );

	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to smallfarms_header_image_width and smallfarms_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'smallfarms_header_image_width', 954 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'smallfarms_header_image_height', 180 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 960 pixels wide by 160 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See smallfarms_admin_header_style(), below.
	add_custom_image_header( 'smallfarms_header_style', 'smallfarms_admin_header_style', 'smallfarms_admin_header_image' );

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	
	// ------ Small Farms --------->
	register_default_headers( array(
		'banner1' => array(
			'url' => '%s/images/sfp/banner_sfp1.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_sfp1_t.jpg',
			'description' => __( 'Small Farms 1', 'Cornell' )
		),
		'banner2' => array(
			'url' => '%s/images/sfp/banner_sfp2.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_sfp2_t.jpg',
			'description' => __( 'Small Farms 2', 'Cornell' )
		),
		'banner3' => array(
			'url' => '%s/images/sfp/banner_sfp3.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_sfp3_t.jpg',
			'description' => __( 'Small Farms 3', 'Cornell' )
		),
		'banner4' => array(
			'url' => '%s/images/sfp/banner_sfp4.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_sfp4_t.jpg',
			'description' => __( 'Small Farms 4', 'Cornell' )
		),
		'banner5' => array(
			'url' => '%s/images/sfp/banner_sfp5.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_sfp5_t.jpg',
			'description' => __( 'Small Farms 5', 'Cornell' )
		),
		'banner6' => array(
			'url' => '%s/images/sfp/banner_sfp6.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_sfp6_t.jpg',
			'description' => __( 'Small Farms 6', 'Cornell' )
		)
	) );
	
	// ------ Beginning Farmers --->
	/*register_default_headers( array(
		'banner1' => array(
			'url' => '%s/images/sfp/banner_bfp1.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_bfp1_t.jpg',
			'description' => __( 'Beginning Farmers 1', 'Cornell' )
		),
		'banner2' => array(
			'url' => '%s/images/sfp/banner_bfp2.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_bfp2_t.jpg',
			'description' => __( 'Beginning Farmers 2', 'Cornell' )
		),
		'banner3' => array(
			'url' => '%s/images/sfp/banner_bfp3.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_bfp3_t.jpg',
			'description' => __( 'Beginning Farmers 3', 'Cornell' )
		),
		'banner4' => array(
			'url' => '%s/images/sfp/banner_bfp4.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_bfp4_t.jpg',
			'description' => __( 'Beginning Farmers 4', 'Cornell' )
		),
		'banner5' => array(
			'url' => '%s/images/sfp/banner_bfp5.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_bfp5_t.jpg',
			'description' => __( 'Beginning Farmers 5', 'Cornell' )
		),
		'banner6' => array(
			'url' => '%s/images/sfp/banner_bfp6.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_bfp6_t.jpg',
			'description' => __( 'Beginning Farmers 6', 'Cornell' )
		),
		'banner7' => array(
			'url' => '%s/images/sfp/banner_bfp7.jpg',
			'thumbnail_url' => '%s/images/sfp/banner_bfp7_t.jpg',
			'description' => __( 'Beginning Farmers 7', 'Cornell' )
		)
	) );*/
}
endif;

if ( ! function_exists( 'smallfarms_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog
 *
 */
function smallfarms_header_style() {

	// If no custom options for text are set, let's bail
	// get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
	if ( HEADER_TEXTCOLOR == get_header_textcolor() )
		return;
	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( 'blank' == get_header_textcolor() ) :
	?>
		#site-title,
		#site-description {
			position: absolute;
			left: -9000px;
		}
	<?php
		// If the user has set a custom color for the text use that
		else :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?> !important;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif;


if ( ! function_exists( 'smallfarms_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 *
 * Referenced via add_custom_image_header() in smallfarms_setup().
 *
 
 */
function smallfarms_admin_header_style() {
?>
	<style type="text/css">
	.appearance_page_custom-header #headimg {
		background: #393e00;
		border: none;
		text-align: center;
	}
	#headimg h1,
	#desc {
		font-family: Georgia, serif;
	}
	#headimg h1 {
		margin: 0;
		font-weight: normal;
		padding: 2px 0 0 0;
	}
	#headimg h1 a {
		font-size: 36px;
		line-height: 42px;
		text-decoration: none;
	}
	#desc {
		font-size: 18px;
		line-height: 31px;
		padding: 0 0 2px 0;
		font-style: italic;
	}
	<?php
		// If the user has set a custom color for the text use that
		if ( get_header_textcolor() != HEADER_TEXTCOLOR ) :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?>;
		}
	<?php endif; ?>
	#headimg img {
		max-width: 960px;
		width: 100%;
		background: #fff;
		padding-top: 5px;
	}
	</style>
<?php
}
endif;

if ( ! function_exists( 'smallfarms_admin_header_image' ) ) :
/**
 * Custom header image markup displayed on the Appearance > Header admin panel.
 *
 * Referenced via add_custom_image_header() in smallfarms_setup().
 *
 
 */
function smallfarms_admin_header_image() { ?>
	<div id="headimg">
		<?php
		if ( 'blank' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) || '' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) )
			$style = ' style="display:none;"';
		else
			$style = ' style="color:#' . get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) . ';"';
		?>
		<h1><a id="name"<?php echo $style; ?> onclick="return false;" href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
		<div id="desc"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></div>
		<img src="<?php esc_url ( header_image() ); ?>" alt="" />
	</div>
<?php }
endif;


/**
 *  Returns the Cornell options with defaults as fallback
 *
 */
function smallfarms_get_theme_options() {
	$defaults = array(
		'color_scheme' => 'light',
		'theme_layout' => 'content-sidebar',
	);
	$options = get_option( 'smallfarms_theme_options', $defaults );

	return $options;
}

/**
 *  Add a section class to the body tag
 *
 */
add_filter('body_class','body_class_section');
function body_class_section ($classes) {
	global $wpdb, $post;
	if (is_page()) {
		if ($post->post_parent) {
			$parent = end(get_post_ancestors($current_page_id));
		}
		else {
			$parent = $post->ID;
		}
	$post_data = get_post($parent, ARRAY_A);
	$classes[] = 'section-' . $post_data['post_name'];
	}
	return $classes;
}



/**
 *  Disable the website field in visitor comments
 *  -- now handled by comments.php
 
add_filter('comment_form_default_fields', 'comment_form_no_website');
function comment_form_no_website($fields) {
	if(isset($fields['url'])) {
		unset($fields['url']);
	}
	return $fields;
}
*/

/**
 *  Strip any commenter websites already in the database
 *
 */
add_filter('get_comment_author_url','remove_comment_author_url');
function remove_comment_author_url($url) {
    $url = '';
    return $url;
}


/**
 *  Custom "Read More" link for homepage posts
 *
 */
function new_excerpt_more($more) {
	global $post;
	return ' ... <a class="excerpt-readmore" href="'. get_permalink($post->ID) . '">Read More</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

/* Taken from a post by Spitzerg http://wordpress.org/support/topic/blank-search-sends-you-to-the-homepage */
add_filter( 'request', 'my_request_filter' );
function my_request_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}

//add_theme_support( 'post-thumbnails' );


/**
 * Enqueue scripts and styles for the front end.
 *
 */
function smallfarms_scripts_styles() {
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	// Loads our main stylesheet.
	wp_enqueue_style( 'smallfarms-style', get_stylesheet_uri(), array(), '2013-07-18' );

}
add_action( 'wp_enqueue_scripts', 'smallfarms_scripts_styles' );

/**
 * Filter the page title.
 *
 */
function smallfarms_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'smallfarms' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'smallfarms_wp_title', 10, 2 );



/**
 * Extend the default WordPress body classes.
 *
 */
function smallfarms_body_class( $classes ) {
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	if ( ! is_front_page() )
		$classes[] = 'secondary';
		
	if (is_page()) {
 		$classes[] = get_post( $post )->post_name;
	}

	return $classes;
}
add_filter( 'body_class', 'smallfarms_body_class' );



/*
Plugin Name: Hierarchical Pages
Version: 1.6.1
Plugin URI: http://www.wlindley.com/website/hierpage/
Description: Adds sidebar widgets to display a context-based list of "nearby" pages, and to display nested categories.
*/


if (!class_exists('SRCS_WP_Widget')) {
  class SRCS_WP_Widget extends WP_Widget
  {
    function form_html($instance) {
      $option_menu = $this->known_params(1);
      $tdom = 'hierarchical-pages';

      foreach (array_keys($option_menu) as $param) {
	$param_display[$param] = htmlspecialchars($instance[$param]);
      }

      foreach ($option_menu as $option_name => $option) {
	$checkval='';
	$desc = '';
	if (isset($option['desc']) && $option['desc'])
	  $desc = '<br /><small>' . __($option['desc'], $tdom) . '</small>';
	switch ($option['type']) {
	case 'checkbox':
	  if ($instance[$option_name]) // special HTML and override value
	    $checkval = 'checked="yes" ';
	  $param_display[$option_name] = 'yes';
	  break;
	case '':
	  $option['type'] = 'text';
	  break;
	}
	print '<p style="text-align:right;"><label for="' . $this->get_field_name($option_name) . '">' . 
	  __($option['title'], $tdom) . 
	  ' <input style="width: 200px;" id="' . $this->get_field_id($option_name) . 
	  '" name="' . $this->get_field_name($option_name) . 
	  "\" type=\"{$option['type']}\" {$checkval}value=\"{$param_display[$option_name]}\" /></label>$desc</p>";
      }
    }
  }
}

class HierPageWidget extends SRCS_WP_Widget
{
  /**
   * Declares the HierPageWidget class.
   *
   */
  function __construct(){
    $tdom = 'hierarchical-pages';
    $widget_ops = array('classname' => 'widget_hier_page widget_pages',
			'description' => __( "Hierarchical Page Directory Widget", $tdom) );
    $control_ops = array('width' => 300, 'height' => 300);
    $this->WP_Widget('hierpage', __('Hierarchical Pages', $tdom), $widget_ops, $control_ops);
  }

  /**
   * Helper function
   *
   */
  function hierpages_list_pages($args = '') {
    global $post;
    global $wp_query;

    if ( !isset($args['echo']) )
      $args['echo'] = 1;

    $output = '';
    
    if(!isset($args['post_type']) || !$args['post_type'])
      $args['post_type'] = 'page';

    // Query pages.  NOTE: The array is sorted in alphabetical, or menu, order.
    $pages = & get_pages($args);

    $page_info = Array();

    if ( $pages ) {
      $current_post = $wp_query->get_queried_object_id();

      foreach ( $pages as $page ) {
	$page_info[$page->ID]['parent'] = $page->post_parent;
	$page_info[$page->post_parent]['children'][] = $page->ID;
      }

      // Display the front page?
      $front_page = -1; // assume no static front page
      if ('page' == get_option('show_on_front')) {
	$front_page = get_option('page_on_front');
	// Regard flag: always show front page?  Otherwise: Show front page only if it has children
	if (($args['show_home'] == 'yes') || (sizeof($page_info[$front_page]['children']))) {
	  $page_info[$front_page]['show'] = 1;	// always show front page
	}
      }
	
      // add all children of the root node, but only to single depth.
      if ($args['show_root'] == 'yes') {
	foreach ( $page_info[0]['children'] as $child ) {
	  if ($child != $front_page) {
	    $page_info[$child]['show'] = 1;
	  }
	}
      }
      
      if (is_post_type_hierarchical($args['post_type'])) {

	$page_count = 0;
	// show the current page's children, if any.
	if (isset($page_info[$current_post]['children']) && 
	    is_array($page_info[$current_post]['children'] )) {
	  foreach ( $page_info[$current_post]['children'] as $child ) {
	    $page_info[$child]['show'] = 1;
	    $page_count++;
	  }
	}

	$post_parent = $page_info[$current_post]['parent'];
	if ($post_parent && ($args['show_siblings'] == 'yes')) {
	  // if showing siblings, add the current page's parent's other children.
	  foreach ( $page_info[$post_parent]['children'] as $child ) {
	    if ($child != $front_page) {
	      $page_info[$child]['show'] = 1;
	      $page_count++;
	    }
	  }

	  // Also show parent node's siblings.
	  $post_grandparent = $page_info[$post_parent]['parent'];
	  if ($post_grandparent) {
	    foreach ( $page_info[$post_grandparent]['children'] as $child ) {
	      if ($child != $front_page) {
		$page_info[$child]['show'] = 1;
		$page_count++;
	      }
	    }
	  }
	}

	// add all ancestors of the current page.
	while ($post_parent) {
	  $page_info[$post_parent]['show'] = 1;
          // show that page's children, if any.                                                            
          if (is_array($page_info[$post_parent]['children'] )) {
            foreach ( $page_info[$post_parent]['children'] as $child ) {
              $page_info[$child]['show'] = 1;
	      $page_count++;
            }
          }
	  $post_parent = $page_info[$post_parent]['parent'];
	}

	if (($post->ID != $front_page) && ($page_count > 0) ) {
	  // The current page is always shown, unless
	  // 1. it is the static front page (see above), or
	  // 2. no other pages are displayed
	  $page_info[$post->ID]['show'] = 1;
	}

      }
      
      // Add pages that were selected
      $my_includes = Array();

      foreach ( $pages as $page ) {
	if (isset($page_info[$page->ID]['show']) && $page_info[$page->ID]['show']) {
	  $my_includes[] = $page->ID;
	}
      }
      if (isset($args['child_of']) && $args['child_of']) {
        $my_includes[] = $args['child_of'];
      }
      
      if (!empty($my_includes)) {
        // List pages, if any. Blank title_li suppresses unwanted elements.
        $output .= wp_list_pages( Array('title_li' => '',
					'sort_column' => $args['sort_column'],
					'sort_order' => $args['sort_order'],
					'include' => $my_includes,
                                        'post_type'=> $args['post_type'],
					'echo' => $args['echo']
                                        ) );
      }
    }

    $output = apply_filters('wp_list_pages', $output);
    
    if ( $args['echo'] )
      echo $output;
    else
      return $output;
  }

  /**
   * Displays the Widget
   *
   */
  function widget($args, $instance){

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $known_params = $this->known_params(0);
    foreach ($known_params as $param) {
      if (strlen($instance[$param])) {
	$page_options[$param] = $instance[$param];
      }
    }
      
    if (isset($instance['menu_order']) && $instance['menu_order'] == 'yes') { 
      // Deprecated, eliminated upon form display (see below)
      $page_options['sort_column']='menu_order,post_title';
    }

    $page_options['echo'] = 0;
    $output = $this->hierpages_list_pages($page_options);
    if (strlen($output)) {
      print $args['before_widget'];
      if ( $title )
	print "{$args['before_title']}{$title}{$args['after_title']}";
      print "<ul>{$output}</ul>{$args['after_widget']}";
    }
  }

  function known_params ($options = 0) {
    $option_menu = array('title' => array('title' => 'Title:'),
			 'show_siblings' => array('title' => 'Show siblings to the current page?',
						  'type' => 'checkbox'),
			 'show_root' => array('title' => 'Always show top-level pages?',
					      'type' => 'checkbox'),
			 'show_home' => array('title' => 'Show the static home page?',
					      'desc' => '(always shown if it has child pages)',
					      'type' => 'checkbox'),
			 'child_of' => array('title' => 'Root page ID:'),
			 'exclude' => array('title' => 'Exclude pages:',
					    'desc' => 'List of page IDs to exclude'),
			 'sort_column' => array('title' => 'Sort field:',
						'desc' => 'Comma-separated list: <em>post_title, menu_order, post_date, post_modified, ID, post_author, post_name</em>'),
			 'sort_order' => array('title' => 'Sort direction:',
					       'desc' => '(default: ASC)'),
			 'meta_key' => array('title' => 'Meta Key:'),
			 'meta_value' => array('title' => 'Meta-key Value:',
					     'desc' => 'for selecting pages by custom fields'),
			 'authors' => array('title' => 'Authors:'),
			 'post_status' => array('title' => 'Post status:',
						'desc' => '(default: publish)'),
			 'post_type' => array('title' => 'Post type:',
						'desc' => '(default: page)'),
			 );
    return ($options ? $option_menu : array_keys($option_menu));
  }

  /**
   * Saves the widget's settings.
   *
   */
  function update($new_instance, $old_instance){
    $instance = $old_instance;
    $known_params = $this->known_params();
    unset($instance['menu_order']);
    foreach ($known_params as $param) {
      $instance[$param] = strip_tags(stripslashes($new_instance[$param]));
    }
    $instance['sort_order'] = strtolower($instance['sort_order']) == 'desc'?'DESC':'ASC';
    return $instance;
  }

  /**
   * Creates the edit form for the widget.
   *
   */
  function form($instance){
    $instance = wp_parse_args( (array) $instance, array('title'=>'') );
    if ($instance['menu_order']) {
      $instance['sort_column'] = 'menu_order,post_title';
    }
    if (empty($instance['sort_column'])) {
      $instance['sort_column'] = 'post_title';
    }

    $this->form_html($instance);
  }

}// END class

/**
 * Register Hierarchical Pages widget.
 *
 * Calls 'widgets_init' action after the widget has been registered.
 */
function HierPageInit() {
  register_widget('HierPageWidget');
}

function HierPageLoad() {
  $plugin_dir = basename(dirname(__FILE__));
  load_plugin_textdomain('hierarchical-pages', false, $plugin_dir . '/languages');
}

  /*
   * Plugin Name: Hierarchical Categories (combined with Hierarchical Pages)
   * Plugin URI: http://www.wlindley.com/
   * Description: Adds a sidebar widget to display a context-based list of "nearby" categories.
   * Author: William Lindley
   * Author URI: http://www.wlindley.com/
   */

class HierCatWidget extends SRCS_WP_Widget
{
  /**
   * Declares our class.
   *
   */
  function __construct(){
    $tdom = 'hierarchical-pages';
    $widget_ops = array('classname' => 'widget_hier_cat', 'description' => __( "Hierarchical Category Widget", $tdom) );
    $control_ops = array('width' => 300, 'height' => 300);
    $this->WP_Widget('hiercat', __('Hierarchical Categories', $tdom), $widget_ops, $control_ops);
  }

  /**
   * Helper function
   *
   */
  function hiercat_list_cats($args) {
    global $post;
    global $wp_query;

    if ( !isset($args['echo']) )
      $args['echo'] = 1;
    $sort_column = $args['sort_column'];

    $output = '';

    // Query categories.
    $cats = & get_categories($args);
    if ($cats['errors']) {
      print "<pre>"; print_r($cats); print "</pre>";
      return;
    }
    $cat_info = Array();

    if ( !empty ($cats) ) {
      $current_cat = $wp_query->get_queried_object_id();

      foreach ( $cats as $cat ) {
	$cat_info[$cat->term_id]['parent'] = $cat->category_parent;
	$cat_info[$cat->category_parent]['children'][] = $cat->term_id;
      }

      // add all children of the root node, but only to single depth.
      foreach ( $cat_info[0]['children'] as $child ) {
	$cat_info[$child]['show'] = 1;
      }
      
      // If currently displaying a category, taxonomy, or tag; AND
      // if it is the same as the one in this widget...
      if ((is_category() || is_tax() || is_tag()) && 
	  ($args['taxonomy'] == get_queried_object()->taxonomy ) ) {
	// show the current category's children, if any.
	if (is_array($cat_info[$current_cat]['children'] )) {
	   foreach ( $cat_info[$current_cat]['children'] as $child ) {
	      $cat_info[$child]['show'] = 1;
	   }
	}

	$cat_parent = $cat_info[$current_cat]['parent'];
	if ($cat_parent && ($args['show_siblings'] == 'yes')) {
	  // if showing siblings, add the current category's parent's other children.
	  foreach ( $cat_info[$cat_parent]['children'] as $child ) {
	    $cat_info[$child]['show'] = 1;
	  }

	  // Also show parent node's siblings.
	  $cat_grandparent = $cat_info[$cat_parent]['parent'];
	  if ($cat_grandparent) {
	    foreach ( $cat_info[$cat_grandparent]['children'] as $child ) {
		$cat_info[$child]['show'] = 1;
	    }
	  }
	}
	
	// add all ancestors of the current category.
	while ($cat_parent) {
	  $cat_info[$cat_parent]['show'] = 1;
	  $cat_parent = $cat_info[$cat_parent]['parent'];
	}
      }
      
      $my_includes = Array();
      // Add categories that were selected
      foreach ( $cats as $cat ) {
	if ($cat_info[$cat->term_id]['show']) {
	  $my_includes[] =$cat->term_id;
	}
      }
      
      if (!empty($my_includes)) {
        // List categories, if any. Blank title_li suppresses unwanted elements.
        $qargs = Array('title_li' => '', 'hide_empty' => 0, 'include' => $my_includes,
                      'order' => $args['order'], 'orderby' => $args['orderby'],
                      'show_count' => $args['show_count']);
        if (!empty($args['taxonomy'])) {
          $qargs['taxonomy'] = $args['taxonomy'];
        }
        $output .= wp_list_categories( $qargs );
      }
    }

    $output = apply_filters('wp_list_categories', $output);
    
    if ( $args['echo'] )
      echo $output;
    else
      return $output;
  }

  function known_params ($options = 0) {
    $option_menu = array('title' => array('title' => 'Title:'),
			 'show_siblings' => array('title' => 'Show siblings to the current category?',
						  'type' => 'checkbox'),
			 'include' => array('title' => 'Include:',
					    'desc' => 'Comma-delimited list of category IDs, or blank for all'),
			 'exclude' => array('title' => 'Exclude:'),
			 'orderby' => array('title' => 'Sort field:',
					    'desc' => 'Enter one of: <em>name, count, term_group, slug</em> or a custom value. Default: name'),
			 'order' => array('title' => 'Sort direction:',
					  'desc' => '(default: ASC)'),
			 'child_of' => array('title' => 'Only display Categories below this ID'),
			 'hide_empty' => array('title' => 'Hide empty categories?', 
						  'type' => 'checkbox'),
			 'show_count' => array('title' => 'Show count of category entries?', 
						  'type' => 'checkbox'),
			 'taxonomy' => array('title' => 'Custom taxonomy:'),
			 );
    if ($options) {
      $taxons = get_taxonomies();
      $option_menu['taxonomy']['desc'] = 'Enter one of: <em>' . 
	implode(', ',array_keys($taxons)) . '</em> or blank for post categories.';
    }
    return ($options ? $option_menu : array_keys($option_menu));
  }
  /**
   * Displays the Widget
   *
   */
  function widget($args, $instance){
    $known_params = $this->known_params(0);
    foreach ($known_params as $param) {
      if (strlen($instance[$param]))
	$cat_options[$param] = $instance[$param];
    }
    $cat_options['title'] = apply_filters('widget_title', $cat_options['title']);
    // WordPress defaults to hiding: thus, always specify.
    $cat_options['hide_empty'] = $cat_options['hide_empty'] == 'yes' ? 1 : 0;
      
    print $args['before_widget'];
    if ( strlen($cat_options['title']) )
      print "{$args['before_title']}{$cat_options['title']}{$args['after_title']}";
    print "<ul>";

    $this->hiercat_list_cats($cat_options);
    print "</ul>{$args['after_widget']}";
  }

  /**
   * Saves the widget's settings.
   *
   */
  function update($new_instance, $old_instance){
    $instance = $old_instance;

    $known_params = $this->known_params();
    foreach ($known_params as $param) {
      $instance[$param] = strip_tags(stripslashes($new_instance[$param]));
    }
    return $instance;
  }

  /**
   * Creates the edit form for the widget.
   *
   */
  function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('title'=>'') );

    $this->form_html($instance);
  }

}// END class

/**
 * Register Hierarchical Categories widget.
 *
 * Calls 'widgets_init' action after the widget has been registered.
 */
function HierCatInit() {
  register_widget('HierCatWidget');
}

/*
 * Initialize both widgets
 */

add_action('widgets_init', 'HierCatInit');
add_action('plugins_loaded', 'HierPageLoad');
add_action('widgets_init', 'HierPageInit');


/**
 * Plugin Name: Display Posts Shortcode
 * Plugin URI: http://www.billerickson.net/shortcode-to-display-posts/
 * Description: Display a listing of posts using the [display-posts] shortcode
 * Version: 2.4
 */
 
 
/**
 * To Customize, use the following filters:
 *
 * `display_posts_shortcode_args`
 * For customizing the $args passed to WP_Query
 *
 * `display_posts_shortcode_output`
 * For customizing the output of individual posts.
 * Example: https://gist.github.com/1175575#file_display_posts_shortcode_output.php
 *
 * `display_posts_shortcode_wrapper_open` 
 * display_posts_shortcode_wrapper_close`
 * For customizing the outer markup of the whole listing. By default it is a <ul> but
 * can be changed to <ol> or <div> using the 'wrapper' attribute, or by using this filter.
 * Example: https://gist.github.com/1270278
 */ 
 
// Create the shortcode
add_shortcode( 'display-posts', 'be_display_posts_shortcode' );
function be_display_posts_shortcode( $atts ) {

	// Original Attributes, for filters
	$original_atts = $atts;

	// Pull in shortcode attributes and set defaults
	$atts = shortcode_atts( array(
		'title'              => '',
		'author'              => '',
		'category'            => '',
		'date_format'         => '(n/j/Y)',
		'display_posts_off'   => false,
		'exclude_current'     => false,
		'id'                  => false,
		'ignore_sticky_posts' => false,
		'image_size'          => false,
		'include_title'       => true,
		'include_author'      => false,
		'include_content'     => false,
		'include_date'        => false,
		'include_excerpt'     => false,
		'meta_key'            => '',
		'meta_value'          => '',
		'no_posts_message'    => '',
		'offset'              => 0,
		'order'               => 'DESC',
		'orderby'             => 'date',
		'post_parent'         => false,
		'post_status'         => 'publish',
		'post_type'           => 'post',
		'posts_per_page'      => '10',
		'tag'                 => '',
		'tax_operator'        => 'IN',
		'tax_term'            => false,
		'taxonomy'            => false,
		'wrapper'             => 'ul',
		'wrapper_class'       => 'display-posts-listing',
		'wrapper_id'          => false,
	), $atts, 'display-posts' );
	
	// End early if shortcode should be turned off
	if( $atts['display_posts_off'] )
		return;

	$shortcode_title = sanitize_text_field( $atts['title'] );
	$author = sanitize_text_field( $atts['author'] );
	$category = sanitize_text_field( $atts['category'] );
	$date_format = sanitize_text_field( $atts['date_format'] );
	$exclude_current = be_display_posts_bool( $atts['exclude_current'] );
	$id = $atts['id']; // Sanitized later as an array of integers
	$ignore_sticky_posts = be_display_posts_bool( $atts['ignore_sticky_posts'] );
	$image_size = sanitize_key( $atts['image_size'] );
	$include_title = be_display_posts_bool( $atts['include_title'] );
	$include_author = be_display_posts_bool( $atts['include_author'] );
	$include_content = be_display_posts_bool( $atts['include_content'] );
	$include_date = be_display_posts_bool( $atts['include_date'] );
	$include_excerpt = be_display_posts_bool( $atts['include_excerpt'] );
	$meta_key = sanitize_text_field( $atts['meta_key'] );
	$meta_value = sanitize_text_field( $atts['meta_value'] );
	$no_posts_message = sanitize_text_field( $atts['no_posts_message'] );
	$offset = intval( $atts['offset'] );
	$order = sanitize_key( $atts['order'] );
	$orderby = sanitize_key( $atts['orderby'] );
	$post_parent = $atts['post_parent']; // Validated later, after check for 'current'
	$post_status = $atts['post_status']; // Validated later as one of a few values
	$post_type = sanitize_text_field( $atts['post_type'] );
	$posts_per_page = intval( $atts['posts_per_page'] );
	$tag = sanitize_text_field( $atts['tag'] );
	$tax_operator = $atts['tax_operator']; // Validated later as one of a few values
	$tax_term = sanitize_text_field( $atts['tax_term'] );
	$taxonomy = sanitize_key( $atts['taxonomy'] );
	$wrapper = sanitize_text_field( $atts['wrapper'] );
	$wrapper_class = sanitize_html_class( $atts['wrapper_class'] );
	if( !empty( $wrapper_class ) )
		$wrapper_class = ' class="' . $wrapper_class . '"';
	$wrapper_id = sanitize_html_class( $atts['wrapper_id'] );
	if( !empty( $wrapper_id ) )
		$wrapper_id = ' id="' . $wrapper_id . '"';

	
	// Set up initial query for post
	$args = array(
		'category_name'       => $category,
		'order'               => $order,
		'orderby'             => $orderby,
		'post_type'           => explode( ',', $post_type ),
		'posts_per_page'      => $posts_per_page,
		'tag'                 => $tag,
	);
	
	// Ignore Sticky Posts
	if( $ignore_sticky_posts )
		$args['ignore_sticky_posts'] = true;
	
	// Meta key (for ordering)
	if( !empty( $meta_key ) )
		$args['meta_key'] = $meta_key;
	
	// Meta value (for simple meta queries)
	if( !empty( $meta_value ) )
		$args['meta_value'] = $meta_value;
		
	// If Post IDs
	if( $id ) {
		$posts_in = array_map( 'intval', explode( ',', $id ) );
		$args['post__in'] = $posts_in;
	}
	
	// If Exclude Current
	if( $exclude_current )
		$args['post__not_in'] = array( get_the_ID() );
	
	// Post Author
	if( !empty( $author ) )
		$args['author_name'] = $author;
		
	// Offset
	if( !empty( $offset ) )
		$args['offset'] = $offset;
	
	// Post Status	
	$post_status = explode( ', ', $post_status );		
	$validated = array();
	$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
	foreach ( $post_status as $unvalidated )
		if ( in_array( $unvalidated, $available ) )
			$validated[] = $unvalidated;
	if( !empty( $validated ) )		
		$args['post_status'] = $validated;
	
	
	// If taxonomy attributes, create a taxonomy query
	if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {
	
		// Term string to array
		$tax_term = explode( ', ', $tax_term );
		
		// Validate operator
		if( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) )
			$tax_operator = 'IN';
					
		$tax_args = array(
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'slug',
					'terms'    => $tax_term,
					'operator' => $tax_operator
				)
			)
		);
		
		// Check for multiple taxonomy queries
		$count = 2;
		$more_tax_queries = false;
		while( 
			isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) && 
			isset( $original_atts['tax_' . $count . '_term'] ) && !empty( $original_atts['tax_' . $count . '_term'] ) 
		):
		
			// Sanitize values
			$more_tax_queries = true;
			$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
	 		$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
	 		$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts['tax_' . $count . '_operator'] : 'IN';
	 		$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
	 		
	 		$tax_args['tax_query'][] = array(
	 			'taxonomy' => $taxonomy,
	 			'field' => 'slug',
	 			'terms' => $terms,
	 			'operator' => $tax_operator
	 		);
	
			$count++;
			
		endwhile;
		
		if( $more_tax_queries ):
			$tax_relation = 'AND';
			if( isset( $original_atts['tax_relation'] ) && in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) ) )
				$tax_relation = $original_atts['tax_relation'];
			$args['tax_query']['relation'] = $tax_relation;
		endif;
		
		$args = array_merge( $args, $tax_args );
	}
	
	// If post parent attribute, set up parent
	if( $post_parent ) {
		if( 'current' == $post_parent ) {
			global $post;
			$post_parent = get_the_ID();
		}
		$args['post_parent'] = intval( $post_parent );
	}
	
	// Set up html elements used to wrap the posts. 
	// Default is ul/li, but can also be ol/li and div/div
	$wrapper_options = array( 'ul', 'ol', 'div' );
	if( ! in_array( $wrapper, $wrapper_options ) )
		$wrapper = 'ul';
	$inner_wrapper = 'div' == $wrapper ? 'div' : 'li';

	
	$listing = new WP_Query( apply_filters( 'display_posts_shortcode_args', $args, $original_atts ) );
	if ( ! $listing->have_posts() )
		return apply_filters( 'display_posts_shortcode_no_results', wpautop( $no_posts_message ) );
		
	$inner = '';
	while ( $listing->have_posts() ): $listing->the_post(); global $post;
		
		$image = $date = $author = $excerpt = $content = '';
		
		if ( $include_title )
			$title = '<a class="title" href="' . apply_filters( 'the_permalink', get_permalink() ) . '">' . get_the_title() . '</a>';
		
		if ( $image_size && has_post_thumbnail() )  
			$image = '<a class="image" href="' . get_permalink() . '">' . get_the_post_thumbnail( get_the_ID(), $image_size ) . '</a> ';
			
		if ( $include_date ) 
			$date = ' <span class="date">' . get_the_date( $date_format ) . '</span>';
			
		if( $include_author )
			$author = apply_filters( 'display_posts_shortcode_author', ' <span class="author">by ' . get_the_author() . '</span>' );
		
		if ( $include_excerpt ) 
			$excerpt = ' <span class="excerpt-dash">-</span> <span class="excerpt">' . get_the_excerpt() . '</span>';
			
		if( $include_content ) {
			add_filter( 'shortcode_atts_display-posts', 'be_display_posts_off', 10, 3 );
			$content = '<div class="content">' . apply_filters( 'the_content', get_the_content() ) . '</div>'; 
			remove_filter( 'shortcode_atts_display-posts', 'be_display_posts_off', 10, 3 );
		}
		
		$class = array( 'listing-item' );
		$class = sanitize_html_class( apply_filters( 'display_posts_shortcode_post_class', $class, $post, $listing, $original_atts ) );
		$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $image . $title . $date . $author . $excerpt . $content . '</' . $inner_wrapper . '>';
		
		// If post is set to private, only show to logged in users
		if( 'private' == get_post_status( get_the_ID() ) && !current_user_can( 'read_private_posts' ) )
			$output = '';
		
		$inner .= apply_filters( 'display_posts_shortcode_output', $output, $original_atts, $image, $title, $date, $excerpt, $inner_wrapper, $content, $class );
		
	endwhile; wp_reset_postdata();
	
	$open = apply_filters( 'display_posts_shortcode_wrapper_open', '<' . $wrapper . $wrapper_class . $wrapper_id . '>', $original_atts );
	$close = apply_filters( 'display_posts_shortcode_wrapper_close', '</' . $wrapper . '>', $original_atts );
	
	$return = $open;

	if( $shortcode_title ) {

		$title_tag = apply_filters( 'display_posts_shortcode_title_tag', 'h2', $original_atts );

		$return .= '<' . $title_tag . ' class="display-posts-title">' . $shortcode_title . '</' . $title_tag . '>' . "\n";
	}

	$return .= $inner . $close;

	return $return;
}

/**
 * Turn off display posts shortcode 
 * If display full post content, any uses of [display-posts] are disabled
 *
 * @param array $out, returned shortcode values 
 * @param array $pairs, list of supported attributes and their defaults 
 * @param array $atts, original shortcode attributes 
 * @return array $out
 */
function be_display_posts_off( $out, $pairs, $atts ) {
	$out['display_posts_off'] = true;
	return $out;
}

/**
 * Convert string to boolean
 * because (bool) "false" == true
 *
 */
function be_display_posts_bool( $value ) {
	return !empty( $value ) && 'true' == $value ? true : false;
}


/**
Plugin Name: TW Recent Posts Widget
Plugin URI: http://vuckovic.biz/wordpress-plugins/tw-recent-posts-widget
Description: TW Recent Posts Widget is advanced version of the WordPress Recent Posts widget allowing increased customization to display recent posts from category you define.
Author: Igor Vuckovic
Author URI: http://vuckovic.biz
Version: 1.0.3
*/

//	Set the wp-content and plugin urls/paths
if (! defined ( 'WP_CONTENT_URL' ))
	define ( 'WP_CONTENT_URL', get_option ( 'siteurl' ) . '/wp-content' );
if (! defined ( 'WP_CONTENT_DIR' ))
	define ( 'WP_CONTENT_DIR', ABSPATH . 'wp-content' );
if (! defined ( 'WP_PLUGIN_URL' ))
	define ( 'WP_PLUGIN_URL', WP_CONTENT_URL . '/plugins' );
if (! defined ( 'WP_PLUGIN_DIR' ))
	define ( 'WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins' );
	
class TW_Recent_Posts extends WP_Widget {
	
	//	@var string (The plugin version)		
	var $version = '1.0.3';
	//	@var string $localizationDomain (Domain used for localization)
	var $localizationDomain = 'tw-recent-posts';
	//	@var string $pluginurl (The url to this plugin)
	var $pluginurl = '';
	//	@var string $pluginpath (The path to this plugin)		
	var $pluginpath = '';
	
	//	PHP 4 Compatible Constructor
	function TW_Recent_Posts() {
		$this->__construct();
	}

	//	PHP 5 Constructor		
	function __construct() {
		$name = dirname ( plugin_basename ( __FILE__ ) );
		$this->pluginurl = WP_PLUGIN_URL . "/$name/";
		$this->pluginpath = WP_PLUGIN_DIR . "/$name/";
		add_action ( 'wp_print_styles', array (&$this, 'tw_recent_posts_css' ) );
		
		$widget_ops = array ('classname' => 'tw-recent-posts', 'description' => __ ( 'Show recent posts from selected category. Includes advanced options.', $this->localizationDomain ) );
		$this->WP_Widget ( 'tw-recent-posts', __ ( 'TW Recent Posts ', $this->localizationDomain ), $widget_ops );
	}
	
	private function truncate_post($amount, $echo = true, $allowed = '') {
		global $post;
		$postExcerpt = '';
		$postExcerpt = $post->post_excerpt;
		
		if ($postExcerpt != '') {
			if (strlen ( $postExcerpt ) <= $amount)
				$echo_out = '';
			else
				$echo_out = '...';
			
			$postExcerpt = strip_tags ( $postExcerpt, $allowed );
			if ($echo_out == '...')
				$postExcerpt = substr ( $postExcerpt, 0, strrpos ( substr ( $postExcerpt, 0, $amount ), ' ' ) );
			else
				$postExcerpt = substr ( $postExcerpt, 0, $amount );
			
			if ($echo)
				echo $postExcerpt . $echo_out;
			else
				return ($postExcerpt . $echo_out);
		} else {
			$truncate = $post->post_content;
			
			$truncate = preg_replace ( '@\[caption[^\]]*?\].*?\[\/caption]@si', '', $truncate );
			
			if (strlen ( $truncate ) <= $amount)
				$echo_out = '';
			else
				$echo_out = '...';
			
			$truncate = apply_filters ( 'the_content', $truncate );
			$truncate = preg_replace ( '@<script[^>]*?>.*?</script>@si', '', $truncate );
			$truncate = preg_replace ( '@<style[^>]*?>.*?</style>@si', '', $truncate );
			
			$truncate = strip_tags ( $truncate, $allowed );
			
			if ($echo_out == '...')
				$truncate = substr ( $truncate, 0, strrpos ( substr ( $truncate, 0, $amount ), ' ' ) );
			else
				$truncate = substr ( $truncate, 0, $amount );
			
			if ($echo)
				echo $truncate . $echo_out;
			else
				return ($truncate . $echo_out);
		}
	}
	
	function widget($args, $instance) {
		extract ( $args );
		$title = apply_filters ( 'title', isset ( $instance ['title'] ) ? esc_attr ( $instance ['title'] ) : '' );
		$category = apply_filters ( 'category', isset ( $instance ['category'] ) ? esc_attr ( $instance ['category'] ) : '' );
		$moretext = apply_filters ( 'moretext', isset ( $instance ['moretext'] ) ? esc_attr ( $instance ['moretext'] ) : '' );
		$count = apply_filters ( 'count', isset ( $instance ['count'] ) && is_numeric ( $instance ['count'] ) ? esc_attr ( $instance ['count'] ) : '' );
		$orderby = apply_filters ( 'orderby', isset ( $instance ['orderby'] ) ? $instance ['orderby'] : '' );
		$order = apply_filters ( 'order', isset ( $instance ['order'] ) ? $instance ['order'] : '' );
		$width = apply_filters ( 'width', isset ( $instance ['width'] ) && is_numeric ( $instance ['width'] ) ? $instance ['width'] : '60' );
		$height = apply_filters ( 'height', isset ( $instance ['height'] ) && is_numeric ( $instance ['height'] ) ? $instance ['height'] : '60' );
		$length = apply_filters ( 'length', isset ( $instance ['length'] ) && is_numeric ( $instance ['length'] ) ? $instance ['length'] : '100' );
		$show_post_title = apply_filters ( 'show_post_title', isset ( $instance ['show_post_title'] ) ? ( bool ) $instance ['show_post_title'] : false );
		$show_post_time = apply_filters ( 'show_post_time', isset ( $instance ['show_post_time'] ) ? ( bool ) $instance ['show_post_time'] : false );
		$show_post_thumb = apply_filters ( 'show_post_thumb', isset ( $instance ['show_post_thumb'] ) ? ( bool ) $instance ['show_post_thumb'] : ( bool ) false );
		$show_post_excerpt = apply_filters ( 'show_post_excerpt', isset ( $instance ['show_post_excerpt'] ) ? ( bool ) $instance ['show_post_excerpt'] : false );
		
		echo $before_widget;
		if (! empty ( $title ))
			echo $before_title . $title . $after_title;
?>

<div class="featured-posts textwidget">
<?php
$wp_query = new WP_Query( array('cat' => $category, 'posts_per_page' => $count, 'orderby' => $orderby, 'order' => $order, 'nopagging' => true));
while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
	<div class="featured-post">
	
	<?php if ($show_post_title) { ?>
		<h4><a href="<?php the_permalink() ?>" rel="bookmark"
	title="<?php the_title_attribute() ?>"><?php the_title() ?></a></h4>
	<?php } ?>

	<?php if ($show_post_time) { ?>
		<div class="post-time">
			<?php the_time ( get_option ( 'date_format' ) ); ?>
		</div>
	<?php } ?>
	
	<?php if ($show_post_thumb && has_post_thumbnail()) { ?>
		<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array($width,$height), array('title' => '', 'class' => 'alignleft')); ?></a>
	<?php } ?>
	
	<?php if ($show_post_excerpt) { ?>
		<div class="excerpt">
			<?php echo $this->truncate_post($length) . ($moretext != '') ? ' <a href="' . get_permalink () . '" class="read-more">' . $moretext . '</a>' : ''; ?>
		</div>
	<?php } ?>
	
		<div class="clear"></div>
	</div>
<?php
endwhile;
wp_reset_query();
wp_reset_postdata();
		?>
</div>
<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance) {
		return $new_instance;
	}
	
	function form($instance) {
		$title = isset ( $instance ['title'] ) ? esc_attr ( $instance ['title'] ) : '';
		$category = isset ( $instance ['category'] ) ? esc_attr ( $instance ['category'] ) : '';
		$moretext = isset ( $instance ['moretext'] ) ? esc_attr ( $instance ['moretext'] ) : 'more&raquo;';
		$count = isset ( $instance ['count'] ) && is_numeric ( $instance ['count'] ) ? esc_attr ( $instance ['count'] ) : '4';
		$orderby = isset ( $instance ['orderby'] ) ? $instance ['orderby'] : '';
		$order = isset ( $instance ['order'] ) ? $instance ['order'] : '';
		$width = isset ( $instance ['width'] ) && is_numeric ( $instance ['width'] ) ? $instance ['width'] : '60';
		$height = isset ( $instance ['height'] ) && is_numeric ( $instance ['height'] ) ? $instance ['height'] : '60';
		$length = isset ( $instance ['length'] ) && is_numeric ( $instance ['length'] ) ? $instance ['length'] : '100';
		$show_post_title = isset ( $instance ['show_post_title'] ) ? ( bool ) $instance ['show_post_title'] : false;
		$show_post_time = isset ( $instance ['show_post_time'] ) ? ( bool ) $instance ['show_post_time'] : false;
		$show_post_thumb = isset ( $instance ['show_post_thumb'] ) ? ( bool ) $instance ['show_post_thumb'] : false;
		$show_post_excerpt = isset ( $instance ['show_post_excerpt'] ) ? ( bool ) $instance ['show_post_excerpt'] : false;
?>

<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', $this->localizationDomain); ?> <input
	class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
	name="<?php echo $this->get_field_name('title'); ?>" type="text"
	value="<?php echo $title; ?>" /></label></p>

<p><label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category:', $this->localizationDomain); ?></label><select
	id="<?php echo $this->get_field_id('category'); ?>"
	name="<?php echo $this->get_field_name('category'); ?>">
	<?php 
	echo '<option value="0" ' .( '0' == $category ? 'selected="selected"' : '' ). '>'. __('All categories', $this->localizationDomain).'</option>';
	$cats = get_categories(array('hide_empty' => 0, 'name' => 'category', 'hierarchical' => true));
	foreach ($cats as $cat) {
		echo '<option value="' . $cat->term_id . '" ' .( $cat->term_id == $category ? 'selected="selected"' : '' ). '>' . $cat->name . '</option>';
	} ?>
	</select></p>

<p><label for="<?php echo $this->get_field_id('orderby'); ?>"><?php _e('Order by:', $this->localizationDomain); ?></label><select
	id="<?php echo $this->get_field_id('orderby'); ?>"
	name="<?php echo $this->get_field_name('orderby'); ?>">
	<option value="date"
		<?php echo 'date' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Date', $this->localizationDomain); ?></option>
	<option value="ID"
		<?php echo 'ID' == $orderby ? 'selected="selected"' : '' ?>><?php _e('ID', $this->localizationDomain); ?></option>
	<option value="title"
		<?php echo 'title' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Title', $this->localizationDomain); ?></option>
	<option value="author"
		<?php echo 'author' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Author', $this->localizationDomain); ?></option>
	<option value="comment_count"
		<?php echo 'comment_count' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Comment count', $this->localizationDomain); ?></option>
	<option value="rand"
		<?php echo 'rand' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Random', $this->localizationDomain); ?></option>
</select></p>

<p><label for="<?php echo $this->get_field_id('order'); ?>"><?php _e('Order:', $this->localizationDomain); ?></label><select
	id="<?php echo $this->get_field_id('order'); ?>"
	name="<?php echo $this->get_field_name('order'); ?>">
	<option value="DESC"
		<?php echo 'DESC' == $order ? 'selected="selected"' : '' ?>><?php _e('DESC:', $this->localizationDomain); ?></option>
	<option value="ASC"
		<?php echo 'ASC' == $order ? 'selected="selected"' : '' ?>><?php _e('ASC:', $this->localizationDomain); ?></option>
</select></p>

<p><label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Number of posts to show:', $this->localizationDomain); ?> <input
	id="<?php echo $this->get_field_id('count'); ?>"
	name="<?php echo $this->get_field_name('count'); ?>" type="text"
	size="3" value="<?php echo $count; ?>" /></label></p>

<p><input id="<?php echo $this->get_field_id('show_post_title'); ?>"
	name="<?php echo $this->get_field_name('show_post_title'); ?>"
	type="checkbox" <?php checked($show_post_title); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_title'); ?>"><?php _e('Show post title', $this->localizationDomain); ?></label>
</p>

<p><input id="<?php echo $this->get_field_id('show_post_time'); ?>"
	name="<?php echo $this->get_field_name('show_post_time'); ?>"
	type="checkbox" <?php checked($show_post_time); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_time'); ?>"><?php _e('Show post time', $this->localizationDomain); ?></label>
</p>

<p><input id="<?php echo $this->get_field_id('show_post_thumb'); ?>"
	name="<?php echo $this->get_field_name('show_post_thumb'); ?>"
	type="checkbox" <?php checked($show_post_thumb); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_thumb'); ?>"><?php _e('Show post thumb', $this->localizationDomain); ?></label><br />
<small><?php _e('Thumbnail size (W-H):', $this->localizationDomain); ?></small>
<input type="text" size="3"
	name="<?php echo $this->get_field_name('width'); ?>"
	value="<?php echo $width; ?>" />px <input type="text" size="3"
	name="<?php echo $this->get_field_name('height'); ?>"
	value="<?php echo $height; ?>" />px</p>

<p><input id="<?php echo $this->get_field_id('show_post_excerpt'); ?>"
	name="<?php echo $this->get_field_name('show_post_excerpt'); ?>"
	type="checkbox" <?php checked($show_post_excerpt); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_excerpt'); ?>"><?php _e('Show post excerpt', $this->localizationDomain); ?></label><br />
<small><?php _e('Post excerpt length (characters)', $this->localizationDomain); ?></small>
<input id="<?php echo $this->get_field_id('length'); ?>"
	name="<?php echo $this->get_field_name('length'); ?>" type="text"
	size="3" value="<?php echo $length; ?>" /><br />
<small><?php _e('Read more text', $this->localizationDomain); ?></small>
<input name="<?php echo $this->get_field_name('moretext'); ?>"
	type="text" size="12" value="<?php echo $moretext; ?>" /></p>

<?php 
    }
	
} // end class TW_Recent_Posts

add_action('widgets_init', create_function('', 'return register_widget("TW_Recent_Posts");'));


/*
Plugin Name: Search Excerpt
Plugin URI: http://fucoder.com/code/search-excerpt/
Description: Modify <code>the_exceprt()</code> template code during search to return snippets containing the search phrase. Snippet extraction code stolen from <a href="http://drupal.org/">Drupal</a>'s search module. And patched by <a href="http://pobox.com/~jam/">Jam</a> to support Asian text.
Version: 1.2 $Rev$
Author: Scott Yang
Author URI: http://scott.yang.id.au/

*/

class SearchExcerpt {
   function get_content() {
       // Get the content of current post. We like to have the entire
       // content. If we call get_the_content() we'll only get the teaser +
       // page 1.
       global $post;
       
       // Password checking copied from
       // template-functions-post.php/get_the_content()
       // Search shouldn't match a passworded entry anyway.
       if (!empty($post->post_password) ) { // if there's a password
           if (stripslashes($_COOKIE['wp-postpass_'.COOKIEHASH]) !=
               $post->post_password )
           {      // and it doesn't match the cookie
               return get_the_password_form();
           }
       }

       return $post->post_content;
   }
   
   function get_query($text) {
       static $last = null;
       static $lastsplit = null;

       if ($last == $text)
           return $lastsplit;

       // The dot, underscore and dash are simply removed. This allows
       // meaningful search behaviour with acronyms and URLs.
       $text = preg_replace('/[._-]+/', '', $text);

       // Process words
       $words = explode(' ', $text);

       // Save last keyword result
       $last = $text;
       $lastsplit = $words;

       return $words;
   }

   function highlight_excerpt($keys, $text) {
       $text = strip_tags($text);

       for ($i = 0; $i < sizeof($keys); $i ++)
           $keys[$i] = preg_quote($keys[$i], '/');

       $workkeys = $keys;

       // Extract a fragment per keyword for at most 4 keywords.  First we
       // collect ranges of text around each keyword, starting/ending at
       // spaces.  If the sum of all fragments is too short, we look for
       // second occurrences.
       $ranges = array();
       $included = array();
       $length = 0;
       while ($length < 256 && count($workkeys)) {
           foreach ($workkeys as $k => $key) {
               if (strlen($key) == 0) {
                   unset($workkeys[$k]);
                   continue;
               }
               if ($length >= 256) {
                   break;
               }
               // Remember occurrence of key so we can skip over it if more
               // occurrences are desired.
               if (!isset($included[$key])) {
                   $included[$key] = 0;
               }

               // NOTE: extra parameter for preg_match requires PHP 4.3.3
               if (preg_match('/'.$key.'/iu', $text, $match,
                              PREG_OFFSET_CAPTURE, $included[$key]))
               {
                   $p = $match[0][1];
                   $success = 0;
                   if (($q = strpos($text, ' ', max(0, $p - 60))) !== false &&
                        $q < $p)
                   {
                       $end = substr($text, $p, 80);
                       if (($s = strrpos($end, ' ')) !== false && $s > 0) {
                           $ranges[$q] = $p + $s;
                           $length += $p + $s - $q;
                           $included[$key] = $p + 1;
                           $success = 1;
                       }
                   }

                   if (!$success) {
                       // for the case of asian text without whitespace
                       $q = _jamul_find_1stbyte($text, max(0, $p - 60));
                       $q = _jamul_find_delimiter($text, $q);
                       $s = _jamul_find_1stbyte_reverse($text, $p + 80, $p);
                       $s = _jamul_find_delimiter($text, $s);
                       if (($s >= $p) && ($q <= $p)) {
                           $ranges[$q] = $s;
                           $length += $s - $q;
                           $included[$key] = $p + 1;
                       } else {
                           unset($workkeys[$k]);
                       }
                   }
               } else {
                   unset($workkeys[$k]);
               }
           }
       }

       // If we didn't find anything, return the beginning.
       if (sizeof($ranges) == 0)
           return '<p>' . _jamul_truncate($text, 256) . '&nbsp;...</p>';

       // Sort the text ranges by starting position.
       ksort($ranges);

       // Now we collapse overlapping text ranges into one. The sorting makes
       // it O(n).
       $newranges = array();
       foreach ($ranges as $from2 => $to2) {
           if (!isset($from1)) {
               $from1 = $from2;
               $to1 = $to2;
               continue;
           }
           if ($from2 <= $to1) {
               $to1 = max($to1, $to2);
           } else {
               $newranges[$from1] = $to1;
               $from1 = $from2;
               $to1 = $to2;
           }
       }
       $newranges[$from1] = $to1;

       // Fetch text
       $out = array();
       foreach ($newranges as $from => $to)
           $out[] = substr($text, $from, $to - $from);

       $text = (isset($newranges[0]) ? '' : '...&nbsp;').
           implode('&nbsp;...&nbsp;', $out).'&nbsp;...';
       $text = preg_replace('/('.implode('|', $keys) .')/iu',
                            '<strong class="search-excerpt">\0</strong>',
                            $text);
       return "<p>$text</p>";
   }

   function the_excerpt($text) {
       static $filter_deactivated = false;
       global $more;
       global $wp_query;

       // If we are not in a search - simply return the text unmodified.
       if (!is_search())
           return $text;

       // Deactivating some of the excerpt text.
       if (!$filter_deactivated) {
           remove_filter('the_excerpt', 'wpautop');
           $filter_deactivated = true;
       }

       // Get the whole document, not just the teaser.
       $more = 1;
       $query = SearchExcerpt::get_query($wp_query->query_vars['s']);
       $content = SearchExcerpt::get_content();

       return SearchExcerpt::highlight_excerpt($query, $content);
   }
}

// The number of bytes used when WordPress looking around to find delimiters
// (either a whitespace or a point where ASCII and other character switched).
// This also represents the number of bytes of few characters.
define('_JAMUL_LEN_SEARCH', 15);

function _jamul_find_1stbyte($string, $pos=0, $stop=-1) {
   $len = strlen($string);
   if ($stop < 0 || $stop > $len) {
       $stop = $len;
   }
   for (; $pos < $stop; $pos++) {
       if ((ord($string[$pos]) < 0x80) || (ord($string[$pos]) >= 0xC0)) {
           break;      // find 1st byte of multi-byte characters.
       }
   }
   return $pos;
}

function _jamul_find_1stbyte_reverse($string, $pos=-1, $stop=0) {
   $len = strlen($string);
   if ($pos < 0 || $pos >= $len) {
       $pos = $len - 1;
   }
   for (; $pos >= $stop; $pos--) {
       if ((ord($string[$pos]) < 0x80) || (ord($string[$pos]) >= 0xC0)) {
           break;      // find 1st byte of multi-byte characters.
       }
   }
   return $pos;
}

function _jamul_find_delimiter($string, $pos=0, $min = -1, $max=-1) {
   $len = strlen($string);
   if ($pos == 0 || $pos < 0 || $pos >= $len) {
       return $pos;
   }
   if ($min < 0) {
       $min = max(0, $pos - _JAMUL_LEN_SEARCH);
   }
   if ($max < 0 || $max >= $len) {
       $max = min($len - 1, $pos + _JAMUL_LEN_SEARCH);
   }
   if (ord($string[$pos]) < 0x80) {
       // Found ASCII character at the trimming point.  So, trying
       // to find new trimming point around $pos.  New trimming point
       // should be on a whitespace or the transition from ASCII to
       // other character.
       $pos3 = -1;
       for ($pos2 = $pos; $pos2 <= $max; $pos2++) {
           if ($string[$pos2] == ' ') {
               break;
           } else if ($pos3 < 0 && ord($string[$pos2]) >= 0x80) {
               $pos3 = $pos2;
           }
       }
       if ($pos2 > $max && $pos3 >= 0) {
           $pos2 = $pos3;
       }
       if ($pos2 > $max) {
           $pos3 = -1;
           for ($pos2 = $pos; $pos2 >= $min; $pos2--) {
               if ($string[$pos2] == ' ') {
                   break;
               } else if ($pos3 < 0 && ord($string[$pos2]) >= 0x80) {
                   $pos3 = $pos2 + 1;
               }
           }
           if ($pos2 < $min && $pos3 >= 0) {
               $pos2 = $pos3;
           }
       }
       if ($pos2 <= $max && $pos2 >= $min) {
           $pos = $pos2;
       }
   } else if ((ord($string[$pos]) >= 0x80) || (ord($string[$pos]) < 0xC0)) {
       $pos = _jamul_find_1stbyte($string, $pos, $max);
   }
   return $pos;
}

function _jamul_truncate($string, $byte) {
   $len = strlen($string);
   if ($len <= $byte)
       return $string;
   $byte = _jamul_find_1stbyte_reverse($string, $byte);
   return substr($string, 0, $byte);
}

// Add with priority=5 to ensure that it gets executed before wp_trim_excerpt
// in default filters.
add_filter('get_the_excerpt', array('SearchExcerpt', 'the_excerpt'), 5);


// Add Your Menu Locations
function register_my_menus() {
  register_nav_menus(
    array(  
    	'primary' => __( 'Primary Navigation' )
	)
  );
} 
add_action( 'init', 'register_my_menus' );


// Enable Shortcodes in Text Widgets
add_filter('widget_text', 'do_shortcode');

// Shortcode: Render Custom Field
function customfields_shortcode($atts, $text) {
	global $post;
	return get_post_meta($post->ID, $text, true);
}
@add_shortcode('cf','customfields_shortcode');

 ?>