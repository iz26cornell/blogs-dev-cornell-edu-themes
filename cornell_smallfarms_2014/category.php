<?php get_header(); ?>

<div id="content">

	<div id="main">
    
    	<div id="main-top"></div>
        
		<?php if ( is_active_sidebar( 'sidebar-1' ) ) { ?>
            <div id="secondary-nav">
                <div class="main-body">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-body">

			<?php if ( have_posts() ) : ?>
                <header class="archive-header">
                    <!--<h1 class="archive-title"><?php //printf( __( 'Category Archives: %s', 'smallfarms' ), single_cat_title( '', false ) ); ?></h1>-->
    
                    <?php if ( category_description() ) : // Show an optional category description ?>
                    <div class="archive-meta"><?php echo category_description(); ?></div>
                    <?php endif; ?>
                </header><!-- .archive-header -->
    
                <?php /* The loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>
                	<h2>
                        <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                    </h2>
                    <?php the_excerpt(); ?>
                <?php endwhile; ?>
    
                <?php echo('<p class="post-nav">'); posts_nav_link(' ', __('<span class="post-nav-back">&laquo; Previous</span>'), __('<span class="post-nav-next">More &raquo;</span>')); echo('</p>'); ?>
    
            <?php else : ?>
                <?php the_content(); ?>
            <?php endif; ?>
            
		</div><!-- end #main-body -->
        
		<?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
            <div id="secondary">
                <div class="main-body">
                    <?php get_sidebar('secondary'); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-bottom"></div>
        
	</div><!-- #main -->
    
</div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- end #wrap -->





<?php get_footer(); ?>