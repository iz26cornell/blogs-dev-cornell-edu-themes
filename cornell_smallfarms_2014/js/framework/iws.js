/* IWS Dynamic Components
   ------------------------------------------- */  

if (js_path == undefined) {
	var js_path = "js/framework/";
}

var iws_include = ["iws_popup.js",
						 "iws_tooltips.js",
						 "iws_expander.js"];

/* load resources */
function iws_load() {
	$.ajaxSetup({async: false});
	for (i=0;i<iws_include.length;i++) {
		$.getScript(js_path+iws_include[i]);
	}
}


/* initialize components */
function iws_init() {
	iws_load();
	
	// Headline Autoscale Variables
	var base_size;
	var base_width;
	var max_size = 72;
	var min_window_size = 0;
	
	// Windows class
	if (navigator.appVersion.indexOf('Win')!=-1) {
		$('html').addClass('win');
		if (navigator.appName.indexOf('Internet Explorer') || !!navigator.userAgent.match(/Trident\/7\./) ) {
			$('html').addClass('ie'); // includes ie11+
		}
	}
	
	// Window Size Tracking
	function resizeChecks() {
		
		// Restore Standard Main Navigation
		if ($(window).width() > 959) {
			$('#navigation ul').removeAttr('style');
			$('#mobile-nav em').removeClass('open');
			$('#cu-search').removeAttr('style');
			$('#cu-brand').removeClass('visible');
			$('.secondary #secondary-nav .main-body .widget_hier_page ul').removeClass('open');
			$('.secondary #secondary-nav .main-body .widget_hier_page .widget-title').removeClass('open');
			$('.secondary #secondary-nav .main-body').removeClass('open'); 
			$('body').removeClass('section-nav-open'); 
		}
		
		// editorial table
		if( ($(window).width() < 680) && ($('body').hasClass('editorial')) ) { 
			var tr = $('<tr />');
			tr.append($('td+td+td'));
		}		
				
		// Refresh Headline Autoscale 
		if ($(window).width() > min_window_size) {
			$('.autosize-header .home #identity-content h1').addClass('autoscale');
			var multiplier = $('#identity-content').width() / base_width;
			if (multiplier > 0) {
				var new_size = base_size * multiplier;
				if (new_size > max_size) {
					new_size = max_size;
				}
				$('.autosize-header .home #identity-content h1').css('font-size',new_size+'px');
			}
		}
		else {
			$('.autosize-header .home #identity-content h1').removeAttr('style');
			$('.autosize-header .home #identity-content h1').removeClass('autoscale');
		}
	}
	
	$(window).load(function() {
		
		// Reinitialize Headline Autoscale (after remote webfonts load)
		$('.autosize-header .home #identity-content h1').removeAttr('style');
		base_width = $('.home #identity-content h1 span').width();
		resizeChecks();
		
		// Single-line News Headlines (move date to line two)
		$('.home #news h3').each(function( index ) {
  			if ($(this).next('h4.date').position().top - $(this).position().top < 8) {
  				$(this).find('a').append('<br />');
  			}
		});
		
		$(window).resize(resizeChecks);
		
	});
	
	$(document).ready(function() {
		popups();
		tooltips(100);
		expander();
		
		$('#content').fitVids();
				
		$(window).resize(resizeChecks);
		
		// Homepage Headline Autoscale
		base_size = parseInt($('.home #identity-content h1').css('font-size'));
		base_width = $('.home #identity-content h1 span').width();
		$(window).resize(resizeChecks);
		resizeChecks();	


		/*
		if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) { //target safari browser
			$('#header #navigation ul.nav-menu li:first-child a').css( 'padding','0 26.5px' );
		}
		*/

		// Mobile Navigation
		var nav_offset = $('#mobile-nav em').offset();
		
		$('#navigation').find('ul.nav-menu').first().addClass('mobile-menu'); // standard
		//$('#mobile-nav').next('div').find('ul.menu').first().addClass('mobile-menu'); // drupal
				
		$('#mobile-nav em').addClass('mobile-menu');
		$('#mobile-nav').click(function() {
			$(this).toggleClass('open');
			
			$('#header #navigation .nav-menu').slideToggle(200); // standard
			//$(this).parent().next('div').find('ul.menu').slideToggle(200); //drupal
		});

		// Justified Navigation
		var nav_count = $('.nav-centered #navigation ul.menu').first().find('li').length;
		if (nav_count > 0) {
			var nav_width = 100 / nav_count;
			$('.nav-centered #navigation ul.menu').first().find('li').css( 'width',nav_width+'%');
		}
		
		/* Detect Increased User Font Size
		var user_font_size = window.getComputedStyle(document.getElementsByTagName('html')[0],null).getPropertyValue('font-size');
		if (parseInt(user_font_size) > 16) {
			$('body').addClass('user-font-scaled');
		}*/
		
		// Homepage Tabs
		$('.home #subfooter .tab').click(function(e){
			e.preventDefault();
			$('.home #subfooter .tab').removeClass('active');
			$(this).addClass('active');
			$('.tab-content').css('visibility','hidden');
			if ( $(this).hasClass('tab-about') ) {
				$('#about').css('visibility','visible');
			}
			else if ( $(this).hasClass('tab-contact') ) {
				$('#contact').css('visibility','visible');
			}
			else if ( $(this).hasClass('tab-videos') ) {
				$('#videos').css('visibility','visible');
			}
		});
		
		// Search
		$('#search-button').click(function(e) {
			e.preventDefault();
			$(this).toggleClass('open');
			$('#cu-search').slideToggle(200);
		});
		
		// In this section mobile
		$('#secondary-nav .main-body #section-nav-icon').click(function(e) {
			e.preventDefault();			
			$('.secondary #secondary-nav .main-body .widget_hier_page ul').toggleClass('open');
			$('.secondary #secondary-nav .main-body .widget_hier_page .widget-title').toggleClass('open');
			$('.secondary #secondary-nav .main-body').toggleClass('open'); 
			$('body').toggleClass('section-nav-open'); 
		});
		
		// RWD Submenu
		if ( $('#section-navigation').has('li').length ) {
			$('#navigation li.active').append($('#section-navigation').clone());
		}
		
		// Mobile Slidedeck
		$('.slidedeck_frame').first().after( $('.slidedeck_frame').first().clone().removeAttr('class').removeAttr('style').attr('id','slidedeck-mobile') );
		$('#slidedeck-mobile dl, #slidedeck-mobile dd').removeAttr('style');
		
		// Mobile Homepage Posts
		$('.home #secondary').before( $('#home-posts').clone().attr('id','home-posts-mobile') );
		
		// Mobile Table Helper
		// example specific target: $('#main-body table.location_search')
		$('#main-body table').wrap('<div class="table-scroller" />');
		$('.table-scroller').append('<div class="table-fader" />').bind('scroll touchmove', function() {
			$(this).find('.table-fader').remove(); // hide fader DIV on user interaction
		});
		// unwrap specific table(s)
		$('#main-body table.calendar-table').unwrap();
		$('#main-body table.calendar-table table').unwrap();
		$('.editorial #main-body table').unwrap();
		$('.writers #main-body table').unwrap();
		$('.quarterly #main-body table').unwrap();
		$('.about #main-body table').unwrap();
		$('.archive-2 #main-body table').unwrap();
		
	});
}