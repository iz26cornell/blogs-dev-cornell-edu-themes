<?php
/**
 * Template Name: Homepage, with posts
 * @package WordPress
 * @subpackage Cornell
 */
get_header(); ?>

	<div id="content">

		<div id="main">
  
			<div id="main-top"></div>

            <?php if ( is_active_sidebar( 'sidebar-1' ) ) { ?>
                <div id="secondary-nav">
                    <div class="main-body color-box green">
                        <?php get_sidebar( 'sidebar-1' ); ?>
                    </div>
                </div>
            <?php } ?>
                            
			<div id="main-body">
            
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if ( is_front_page() ) { ?>

                    <?php } else { ?>
                        <h2 class="entry-title"><?php the_title(); ?></h2>
                    <?php } ?>
        
                    <div class="entry-content">
                        <?php the_content(); ?>
                        <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
                        <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
                    </div><!-- .entry-content -->
                </div><!-- #post-## -->
        
                <?php ?> <?php /*comments_template( '', true ); */?> <?php ?>
        
            <?php endwhile; ?>
                
			<?php /*query_posts('category_name=home-featured&showposts=3');*/ ?>
            <?php query_posts('cat=213&showposts=3'); ?>
            <?php //query_posts('cat=25&showposts=3'); ?>
            <?php /*<h2 class="latest-title">Latest News</h2> */?>
    
            <div id="home-posts">
            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    
                    <div class="entry-date"><span class="month"><?php the_time('M') ?></span><span class="day"><?php the_time('d') ?></span></div>
                    <?php if ( is_front_page() ) { ?>
                        <h3 class="entry-title"><?php the_title(); ?></h3>
                    <?php } else { ?>
                        <h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'Cornell' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                    <?php } ?>
                    
                    <div class="post-meta">
                    <?php if ( count( get_the_category() ) ) : ?>
                        Posted by <?php the_author_posts_link(); ?><span class="cat-links">
                            <?php printf( __( '<span class="%1$s">in</span> %2$s', 'Cornell' ), 'entry-info-prep entry-info-prep-cat-links', get_the_category_list( ', ' ) ); ?>
                        </span>
                    <?php endif; ?>
                    <span class="comments-link"><?php comments_popup_link( __( 'Comment', 'Cornell' ), __( '1 Comment', 'Cornell' ), __( '% Comments', 'Cornell' ) ); ?></span>
                    </div>
                    
                    
                    <div class="entry-content">
                        <?php the_excerpt(); ?>
                        <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
                        <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
                    </div><!-- .entry-content -->
                </div><!-- #post-## -->
    
                <?php /*?> <?php comments_template( '', true ); ?> <?php */?>
    
            <?php endwhile; ?>
            </div>

			</div>
              
            <?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
                <div id="secondary">
                    <div class="main-body">
                        <?php get_sidebar('secondary'); ?>
                    </div>
                </div>
            <?php } ?>
            
            <div id="main-bottom"></div>
            
        </div><!-- #main -->
        
    </div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- #wrap -->


<?php get_footer(); ?>
