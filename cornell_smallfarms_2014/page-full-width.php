<?php
/**
 * Template Name: Full Width (No Sidebar)
 * @package WordPress
 * @subpackage Cornell
 */

get_header(); ?>

	<div id="content">

		<div id="main">
  
			<div id="main-top"></div>

			<div id="main-body">
            
                
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                
                <?php if ( is_front_page() ) { ?>
                    
				<?php } else { ?>
                    <h2 class="entry-title"><?php the_title(); ?></h2>
                <?php } ?>
                    
                <?php the_content(__('Read more'));?>
                <?php edit_post_link( __( 'Edit', 'smallfarms' ), '<span class="edit-link">', '</span>' ); ?>
                <?php endwhile; else: ?>
                <p>
                  <?php _e('Sorry, no posts matched your criteria.'); ?>
                </p>
                <?php endif; ?>
                <?php echo('<p class="post-nav">'); posts_nav_link(' ', __('<span class="post-nav-back">&laquo; Previous</span>'), __('<span class="post-nav-next">More &raquo;</span>')); echo('</p>'); ?>
                
			</div>
              
            <div id="main-bottom"></div>
            
        </div><!-- #main -->
        
    </div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- #wrap -->


<?php get_footer(); ?>
