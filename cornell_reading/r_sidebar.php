<!-- begin r_sidebar -->

<div id="secondary">
  <div id="secondary-navigation">
  <!--<p class="admin-link"><a href="http://blogs.cornell.edu/wp-login.php">Blog Admin</a></p>-->
    <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
    <div id="pages">
    <ul>
      <?php wp_list_pages('title_li=&depth=1'); ?>
    </ul>
  </div>
    <?php endif; ?>
  </div>
  <!-- <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. -->
</div>
<!-- end r_sidebar -->
