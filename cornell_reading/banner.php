<!--
	The following link provides a way for people using text-based browsers and
	screen readers to skip over repetitive navigation elements so that they can 
	get directly to the content. It is hidden from general users through CSS.
-->
<div id="skipnav">
	<a href="#content">Skip to main content</a>
</div>

<hr />

<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity">
	<div id="cu-logo">
		<a id="insignia-link" href="http://www.cornell.edu/"><img src="images/NSRP_2line_4c.gif" alt="Cornell University" /></a>
		<div id="unit-signature-links">
			<a id="cornell-link" href="http://www.cornell.edu/">Cornell University</a>
            <a id="unit-link" href="<?php echo get_settings('home'); ?>/">New Student Reading Project</a>
		</div>
	</div>
	<div id="search-form">
		<form action="http://web.search.cornell.edu/search" method="get" enctype="application/x-www-form-urlencoded">
			<div id="search-input">
				<label for="search-form-query">SEARCH:</label>
				<input type="text" id="search-form-query" name="q" value="" size="50" />
				<input type="submit" id="search-form-submit" value="go" />
                <input type="hidden" name="output" value="xml_no_dtd" />
                <input type="hidden" name="client" value="default_frontend" />
                <input type="hidden" name="proxystylesheet" value="default_frontend" />
			</div>

			<div id="search-filters">
					<input type="radio" id="search-filters1" name="sitesearch" value="reading.cornell.edu" checked="checked" />
					<label for="search-filters1">New Student Reading Project</label>
				
					<input type="radio" id="search-filters2" name="sitesearch" value="" />
					<label for="search-filters2">Cornell</label>
					
					<a href="http://www.cornell.edu/search/">more options</a>
			</div>	
		</form>
	</div>

</div>

<hr />
