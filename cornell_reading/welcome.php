<?php
/*
Template Name: Welcome
*/
?>
<?php get_header(); ?>
<div id="pattern">
<div id="wrap" class="welcome">
<div id="maintag">
<p><?php bloginfo('description'); ?></p>
</div>
<div id="content">
  <div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h2>
      <?php the_title(); ?>
    </h2>
    <?php the_content(__('Read more'));?>
    <!--

	<?php trackback_rdf(); ?>

	-->
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
  </div>
  <?php include(TEMPLATEPATH."/r_sidebar.php");?>
</div>
<!-- The main column ends  -->
</div></div>
<?php get_footer(); ?>
