<!--
	The following link provides a way for people using text-based browsers and
	screen readers to skip over repetitive navigation elements so that they can 
	get directly to the content. It is hidden from general users through CSS.
-->
<div id="skipnav">
	<a href="#content">Skip to main content</a>
</div>

<hr />

<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity">
	<div id="cu-logo">
		<a id="insignia-link" href="http://www.cornell.edu/"><img src="images/NSRP_2line_4c.gif" alt="Cornell University" /></a>
		<div id="unit-signature-links">
			<a id="cornell-link" href="http://www.cornell.edu/">Cornell University</a>
			<a id="unit-link" href=/>New Student Reading Project</a>
		</div>
	</div>
	<div id="search-form">
		<form action="#" method="post" enctype="application/x-www-form-urlencoded">
			<div id="search-input">
				<label for="search-form-query">SEARCH:</label>
				<input type="text" id="search-form-query" name="query" value="" size="20" />
				<input type="submit" id="search-form-submit" name="submit" value="go" />
			</div>

			<div id="search-filters">
					<input type="radio" id="search-filters1" name="target" value="unit" checked="checked" />
					<label for="search-filters1">New Student Reading Project</label>
				
					<input type="radio" id="search-filters2" name="target" value="cornell" />
					<label for="search-filters2">Cornell</label>
					
					<a href="#">more options</a>
			</div>	
		</form>
	</div>

</div>

<hr />