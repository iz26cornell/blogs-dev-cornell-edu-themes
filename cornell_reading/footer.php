<!-- begin footer -->
<hr />
<div id="footer">
  <!-- The footer-content div contains the Cornell University copyright -->
  <div id="footer-content">
        <ul id="footer-main">
       <li class="first"><a href="http://www.library.cornell.edu">Cornell University Library</a></li>
       <li><a href="/reading/2011/05/20/tech-help/">Tech Help</a></li>
       <li><a href="<?php echo get_settings('home'); ?>/">Reading Project Home</a></li>
        </ul>
        <ul id="footer-secondary">
        <li class="first"><a href="mailto:webmaster_dpb@cornell.edu">Webmaster</a></li>
        <li>&copy;<?php echo date("Y");?> <a href="http://www.cornell.edu/">Cornell University</a></li>
        <li><a href="/reading/2011/05/20/credits/">Credits</a></li>
        <li><?php wp_loginout(); ?></li>
        <?php wp_register(); ?>
        </ul>
  </div>
</div>
<?php do_action('wp_footer'); ?>
</body></html>