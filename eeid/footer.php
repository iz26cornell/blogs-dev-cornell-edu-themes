<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package EEID
 * @since EEID 1.0
 */
?>

	</div><!-- #main .site-main -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<p class="footertext">&copy; 2012-2013 Ecology and Evolution of Infections and Disease at Cornell. Website build and designed by William M. Najar '13 (<a href="mailto:wmn9@cornell.edu">wmn9</a>)</p>
		</div><!-- .site-info -->
	</footer><!-- #colophon .site-footer -->
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="/wp-content/themes/eeid/static/eeid.js"></script>
</body>
</html>