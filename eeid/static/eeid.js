$("#slideshow > div:gt(0)").hide();

var captiontext = new Array();
captiontext[0] = "<span style=\"font-style:italic\">Aedes aegypti</span>, a mosquito that can spread the dengue fever, Chikungunya and yellow fever viruses, among others";
captiontext[1] = "A high magnification micrograph showing a fetal parvovirus infection in the placenta (H&E stain).";
captiontext[2] = "Electron microscope image of <span style=\"font-style:italic\">Corynebacterum diphteriae</span>, the pathogenic bacterium that causes diphtheria.";
var i = -1;

setInterval(function() { 

	i++;
	$('#slideshow > div:first').fadeOut(800).next().fadeIn(800).end().appendTo('#slideshow');
	if (i >= captiontext.length) {
        i = 0;
    }
	$('.captiontext').fadeOut(800, function() {
        $(this).html(captiontext[i]).fadeIn(900);
    });
	
},  5400);