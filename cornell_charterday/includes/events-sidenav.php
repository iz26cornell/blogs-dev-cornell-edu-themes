            <div class="col-sm-5 col-md-4 col-lg-3 events-menu">
                <div class="mobile-header visible-xs">
                    Events
                    <button type="button" class="navbar-collapse collapsed" data-toggle="collapse" data-target="#eventsnav">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="eventsnav">
                <ul>
                    <li>
                        <a href="<?php echo $nav_path; ?>/events">All Events</a>
                    </li>
                    <li>
                        <a class="drop" href="<?php echo $nav_path; ?>/events/regional">Regional Celebrations</a>
                    </li>
                    <li class="active-parent"><a class="drop" href="<?php echo $nav_path; ?>/events/charterday">Charter Day Weekend</a>
                    <?php dynamic_sidebar('section-navigation'); ?>
                    </li>
                    <li>
                        <a class="drop" target="_blank" href="//alumni.cornell.edu/globalcharterday">Global Charter Day</a>
                    </li>
                </ul>
                </div>
            </div>