function create_map(centerLAT,centerLONG,markers,DIVid,zoom){
  var map;
  var center = new google.maps.LatLng(centerLAT,centerLONG);

  var MY_MAPTYPE_ID = 'custom_style';

  function initialize() {

    var featureOpts = [
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          { "visibility": "simplified" },
          { "hue": "#ff0000" },
          { "color": "#ffffff" }
        ]
      },
      {
        "elementType": "labels.icon",
        "stylers": [
          { "visibility": "simplified" }
        ]
      },{
        "featureType": "poi",
        "stylers": [
          { "visibility": "simplified" }
        ]
      }
    ];

    var mapOptions = {
      zoom: zoom,
      center: center,
      disableDefaultUI: true,
      mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
      },
      mapTypeId: MY_MAPTYPE_ID,
      scrollwheel: false,
      draggable: false
    };

    map = new google.maps.Map(document.getElementById(DIVid),
        mapOptions);

    var styledMapOptions = {
      name: 'Cornell Style'
    };

    var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

    map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
    var markers = new Array();
    var infowindow = new google.maps.InfoWindow({
      maxWidth: 160
    });

    for(var i=0; i < locations.length; i++){
      var marker = new google.maps.Marker({ 
        position: new google.maps.LatLng(locations[i][1], locations[i][2]), 
        map: map, 
        title: locations[i][0],
        icon: locations[i][3] }
      );

      markers.push(marker);

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  }

  google.maps.event.addDomListener(window, 'load', initialize);
}