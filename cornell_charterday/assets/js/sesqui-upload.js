
function getreCAPTCHAval() {
    // Find reCAPTCHA response value (there are two fields with this name somehow, so get the one with a value).
    var recaptcha_response_field_val;
    var fieldValuePairs = $('#uploadForm').serializeArray();
    $.each(fieldValuePairs, function(index, fieldValuePair) {
        //console.debug("Item " + index + " is [" + fieldValuePair.name + "," + fieldValuePair.value + "]");
        if (fieldValuePair.name == "recaptcha_response_field") {
            if (fieldValuePair.value != "") {
                recaptcha_response_field_val = fieldValuePair.value;
                //console.debug(recaptcha_response_field_val);
            }
        }
    });

    return recaptcha_response_field_val;
}



function validateForm(uploadType) {
    // Ensure form fields have valid content.
	var valid = true;    
	var errorMsg = '';

    var minTitleChars = 5;
    if ($("#txtItemTitle").val().length < minTitleChars) {
        //alert("A title of at least " + minTitleChars + " characters is required.");
		errorMsg += "A title of at least " + minTitleChars + " characters is required.\n\n";
        valid = false;
    }

    var minDateChars = 8;
    if ($("#txtItemDate").val().length < minDateChars) {
        //alert("A date is required (format should be mm/dd/yyyy).");
        errorMsg += "A date is required (format should be mm/dd/yyyy).\n\n";
		valid = false;
    }

    if (uploadType == "file") {
        var minDescChars = 5;
        if ($("#txtItemDesc").val().length < minDescChars) {
            //alert("A description of at least " + minDescChars + " characters is required.");
            errorMsg += "A description of at least " + minDescChars + " characters is required.\n\n";
			valid = false;
        }
    }

    if (uploadType == "story") {
        var minStoryChars = 100;
        if ($("#txtItemStory").val().length < minStoryChars) {
            //alert("A story of at least " + minStoryChars + " characters is required.");
			errorMsg += "A story of at least " + minStoryChars + " characters is required.\n\n";
            valid = false;
        }
    }
	
    if (uploadType == "file") {
		if ($("#uploadifyQueue .uploadifive-queue-item").length == 0) {
			//alert("No files are in the upload queue box above: please add one.");
			errorMsg += "No files have been selected for upload.\n\n";
			valid = false;
		}
	}
	
	//everything else is valid, check recaptcha. otherwise skip check to recaptcha isn't used up
	if (valid == true) {
		//make sure recaptch isn't empty
		if (getreCAPTCHAval()) {
			// Validate the reCAPTCHA via Ajax.
			$.ajax({
					url: "checkReCAPTCHA.cfm",
					type: "POST",
					async: false,
					data: {recaptcha_challenge_field: $("#recaptcha_challenge_field").val(), recaptcha_response_field: getreCAPTCHAval()},
					complete: function(xhr, status) {
						//alert(xhr.responseText);
					},
					success: function(data) {
						//console.log((data));
						// If we have a fail message, short-circuit the upload process so the file selector does not clear.
						if ((data.indexOf('failed')) > -1) { 
							valid = false;
							$("#recaptcha_reload").trigger("click");
						// If we have a succeeded message then allow the file upload to happen.	
						} else if ((data.indexOf('succeded')) > -1) { 
							valid = true;
						}
					}
			});
		} else {
			alert("The reCAPTCHA verification service requires a response; note the small red button that allows alternative words to be used if the present ones are illegible.");
			valid = false;	
		}
	} else {
		//previous validation errors ccurred, display msg
		alert(errorMsg);	
	}
	
    return valid;
}

function fillForm() {
	 $.ajax({
			url: "getBacon.cfm", 
			dataType: "json",
			data: {context: "sesquiUpload"},
			success: function(data) {
				$("#txtItemTitle").val(data.Title);
				var tags = data.ShortDescription.substring(0, 20).replace(/\s+/g, ",");
				$("#txtItemTags").val(tags);
				$("#txtItemDesc").val(data.LongDescription);
				$("#txtItemStory").val(data.LongDescription);
				$("#txtItemDate").val("4/4/2014");
				$("#txtItemEmail").val("cuwebtech@gmail.com");
				$("#txtItemName").val("Ezra Cornell");
			}
	});
}

$(document).ready(function() {

	//init and settings for uploadifive upload
    var uploadURL = "https://sesquiwell.univcomm.cornell.edu/core/ctrl/ctrl_upload.cfc?method=upload";
    $("#file_upload").uploadifive({
            "fileObjName": "file_to_upload",
            "uploadScript": uploadURL,
            "auto": false,
            "queueID": "uploadifyQueue",
            "queueSizeLimit": 1,
		    "fileSizeLimit" : '100MB',
            "multi": false,
            "removeCompleted": true,
            "fileType": "image/*|audio/*|video/*|application/pdf",
			'onError'      : function(errorType) {
            	alert('The error was: ' + errorType);
				if (errorType == 'FILE_SIZE_LIMIT_EXCEEDED') {
					alert("Sorry, that file is too large. The maximum file size is 100 MB");
 					$('#file_upload').uploadifive('cancel', $('.uploadifive-queue-item').first().data('file'),'true');
					$('#file_upload').uploadifive('clearQueue');
				}
        	},
			'onUpload'     : function(filesToUpload) {
            	//alert(filesToUpload + ' files will be uploaded.');
				window.scrollTo(0,0);
				$("#innerForm").hide();
				$("#loading").show();
        	},
            "onUploadComplete": function(file, data) {
                //console.log("Upload complete.");
                //console.debug(file);
                console.debug(data);
                $("#recaptcha_reload").trigger("click");
				//reset form fields
				$("#txtItemName, #txtItemEmail, #txtItemDescription, #txtItemStory, #txtItemTags, #txtItemDate").val('');
				//redirect to thank you page here
				window.location.assign("/thankYou.cfm?type=file");
            }
    });
	

    // Uploadifive upload click bind.
    $("#uploadItemButton").on("click", function() {
         if (validateForm("file")) {
            // Send dynamic form data along with image.            
            // http://www.uploadify.com/forum/#/discussion/8206/dynamic-formdata

            $("#file_upload").data("uploadifive").settings.formData = {
                "title": $("#txtItemTitle").val(), 
                "tags": $("#txtItemTags").val(), 
                "date": $("#txtItemDate").val(),
                "description": $("#txtItemDesc").val(),
                "name": $("#txtItemName").val(),
                "email": $("#txtItemEmail").val(),
                //"recaptcha_response_field": getreCAPTCHAval(),
                //"recaptcha_challenge_field": $("#recaptcha_challenge_field").val(),
				"skipreCAPTCHA" : 1,
                "kup": 1
            };
			
            $("#file_upload").uploadifive("upload");
			
			
        }
    });
	
	

    $("#uploadStoryButton").click(function() {
        if (validateForm("story")) {
			$("#innerForm").hide();
			$("#loading").show();
            $.ajax({
                    url: "submitStory.cfm", 
                    data: $("#uploadForm").serialize(),
                    complete: function(xhr, status) {
                        //console.debug(xhr.responseText);
                    },
                    success: function(data) {
                        // Story was submitted.
                        $("#recaptcha_reload").trigger("click");
                        console.debug(data);
						window.location.assign("/thankYou.cfm?type=story");
                    }
            });
        }
    });

    //$("#fillFormButton").on("click", fillForm());

    // Apply date-picker widget.
    $("#txtItemDate").datepicker({
        dateFormat: "mm/dd/yy",
        gotoCurrent: true,
        constrainInput: true
    });

    // Apply Chosen plug-in to limit choices on tags field.
    $("#txtItemTags").chosen({width: "100%"});

    /*$(".uploadType").click(function() {
		$("#uploadTypeButtons").hide();
		if ($(this).attr("id") == "file") {            
			$("#formTitle").text("Upload a file");
            $("#uploadifyQueue").show();
            $("#description").show();
            $("#storyInput").hide();
            $("#uploadStoryButton").hide();
            $("#uploadItemButton").show();
			$(".story").hide();
			$(".file").show();
            $("#uploadForm").show();
			
        }
		if ($(this).attr("id") == "story") {            
            $("#formTitle").text("Submit a story");
            $("#uploadifyQueue").hide();
            $("#description").hide();
            $("#storyInput").show();
            $("#uploadItemButton").hide();
            $("#uploadStoryButton").show();
			$(".story").show();
			$(".file").hide();
            $("#uploadForm").show();
        }
        return false;
    });*/
	
	$(".uploadType").change(function() {
		if ( $('input:radio[name=uploadType]:checked').attr("id") == "file") {            
			$("#formTitle").text("Upload a file");
            $("#uploadifyQueue").show();
            $("#description").show();
            $("#storyInput").hide();
            $("#uploadStoryButton").hide();
            $("#uploadItemButton").show();
			$(".story").hide();
			$(".file").show();
            $("#uploadForm").show();
			
        }
		if ($('input:radio[name=uploadType]:checked').attr("id") == "story") {            
            $("#formTitle").text("Submit a story");
            $("#uploadifyQueue").hide();
            $("#description").hide();
            $("#storyInput").show();
            $("#uploadItemButton").hide();
            $("#uploadStoryButton").show();
			$(".story").show();
			$(".file").hide();
            $("#uploadForm").show();
        }
        return false;
    });

});// end onready