$(function(){
	//set up drop down search form
	$('#searchClick').click(function(){
	  $('#searchWrap').slideToggle('fast', 'easeInOutQuad', function() {
		  // if visiible or not, focus/blur
		  $(this).is(":visible") ? $('#search-form-query').focus() : $('#search-form-query').blur();
		  
		  if ($(this).is(":visible"))
			{
				$('#searchClick i').removeClass('fa-search');
				$('#searchClick i').addClass('fa-times')
			}
		  else
			{
				$('#searchClick i').removeClass('fa-times');
				$('#searchClick i').addClass('fa-search')
			} 
		  
		});	
 	 }); //searchClick.click	
  
});// onready