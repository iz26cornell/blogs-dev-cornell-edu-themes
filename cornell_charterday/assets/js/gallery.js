// SAME MULTISELECT

// Create function to create multiselects for multiple variables. Allows for custom ID and display 
function same_multiselect(id,text){
    // Set Multiselect options
    $('select#'+id).multiselect({
        dropRight: true,
        numberDisplayed: 0,
        buttonContainer: '<div id="'+id+'_container" class="btn-group"/>',
        nonSelectedText: text,
        onChange: function(element, checked) {
            // Display a button that shows the filter chosen, or hide it if it's already displayed (and in that case also deselect the filter in the relevant menu).
            //console.debug(element, checked);
            var buttonID = "btn-" + $(element).text().replace(/\s/g, "").replace(/,/g, "");
            var buttonParentType = $(element).parent().attr('id');
            var buttonVal = $(element).val();
            var filters = '';
            var schoolFilters = '';
            var tagFilters = '';

            if (checked) {
                // Make the button.
                var btn = $("<button>").text($(element).text()).addClass("btn").addClass("btn-primary").addClass("btn-" + buttonParentType).addClass("filterButtons").attr("id", buttonID).data("myValue", buttonVal);
                // Have button do something when it's clicked, i.e., affect corresponding menu item..
                $(btn).on("click", function() {
                    var btnVal = $(this).data("myValue");
                    $("#multiselect_container input:checkbox").each(function() {
                        //console.debug($(this), $(this).val(), btnVal, $(this).prop("checked"));
                        if ($(this).val() == btnVal) {
                            $(this).trigger("click");
                            $("#multiselect_container input:checkbox").prop("disabled", false);
                        }
                    });
                });
                // Put the new button into the DOM.
                $('#display_tags').append(btn);
                // Disable the remaining menu options if we have reached the timeline plugin's filter limit.
                var numFilterButtons = $(".filterButtons").length;
                if (numFilterButtons == 6) {
                    $("#multiselect_container input:checkbox:not(:checked)").prop("disabled", true);
                }
            }
            else {
                // Get rid of the button.
                $("#" + buttonID).hide().remove();
                $("#multiselect_container input:checkbox").prop("disabled", false);
            }


            var schoolFilters = $('select#inputSchools').val();
            var tagFilters = $('select#inputTags').val();
            

            if (schoolFilters) { 
                schoolFilters = schoolFilters.join(', .');
                filters = '.' + schoolFilters;
                filters = filters.trim();
            } 

            if (tagFilters) {

                tagFilters = tagFilters.join(', .');
                if (schoolFilters) { 
                    filters = filters + ' ' + ', .' + tagFilters;
                } else { 
                    filters = filters + ' ' + '.' + tagFilters;
                }
    

            }

            $('ul#gallery-items').isotope({ filter: filters });

            console.log(filters);


        }//end onChange
    });
}