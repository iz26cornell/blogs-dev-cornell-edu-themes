<?php
/**
 * The main template file.
 *
 */

get_header(); ?>

            <!-- <div class="row inner-detail-wrapper"> -->
            <div class="col-sm-7 col-md-8 col-lg-9 events-main detail">
            <div class="inner">
        
				<?php if ( have_posts() ) : ?>
		
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						
						<div class="row no-bg">
                    <div class="col-sm-12"> <!-- .heading -->
                        <div class="page-title"><h2><?php the_title(); ?></h2></div>
                        <?php the_content(__('Read more'));?>
                        <?php edit_post_link(); ?>
                    </div>
                </div>
		
					<?php endwhile; ?>
		
					<?php posts_nav_link(' &#8212; ', __('&laquo; Previous'), __('Next &raquo;')); ?>
		
				<?php else : ?>
		
					<?php _e('Sorry, no posts matched your criteria.'); ?>
		
				<?php endif; ?>

            </div>
            <?php dynamic_sidebar('bottom'); ?>
            </div>
           <!--  </div> -->

<?php get_footer(); ?>
