/* Cornell Events Calendar: Charter Day Weekend (ama39 9/23/14) */

var api = '2.1';
var month_headers = false;
var groups_enabled = false;
var excerpt_length = 0; // use 0 for no truncation

function renderEvents(target,depts,group,singleday,entries,format) {
	group = (typeof group === 'undefined') ? 0 : group;
	singleday = (typeof singleday === 'undefined') ? false : singleday;
	entries = (typeof entries === 'undefined') ? 3 : entries;
	format = (typeof format === 'undefined') ? 'standard' : format;
	
	jQuery(document).ready(function($){
		getEvents(depts);

		function getEvents(depts) {
			
			var month_array = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			var day_array = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
			var day_array_abb = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
			var current_year;
			var current_month;
			var current_day;
			
			// archive option
			var today = new Date();
			var today_month = today.getMonth();
			var today_year = today.getFullYear();
			var past_year = today_year;
			var past_month = today_month - 6; // past 6 months
			if (past_month < 0) {
				past_month += 12;
				past_year -= 1;
			}
			var start_results = today.getFullYear() + '-' + addLeadingZero(parseInt(today.getMonth()+1)) + '-' + addLeadingZero(today.getDate());
			var end_results = parseInt(today.getFullYear()+1) + '-' + addLeadingZero(parseInt(today.getMonth()+1)) + '-' + addLeadingZero(today.getDate());
			if (format == 'archive') {
				end_results = start_results;
				start_results = past_year + '-' + addLeadingZero(parseInt(past_month+1)) + '-' + addLeadingZero(today.getDate());
			}
			
			// loading animation
			$('#'+target).append('<div id="loader"><span class="fa fa-spin fa-cog"></span></div>');
			var c_loader = setTimeout(function(){ $('#loader').fadeIn(50); }, 500); // skip loading animation if under 0.5s
			
			// group filter
			if (groups_enabled && group > 0) {
				group = [5708,group]; // for filtered requests, include the CTE All group as well (group_id 5708)
			}
			
			// single day
			if (singleday) {
				start_results = singleday;
				//end_results = singleday.split('-'); TODO: integrate both options days and end_results
			}
						
			$.ajax({
				url: '//events.cornell.edu/api/'+api+'/events',
				dataType: 'jsonp',
				crossDomain: true,
				data: {api_key: 'KLhy2GtuSAGirYGY', keyword: 'Charter Day Weekend', pp: entries, start: start_results, days: 1, group_id: group, distinct: true},
				complete: function(xhr, status) { // disabled "end: end_results" TODO: integrate both options
					// cancel loading animation
					clearTimeout(c_loader);
					$('#loader').fadeOut(200);
				},
				success: function(json) {
					//console.log(json);
					
					var events = json.events;
					var events_container = $('<div id="events-list" class="events cornell-events events-main">');
					if (format == 'compact') {
						$('#'+target).addClass('compact');
						events_container.addClass('compact');
					}
					// archive custom sorting
					if (format == 'archive') {
						events = events.reverse();
					}
					
					for (i=0;i<events.length;i++) {
						// data selection and processing
						title = events[i].event.title;
						localist_url = events[i].event.localist_url;
						location_name = events[i].event.location_name;
						room_number = events[i].event.room_number;
						venue_url = events[i].event.venue_url;
						if (excerpt_length > 0 ) {
							description = $.trim($.trim(events[i].event.description).substring(0, excerpt_length).split(' ').slice(0, -1).join(' '));
						}
						else {
							description = events[i].event.description;
						}
						photo_url = events[i].event.photo_url.replace('/huge/','/big/'); // retrieve thumbnail size
						event_fulldate = new Date(events[i].event.event_instances[0].event_instance.start);
						event_day = day_array_abb[event_fulldate.getDay()];
						event_date = events[i].event.event_instances[0].event_instance.start.split('T')[0];
						event_time = events[i].event.event_instances[0].event_instance.start.split('T')[1];
						event_time_hour = event_time.split(':')[0];
						event_time_min = event_time.split(':')[1];
						if (parseInt(event_time_hour) >= 12) {
							if (parseInt(event_time_hour) > 12) {
								event_time_hour = (parseInt(event_time_hour) - 12);
							}
							event_time = event_time_hour + ':' + event_time_min + ' p.m.';
						}
						else {
							event_time = parseInt(event_time_hour) + ':' + event_time_min + ' a.m.';
						}
						year = event_date.split('-')[0];
						month = event_date.split('-')[1].replace(/\b0+/g, ''); // remove leading zeroes
						day = event_date.split('-')[2].replace(/\b0+/g, ''); // remove leading zeroes
						event_date = month+'/'+day+'/'+year; // convert date format
						
						// optional fields
						group_name = 'General';
						if (events[i].event.group_name != undefined) {
							group_name = events[i].event.group_name;
						}
						event_types = '';
						if (events[i].event.filters.event_types != undefined) {
							event_types = events[i].event.filters.event_types[0].name;
						}
						ticket_cost = '';
						if (events[i].event.ticket_cost != undefined) {
							ticket_cost = events[i].event.ticket_cost;
						}
						event_time_end = '';
						if (events[i].event.event_instances[0].event_instance.end != undefined) {
							event_time_end = events[i].event.event_instances[0].event_instance.end.split('T')[1];
							// TODO: consolidate code into a utility function, to translate time format and populate variables >>>>
							event_time_end_hour = event_time_end.split(':')[0];
							event_time_end_min = event_time_end.split(':')[1];
							if (parseInt(event_time_end_hour) >= 12) {
								if (parseInt(event_time_end_hour) > 12) {
									event_time_end_hour = (parseInt(event_time_end_hour) - 12);
								}
								event_time_end = event_time_end_hour + ':' + event_time_end_min + ' p.m.';
							}
							else {
								event_time_end = parseInt(event_time_end_hour) + ':' + event_time_end_min + ' a.m.';
							}
							// ^^^^^^^^^
							//event_time += ' - ' + event_time_end;
							
						}
						else {
							event_time = 'all day';
						}
						
						// build node
						//event_node = $('<div class="node faculty grads ">');
						event_node = $('<div class="row">');
						
						
						// TODO: create markup template >>>
						c_title = $('<div class="col-xs-10">').html('<a target="_blank" href="'+localist_url+'"><p class="eventCity">'+title+'</p><p class="eventTitle">'+location_name+'</p></a>');
						
						c_date = $('<div class="col-xs-2 date">').html('<p class="month">'+event_time+'</p><p class="year">to '+event_time_end+'</p>');
						
						//c_eventinfo = $('<div class="col-xs-5">').html('<p class="availability"><a href="#">Now Available <i class="fa fa-ticket"></i></a></p><p class="times">'+ticket_cost+'</p>');
						c_eventinfo = '';
						
						// ^^^^^^^^^^
						
						if (api == '2.0') {
							c_abstract = $('<p>').text(description);
						}
						else {
							c_abstract = $('<div class="description"><div class="col-xs-10 description-content">'+description+'</div></div>');
						}
						if (excerpt_length > 0 ) {
							c_abstract.append(' <a class="read-more"></a>').find('.read-more').attr('href',localist_url).attr('target','_blank').text('... read more');
						}

						event_node.append(c_date).append(c_title).append(c_eventinfo);
						if (description) {
							event_node.append('<div class="event-details col-xs-10"><h4><a href="#"><span class="fa fa-chevron-down"></span>event details</a></h4></div>').append(c_abstract);
						}
						
						// month and day headers
						if (month_headers) {
							if (format != 'compact' && month != current_month) {
								current_month = month;
								current_day = day;
								mheader = $('<h3 class="month-header">').text(month_array[month-1] + ' ' + year);
								dheader = $('<h4 class="day-header">').text(event_day + ', ' + month_array[month-1] + ' ' + daySuffix(day));
								events_container.append(mheader).append(dheader);
							}
							else if (format != 'compact' && day != current_day) {
								current_day = day;
								dheader = $('<h4 class="day-header">').text(event_day + ', ' + month_array[month-1] + ' ' + daySuffix(day));
								events_container.append(dheader);
							}
						}
						
						events_container.append(event_node);
					}
					$('#'+target).append('<div class="category-listing">').find('.category-listing').append(events_container);
					
					$('#'+target+' .event-details a').click(function(e) {
						e.preventDefault();
						//console.log('click');
						
						$(this).parents('.event-details').first().next('.description').slideToggle(150);
						if ( $(this).children('.fa').hasClass('fa-chevron-down') ) {
							$(this).children('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-up');
						}
						else {
							$(this).children('.fa').removeClass('fa-chevron-up').addClass('fa-chevron-down');
						}
					});
					
				}
			});
			
		}
		
		// utility functions
		function daySuffix(day) {
			switch (day) {
				case '1':
				case '21':
				case '31':
					return day+'st';
				case '2':
				case '22':
					return day+'nd';
				case '3':
				case '23':
					return day+'rd';
				default:
					return day+'th';
			}
		}
		function addLeadingZero(num) {
			if (num.toString().length == 1) {
				num = '0'+num;
			}
			return num;
		}
				
	});
}


/* Active Menu Item for Side Nav */
jQuery(document).ready(function($){
	if (location.pathname.indexOf('/charterday/') >= 0) {
		var current_page = location.pathname.split('/charterday/')[1].split('/')[0];
		$('#eventsnav li > a').each(function(i) {
			if ($(this).attr('href').indexOf('/charterday/') >= 0) {
				this_link = $(this).attr('href').split('/charterday/')[1].split('/')[0];
				if ( !$(this).hasClass('drop') && this_link == current_page) {
					$(this).parent('li').addClass('active');
				}
			}
		});
	}
});
