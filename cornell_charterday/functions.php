<?php

/* Activate Post Thumbnails */
add_theme_support( 'post-thumbnails' );

/* Widget Areas */
if ( function_exists('register_sidebar') ) {
    
	register_sidebar( array(
		'name' => __( 'Header Details', 'Cornell' ),
		'id' => 'header-details',
		'description' => __( 'This text appears in the header, under the site title.', 'Cornell' ),
		'before_widget' => '<div class="hdr-details">',
		'after_widget' => '</div>'
	) );
	register_sidebar( array(
		'name' => __( 'Gold Banner', 'Cornell' ),
		'id' => 'gold-banner',
		'description' => __( 'This text appears between the header and main content.', 'Cornell' ),
		'before_widget' => '<h2>',
		'after_widget' => '</h2>'
	) );
	register_sidebar( array(
		'name' => __( 'Section Nav', 'Cornell' ),
		'id' => 'section-navigation',
		'description' => __( 'This is section navigation area.', 'Cornell' ),
		'before_widget' => '<div class="section-nav-wrap">',
		'after_widget' => '</div>'
	) );
	register_sidebar( array(
		'name' => __( 'Bottom', 'Cornell' ),
		'id' => 'bottom',
		'description' => __( 'This text appears between the main conent and the footer.', 'Cornell' ),
		'before_widget' => '<div class="inner">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	) );
}

/* Theme Customizer Settings */
function charterday_customize_register( $wp_customize ) {
	
	// settings
	$wp_customize->add_setting( 'cd_nav_path' , array(
		'default'	=>	''
	) );
	$wp_customize->add_setting( 'cd_asset_path' , array(
		'default'	=>	''
	) );
	
	// sections
	$wp_customize->add_section( 'charterday_dynamicpaths' , array(
		'title'		=>	__( 'Dynamic Paths (Advanced)', 'charterday' ),
		'priority'	=>	0
	) );
	
	// controls
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cd_nav_path', array(
		'label'		=>	__( 'Navigation Path', 'charterday' ),
		'section'	=>	'charterday_dynamicpaths',
		'settings'	=>	'cd_nav_path'
	) ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cd_asset_path', array(
		'label'		=>	__( 'Asset Path', 'charterday' ),
		'section'	=>	'charterday_dynamicpaths',
		'settings'	=>	'cd_asset_path'
	) ) );
}
add_action( 'customize_register', 'charterday_customize_register' );


/* Shortcode: Render Custom Field */
function customfields_shortcode($atts, $text) {
	global $post;
	return get_post_meta($post->ID, $text, true);
}
@add_shortcode('cf','customfields_shortcode');


/* http header addition for font sharing with 150.cornell.edu */
header('Access-Control-Allow-Origin: *');



?>