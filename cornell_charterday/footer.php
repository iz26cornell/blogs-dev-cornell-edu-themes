<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the main container and all content after
 *
 */
 	global $nav_path;
	global $asset_path;
?>
         </div><!-- #events-list -->


        
    </div>
    <!-- /WRAPPER -->

    <!-- FOOTER -->
    <?php include('includes/footer.html');?>
    <!-- /FOOTER -->
    
    <?php include('includes/javascripts.html');?>
    
    <!-- ANALYTICS TO BE INCLUDED AFTER LAUNCH -->
    <?php //include('includes/analytics.html'); ?>
    
    <?php wp_footer(); ?>
    
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-51880473-1', 'auto');
ga('send', 'pageview');

</script>
</body>
</html>