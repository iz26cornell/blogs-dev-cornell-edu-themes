<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and beginning of <body> including the start of the #events-list main container (including the side navigation)
 *
 */

	// Dynamic Paths (Set Through Theme Customizer)
	
	// navigation path
	global $nav_path;
	$nav_path = get_theme_mod('cd_nav_path');
	if ( empty($nav_path) ) {
		$nav_path = get_home_url(); 
	}
	// asset path
	global $asset_path;
	$asset_path = get_theme_mod('cd_asset_path');
	if ( empty($asset_path) ) {
		$asset_path = get_stylesheet_directory_uri('template_directory'); 
	}

?><!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php echo the_title() . ' - ' . get_bloginfo( 'name' ) . ' - ' . get_bloginfo( 'description' ); ?></title>
    <meta name="keywords" content="Cornell, Sesquicentennial" />
    <meta name="description" content="" />
    <meta name="Author" content="Ezra Cornell" />

    <!-- <link href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/assets/css/font-awesome.css" rel="stylesheet" type="text/css" /> -->
    <?php include('includes/head.html');?>

    <!-- Additional Styles -->
    <link href='//fonts.googleapis.com/css?family=Merriweather:400,300,700,700italic,300italic' rel='stylesheet' type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">   

    <?php wp_head(); ?>
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/style.css" />
    
    <!-- CWD Scripts -->
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws/iws.js"></script>
</head>

<body class="regional full-bg events-list-bg" <?php body_class(); ?>>

    <!-- HEADER AND MAIN NAV -->
    <?php include('includes/nav.html');?>

    <!-- /MAIN NAV -->

    <!-- WRAPPER -->
    <div id="wrapper">

    	<!-- PAGE TITLE -->
        <header id="page-title">
            <div class="container">
                <!-- CELEBRATE THE SESQUI IN YOUR CITY -->
                <h1>
                    <!-- <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> -->
                    <span class="celebrate">Celebrate the Sesquicentennial</span>
                    <span class="city"><?php bloginfo( 'name' ); ?></span>
                </h1>
                <!-- DETAILS, INCLUDING DATE, TIME, AND LOCATION -->
                <img class="header-image" src="//150.cornell.edu/assets/images/cdw/cdwstripebanner.jpg" width="100%" height="auto" alt="Charter Day Weekend: April 24-27, 2015">
                <?php dynamic_sidebar('header-details'); ?>
            </div>
        </header>
        
        <div class="goldbanner">
            <div class="container">
                <?php dynamic_sidebar('gold-banner'); ?>
            </div>
        </div>

        <div id="main-content" class="container detail-wrapper"> <!-- #events-list .detail-wrapper -->
            <?php include('includes/events-sidenav.php');?>