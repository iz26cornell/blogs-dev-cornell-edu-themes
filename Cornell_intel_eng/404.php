<?php get_header(); ?>
<div id="wrap">
<div id="content">
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
  <div id="main">
    <h2>Not Found, Error 404</h2>
    <p>The page you are looking for no longer exists.</p>
  </div>
  
</div>
<!-- The main column ends  -->
</div></div>
<?php get_footer(); ?>
