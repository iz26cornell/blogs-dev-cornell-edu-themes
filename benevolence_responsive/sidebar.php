<div id="sidebar">
<ul class="topMenu">

<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?> 

<li>
	<form style="padding: 0px; margin-top: 0px; margin-bottom: 0px;" id="searchform" method="get" action="<?php bloginfo('url'); ?>">
	<div class="title"><?php _e('Search:','benevolence'); ?></div>
	<p style="padding: 0px; margin-top: 0px; margin-bottom: 0px;"><input type="text" class="input" name="s" id="search" size="15" />
	<input name="submit" type="submit" tabindex="5" value="<?php _e('GO','benevolence'); ?>" /></p>
	</form>
</li>

<li>
	<h2 class="category"><?php _e('Categories','benevolence'); ?></h2>
    <ul class = "submenu">
	<?php wp_list_categories('list=0'); ?>
    </ul>
</li>


<li>
	<h2 ><?php _e('Archives','benevolence'); ?></h2>
    <ul class = "submenu">
	<?php wp_get_archives('type=monthly&format=other&after=<br />'); ?>
    </ul>
</li>

<li>
	<h2 class="title"><?php _e('Links','benevolence'); ?></h2>
    <ul class = "submenu">
	<?php get_bookmarks('-1', '', '<br />', '<br />', 0, 'name', 0, 0, -1, 0); ?>
	</ul>
</li>


<?php endif; ?> 
</ul>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
<script>
// variable to hold current window state - small, medium, or large
var windowState = 'large';

// check intital width of the screen, respond with appropriate menu
$(document).ready(function() {
    var sw = document.body.clientWidth;
    if (sw <= 799) {
		medMenu();
	} else {
		lgMenu();
	}
});

// take care of resizing the window
$(window).resize(function() {
	var sw = document.body.clientWidth;
    if (sw < 801 && windowState != 'mobile') {
       medMenu();
    }  
    if (sw > 800 && windowState != 'large') {
       lgMenu();
    } 
});


//handle menu for small screens
function medMenu() {
	
	//create the menu toggle
    $('.topMenu').before('<div class="menuToggle"><a href="#" class="exploreToggle">EXPLORE</a></div>'); /* <span class="indicator">+</span> */
    // append the + indicator
    //$('.topMenu h2').append('<span class="indicator">+</span>');
    // wire up clicks and changing the various menu states
	//we'll use clicks instead of touch in case a smaller screen has a pointer device
	//first, let's deal with the menu toggle
	
	var tempCtr = 0;

	
	$('.menuToggle a').click(function() {
		//expand the menu
		$('.topMenu').toggleClass('expand');
		// figure out whether the indicator should be changed to + or -
		/* var newValue = $(this).find('span.indicator').text() == '+' ? '-' : '+';
		// set the new value of the indicator
		$(this).find('span.indicator').text(newValue); */
		
		
		tempCtr=tempCtr+1;
		
		if (tempCtr%2 == 1){			
			$('.menuToggle').css({'background': 'rgb(203,203,203)'}); /* Old browsers */
			$('.menuToggle').css({'background': '-moz-linear-gradient(top, rgba(203, 203, 203 ,1) 0%, rgba(107, 107, 107,1) 100%)'}); /* FF3.6+ */
			$('.menuToggle').css({'background': '-webkit-gradient(linear, bottom, top, color-stop(0%,rgba(203, 203, 203,1)), color-stop(100%,rgba(107, 107, 107,1)))'}); /* Chrome,Safari4+ */
			$('.menuToggle').css({'background': '-webkit-linear-gradient(top, rgba(203, 203, 203,1) 0%,rgba(107, 107, 107,1) 100%)'}); /* Chrome10+,Safari5.1+ */
			$('.menuToggle').css({'background': '-o-linear-gradient(top, rgba( 203, 203, 203, 1) 0%,rgba(107, 107, 107,1) 100%)'}); /* Opera 11.10+ */
			$('.menuToggle').css({'background': '-ms-linear-gradient(top, rgba(203, 203, 203,1) 0%,rgba(107, 107, 107,1) 100%)'}); /* IE10+ */
			$('.menuToggle').css({'background': 'linear-gradient(top, rgba(203, 203, 203,1) 0%,rgba(107, 107, 107,1) 100%)'}); /* W3C */
		} else {
			$('.menuToggle').css({'background': 'rgb(107, 107, 107)'}); /* Old browsers */
			$('.menuToggle').css({'background': '-moz-linear-gradient(top, rgba(107, 107, 107,1) 0%, rgba(203, 203, 203,1) 100%)'}); /* FF3.6+ */
			$('.menuToggle').css({'background': '-webkit-gradient(linear, bottom, top, color-stop(0%,rgba(107, 107, 107,1)), color-stop(100%,rgba(203, 203, 203,1)))'}); /* Chrome,Safari4+ */
			$('.menuToggle').css({'background': '-webkit-linear-gradient(top, rgba(107, 107, 107,1) 0%,rgba(203, 203, 203,1) 100%)'}); /* Chrome10+,Safari5.1+ */
			$('.menuToggle').css({'background': '-o-linear-gradient(top, rgba(107, 107, 107, 1) 0%,rgba(203, 203, 203,1) 100%)'}); /* Opera 11.10+ */
			$('.menuToggle').css({'background': '-ms-linear-gradient(top, rgba(107, 107, 107,1) 0%,rgba(203, 203, 203,1) 100%)'}); /* IE10+ */
			$('.menuToggle').css({'background': 'linear-gradient(top, rgba(107, 107, 107,1) 0%,rgba(203, 203, 203,1) 100%)'}); /* W3C */
		}
		/*
		if (tempCtr%2 == 1){
			<style type="text/css">
			.menuToggle{
				background: red;	
			}
			</style>
		}else if (tempCtr%2 == 0) {
			<style type="text/css">
			.menuToggle{
				background: green;	
			}
			</style>	
		}
		*/
		
		
	});
	
	/*
		//now we'll wire up the submenus
	$(".topMenu h2").click(function() {
		//find the current submenu
		var currentItem = $(this).siblings('.submenu');
		//remove the expand class from other submenus to close any currently open submenus
		$('ul.submenu').not(currentItem).removeClass('expand');
		//change the indicator of any closed submenus 
		$('.topMenu h2').not(this).find('span.indicator:contains("-")').text('+');
		//open the selected submenu
		$(this).siblings('.submenu').toggleClass('expand');
		//change the selected submenu indicator
		var newValue = $(this).find('span.indicator').text() == '+' ? '-' : '+';
        $(this).find('span.indicator').text(newValue);
	});
	*/
	//indicate current window state
	windowState = 'mobile';
}


//handle menu for large screens
function lgMenu() {
	 //largely what we'll do here is simple remove functionality that
	//may be left behind by other screen sizes
	//at this size the menu will function as a pure-css driven dropdown
	//advances in touch screen are beginning to make us re-think
	//this approach
    // unbind click and touch events    
    $('.menuToggle a').off('click');
    $('.topMenu h2').off('click touchstart');
	$('html').off('touchstart');
	$('#sidebar').off('touchstart');
    
    // remove any expanded submenus
    $('.topMenu').find('ul.submenu').removeClass('expand');
    
    // remove the span tags inside the menu
    $('.topMenu h2').find('span.indicator').remove();
    
    // remove the "menu" element
    $('.menuToggle').remove();
	
    //indicate current window state
    windowState = 'large';
}

</script>
