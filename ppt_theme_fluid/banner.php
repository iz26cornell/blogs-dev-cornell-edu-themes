<!--
	The following link provides a way for people using text-based browsers and
	screen readers to skip over repetitive navigation elements so that they can 
	get directly to the content. It is hidden from general users through CSS.
-->
<div id="skipnav">
	<a href="#content">Skip to main content</a>
</div>

<hr />

<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity">
	<div id="cu-logo">
		<a href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/cu_logo_unstyled.gif" alt="Cornell University" /></a>
	</div>
	<div id="cu-search">
		<a href="http://www.cornell.edu/search/">Search Cornell</a>
	</div>
</div>

<hr />