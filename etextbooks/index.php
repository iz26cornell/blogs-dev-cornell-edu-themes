<?php get_header(); ?>
<div id="wrap">
<div id="content">
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
  <div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h2><a href="<?php the_permalink() ?>" rel="bookmark">
      <?php the_title(); ?>
      </a></h2>
      <?php the_content(__('Read more'));?>
    <div class="postmeta">
      <div class="postmetaleft">
      
        <p>
        <span class="avatar-box"><?php echo get_avatar( get_the_author_email(), '25' ); ?><br/><?php the_author_posts_link(); ?></span>
          category: 
          <?php the_category(', ') ?><br />
          posted: <?php the_time('m/j/y g:i A') ?> by  <?php the_author_posts_link(); ?><br />
          <?php if ('open' == $post->comment_status) : ?><?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?> <?php else : ?>
<?php endif; ?><span class="edit-post""><?php edit_post_link('edit', '', ''); ?></span>
        </p>
      </div>
    </div>
    <!--

	<?php trackback_rdf(); ?>

	-->
    <h2>Comments</h2>
    <?php comments_template(); // Get wp-comments.php template ?>
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
  </div>
  
</div>

</div>
<?php get_footer(); ?>
