<!-- begin footer -->

<hr />
<div id="footer">
  <!-- The footer-content div contains the Cornell University copyright -->
  <div id="footer-content">
    <p><a href="http://www.it.cornell.edu/teaching/">Contact Us</a></p>
    <ul class="admin">
      <li>
        <?php wp_loginout(); ?>
      </li>
      <?php wp_register(); ?>
      <li><span class="blog-login"><?php wp_footer();?></span></li>
    </ul>
    <p class="copyrights"><span>&copy;<?php echo date("Y");?> <a href="http://www.cornell.edu/">Cornell University</a></span></p>
    
  </div>
</div>

<?php
if (isset($_SESSION["iws_amMobileDevice"]) && $_SESSION["iws_amMobileDevice"] === "TRUE") {
	echo "<div id=\"mobileswitch\">";
    echo "<h3>Switch to:<br />";
    echo "<a href=\"?startMobileSession=1\">Mobile Version</a>";
    echo "<a class=\"last\" href=\"?killMobileSession=1\">Web Version</a>";
    echo "</h3>";
    echo "</div>";
}
?>

</body></html>