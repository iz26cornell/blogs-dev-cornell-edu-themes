<?PHP
// IWS: Cornell Search
if (isset($_GET['submit'])) {
	session_start();
	$selected_radio = $_GET['sitesearch'];
	
	if ($selected_radio == 'cornell') {
		$search_terms = urlencode($_GET['s']);
		$URL="http://www.cornell.edu/search" . "?q=" . $search_terms . "&client=default_frontend&proxystylesheet=default_frontend";
		print $URL;
		header ("Location: $URL");
	}
}
?>
<?php get_header(); ?>

<?php do_action( 'bp_before_blog_page' ) ?>

<div id="post-entry">

<?php if (have_posts()) : ?>

<?php locate_template (array('/lib/templates/wp-template/headline.php'), true); ?>

<?php while (have_posts()) : the_post(); ?>
<div <?php if(function_exists("post_class")) : ?><?php post_class(); ?><?php else: ?>class="page"<?php endif; ?> id="post-<?php the_ID(); ?>">
<h1 class="post-title"><?php the_title(); ?></h1>
<div class="post-content">
<?php $facebook_like_status = get_option('tn_wpmu_facebook_like_status'); if ($facebook_like_status == 'enable') { ?>
<div class="fb-like" data-href="<?php echo urlencode(get_permalink($post->ID)); ?>" data-send="false" data-width="450" data-show-faces="false" style="margin-bottom: 6px;"></div>
<?php } ?>
<?php the_content(__('<p>Read the rest of this entry &raquo;</p>', 'nelo')); ?>
<p><?php edit_post_link(__('Edit This Page', 'nelo')); ?></p>
</div>
</div>

<?php endwhile; ?>

<?php if ( comments_open() ) { ?><?php comments_template('', true); ?><?php } ?>

<?php locate_template (array('/lib/templates/wp-template/paginate.php'), true); ?>

<?php else: ?>

<?php locate_template (array('/lib/templates/wp-template/result.php'), true); ?>

<?php endif; ?>

</div>

<?php do_action( 'bp_after_blog_page' ) ?>

<?php get_sidebar(); ?>

<?php locate_template (array('/lib/templates/wp-template/bottom-content.php'), true); ?>

<?php get_footer(); ?>