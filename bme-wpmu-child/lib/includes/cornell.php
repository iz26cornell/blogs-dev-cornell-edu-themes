<div id="skipnav"><a href="#content">Skip to main content</a></div>
<div id="cu-identity" class="theme-white75">
  <div id="cu-identity-content">
  
    <div id="cu-logo" class="unit75">
    <a href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/lib/images/BME_2line_4c.gif" alt="Cornell University" /></a>
    <a id="unit-link" href="http://www.bme.cornell.edu/">Department of Biomedical Engineering</a>
    </div>
    
    
    <div id="cu-search" class="options">
      <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
        <div id="search-form">
          <label for="search-form-query">SEARCH:</label> 
          <input type="text" id="search-form-query" name="s" value="enter keyword" size="20" onblur="this.value = this.value || this.defaultValue;" onfocus="this.value == this.defaultValue && (this.value =''); this.select()" />
          <input type="submit" id="search-form-submit" name="submit" value="go" />
      </div>
      <div id="search-filters">
			<input type="radio" name="sitesearch" id="search-filters1" value="thissite" checked="checked" />
			<label for="search-filters1">This Site</label>
			<input type="radio" name="sitesearch" id="search-filters2" value="cornell" />
			<label for="search-filters2">Cornell</label>
		</div>
      </form>
    </div>
    
  </div>
</div>
