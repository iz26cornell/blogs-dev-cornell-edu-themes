<?php
/*
Template Name: All Blogs
*/
?>
<?php get_header(); ?>
<div id="wrap">
<div id="content">
  <div id="main">
    <h2>
      <?php the_title(); ?>
    </h2>
    <?php
	list_all_wpmu_blogs('10000', 'name', '<p>', '</p>', 'updated');
	?>
  </div>
  <?php include(TEMPLATEPATH."/r_sidebar.php");?>
</div>
<!-- The main column ends  -->
</div>
<?php get_footer(); ?>
