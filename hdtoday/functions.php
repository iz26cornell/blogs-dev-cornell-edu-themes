<?php
// Start the engine
require_once(TEMPLATEPATH.'/lib/init.php');

// Add new image sizes
genesis_add_image_size('Mini Square', 70, 70, TRUE);
genesis_add_image_size('Square', 130, 170, TRUE);
genesis_add_image_size('Tabber', 200, 200, TRUE);
genesis_add_image_size('Cover', 226, 300, TRUE);

// Force layout on homepage
add_filter('genesis_options', 'lifestyle_home_layout', 10, 2);
function lifestyle_home_layout($options, $setting) {
	if($setting == GENESIS_SETTINGS_FIELD) {
		if(is_home())
		$options['site_layout'] = 'content-sidebar';
	}
	return $options;
}

// Add Google AdSense after single post
add_action('genesis_after_post_content', 'lifestyle_include_adsense', 9); 
function lifestyle_include_adsense() {
    if(is_single())
    require(CHILD_DIR.'/adsense.php');
}

// Add two sidebars to the main sidebar area
add_action('genesis_after_sidebar_widget_area', 'lifestyle_include_bottom_sidebars'); 
function lifestyle_include_bottom_sidebars() {
    require(CHILD_DIR.'/sidebar-bottom.php');
}

// Add sidebars above main sidebar area
add_action('genesis_before_sidebar_widget_area', 'lifestyle_top_sidebar'); 
function lifestyle_top_sidebar() { ?>
<div id="sidebar-top">
	<?php if (!dynamic_sidebar('Sidebar Top')) :
	endif; ?>
</div>
<?php }

// Register sidebars
genesis_register_sidebar(array(
	'name'=>'Sidebar Bottom Left',
	'description' => 'This is the bottom left column in the sidebar.',
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Sidebar Bottom Right',
	'description' => 'This is the bottom right column in the sidebar.',
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Home Top',
	'description' => 'This is the featured bottom section of the homepage.',
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Featured Top Left',
	'description' => 'This is the featured top left column of the homepage.',
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Featured Top Right',
	'description' => 'This is the featured top right column of the homepage.',
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Featured Bottom',
	'description' => 'This is the featured bottom section of the homepage.',
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Sidebar Top',
	'description' => 'This is the sidebar top section of the homepage.',
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));
?>