Lifestyle Child Theme
http://www.studiopress.com/themes/lifestyle

INSTALL
1. Upload the Lifestyle child theme folder via FTP to your wp-content/themes/ directory.
2. Go to your WordPress dashboard and select Appearance.
3. Activate the Lifestyle theme.
4. Inside your WordPress dashboard, go to Genesis > Theme Settings and configure them to your liking. (the Genesis parent theme needs to be in the wp-content/themes/ directory as well)

DYNAMIC CONTENT GALLERY
1. Download the latest version of the Dynamic Content Gallery plugin (http://www.studiograsshopper.ch/dynamic-content-gallery)
2. Upload the Dynamic Content Gallery plugin folder via FTP to your wp-content/plugins directory, and follow the directions from the docs page to install it. (http://www.studiograsshopper.ch/dynamic-content-gallery/documentation)

WIDGETS
Sidebar - This is the main sidebar if you are using the Content/Sidebar, Sidebar/Content, Content/Sidebar/Sidebar, Sidebar/Sidebar/Content or Sidebar/Content/Sidebar Site Layout option.
Sidebar Alt - This is the alternate sidebar if you are using the Content/Sidebar/Sidebar, Sidebar/Sidebar/Content or Sidebar/Content/Sidebar Site Layout option.
Sidebar Bottom Left - This is the bottom left sidebar which is placed under the main sidebar.
Sidebar Bottom Right - This is the bottom right sidebar which is placed under the main sidebar.
Featured Top Left - This is the featured top left column of the homepage.
Featured Top Right - This is the featured top right column of the homepage.
Featured Bottom - This is the featured bottom section of the home page.

THUMBNAILS
By default WordPress will create a default thumbnail image for each image you upload and the size can be specified in your dashboard under Settings > Media. In addition, the Lifestyle child theme creates the following thumbnail images you'll see below, which are defined (and can be modified) in the functions.php file. These are the recommended thumbnail sizes that are used on the Lifestyle child theme demo site.

Mini Square - 70px by 70px
Square - 110px by 110px

SUPPORT
If you are looking for theme support, please visit http://www.studiopress.com/support.