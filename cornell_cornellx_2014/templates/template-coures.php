<?php
/**
 * Template Name: One column for Course Slide Show, no sidebar
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package Cryout Creations
 * @subpackage parabola
 * @since parabola 0.5
 */
get_header();
?>




 <?php

// When a post query has been selected from the Slider type in the admin area
     global $post;
     // Initiating query
     $custom_query = new WP_query();
     $slides = array();
	 
	 if($parabola_pslideNumber>0):

     // Switch for Query type
     switch ($parabola_pslideType) {
          case 'Latest Posts' :
               $custom_query->query('showposts='.$parabola_pslideNumber.'&ignore_sticky_posts=1');
          break;
          case 'Random Posts' :
               $custom_query->query('showposts='.$parabola_pslideNumber.'&orderby=rand&ignore_sticky_posts=1');
          break;
          case 'Latest Posts from Category' :
               $custom_query->query('showposts='.$parabola_pslideNumber.'&category_name='.$parabola_pslideCateg.'&ignore_sticky_posts=1');
          break;
          case 'Random Posts from Category' :
               $custom_query->query('showposts='.$parabola_pslideNumber.'&category_name='.$parabola_pslideCateg.'&orderby=rand&ignore_sticky_posts=1');
          break;
          case 'Sticky Posts' :
               $custom_query->query(array('post__in'  => get_option( 'sticky_posts' ), 'showposts' =>$parabola_pslideNumber,'ignore_sticky_posts' => 1));
          break;
          case 'Specific Posts' :
               // Transofm string separated by commas into array
               $pieces_array = explode(",", $parabola_pslideSpecific);
               $custom_query->query(array( 'post_type' => 'any', 'post__in' => $pieces_array, 'ignore_sticky_posts' => 1,'orderby' => 'post__in' ));
               break;
          case 'Custom Slides':

               break;
     }//switch
	 
	 endif; // slidenumber>0

	 add_filter( 'excerpt_length', 'parabola_excerpt_length_slider', 999 );
	 add_filter( 'excerpt_more', 'parabola_excerpt_more_slider', 999 );
     // switch for reading/creating the slides
     switch ($parabola_pslideType) {
          case 'Page Custom Slides':
		       for ($i=1;$i<=10;$i++):
                    if(${"parabola_psliderimg$i"}):
                         $slide['image'] = esc_url(${"parabola_psliderimg$i"});
                         $slide['link'] = esc_url(${"parabola_psliderlink$i"});
                         $slide['title'] = ${"parabola_pslidertitle$i"};
                         $slide['text'] = ${"parabola_pslidertext$i"};
                         $slides[] = $slide;
                    endif;
               endfor;
               break;
          default:
			   if($parabola_pslideNumber>0):	
               if ( $custom_query->have_posts() ) while ($custom_query->have_posts()) :
                    $custom_query->the_post();
                         $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),'slider'); 
                	$slide['image'] = $img[0];
                	$slide['link'] = get_permalink();
                	$slide['title'] = get_the_title();
                	$slide['text'] = get_the_excerpt();
                	$slides[] = $slide;
               endwhile;
			   endif; // slidenumber>0
               break;
     }; // switch

?> 
<br />

 <div class="jcarousel-wrapper">
                <div class="jcarousel">
                  
          
<?php        
      if (count($slides)>0):   
     ?>
<ul>
	<?php foreach($slides as $id=>$slide):
            if($slide['image']): 
			if($slide['link']) {
			   $link = 	$slide['link'];
			   } else { $link = "#"; }
			
			?>
            <li><a href='<?php echo ($slide['link']?$slide['link']:'#'); ?>'>
                 <img src='<?php echo $slide['image']; ?>' alt="" <?php if ($slide['title'] || $slide['text']): ?>title="#caption<?php echo $id;?>" <?php endif; ?> width="250" height="110"/>
            </a><?php if ($slide['title']) { echo '<div class="jc-title"><a href="'.$link.'"><span class="jc-content">'.$slide['title'].'</span></a></div>'; ?></li><?php } endif; ?>
     <?php endforeach; ?>
    
</ul>  
      </div>

              <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next">&rsaquo;</a>

                <p class="jcarousel-pagination"></p>
          </div>
     
<?php endif; ?>  
  
<section id="container" class="one-column">
        
 <div id="content" role="main">
            
            <?php get_template_part( 'content/content', 'page'); ?>
           </div><!-- #content -->
                
         </section><!-- #container -->

<?php get_footer(); ?>
