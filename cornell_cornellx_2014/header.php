<?php
/**
 * The Header
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Cryout Creations
 * @subpackage parabola
 * @since parabola 0.5
 **/
$parabolas= parabola_get_theme_options();
foreach ($parabolas as $key => $value) { ${"$key"} = $value; } 
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php  cryout_meta_hook(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
 	cryout_header_hook();
	wp_head();
	?>
</head>
<body <?php body_class(); ?>>

<?php cryout_body_hook(); ?>

<div id="wrapper" class="hfeed">

<?php cryout_wrapper_hook(); ?>

<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity">
	<div id="cu-logo">
		<a id="insignia-link" href="http://www.cornell.edu/"><img src="http://iws-lamp-dev.hosting.cornell.edu/wordpress/stan/files/2013/07/cornell.gif" alt="Cornell University" /></a>
    </div>
    
    
 <div id="cu-search" class="options">
	<form role="search" method="get" id="searchform" action="<?php echo home_url('/'); ?>">
		<div id="search-form">
				<input type="radio" name="sitesearch" id="search-filters1" value="thissite" checked="checked">
				<label for="search-filters1">This Site</label>
				<input type="radio" name="sitesearch" id="search-filters2" value="cornell">
				<label for="search-filters2">Cornell</label>
				<input type="text" id="search-form-query" name="s" value="Search" size="20" onBlur="this.value = this.value || this.defaultValue;" onFocus="this.value == this.defaultValue &amp;&amp; (this.value =''); this.select()">
				<input type="submit" id="search-form-submit" name="btnG" value="go">
		</div>
	</form>
</div>
    
   <!--
   <form action="http://www.cornell.edu/search" method="GET" name="gs" class="formaction">

        <input type="text" name="q" value="" size="40" maxlength="256" class="textform"/>
        
        <input type="submit" name="btnG" value="Go" class="submitform"/>
        
        <!-- SITE SEARCH: to search specific sites, add URL in 'value' below -->
 <!--       
        <input type="hidden" name="as_sitesearch" value="www.cornellx.cornell.edu" />
 
	</form>
    -->
   
</div>





<div id="header-full">

<header id="header">

<?php cryout_masthead_hook(); ?>

		<div id="masthead">

			<div id="branding" role="banner" >

				<?php cryout_branding_hook();?>
				<div style="clear:both;"></div>
 <div id="new">  <nav id="access" role="navigation">
    
                    <?php cryout_access_hook();?>
    
                </nav><!-- #access -->
			</div>
			</div><!-- #branding -->

             
		</div><!-- #masthead -->

	<div style="clear:both;height:1px;width:1px;"> </div>

</header><!-- #header -->
</div><!-- #header-full -->
<div id="main">
	<div  id="forbottom" >
		<?php cryout_forbottom_hook(); ?>

		<div style="clear:both;"> </div>

		<?php cryout_breadcrumbs_hook();?>
