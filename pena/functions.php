<?php
/**
 * pena functions and definitions
 *
 * @package Pena
 */

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
if ( ! isset( $content_width ) )
	$content_width = 870; /* pixels */

/**
 * Adjusts content_width value for full-width page and grid page.
 */
if ( ! function_exists( 'pena_content_width' ) ) :

function pena_content_width() {
	global $content_width;
	if ( is_page_template( 'page-templates/grid-template.php' )
	|| is_page_template( 'page-templates/involved-template.php' )
	|| is_page_template( 'page-templates/splash-template.php' )
	|| is_page_template( 'page-templates/main-template.php' )
	|| is_page_template( 'page-templates/main-template-one.php' )
	|| is_page_template( 'page-templates/fullwidth-boxed-template.php' )
	|| is_page_template( 'page-templates/fullwidth-template.php' ))
		$content_width = 1280;
}
add_action( 'template_redirect', 'pena_content_width' );

endif; // if ! function_exists( 'pena_content_width' )

if ( ! function_exists( 'pena_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function pena_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on pena, use a find and replace
	 * to change 'pena' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'pena', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Activate support for featured images
	add_theme_support( 'post-thumbnails' );

	// Front Page Child Page thumbnail
	add_image_size( 'pena-front-child-page-thumbnail', 430, 9999 );
	add_image_size( 'pena-front-child-page-one-thumbnail', 1280, 9999 );
	add_image_size( 'pena-front-child-page-two-thumbnail', 624, 9999 );

	// Front Page Latest Post thumbnail
	add_image_size( 'pena-recent-post-image', 624, 9999 );
	
	// Post Page thumbnail
	add_image_size( 'pena-post-thumbnail', 870, 9999 );
	add_image_size( 'pena-post-list-thumbnail', 430, 9999 );
	add_image_size( 'pena-post-grid-two-thumbnail', 624, 9999 );
	add_image_size( 'pena-post-grid-three-thumbnail', 405, 9999 );
	
	/*
	 * Enable support for custom logo.
	 *
	 *  @since Pena 1.0
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 9999,
		'width'       => 9999,
		'flex-height' => true,
	) );

	add_filter( 'excerpt_more', 'pena_continue_reading_link' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/* Add support for editor styles */
	add_editor_style( array( 'editor-style.css', pena_fonts_url() ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'pena' ),
		'social'  => esc_html__( 'Social Menu', 'pena' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'pena_custom_background_args', array(
		'default-color' => 'ffffff',
	) ) );
	
	// Add support for Gutenberg
	add_theme_support( 'gutenberg', array(
		'wide-images' => true,
		'colors' => array(
			'#980560',
			'#20c9f3',
		),
	) );
}
endif; // pena_setup
add_action( 'after_setup_theme', 'pena_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function pena_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'pena' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Front Page Call To Action Block', 'pena' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Front Page Recent Post Block', 'pena' ),
		'id'            => 'sidebar-3',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widgets 1', 'pena' ),
		'id'            => 'footer-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widgets 2', 'pena' ),
		'id'            => 'footer-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widgets 3', 'pena' ),
		'id'            => 'footer-3',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Menu Widget', 'pena' ),
		'id'            => 'footer-4',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'pena_widgets_init' );

if ( ! function_exists( 'pena_fonts_url' ) ) :
/**
 * Define Google Fonts
 */
function pena_fonts_url() {
	$fonts_url = '';

	/* Translators: If there are characters in your language that are not
	* supported by Droid, translate this to 'off'. Do not translate
	* into your own language.
	*/
	$droid = esc_html_x( 'on', 'Droid Serif: on or off', 'pena' );

	/* Translators: If there are characters in your language that are not
	* supported by Pacifico, translate this to 'off'. Do not translate
	* into your own language.
	*/
	$montserrat = esc_html_x( 'on', 'Montserrat font: on or off', 'pena' );

	if ( 'off' !== $droid || 'off' !== $montserrat ) {
		$font_families = array();

		if ( 'off' !== $droid ) {
			$font_families[] = 'Merriweather:400,300,300italic,400italic,700,700italic,900,900italic';
		}

		if ( 'off' !== $montserrat ) {
			$font_families[] = 'Montserrat:400,700';
		}

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;


/*
 * Query whether WooCommerce is activated.
 */
function pena_is_woocommerce_activated() {
	if ( class_exists( 'woocommerce' ) ) {
		return true;
	} else {
		return false;
	}
}

/*
 * Query whether Events Calendar is activated.
 */
function pena_is_the_events_calendar_activated() {
	if ( class_exists( 'the-events-calendar' ) ) {
		return true;
	} else {
		return false;
	}
}

/*
 * Query whether Give is activated.
 */
function pena_is_give_activated() {
	if ( class_exists( 'give' ) ) {
		return true;
	} else {
		return false;
	}
}
/**
 * Enqueue scripts and styles.
 */
function pena_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'pena-fonts', pena_fonts_url(), array(), null );

	wp_enqueue_style( 'pena-style', get_stylesheet_uri() );
	
	wp_enqueue_script( 'pena-main', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), '1.0', true );

	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );
	
	if ( pena_is_woocommerce_activated() ) {
	wp_enqueue_style( 'pena-woocommerce', get_theme_file_uri() . '/css/woocommerce.css', array(), '1.0' );
	}
	
	if ( pena_is_the_events_calendar_activated() ) {
	wp_enqueue_style( 'pena-events', get_template_directory_uri() . '/css/tribe-events.css', array(), '1.0' );
	}
	
	if ( pena_is_give_activated() ) {
	wp_enqueue_style( 'pena-give', get_template_directory_uri() . '/css/give.css', array(), '1.0' );
	}
	
	// Add styles for Gutenberg blocks. -This will load even if Gutenerg is not active, in case there is content that has been edited in Gutenberg before.
	wp_enqueue_style( 'pena-blocks-style', get_template_directory_uri() . '/assets/css/blocks.css' );

	$layout = get_theme_mod( 'pena_blog_layout' );
	if ( is_page_template( 'page-templates/main-template.php' )
	|| is_page_template( 'page-templates/main-template-one.php' )
	|| is_page_template( 'page-templates/grid-template.php' )
	|| is_page_template( 'page-templates/involved-template.php' )
	|| is_page_template( 'page-templates/about-template.php' )) {
		wp_enqueue_script( 'pena-masonry', get_template_directory_uri() . '/js/grid.js', array( 'jquery', 'masonry' ), '', true );
	}
	
	$layout = get_theme_mod( 'pena_blog_layout' );
	if (( $layout === 'grid-two' ||  $layout === 'grid-three' ||  $layout === 'grid-two-sidebar' ) ) {
		wp_enqueue_script( 'pena-masonry', get_template_directory_uri() . '/js/grid-custom.js', array( 'jquery', 'masonry' ), '', true );
	}

	wp_enqueue_script( 'pena-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'pena-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'pena_scripts' );

if (!function_exists('pena_admin_scripts')) {
	function pena_admin_scripts($hook) {
		if ('appearance_page_blog' === $hook) {
			wp_enqueue_style('pena-admin', get_template_directory_uri() . '/admin/admin.css');
		}
	}
}
add_action('admin_enqueue_scripts', 'pena_admin_scripts');

/*
 * Filters the Categories archive widget to add a span around the post count
 */

function pena_cat_count_span( $links ) {
	$links = str_replace( '</a> (', '</a><span class="post-count">(', $links );
	$links = str_replace( ')', ')</span>', $links );
	return $links;
}
add_filter( 'wp_list_categories', 'pena_cat_count_span' );

/*
 * Add a span around the post count in the Archives widget
 */

function pena_archive_count_span( $links ) {
  $links = str_replace( '</a>&nbsp;(', '</a><span class="post-count">(', $links );
  $links = str_replace( ')', ')</span>', $links );
  return $links;
}
add_filter( 'get_archives_link', 'pena_archive_count_span' );

if ( ! function_exists( 'pena_continue_reading_link' ) ) :
/**
 * Returns an ellipsis and "Continue reading" plus off-screen title link for excerpts
 */
function pena_continue_reading_link() {
	return '...<a class="excerpt-link" href="'. esc_url( get_permalink() ) . '">' . sprintf( wp_kses_post( __( 'Continue reading <span class="screen-reader-text">%1$s</span> <span class="meta-nav" aria-hidden="true">&rarr;</span>', 'pena' ) ), esc_attr( strip_tags( get_the_title() ) ) ) . '</a>';
}
endif; // pena_continue_reading_link
/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 */
function pena_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= pena_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'pena_custom_excerpt_more' );
add_filter('the_excerpt', 'do_shortcode');

/*
 * Custom comments display to move Reply link,
 * used in comments.php
 */
function pena_comments( $comment, $args, $depth ) {
?>
		<li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>
			<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-metadata">
						<span class="comment-author vcard">
							<?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>

							<?php printf( '<b class="fn">%s</b>', get_comment_author_link() ); ?>
						</span>
						<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID, $args ) ); ?>">
							<time datetime="<?php comment_time( 'c' ); ?>">
								<?php printf( '<span class="comment-date">%1$s</span><span class="comment-time screen-reader-text">%2$s</span>', get_comment_date(), get_comment_time() ); ?>
							</time>
						</a>
						<?php
						comment_reply_link( array_merge( $args, array(
							'add_below' => 'div-comment',
							'depth'     => $depth,
							'max_depth' => $args['max_depth'],
							'before'    => '<span class="reply">',
							'after'     => '</span>'
						) ) );
						?>
						<?php edit_comment_link( esc_html__( 'Edit', 'pena' ), '<span class="edit-link">', '</span>' ); ?>

					</div><!-- .comment-metadata -->

					<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'pena' ); ?></p>
					<?php endif; ?>
				</footer><!-- .comment-meta -->

				<div class="comment-content">
					<?php comment_text(); ?>
				</div><!-- .comment-content -->

			</article><!-- .comment-body -->
<?php
}
/***** Include Admin *****/

if (is_admin()) {
	require_once('admin/admin.php');
}
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Widgets compatibility file.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Load Styles.
 *
 * @since pena 1.0
 */
require( get_template_directory() . '/inc/pena_customizer_style.php' );

/**
 * WooCommerce
 *
 * Unhook sidebar
 */
add_theme_support( 'woocommerce' );
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 40;' ), 20 );

/**
 * TGM Plugin Activation
 */
require get_template_directory() . '/assets/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'pena_require_plugins' );

function pena_require_plugins() {

	$plugins = array(
		// One Click Demo Import
		array(
			'name'      => esc_html__( 'One Click Demo Import', 'pena' ),
			'slug'      => 'one-click-demo-import',
			'required'  => false,
		),
		// Woocommerce
		array(
			'name'      => esc_html__( 'Woocommerce', 'pena' ),
			'slug'      => 'woocommerce',
			'required'  => false,
		),
		// Contact Form 7
		array(
			'name'      => esc_html__( 'Contact Form 7', 'pena' ),
			'slug'      => 'contact-form-7',
			'required'  => false,
		),
		// Give
		array(
			'name'      => esc_html__( 'Give', 'pena' ),
			'slug'      => 'give',
			'required'  => false,
		),
		// Email Subscribers & Newsletter
		array(
			'name'      => esc_html__( 'Email Subscribers & Newsletter', 'pena' ),
			'slug'      => 'email-subscribers',
			'required'  => false,
		),
		// Page Builder by SiteOrigin
		array(
			'name'      => esc_html__( 'Page Builder by SiteOrigin', 'pena' ),
			'slug'      => 'siteorigin-panels',
			'required'  => false,
		),
		// The Events Calendar
		array(
			'name'      => esc_html__( 'The Events Calendar', 'pena' ),
			'slug'      => 'the-events-calendar',
			'required'  => false,
		),
		// Widget Visibility
		array(
			'name'      => esc_html__( 'Widget Visibility Without Jetpack', 'pena' ),
			'slug'      => 'widget-visibility-without-jetpack',
			'required'  => false,
		),
		// SiteOrigin Widgets Bundle
		array(
			'name'      => esc_html__( 'SiteOrigin Widgets Bundle', 'pena' ),
			'slug'      => 'so-widgets-bundle',
			'required'  => false,
		),
		// Soliloquy Slider
		array(
			'name'      => esc_html__( 'Slider by Soliloquy', 'pena' ),
			'slug'      => 'soliloquy-lite',
			'required'  => false,
		),
		// Pena Custom Widgets For SiteOrigin Page Builder
		array(
			'name'         => esc_html__( 'Pena Custom Widgets For SiteOrigin Page Builder', 'pena' ), // The plugin name.
			'slug'         => 'pena-siteorigin-custom-widgets', // The plugin slug (typically the folder name).
			'source'       => esc_url('https://github.com/anarieldesign/pena-siteorigin-custom-widgets/archive/master.zip' ), // The plugin source.
			'required'     => false, // If false, the plugin is only 'recommended' instead of required.
			'external_url' => esc_url( 'https://github.com/anarieldesign/pena-siteorigin-custom-widgets' ), // If set, overrides default API URL and points to an external URL.
		),
);
		$config = array(
		'id'           => 'pena',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
		);
	tgmpa( $plugins, $config );
}

/**
 * One Click Demo Import
 */
function pena_ocdi_import_files() {
	return array(
		array(
			'import_file_name'           => esc_html__( 'Demo Import', 'pena' ),
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demo/pena-demo-content.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demo/pena-widgets.json',
			'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'inc/demo/pena-customizer.dat',
		),
	);
}
add_filter( 'pt-ocdi/import_files', 'pena_ocdi_import_files' );
function pena_ocdi_after_import_setup() {
	// Assign menus to their locations.
	$main_menu = get_term_by( 'name', 'Primary Menu', 'nav_menu' );
	$social_menu = get_term_by( 'name', 'Social Menu', 'nav_menu' );

	set_theme_mod( 'nav_menu_locations', array(
		'primary' => $main_menu->term_id,
		'social' => $social_menu->term_id,
		)
	);

	// Assign front page and posts page (blog page).
	$front_page_id = get_page_by_title (esc_html__( 'Home', 'pena' ));
	$blog_page_id  = get_page_by_title (esc_html__( 'Blog', 'pena' ));

	update_option( 'show_on_front', 'page' );
	update_option( 'page_on_front', $front_page_id->ID );
	update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'pena_ocdi_after_import_setup' );
function pena_ocdi_plugin_intro_notice ( $default_text ) {
	return wp_kses_post( str_replace ( 'Before you begin, make sure all the required plugins are activated.', esc_html__( 'Before you begin, make sure all the recommended plugins are activated.', 'pena'), $default_text ) );
}
add_filter( 'pt-ocdi/plugin_intro_text', 'pena_ocdi_plugin_intro_notice' );