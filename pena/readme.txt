= Pena =

* by Anariel Design, http://www.anarieldesign.com

- this theme is based on Underscores: http://underscores.me/ and Libre: https://theme.wordpress.com/themes/libre/
License: Distributed under the terms of the GNU GPL
Copyright: Automattic, automattic.com

Pena is distributed under the terms of the GNU GPL

UPDATES

1.1.5 - May 8, 2018
- small fix for the class-tgm-plugin-activation.php file

1.1.4 - February 19, 2018

- small fix for the excerpt link

1.1.3 - February 6, 2018

- added more options for the Get Involved page in the Customizer - number of columns for the child pages
- small fix for the front page Customizer columns option

1.1.2 - January 25, 2018

- one-click demo files added to the theme

1.1.1 - January 8, 2017

- added basic support for the Gutenberg

1.1.0 - November 7, 2017

- added new color options inside the Customizer
- added Image Options inside the Customizer - option to convert images to black and white with just one-click
- added One Click Demo Import
- small fix for the flexbox in IE11

1.0.9 - July 14, 2017

- small fix for the Pena Recent Post widget - widgets.php file

1.0.8 - July 12, 2017

- added new option on the blog and single blog page - disable featured image 
- added post thumbnails image sizes - better website performance
- front page “First Content Block” - disable child page featured image and title link from the Customizer


1.0.7 - April 03, 2017
- added new features inside the Customizer - front page options - added caption background color, child page columns
- small fixes inside the style.css file 

1.0.6 - December 29, 2016
- small fix inside the style.css - front page, when sidebar isn’t active recent posts block expand to full width

1.0.5 - December 8, 2016
- from WordPress 4.7 Customizer “Custom CSS” section migrated to the core Customizer section called “Additional CSS” - changes inside the inc folder “customizer.php” file
- small fix inside the style.css for the front page with slider

Update October 4, 2016
- small fix inside the style.css for the action buttons

Update September 29, 2016
- added wpml-config.xml file - theme is fully compatible with the WPML plugin

Update July 28, 2016
- changes inside the “template-parts” & “inc” folders - new feature added inside the Customizer. For the About Page it is now possible to choose number of columns for the Second Content Block
- small fixes inside the style.css for the “Pena: Call to Action” widget
- custom Google Fonts plugin is added to the “plugins” folder inside the main download folder

Update June 13, 2016
- added core theme logo support instead of the custom one (with the latest WordPress version) - with this update please add your logo one more time inside the Customizer - Site Identity - Logo 
- few fixes inside the functions.php file and clearing code inside the “template-parts” folder
- fixed typo inside the customizer.php file
- fixed tags inside the style.css - few tags are deprecated with the latest WordPress version


