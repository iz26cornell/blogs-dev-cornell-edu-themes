<?php

/***** Theme Info Page *****/

if (!function_exists('pena_theme_info_page')) {
	function pena_theme_info_page() {
		add_theme_page(esc_html__('Welcome to Pena', 'pena'), esc_html__('Theme Info', 'pena'), 'edit_theme_options', 'blog', 'pena_display_theme_page');
	}
}
add_action('admin_menu', 'pena_theme_info_page');

if (!function_exists('pena_display_theme_page')) {
	function pena_display_theme_page() {
		$theme_data = wp_get_theme(); ?>
		<div class="theme-info-wrap">
			<h1>
				<?php printf(esc_html__('Welcome to %1s %2s', 'pena'), $theme_data->Name, $theme_data->Version); ?>
			</h1>

			<p>
				<a href="<?php echo esc_url('http://www.anarieldesign.com/themes/pena-charity-wordpress-theme/'); ?>" target="_blank" class="button button-primary">
					<?php esc_html_e('Find more about Pena', 'pena'); ?>
				</a>
			</p>
		<div class="ad-row clearfix">
			<div class="ad-col-1-2">
				<div class="section">
					<div class="theme-description">
						<?php echo esc_html($theme_data['Description']); ?>
					</div>
				</div>
			</div>
			<div class="ad-col-1-2">
				<img src="<?php echo get_template_directory_uri(); ?>/screenshot.png" alt="<?php esc_html_e('Theme Screenshot', 'pena'); ?>" />
			</div></div>
			<hr>
			<div id="getting-started" class="bg">
				<h3>
					<?php printf(esc_html__('Getting Started with %s', 'pena'), $theme_data->Name); ?>
				</h3>
				<div class="ad-row clearfix">
						<div class="section documentation">
							<h4>
								<?php esc_html_e('Theme Documentation', 'pena'); ?>
							</h4>
							<p class="about">
								<?php printf(esc_html__('Please check the documentation to get better overview of how the theme is structured.', 'pena'), $theme_data->Name); ?>
							</p>
							<p>
								<a href="<?php echo esc_url('http://www.anarieldesign.com/documentation/pena/'); ?>" target="_blank" class="button button-primary">
									<?php esc_html_e('Visit Documentation', 'pena'); ?>
								</a>
							</p>
						</div>
						<div class="section options">
							<h4>
								<?php esc_html_e('Theme Options', 'pena'); ?>
							</h4>
							<p class="about">
								<?php printf(esc_html__('Click "Customize" to open the Customizer. Pena has implemented Customizer and added some useful options to help you style theme background, color elements, upload image logo, to choose different blog layouts and a lot more.',  'pena'), $theme_data->Name); ?>
							</p>
							<p>
								<a href="<?php echo admin_url('customize.php'); ?>" class="button button-secondary">
									<?php esc_html_e('Customize', 'pena'); ?>
								</a>
							</p>
						</div>
						<div class="section video">
							<h4>
								<?php esc_html_e('Pena Video Presentation', 'pena'); ?>
							</h4>
							<p>
								<a href="<?php echo esc_url('https://www.youtube.com/watch?v=O6UyKaxwC2Y'); ?>" class="button button-primary" target="_blank">
									<?php esc_html_e('Video Presentation', 'pena'); ?>
								</a>
							</p>
						</div>
						<div class="section recommend clear">
							<h4>
								<?php esc_html_e('Recommended Plugins', 'pena'); ?>
							</h4>
							<p class="center"><?php esc_html_e('Plugins listed are not mandatory for theme to work! Install only the ones you need for your website!', 'pena'); ?></p>
							<!-- Give -->
							<div class="pena-tab-pane-half pena-tab-pane-first-half">
							<p><strong><?php esc_html_e( 'Give', 'pena' ); ?></strong></p>
							<p><?php esc_html_e( 'The most robust, flexible, and intuitive way to accept donations on WordPress.', 'pena' ); ?></p>

							<?php if ( is_plugin_active( 'give/give.php' ) ) { ?>

							<p><span class="pena-w-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p><a href="<?php echo esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=give' ), 'install-plugin_give' ) ); ?>" class="button button-primary"><?php esc_html_e( 'Install Give', 'pena' ); ?></a></p>

							<?php
							}

							?>
							<!-- WooCommerce -->
							<p><strong><?php esc_html_e( 'WooCommerce', 'pena' ); ?></strong></p>
							<p><?php esc_html_e( 'An e-commerce toolkit that helps you sell anything. Beautifully.', 'pena' ); ?></p>

							<?php if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) { ?>

							<p><span class="pena-w-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p><a href="<?php echo esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=woocommerce' ), 'install-plugin_woocommerce' ) ); ?>" class="button button-primary"><?php esc_html_e( 'Install WooCommerce', 'pena' ); ?></a></p>

							<?php
							}

							?>
							<!-- Jetpack -->
							<p><strong><?php esc_html_e( 'Contact Form 7', 'pena' ); ?></strong></p>
							<p><?php esc_html_e( 'Just another contact form plugin. Simple but flexible.', 'pena' ); ?></p>

							<?php if ( is_plugin_active( 'contact-form-7/contact-form-7.php' ) ) { ?>

							<p><span class="pena-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p><a href="<?php echo esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=contact-form-7' ), 'install-plugin_contact-form-7' ) ); ?>" class="button button-primary"><?php esc_html_e( 'Install Contact Form 7', 'pena' ); ?></a></p>

							<?php
							}

							?>
							<!-- Widget Visibility Without Jetpack -->
							<p><strong><?php esc_html_e( 'Widget Visibility Without Jetpack', 'pena' ); ?></strong></p>
							<p><?php esc_html_e( 'Control what pages your widgets appear on. Based on Widget Visibility module, from Jetpack plugin, http://wordpress.org/plugins/jetpack/.', 'pena' ); ?></p>

							<?php if ( is_plugin_active( 'widget-visibility-without-jetpack/widget-visibility-without-jetpack.php' ) ) { ?>

							<p><span class="pena-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p><a href="<?php echo esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=widget-visibility-without-jetpack' ), 'install-plugin_widget-visibility-without-jetpack' ) ); ?>" class="button button-primary"><?php esc_html_e( 'Install Widget Visibility Without Jetpack', 'pena' ); ?></a></p>

							<?php
							}

							?>
							<!-- The Events Calendar -->
							<p><strong><?php esc_html_e( 'The Events Calendar', 'pena' ); ?></strong></p>
							<p><?php esc_html_e( 'The Events Calendar is a carefully crafted, extensible plugin that lets you easily share your events. Beautiful. Solid. Awesome.', 'pena' ); ?></p>

							<?php if ( is_plugin_active( 'the-events-calendar/the-events-calendar.php' ) ) { ?>

							<p><span class="pena-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p><a href="<?php echo esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=the-events-calendar' ), 'install-plugin_the-events-calendar' ) ); ?>" class="button button-primary"><?php esc_html_e( 'Install The Events Calendar', 'pena' ); ?></a></p>

							<?php
							}

							?>
							</div>

							<div class="pena-tab-pane-half">

							<!-- Page Builder by SiteOrigin -->
							<p><strong><?php esc_html_e( 'Page Builder by SiteOrigin', 'pena' ); ?></strong></p>
							<p><?php esc_html_e( 'A drag and drop, responsive page builder that simplifies building your website.', 'pena' ); ?></p>

							<?php if ( is_plugin_active( 'siteorigin-panels/siteorigin-panels.php' ) ) { ?>

							<p><span class="pena-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p><a href="<?php echo esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=siteorigin-panels' ), 'install-plugin_siteorigin-panels' ) ); ?>" class="button button-primary"><?php esc_html_e( 'Install Page Builder by SiteOrigin', 'pena' ); ?></a></p>

							<?php
							}

							?>

							<!-- SiteOrigin Widgets Bundle -->
							<p><strong><?php esc_html_e( 'SiteOrigin Widgets Bundle', 'pena' ); ?></strong></p>
							<p><?php esc_html_e( 'A collection of all widgets, neatly bundled into a single plugin.', 'pena' ); ?></p>

							<?php if ( is_plugin_active( 'so-widgets-bundle/so-widgets-bundle.php' ) ) { ?>

							<p><span class="pena-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p><a href="<?php echo esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=so-widgets-bundle' ), 'install-plugin_so-widgets-bundle' ) ); ?>" class="button button-primary"><?php esc_html_e( 'SiteOrigin Widgets Bundle', 'pena' ); ?></a></p>

							<?php
							}

							?>
							<!-- Premium Soliloquy Slider -->
							<p><strong><?php esc_html_e( 'Premium Soliloquy Slider', 'pena' ); ?></strong></p>

							<?php if ( is_plugin_active( 'soliloquy/soliloquy.php' ) ) { ?>

							<p><span class="pena-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p class="bg2"><?php esc_html_e( 'Plugin & license key can be found inside the plugins folder within the main folder you downloaded', 'pena' ); ?></p>

							<?php
							}
							?>
							<!-- Custom Google Fonts Plugin -->
							<p><strong><?php esc_html_e( 'Custom Google Fonts Plugin', 'pena' ); ?></strong></p>

							<?php if ( is_plugin_active( 'AnarielDesign-GoogleFonts/ad_gfp.php' ) ) { ?>

							<p><span class="pena-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p class="bg2"><?php esc_html_e( 'Plugin can be found inside the plugins folder within the main folder you downloaded', 'pena' ); ?></p>

							<?php
							}
							?>
							<!-- Pena Custom Widgets for SiteOrigin Page Builder Plugin -->
							<p><strong><?php esc_html_e( 'Custom Widgets for SiteOrigin Page Builder Plugin', 'pena' ); ?></strong></p>

							<?php if ( is_plugin_active( 'pena-siteorigin-customwidgets/pena-siteorigin-customwidget.php' ) ) { ?>

							<p><span class="pena-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p class="bg2"><?php esc_html_e( 'Plugin can be found inside the plugins folder within the main folder you downloaded', 'pena' ); ?></p>

							<?php
							}
							?>
							<!-- Email Subscribers & Newsletter -->
							<p><strong><?php esc_html_e( 'Email Subscribers & Newsletters', 'pena' ); ?></strong></p>
							<p><?php esc_html_e( 'Add subscription forms on website, send HTML newsletters & automatically notify subscribers about new blog posts once it is published.', 'pena' ); ?></p>

							<?php if ( is_plugin_active( 'email-subscribers/email-subscribers.php' ) ) { ?>

							<p><span class="pena-activated button"><?php esc_html_e( 'Already activated', 'pena' ); ?></span></p>

							<?php
							}
							else { ?>

							<p><a href="<?php echo esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=email-subscribers' ), 'install-plugin_email-subscribers' ) ); ?>" class="button button-primary"><?php esc_html_e( 'Email Subscribers & Newsletters', 'pena' ); ?></a></p>

							<?php
							}

							?>
							</div>
						</div>
						<div class="clear"></div>
						<div class="section bg1">
							<h3>
								<?php esc_html_e('More Themes by Anariel Design', 'pena'); ?>
							</h3>
							<p class="about">
								<?php printf(esc_html__('Build Your Dream WordPress Site with Premium Niche Themes for Bloggers & Charities',  'pena'), $theme_data->Name); ?>
							</p>
							<a target="_blank" href="<?php echo esc_url('http://www.anarieldesign.com/themes/'); ?>"><img src="http://www.anarieldesign.com/themedemos/marketimages/anarieldesign-themes.jpg" alt="<?php esc_html_e('Theme Screenshot', 'pena'); ?>" /></a>
							<p>
								<a target="_blank" href="<?php echo esc_url('http://www.anarieldesign.com/themes/'); ?>" class="button button-primary advertising">
									<?php esc_html_e('More Themes', 'pena'); ?>
								</a>
							</p>
						</div>
					</div>
			</div>
			<hr>
			<div id="theme-author">
				<p>
					<?php printf(esc_html__('%1s is proudly brought to you by %2s. %3s: %4s.', 'pena'), $theme_data->Name, '<a target="_blank" href="http://www.anarieldesign.com/" title="Anariel Design">Anariel Design</a>', $theme_data->Name, '<a target="_blank" href="http://www.anarieldesign.com/themes/pena-charity-wordpress-theme/" title="Pena Theme Demo">' . esc_html__('Theme Demo', 'pena') . '</a>'); ?>
				</p>
			</div>
		</div><?php
	}
}

?>