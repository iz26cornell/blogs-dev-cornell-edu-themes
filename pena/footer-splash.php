<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Pena
 */

?>
	<?php if ( has_nav_menu( 'social' ) ) : ?>
	<div class="social-block">
		<nav id="social-navigation" class="social-navigation" role="navigation">
			<?php
				// Social links navigation menu.
				wp_nav_menu( array(
					'theme_location' => 'social',
					'depth'		  => 1,
					'link_before'	=> '<span class="screen-reader-text">',
					'link_after'	 => '</span>',
				) );
			?>
		</nav><!-- .social-navigation -->
	</div>
	<?php endif; ?>

<?php wp_footer(); ?>

</body>
</html>