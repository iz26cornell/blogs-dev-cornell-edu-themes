<?php
/**
 * The template for displaying all single posts.
 *
 * @package Pena
 */

get_header(); ?>

<div class="hfeed site single">
	<div class="content site-content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<?php give_get_template_part( 'single-give-form/content', 'single-give-form' );?>


				<?php the_post_navigation( array( 'next_text' => wp_kses( __( '<span class="meta-nav">Next Post</span> %title', 'pena' ), array( 'span' => array( 'class' => array() ) ) ), 'prev_text' => wp_kses( __( '<span class="meta-nav">Previous Post</span> %title', 'pena' ), array( 'span' => array( 'class' => array() ) ) ) ) ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- #primary -->

	</div><!-- .site-content -->
</div><!-- .site -->

<?php get_footer(); ?>