<?php
/**
 * Template part for displaying posts.
 *
 * @package Pena
 */

?>

<div class="twocolumn clear">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">

			<?php if ( has_post_thumbnail() ) { ?>
			<div class="post-thumbnail">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail(); ?>
				</a>
			</div>
			<?php } ?>

			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

			<?php if( ! get_theme_mod( 'pena_post_footer' ) ) : ?>
				<?php if ( 'post' == get_post_type() ) : ?>
					<div class="entry-meta">
						<?php pena_posted_on(); ?>
					</div><!-- .entry-meta -->
				<?php endif; ?>
			<?php endif; ?>

		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php
				/* translators: %s: Name of current post */
				the_excerpt();
			?>

			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pena' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
	</article><!-- #post-## -->
</div>