<?php
/**
 * The template used for displaying child page content
 *
 * @package Pena
 * @since Pena 1.0
 */
?>
	<div class="block-five">
	<?php $image_id = get_post_thumbnail_id(); ?>
	<?php $image_url = wp_get_attachment_image_src( $image_id,'full' );?>
	<div class="header section fixed" style="background-image:url(<?php echo esc_url( $image_url[0] ); ?>);"><div class="overlay"></div>
		<div class="hfeed site">
			<div class="content site-content">
				<div class="content-area">
					<div class="main site-main" role="main">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php
									if ( is_single() ) :
									the_title( '<h1 class="entry-title">', '</h1>' );
								else :
									the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' );
								endif;
								?>
								<?php
									/* translators: %s: Name of current post */
									the_content( sprintf(
									wp_kses( __( 'Continue reading %s', 'pena' ), array( 'span' => array( 'class' => array() ) ) ),
									the_title( '<span class="screen-reader-text">"', '"</span>', false )
									) );
								?>
								<?php
									wp_link_pages( array(
										'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'pena' ) . '</span>',
										'after'       => '</div>',
										'link_before' => '<span>',
										'link_after'  => '</span>',
										'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'pena' ) . ' </span>%',
										'separator'   => '<span class="screen-reader-text">, </span>',
									) );
								?>
							</div><!-- .entry-content -->
						</article><!-- #post-## -->
					</div><!-- .site-main -->
				</div><!-- .content-area -->
			</div><!-- .site-content -->
		</div><!-- .site -->
	</div><!-- .header -->
	</div><!-- .block-five -->
		<div class="hfeed site causes">
			<div class="content site-content">
				<div class="content-area">
					<div class="main site-main" role="main">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="posts child-pages columns clear">
								<?php
									$child_pages = new WP_Query( array(
										'post_type'      => 'page',
										'orderby'        => 'menu_order',
										'order'          => 'ASC',
										'post_parent'    => $post->ID,
										'posts_per_page' => 999,
										'no_found_rows'  => true,
									) );
									while ( $child_pages->have_posts() ) : $child_pages->the_post();
										 get_template_part( 'template-parts/content', 'grid-four-involved-page' );
									endwhile;
									wp_reset_postdata();
								?>
							</div><!-- .child-pages -->
						</article><!-- #post-## -->
					</div><!-- .site-main -->
				</div><!-- .content-area -->
			</div><!-- .site-content -->
		</div><!-- .site -->