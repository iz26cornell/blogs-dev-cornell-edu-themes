<?php
/**
 * Template part for displaying posts.
 *
 * @package Pena
 */

?>

<div class="twocolumn clear">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">

			<?php if(!get_theme_mod('pena_main_featured_image')) : ?>
			<?php if ( has_post_thumbnail() ) { ?>
			<div class="post-thumbnail">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( 'pena-post-grid-two-thumbnail' ); ?>
				</a>
			</div>
			<?php } ?>
			<?php endif; ?>

			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

			<?php if( ! get_theme_mod( 'pena_post_footer' ) ) : ?>
				<?php if ( 'post' == get_post_type() ) : ?>
					<div class="entry-meta">
						<?php pena_posted_on(); ?>
					</div><!-- .entry-meta -->
				<?php endif; ?>
			<?php endif; ?>

		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php if(get_theme_mod('pena_post_type') == 'excerpt-lenght') : ?>
			<?php
				/* translators: %s: Name of current post */
				the_excerpt();
			?>
			<?php else: ?>
			<?php
				/* translators: %s: Name of current post */
				the_content( sprintf(
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'pena' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
			?>
			<?php endif; ?>

			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pena' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
	</article><!-- #post-## -->
</div>