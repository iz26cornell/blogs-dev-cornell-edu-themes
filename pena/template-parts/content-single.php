<?php
/**
 * Template part for displaying single posts.
 *
 * @package Pena
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<?php if(!get_theme_mod('pena_single_featured_image')) : ?>
		<?php if ( has_post_thumbnail() ) { ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'pena-post-thumbnail' ); ?>
			</a>
		</div>
		<?php } ?>
		<?php endif; ?>

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<?php if( ! get_theme_mod( 'pena_post_footer' ) ) : ?>
		<div class="entry-meta">
			<?php pena_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pena' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php if( ! get_theme_mod( 'pena_post_footer' ) ) : ?>
	<footer class="entry-footer">
		<?php pena_entry_footer(); ?>
	</footer><!-- .entry-footer -->
	<?php endif; ?>

	<?php if( ! get_theme_mod( 'pena_author_bio' ) ) : ?>
	<?php get_template_part( 'author-bio' );?>
	<?php endif; ?>
</article><!-- #post-## -->

