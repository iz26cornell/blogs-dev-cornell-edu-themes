<?php
/**
 * The template used for displaying child page content
 *
 * @package Pena
 * @since Pena 1.0
 */
?>
	<?php if ( has_post_thumbnail() ): ?>
	<?php $image_id = get_post_thumbnail_id(); ?>
	<?php $image_url = wp_get_attachment_image_src( $image_id,'full' );?>
	<div class="info_pic" style="background-image:url(<?php echo $image_url[0]; ?>);">
	<div class="hfeed site info_details">
		<div class="content site-content">
			<div class="content-area">
				<div class="main site-main" role="main">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="entry-content">
							<div class="entry-inner">
								<?php the_title( '<h3>', '</h3>' ); ?>
								<?php
									/* translators: %s: Name of current post */
										the_content( sprintf(
											wp_kses( __( 'Continue reading %s', 'pena' ), array( 'span' => array( 'class' => array() ) ) ),
											the_title( '<span class="screen-reader-text">"', '"</span>', false )
										) );
								?>
								<?php
									wp_link_pages( array(
										'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'pena' ) . '</span>',
										'after'       => '</div>',
										'link_before' => '<span>',
										'link_after'  => '</span>',
										'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'pena' ) . ' </span>%',
										'separator'   => '<span class="screen-reader-text">, </span>',
									) );
								?>
							</div>
						</div><!-- .entry-content -->
					</article><!-- #post-## -->
				</div><!-- .site-main -->
			</div><!-- .content-area -->
		</div><!-- .site-content -->
		</div>
	</div><!-- .site -->
	<?php else: ?>
	<div class="hfeed site info_details full">
		<div class="content site-content">
			<div class="content-area">
				<div class="main site-main" role="main">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="entry-content">
							<div class="entry-inner">
								<?php the_title( '<h3>', '</h3>' ); ?>
								<?php
									/* translators: %s: Name of current post */
										the_content( sprintf(
											wp_kses( __( 'Continue reading %s', 'pena' ), array( 'span' => array( 'class' => array() ) ) ),
											the_title( '<span class="screen-reader-text">"', '"</span>', false )
										) );
								?>
								<?php
									wp_link_pages( array(
										'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'pena' ) . '</span>',
										'after'       => '</div>',
										'link_before' => '<span>',
										'link_after'  => '</span>',
										'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'pena' ) . ' </span>%',
										'separator'   => '<span class="screen-reader-text">, </span>',
									) );
								?>
							</div>
						</div><!-- .entry-content -->
					</article><!-- #post-## -->
				</div><!-- .site-main -->
			</div><!-- .content-area -->
		</div><!-- .site-content -->
	</div><!-- .site -->
	<?php endif; ?>