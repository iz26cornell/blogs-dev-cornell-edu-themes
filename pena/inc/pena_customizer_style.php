<?php
//////////////////////////////////////////////////////////////////
// Customizer - Add CSS
//////////////////////////////////////////////////////////////////
function pena_customizer_css() {
	?>
	<style type="text/css">
		body, .inner-page { background:<?php echo get_theme_mod( 'pena_bg_color' ); ?>; }
		
		.search-toggle, .main-navigation li.blue a, .site-footer a.button, a.blue, .jetpack_subscription_widget input[type="submit"], #comments input[type="submit"], .entry-content a.button, #promo a, .site-footer.custom #promo a,
		.info, .info .content.site-content, .info .entry-content a.button, .info .entry-content a.more-link, .page .soliloquy-container.soliloquy-theme-karisma .soliloquy-controls-direction a:hover, .page .soliloquy-container.soliloquy-theme-karisma .soliloquy-controls-auto-item:hover, 
		.page .soliloquy-container.soliloquy-theme-karisma .soliloquy-controls-auto-item a:hover, .block-five a.blue, .site-footer input.give-submit, input.give-submit, #give-purchase-gravatars .give-gravatars-title, [id*=give-form].give-display-modal .give-btn, 
		[id*=give-form].give-display-reveal .give-btn, input#give_login_submit, #give-register-form .button, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button,.woocommerce #respond input#submit.alt, 
		.woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .contact-form input[type="submit"], .top .soliloquy-container.soliloquy-theme-karisma .soliloquy-controls-direction a:hover, 
		.top .soliloquy-container.soliloquy-theme-karisma .soliloquy-controls-auto-item a:hover, #main .soliloquy-container.soliloquy-theme-karisma .soliloquy-controls-direction a:hover, #main .soliloquy-container.soliloquy-theme-karisma .soliloquy-controls-auto-item a:hover,
		#page .soliloquy-container.soliloquy-theme-karisma .soliloquy-controls-direction a:hover, #page .soliloquy-container.soliloquy-theme-karisma .soliloquy-controls-auto-item a:hover, .es_widget_form input[type="button"], .wpcf7 input[type="submit"] { background:<?php echo get_theme_mod( 'pena_blue_colors' ); ?>; }
		.woocommerce span.onsale, .woocommerce .widget_price_filter .ui-slider .ui-slider-handle, .woocommerce .widget_price_filter .ui-slider .ui-slider-range, #tribe-events .tribe-events-button, #tribe-events .tribe-events-button:hover, 
		#tribe_events_filters_wrapper input[type=submit], .tribe-events-button, .tribe-events-button.tribe-active:hover, .tribe-events-button.tribe-inactive, .tribe-events-button:hover, .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-], 
		.tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]>a { background-color:<?php echo get_theme_mod( 'pena_blue_colors' ); ?>; }
		span.blue, .site-footer .give-goal-progress .income, .give-goal-progress .income, .woocommerce .woocommerce-message:before, .woocommerce .woocommerce-info:before, .woocommerce .star-rating span:before { color:<?php echo get_theme_mod( 'pena_blue_colors' ); ?>; }
		.call-to-action .one-fourth:nth-child(1), .call-to-action .one-fourth:nth-child(4), .call-to-action .one-third:nth-child(1), .page .soliloquy-container.soliloquy-theme-karisma .soliloquy-caption-inside a, 
		.page .soliloquy-container.soliloquy-theme-karisma .soliloquy-caption-inside .soliloquy-fc-title-link, .page .soliloquy-container.soliloquy-theme-karisma .soliloquy-caption-inside .soliloquy-fc-read-more,
		.page .soliloquy-container a { border-bottom-color:<?php echo get_theme_mod( 'pena_blue_colors' ); ?>; }
		.woocommerce .woocommerce-message, .woocommerce .woocommerce-info { border-top-color:<?php echo get_theme_mod( 'pena_blue_colors' ); ?>; }
		@media screen and ( min-width: 55em ) {
		.search-toggle { background:<?php echo get_theme_mod( 'pena_blue_colors' ); ?>; }
		}
		@media screen and (min-width: 80em) {
		.info .content.site-content {
		background: none;
		}
		}
		.social-navigation a:before, a.lila, .content-caption .entry-content .button, .block-five a.lila { background:<?php echo get_theme_mod( 'pena_purple_colors' ); ?>; }
		.call-to-action .one-fourth:nth-child(3), .call-to-action .one-third:nth-child(3) { border-bottom-color:<?php echo get_theme_mod( 'pena_purple_colors' ); ?>; }
		span.lila, .donation-form:before, #give-recurring-form .give-required-indicator, form.give-form .give-required-indicator, form[id*=give-form] .give-required-indicator { color:<?php echo get_theme_mod( 'pena_purple_colors' ); ?>; }
		@media screen and ( min-width: 80em ) {
		.content-caption .entry-content .button { background:<?php echo get_theme_mod( 'pena_purple_colors' ); ?>; }
		}
		
		hr, .site-footer .widget_archive a, .site-footer .widget_categories a, hr.short, .site-footer form[id*=give-form] .give-donation-amount .give-currency-symbol, .site-footer form[id*=give-form] #give-final-total-wrap .give-donation-total-label,
		.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content, .tribe-events-calendar thead th { background-color:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
		form.search-form, .footer-widgets.clear, .site-footer.custom #promo, .widgetized-content .widget-title, .call-to-action .one-fourth:nth-child(2), .call-to-action .one-third:nth-child(2), .block-one .child-pages h2 a:hover, .block-three .child-pages h2 a:hover, 
		.site-footer form[id*=give-form] .give-donation-amount .give-currency-symbol, .site-footer form[id*=give-form] #give-final-total-wrap .give-donation-total-label, .site-footer #give-recurring-form h3.give-section-break, .site-footer #give-recurring-form h4.give-section-break, 
		.site-footer #give-recurring-form legend, .site-footer form.give-form h3.give-section-break, .site-footer form.give-form h4.give-section-break, .site-footer form.give-form legend, .site-footer form[id*=give-form] h3.give-section-break, .site-footer form[id*=give-form] h4.give-section-break, 
		.site-footer form[id*=give-form] legend, .woocommerce ul.products li.product .price, .woocommerce div.product p.price, .woocommerce div.product span.price, .woocommerce-checkout #payment ul.payment_methods,
		.search input.search-submit, .error404 input.search-submit, .textwidget a:hover, .textwidget a:focus, .textwidget a:active, .entry-content a:hover, .entry-content a:focus, .entry-content a:active, .comment-content a:hover,
		.comment-content a:focus, .comment-content a:active, .page-template-about-template .entry-content a { border-bottom-color:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
		.footer-widgets.clear, .site-footer.custom #promo, #main div.sharedaddy h3.sd-title:before, #main #jp-relatedposts h3.jp-relatedposts-headline em:before, .site-footer form[id*=give-form] .give-donation-amount .give-currency-symbol,
		.site-footer form[id*=give-form] #give-final-total-wrap .give-donation-total-label, .woocommerce div.product .woocommerce-tabs ul.tabs li, .woocommerce #content div.product .woocommerce-tabs ul.tabs li { border-top-color:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
		.site-footer form[id*=give-form] .give-donation-amount .give-currency-symbol.give-currency-position-before, .woocommerce div.product .woocommerce-tabs ul.tabs li, .woocommerce #content div.product .woocommerce-tabs ul.tabs li { border-left-color:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
		.woocommerce div.product .woocommerce-tabs ul.tabs li, .woocommerce #content div.product .woocommerce-tabs ul.tabs li { border-right-color:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
		.site-footer input[type="text"], .site-footer input[type="email"], .site-footer input[type="url"], .site-footer input[type="password"], .site-footer input[type="search"], .fixed-menu .site-header, .site-header,
		.site-header.float-header, .fixed-menu .toggled .menu-primary-menu-container, .main-navigation li.blue a:hover, .social-block, .site-footer .widget .post-count, .blog .list-layout .entry-meta span.posted-on, 
		.single .list-layout.entry-meta span.posted-on, .search .list-layout .entry-meta span.posted-on, .archive .list-layout.entry-meta span.posted-on, .site-footer, a.blue:hover, a.lila:hover, .button, .jetpack_subscription_widget input[type="submit"]:hover,
		#comments input[type="submit"]:hover, .entry-content a.button:hover, #promo, #promo a:hover, .homepage, 
		 body.page-template-splash-template, .site-footer input.give-submit:hover, .site-footer .give-progress-bar, .site-footer form.floated-labels .floatlabel input, 
		.site-footer form.floated-labels .floatlabel select, .site-footer form.floated-labels .floatlabel.is-active input, .site-footer form.floated-labels .floatlabel.is-active select, form.floated-labels .floatlabel.is-active textarea,
		input.give-submit:hover, .give-progress-bar, input#give_login_submit:hover, #give-register-form .button:hover, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover,
		.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, #tribe-bar-form .tribe-bar-submit input[type=submit], .contact-form input[type="submit"]:hover,
		#page .soliloquy-container .soliloquy-caption .soliloquy-caption-inside,.info .entry-content a.button:hover, .info .entry-content a.more-link:hover, .block-five a.blue:hover, .block-five a.lila:hover, .site-footer .jetpack_subscription_widget #subscribe-email input[type="email"], .es_widget_form input[type="button"]:hover, .wpcf7 input[type="submit"]:hover { background:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
		.site-footer button, .site-footer input[type="button"], .site-footer input[type="reset"], .site-footer #main #infinite-handle span, .site-footer input[type="text"], .site-footer input[type="email"], .site-footer input[type="url"], .site-footer input[type="password"], .site-footer input[type="search"],
		.site-footer .search-box input[type="search"], .site-footer .error404 input[type="search"], .widget_calendar td, .widget_calendar th, .widget_calendar caption, .tagcloud a, .info .entry-content a.button, .info .entry-content a.more-link,
		.site-footer form[id*=give-form] .give-donation-amount #give-amount, .site-footer form[id*=give-form] .give-donation-amount #give-amount-text, .site-footer form[id*=give-form] #give-final-total-wrap .give-final-total-amount,
		.site-footer form[id*=give-form] #give-final-total-wrap .give-donation-total-label, .site-footer form.floated-labels .floatlabel input, .site-footer form.floated-labels .floatlabel select, .site-footer form.floated-labels .floatlabel.is-active input, 
		.site-footer form.floated-labels .floatlabel.is-active select, form.floated-labels .floatlabel.is-active textarea, .woocommerce div.product .woocommerce-tabs .panel, .woocommerce-checkout #payment, .woocommerce .quantity .qty,
		.woocommerce .woocommerce-ordering select, .site-footer input[type="text"], .site-footer input[type="email"], .site-footer input[type="url"], .site-footer input[type="password"], .site-footer input[type="search"], .site-footer .jetpack_subscription_widget #subscribe-email input[type="email"], table, th, td { border-color:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
		.site-footer button, .site-footer input[type="button"], .site-footer input[type="reset"], .site-footer #main #infinite-handle span, input[type="search"], .site-footer input[type="text"], .site-footer input[type="email"], .site-footer input[type="url"], .site-footer input[type="password"], .site-footer input[type="search"],
		.site-footer .search-box input[type="search"], .site-footer .error404 input[type="search"], .site-footer input[type="text"], .site-footer input[type="email"], .site-footer input[type="url"], .site-footer input[type="password"], .site-footer input[type="search"], .site-footer .jetpack_subscription_widget #subscribe-email input[type="email"] { box-shadow:<?php echo get_theme_mod( 'pena_black_colors' ); ?> 0 2px 0; }
		.site-footer button, .site-footer input[type="button"], .site-footer input[type="reset"], .site-footer #main #infinite-handle span, input[type="search"], .site-footer input[type="text"], .site-footer input[type="email"], .site-footer input[type="url"], .site-footer input[type="password"], .site-footer input[type="search"],
		.site-footer .search-box input[type="search"], .site-footer .error404 input[type="search"], .site-footer input[type="text"], .site-footer input[type="email"], .site-footer input[type="url"], .site-footer input[type="password"], .site-footer input[type="search"], .site-footer .jetpack_subscription_widget #subscribe-email input[type="email"] { -webkit-box-shadow:<?php echo get_theme_mod( 'pena_black_colors' ); ?> 0 2px 0; }
		.site-footer button, .site-footer input[type="button"], .site-footer input[type="reset"], .site-footer #main #infinite-handle span, input[type="search"], .site-footer input[type="text"], .site-footer input[type="email"], .site-footer input[type="url"], .site-footer input[type="password"], .site-footer input[type="search"],
		.site-footer .search-box input[type="search"], .site-footer .error404 input[type="search"], .site-footer input[type="text"], .site-footer input[type="email"], .site-footer input[type="url"], .site-footer input[type="password"], .site-footer input[type="search"], .site-footer .jetpack_subscription_widget #subscribe-email input[type="email"] { -moz-box-shadow:<?php echo get_theme_mod( 'pena_black_colors' ); ?> 0 2px 0; }
		.site-footer button:hover, .site-footer input[type="button"]:hover, .site-footer input[type="reset"]:hover, .site-footer #main #infinite-handle span:hover, .site-footer button:focus, .site-footer input[type="button"]:focus,
		.site-footer input[type="reset"]:focus, .site-footer input[type="submit"]:focus, .site-footer button:active, .site-footer input[type="button"]:active, .site-footer input[type="reset"]:active,
		.site-footer input[type="submit"]:active, input[type="search"]:focus, .site-footer input[type="text"]:focus, .site-footer input[type="email"]:focus, .site-footer input[type="url"]:focus, .site-footer input[type="password"]:focus,
		.site-footer input[type="search"]:focus, .list-layout .entry-content a.more-link:hover, .blog .grid .entry-content a.more-link:hover, .page-template-grid-template .customwidget .entry-content a:hover { box-shadow:<?php echo get_theme_mod( 'pena_black_colors' ); ?> 0 4px 0; }
		.site-footer button:hover, .site-footer input[type="button"]:hover, .site-footer input[type="reset"]:hover, .site-footer #main #infinite-handle span:hover, .site-footer button:focus, .site-footer input[type="button"]:focus,
		.site-footer input[type="reset"]:focus, .site-footer input[type="submit"]:focus, .site-footer button:active, .site-footer input[type="button"]:active, .site-footer input[type="reset"]:active,
		.site-footer input[type="submit"]:active, input[type="search"]:focus, .site-footer input[type="text"]:focus, .site-footer input[type="email"]:focus, .site-footer input[type="url"]:focus, .site-footer input[type="password"]:focus,
		.site-footer input[type="search"]:focus, .list-layout .entry-content a.more-link:hover, .blog .grid .entry-content a.more-link:hover, .page-template-grid-template .customwidget .entry-content a:hover { -webkit-box-shadow:<?php echo get_theme_mod( 'pena_black_colors' ); ?> 0 4px 0; }
		.site-footer button:hover, .site-footer input[type="button"]:hover, .site-footer input[type="reset"]:hover, .site-footer #main #infinite-handle span:hover, .site-footer button:focus, .site-footer input[type="button"]:focus,
		.site-footer input[type="reset"]:focus, .site-footer input[type="submit"]:focus, .site-footer button:active, .site-footer input[type="button"]:active, .site-footer input[type="reset"]:active,
		.site-footer input[type="submit"]:active, input[type="search"]:focus, .site-footer input[type="text"]:focus, .site-footer input[type="email"]:focus, .site-footer input[type="url"]:focus, .site-footer input[type="password"]:focus,
		.site-footer input[type="search"]:focus, .list-layout .entry-content a.more-link:hover, .blog .grid .entry-content a.more-link:hover, .page-template-grid-template .customwidget .entry-content a:hover { -moz-box-shadow:<?php echo get_theme_mod( 'pena_black_colors' ); ?> 0 4px 0; }
		.list-layout .entry-content a.more-link, .blog .grid .entry-content a.more-link, .page-template-grid-template .customwidget .entry-content a { box-shadow:<?php echo get_theme_mod( 'pena_black_colors' ); ?> 0 1px 0; }
		.list-layout .entry-content a.more-link, .blog .grid .entry-content a.more-link, .page-template-grid-template .customwidget .entry-content a { -webkit-box-shadow:<?php echo get_theme_mod( 'pena_black_colors' ); ?> 0 1px 0; }
		.list-layout .entry-content a.more-link, .blog .grid .entry-content a.more-link, .page-template-grid-template .customwidget .entry-content a { -moz-box-shadow:<?php echo get_theme_mod( 'pena_black_colors' ); ?> 0 1px 0; }
		@media screen and ( min-width: 55em ) {
			.site-header.front.float-header, .standard-menu .site-header.front, .no-fixed.standard-menu .site-header.front, .standard-menu .site-header, .alternative-menu .site-header.front, .alternative-menu .site-header,
			.main-navigation ul ul { background:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
			hr.short { background-color:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
			.single .entry-footer span { border-right-color:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
			.site-header.front, .social-block { background: none; }
		}
		@media screen and ( min-width: 70em ) {
		.comment .comment-metadata span.comment-author { border-bottom-color:<?php echo get_theme_mod( 'pena_black_colors' ); ?>; }
		}
		.blog .entry-meta a, .single .entry-meta a, .search .entry-meta a, .archive .entry-meta a, .customwidget span.posted-on a { color:<?php echo get_theme_mod( 'pena_gray_colors' ); ?>; }
		.site-footer .widget_calendar caption, .site-footer .jetpack_subscription_widget form, .site-footer .es_widget_form { background:<?php echo get_theme_mod( 'pena_gray_colors' ); ?>; }
		#site-navigation button, .site-footer .widget_calendar td, .site-footer .widget_calendar th, .customwidget .entry-meta, .singular .site-content .widget-area.footer-menu, .widget-area.footer-menu { border-color:<?php echo get_theme_mod( 'pena_gray_colors' ); ?>; }
		.social-block, .site-footer .widget_search { border-bottom-color:<?php echo get_theme_mod( 'pena_gray_colors' ); ?>; }
		#site-navigation button{ box-shadow:<?php echo get_theme_mod( 'pena_gray_colors' ); ?> 0 2px 0; }
		#site-navigation button{ -webkit-box-shadow:<?php echo get_theme_mod( 'pena_gray_colors' ); ?> 0 2px 0; }
		#site-navigation button{ -moz-box-shadow:<?php echo get_theme_mod( 'pena_gray_colors' ); ?> 0 2px 0; }
		@media screen and ( min-width: 55em ) {
		.blog .entry-meta a, .search .entry-meta a, .archive .entry-meta a { border-right-color:<?php echo get_theme_mod( 'pena_gray_colors' ); ?>; }
		.blog .entry-meta a, .search .entry-meta a, .archive .entry-meta a, .single .entry-meta a{ color:<?php echo get_theme_mod( 'pena_gray_colors' ); ?>; }
		.blog .entry-meta span.posted-on a, .single .entry-meta span.posted-on a, .archive .entry-meta span.posted-on a { border-color:<?php echo get_theme_mod( 'pena_gray_colors' ); ?>; }
		}
		.main-navigation li.blue a, a.button, a.blue, #promo a, .jetpack_subscription_widget input[type="submit"], .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button,
		.woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, #tribe-events .tribe-events-button,
		#tribe-events .tribe-events-button:hover, #tribe_events_filters_wrapper input[type=submit], .tribe-events-button, .tribe-events-button.tribe-active:hover, .tribe-events-button.tribe-inactive, 
		.tribe-events-button:hover, .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-], .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]>a, .contact-form input[type="submit"],
		.block-one .entry-content .button, .info .entry-content a.button, .info .entry-content a.more-link, .block-five a.blue, #comments input[type="submit"], input.give-submit, #give-purchase-gravatars .give-gravatars-title,
		[id*=give-form].give-display-modal .give-btn, [id*=give-form].give-display-reveal .give-btn, .es_widget_form input[type="button"], .es_widget_form input[type="button"], .wpcf7 input[type="submit"] { color:<?php echo get_theme_mod( 'pena_black_button_text_colors' ); ?>; }
		.content-caption .entry-content .button, a.lila:visited, a.lila:active, a.lila:focus, a.lila, .block-five a.lila, .site-footer a.lila { color:<?php echo get_theme_mod( 'pena_white_button_text_colors' ); ?>; }

		.content-caption { background:<?php echo get_theme_mod( 'pena_caption_bg' ); ?>; }
		.content-caption .entry-content h1, .content-caption .entry-content { color:<?php echo get_theme_mod( 'pena_caption_text' ); ?>; }
		
		.site-header.front, .site-title a:visited, .site-title a:focus, .site-title a:active, .site-title a, .site-description, .main-navigation a { color:<?php echo get_theme_mod( 'pena_header_colors' ); ?>; }
		
		.site-footer, .footer-menu .widget_nav_menu li a, .site-footer a:hover, .site-footer a:visited, .site-footer a:active, .site-footer a { color:<?php echo get_theme_mod( 'pena_footer_colors' ); ?>; }
		
		#secondary #promo p, .site-footer #promo p, .info .entry-content a.more-link:hover { color:<?php echo get_theme_mod( 'pena_page_colors' ); ?>; }
		
		<?php if(get_theme_mod( 'pena_image_color' )) : ?>
		body img {
			filter: grayscale(100%);
		}
		
		.section {
			filter: grayscale(100%); /* Current draft standard */
			-webkit-filter: grayscale(100%); /* New WebKit */
			-moz-filter: grayscale(100%);
			-ms-filter: grayscale(100%); 
			-o-filter: grayscale(100%); /* Not yet supported in Gecko, Opera or IE */ 
			filter: url(resources.svg#desaturate); /* Gecko */
			filter: gray; /* IE */
			-webkit-filter: grayscale(1); /* Old WebKit */
		}
		<?php endif; ?>

		<?php if(get_theme_mod( 'pena_custom_css' )) : ?>
		<?php echo wp_kses( get_theme_mod( 'pena_custom_css' ), '' ); ?>
		<?php endif; ?>
		
		<?php if(get_theme_mod( 'pena_shop_sidebar' )) : ?>
		.archive.woocommerce #secondary {
			display: none;
		}
		.archive.woocommerce .singular .site-main,
		.single-product.woocommerce-page.singular .site-main {
			width: 100%;
			float: none;
			margin-right: 0;
		}
		<?php endif; ?>
		
		<?php if(get_theme_mod( 'pena_shop_single_sidebar' )) : ?>
		.single.woocommerce #secondary {
			display: none;
		}
		.single.woocommerce .singular .site-main {
			width: 100%;
			float: none;
			margin-right: 0;
		}
		<?php endif; ?>

	</style>
	<?php
}
add_action( 'wp_head', 'pena_customizer_css' );
?>