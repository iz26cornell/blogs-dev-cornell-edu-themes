<?php
/**
 * Available Pena Custom Widgets
 *
 * Learn more: http://codex.wordpress.org/Widgets_API#Developing_Widgets
 *
 * @package Pena
 * @since Pena 1.0
 */
 /*-----------------------------------------------------------------------------------*/
/* Custom pena Widget: Call To Action Three Columns
/*-----------------------------------------------------------------------------------*/
class Pena_Action_Buttons extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'description' => esc_html__( 'Three Column Call To Action Buttons Widget', 'pena' ) );
		parent::__construct( false, esc_html__( 'Pena: Three Column Call To Action', 'pena' ), $widget_ops );
	}
	function widget( $args, $instance ) {
		$title = null; $imageurl = null; $imagealt = null; $buttonlink = null; $buttontext = null; $imageurl1 = null; $imagealt1 = null; $buttonlink1 = null; $buttontext1 = null; $imageurl2 = null; $imagealt2 = null; $buttonlink2 = null; $buttontext2 = null; $imageurl3 = null; $imagealt3 = null; $buttonlink3 = null; $buttontext3 = null;
		if ( ! empty( $instance['title'] ) ) { $title = apply_filters( 'widget_title', $instance['title'] ); }
		if ( ! empty( $instance['imageurl'] ) ) { $imageurl = $instance['imageurl']; }
		if ( ! empty( $instance['imagealt'] ) ) { $imagealt = $instance['imagealt']; }
		if ( ! empty( $instance['buttontext'] ) ) { $buttontext = $instance['buttontext']; }
		if ( ! empty( $instance['buttonlink'] ) ) { $buttonlink = $instance['buttonlink']; }
		if ( ! empty( $instance['imageurl1'] ) ) { $imageurl1 = $instance['imageurl1']; }
		if ( ! empty( $instance['imagealt1'] ) ) { $imagealt1 = $instance['imagealt1']; }
		if ( ! empty( $instance['buttontext1'] ) ) { $buttontext1 = $instance['buttontext1']; }
		if ( ! empty( $instance['buttonlink1'] ) ) { $buttonlink1 = $instance['buttonlink1']; }
		if ( ! empty( $instance['imageurl2'] ) ) { $imageurl2 = $instance['imageurl2']; }
		if ( ! empty( $instance['imagealt2'] ) ) { $imagealt2 = $instance['imagealt2']; }
		if ( ! empty( $instance['buttontext2'] ) ) { $buttontext2 = $instance['buttontext2']; }
		if ( ! empty( $instance['buttonlink2'] ) ) { $buttonlink2 = $instance['buttonlink2']; }
		if ( ! empty( $instance['imageurl3'] ) ) { $imageurl3 = $instance['imageurl3']; }
		if ( ! empty( $instance['imagealt3'] ) ) { $imagealt3 = $instance['imagealt3']; }
		if ( ! empty( $instance['buttontext3'] ) ) { $buttontext3 = $instance['buttontext3']; }
		if ( ! empty( $instance['buttonlink3'] ) ) { $buttonlink3 = $instance['buttonlink3']; }
		echo $args['before_widget']; ?>
		<?php if( ! empty( $title ) )
			echo '<div class="widget-title-wrap"><h3 class="widget-title"><span>'. esc_html( $title ) .'</span></h3></div>'; ?>
	
	<div class="call-to-action clearfix">
		<div class="one-third">
			<img src="<?php echo esc_url( $imageurl ); ?>" alt="<?php echo esc_url( $imagealt ); ?>">
			<a href="<?php echo esc_url( $buttonlink ); ?>"><?php echo esc_html( $buttontext ); ?></a>
		</div>
		<div class="one-third">
			<img src="<?php echo esc_url( $imageurl1 ); ?>" alt="<?php echo esc_url( $imagealt1 ); ?>">
			<a href="<?php echo esc_url( $buttonlink1 ); ?>"><?php echo esc_html( $buttontext1 ); ?></a>
		</div>
		<div class="one-third lastcolumn">
			<img src="<?php echo esc_url( $imageurl2 ); ?>" alt="<?php echo esc_url( $imagealt2 ); ?>">
			<a href="<?php echo esc_url( $buttonlink2 ); ?>"><?php echo esc_html( $buttontext2 ); ?></a>
		</div>
	</div>
		<?php
		echo $args['after_widget'];
		// Reset the post globals as this query will have stomped on it
		wp_reset_postdata();
		}
	function update( $new_instance, $old_instance ) {
		$instance['title'] = $new_instance['title'];
		$instance['imageurl'] = $new_instance['imageurl'];
		$instance['imagealt'] = $new_instance['imagealt'];
		$instance['buttontext'] = $new_instance['buttontext'];
		$instance['buttonlink'] = $new_instance['buttonlink'];
		$instance['imageurl1'] = $new_instance['imageurl1'];
		$instance['imagealt1'] = $new_instance['imagealt1'];
		$instance['buttontext1'] = $new_instance['buttontext1'];
		$instance['buttonlink1'] = $new_instance['buttonlink1'];
		$instance['imageurl2'] = $new_instance['imageurl2'];
		$instance['imagealt2'] = $new_instance['imagealt2'];
		$instance['buttontext2'] = $new_instance['buttontext2'];
		$instance['buttonlink2'] = $new_instance['buttonlink2'];
		return $new_instance;
	}
	function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$imageurl = isset( $instance['imageurl'] ) ? esc_attr( $instance['imageurl'] ) : '';
		$imagealt = isset( $instance['imagealt'] ) ? esc_attr( $instance['imagealt'] ) : '';
		$buttontext = isset( $instance['buttontext'] ) ? esc_attr( $instance['buttontext'] ) : '';
		$buttonlink = isset( $instance['buttonlink'] ) ? esc_attr( $instance['buttonlink'] ) : '';
		$imageurl1 = isset( $instance['imageurl1'] ) ? esc_attr( $instance['imageurl1'] ) : '';
		$imagealt1 = isset( $instance['imagealt1'] ) ? esc_attr( $instance['imagealt1'] ) : '';
		$buttontext1 = isset( $instance['buttontext1'] ) ? esc_attr( $instance['buttontext1'] ) : '';
		$buttonlink1 = isset( $instance['buttonlink1'] ) ? esc_attr( $instance['buttonlink1'] ) : '';
		$imageurl2 = isset( $instance['imageurl2'] ) ? esc_attr( $instance['imageurl2'] ) : '';
		$imagealt2 = isset( $instance['imagealt2'] ) ? esc_attr( $instance['imagealt2'] ) : '';
		$buttontext2 = isset( $instance['buttontext2'] ) ? esc_attr( $instance['buttontext2'] ) : '';
		$buttonlink2 = isset( $instance['buttonlink2'] ) ? esc_attr( $instance['buttonlink2'] ) : '';
	?>
	<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:','pena' ); ?></label>
		<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" /></p>
<p><strong>1. Call To Action Button</strong></p>
	<p><label for="<?php echo $this->get_field_id( 'imageurl' ); ?>"><?php esc_html_e( 'Image URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imageurl' ) ); ?>" value="<?php echo esc_attr( $imageurl ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imageurl' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'imagealt' ); ?>"><?php esc_html_e( 'Image ALT Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imagealt' ) ); ?>" value="<?php echo esc_attr( $imagealt ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imagealt' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttontext' ); ?>"><?php esc_html_e( 'Link Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttontext' ) ); ?>" value="<?php echo esc_attr( $buttontext ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttontext' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttonlink' ); ?>"><?php esc_html_e( 'Link URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttonlink' ) ); ?>" value="<?php echo esc_attr( $buttonlink ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttonlink' ); ?>" /></p>
<hr>
<p><strong>2. Call To Action Button</strong></p>
	<p><label for="<?php echo $this->get_field_id( 'imageurl1' ); ?>"><?php esc_html_e( 'Image URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imageurl1' ) ); ?>" value="<?php echo esc_attr( $imageurl1 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imageurl1' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'imagealt1' ); ?>"><?php esc_html_e( 'Image ALT Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imagealt1' ) ); ?>" value="<?php echo esc_attr( $imagealt1 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imagealt1' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttontext1' ); ?>"><?php esc_html_e( 'Link Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttontext1' ) ); ?>" value="<?php echo esc_attr( $buttontext1 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttontext1' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttonlink1' ); ?>"><?php esc_html_e( 'Link URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttonlink1' ) ); ?>" value="<?php echo esc_attr( $buttonlink1 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttonlink1' ); ?>" /></p>
<hr>
<p><strong>3. Call To Action Button</strong></p>
	<p><label for="<?php echo $this->get_field_id( 'imageurl2' ); ?>"><?php esc_html_e( 'Image URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imageurl2' ) ); ?>" value="<?php echo esc_attr( $imageurl2 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imageurl2' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'imagealt2' ); ?>"><?php esc_html_e( 'Image ALT Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imagealt2' ) ); ?>" value="<?php echo esc_attr( $imagealt2 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imagealt2' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttontext2' ); ?>"><?php esc_html_e( 'Link Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttontext2' ) ); ?>" value="<?php echo esc_attr( $buttontext2 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttontext2' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttonlink2' ); ?>"><?php esc_html_e( 'Link URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttonlink2' ) ); ?>" value="<?php echo esc_attr( $buttonlink2 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttonlink2' ); ?>" /></p>
	<?php
	}
}
register_widget( 'Pena_Action_Buttons' );
 /*-----------------------------------------------------------------------------------*/
/* Custom pena Widget: Call To Action Four Columns
/*-----------------------------------------------------------------------------------*/
class Pena_Action_Buttons_Four extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'description' => esc_html__( 'Four Column Call To Action Buttons Widget', 'pena' ) );
		parent::__construct( false, esc_html__( 'Pena: Four Column Call To Action', 'pena' ), $widget_ops );
	}
	function widget( $args, $instance ) {
		$title = null; $imageurl = null; $imagealt = null; $buttonlink = null; $buttontext = null; $imageurl1 = null; $imagealt1 = null; $buttonlink1 = null; $buttontext1 = null; $imageurl2 = null; $imagealt2 = null; $buttonlink2 = null; $buttontext2 = null; $imageurl3 = null; $imagealt3 = null; $buttonlink3 = null; $buttontext3 = null; $imageurl4 = null; $imagealt4 = null; $buttonlink4 = null; $buttontext4 = null;
		if ( ! empty( $instance['title'] ) ) { $title = apply_filters( 'widget_title', $instance['title'] ); }
		if ( ! empty( $instance['imageurl'] ) ) { $imageurl = $instance['imageurl']; }
		if ( ! empty( $instance['imagealt'] ) ) { $imagealt = $instance['imagealt']; }
		if ( ! empty( $instance['buttontext'] ) ) { $buttontext = $instance['buttontext']; }
		if ( ! empty( $instance['buttonlink'] ) ) { $buttonlink = $instance['buttonlink']; }
		if ( ! empty( $instance['imageurl1'] ) ) { $imageurl1 = $instance['imageurl1']; }
		if ( ! empty( $instance['imagealt1'] ) ) { $imagealt1 = $instance['imagealt1']; }
		if ( ! empty( $instance['buttontext1'] ) ) { $buttontext1 = $instance['buttontext1']; }
		if ( ! empty( $instance['buttonlink1'] ) ) { $buttonlink1 = $instance['buttonlink1']; }
		if ( ! empty( $instance['imageurl2'] ) ) { $imageurl2 = $instance['imageurl2']; }
		if ( ! empty( $instance['imagealt2'] ) ) { $imagealt2 = $instance['imagealt2']; }
		if ( ! empty( $instance['buttontext2'] ) ) { $buttontext2 = $instance['buttontext2']; }
		if ( ! empty( $instance['buttonlink2'] ) ) { $buttonlink2 = $instance['buttonlink2']; }
		if ( ! empty( $instance['imageurl3'] ) ) { $imageurl3 = $instance['imageurl3']; }
		if ( ! empty( $instance['imagealt3'] ) ) { $imagealt3 = $instance['imagealt3']; }
		if ( ! empty( $instance['buttontext3'] ) ) { $buttontext3 = $instance['buttontext3']; }
		if ( ! empty( $instance['buttonlink3'] ) ) { $buttonlink3 = $instance['buttonlink3']; }
		echo $args['before_widget']; ?>
		<?php if( ! empty( $title ) )
			echo '<div class="widget-title-wrap"><h3 class="widget-title"><span>'. esc_html( $title ) .'</span></h3></div>'; ?>
	
	<div class="call-to-action clearfix">
		<div class="one-fourth">
			<img src="<?php echo esc_url( $imageurl ); ?>" alt="<?php echo esc_url( $imagealt ); ?>">
			<a href="<?php echo esc_url( $buttonlink ); ?>"><?php echo esc_html( $buttontext ); ?></a>
		</div>
		<div class="one-fourth">
			<img src="<?php echo esc_url( $imageurl1 ); ?>" alt="<?php echo esc_url( $imagealt1 ); ?>">
			<a href="<?php echo esc_url( $buttonlink1 ); ?>"><?php echo esc_html( $buttontext1 ); ?></a>
		</div>
		<div class="one-fourth">
			<img src="<?php echo esc_url( $imageurl2 ); ?>" alt="<?php echo esc_url( $imagealt2 ); ?>">
			<a href="<?php echo esc_url( $buttonlink2 ); ?>"><?php echo esc_html( $buttontext2 ); ?></a>
		</div>
		<div class="one-fourth lastcolumn">
			<img src="<?php echo esc_url( $imageurl3 ); ?>" alt="<?php echo esc_url( $imagealt3 ); ?>">
			<a href="<?php echo esc_url( $buttonlink3 ); ?>"><?php echo esc_html( $buttontext3 ); ?></a>
		</div>
	</div>
		<?php
		echo $args['after_widget'];
		// Reset the post globals as this query will have stomped on it
		wp_reset_postdata();
		}
	function update( $new_instance, $old_instance ) {
		$instance['title'] = $new_instance['title'];
		$instance['imageurl'] = $new_instance['imageurl'];
		$instance['imagealt'] = $new_instance['imagealt'];
		$instance['buttontext'] = $new_instance['buttontext'];
		$instance['buttonlink'] = $new_instance['buttonlink'];
		$instance['imageurl1'] = $new_instance['imageurl1'];
		$instance['imagealt1'] = $new_instance['imagealt1'];
		$instance['buttontext1'] = $new_instance['buttontext1'];
		$instance['buttonlink1'] = $new_instance['buttonlink1'];
		$instance['imageurl2'] = $new_instance['imageurl2'];
		$instance['imagealt2'] = $new_instance['imagealt2'];
		$instance['buttontext2'] = $new_instance['buttontext2'];
		$instance['buttonlink2'] = $new_instance['buttonlink2'];
		$instance['imageurl3'] = $new_instance['imageurl3'];
		$instance['imagealt3'] = $new_instance['imagealt3'];
		$instance['buttontext3'] = $new_instance['buttontext3'];
		$instance['buttonlink3'] = $new_instance['buttonlink3'];
		return $new_instance;
	}
	function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$imageurl = isset( $instance['imageurl'] ) ? esc_attr( $instance['imageurl'] ) : '';
		$imagealt = isset( $instance['imagealt'] ) ? esc_attr( $instance['imagealt'] ) : '';
		$buttontext = isset( $instance['buttontext'] ) ? esc_attr( $instance['buttontext'] ) : '';
		$buttonlink = isset( $instance['buttonlink'] ) ? esc_attr( $instance['buttonlink'] ) : '';
		$imageurl1 = isset( $instance['imageurl1'] ) ? esc_attr( $instance['imageurl1'] ) : '';
		$imagealt1 = isset( $instance['imagealt1'] ) ? esc_attr( $instance['imagealt1'] ) : '';
		$buttontext1 = isset( $instance['buttontext1'] ) ? esc_attr( $instance['buttontext1'] ) : '';
		$buttonlink1 = isset( $instance['buttonlink1'] ) ? esc_attr( $instance['buttonlink1'] ) : '';
		$imageurl2 = isset( $instance['imageurl2'] ) ? esc_attr( $instance['imageurl2'] ) : '';
		$imagealt2 = isset( $instance['imagealt2'] ) ? esc_attr( $instance['imagealt2'] ) : '';
		$buttontext2 = isset( $instance['buttontext2'] ) ? esc_attr( $instance['buttontext2'] ) : '';
		$buttonlink2 = isset( $instance['buttonlink2'] ) ? esc_attr( $instance['buttonlink2'] ) : '';
		$imageurl3 = isset( $instance['imageurl3'] ) ? esc_attr( $instance['imageurl3'] ) : '';
		$imagealt3 = isset( $instance['imagealt3'] ) ? esc_attr( $instance['imagealt3'] ) : '';
		$buttontext3 = isset( $instance['buttontext3'] ) ? esc_attr( $instance['buttontext3'] ) : '';
		$buttonlink3 = isset( $instance['buttonlink3'] ) ? esc_attr( $instance['buttonlink3'] ) : '';
	?>
	<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:','pena' ); ?></label>
		<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" /></p>
<p><strong>1. Call To Action Button</strong></p>
	<p><label for="<?php echo $this->get_field_id( 'imageurl' ); ?>"><?php esc_html_e( 'Image URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imageurl' ) ); ?>" value="<?php echo esc_attr( $imageurl ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imageurl' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'imagealt' ); ?>"><?php esc_html_e( 'Image ALT Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imagealt' ) ); ?>" value="<?php echo esc_attr( $imagealt ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imagealt' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttontext' ); ?>"><?php esc_html_e( 'Link Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttontext' ) ); ?>" value="<?php echo esc_attr( $buttontext ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttontext' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttonlink' ); ?>"><?php esc_html_e( 'Link URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttonlink' ) ); ?>" value="<?php echo esc_attr( $buttonlink ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttonlink' ); ?>" /></p>
<hr>
<p><strong>2. Call To Action Button</strong></p>
	<p><label for="<?php echo $this->get_field_id( 'imageurl1' ); ?>"><?php esc_html_e( 'Image URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imageurl1' ) ); ?>" value="<?php echo esc_attr( $imageurl1 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imageurl1' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'imagealt1' ); ?>"><?php esc_html_e( 'Image ALT Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imagealt1' ) ); ?>" value="<?php echo esc_attr( $imagealt1 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imagealt1' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttontext1' ); ?>"><?php esc_html_e( 'Link Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttontext1' ) ); ?>" value="<?php echo esc_attr( $buttontext1 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttontext1' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttonlink1' ); ?>"><?php esc_html_e( 'Link URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttonlink1' ) ); ?>" value="<?php echo esc_attr( $buttonlink1 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttonlink1' ); ?>" /></p>
<hr>
<p><strong>3. Call To Action Button</strong></p>
	<p><label for="<?php echo $this->get_field_id( 'imageurl2' ); ?>"><?php esc_html_e( 'Image URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imageurl2' ) ); ?>" value="<?php echo esc_attr( $imageurl2 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imageurl2' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'imagealt2' ); ?>"><?php esc_html_e( 'Image ALT Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imagealt2' ) ); ?>" value="<?php echo esc_attr( $imagealt2 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imagealt2' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttontext2' ); ?>"><?php esc_html_e( 'Link Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttontext2' ) ); ?>" value="<?php echo esc_attr( $buttontext2 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttontext2' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttonlink2' ); ?>"><?php esc_html_e( 'Link URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttonlink2' ) ); ?>" value="<?php echo esc_attr( $buttonlink2 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttonlink2' ); ?>" /></p>
<hr>
<p><strong>4. Call To Action Button</strong></p>
	<p><label for="<?php echo $this->get_field_id( 'imageurl3' ); ?>"><?php esc_html_e( 'Image URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imageurl3' ) ); ?>" value="<?php echo esc_attr( $imageurl3 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imageurl3' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'imagealt3' ); ?>"><?php esc_html_e( 'Image ALT Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'imagealt3' ) ); ?>" value="<?php echo esc_attr( $imagealt3 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'imagealt3' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttontext3' ); ?>"><?php esc_html_e( 'Link Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttontext3' ) ); ?>" value="<?php echo esc_attr( $buttontext3 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttontext3' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttonlink3' ); ?>"><?php esc_html_e( 'Link URL','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttonlink3' ) ); ?>" value="<?php echo esc_attr( $buttonlink3 ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttonlink3' ); ?>" /></p>
	<?php
	}
}
register_widget( 'Pena_Action_Buttons_Four' );
/*-----------------------------------------------------------------------------------*/
/* Custom pena Widget: Promo Block
/*-----------------------------------------------------------------------------------*/
class Pena_Order_Block extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'description' => esc_html__( 'Promo Block', 'pena' ) );
		parent::__construct( false, esc_html__( 'Pena: Promo Block', 'pena' ),$widget_ops );
	}
	function widget( $args, $instance ) {
		$title = null; $text = null; $buttontext = null; $buttonlink = null;
		if ( ! empty( $instance['title'] ) ) { $title = apply_filters( 'widget_title', $instance['title'] ); }
		if ( ! empty( $instance['text'] ) ) { $text = $instance['text']; }
		if ( ! empty( $instance['buttontext'] ) ) { $buttontext = $instance['buttontext']; }
		if ( ! empty( $instance['buttonlink'] ) ) { $buttonlink = $instance['buttonlink']; }
		echo $args['before_widget']; ?>
		<?php if( ! empty( $title ) )
			echo '<div class="widget-title-wrap"><h3 class="widget-title"><span>'. esc_html( $title ) .'</span></h3></div>'; ?>

		<div id="promo">
		<div class="clear">
			<p><?php echo esc_html( $text ); ?></p>
			<a href="<?php echo esc_html( $buttonlink ); ?>"><?php echo esc_html( $buttontext ); ?></a>
		</div>
		</div>

		<?php
		echo $args['after_widget'];
		// Reset the post globals as this query will have stomped on it
		wp_reset_postdata();
		}
	function update ($new_instance, $old_instance ) {
		$instance['title'] = $new_instance['title'];
		$instance['text'] = $new_instance['text'];
		$instance['buttontext'] = $new_instance['buttontext'];
		$instance['buttonlink'] = $new_instance['buttonlink'];
		return $new_instance;
	}
	function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$text = isset( $instance['text'] ) ? esc_attr( $instance['text'] ) : 'Donate to the Living On One Cause';
		$buttontext = isset( $instance['buttontext'] ) ? esc_attr( $instance['buttontext'] ) : 'Donate';
		$buttonlink = isset( $instance['buttonlink'] ) ? esc_attr( $instance['buttonlink'] ) : 'http://yourlink.com/';
	?>
	<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:','pena' ); ?></label>
		<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php esc_html_e( 'Text Part','pena' ); ?></label>
		<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" value="<?php echo esc_attr( $text ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttontext' ); ?>"><?php esc_html_e( 'Button Text','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttontext' ) ); ?>" value="<?php echo esc_attr( $buttontext ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttontext' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'buttonlink' ); ?>"><?php esc_html_e( 'Button Link','pena' ); ?></label>
	<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'buttonlink' ) ); ?>" value="<?php echo esc_attr( $buttonlink ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'buttonlink' ); ?>" /></p>
	<?php
	}
}
register_widget( 'Pena_Order_Block' );
/*-----------------------------------------------------------------------------------*/
/* Custom pena Widget: Recent Posts
/*-----------------------------------------------------------------------------------*/
class Pena_Recent_Post extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'description' => esc_html__( 'One, Two, Three or Four Column Recent Post Widget', 'pena' ) );
		parent::__construct( false, esc_html__( 'Pena: Latest Posts', 'pena' ), $widget_ops );
	}
	function widget( $args, $instance ) {
		$title = null; $postnumber = null; $columns = null; $category = null;
		if ( ! empty( $instance['title'] ) ) { $title = apply_filters( 'widget_title', $instance['title'] ); }
		if ( ! empty( $instance['postnumber'] ) ) { $postnumber = $instance['postnumber']; }
		if ( ! empty( $instance['columns'] ) ) { $columns = $instance['columns']; }
		if ( ! empty( $instance['category'] ) ) { $category = apply_filters( 'widget_title', $instance['category'] ); }
		echo $args['before_widget']; ?>
		<?php if( ! empty( $title ) )
			echo '<div class="widget-title-wrap"><h3 class="widget-title"><span>'. esc_html( $title ) .'</span></h3></div>'; ?>
<?php
// The Query
$recent_query = new WP_Query(array (
		'post_status'	=> 'publish',
		'posts_per_page' => $postnumber,
		'ignore_sticky_posts' => 1,
		'cat' => $category,
	) );
?>
<div class="posts clearfix customwidget columns">
<?php
// The Loop
if( $recent_query->have_posts() ) : ?>
	<?php while( $recent_query->have_posts()) : $recent_query->the_post() ?>
	<div class="<?php echo esc_html( $columns ); ?> clear">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">
				<?php if ( has_post_thumbnail() ) { ?>
					<div class="post-thumbnail">
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail( 'pena-recent-post-image' ); ?>
						</a>
					</div>
				<?php } ?>
				<div class="post-content">
				<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
				<?php if ( 'post' == get_post_type() ) : ?>
				<div class="entry-meta">
					<?php pena_posted_on_custom(); ?>
				</div><!-- .entry-meta -->
				<?php endif; ?>
				<?php the_excerpt(); ?>
				</div><!-- .post-content -->
			</div>
		</article><!-- #post-## -->
	</div>
	<?php endwhile ?>
<?php endif ?>
</div>
		<?php
		echo $args['after_widget'];
		// Reset the post globals as this query will have stomped on it
		wp_reset_postdata();
		}
	function update( $new_instance, $old_instance ) {
		$instance['title'] = $new_instance['title'];
		$instance['postnumber'] = $new_instance['postnumber'];
		$instance['columns'] = $new_instance['columns'];
		$instance['category'] = $new_instance['category'];
		return $new_instance;
	}
	function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$postnumber = isset( $instance['postnumber'] ) ? esc_attr( $instance['postnumber'] ) : '4';
		$columns = isset( $instance['columns'] ) ? esc_attr( $instance['columns'] ) : 'fourcolumn';
		$category = isset( $instance['category'] ) ? esc_attr( $instance['category'] ) : '';
	?>
	<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:','pena' ); ?></label>
		<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'postnumber' ); ?>"><?php esc_html_e( 'Number of posts to show:','pena' ); ?></label>
		<input type="text" name="<?php echo esc_attr( $this->get_field_name( 'postnumber' ) ); ?>" value="<?php echo esc_attr( $postnumber ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'postnumber' ); ?>" /></p>
	<p><label for="<?php echo $this->get_field_id( 'columns' ); ?>"><?php esc_html_e( 'Number of columns to show:','pena' ); ?></label>
	<select name="<?php echo $this->get_field_name( 'columns' ); ?>" id="<?php echo $this->get_field_id( 'columns' ); ?>" class="widefat">
	<?php
		$options = array(
		'fourcolumn'  => 'Four Columns',
		'threecolumn' => 'Three Columns',
		'twocolumn'   => 'Two Columns',
		'onecolumn'   => 'One Column'
	);
	$option = '';
	foreach ( $options as $class => $label ) {
	echo '<option value="' . $class . '" id="' . $option . '"', $columns == $class ? ' selected="selected"' : '', '>', $label, '</option>';
	}
	?>
	</select>
	<p><label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php esc_html_e( 'Category:', 'pena' ); ?></label></p>
	<?php
			wp_dropdown_categories( array(
				'orderby'    => 'title',
				'hide_empty' => false,
				'name'       => $this->get_field_name( 'category' ),
				'class'      => 'widefat',
				'show_option_all' => 'all categories',
				'selected'   => $category
			) );
			?>
<hr />
	<?php
	}
}
register_widget( 'Pena_Recent_Post' );