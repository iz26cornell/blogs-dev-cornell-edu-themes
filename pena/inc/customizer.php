<?php
/**
 * Pena Theme Customizer
 *
 * @package Pena
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function pena_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_section('colors');
	
/**
* Adds the individual sections for header
*/
	$wp_customize->add_section( 'pena_theme_options_header', array(
		'title'    => esc_html__( 'Header Options', 'pena' ),
		'priority' => 30,
	) );

	/* Header Layout */
	$wp_customize->add_setting( 'pena_header_layout', array(
		'default'           => 'standard-header',
		'sanitize_callback' => 'pena_sanitize_choices',
	) );
	$wp_customize->add_control( 'pena_header_layout', array(
		'label'             => esc_html__( 'Header Options', 'pena' ),
		'section'           => 'pena_theme_options_header',
		'settings'          => 'pena_header_layout',
		'priority'          => 1,
		'type'              => 'radio',
		'choices'           => array(
			'fixed-header'   => esc_html__( 'Fixed Header', 'pena' ),
			'standard-header'  => esc_html__( 'Standard Header', 'pena' ),
			'alternative-header'  => esc_html__( 'Alternative Header', 'pena' ),
		)
	) );
	/* Front Page: Featured Pages */
	$wp_customize->add_section( 'pena_theme_options', array(
		'title'    => esc_html__( 'Front Page', 'pena' ),
		'priority' => 31,
	) );
	
	$wp_customize->add_setting( 'pena_caption_bg', array(
		'default'           => '',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_caption_bg', array(
		'label'             => esc_html__( 'Hero Caption Background Color', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 1,
	) ) );
	
	$wp_customize->add_setting( 'pena_caption_text', array(
		'default'           => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_caption_text', array(
		'label'             => esc_html__( 'Hero Caption Text Color', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 2,
	) ) );
	
	$wp_customize->add_setting( 'pena_featured_page_one', array(
		'default'           => '',
		'sanitize_callback' => 'pena_sanitize_dropdown_pages',
	) );
	$wp_customize->add_control( 'pena_featured_page_one', array(
		'label'             => esc_html__( 'First Content Block', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 9,
		'type'              => 'dropdown-pages',
	) );
	
	/* Call to Action Button */
	$wp_customize->add_setting( 'pena_panel_button', array(
		'default'           => '',
		'sanitize_callback' => 'wp_kses_post',
	) );
	$wp_customize->add_control( 'pena_panel_button', array(
		'label'             => esc_html__( 'Button Text', 'pena' ),
		'description'       => esc_html__( 'Enter the text for the button', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 10,
		'type'              => 'text',
	) );
		$wp_customize->add_setting( 'pena_panel_button_link', array(
		'default'           => '',
		'sanitize_callback' => 'wp_kses_post',
	) );
	$wp_customize->add_control( 'pena_panel_button_link', array(
		'label'             => esc_html__( 'Button URL', 'pena' ),
		'description'       => esc_html__( 'Enter the URL for the button', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 11,
		'type'              => 'text',
	) );
	
	/* Columns */
	$wp_customize->add_setting( 'pena_front_first_layout', array(
		'default'           => 'three-columns',
		'sanitize_callback' => 'pena_sanitize_choices',
	) );
	$wp_customize->add_control( 'pena_front_first_layout', array(
		'description'       => esc_html__( 'Choose number of columns for First Content Block', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 11,
		'type'              => 'radio',
		'choices'           => array(
			'four-columns'   => esc_html__( 'Four Columns', 'pena' ),
			'three-columns'  => esc_html__( 'Three Columns', 'pena' ),
			'two-columns'  => esc_html__( 'Two Columns', 'pena' ),
			'one-column'  => esc_html__( 'One Column', 'pena' ),
		)
	) );
	
	/* Child Pages Image Link */
	$wp_customize->add_setting( 'pena_image_link', array(
		'default'           => false,
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'pena_image_link', array(
		'label'             => esc_html__( 'Disable Child Page Featured Image Link', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 11,
		'type'              => 'checkbox',
	) );
		
	/* Child Pages Title Link */
	$wp_customize->add_setting( 'pena_child_title_link', array(
		'default'           => false,
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'pena_child_title_link', array(
		'label'             => esc_html__( 'Disable Child Page Title Link', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 11,
		'type'              => 'checkbox',
	) );
	
	/* Front Page: Featured Page Two */
	$wp_customize->add_setting( 'pena_featured_page_two', array(
		'default'           => '',
		'sanitize_callback' => 'pena_sanitize_dropdown_pages',
	) );
	$wp_customize->add_control( 'pena_featured_page_two', array(
		'label'             => esc_html__( 'Second Content Block', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 12,
		'type'              => 'dropdown-pages',
	) );
	
	/* Front Page: Featured Page Three */
	$wp_customize->add_setting( 'pena_featured_page_three', array(
		'default'           => '',
		'sanitize_callback' => 'pena_sanitize_dropdown_pages',
	) );
	$wp_customize->add_control( 'pena_featured_page_three', array(
		'label'             => esc_html__( 'Third Content Block', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 13,
		'type'              => 'dropdown-pages',
	) );
	
	/* Columns */
	$wp_customize->add_setting( 'pena_front_third_layout', array(
		'default'           => 'four-columns',
		'sanitize_callback' => 'pena_sanitize_choices',
	) );
	$wp_customize->add_control( 'pena_front_third_layout', array(
		'description'       => esc_html__( 'Choose number of columns for Third Content Block', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 13,
		'type'              => 'radio',
		'choices'           => array(
			'four-columns'   => esc_html__( 'Four Columns', 'pena' ),
			'three-columns'  => esc_html__( 'Three Columns', 'pena' ),
			'two-columns'  => esc_html__( 'Two Columns', 'pena' ),
			'one-column'  => esc_html__( 'One Column', 'pena' ),
		)
	) );
	
	/* Front Page: Featured Page Four */
	$wp_customize->add_setting( 'pena_featured_page_four', array(
		'default'           => '',
		'sanitize_callback' => 'pena_sanitize_dropdown_pages',
	) );
	$wp_customize->add_control( 'pena_featured_page_four', array(
		'label'             => esc_html__( 'Fourth Content Block', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 14,
		'type'              => 'dropdown-pages',
	) );
	/* Front Page: Featured Page Four */
	$wp_customize->add_setting( 'pena_featured_page_five', array(
		'default'           => '',
		'sanitize_callback' => 'pena_sanitize_dropdown_pages',
	) );
	$wp_customize->add_control( 'pena_featured_page_five', array(
		'label'             => esc_html__( 'Fifth Content Block', 'pena' ),
		'section'           => 'pena_theme_options',
		'priority'          => 15,
		'type'              => 'dropdown-pages',
	) );
	/**
	* Remove Fixed Layout
	*/
	$wp_customize->add_setting( 'pena_fixed_top', array(
		'default'           => true,
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'pena_fixed_top', array(
		'label'             => esc_html__( 'Deactivate Scrolling Over the Top Image', 'pena' ),
		'description'       => esc_html__( 'If you want to have a standard layout without page scrolling over the top image, check this box. This option applies only to Front Page Template.', 'pena' ),
		'section'           => 'pena_theme_options',
		'settings'          => 'pena_fixed_top',
		'type'		        => 'checkbox',
		'priority'          => 16,
	) );
/* About Page: Featured Pages */
	$wp_customize->add_section( 'pena_about_options', array(
		'title'    => esc_html__( 'About Page', 'pena' ),
		'priority' => 32,
	) );
	$wp_customize->add_setting( 'pena_featured_page_six', array(
		'default'           => '',
		'sanitize_callback' => 'pena_sanitize_dropdown_pages',
	) );
	$wp_customize->add_control( 'pena_featured_page_six', array(
		'label'             => esc_html__( 'First Content Block', 'pena' ),
		'section'           => 'pena_about_options',
		'priority'          => 1,
		'type'              => 'dropdown-pages',
	) );
	$wp_customize->add_setting( 'pena_featured_page_seven', array(
		'default'           => '',
		'sanitize_callback' => 'pena_sanitize_dropdown_pages',
	) );
	$wp_customize->add_control( 'pena_featured_page_seven', array(
		'label'             => esc_html__( 'Second Content Block', 'pena' ),
		'section'           => 'pena_about_options',
		'priority'          => 2,
		'type'              => 'dropdown-pages',
	) );
		/* Columns */
	$wp_customize->add_setting( 'pena_about_layout', array(
		'default'           => 'four-columns',
		'sanitize_callback' => 'pena_sanitize_choices',
	) );
	$wp_customize->add_control( 'pena_about_layout', array(
		'description'       => esc_html__( 'Choose number of columns for Second Content Block', 'pena' ),
		'section'           => 'pena_about_options',
		'settings'          => 'pena_about_layout',
		'priority'          => 3,
		'type'              => 'radio',
		'choices'           => array(
			'four-columns'   => esc_html__( 'Four Columns', 'pena' ),
			'three-columns'  => esc_html__( 'Three Columns', 'pena' ),
			'two-columns'  => esc_html__( 'Two Columns', 'pena' ),
			'one-column'  => esc_html__( 'One Column', 'pena' ),
		)
	) );
	$wp_customize->add_setting( 'pena_featured_page_eight', array(
		'default'           => '',
		'sanitize_callback' => 'pena_sanitize_dropdown_pages',
	) );
	$wp_customize->add_control( 'pena_featured_page_eight', array(
		'label'             => esc_html__( 'Third Content Block', 'pena' ),
		'section'           => 'pena_about_options',
		'priority'          => 4,
		'type'              => 'dropdown-pages',
	) );
	/* Involved Page: Featured Pages */
	$wp_customize->add_section( 'pena_involved_options', array(
		'title'    => esc_html__( 'Involved Page', 'pena' ),
		'priority' => 33,
	) );
	/* Columns */
	$wp_customize->add_setting( 'pena_involved_layout', array(
		'default'           => 'four-columns',
		'sanitize_callback' => 'pena_sanitize_choices',
	) );
	$wp_customize->add_control( 'pena_involved_layout', array(
		'description'       => esc_html__( 'Choose number of columns for the child pages', 'pena' ),
		'section'           => 'pena_involved_options',
		'settings'          => 'pena_involved_layout',
		'priority'          => 1,
		'type'              => 'radio',
		'choices'           => array(
			'four-columns'   => esc_html__( 'Four Columns', 'pena' ),
			'three-columns'  => esc_html__( 'Three Columns', 'pena' ),
			'two-columns'  => esc_html__( 'Two Columns', 'pena' ),
			'one-column'  => esc_html__( 'One Column', 'pena' ),
		)
	) );
	$wp_customize->add_setting( 'pena_featured_page_nine', array(
		'default'           => '',
		'sanitize_callback' => 'pena_sanitize_dropdown_pages',
	) );
	$wp_customize->add_control( 'pena_featured_page_nine', array(
		'label'             => esc_html__( 'First Content Block', 'pena' ),
		'section'           => 'pena_involved_options',
		'priority'          => 2,
		'type'              => 'dropdown-pages',
	) );

	$wp_customize->add_section( 'pena_general_options', array(
		'title'             => esc_html__( 'Blog Options', 'pena' ),
		'priority'          => 34,
	) );
	
	/* Blog Layout */
	$wp_customize->add_setting( 'pena_blog_layout', array(
		'default'           => 'sidebar-right',
		'sanitize_callback' => 'pena_sanitize_choices',
	) );
	$wp_customize->add_control( 'pena_blog_layout', array(
		'label'             => esc_html__( 'Blog Layout', 'pena' ),
		'description'       => esc_html__( 'Choose the best blog layout for your site. You can have no sidebar, change the position of the sidebar, or select a grid layout. Also applies to archive pages.', 'pena' ),
		'section'           => 'pena_general_options',
		'settings'          => 'pena_blog_layout',
		'priority'          => 13,
		'type'              => 'radio',
		'choices'           => array(
			'list'   => esc_html__( 'List Post Layout', 'pena' ),
			'sidebar-right'  => esc_html__( 'Right Sidebar Layout', 'pena' ),
			'sidebar-left'  => esc_html__( 'Left Sidebar Layout', 'pena' ),
			'grid-two'  => esc_html__( 'Two Column Grid Layout', 'pena' ),
			'grid-two-sidebar'  => esc_html__( 'Two Column Grid Layout with Sidebar', 'pena' ),
			'grid-three'  => esc_html__( 'Three Column Grid Layout', 'pena' ),
		)
	) );
	
	/* Post Display */
	$wp_customize->add_setting( 'pena_post_type', array(
		'default'           => 'full-lenght',
		'sanitize_callback' => 'pena_sanitize_choices',
	) );
	$wp_customize->add_control( 'pena_post_type', array(
		'label'             => esc_html__( 'Post Display', 'pena' ),
		'section'           => 'pena_general_options',
		'settings'          => 'pena_post_type',
		'priority'          => 14,
		'type'              => 'radio',
		'choices'           => array(
			'full-lenght'   => esc_html__( 'Full Length', 'pena' ),
			'excerpt-lenght'  => esc_html__( 'Excerpt', 'pena' ),
		)
	) );

	/* Post Settings */
	$wp_customize->add_setting( 'pena_post_footer', array(
		'default'           => false,
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );
	$wp_customize->add_control('pena_post_footer', array(
				'label'      => esc_html__( 'Hide post author and date from posts.', 'pena' ),
				'section'    => 'pena_general_options',
				'settings'   => 'pena_post_footer',
				'type'		 => 'checkbox',
				'priority'	 => 15
		) );

	$wp_customize->add_setting( 'pena_author_bio', array(
		'default'           => false,
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );
	$wp_customize->add_control('pena_author_bio', array(
				'label'      => esc_html__( 'Hide author bio ("published by" box).', 'pena' ),
				'section'    => 'pena_general_options',
				'settings'   => 'pena_author_bio',
				'type'		 => 'checkbox',
				'priority'	 => 15
		) );
		
	/* Featured Image Display */
	$wp_customize->add_setting( 'pena_main_featured_image', array(
		'default'           => false,
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );
	$wp_customize->add_control('pena_main_featured_image', array(
				'label'      => esc_html__( 'Hide Featured Image on Blog/Archive Page', 'pena' ),
				'section'    => 'pena_general_options',
				'settings'   => 'pena_main_featured_image',
				'type'		 => 'checkbox',
				'priority'	 => 16
	) );
	
	$wp_customize->add_setting( 'pena_single_featured_image', array(
		'default'           => false,
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );
	$wp_customize->add_control('pena_single_featured_image', array(
				'label'      => esc_html__( 'Hide Featured Image on Single Post Page', 'pena' ),
				'section'    => 'pena_general_options',
				'settings'   => 'pena_single_featured_image',
				'type'		 => 'checkbox',
				'priority'	 => 17
	) );

	/**
	* Shop Sidebar
	*/
	$wp_customize->add_section( 'pena_shop_section' , array(
		'title'       => esc_html__( 'WooCommerce Options', 'pena' ),
		'priority'    => 35,
		'active_callback' => 'pena_is_meta_active',
		'description' => esc_html__( 'Hide sidebar on main and single product page', 'pena' )
	) );
	$wp_customize->add_setting( 'pena_shop_sidebar', array(
		'default'           => false,
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'pena_shop_sidebar', array(
		'label'             => esc_html__( 'Check this box if you want to hide sidebar on the WooCommerce pages', 'pena' ),
		'section'           => 'pena_shop_section',
		'settings'          => 'pena_shop_sidebar',
		'type'		        => 'checkbox',
		'priority'          => 1,
	) );
	$wp_customize->add_setting( 'pena_shop_single_sidebar', array(
		'default'           => false,
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'pena_shop_single_sidebar', array(
		'label'             => esc_html__( 'Check this box if you want to hide sidebar on WooCommerce single product page', 'pena' ),
		'section'           => 'pena_shop_section',
		'settings'          => 'pena_shop_single_sidebar',
		'type'		        => 'checkbox',
		'priority'          => 2,
	) );
	
	/* Image Options */
	$wp_customize->add_section( 'pena_image_section' , array(
		'title'       => esc_html__( 'Image Options', 'pena' ),
		'priority'   => 36,
	) );
	$wp_customize->add_setting( 'pena_image_color', array(
		'default'           => false,
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'pena_image_color', array(
		'label'             => esc_html__( 'Check this box if you want to convert images to black and white', 'pena' ),
		'section'           => 'pena_image_section',
		'type'		        => 'checkbox',
		'priority'          => 1,
	) );
	
	/**
* Custom CSS
*/
	$wp_customize->add_section( 'pena_custom_css_section' , array(
		'title'    => esc_html__( 'Custom CSS', 'pena' ),
		'description'=> 'Add your custom CSS which will overwrite the theme CSS',
		'priority'   => 37,
	) );

	/* Custom CSS*/
	$wp_customize->add_setting( 'pena_custom_css', array(
		'default'           => '',
		'sanitize_callback' => 'pena_sanitize_text',
	) );
	$wp_customize->add_control( 'pena_custom_css', array(
		'label'             => esc_html__( 'Custom CSS', 'pena' ),
		'section'           => 'pena_custom_css_section',
		'settings'          => 'pena_custom_css',
		'type'		        => 'textarea',
		'priority'          => 1,
	) );
	
/**
* Migrating Custom CSS to the core Additional CSS if user uses WordPress 4.7.
*
* @since Pena 1.0.5
*/
	if ( function_exists( 'wp_update_custom_css_post' ) ) {
		$custom_css = get_theme_mod( 'pena_custom_css' );
		if ( $custom_css ) {
			$core_css = wp_get_custom_css(); // Preserve any CSS already added to the core option.
			$return = wp_update_custom_css_post( $core_css . $custom_css );
			if ( ! is_wp_error( $return ) ) {
				// Remove the old theme_mod, so that the CSS is stored in only one place moving forward.
				remove_theme_mod( 'pena_custom_css');
			}
		}
		$wp_customize->remove_control( 'pena_custom_css' );
	}

/**
* Custom Colors
*/
	$wp_customize->add_panel( 'pena_colors_panel', array(
		'title'          => esc_html__( 'Custom Colors', 'pena' ),
		'priority'   => 36,
	) );
	
	$wp_customize->add_section( 'pena_new_section_color_general' , array(
		'title'      => esc_html__( 'General Colors', 'pena' ),
		'panel'           => 'pena_colors_panel',
	) );

	/* Colors General */
	$wp_customize->add_setting( 'pena_bg_color', array(
		'default'           => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_bg_color', array(
		'label'             => esc_html__( 'Background Color', 'pena' ),
		'section'           => 'pena_new_section_color_general',
		'settings'          => 'pena_bg_color',
		'priority'          => 1,
	) ) );
	
	$wp_customize->add_setting( 'pena_blue_colors', array(
		'default'           => '#20c9f3',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_blue_colors', array(
		'label'             => esc_html__( 'All Elements with Accent Color 1', 'pena' ),
		'section'           => 'pena_new_section_color_general',
		'settings'          => 'pena_blue_colors',
		'priority'          => 1,
	) ) );
	
	$wp_customize->add_setting( 'pena_black_button_text_colors', array(
		'default'           => '#000000',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_black_button_text_colors', array(
		'label'             => esc_html__( 'Button with Accent Color 1 - Text Color', 'pena' ),
		'section'           => 'pena_new_section_color_general',
		'settings'          => 'pena_black_button_text_colors',
		'priority'          => 2,
	) ) );
	
	$wp_customize->add_setting( 'pena_purple_colors', array(
		'default'           => '#980560',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_purple_colors', array(
		'label'             => esc_html__( 'All Elements with Accent Color 2', 'pena' ),
		'section'           => 'pena_new_section_color_general',
		'settings'          => 'pena_purple_colors',
		'priority'          => 3,
	) ) );
	
	$wp_customize->add_setting( 'pena_white_button_text_colors', array(
		'default'           => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_white_button_text_colors', array(
		'label'             => esc_html__( 'Button with Accent Color 2 - Text Color', 'pena' ),
		'section'           => 'pena_new_section_color_general',
		'settings'          => 'pena_white_button_text_colors',
		'priority'          => 4,
	) ) );
	
	$wp_customize->add_setting( 'pena_black_colors', array(
		'default'           => '#000000',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_white_colors', array(
		'label'             => esc_html__( 'All Background and Border Elements with Black Color', 'pena' ),
		'section'           => 'pena_new_section_color_general',
		'settings'          => 'pena_black_colors',
		'priority'          => 5,
	) ) );
	
	$wp_customize->add_setting( 'pena_gray_colors', array(
		'default'           => '#262626',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_gray_colors', array(
		'label'             => esc_html__( 'All Elements with Dark Gray Color', 'pena' ),
		'section'           => 'pena_new_section_color_general',
		'settings'          => 'pena_gray_colors',
		'priority'          => 6,
	) ) );
	
	$wp_customize->add_section( 'pena_new_section_color_general_one' , array(
		'title'      => esc_html__( 'Text Color', 'pena' ),
		'panel'           => 'pena_colors_panel',
	) );
	
	$wp_customize->add_setting( 'pena_header_colors', array(
		'default'           => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_header_colors', array(
		'label'             => esc_html__( 'Header Text Color', 'pena' ),
		'section'           => 'pena_new_section_color_general_one',
		'settings'          => 'pena_header_colors',
		'priority'          => 1,
	) ) );
	
	$wp_customize->add_setting( 'pena_footer_colors', array(
		'default'           => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_footer_colors', array(
		'label'             => esc_html__( 'Footer Text Color', 'pena' ),
		'section'           => 'pena_new_section_color_general_one',
		'settings'          => 'pena_footer_colors',
		'priority'          => 2,
	) ) );
	
	$wp_customize->add_setting( 'pena_page_colors', array(
		'default'           => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'pena_page_colors', array(
		'label'             => esc_html__( 'Page White Text Color', 'pena' ),
		'section'           => 'pena_new_section_color_general_one',
		'settings'          => 'pena_page_colors',
		'priority'          => 3,
	) ) );
	
	
	/**
* Adds the individual sections for footer
*/
	$wp_customize->add_section( 'pena_copyright_section' , array(
   		'title'    => esc_html__( 'Copyright Settings', 'pena' ),
   		'description' => esc_html__( 'This is a settings section.', 'pena' ),
   		'priority'   => 302,
	) );

	$wp_customize->add_setting( 'pena_copyright', array(
		'default'           => esc_html__( 'Pena Theme by Anariel Design. All rights reserved', 'pena' ),
		'sanitize_callback' => 'pena_sanitize_text',
	) );
	$wp_customize->add_control( 'pena_copyright', array(
		'label'             => esc_html__( 'Copyright text', 'pena' ),
		'section'           => 'pena_copyright_section',
		'settings'          => 'pena_copyright',
		'type'		        => 'text',
		'priority'          => 1,
	) );

	$wp_customize->add_setting( 'hide_copyright', array(
		'sanitize_callback' => 'pena_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'hide_copyright', array(
		'label'             => esc_html__( 'Hide copyright text', 'pena' ),
		'section'           => 'pena_copyright_section',
		'settings'          => 'hide_copyright',
		'type'		        => 'checkbox',
		'priority'          => 1,
	) );
		/***** Register Custom Controls *****/

	class Pena_Upgrade extends WP_Customize_Control {
		public function render_content() {  ?>
			<p class="pena-upgrade-thumb">
				<img src="<?php echo get_template_directory_uri(); ?>/screenshot.png" />
			</p>
			<p class="textfield pena-upgrade-text">
				<a href="<?php echo esc_url('http://www.anarieldesign.com/documentation/pena/'); ?>" target="_blank" class="button button-secondary">
					<?php esc_html_e('Visit Documentation', 'pena'); ?>
				</a>
			</p>
			<p class="customize-control-title pena-upgrade-title">
				<a href="<?php echo esc_url('https://www.youtube.com/watch?v=O6UyKaxwC2Y'); ?>" class="button button-secondary" target="_blank">
					<?php esc_html_e('Video Presentation', 'pena'); ?>
				</a>
			</p>
			<p class="pena-upgrade-button">
				<a href="http://www.anarieldesign.com/themes/" target="_blank" class="button button-secondary">
					<?php esc_html_e('More Themes by Anariel Design', 'pena'); ?>
				</a>
			</p><?php
		}
	}

	/***** Add Sections *****/

	$wp_customize->add_section('pena_upgrade', array(
		'title' => esc_html__('Theme Info', 'pena'),
		'priority' => 600
	) );

	/***** Add Settings *****/

	$wp_customize->add_setting('pena_options[premium_version_upgrade]', array(
		'default' => '',
		'type' => 'option',
		'sanitize_callback' => 'esc_attr'
	) );

	/***** Add Controls *****/

	$wp_customize->add_control(new Pena_Upgrade($wp_customize, 'premium_version_upgrade', array(
		'section' => 'pena_upgrade',
		'settings' => 'pena_options[premium_version_upgrade]',
		'priority' => 1
	) ) );
}
add_action( 'customize_register', 'pena_customize_register' );

/**
 * Sanitization
 */
//Checkboxes
function pena_sanitize_checkbox( $input ) {
	if ( $input == 1 ) {
		return 1;
	} else {
		return '';
	}
}
//Integers
function pena_sanitize_int( $input ) {
	if( is_numeric( $input ) ) {
		return intval( $input );
	}
}
//Text
function pena_sanitize_text( $input ) {
	return wp_kses_post( force_balance_tags( $input ) );
}
//Radio Buttons and Select Lists
function pena_sanitize_choices( $input, $setting ) {
	global $wp_customize;

	$control = $wp_customize->get_control( $setting->id );

	if ( array_key_exists( $input, $control->choices ) ) {
		return $input;
	} else {
		return $setting->default;
	}
}

//Sanitize the dropdown pages.
function pena_sanitize_dropdown_pages( $input ) {
	if ( is_numeric( $input ) ) {
		return intval( $input );
	}
}

//Shop section
function pena_is_meta_active(){
	if( !class_exists( 'WooCommerce' ) ){
		// If it doesn't exist it won't show the section/panel/control
	return false;
	} else {
		return true;
	}
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function pena_customize_preview_js() {
	wp_enqueue_script( 'pena-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20150908', true );
}
add_action( 'customize_preview_init', 'pena_customize_preview_js' );

/***** Enqueue Customizer JS *****/

function pena_customizer_js() {
	wp_enqueue_script('pena-customizer', get_template_directory_uri() . '/js/pena-customizer.js', array(), '1.0.0', true);
	wp_localize_script('pena-customizer', 'pena_links', array(
		'title'	=> esc_html__('Theme Related Links:', 'pena'),
		'themeURL' => esc_url('http://www.anarieldesign.com/themes/pena-charity-wordpress-theme/'),
		'themeLabel' => esc_html__('Theme Info Page', 'pena'),
		'docsURL' => esc_url('http://www.anarieldesign.com/documentation/pena/'),
		'docsLabel'	=> esc_html__('Theme Documentation', 'pena'),
		'rateURL' => esc_url('https://www.youtube.com/watch?v=O6UyKaxwC2Y'),
		'rateLabel'	=> esc_html__('Video Presentation', 'pena'),
	));
}
add_action('customize_controls_enqueue_scripts', 'pena_customizer_js');