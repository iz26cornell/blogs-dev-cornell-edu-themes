<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Pena
 */

if ( ! function_exists( 'pena_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function pena_posted_on() {
	list( $byline, $posted_on ) = pena_posted_on_custom( true );

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', 'pena' ), esc_html__( '1 Comment', 'pena' ), esc_html__( '% Comments', 'pena' ) );
		echo '</span>';
	}

	if ( ! is_single() ) {
		edit_post_link( sprintf( esc_html__( 'Edit %1$s', 'pena' ), '<span class="screen-reader-text">' . the_title_attribute( 'echo=0' ) . '</span>' ), '<span class="edit-link">', '</span>' );
	}
}
endif;

if ( ! function_exists( 'pena_posted_on_custom' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function pena_posted_on_custom( $return=false ) {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	if ( is_single() ) {
		$posted_on = sprintf( esc_attr__( 'Posted on %1$s', 'pena' ),
						'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
					);
	}
	else {
		$posted_on = '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>';
	}

	if ( is_single() ) {
		$byline = sprintf( esc_html__( 'by %1$s', 'pena' ),
					'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
				);
	}
	else {
		$byline = '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>';
	}

	if ( $return ) {
		return array( $byline, $posted_on );
	} else {
		echo '<p><span class="posted-on">' . $posted_on . '</span></p>'; // WPCS: XSS OK.
	}

}
endif;

if ( ! function_exists( 'pena_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function pena_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' == get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'pena' ) );
		if ( $categories_list && pena_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'pena' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'pena' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'pena' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	edit_post_link( sprintf( esc_html__( 'Edit %1$s', 'pena' ), '<span class="screen-reader-text">' . the_title_attribute( 'echo=0' ) . '</span>' ), '<span class="edit-link">', '</span>' );
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function pena_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'pena_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'pena_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so pena_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so pena_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in pena_categorized_blog.
 */
function pena_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'pena_categories' );
}
add_action( 'edit_category', 'pena_category_transient_flusher' );
add_action( 'save_post',     'pena_category_transient_flusher' );
/**
 * Front Page - Featured Page One
 */
function pena_featured_page_one() {
	$featured_page_1 = esc_attr( get_theme_mod( 'pena_featured_page_one', '0' ) );
	if ( 0 == $featured_page_1 ) {
		return;
	}
?>
<div class="block-one">
	<div class="hfeed site">
		<div class="content site-content">
			<div class="content-area">
				<div class="main site-main" role="main">
					<?php for ( $page_number = 1; $page_number <= 1; $page_number++ ) : ?>
						<?php if ( 0 != ${'featured_page_' . $page_number} ) : // Check if a featured page has been set in the customizer ?>
								<?php
									// Create new argument using the page ID of the page set in the customizer
									$featured_page_args = array(
										'page_id' => ${'featured_page_' . $page_number},
									);
									// Create a new WP_Query using the argument previously created
									$featured_page_query = new WP_Query( $featured_page_args );
								?>
								<?php while ( $featured_page_query->have_posts() ) : $featured_page_query->the_post(); ?>
									<?php get_template_part( 'template-parts/content', 'front-firstblock' ); ?>
								<?php
									endwhile;
									wp_reset_postdata();
								?>
						<?php endif; ?>
					<?php endfor; ?>
				</div><!-- .site-main -->
			</div><!-- .content-area -->
		</div><!-- .site-content -->
	</div><!-- .site -->
</div><!-- .block-one -->
<?php
}
/**
 * Front Page - Featured Page Two
 */
function pena_featured_page_two() {
	$featured_page_1 = esc_attr( get_theme_mod( 'pena_featured_page_two', '0' ) );
	if ( 0 == $featured_page_1 ) {
		return;
	}
?>
<div class="second-block">
	<?php for ( $page_number = 1; $page_number <= 1; $page_number++ ) : ?>
		<?php if ( 0 != ${'featured_page_' . $page_number} ) : // Check if a featured page has been set in the customizer ?>
				<?php
					// Create new argument using the page ID of the page set in the customizer
					$featured_page_args = array(
						'page_id' => ${'featured_page_' . $page_number},
					);
					// Create a new WP_Query using the argument previously created
					$featured_page_query = new WP_Query( $featured_page_args );
				?>
				<?php while ( $featured_page_query->have_posts() ) : $featured_page_query->the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'front-secondblock' ); ?>
				<?php
					endwhile;
					wp_reset_postdata();
				?>
		<?php endif; ?>
	<?php endfor; ?>
</div><!-- .second-block -->
<?php
}
/**
 * Front Page - Featured Page Three
 */
function pena_featured_page_three() {
	$featured_page_1 = esc_attr( get_theme_mod( 'pena_featured_page_three', '0' ) );
	if ( 0 == $featured_page_1 ) {
		return;
	}
?>
<div class="block-three">
	<div class="hfeed site">
		<div class="content site-content">
			<div class="content-area">
				<div class="main site-main" role="main">
					<?php for ( $page_number = 1; $page_number <= 1; $page_number++ ) : ?>
					<?php if ( 0 != ${'featured_page_' . $page_number} ) : // Check if a featured page has been set in the customizer ?>
							<?php
								// Create new argument using the page ID of the page set in the customizer
								$featured_page_args = array(
									'page_id' => ${'featured_page_' . $page_number},
								);
								// Create a new WP_Query using the argument previously created
								$featured_page_query = new WP_Query( $featured_page_args );
							?>
							<?php while ( $featured_page_query->have_posts() ) : $featured_page_query->the_post(); ?>
								<?php get_template_part( 'template-parts/content', 'front-thirdblock' ); ?>
							<?php
								endwhile;
								wp_reset_postdata();
							?>
					<?php endif; ?>
					<?php endfor; ?>
				</div><!-- .site-main -->
			</div><!-- .content-area -->
		</div><!-- .site-content -->
	</div><!-- .site -->
</div><!-- .block-one -->
<?php
}
/**
 * Front Page - Featured Page Four
 */
function pena_featured_page_four() {
	$featured_page_1 = esc_attr( get_theme_mod( 'pena_featured_page_four', '0' ) );
	if ( 0 == $featured_page_1 ) {
		return;
	}
?>
<section class="info">
	<?php for ( $page_number = 1; $page_number <= 1; $page_number++ ) : ?>
		<?php if ( 0 != ${'featured_page_' . $page_number} ) : // Check if a featured page has been set in the customizer ?>
				<?php
					// Create new argument using the page ID of the page set in the customizer
					$featured_page_args = array(
						'page_id' => ${'featured_page_' . $page_number},
					);
					// Create a new WP_Query using the argument previously created
					$featured_page_query = new WP_Query( $featured_page_args );
				?>
				<?php while ( $featured_page_query->have_posts() ) : $featured_page_query->the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'front-fourthblock' ); ?>
				<?php
					endwhile;
					wp_reset_postdata();
				?>
		<?php endif; ?>
	<?php endfor; ?>
</section><!-- .second-block -->
<?php
}
/**
 * Front Page - Featured Page Five
 */
function pena_featured_page_five() {
	$featured_page_1 = esc_attr( get_theme_mod( 'pena_featured_page_five', '0' ) );
	if ( 0 == $featured_page_1 ) {
		return;
	}
?>
<div class="block-five">
	<?php for ( $page_number = 1; $page_number <= 1; $page_number++ ) : ?>
		<?php if ( 0 != ${'featured_page_' . $page_number} ) : // Check if a featured page has been set in the customizer ?>
				<?php
					// Create new argument using the page ID of the page set in the customizer
					$featured_page_args = array(
						'page_id' => ${'featured_page_' . $page_number},
					);
					// Create a new WP_Query using the argument previously created
					$featured_page_query = new WP_Query( $featured_page_args );
				?>
				<?php while ( $featured_page_query->have_posts() ) : $featured_page_query->the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'front-fifthblock' ); ?>
				<?php
					endwhile;
					wp_reset_postdata();
				?>
		<?php endif; ?>
	<?php endfor; ?>
</div><!-- .second-block -->
<?php
}
/**
 * About Page - Featured Page One
 */
function pena_featured_page_six() {
	$featured_page_1 = esc_attr( get_theme_mod( 'pena_featured_page_six', '0' ) );
	if ( 0 == $featured_page_1 ) {
		return;
	}
?>
<div class="block-five">
	<?php for ( $page_number = 1; $page_number <= 1; $page_number++ ) : ?>
		<?php if ( 0 != ${'featured_page_' . $page_number} ) : // Check if a featured page has been set in the customizer ?>
				<?php
					// Create new argument using the page ID of the page set in the customizer
					$featured_page_args = array(
						'page_id' => ${'featured_page_' . $page_number},
					);
					// Create a new WP_Query using the argument previously created
					$featured_page_query = new WP_Query( $featured_page_args );
				?>
				<?php while ( $featured_page_query->have_posts() ) : $featured_page_query->the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'about-firstblock' ); ?>
				<?php
					endwhile;
					wp_reset_postdata();
				?>
		<?php endif; ?>
	<?php endfor; ?>
</div><!-- .second-block -->
<?php
}
/**
 * About Page - Featured Page Two
 */
function pena_featured_page_seven() {
	$featured_page_1 = esc_attr( get_theme_mod( 'pena_featured_page_seven', '0' ) );
	if ( 0 == $featured_page_1 ) {
		return;
	}
?>
<div class="block-six">
	<?php for ( $page_number = 1; $page_number <= 1; $page_number++ ) : ?>
		<?php if ( 0 != ${'featured_page_' . $page_number} ) : // Check if a featured page has been set in the customizer ?>
				<?php
					// Create new argument using the page ID of the page set in the customizer
					$featured_page_args = array(
						'page_id' => ${'featured_page_' . $page_number},
					);
					// Create a new WP_Query using the argument previously created
					$featured_page_query = new WP_Query( $featured_page_args );
				?>
				<?php while ( $featured_page_query->have_posts() ) : $featured_page_query->the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'about-secondblock' ); ?>
				<?php
					endwhile;
					wp_reset_postdata();
				?>
		<?php endif; ?>
	<?php endfor; ?>
</div><!-- .second-block -->
<?php
}
/**
 * About Page - Featured Page Three
 */
function pena_featured_page_eight() {
	$featured_page_1 = esc_attr( get_theme_mod( 'pena_featured_page_eight', '0' ) );
	if ( 0 == $featured_page_1 ) {
		return;
	}
?>
<div class="block-five">
	<?php for ( $page_number = 1; $page_number <= 1; $page_number++ ) : ?>
		<?php if ( 0 != ${'featured_page_' . $page_number} ) : // Check if a featured page has been set in the customizer ?>
				<?php
					// Create new argument using the page ID of the page set in the customizer
					$featured_page_args = array(
						'page_id' => ${'featured_page_' . $page_number},
					);
					// Create a new WP_Query using the argument previously created
					$featured_page_query = new WP_Query( $featured_page_args );
				?>
				<?php while ( $featured_page_query->have_posts() ) : $featured_page_query->the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'about-firstblock' ); ?>
				<?php
					endwhile;
					wp_reset_postdata();
				?>
		<?php endif; ?>
	<?php endfor; ?>
</div><!-- .second-block -->
<?php
}
/**
 * Involved Page - Featured Page One
 */
function pena_featured_page_nine() {
	$featured_page_1 = esc_attr( get_theme_mod( 'pena_featured_page_nine', '0' ) );
	if ( 0 == $featured_page_1 ) {
		return;
	}
?>
<div class="block-five">
	<?php for ( $page_number = 1; $page_number <= 1; $page_number++ ) : ?>
		<?php if ( 0 != ${'featured_page_' . $page_number} ) : // Check if a featured page has been set in the customizer ?>
				<?php
					// Create new argument using the page ID of the page set in the customizer
					$featured_page_args = array(
						'page_id' => ${'featured_page_' . $page_number},
					);
					// Create a new WP_Query using the argument previously created
					$featured_page_query = new WP_Query( $featured_page_args );
				?>
				<?php while ( $featured_page_query->have_posts() ) : $featured_page_query->the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'about-firstblock' ); ?>
				<?php
					endwhile;
					wp_reset_postdata();
				?>
		<?php endif; ?>
	<?php endfor; ?>
</div><!-- .second-block -->
<?php
}
if ( ! function_exists( 'pena_the_custom_logo' ) ) :
/**
 * Displays the optional custom logo.
 *
 * Does nothing if the custom logo is not available.
 *
 * @since Pena 1.0
 */
function pena_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}
endif;