<?php
/**
 * Template Name: WooCommerce Shop Template
 *
 * @package Pena
 */
?>
<?php get_header(); ?>
	<div id="shop" class="singular site blog">
		<div class="content site-content">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
					<?php woocommerce_breadcrumb(); ?>
					<?php woocommerce_content(); ?>

				</main><!-- #main -->
			</div><!-- #primary -->
			<?php get_sidebar(); ?>
		</div><!-- .content -->
	</div><!-- .site -->
<?php get_footer(); ?>
