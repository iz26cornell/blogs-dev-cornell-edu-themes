<?php
/**
 * Template Name: Splash Template
 * The template for displaying a front page.
 *
 * @package Pena
 */

get_header( 'splash' ); ?>
	<div class="intro">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'splash' );

		// End the loop.
		endwhile;
		?>
	</div><!-- .intro -->
	<div class="inner">
		<div class="inner-page">
		<div class="posts columns customwidget clear">
			<?php
				$child_pages = new WP_Query( array(
					'post_type'      => 'page',
					'orderby'        => 'menu_order',
					'order'          => 'ASC',
					'post_parent'    => $post->ID,
					'posts_per_page' => 999,
					'no_found_rows'  => true,
				) );
			while ( $child_pages->have_posts() ) : $child_pages->the_post();
				 get_template_part( 'template-parts/content', 'splash-page' );
			endwhile;
			wp_reset_postdata();
			?>
		</div><!-- .child-pages -->
		</div>
	</div>
<?php get_footer( 'splash' ); ?>