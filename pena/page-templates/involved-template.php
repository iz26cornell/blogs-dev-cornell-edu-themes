<?php
/**
 * Template Name: Involved Template
 * The template for displaying a front page.
 *
 * @package Pena
 */

get_header(); ?>
	<div class="involved">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'involved-firstblock' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

		<?php endwhile; // End of the loop. ?>
	</div>
	<?php if( get_theme_mod( 'pena_featured_page_nine' ) ) : ?>
	<?php pena_featured_page_nine(); ?>
	<?php endif; ?>
<?php get_footer(); ?>