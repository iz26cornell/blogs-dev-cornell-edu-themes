<?php
/**
 * Template Name: Full Width Boxed Template
 * The template for displaying a full width page.
 *
 * @package Pena
 */

get_header(); ?>

	<div class="hfeed site blog">
		<div class="content site-content">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
					<div class="main-content">
						<?php
						// Start the loop.
						while ( have_posts() ) : the_post();

							// Include the page content template.
							get_template_part( 'template-parts/content', 'page-boxed' );

						// End the loop.
						endwhile;
						?>
						<?php
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>

					</div><!-- .main-content -->

				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- .content -->
	</div><!-- .site -->

<?php get_footer(); ?>