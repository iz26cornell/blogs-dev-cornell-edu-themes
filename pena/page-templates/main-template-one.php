<?php
/**
 * Template Name: Front Template with Slider
 * The template for displaying a front page.
 *
 * @package Pena
 */

get_header( 'custom' ); ?>
	<div class="intro">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'front-page-one' );

		// End the loop.
		endwhile;
		?>
	</div><!-- .intro -->
	<div class="inner">
		<div class="inner-page">
			<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
			<div class="widgetized-content">
				<div class="widget-area-front top-part" role="complementary">
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</div><!-- widget-area-front -->
			</div><!-- .widgetized-content -->
			<?php endif; ?>
			<?php if( get_theme_mod( 'pena_featured_page_one' ) ) : ?>
			<?php pena_featured_page_one(); ?>
			<?php endif; ?>
			<?php if( get_theme_mod( 'pena_featured_page_two' ) ) : ?>
			<?php pena_featured_page_two(); ?>
			<?php endif; ?>
			<?php if( get_theme_mod( 'pena_featured_page_three' ) ) : ?>
			<?php pena_featured_page_three(); ?>
			<?php endif; ?>
			<?php if( get_theme_mod( 'pena_featured_page_four' ) ) : ?>
			<?php pena_featured_page_four(); ?>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
			<div class="clear site news">
				<div class="two-third main">
				<div class="widgetized-content">
					<div class="widget-area-front" role="complementary">
						<?php dynamic_sidebar( 'sidebar-3' ); ?>
					</div><!-- #secondary -->
				</div><!-- .block-four -->
				</div>
				<div class="one-third main lastcolumn secondblock">
				<?php get_sidebar(); ?>
				</div></div>
			<?php endif; ?>
			<?php if( get_theme_mod( 'pena_featured_page_five' ) ) : ?>
			<?php pena_featured_page_five(); ?>
			<?php endif; ?>
<?php get_footer( 'custom' ); ?>