<?php get_header(); ?>
            
<?php get_header( 'calendar-plus' ); ?>

<?php do_action( 'calendarp_before_content' ); ?>

<div id="content">

	<div id="main">
    
    	<div id="main-top"></div>
        
		<?php if ( is_active_sidebar( 'sidebar-3' ) ) { ?>
            <div id="secondary-nav">
                <div class="main-body">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        <?php } ?>
        
        <div id="main-body">

			<div class="calendarp-breadcrumbs row">
				<div class="large-12">
					<?php echo '<a href="' . home_url() . '">Home</a> ' . calendarp_breadcrumbs(); ?>
				</div>
			</div>
			
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<?php calendarp_get_template_part( 'content', 'event' ); ?>
				<?php /*?><?php cornell_base_post_nav(); ?><?php */?>
				<?php /*?><?php comments_template(); ?><?php */?>
			<?php endwhile; ?>
		</div><!-- #main-body -->
        
            <?php if ( is_active_sidebar( 'sidebar-4' ) ) { ?>
                <div id="secondary">
                    <div class="main-body">
                        <?php get_sidebar('secondary'); ?>
                    </div>
                </div>
            <?php } ?>

        <div id="main-bottom"></div>
        
	</div><!-- #main -->
    
</div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- #wrap -->


<?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
    <div class="colorband tint-one">
        <div class="colorband-content">
            <?php if ( get_theme_mod('section_one') != '' ) : ?><h2 class="section-title"><span><?php echo get_theme_mod('section_one'); ?></span></h2><?php endif; ?>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-5' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'sidebar-6' ) ) : ?>
    <div class="colorband tint-two">
        <div class="colorband-content">
            <?php if ( get_theme_mod('section_two') != '' ) : ?><h2 class="section-title"><span><?php echo get_theme_mod('section_two'); ?></span></h2><?php endif; ?>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-6' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'sidebar-7' ) ) : ?>
    <div class="colorband tint-three">
        <div class="colorband-content">
            <?php if ( get_theme_mod('section_three') != '' ) : ?><h2 class="section-title"><span><?php echo get_theme_mod('section_three'); ?></span></h2><?php endif; ?>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-7' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>


<?php do_action( 'calendarp_after_content' ); ?>

<?php do_action( 'calendarp_sidebar' ); ?>

<?php get_footer( 'calendar-plus' ); ?>

<?php get_footer(); ?>