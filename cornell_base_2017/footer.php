
<div class="site-footer">

  <?php if ( is_active_sidebar( 'sidebar-8' ) ) : ?>
  <div id="footer">
    <div class="footer-content">
      <div class="columns">
	      <?php dynamic_sidebar( 'sidebar-8' ); ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <div id="subfooter">
    <div class="footer-content">
      <div class="columns">
        <div class="col-item">
          <ul class="social-links">
            <?php 
				if ( get_theme_mod( 'facebook' ) ) {  
            		echo '<li class="facebook"> <a href="' . esc_url( get_theme_mod( 'facebook', 'http://facebook.com' ) ) . '"> <span class="fa fa-fw fa-facebook"> <span>Facebook</span> </span> </a> </li>'; }
				if ( get_theme_mod( 'twitter' ) ) {  
            		echo '<li class="twitter"> <a href="' . esc_url( get_theme_mod( 'twitter', 'http://twitter.com' ) ) . '"> <span class="fa fa-fw fa-twitter"> <span>Twitter</span> </span> </a> </li>'; }
				if ( get_theme_mod( 'google_plus' ) ) {  
            		echo '<li class="google_plus"> <a href="' . esc_url( get_theme_mod( 'google_plus', 'https://plus.google.com' ) ) . '"> <span class="fa fa-fw fa-google-plus"> <span>Google Plus</span> </span> </a> </li>'; }
				if ( get_theme_mod( 'pinterest' ) ) {  
            		echo '<li class="pinterest"> <a href="' . esc_url( get_theme_mod( 'pinterest' ) ) . '"> <span class="fa fa-fw fa-pinterest"> <span>Pinterest</span> </span> </a> </li>'; }
				if ( get_theme_mod( 'tumblr' ) ) {  
            		echo '<li class="tumblr"> <a href="' . esc_url( get_theme_mod( 'tumblr' ) ) . '"> <span class="fa fa-fw fa-tumblr"> <span>Tumblr</span> </span> </a> </li>'; }
				if ( get_theme_mod( 'flickr' ) ) {  
            		echo '<li class="flickr"> <a href="' . esc_url( get_theme_mod( 'flickr' ) ) . '"> <span class="fa fa-fw fa-flickr"> <span>Flickr</span> </span> </a> </li>'; }
				if ( get_theme_mod( 'linkedin' ) ) {  
            		echo '<li class="linkedin"> <a href="' . esc_url( get_theme_mod( 'linkedin' ) ) . '"> <span class="fa fa-fw fa-linkedin"> <span>Linked In</span> </span> </a> </li>'; }
				if ( get_theme_mod( 'instagram' ) ) {  
            		echo '<li class="instagram"> <a href="' . esc_url( get_theme_mod( 'instagram' ) ) . '"> <span class="fa fa-fw fa-instagram"> <span>Instagram</span> </span> </a> </li>'; }
				if ( get_theme_mod( 'youtube' ) ) {  
            		echo '<li class="youtube"> <a href="' . esc_url( get_theme_mod( 'youtube' ) ) . '"> <span class="fa fa-fw fa-youtube"> <span>YouTube</span> </span> </a> </li>'; }
				if ( get_theme_mod( 'vimeo' ) ) {  
            		echo '<li class="vimeo"> <a href="' . esc_url( get_theme_mod( 'vimeo' ) ) . '"> <span class="fa fa-fw fa-vimeo-square"> <span>Vimeo</span> </span> </a> </li>'; }
			  ?>
          </ul>
          <p id="copy"><a class="copy" href="https://www.cornell.edu/copyright.cfm">&copy; <?php echo date('Y'); ?> Cornell University</a></p>
        </div>
        <div class="col-item">
			<p>If you have a disability and are having trouble accessing information on this website or need materials in an alternate format, contact <a href="mailto:web-accessibility@cornell.edu" id="access-help" title="Web Accessibility Help">web-accessibility@cornell.edu</a> for assistance.</p>
		</div>
      </div>
    </div>
  </div>
	  
</div>

<div id="cu-overlay"></div>
	<?php wp_footer(); ?>
</body>
</html>
