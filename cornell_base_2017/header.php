<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        
        <!-- CU Typekit fonts -->
        <script type="text/javascript" src="//use.typekit.net/gog6dck.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
        
        <link rel="stylesheet" type="text/css" media="print" href="<?php echo get_template_directory_uri(); ?>/style/print.css" />
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" />
        
        <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/style/ie.css" />
            <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
        <![endif]-->
        <!--[if gte IE 9]>
          <style type="text/css">
            .gradient {
               filter: none;
            }
          </style>
        <![endif]-->
        
        <?php         
			if (isset($_GET['btnG'])) {
					$selected_radio = $_GET['sitesearch'];
					
					if ($selected_radio == 'cornell') {
						$search_terms = urlencode($_GET['s']);
						$location = "http://www.cornell.edu/search/" . "?q=" . $search_terms;
						wp_redirect($location);
					}
			}
		?>

        <?php wp_head(); ?>
    
    </head>
    
    <body <?php if ( get_theme_mod('custom_banner') == '' ) { body_class('theme_red_45'); } elseif ( get_theme_mod('custom_banner') == 'theme_white75' ) { body_class('theme_white_75'); } else { body_class(); } ?>>
    <div id="skipnav"><a href="#content">Skip to main content</a></div>
        <div id="search-box">
          <div id="search-box-content">
            <div id="cu-search" class="options">
              <form method="get" action="<?php echo home_url( '/' ); ?>">
                <div id="search-form">
                  <label class="screen-reader-text" for="search-form-query">SEARCH:</label>
                  <input type="text" id="search-form-query" name="s" value="" size="20" onblur="this.value = this.value || this.defaultValue;" onfocus="this.value == this.defaultValue && (this.value =''); this.select()" />
                  <input type="submit" id="search-form-submit" name="btnG" value="go" />
                  <div id="search-filters" role="radiogroup" aria-label="Choose a search location">
                    <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
                    <label for="search-filters1">This Site</label>
                    <input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
                    <label for="search-filters2">Cornell</label>
                    <a href="http://www.cornell.edu/search/" title="more options">more options</a> </div>
                </div>
              </form>
            </div>
          </div><!-- #search-box-content -->
        </div><!-- #search-box -->

        <div id="cu-identity" class="<?php echo get_theme_mod('custom_banner', 'theme_red45'); if ( get_theme_mod('display_tagline') != 1 || get_bloginfo( 'description' ) == '' ) { echo ' no-tagline'; } else { echo ' tagline'; } ?>">
            <div id="cu-identity-content">
                <div id="cu-brand-wrapper">
                    <div id="cu-brand">
                        <?php if ( get_theme_mod('custom_banner') == 'theme_white75' ) : ?> <!-- large insignia layout -->
                        <div id="search-button">
                            <p><a href="#search-form" title="Search This Site">Search This Site</a></p>
                        </div>
                        <div id="top-navigation-bar">
                            <div id="top-navigation-wrap">
                                <div id="top-navigation">
                                    <button id="menu-button"><em>menu</em></button><!-- mobile drop down navigation -->
                                    <?php $menu1 = get_theme_menu_name( 'top-menu' ); $menu2 = get_theme_menu_name( 'primary' ); 
                                          if ($menu1 == $menu2) {  //in the [unlikely] event that the same menu is in both locations, only display one
                                            wp_nav_menu( array( 'theme_location' => 'top-menu', 'fallback_cb' => '', 'menu_class' => 'top nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) );
                                          }  
                                          else {   //otherwise display both
                                            wp_nav_menu( array( 'theme_location' => 'top-menu', 'fallback_cb' => '', 'menu_class' => 'top nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) ); 
                                            wp_nav_menu( array( 'theme_location' => 'primary', 'fallback_cb' => '', 'menu_class' => 'main nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) ); 
                                          } 
                                    ?>
                                </div>
                            </div>
                        </div><!-- #navigation-bar -->
                        <div id="cu-seal">&nbsp;<a href="http://www.cornell.edu/" title="Cornell University">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cornell_identity/<?php echo get_theme_mod('custom_banner', 'theme_red45'); ?>.png" alt="Cornell University" /></a>
                        </div>
                        <div id="custom-branding" class="<?php if ( get_theme_mod('display_site_name') == 1 ) { echo ' site-title'; } else { echo ' no-site-title'; } ?>"> 
                        	<?php if ( get_theme_mod('display_site_name') == 1 ) { ?>
                            	<div id="site-title">
                                	<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" style="color: <?php echo get_theme_mod('site_name_color', '#2C6C8C'); ?>">
										<?php bloginfo( 'name' ); ?>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if ( get_theme_mod('display_tagline') == 1 ) { ?>
                                <div id="site-description" style="color: <?php echo get_theme_mod('tagline_color', '#000000'); ?>"><?php bloginfo( 'description' ); ?></div>
                            <?php } ?>
                        </div><!-- #custom-branding -->
                    </div><!-- #cu-brand -->
                </div><!-- #cu-brand-wrapper -->
                        <?php elseif ( get_theme_mod('custom_banner') != 'theme_white75' ) : ?> <!-- small banner layout -->
                        <div id="cu-seal"><a href="http://www.cornell.edu/" title="Cornell University">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cornell_identity/<?php echo get_theme_mod('custom_banner', 'theme_red45'); ?>_white.png" alt="Cornell University" /></a>
                        </div>
                        <div id="search-button">
                            <p><a href="#search-box" title="Search This Site">Search This Site</a></p>
                        </div>
                        <div id="top-navigation-bar">
                            <div id="top-navigation-wrap">
                                <div id="top-navigation">
                                    <button id="menu-button"><em>menu</em></button><!-- mobile drop down navigation -->
                                    <?php $menu1 = get_theme_menu_name( 'top-menu' ); $menu2 = get_theme_menu_name( 'primary' );
											  if ($menu1 == $menu2) {  //in the [unlikely] event that the same menu is in both locations, only display one
												wp_nav_menu( array( 'theme_location' => 'top-menu', 'fallback_cb' => '', 'menu_class' => 'top nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) );
											  }  
											  else {   //otherwise display both
												wp_nav_menu( array( 'theme_location' => 'top-menu', 'fallback_cb' => '', 'menu_class' => 'top nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) ); 
												wp_nav_menu( array( 'theme_location' => 'primary', 'fallback_cb' => '', 'menu_class' => 'main nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) ); 
											  }
                                    ?>
                                </div>
                            </div>
                        </div><!-- #navigation-bar -->
                    </div><!-- #cu-brand -->
                </div><!-- #cu-brand-wrapper -->
				<div id="custom-branding" class="<?php if ( get_theme_mod('display_site_name') == 1 ) { echo ' site-title'; } else { echo ' no-site-title'; } ?>"> 
					<?php if ( get_theme_mod('display_site_name') == 1 ) { ?>
                        <div id="site-title">
                            <a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" style="color: <?php echo get_theme_mod('site_name_color', '#2C6C8C'); ?>">
                                <?php bloginfo( 'name' ); ?>
                            </a>
                        </div>
                    <?php } ?>
                    <?php if ( get_theme_mod('display_tagline') == 1 ) { ?>
                    	<div id="site-description" style="color: <?php echo get_theme_mod('tagline_color', '#000000'); ?>"><?php bloginfo( 'description' ); ?></div>
					<?php } ?>
               </div><!-- #custom-branding -->
               
               <?php endif; ?> <!-- end banner layouts -->
               
            </div><!-- #cu-identity-content -->
        </div><!-- #cu-identity -->
        
		<div id="wrap">
            <div id="header">
                <div id="header-band">
                    <div id="identity">
                        <div id="navigation-bar">
                            <div id="navigation-wrap">
                                <div id="navigation">
                                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'fallback_cb' => '', 'menu_class' => 'main nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="image-band" 
                    <?php 
                          $header_image = get_header_image(); 
                          $hide_header = get_theme_mod('remove_header', 0); 
                          $hide_header_by_page = get_post_meta( get_the_ID(), 'remove_this_header', true );
                          echo 'class="'; 
                          if ( empty( $header_image ) ) { 
                            echo 'no-header-image'; 
                          } 
                  		  if ( ( ($hide_header == 1) && ($hide_header_by_page == '') ) || (($hide_header == 1) && ($hide_header_by_page == 'Yes')) || (($hide_header == 0) && ($hide_header_by_page == 'Yes')) ) { 
                            echo 'hideHeader'; 
                          } 
                          echo '"'; 
                    ?>>
                    <div class="image-meta">
                        <div class="image-subtitle">
                            <?php if ( is_active_sidebar( 'sidebar-1' ) && is_front_page() ) { //header widget ?>
                                <div id="tertiary3" class="sidebar-container" role="complementary">
                                    <div class="sidebar-inner">
                                        <div class="widget-area">
                                            <?php dynamic_sidebar( 'sidebar-1' ); ?>
                                        </div><!-- .widget-area -->
                                    </div><!-- .sidebar-inner -->
                                </div><!-- #tertiary -->
                            <?php } ?>
                            
                            <?php if ( !is_search() && !is_home() && !is_front_page() && !is_archive() ) { //page title ?>
                            <a href="<?php echo empty( $post->post_parent ) ? get_permalink( $post->ID ) : get_permalink( $post->post_parent ); ?>" title="<?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?>"><span class="main_title"><?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?></span></a><?php } ?>
                            
                            <?php $blog_title = get_the_title( get_option('page_for_posts', true) ); ?>
                            <?php if ( is_home() && !is_front_page() ) { ?><span class="main_title"><?php echo $blog_title; ?></span><?php } //blog page title ?>
                            <?php if ( is_tag() ) { ?><span class="main_title"><?php printf( __( 'Posts Tagged: %s', 'cornell_base' ), single_tag_title( '', false ) ); ?></span><?php } //tag title ?>
                            <?php if ( is_archive() && !is_tag() ) { ?><span class="main_title"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></span><?php } //archive title ?>
                            <?php if ( is_search() ) { ?><span class="main_title"><?php printf( __( 'Search Results for: %s', 'cornell_base' ), get_search_query() ); ?></span><?php } //search page title ?>
                        </div>
                        
                     </div>

                    <div class="image-cover gradient"></div>
                    <div class="text-overlay gradient"></div>
                </div><!-- #image-band -->
                
            </div><!-- end #header -->
        
            <div id="midband-wrap" class="">
                <div id="midband"></div>
            </div>
	
			<div id="content-wrap"
            
			<?php 
                  if ( ( ($hide_header == 1) && ($hide_header_by_page == '') ) || (($hide_header == 1) && ($hide_header_by_page == 'Yes')) || (($hide_header == 0) && ($hide_header_by_page == 'Yes')) ) { 
                    echo 'class="wrap_no_header"'; 
                  } 
            ?>>
