<?php

function cornell_base_custom_header_setup() {
	$args = array(
		// Text color and image (empty to use none).
		'default-text-color'     => '220e10',
		'default-image'          => '%s/images/headers/header_image.jpg',

		// Set height and width, with a maximum value for the width.
		'height'                 => 540,
		'width'                  => 1280,

		// Callbacks for styling the header and the admin preview.
		'wp-head-callback'       => 'cornell_base_header_style',
		'admin-head-callback'    => 'cornell_base_admin_header_style',
		'admin-preview-callback' => 'cornell_base_admin_header_image',
	);

	add_theme_support( 'custom-header', $args );
}
add_action( 'after_setup_theme', 'cornell_base_custom_header_setup', 11 );

/**
 * Style the header text displayed on the blog.
 *
 * get_header_textcolor() options: Hide text (returns 'blank'), or any hex value.
 *
 */
function cornell_base_header_style() {
	$header_image = get_header_image();
	$text_color   = get_header_textcolor();

	// If no custom options for text are set, let's bail.
	if ( empty( $header_image ) && $text_color == get_theme_support( 'custom-header', 'default-text-color' ) )
		return;

	// If we get this far, we have custom styles.
	?>
	<style type="text/css" id="cornell_base-header-css">
	<?php
		if ( ! empty( $header_image ) ) :
	?>
		#image-band {
			float: left;
			width: 100%;
			clear: both;
			position: relative;
			background: url(<?php header_image(); ?>) no-repeat 0 0;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover !important;
		}

	<?php endif; ?>

	</style>
	<?php
}

/**
 * Style the header image displayed on the Appearance > Header admin panel.
 *
 */
function cornell_base_admin_header_style() {
	$header_image = get_header_image();
?>
	<style type="text/css" id="cornell_base-admin-header-css">
	.appearance_page_custom-header #headimg {
		border: none;
		-webkit-box-sizing: border-box;
		-moz-box-sizing:    border-box;
		box-sizing:         border-box;
		<?php
		if ( ! empty( $header_image ) ) {
			echo 'background: url(' . esc_url( $header_image ) . ') no-repeat scroll top; background-size: 1280px auto;';
		} ?>
		padding: 0 20px;
	}
	.appearance_page_custom-header .displaying-header-text,
	.appearance_page_custom-header #wpcontent #wpbody #wpbody-content .wrap form > h3,
	.appearance_page_custom-header #wpcontent #wpbody #wpbody-content .wrap form .form-table:nth-child(3),
	.appearance_page_custom-header #wpcontent #wpbody #wpbody-content .wrap form .form-table:first-child tbody tr:nth-child(3),
	.accordion-section.open .customize-control-image.open .library li.library-selected {
		display: none!important;
	}

	#headimg .home-link {
		-webkit-box-sizing: border-box;
		-moz-box-sizing:    border-box;
		box-sizing:         border-box;
		margin: 0 auto;
		max-width: 1040px;
		<?php
		if ( ! empty( $header_image ) || display_header_text() ) {
			echo 'min-height: 220px;';
		} ?>
		width: 100%;
	}
	<?php if ( ! display_header_text() ) : ?>
	#headimg h1,
	#headimg h2 {
		position: absolute !important;
		clip: rect(1px 1px 1px 1px); /* IE7 */
		clip: rect(1px, 1px, 1px, 1px);
	}
	<?php endif; ?>
	#headimg h1 {
		font: bold 60px/1 Bitter, Georgia, serif;
		margin: 0;
		padding: 58px 0 10px;
	}
	#headimg h1 a {
		text-decoration: none;
	}
	#headimg h1 a:hover {
		text-decoration: underline;
	}
	#headimg h2 {
		font: 200 italic 24px "Source Sans Pro", Helvetica, sans-serif;
		margin: 0;
		text-shadow: none;
	}
	.default-header img {
		max-width: 230px;
		width: auto;
	}
	</style>

<?php }


/**
 * Output markup to be displayed on the Appearance > Header admin panel.
 *
 * This callback overrides the default markup displayed there.
 *
 */
function cornell_base_admin_header_image() {
	?>
	<div id="headimg" style="background: url(<?php header_image(); ?>) no-repeat scroll top; background-size: 1280px auto;">
		<?php $style = ' style="color:#' . get_header_textcolor() . ';"'; ?>
		<div class="home-link">
			<h1 class="displaying-header-text"><a id="name"<?php echo $style; ?> onclick="return false;" href="#"><?php bloginfo( 'name' ); ?></a></h1>
			<h2 id="desc" class="displaying-header-text"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></h2>
		</div>
	</div>
<?php } ?>