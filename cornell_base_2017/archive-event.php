<?php get_header(); ?>

<?php get_header( 'calendar-plus' ); ?>
		
<?php do_action( 'calendarp_before_content' ); ?>

<div id="content">

	<div id="main">
    
    	<div id="main-top"></div>
        
		<?php if ( is_active_sidebar( 'sidebar-3' ) ) { ?>
            <div id="secondary-nav">
                <div class="main-body">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-body">

		<div class="calendarp-breadcrumbs row">
			<div class="large-12">
				<?php echo '<a href="' . home_url() . '">Home</a> ' . calendarp_breadcrumbs(); ?>
			</div>
		</div>
			
		<?php if ( have_posts() ) : ?>
        
			<header class="archive-header">
				<?php do_action( 'calendarp_before_page_title' ); ?>
                    <h1 class="<?php echo apply_filters( 'calendarp_page_archive_event_title_class', 'page-title' ); ?>"><?php echo esc_html( calendarp_get_template_title() ); ?></h1>
                <?php do_action( 'calendarp_after_page_title' ); ?>
			</header><!-- .archive-header -->
            
			<?php /* The loop */ ?>
			<?php do_action( 'calendarp_before_events_loop' ); ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php calendarp_get_template_part( 'content', 'event' ); ?>
			<?php endwhile; ?>
                        
			<?php do_action( 'calendarp_after_events_loop' ); ?>
            
			<?php if(function_exists('tw_pagination')) tw_pagination(); ?>
            
		<?php else : ?>
			<p class="calendarp-warning"><?php _e( 'No events were found.', CALENDARP_LANG_DOMAIN ); ?></p>
		<?php endif; ?>

		</div><!-- end #main-body -->
        
		<?php if ( is_active_sidebar( 'sidebar-4' ) ) { ?>
            <div id="secondary">
                <div class="main-body">
                    <?php get_sidebar('secondary'); ?>
                </div>
            </div>
        <?php } ?>
            
        <div id="main-bottom"></div>
        
	</div><!-- #main -->
    
</div><!-- #content -->

</div><!-- #content-wrap -->
</div><!-- end #wrap -->


<?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
    <div class="colorband tint-one">
        <div class="colorband-content">
            <?php if ( get_theme_mod('section_one') != '' ) : ?><h2 class="section-title"><span><?php echo get_theme_mod('section_one', 'Section One'); ?></span></h2><?php endif; ?>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-5' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'sidebar-6' ) ) : ?>
    <div class="colorband tint-two">
        <div class="colorband-content">
            <?php if ( get_theme_mod('section_two') != '' ) : ?><h2 class="section-title"><span><?php echo get_theme_mod('section_two', 'Section Two'); ?></span></h2><?php endif; ?>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-6' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'sidebar-7' ) ) : ?>
    <div class="colorband tint-three">
        <div class="colorband-content">
            <?php if ( get_theme_mod('section_three') != '' ) : ?><h2 class="section-title"><span><?php echo get_theme_mod('section_three', 'Section Three'); ?></span></h2><?php endif; ?>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-7' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>



<?php do_action( 'calendarp_after_content' ); ?>

<?php do_action( 'calendarp_sidebar' ); ?>

<?php get_footer( 'calendar-plus' ); ?>


<?php get_footer(); ?>