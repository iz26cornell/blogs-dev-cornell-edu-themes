var global_toggle = 0;

function expander() {
	$(".iws-expander .expand-all").show();
	$(".iws-expander dd").hide();
	$(".iws-expander dd").data("ex",false);
	$(".iws-expander dt").click(function() {
		$(this).next("dd").slideToggle(300);
		if ($(this).next("dd").data("ex") == false) {
			$(this).next("dd").data("ex",true);
			$(this).addClass("expanded");
		}
		else {
			$(this).next("dd").data("ex",false);
			$(this).removeClass("expanded");
		}
	});
	
	$(".iws-expander .expand-all a").click(function() {
		if (window.global_toggle == 0) {
			window.global_toggle = 1;
			$(".iws-expander dt").addClass("expanded");
			$(".iws-expander dd").slideDown(300);
			$(".iws-expander dd").data("ex",true);
			$(".iws-expander .expand-all a").text("close all");
		}
		else {
			window.global_toggle = 0;
			$(".iws-expander dt").removeClass("expanded");
			$(".iws-expander dd").slideUp(300);
			$(".iws-expander dd").data("ex",false);
			$(".iws-expander .expand-all a").text("expand all");
		}
	});
}