<?php
/**
 * The bottom area of the sidebar under the primary area
 *
 */

if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
	<div id="tertiary2" class="sidebar-container" role="complementary">
		<div class="sidebar-inner">
			<div class="widget-area">
				<?php dynamic_sidebar( 'sidebar-4' ); ?>
			</div><!-- .widget-area -->
		</div><!-- .sidebar-inner -->
	</div><!-- #tertiary -->
<?php endif; ?>