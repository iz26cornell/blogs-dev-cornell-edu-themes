<?php 
if ( post_password_required() )
	return;
?>

<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'cornell_base' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h2>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 74,
				) );
			?>
		</ol><!-- .comment-list -->

		<?php if(function_exists('tw_pagination')) tw_pagination_comments(); ?>

		<?php if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="no-comments"><?php _e( 'Comments are closed.' , 'cornell_base' ); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php 
	
	
$comments_args = array(
        // change the title of send button 
        'label_submit'=>'Submit',
        // redefine your own textarea (the comment body)
        'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label>
		<textarea id="comment" name="comment" rows="8" aria-required="true"></textarea></p>',
);

comment_form($comments_args);	
	
	 ?>

</div><!-- #comments -->