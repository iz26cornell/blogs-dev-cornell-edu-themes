<?php
/**
 * Template Name: News Full Width Template
 * The template for displaying items tagged as "News & Insights"
 *
 * @package Pena
 */

get_header('custom'); ?>
	<div class="featured-image">
	<?php if ( has_post_thumbnail() ): ?>
		<?php $image_id = get_post_thumbnail_id(); ?>
		<?php $image_url = wp_get_attachment_image_src( $image_id,'full' );   ?>
		<div class="header section pages" style="background-image:url(<?php echo esc_url( $image_url[0] ); ?>);">
		<div class="entry-content">
			<h1><?php the_title(); ?></h1>
		</div>
		<span class="overlay"></span>
		</div>
	<?php else: ?>
		<div class="header section pages without-featured-image">
		<div class="entry-content">
		<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<hr class="short">
		</header><!-- .entry-header -->
		</div>
		</div>
	<?php endif; ?>
	</div>

	<div class="hfeed site page-content news_page">
		<div class="content site-content">
			<div id="primary" class="content-area gridpage">
				<main id="main" role="main">
					<div class="main-content">
						<?php
						// Start the loop.
						while ( have_posts() ) : the_post();

							// Include the page content template.
							get_template_part( 'template-parts/content', 'page-default' );

						// End the loop.
						endwhile;
						?>

						<?php
							$news_cat_ID = get_cat_ID('News & Insights');
							$news_query = new WP_Query(array('cat' => $news_cat_ID, 'orderby' => 'date'));

							if ($news_query -> have_posts() ) : while($news_query -> have_posts() ) : $news_query -> the_post();
							?>
							<a href="<?php the_permalink(); ?>">
								<article class="news_post_excerpt" aria-labelledby="title-<?php echo get_the_ID(); ?>">
									<div class="news_post_excerpt_thumbnail"><?php the_post_thumbnail(); ?></div>
									<div class="news_post_excerpt_content">
										<h2 id="title-<?php echo get_the_ID(); ?>"><?php the_title(); ?></h2>
										<p><?php the_date(); ?></p>
										<?php the_excerpt() ?>
									</div>
								</article>
							</a>
							<?php
							endwhile;
							endif;
							?>


							<?php
							wp_reset_postdata();

						 ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>

					</div><!-- .main-content -->

				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- .content -->
	</div><!-- .site -->

<?php get_footer(); ?>
