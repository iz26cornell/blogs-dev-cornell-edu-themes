<?php
/**
 * Template Name: People Template - Full width
 * The template for displaying the About/People page.
 *
 * @package Pena
 */

get_header('custom'); ?>
	<div class="featured-image">
	<?php if ( has_post_thumbnail() ): ?>
		<?php $image_id = get_post_thumbnail_id(); ?>
		<?php $image_url = wp_get_attachment_image_src( $image_id,'full' );   ?>
		<div class="header section pages" style="background-image:url(<?php echo esc_url( $image_url[0] ); ?>);">
		<div class="entry-content">
			<h1><?php the_title(); ?></h1>
		</div>
		<span class="overlay"></span>
		</div>
	<?php else: ?>
		<div class="header section pages without-featured-image">
		<div class="entry-content">
		<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<hr class="short">
		</header><!-- .entry-header -->
		</div>
		</div>
	<?php endif; ?>
	</div>

	<div class="hfeed site page-content">
		<div class="content site-content">
			<div id="primary" class="content-area gridpage">
				<main id="main" class="site-main" role="main">
					<div class="main-content">
						<?php
						// Start the loop.
						while ( have_posts() ) : the_post();

							// Include the page content template.
							get_template_part( 'template-parts/content', 'page-default' );

						// End the loop.
						endwhile;
						?>

                        <?php //CWD: Adds members from CICSS Team Members
                    $member_query = new WP_Query(array('post_type'=>'member', 'post_status'=>'publish', 'meta_key'=>'priority', 'posts_per_page'=>-1, 'order'=>'ASC', 'orderby'=>'priority'));
                    ?>

                    <?php if($member_query->have_posts() ) : ?>
                        <section id="core_sec" class="member_sec full-width" aria-labelledby="h_core">
                            <h2 id="h_core">Core Team</h2>
                            <div class="member_group" id="member_core_group">
                            <?php while($member_query->have_posts() ) : $member_query->the_post(); ?>
                            <?php
                                $memcat = get_field('category');
                                $role = get_field('department');
                                $email = get_field('email');
                                $phone = get_field('phone');
                                $addr = get_field('campus_address');
                            ?>

                            <?php if ($memcat=="core") : ?>
                                    <div class="member member_core">
                                        <div class="member_pic">
                                            <a href="<?php the_permalink(); ?>">
                                        <?php
                                            if (has_post_thumbnail() ){
                                                the_post_thumbnail();
                                            } else { ?>
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/person-random.png" alt="<?php the_title(); ?>"/>
                                            <?php } ?>
                                            </a>
                                        </div>
                                        <div class="member_content">
                                            <h3><?php the_title(); ?></h3>
                                            <p class="member_role"><?php echo $role ?></p>
                                            <?php if($email) : ?>
                                                <p class="member_email"><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></p>
                                            <?php endif; ?>
                                            <?php if($phone) : ?>
                                                <p class="member_phone"><?php echo $phone ?></p>
                                            <?php endif; ?>
                                            <?php if($addr) : ?>
                                                <p class="member_addr"><?php echo $addr ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                            <?php endif; ?>
                            <?php endwhile; ?>
                            </div>
                        </section>
                        <section id="advisory_sec" class="member_sec full-width" aria-labelledby="h_advisory">
                            <h2 id="h_advisory">Our Advisory Board</h2>
                            <div class="member_group" id="member_advisory_group">

                            <?php while($member_query->have_posts() ) : $member_query->the_post(); ?>
                            <?php
                                $memcat = get_field('category');
                                $role = get_field('department');
                                $email = get_field('email');
                                $phone = get_field('phone');
                                $addr = get_field('campus_address');
                            ?>

                            <?php if ($memcat=="board") : ?>
                                    <div class="member member_board">
                                        <div class="member_pic">
                                            <a href="<?php the_permalink(); ?>">
                                        <?php
                                            if (has_post_thumbnail() ){
                                                the_post_thumbnail();
                                            } else { ?>
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/person-random.png" alt="<?php the_title(); ?>"/>
                                            <?php } ?>
                                            </a>
                                        </div>
                                        <div class="member_content">
                                            <h3><?php the_title(); ?></h3>
                                            <p class="member_role"><?php echo $role ?></p>
                                            <?php if($email) : ?>
                                                <p class="member_email"><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></p>
                                            <?php endif; ?>
                                            <?php if($phone) : ?>
                                                <p class="member_phone"><?php echo $phone ?></p>
                                            <?php endif; ?>
                                            <?php if($addr) : ?>
                                                <p class="member_addr"><?php echo $addr ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                            <?php endif; ?>
                            <?php endwhile; ?>
                            </div>
                        </section>
                        <section id="student_sec" class="member_sec full-width" aria-labelledby="h_student">
                            <h2 id="h_student">Fellows &amp; Research Assistants</h2>
                            <div class="member_group" id="member_student_group">
                            <?php while($member_query->have_posts() ) : $member_query->the_post(); ?>
                            <?php
                                $memcat = get_field('category');
                                $role = get_field('department');
                                $email = get_field('email');
                                $phone = get_field('phone');
                                $addr = get_field('campus_address');
                            ?>

                            <?php if ($memcat=="student") : ?>
                                    <div class="member member_student">
                                        <div class="member_pic">
                                            <a href="<?php the_permalink(); ?>">
                                        <?php
                                            if (has_post_thumbnail() ){
                                                the_post_thumbnail();
                                            } else { ?>
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/person-random.png" alt="<?php the_title(); ?>"/>
                                            <?php } ?>
                                            </a>
                                        </div>
                                        <div class="member_content">
                                            <h3><?php the_title(); ?></h3>
                                            <p class="member_role"><?php echo $role ?></p>
                                            <?php if($email) : ?>
                                                <p class="member_email"><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></p>
                                            <?php endif; ?>
                                            <?php if($phone) : ?>
                                                <p class="member_phone"><?php echo $phone ?></p>
                                            <?php endif; ?>
                                            <?php if($addr) : ?>
                                                <p class="member_addr"><?php echo $addr ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                            <?php endif; ?>
                            <?php endwhile; ?>
                            </div>
                        </section>
                            <?php wp_reset_postdata(); ?>
                    <?php else : ?>
                            <section id="member_sec" aria-label="CICSS Team Members"><p><?php _e('No members found.'); ?></p></section>
                    <?php endif; ?>

					</div><!-- .main-content -->

				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- .content -->
	</div><!-- .site -->

<?php get_footer(); ?>
