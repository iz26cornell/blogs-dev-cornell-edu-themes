<?php
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(), array( 'parent-style' ) );
    wp_enqueue_style( 'event-style', get_stylesheet_directory_uri() . '/events-style.css');
    wp_enqueue_style( 'cornell-main', get_stylesheet_directory_uri() . '/css/cornell-main.css');
    wp_enqueue_style( 'cornell-base', get_stylesheet_directory_uri() . '/css/cornell.css');
    //Loads specific CSS if the page has a featured image.
    if (has_post_thumbnail()) wp_enqueue_style('navigation-style', get_stylesheet_directory_uri() . '/navigation-style.css');

}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function my_theme_enqueue_scripts() {
    wp_enqueue_script('iws2', get_stylesheet_directory_uri() . '/js/iws-search.js', array('jquery'));
    wp_enqueue_script('iws-events', get_stylesheet_directory_uri() . '/js/iws_events.js', array('jquery'));
    wp_enqueue_script('iws-events-archive', get_stylesheet_directory_uri() . '/js/iws_events_archive.js', array('jquery') );
    wp_enqueue_script('cicss_scripts', get_stylesheet_directory_uri() . '/js/cicss.js', array('jquery'));

}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_scripts' );

function create_cicss_members() {
    $labels = array (
        "name" => __("Members", ""),
        "singular_name" => __("Member", ""),
        "menu_name" => __("CICSS Members", ""),
        "all_items" => __("All Members", ""),
        "add_new" => __("Add New", ""),
        "add_new_item" => __("Add New Member", ""),
        "edit_item" => __("Edit Member", ""),
        "new_item" => __("New Member", ""),
        "view_item" => __("View Member", ""),
        "view_items" => __("View Members", ""),
        "search_items" => __("Search Members", ""),
        "not_found" => __("No Members Found", ""),
    );

    $args = array(
        "label" => __("Members", ""),
        "labels" => $labels,
        "description" => "Members of the CICSS team to display on the site.",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array("slug" => "member", "with_front" => true),
        "query_var" => true,
        "menu_position" => 25,
        "supports" => array("title", "thumbnail", "revisions", "editor", "custom-fields", "excerpt", "revisions"),
    );

    register_post_type("member", $args);
}

add_action("init", 'create_cicss_members');


// Enable Shortcodes in Text Widgets
add_filter('widget_text', 'do_shortcode');

// Shortcode: Render Custom Field
function customfields_shortcode($atts, $text) {
                global $post;
                return get_post_meta($post->ID, $text, true);
}
@add_shortcode('cf','customfields_shortcode');

// Create Widgets for About Page
function cicss_widgets_init() {

    register_sidebar ( array(
        'name'          => 'Outreach-Videos',
        'id'            => 'outreach_videos',
        'description'   => 'Video content which will appear on the Outreach page. Only use Video widgets.',
        'before_widget' => '<div class="cicss_videos">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="video_widget_h">',
		'after_title'   => '</h3>',
    ));
    register_sidebar ( array(
        'name'          => 'Outreach-Podcasts',
        'id'            => 'outreach_podcasts',
        'description'   => 'Add the description for the Podcasts block if one is desired.',
        'before_widget' => '<div class="cicss_podcasts">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="seminar_widget_h">',
		'after_title'   => '</h3>',
    ));
    register_sidebar ( array(
        'name'          => 'Outreach-Seminars',
        'id'            => 'outreach_seminars',
        'description'   => 'Add the description for the Seminars block if one is desired.',
        'before_widget' => '<div class="cicss_seminars">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="seminar_widget_h">',
		'after_title'   => '</h3>',
    ));
    register_sidebar ( array(
        'name'          => 'Outreach-Newsletters',
        'id'            => 'outreach_newsletters',
        'description'   => 'Add the description for the Newsletter block if one is desired.',
        'before_widget' => '<div class="cicss_newsletters">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="seminar_widget_h">',
		'after_title'   => '</h3>',
    ));
    register_sidebar ( array(
        'name'          => 'Sponsors & Partners',
        'id'            => 'cicss_partners',
        'description'   => 'Sponsors and Partners which will appear at the bottom of the Outreach page.',
        'before_widget' => '<div class="cicss_partners">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="partners_h">',
		'after_title'   => '</h2>',
    ));
}

add_action('widgets_init', 'cicss_widgets_init');

remove_action('widgets_init', 'pena_widgets_init');

function pena_widgets_init2() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'pena' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Front Page Call To Action Block', 'pena' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Front Page Recent Post Block', 'pena' ),
		'id'            => 'sidebar-3',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widgets 1', 'pena' ),
		'id'            => 'footer-1',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widgets 2', 'pena' ),
		'id'            => 'footer-2',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widgets 3', 'pena' ),
		'id'            => 'footer-3',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Menu Widget', 'pena' ),
		'id'            => 'footer-4',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s" aria-label="Fourth footer region">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'pena_widgets_init2', 15 );

add_filter( 'the_excerpt',
  function ($excerpt) {
    return substr($excerpt,0,strpos($excerpt,'.')+1);
  }
);

add_filter('get_the_archive_title', function ($title) {
    return preg_replace('/^\w+: /', '', $title);
});
