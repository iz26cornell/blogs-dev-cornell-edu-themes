<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Pena
 */

?>
	<?php if ( has_nav_menu( 'social' ) ) : ?>
	<div class="social-block">
		<nav id="social-navigation" class="social-navigation" role="navigation">
			<?php
				// Social links navigation menu.
				wp_nav_menu( array(
					'theme_location' => 'social',
					'depth'		  => 1,
					'link_before'	=> '<span class="screen-reader-text">',
					'link_after'	 => '</span>',
				) );
			?>
		</nav><!-- .social-navigation -->
	</div>
	<?php endif; ?>
	<?php if ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) ) : ?>

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="hfeed site">

			<div class="content site-content">

				<div class="footer-widgets clear">

					<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>

						<div class="widget-area">

							<?php dynamic_sidebar( 'footer-1' ); ?>

						</div><!-- .widget-area -->

					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>

						<div class="widget-area">

							<?php dynamic_sidebar( 'footer-2' ); ?>

						</div><!-- .widget-area -->

					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>

						<div class="widget-area">

							<?php dynamic_sidebar( 'footer-3' ); ?>

						</div><!-- .widget-area -->

					<?php endif; ?>

				</div><!-- .footer-widgets -->

				<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>

					<div class="widget-area footer-menu">

						<?php dynamic_sidebar( 'footer-4' ); ?>

				</div><!-- .widget-area -->

				<?php endif; ?>

			</div><!-- .site -->

		</div><!-- .content -->

			</footer><!-- #colophon -->
	<?php endif; ?>

			<div class="site-info">
				<div class="hfeed site">
					<div class="content site-content">
						<?php if( get_theme_mod( 'hide_copyright' ) == '') { ?>
							<?php
								/**
								 * Fires before the Maisha footer text for footer customization.
								 *
								 * @since Maisha 1.0
								 */
								do_action( 'pena_credits' );
							?>
							<?php esc_attr_e('&copy;', 'pena'); ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"> <?php echo esc_textarea ( get_theme_mod( 'pena_copyright', 'Pena Theme by Anariel Design.' ) ); ?> </a>
						<?php } // end if ?>
					</div><!-- .content -->
				</div><!-- .site -->
			</div><!-- .site-info -->
		</div><!-- .inner-page -->
	</div><!-- .inner -->
<?php wp_footer(); ?>

</body>
</html>