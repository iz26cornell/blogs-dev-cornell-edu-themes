<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Pena
 */

get_header(); ?>
<div class="hfeed site blog">
	<div class="content site-content">
	<div class="primary content-area sidebar-right-layout grid clear">
		<main class="site-main" aria-label="Main Content">
			<div class="posts columns clearfix">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							 get_template_part( 'template-parts/content-grid-give', get_post_format() );
						?>

					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; ?>

			</div><!-- .posts -->
		</main><!-- #main -->
	</div><!-- #primary -->
	<div class="primary content-area sidebar-right-layout">
		<div class="site-main" role="main">
			<?php the_posts_navigation(); ?>
		</div><!-- #main -->
	</div><!-- #primary -->

	</div><!-- .content -->
</div><!-- .site -->

<?php get_footer(); ?>