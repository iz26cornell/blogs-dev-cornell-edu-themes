<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Pena
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary" aria-label="Sidebar 1 Content">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->