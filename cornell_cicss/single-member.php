<?php
/**
 * The template for displaying all members.
 *
 * @package Pena
 */

get_header('custom'); ?>

<div class="hfeed site single member-single">
	<div class="content site-content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" aria-label="Main Content">

			<?php while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                    <header class="entry-header" aria-label="Person Information">

                        <?php if(!get_theme_mod('pena_main_featured_image')) : ?>
                            <?php if ( has_post_thumbnail() ) { ?>
                            <div class="member_thumbnail post-thumbnail">
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail(  'pena-post-list-thumbnail' ); ?>
                                </a>
                            </div>
                            <?php } ?>
                        <?php endif; ?>
												<div class="member_bio">
                       <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                         <?php
                                $curr = get_the_ID();
                                $role = get_field('department', $curr);
                                $email = get_field('email', $curr);
                                $phone = get_field('phone', $curr);
                                $addr = get_field('campus_address', $curr);
                        ?>
                        <p class="member_role"><?php echo $role ?></p>
                        <?php if($email) : ?>
                            <p class="member_email"><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></p>
                        <?php endif; ?>
                        <?php if($phone) : ?>
                            <p class="member_phone"><?php echo $phone ?></p>
                        <?php endif; ?>
                        <?php if($addr) : ?>
                            <p class="member_addr"><?php echo $addr ?></p>
                        <?php endif; ?>
											</div>

                    </header><!-- .entry-header -->

                    <div class="entry-content">


                        <?php if(get_theme_mod('pena_post_type') == 'excerpt-lenght') : ?>
                        <?php
                            /* translators: %s: Name of current post */
                            the_excerpt();
                        ?>
                        <?php else: ?>
                        <?php
                            /* translators: %s: Name of current post */
                            the_content( sprintf(
                                wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'pena' ), array( 'span' => array( 'class' => array() ) ) ),
                                the_title( '<span class="screen-reader-text">"', '"</span>', false )
                            ) );
                        ?>
                        <?php endif; ?>

                        <?php
                            wp_link_pages( array(
                                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pena' ),
                                'after'  => '</div>',
                            ) );
                        ?>
                    </div><!-- .entry-content -->
                </article><!-- #post-## -->

			<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- #primary -->

	</div><!-- .site-content -->
</div><!-- .site -->

<?php get_footer(); ?>
