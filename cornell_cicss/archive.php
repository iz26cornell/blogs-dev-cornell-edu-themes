<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Pena
 */

get_header( 'custom' ); ?>

<div class="hfeed site blog">
	<div class="content site-content">
	<?php if( 'list' == get_theme_mod( 'pena_blog_layout' ) ) : ?>
	<div id="primary" class="content-area list-layout">
		<main class="site-main" aria-label="Main Content">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );

					if ( is_author() && '' !== get_the_author_meta( 'description' ) ) {
						echo '<div class="taxonomy-description">' . get_the_author_meta( 'description' ) . '</div><!-- .taxonomy-description -->';
					} else {
						the_archive_description( '<div class="taxonomy-description">', '</div><!-- .taxonomy-description -->' );
					}
				?>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					 get_template_part( 'template-parts/content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php elseif( 'sidebar-left' == get_theme_mod( 'pena_blog_layout' ) ) : ?>
	<header class="page-header" aria-label="Page Header">
		<?php
			the_archive_title( '<h1 class="page-title">', '</h1>' );

			if ( is_author() && '' !== get_the_author_meta( 'description' ) ) {
				echo '<div class="taxonomy-description">' . get_the_author_meta( 'description' ) . '</div><!-- .taxonomy-description -->';
			} else {
				the_archive_description( '<div class="taxonomy-description">', '</div><!-- .taxonomy-description -->' );
			}
		?>
	</header><!-- .page-header -->
	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	<div class="one-third main">
		<?php get_sidebar(); ?>
	</div><!-- .one_third -->
	<div class="two-third main lastcolumn">
		<div id="primary" class="content-area sidebar-right-layout">
			<main class="site-main" role="main">

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						 get_template_part( 'template-parts/content-default', get_post_format() );
					?>

				<?php endwhile; ?>

				<?php the_posts_navigation(); ?>

			<?php else : ?>

				<?php get_template_part( 'template-parts/content', 'none' ); ?>

			<?php endif; ?>

			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- .two_third -->
	<?php else: ?>
		<div id="primary" class="content-area sidebar-right-layout">
			<main class="site-main" role="main">

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						 get_template_part( 'template-parts/content-default', get_post_format() );
					?>

				<?php endwhile; ?>

				<?php the_posts_navigation(); ?>

			<?php else : ?>

				<?php get_template_part( 'template-parts/content', 'none' ); ?>

			<?php endif; ?>

			</main><!-- #main -->
		</div><!-- #primary -->
	<?php endif; ?>

	<?php elseif( 'grid-two' == get_theme_mod( 'pena_blog_layout' ) ) : ?>
		<header class="page-header" aria-label="Page Header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );

					if ( is_author() && '' !== get_the_author_meta( 'description' ) ) {
						echo '<div class="taxonomy-description">' . get_the_author_meta( 'description' ) . '</div><!-- .taxonomy-description -->';
					} else {
						the_archive_description( '<div class="taxonomy-description">', '</div><!-- .taxonomy-description -->' );
					}
				?>
		</header><!-- .page-header -->
	<div id="primary" class="content-area sidebar-right-layout grid clear">
		<main class="site-main" aria-label="Main Content">
			<div class="posts columns clearfix">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							 get_template_part( 'template-parts/content-grid-two', get_post_format() );
						?>

					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; ?>

			</div><!-- .posts -->
		</main><!-- #main -->
	</div><!-- #primary -->
	<div id="primary" class="content-area sidebar-right-layout">
		<main class="site-main" aria-label="Main Content">
			<?php the_posts_navigation(); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php elseif( 'grid-two-sidebar' == get_theme_mod( 'pena_blog_layout' ) ) : ?>
		<header class="page-header" aria-label="Sidebar Header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );

					if ( is_author() && '' !== get_the_author_meta( 'description' ) ) {
						echo '<div class="taxonomy-description">' . get_the_author_meta( 'description' ) . '</div><!-- .taxonomy-description -->';
					} else {
						the_archive_description( '<div class="taxonomy-description">', '</div><!-- .taxonomy-description -->' );
					}
				?>
		</header><!-- .page-header -->
	<div class="two-third main">
		<div id="primary" class="content-area sidebar-right-layout grid clear">
			<main class="site-main" aria-label="Sidebar Content">
				<div class="posts columns clearfix">

					<?php if ( have_posts() ) : ?>

						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>

							<?php

								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								 get_template_part( 'template-parts/content-grid-two', get_post_format() );
							?>

						<?php endwhile; ?>

					<?php else : ?>

						<?php get_template_part( 'template-parts/content', 'none' ); ?>

					<?php endif; ?>

				</div><!-- .posts -->
			</main><!-- #main -->
		</div><!-- #primary -->
	<div id="primary" class="content-area sidebar-right-layout">
		<main class="site-main" aria-label="Sidebar Navigation">
			<?php the_posts_navigation(); ?>
		</main><!-- #main -->
	</div><!-- #primary -->
	</div><!-- .two_third -->
	<div class="one-third main lastcolumn">
		<?php get_sidebar(); ?>
	</div><!-- .one_third -->

	<?php elseif( 'grid-three' == get_theme_mod( 'pena_blog_layout' ) ) : ?>
		<header class="page-header" aria-label="Sidebar Header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );

					if ( is_author() && '' !== get_the_author_meta( 'description' ) ) {
						echo '<div class="taxonomy-description">' . get_the_author_meta( 'description' ) . '</div><!-- .taxonomy-description -->';
					} else {
						the_archive_description( '<div class="taxonomy-description">', '</div><!-- .taxonomy-description -->' );
					}
				?>
		</header><!-- .page-header -->
	<div id="primary" class="content-area sidebar-right-layout grid clear">
		<main class="site-main" aria-label="Sidebar Content">
			<div class="posts columns clearfix">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							 get_template_part( 'template-parts/content-grid-three', get_post_format() );
						?>

					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; ?>

			</div><!-- .posts -->
		</main><!-- #main -->
	</div><!-- #primary -->
	<div id="primary" class="content-area sidebar-right-layout">
		<main class="site-main" aria-label="Sidebar Navigation">
			<?php the_posts_navigation(); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php else: ?>
	<header class="page-header" aria-label="Sidebar Header">
		<?php
			the_archive_title( '<h1 class="page-title">', '</h1>' );

			if ( is_author() && '' !== get_the_author_meta( 'description' ) ) {
				echo '<div class="taxonomy-description">' . get_the_author_meta( 'description' ) . '</div><!-- .taxonomy-description -->';
			} else {
				the_archive_description( '<div class="taxonomy-description">', '</div><!-- .taxonomy-description -->' );
			}
		?>
	</header><!-- .page-header -->
	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	<div class="two-third main">
		<div id="primary" class="content-area sidebar-right-layout">
			<main class="site-main" aria-label="Sidebar Content">

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						 get_template_part( 'template-parts/content-default', get_post_format() );
					?>

				<?php endwhile; ?>

				<?php the_posts_navigation(); ?>

			<?php else : ?>

				<?php get_template_part( 'template-parts/content', 'none' ); ?>

			<?php endif; ?>

			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- .two_third -->
	<div class="one-third main lastcolumn">
		<?php get_sidebar(); ?>
	</div><!-- .one_third -->
	<?php else: ?>
		<div id="primary" class="content-area sidebar-right-layout">
			<main class="site-main" aria-label="Main Content">

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						 get_template_part( 'template-parts/content-default', get_post_format() );
					?>

				<?php endwhile; ?>

				<?php the_posts_navigation(); ?>

			<?php else : ?>

				<?php get_template_part( 'template-parts/content', 'none' ); ?>

			<?php endif; ?>

			</main><!-- #main -->
		</div><!-- #primary -->
	<?php endif; ?>

	<?php endif; ?>
	</div><!-- .content -->
</div><!-- .site -->

<?php get_footer(); ?>