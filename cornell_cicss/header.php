<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Pena
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'pena' ); ?></a>
    <!-- Cornell Branding - Trimmed -->
        
    <div id="cu-brand-wrapper-z">
        <div id="search-box">
          <div id="search-box-content">
            <div id="cu-search" class="options">
              <form method="get" action="<?php echo home_url( '/' ); ?>">
                <div id="search-form">
                  <label for="search-form-query">SEARCH:</label>
                  <input type="text" id="search-form-query" name="s" value="" size="20" onblur="this.value = this.value || this.defaultValue;" onfocus="this.value == this.defaultValue && (this.value =''); this.select()" />
                  <input type="submit" id="search-form-submit" name="btnG" value="go" />
                  <div id="search-filters" role="radiogroup" aria-label="Choose a search location">
                    <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
                    <label for="search-filters1">This Site</label>
                    <input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
                    <label for="search-filters2">Cornell</label>
                    </div>
                </div>
              </form>
            </div>
          </div><!-- #search-box-content -->
        </div><!-- #search-box -->
        
        <div id="cu-identity" class="<?php echo get_theme_mod('custom_banner', 'theme_gray75'); if ( get_theme_mod('display_tagline') != 1 || get_bloginfo( 'description' ) == '' ) { echo ' no-tagline'; } else { echo ' tagline'; } ?>">
            <div id="cu-identity-content">
                <div id="cu-brand-wrapper">
                    <div id="cu-brand">
                        <div id="cu-seal"><a href="http://www.cornell.edu/" title="Cornell University">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cornell_identity/culogo_hd_white45.svg" alt="Cornell University" /></a>
                        </div>
                        <div id="search-button">
                            <p><a href="#search-box" title="Search This Site">Search This Site</a></p>
                        </div>
                    </div><!-- #cu-brand -->
                </div><!-- #cu-brand-wrapper -->
               
               
            </div><!-- #cu-identity-content -->
        </div><!-- #cu-identity -->
        <?php if (is_dynamic_sidebar('sm-icons')) : ?>
            <aside aria-label="Social Media Links">
                <?php dynamic_sidebar('sm-icons'); ?>
            </aside>
        <?php endif; ?>
        
    </div><!-- #cu-brand-wrapper-z -->
	<header id="masthead" class="site-header front" role="banner">
        <div id="headerContainer">
            <div class="site-branding">
                    <?php pena_the_custom_logo(); ?>
                    <?php if ( is_front_page() && is_home() ) : ?>
                        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                    <?php else : ?>
                        <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
                    <?php endif;
                        $description = get_bloginfo( 'description', 'display' );
                        if ( $description || is_customize_preview() ) : ?>
                            <p class="site-description"><?php echo $description; ?></p>
                    <?php endif; ?>
            </div><!-- .site-branding -->
            <nav id="site-navigation" class="main-navigation" aria-label="Main Navigation">
                <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'pena' ); ?></button>
                <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
            </nav><!-- #site-navigation -->
        </div>
	</header>