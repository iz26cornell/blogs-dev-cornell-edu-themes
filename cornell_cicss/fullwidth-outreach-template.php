<?php
/**
 * Template Name: Outreach Template - Full Width
 * Description: Designed for the Outreach page
 *
 * @package Pena
 */

get_header('custom'); ?>
	<div class="featured-image">
	<?php if ( has_post_thumbnail() ): ?>
		<?php $image_id = get_post_thumbnail_id(); ?>
		<?php $image_url = wp_get_attachment_image_src( $image_id,'full' );   ?>
		<div class="header section pages" style="background-image:url(<?php echo esc_url( $image_url[0] ); ?>);">
		<div class="entry-content">
			<h1><?php the_title(); ?></h1>
		</div>
		<span class="overlay"></span>
		</div>
	<?php else: ?>
		<div class="header section pages without-featured-image">
		<div class="entry-content">
		<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<hr class="short">
		</header><!-- .entry-header -->
		</div>
		</div>
	<?php endif; ?>
	</div>
	<div class="hfeed site page-content">
		<div class="content site-content">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'page-default' );

						// Include all child page content.

						$currID = get_the_ID();

						$childPages = get_pages( array( 'child_of' => $currID, 'sort_column' => 'menu_order', 'sort_order' => 'desc'));

						foreach($childPages as $page) {
								$pageContent = $page->post_content;
								if(! $pageContent) continue;

								$pageContent = apply_filters('the_content', $pageContent);
						?>
								<article aria-labelledby="post-<?php echo $page->ID; ?>">
								<h2 id="post-<?php echo $page->ID; ?>"><?php echo $page->post_title; ?></h2>
								<?php echo $pageContent; ?>
							</article>
							<?php
						}

					endwhile; // End of the loop. ?>

                    <!-- Videos -->

                    <?php if ( is_active_sidebar( 'outreach_videos' ) ) : ?>

                        <section id="outreach_videos" class="full-width outreach"  aria-labelledby="videosHeader">
													<div class="full-width outreach_header">
                            <h2 class="full-width" id="videosHeader"><a href="https://www.youtube.com/channel/UC18K9H429WZ8V-H9gGrJzzQ">Videos</a></h2>
													</div>
                            <div class="video_flex">
                                <?php dynamic_sidebar('outreach_videos'); ?>
                            </div>
                            <a class="outreach_more" id="videos_more" href="https://www.youtube.com/channel/UC18K9H429WZ8V-H9gGrJzzQ">Watch additional videos on YouTube</a>
                        </section>

                    <?php endif; ?>

                    <!-- Podcasts -->

                    <?php $outreachPodcasts = new WP_Query(array(
                                        'category_name' => 'podcast',
                                        'posts_per_page' => 3,
                                                            ));
                        $podcastID = get_cat_ID('podcast'); ?>

                    <?php if ($outreachPodcasts -> have_posts() ) : ?>

                    <section id="outreach_podcasts" class="full-width outreach" aria-labelledby="podcastHeader">
												<div class="full-width outreach_header">
                        <h2 id="podcastHeader"><a href="<?php echo esc_url(get_category_link($podcastID)); ?>">Down to Earth:<br>Cornell Conversations About...</a></h2>
											</div>
												<div class="podcast_flex">
                        <?php if (is_active_sidebar('outreach_podcasts')) : dynamic_sidebar('outreach_podcasts'); endif; ?>
                        <?php while ( $outreachPodcasts -> have_posts() ) : $outreachPodcasts -> the_post(); ?>
                            <div class="podcast">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail() ?>
                                <?php if(get_field('podcast_episode')) : ?>
                                    <p>Episode <?php echo the_field('podcast_episode'); endif; ?></p>
                                <h3><?php the_title(); ?></h3>
                            </a>
														<div class="podcast_external_links">
														<?php if(get_field('podcast_link')) { ?>
															<a class="podcast_option_listen" href="<?php if(get_field('podcast_link')) : echo the_field('podcast_link'); else : echo '#'; endif; ?>" aria-label="Listen to Episode <?php get_field('podcast_episode'); the_title(); ?>">Listen</a>
														<?php } ?>
															<a class="podcast_option_read" href="<?php the_permalink(); ?>" aria-label="Read about Episode <?php get_field('podcast_episode'); the_title(); ?>">About</a>
                            </div>
													</div>
                        <?php endwhile; ?>
                        </div>
                        <a class="outreach_more" id="podcast_more" href="<?php echo esc_url(get_category_link($podcastID)); ?>">View Older Podcasts</a>
                    </section>

                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>

                    <!-- Newsletters -->

                    <?php $outreachNewsletter = new WP_Query(array(
                                        'category_name' => 'newsletter',
                                        'posts_per_page' => 4,
                                                            ));
                        $newsletterID = get_cat_ID('newsletter'); ?>

                    <?php if ($outreachNewsletter -> have_posts() ) : ?>
                    <section id="outreach_newsletter" class="full-width outreach"  aria-labelledby="newsletterHeader">
											<div class="full-width outreach_header">
                        <h2 class="full-width" id="newsletterHeader"><a href="<?php echo get_page_link(1779) ?>">Newsletters</a></h2>
											</div>
                        <?php if (is_active_sidebar('outreach_newsletters')) : dynamic_sidebar('outreach_newsletters'); endif; ?>
                        <div class="newsletter_flex">
                        <?php while($outreachNewsletter -> have_posts() ) : $outreachNewsletter -> the_post(); ?>
                            <div class="newsletter">
                            <a href="<?php if(get_field('newsletter_link')) : echo the_field('newsletter_link'); else : echo '#'; endif; ?>">
                                <?php the_post_thumbnail() ?>
                                <h3><?php the_title(); ?></h3>
                                <?php the_excerpt(); ?>
                            </a>
                            </div>
                        <?php endwhile; ?>
                        </div>
                        <a class="outreach_more" id="newsletters_more" href="<?php echo get_page_link(1779) ?>">View Older Newsletters</a>
                    </section>
                    <?php endif; ?>

                    <!-- Seminars -->
                    <?php $outreachSeminars = new WP_Query(array(
                                        'category_name' => 'seminar',
                                        'posts_per_page' => 3,
                                        ));
                        $seminarID = get_cat_ID('seminar'); ?>
                    <?php if ($outreachSeminars -> have_posts() ) : ?>
                        <section id="outreach_seminars" class="full-width outreach"  aria-labelledby="seminarsHeader">
													<div class="full-width outreach_header">
                            <h2 class="full-width" id="seminarsHeader"><a href="<?php echo esc_url(get_category_link($seminarID)); ?>">Seminars</a></h2>
													</div>
                            <?php if (is_active_sidebar('outreach_seminars')) : dynamic_sidebar('outreach_seminars'); endif; ?>
                            <div class="seminars_flex">
                                <?php while($outreachSeminars -> have_posts() ) : $outreachSeminars -> the_post(); ?>
                                <div class="seminar">
                                    <?php the_post_thumbnail(); ?>
                                    <?php if(get_field('seminar_date')) : ?>
                                        <p>Date: <?php the_field('seminar_date'); ?></p>
                                    <?php endif; ?>
                                    <?php the_title(); ?>
                                    <?php the_excerpt(); ?>

                                </div>
                                <?php endwhile; ?>
                            </div>
                            <a class="outreach_more" id="seminars_more" href="<?php echo esc_url(get_category_link($seminarID)); ?>">View Older Seminars</a>
                        </section>
                    <?php endif; ?>


                    <?php if (is_active_sidebar('cicss_partners')) : ?>
                        <section id="outreach_partners" class="full-width outreach"  aria-labelledby="partners_h">
                                <?php dynamic_sidebar('cicss_partners'); ?>
                        </section>
                    <?php endif; ?>
				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- .content -->
	</div><!-- .site -->

<?php get_footer(); ?>
