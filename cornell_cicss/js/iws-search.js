		jQuery(document).ready(function($) {
        // Search
		var mousedown = false;
		jQuery('#search-button a').click(function(e) {
			e.preventDefault();
			mousedown = true;
			jQuery('#search-box').toggleClass('open');
			jQuery(this).toggleClass('open');		
			if ( jQuery(this).hasClass('open') ) {
				jQuery('#search-form-query').focus();
			}
			else {
				jQuery(this).focus();
				mousedown = false;
			}
		});
		jQuery('#cu-search input').focus(function() {
			if (!mousedown) {
				jQuery('#search-button a, #search-box').addClass('open');
				mousedown = false;
			}
        });
            
        jQuery('#wpfront-scroll-top-container img').attr('tab-index', '0');
            
        });