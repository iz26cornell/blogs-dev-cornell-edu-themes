/* IWS Dynamic Components
   ------------------------------------------- */  
	
	// Headline Autoscale Variables
	var base_size;
	var base_width;
	var max_size = 72;
	var min_window_size = 0;

	// Window Size Tracking
	function resizeChecks() {
		
		// Restore Standard Main Navigation

		"use strict";
		
		if (jQuery(window).width() > 959) {
			jQuery('#top-navigation ul').removeAttr('style');
			jQuery('#top-navigation #menu-button em').removeClass('open');
			jQuery('#cu-overlay').removeClass('overlay-visible');
			jQuery('#cu-overlay-top').removeClass('overlay-visible');
		}
		
		// Refresh Headline Autoscale 
		if (jQuery(window).width() > min_window_size) {
			jQuery('.autosize-header .home #identity-content h1').addClass('autoscale');
			var multiplier = jQuery('#identity-content').width() / base_width;
			if (multiplier > 0) {
				var new_size = base_size * multiplier;
				if (new_size > max_size) {
					new_size = max_size;
				}
				jQuery('.autosize-header .home #identity-content h1').css('font-size',new_size+'px');
			}
		}
		else {
			jQuery('.autosize-header .home #identity-content h1').removeAttr('style');
			jQuery('.autosize-header .home #identity-content h1').removeClass('autoscale');
		}
		
		// Extend sidebar (remove on resizing to tablet)
		if (jQuery(window).width() < 959) {
			jQuery('#secondary-nav .main-body').css('height', 'auto');
			jQuery('#secondary .main-body').css('height', 'auto');
		}

		// Extend sidebar all the way to footer
		if (jQuery(window).width() > 959) {
			
			var mainBodyHeight = jQuery('#main-body').outerHeight();
			var secondaryNavHeight = jQuery('#secondary-nav').outerHeight();
			var secondaryHeight = jQuery('#secondary').outerHeight();
			
			var calculate = setTimeout(function() {
				if ( secondaryNavHeight + secondaryHeight < mainBodyHeight ) {
					if ( (secondaryNavHeight !== null) && (secondaryHeight === null) ) {
						jQuery('#secondary-nav .main-body').height(mainBodyHeight);
					}
					if ( (secondaryNavHeight === null) && (secondaryHeight !== null) ) {
						jQuery('#secondary .main-body').height(mainBodyHeight);
					}
					if ( (secondaryNavHeight !== null) && (secondaryHeight !== null) ) {
						jQuery('#secondary .main-body').height(mainBodyHeight - secondaryNavHeight);
					}
				}
				clearTimeout(calculate);
			}, 500);
			
		}

	}
	
	
	jQuery(document).ready(function($) {
	
		"use strict";
		
		$('#content').fitVids();
		$('.col-item').fitVids();
		$('iframe').fitVids();
		
		// hide social links ul if empty
		if( !$('#subfooter .social-links').has('li').length ) {
			$('#subfooter .social-links').css('display','none');
		}		

		// Homepage Headline Autoscale
		base_size = parseInt($('.home #identity-content h1').css('font-size'));
		base_width = $('.home #identity-content h1 span').width();
		$(window).resize(resizeChecks);
		resizeChecks();	
		
		//calculate number of elements and add to parent columns class
		$('.columns').each(function(){ 
			var col_count = $(this).children('.col-item').length;
			if (col_count > 4) {
				col_count = 3;}
				$(this).addClass(' columns-'+col_count);
			});
			
		//match column heights
		$(function() {
			if ($(window).width() > 650) {
				$('.columns .col-item').matchHeight();
			}
		});
			
		//Adjustments for Safari on Mac
		if (navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Mac') !== -1 && navigator.userAgent.indexOf('Chrome') === -1) {
			$('html').addClass('safari-mac'); 
		}
	
		//Add truncate class
		var txt = $('#image-band .main_title').text();
        var charCount = txt.length;
        var maxLength = 55;
		console.log(charCount);
    	if (charCount > maxLength) {
			txt = txt.substr(0,maxLength-3) + "...";
			$('#image-band .main_title').text(txt);
		}	

		
		//Improve mouse support
		var timer;
		$('[id$="navigation"] .menu-item-has-children').each(function(){
			$(this).on('mouseover', function(){
				$(this).siblings().children('.sub-menu.open').removeClass('open').addClass('closed');
				$(this).children('.sub-menu').removeClass('closed').addClass('open');
				clearTimeout(timer);
			});	
			if ( !$(this).parents('.sub-menu').length ) {
				$(this).on('mouseout', function(){
					timer = setTimeout(function(){
						$('.sub-menu.open').removeClass('open').addClass('closed');
					}, 750);
				});	
			}
		});	
		
		//Improve keyboard support - Toggle submenu using the top-level menu item as a button instead of a link
		/*$('#wrap #header #navigation li').each(function(){
			$(this).on('click', function(e){
				if ( $(this).hasClass('menu-item-has-children') && $(this).children('.sub-menu').hasClass('closed') ) {
					e.preventDefault();
					$(this).children('.sub-menu.closed').removeClass('closed').addClass('open');
				}
				if ( $(this).hasClass('menu-item-has-children') && $(this).children('.sub-menu').hasClass('open') ) {
					e.preventDefault();
					$(this).children('.sub-menu.closed').removeClass('open').addClass('closed');
				}
				//return false;
			});	
		});	*/
		
		//Improve keyboard support - Toggle submenu using a hidden button
		if ($(window).width() > 959) {
			$('[id$="navigation"] .menu-item-has-children > a').each(function(){
				$(this).append('<button><span><span class="screen-reader-text">Toggle the sub-menu</span></span></button>');
				$(this).append('<span class="screen-reader-text">Press tab again to toggle the sub-menu</span>'); // Let screen readers know about the hidden button
				$(this).parent().attr('aria-haspopup', 'true').addClass('open');
				$('button', this).on('click', function(e){
					e.preventDefault();
					if ( $(this).parents('li').children('.sub-menu').hasClass('closed') ) {
						$(this).attr('aria-expanded', 'true');
						$(this).parents('li').children('.sub-menu.closed').removeClass('closed').addClass('open');
						$(this).removeClass('closedButton').addClass('openButton');
					}
					else if ( $(this).parents('li').children('.sub-menu').hasClass('open') ) {
						$(this).attr('aria-expanded', 'false');
						$(this).parent().parent().children('.sub-menu.open').removeClass('open').addClass('closed');
						$(this).removeClass('openButton').addClass('closedButton');
					}
				});
			});
			
			// Close sub-menu when leaving the last item in the list (if it's the last sub-menu in the tree)
			$('[id$="navigation"] .menu-item-has-children .sub-menu li:last-child').on('focusout', function(){
				if ( $(this).find('.sub-menu').length === 0) {
					$(this).parents('.sub-menu.open').removeClass('open').addClass('closed');
					$(this).parents('.menu-item-has-children').find('.openButton').attr('aria-expanded', 'false').removeClass('openButton').addClass('closedButton');
				}
			});
					
			// Collapse all other sub-menus when any top level parent is focused
			$('[id$="navigation"] .nav-menu > li > a').on('focus', function(){
				$(this).parent().siblings('.menu-item-has-children').find('.sub-menu.open').removeClass('open').addClass('closed');
				$(this).parent().siblings('.menu-item-has-children').find('.openButton').attr('aria-expanded', 'false').removeClass('openButton').addClass('closedButton');
			});
			
			// Collapse sub-menu when expanded button loses focus away from sub-menu
			$('[id$="navigation"] .nav-menu > .menu-item-has-children > a').on('focus', function(){
				$(this).parents('li').children('.sub-menu.open').removeClass('open').addClass('closed');
				$(this).parents('.menu-item-has-children').find('.openButton').attr('aria-expanded', 'false').removeClass('openButton').addClass('closedButton');
			});
		}
		
		// Mobile Navigation
		//var nav_offset = $('#top-navigation #menu-button em').offset();
		
		$('#top-navigation').find('ul.nav-menu').first().addClass('mobile-menu'); // standard
				
		$('#top-navigation #menu-button em').addClass('mobile-menu');
		$('#top-navigation #menu-button').click(function() {
			$(this).toggleClass('open');
			$('#cu-overlay').toggleClass('overlay-visible');
			$('#cu-overlay-top').toggleClass('overlay-visible');
			
			if (navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1) {
				$(this).parent().parent().find('ul.nav-menu').slideToggle(250); 
			}
			else {
				$(this).parent().parent().find('ul.nav-menu').slideToggle(250); 
			}
		});
				
		//mobile nav toggle submenu		
		$('[id$="navigation"] .nav-arrow').click(function() {
			$(this).toggleClass('closed open');
			$(this).closest('li').find('.sub-menu').toggleClass('closed open').slideToggle(250); 
			$(this).closest('li').find('.sub-menu li ul').hide(); 
		});
								
		//Pages widget toggle submenu		
		$('.widget_pages .nav-arrow').click(function() {
			$(this).toggleClass('closed open');
			$(this).closest('li').find('.children:first').slideToggle(200); //pages widget		
			$('#secondary .main-body').removeAttr('style');
			$('#secondary-nav .main-body').removeAttr('style');
			$('#secondary').css('margin-bottom','-5000em').css('padding-bottom','5000em');
			$('#secondary-nav').css('margin-bottom','-5000em').css('padding-bottom','5000em');
			$('#secondary .main-body').css('margin-bottom','-5000em').css('padding-bottom','5000em');
			$('#secondary-nav .main-body').css('margin-bottom','-5000em').css('padding-bottom','5000em');
		});
		
		$('.widget_nav_menu .nav-arrow').click(function() {
			$(this).toggleClass('closed open');
			$(this).closest('li').find('.sub-menu:first').slideToggle(200); //custom menu widget
			$(this).closest('li').find('.sub-menu li ul').hide(); //custom menu widget
		});
		
		$('.page_item_has_children.current_page_item').each(function(){
			$(this).find('.nav-arrow').first().addClass('open');
			$(this).find('.children').first().removeClass('closed').addClass('open');
		});

		$('.widget_pages .current_page_item').each(function(){
			$(this).parent().removeClass('closed').addClass('open');
			$(this).parents('li').last().addClass('active-trail');
			$(this).parent().parent().parent().parent().find('.children').first().removeClass('closed').addClass('open');
			$(this).parent().parent().parent().parent().parent().find('.children').first().removeClass('closed').addClass('open');
			$(this).parent().parent().parent().parent().parent().parent().find('.children').first().removeClass('closed').addClass('open');
			$(this).parent().parent().parent().parent().find('.nav-arrow').first().addClass('open');
			$(this).parent().parent().parent().parent().parent().find('.nav-arrow').first().addClass('open');
			$(this).parent().parent().parent().parent().parent().parent().find('.nav-arrow').first().addClass('open');
			$(this).parent().parent().find('.nav-arrow').first().addClass('open');
			$(this).parent().parent().parent().find('.nav-arrow').first().addClass('open');
			$(this).parent().parent().parent().parent().find('.nav-arrow').first().addClass('open');
			$(this).parent().parent().siblings('.page_item_has_children').find('.nav-arrow').first().removeClass('open');
			$(this).siblings('.page_item_has_children').find('.children').removeClass('open').addClass('closed');
			$(this).siblings('.page_item_has_children').find('.nav-arrow').first().removeClass('open');
			$(this).parent().parent().siblings('.page_item_has_children').find('.children').removeClass('open').addClass('closed');
	});

		// Add button to simple section navigation						
		$('.simple-section-nav .page_item_has_children > a + .children').before('<button class="nav-arrow"><span class="screen-reader-text">Toggle the sub-menu</span></button>');
		
		// Simple section nav sub-menu initial visibility
		$('.simple-section-nav .page_item_has_children.current_page_item > a + button').removeClass('closed').addClass('open');
		$('.simple-section-nav .page_item_has_children.current_page_item > a + button + .children').removeClass('closed').addClass('open');
		$('.simple-section-nav .page_item_has_children.current_page_parent > a + button').removeClass('closed').addClass('open');
		$('.simple-section-nav .page_item_has_children.current_page_parent > a + button + .children').removeClass('closed').addClass('open');
		$('.simple-section-nav .page_item_has_children.current_page_ancestor > a + button').removeClass('closed').addClass('open');
		$('.simple-section-nav .page_item_has_children.current_page_ancestor > a + button + .children').removeClass('closed').addClass('open');
		$('.simple-section-nav .page_item_has_children:not(.current_page_item):not(.current_page_parent):not(.current_page_ancestor) > a ~ .children').addClass('closed');
		
		//Simple section nav widget toggle submenu	
		$('.simple-section-nav .nav-arrow').click(function() {
			$(this).toggleClass('closed open');
			$(this).closest('li').find('.children:first').slideToggle(200); //pages widget
		});
				
		// add banner body class
		if ( $('#cu-identity').hasClass('theme_gray45') ) {
				$('body').addClass('theme_gray_45');
		}	
		else if ( $('#cu-identity').hasClass('theme_red45') ) {
				$('body').addClass('theme_red_45');
		}		
		else if ( $('#cu-identity').hasClass('theme_white45') ) {
				$('body').addClass('theme_white_45');
		}
		
		/* Mobile default pages widget navigation (dropdown) */
		$("<select name='pages_dropdown' title='Select Page' id='responsive-main-nav-menu' onchange='javascript:window.location.replace(this.value);'></select></form>").appendTo(".widget_pages");
		
		// Create default option 
		var widgettitle = $('.widget_pages h3');
		$("<option />", {
		   "selected": "selected",
		   "class"   : "widget-pages-title",
		   "value"   : "",
		   "text"    : widgettitle.text()
		}).appendTo("#responsive-main-nav-menu");
				
		mainNavChildren(jQuery(".widget_pages > ul") , 0);
		
		function mainNavChildren(parent , level){
			jQuery(parent).children("li").each(function(i , obj){
				var label = "";
				for(var k = 0 ; k < level ; k++){
					label += "&nbsp;&nbsp;&nbsp;&nbsp;";
				}
				label += jQuery(obj).children("a").text();
				jQuery("#responsive-main-nav-menu").append("<option value = '" + jQuery(obj).children("a").attr("href") + "'>" + label + "</option>");
				
				if(jQuery(obj).children("ul").size() === 1){
					mainNavChildren(jQuery(obj).children("ul") , level + 1);
				}
			});
		}
	
		/* Mobile hierarchical pages widget navigation (dropdown)*/
		$("<select id = 'responsive-hier-nav-menu' onchange = 'javascript:window.location.replace(this.value);'></select>").appendTo(".widget_hier_page");
		
		// Create default option 
		var widgetHiertitle = $('.widget_hier_page h3');
		$("<option />", {
		   "selected": "selected",
		   "class"   : "widget-hier-pages-title",
		   "value"   : "",
		   "text"    : widgetHiertitle.text()
		}).appendTo("#responsive-hier-nav-menu");
				
		mainHierNavChildren(jQuery(".widget_hier_page > ul") , 0);
		
		function mainHierNavChildren(parent , level){
			jQuery(parent).children("li").each(function(i , obj){
				var label = "";
				for(var k = 0 ; k < level ; k++){
					label += "&nbsp;&nbsp;&nbsp;&nbsp;";
				}
				label += jQuery(obj).children("a").text();
				jQuery("#responsive-hier-nav-menu").append("<option value = '" + jQuery(obj).children("a").attr("href") + "'>" + label + "</option>");
				
				if(jQuery(obj).children("ul").size() === 1){
					mainHierNavChildren(jQuery(obj).children("ul") , level + 1);
				}
			});
		}
	

		// Justified Navigation
		var nav_count = $('.nav-centered #navigation ul.menu').first().find('li').length;
		if (nav_count > 0) {
			var nav_width = 100 / nav_count;
			$('.nav-centered #navigation ul.menu').first().find('li').css( 'width',nav_width+'%');
		}
		
		// Homepage Tabs
		$('.home #subfooter .tab').click(function(e){
			e.preventDefault();
			$('.home #subfooter .tab').removeClass('active');
			$(this).addClass('active');
			$('.tab-content').css('visibility','hidden');
			if ( $(this).hasClass('tab-about') ) {
				$('#about').css('visibility','visible');
			}
			else if ( $(this).hasClass('tab-contact') ) {
				$('#contact').css('visibility','visible');
			}
			else if ( $(this).hasClass('tab-videos') ) {
				$('#videos').css('visibility','visible');
			}
		});

		// Search
		var mousedown = false;
		$('#search-button a').click(function(e) {
			e.preventDefault();
			mousedown = true;
			$('#search-box').toggleClass('open');
			$(this).toggleClass('open');		
			if ( $(this).hasClass('open') ) {
				$('#search-form-query').focus();
			}
			else {
				$(this).focus();
				mousedown = false;
			}
		});
		$('#cu-search input').focus(function() {
			if (!mousedown) {
				$('#search-button a, #search-box').addClass('open');
				mousedown = false;
			}
		});


	});
	
	jQuery(window).load(function() {
		
		"use strict";

		// Reinitialize Headline Autoscale (after remote webfonts load)
		jQuery('.autosize-header .home #identity-content h1').removeAttr('style');
		base_width = jQuery('.home #identity-content h1 span').width();
		resizeChecks();
		
		// Single-line News Headlines (move date to line two)
		jQuery('.home #news h3').each(function() {
  			if (jQuery(this).next('h4.date').position().top - jQuery(this).position().top < 8) {
  				jQuery(this).find('a').append('<br />');
  			}
		});
		
		// Remove problematic tabindex attributes from the AddThis widget and comments form
		jQuery('#atstbx a').removeAttr('tabindex');
		var remove_this = setTimeout(function() {
			if ( jQuery('#atstbx').length > 0 ) {
				jQuery('#atstbx a').removeAttr('tabindex');
				clearTimeout(remove_this);
			}
		}, 500);
		
		jQuery('#comments input').removeAttr('tabindex');
		remove_this = setTimeout(function() {
			jQuery('#comments input').removeAttr('tabindex');
			clearTimeout(remove_this);
		}, 500);

				
		// Extend sidebar all the way to footer
		if (jQuery(window).width() > 959) {
			
			var mainBodyHeight = jQuery('#main-body').outerHeight();
			var secondaryNavHeight = jQuery('#secondary-nav').outerHeight();
			var secondaryHeight = jQuery('#secondary').outerHeight();
			
			var calculate = setTimeout(function() {
				if ( secondaryNavHeight + secondaryHeight < mainBodyHeight ) {
					if ( (secondaryNavHeight !== null) && (secondaryHeight === null) ) {
						jQuery('#secondary-nav .main-body').height(mainBodyHeight);
					}
					if ( (secondaryNavHeight === null) && (secondaryHeight !== null) ) {
						jQuery('#secondary .main-body').height(mainBodyHeight);
					}
					if ( (secondaryNavHeight !== null) && (secondaryHeight !== null) ) {
						jQuery('#secondary .main-body').height(mainBodyHeight - secondaryNavHeight);
					}
				}
				clearTimeout(calculate);
			}, 500);
			
		}
		
	});
