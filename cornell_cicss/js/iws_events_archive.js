/* Cornell Events Calendar (ama39, January 2015) */


/* Global Settings 
   -------------------------------------------------------------------------- */

var api2 = '2.1'; // Localist API version (e.g., '2' for the latest 2.x, '2.1' for the specific point release) 
var pref_date_headers2 = true; // include month and day headers (if set to 'false', the full date will instead display on each entry) TODO: separate into two preferences
var pref_show_time2 = true; // include time
var pref_show_endtime2 = false; // include end time (requires 'pref_show_time' to be true)
var pref_show_location2 = true; // include location
var pref_show_type2 = true; // include event type(s)
var pref_show_thumbs2 = true; // include thumbnails (thumbnails are always off in "compact" mode)
var pref_hide_desc2 = false; // hides event descriptions and provides toggle functionality
var pref_excerpt_length2 = 250; // use 0 for no truncation, using truncation will force descriptions to plaintext
var pref_excerpt_length_compact2 = 125; // excerpt length can be made shorter for 'compact' mode
var pref_allow_rich2 = true; // setting to false will force plaintext descriptions (only affects api 2.1 or later)
var pref_days2 = 365; // range of days to retrieve (overridden by "singleday" requests)
var pref_distinct2 = true; // controls the "distinct" filter for the events query ('true' only returns one instance of a repeating event)
var pref_readmore2 = 'read more'; // label for "read more" links at the end of truncated excerpts 
var pref_eventdetails2 = 'event details'; // label for "event details" toggle buttons
var pref_archive_range2 = 12; // number of past months to retrieve (legacy API 2.0 only)
var pref_category2 = 'dept'; // the event "type" label can be replaced with other localist values to act as custom categorization (currently supports: 'type','dept','group')
var pref_category_filters2 = true; // include filtering based on 'pref_category'
var pref_alpha_categories2 = true; // alphabatize the filters (if 'false', their order will be pseudo-random, based on the order of events in the Localist response)

/* detect feature support */
var supports_rich2 = false;
var supports_direction2 = false;
if (parseFloat(api2) >= 2.1) {
	supports_rich2 = true; // rich text descriptions (HTML) were added in API 2.1
	supports_direction2 = true; // "direction" (asc/desc) was added in API 2.1
}



/* renderEvents() -- initializes and renders events on document.ready
   -------------------------------------------------------------------------- */
/* 
	Parameters:
		target: the DOM container where events will be displayed (required)
		depts: the department(s) to be included, by ID (single dept ID or an array of multiple IDs)
		entries: max number of events to retrieve (max 100 in API 2.1+)
		format: possible values include 'standard' (default), 'compact' (omits thumbnails, type, and end-time, and includes "compact" class for independent styling), and "archive" (past events in reverse chronological order)
		group: another Localist filter option (can be used alone or in combination with 'depts' and 'keyword') 
		singleday: can be used to specify a single day to display events from (e.g., the focused days of Charter Day Weekend)
		keyword: display events with a specific tag or keyword (can be used alone or in combination with 'depts' and 'group')
		
	Examples: 
		renderEvents('events-listing',4212,15);
		-> targets #events-listing, and outputs up to 15 entries from dept 4212 (the Department of Music)
		renderEvents('main',4212,50,'archive');
		-> targets #main, and outputs up to the 50 most recent *past* events from dept 4212
		renderEvents('events',0,50,'standard',0,'2015-04-24','Charter Day Weekend');
		-> targets #events, and outputs up to 50 entries with the tag "Charter Day Weekend," from any dept, and only from a specific day in April

   -------------------------------------------------------------------------- */

function renderEvents2(target2, depts2, entries2, format2, group2, singleday2, keyword2) {
	depts2 = (typeof depts2 === 'undefined') ? 0 : depts2;
	entries2 = (typeof entries2 === 'undefined') ? 3 : entries2;
	format2 = (typeof format2 === 'undefined') ? 'standard' : format2;
	group2 = (typeof group2 === 'undefined') ? 0 : group2;
	singleday2 = (typeof singleday2 === 'undefined') ? false : singleday2;
	keyword2 = (typeof keyword2 === 'undefined') ? false : keyword2;
		
	jQuery(document).ready(function($){
		
		var month_array2 = ['January','February','March','April','May','June','July','August','September','October','November','December'];
		var month_array_abb2 = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		var day_array2 = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
		var day_array_abb2 = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
		var current_year2;
		var current_month2;
		var current_day;
		
		// calculate date ranges (including archive options)
		var today2 = new Date();
		var today_month2 = today2.getMonth();
		var today_year2 = today2.getFullYear();
		var past_year2 = today_year2;
		var past_month2 = today_month2 - pref_archive_range2; // past x months (legacy API 2.0)
		if (past_month2 < 0) {
			past_month2 += 12;
			past_year2 -= 1;
		}
		var start_results2 = today2.getFullYear() + '-' + addLeadingZero(parseInt(today2.getMonth()+1)) + '-' + addLeadingZero(today2.getDate());
		var end_results2 = parseInt(today2.getFullYear()+1) + '-' + addLeadingZero(parseInt(today2.getMonth()+1)) + '-' + addLeadingZero(today2.getDate()); // 'end_results' is only used for archive mode in legacy API 2.0
		if (format2 == 'archive') {
			end_results2 = start_results2;
			if (supports_direction2) {
				start_results2 = (today2.getFullYear()-1) + '-' + addLeadingZero(parseInt(today2.getMonth()+1)) + '-' + addLeadingZero(today2.getDate());
			}
			else { // legacy API 2.0
				start_results2 = past_year2 + '-' + addLeadingZero(parseInt(past_month2+1)) + '-' + addLeadingZero(today2.getDate());
			}
		}
					
		// single day option
		if (singleday2) {
			start_results2 = singleday2;
			pref_days2 = 1;
		}
		
		// build query filters
		query = {
			api_key: 'KLhy2GtuSAGirYGY',
			pp: entries2,
			start: start_results2,
			days: pref_days2,
			distinct: pref_distinct2
		};
		if (depts2 && depts2 != 0) {
			query.type = depts2;
		}
		if (group2 != 0) {
			query.group_id = group2;
		}
		if (keyword2 && keyword2 != '') {
			query.keyword = keyword2;
		}
		if (format2 == 'archive' && !supports_direction2) {
			query.end = end_results2; // legacy API 2.0
		}
		else {
			query.days = pref_days2;
		}
		if (format2 == 'archive' && supports_direction2) {
			query.direction = 'desc';
		}
		
		// loading animation icon (requires Font Awesome)
		$('#'+target2).append('<div id="loader2"><span class="fa fa-spin fa-cog"></span></div>');
		var c_loader = setTimeout(function(){ $('#loader2').fadeIn(50); }, 500); // skip loading animation if under 0.5s
					
		// send query
		$.ajax({
			url: '//events.cornell.edu/api/'+api+'/events',
			dataType: 'jsonp',
			crossDomain: true,
			data: query,
			complete: function(xhr, status) {
				// cancel loading animation
				clearTimeout(c_loader);
				$('#loader2').fadeOut(200);
			},
			success: function(json) {
				//console.log(json); // response object
				
				var events2 = json.events;
				var department_ids2 = [];
				var department_names2 = [];
				var events_container2 = $('<div class="events-list">');
				if (format2 == 'compact') {
					$('#'+target2).addClass('compact');
				}
				if (!pref_show_type2) {
					$('#'+target2).addClass('no-type');
				}
				if (!pref_show_location2) {
					$('#'+target2).addClass('no-location');
				}
				if (!pref_show_thumbs2) {
					$('#'+target2).addClass('no-thumbnails');
				}
				if (pref_hide_desc2) {
					$('#'+target2).addClass('hide-descriptions');
				}
				// archive custom sorting
				if (format2 == 'archive' && !supports_direction2) {
					events2 = events2.reverse(); // legacy API 2.0
				}
				
				// Data Selection and Processing
				for (i=0;i<events2.length;i++) {
					
					// basic event data
					title = events2[i].event.title;
					localist_url = events2[i].event.localist_url;
					location_name = events2[i].event.location_name;
					room_number = events2[i].event.room_number;
					venue_url = events2[i].event.venue_url;
					photo_url = events2[i].event.photo_url.replace('/huge/','/big/'); // retrieve thumbnail size
					
					// description (with rich text and excerpt options)
					excerpt_length = pref_excerpt_length2;
					if (format2 == 'compact') {
						excerpt_length = pref_excerpt_length_compact2;
					}
					
					if (!supports_rich2) {
						if (excerpt_length > 0 && events2[i].event.description.length > excerpt_length) {
							description = $.trim($.trim(events[i].event.description).substring(0, excerpt_length).split(' ').slice(0, -1).join(' '));
						}
						else {
							description = events2[i].event.description;
						}
					}
					else {
						if (excerpt_length > 0) {
							if (events2[i].event.description_text.length > excerpt_length) {
								description = $.trim($.trim(events2[i].event.description_text).substring(0, excerpt_length).split(' ').slice(0, -1).join(' '));
							}
							else {
								description = events2[i].event.description_text;
							}
							// TODO: add support for Rich Text (HTML) truncation
						}
						else {
							if (pref_allow_rich2) {
								description = events2[i].event.description;
							}
							else {
								description = events2[i].event.description_text;
							}
						}
					}
					
					// date and time
					event_fulldate = new Date(events2[i].event.event_instances[0].event_instance.start);
					event_day = day_array_abb2[event_fulldate.getDay()];
					event_date = events2[i].event.event_instances[0].event_instance.start.split('T')[0];
					event_time = events2[i].event.event_instances[0].event_instance.start.split('T')[1];
					event_time = convertTime(event_time); // convert to 12-hour format
					year = event_date.split('-')[0];
					month = event_date.split('-')[1].replace(/\b0+/g, ''); // remove leading zeroes
					day = event_date.split('-')[2].replace(/\b0+/g, ''); // remove leading zeroes
					event_date = month+'/'+day+'/'+year; // convert date format
					//event_date_compact = month+'/'+day; // for compact mode (numbers only, e.g., "4/13")
					event_date_compact = month_array_abb2[month-1]+' '+day; // for compact mode (month text, e.g., "Apr 13")
					if (events2[i].event.event_instances[0].event_instance.all_day) {
						event_time = 'all day';
					}
					
					// optional fields
					department = 0;
					type = 0;
					if (typeof events2[i].event.filters.departments !== 'undefined') {
						department = events2[i].event.filters.departments[0].id; // TODO: add support for multiple departments per event ( departments[1+] )
					}
					if (typeof events2[i].event.filters.event_types2 !== 'undefined') {
						type = events2[i].event.filters.event_types2[0].id; // TODO: add support for multiple types per event per event ( event_types2[1+] )
					}
					group_name = '';
					group_id = 0;
					if (typeof events2[i].event.group_name !== 'undefined') {
						group_name = events2[i].event.group_name;
						group_id = events2[i].event.group_id;
					}
					event_types2 = '';
					if (typeof events2[i].event.filters.event_types2 !== 'undefined') {
						event_types2 = events2[i].event.filters.event_types2[0].name;
					}
					if (pref_category2 == 'dept' && department != 0) {
						event_types2 = events2[i].event.filters.departments[0].name;
					}
					if (pref_category2 == 'group' && group_name != '') {
						event_types2 = group_name;
					}
					if (pref_category_filters2) {
						if (pref_category2 == 'type' && type != 0) {
							if ( $.inArray(events2[i].event.filters.event_types2[0].id,department_ids2) == -1 ) {
								department_ids2.push(events2[i].event.filters.event_types2[0].id);
								department_names2.push(events2[i].event.filters.event_types2[0].name);
								// TODO: add support for multiple types per event ( event_types2[1+] )
							}
						}
						else if (pref_category2 == 'dept' && department != 0) {
							if ( $.inArray(events2[i].event.filters.departments[0].id,department_ids2) == -1 ) {
								department_ids2.push(events2[i].event.filters.departments[0].id);
								department_names2.push(events2[i].event.filters.departments[0].name);
								// TODO: add support for multiple departments per event ( departments[1+] )
							}
						}
						else if (pref_category2 == 'group' && group_name != '') {
							if ( $.inArray(events2[i].event.group_id,department_ids2) == -1 ) {
								department_ids2.push(group_id);
								department_names2.push(group_name);
							}
						}
					}
					ticket_cost = '';
					if (typeof events2[i].event.ticket_cost !== 'undefined') {
						ticket_cost = events2[i].event.ticket_cost;
					}
					event_time_end = '';
					if (typeof events2[i].event.event_instances[0].event_instance.end !== 'undefined' && events2[i].event.event_instances[0].event_instance.end != null) {
						event_time_end = events2[i].event.event_instances[0].event_instance.end.split('T')[1];
						event_time_end = convertTime(event_time_end); // convert to 12-hour format
					}
					
					// build node
					event_node2 = $('<div class="node">').addClass('dept-'+department).addClass('type-'+type).addClass('group-'+group_id);
					
					c_title = $('<h4>').html('<a target="_blank" href="'+localist_url+'">'+title);
					c_location = $('<p class="meta location">').text(location_name);
					c_type = $('<p class="meta type"><span class="fa"></span>').append(event_types2);
					c_date = $('<p class="meta date">')
					if (!pref_date_headers2 || format2 == 'compact') {
						$(c_date).append('<span class="fulldate"></span> ');
						if (format2 == 'compact') {
							$(c_date).find('.fulldate').text(event_date_compact);
						}
						else {
							$(c_date).find('.fulldate').text(event_date);
						}
					}
					$(c_date).append('<span class="start">'+event_time+'</span>');
					if (event_time_end != '' && pref_show_endtime2) {
						$(c_date).find('.start').after(' to <span class="end">'+event_time_end+'</span>');
					}
					
					if (excerpt_length > 0 || !supports_rich2 || (supports_rich2 && pref_allow_rich2)) {
						c_abstract = $('<p class="description">').text(description);
					}
					else {
						c_abstract = $('<div class="description"><div class="description-content">'+description+'</div></div>');
					}
					if (excerpt_length > 0 ) {
						c_abstract.append('... <a class="read-more more"></a>').find('.read-more').attr('href',localist_url).attr('target','_blank').text(pref_readmore);
					}
					if (format2 != 'compact' && pref_show_thumbs2) {
						event_node2.append('<a><img class="event-image dropshadow-light" src="'+photo_url+'" target="_blank" /></a>');
						event_node2.find('a').first().attr('href',localist_url).attr('target','_blank');
					}
					
					event_node2.append(c_title);
					event_node2.append(c_date);
					if (pref_show_location2) {
						event_node2.append(c_location);
					}
					if (pref_show_type2 && format2 != 'compact') {
						event_node2.append(c_type);
					}
					if (description) {
						event_node2.append('<div class="event-details"></div>').append(c_abstract);
						if (pref_hide_desc) {
							event_node2.find('.event-details').prepend('<h4><a href="#"><span class="fa fa-chevron-down"></span>'+pref_eventdetails2+'</a></h4>');
						}
					}
					
					// month and day headers / Note: Font Awesome icons removed.
					if (pref_date_headers2) {
						if (format2 != 'compact' && month != current_month2) {
							current_month2 = month;
							current_day2 = day;
							mheader = $('<h3 class="month-header">').text(month_array2[month-1] + ' ' + year);
							dheader = $('<h4 class="day-header">').text(event_day + ', ' + month_array2[month-1] + ' ' + daySuffix(day));
							events_container2.append(mheader).append(dheader);
						}
						else if (format2 != 'compact' && day != current_day2) {
							current_day2 = day;
							dheader = $('<h4 class="day-header">').text(event_day + ', ' + month_array2[month-1] + ' ' + daySuffix(day));
							events_container2.append(dheader);
						}
					}
					
					events_container2.append(event_node2);
				}
				$('#'+target2).append(events_container2);
				
				/* Event Filters
				if (pref_category_filters2) {
				
					
					/* build filter UI [close comment tag]
					event_filters2 = $('<div id="events-filters"><h3 class="hidden">Show:</h3><ul class="events-filters" /></div>');
					event_filters2.children('ul').append('<li><a data-filter="all" class="active" href="#">All Events</a></li>');
					
					// alphabetize option
					department_ids_sorted2 = department_ids2.slice(0);
					department_names_sorted2 = department_names2.slice(0);
					if (pref_alpha_categories2) {
						department_names_sorted2.sort();
						for (i=0;i<department_ids2.length;i++) {
							department_ids_sorted2[i] = department_ids2[department_names2.indexOf(department_names_sorted2[i])]; // translate IDs to match alphabetized names
						}
					}
					for (i=0;i<department_ids2.length;i++) {
						event_filters2.children('ul').append('<li><a href="#" data-filter="' + department_ids_sorted2[i] + '">' + department_names_sorted2[i] + '</a></li>');
					}
					$('#'+target2).prepend(event_filters2);
					$('#'+target2+' .events-filters a').click(function(e) {
						e.preventDefault();
						
						$('#'+target2+' .events-filters a').removeClass('active');
						$(this).addClass('active');
						
						if ($(this).attr('data-filter') == 'all') {
							$('#'+target2+' .node').show();
						}
						else {
							$('#'+target2+' .node').hide();
							$('#'+target2+' .node.'+pref_category2+'-'+$(this).attr('data-filter')).show();
						}
						
						// refresh headers
						$('.day-header, .month-header').hide();
						$('.day-header').each(function() {
							if ($(this).nextUntil('.day-header','.node:visible').length > 0) {
								$(this).show();
							}
						});
						$('.month-header').each(function() {
							if ($(this).nextUntil('.month-header','.day-header:visible').length > 0) {
								$(this).show();
							}
						});
					});
					
					/* URL parameter support [close comment tag]
					var url_params = window.location.search.substring(1).split('&');
					for (i=0;i<url_params.length;i++) {
						c_parameter = url_params[i].split('=');
						if (c_parameter[0] == 'id' && !isNaN(c_parameter[1]) && format2 != 'compact') {
							$('#'+target2+' .events-filters a').each(function() {
								if ($(this).attr('data-filter') == c_parameter[1]) {
									$(this).trigger('click');
								}
							});
						}
					}
					
				}
                        If reactivaing functionality, remove -> */
				// Show/Hide Descriptions
				if (pref_hide_desc2) {
					$('.events-archive-listing .description').hide();
					$('#'+target2+' .event-details a').click(function(e) {
						e.preventDefault();
						console.log('click');
						
						$(this).parents('.event-details').first().next('.description').slideToggle(150);
						if ( $(this).children('.fa').hasClass('fa-chevron-down') ) {
							$(this).children('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-up');
						}
						else {
							$(this).children('.fa').removeClass('fa-chevron-up').addClass('fa-chevron-down');
						}
					});
				}
				
				//projectCustom2(); // run any project-specific post-processing
			}
		});	
		
		// Utility Functions
		function daySuffix(day) {
			switch (day) {
				case '1':
				case '21':
				case '31':
					return day+'st';
				case '2':
				case '22':
					return day+'nd';
				case '3':
				case '23':
					return day+'rd';
				default:
					return day+'th';
			}
		}
		function addLeadingZero(num) {
			if (num.toString().length == 1) {
				num = '0'+num;
			}
			return num;
		}
		function convertTime(time) {
			time_hour = time.split(':')[0];
			time_min = time.split(':')[1];
			if (parseInt(time_hour) >= 12) {
				if (parseInt(time_hour) > 12) {
					time_hour = (parseInt(time_hour) - 12);
				}
				return time_hour + ':' + time_min + ' p.m.';
			}
			else {
				return parseInt(time_hour) + ':' + time_min + ' a.m.';
			}
		}
		
		
		/* Custom Postprocessing (for project-specific needs) 
	   -------------------------------------------------------------------------- */
	   function projectCustom2() {
	   	
	   	// -------------- Small Farms Program ---------------------------------
	   	
	   	// move miscellaneous departments to the end of the category filters (leaving CCE regions first)
	   	$('.events-filters a').each(function() {
	   		if ( $(this).text().indexOf('CCE ') != 0 && $(this).text() != 'All Events') {
	   			//console.log($(this).text().indexOf('CCE '));
	   			//$(this).parent('li').hide();
	   			$(this).parent('li').parent('ul').append( $(this).parent('li') );
	   		}
	   	});
	   	
	   	// remove "CCE" from department labels (e.g., "CCE Tompkins" becomes "Tompkins")
	   	// remove "Small Farms Program:" from group labels (e.g., "Small Farms Program: Fingerlakes" becomes "Fingerlakes")
	   	$('.events-filters a, .events-list .type').each(function() {
	   		if ( $(this).text().indexOf('CCE ') == 0) {
	   			$(this).text( $(this).text().replace('CCE ','') );
	   			if ( $(this).hasClass('type') ) {
						$(this).prepend('<span class="fa"></span>'); 
					}
	   		}
	   		if ( $(this).text().indexOf('Small Farms Program: ') == 0) {
	   			$(this).text( $(this).text().replace('Small Farms Program: ','') );
	   			if ( $(this).hasClass('type') ) {
						$(this).prepend('<span class="fa"></span>'); 
					}
	   		}
	   	});
	   	
	   	// link NY State map hotspots to filter buttons
	   	$('#m_nymap area').click(function(e) {
				e.preventDefault();
				
				c_filterid = $(this).attr('href').split('id=')[1]; // get id
				$('.events-filters a').each(function() {
					if ( $(this).attr('data-filter') == c_filterid) {
						$(this).trigger('click');
					}
				});
			});
	   	
	   }
				
	});
}

