<?php
/**
 * Template Name: Right Content, No Sidebar
 * The template for displaying a page without a sidebar.
 *
 * @package Pena
 */

get_header(); ?>

<div class="hfeed site blog">
	<div class="content site-content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" aria-label="Main Content">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/content', 'page' ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					?>

				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- .content -->
</div><!-- .site -->
<?php get_footer(); ?>