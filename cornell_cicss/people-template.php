<?php
/**
 * Template Name: People Template w/ Sidebar
 * Applied to the page where you want team members displayed
 *
 * @package Pena
 */

get_header('custom'); ?> 
	<div class="featured-image">
	<?php if ( has_post_thumbnail() ): ?>
		<?php $image_id = get_post_thumbnail_id(); ?>
		<?php $image_url = wp_get_attachment_image_src( $image_id,'full' );   ?>
		<div class="header section pages" style="background-image:url(<?php echo esc_url( $image_url[0] ); ?>);">
		<div class="entry-content">
			<h1><?php the_title(); ?></h1>
		</div>
		<span class="overlay"></span>
		</div>
	<?php else: ?>
		<div class="header section pages without-featured-image">
		<div class="entry-content">
		<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<hr class="short">
		</header><!-- .entry-header -->
		</div>
		</div>
	<?php endif; ?>
	</div>
	<div class="hfeed site page-content">
		<div class="content site-content">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'template-parts/content', 'page-default' ); ?>
                        
						<?php
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
				?>

					<?php endwhile; // End of the loop. ?>
                    
                    <?php //CWD: Adds members from CICSS Team Members
                    $member_query = new WP_Query(array('post_type'=>'member', 'post_status'=>'publish', 'meta_key'=>'priority', 'posts_per_page'=>-1, 'order'=>'ASC', 'orderby'=>'priority'));
                    ?>
                    
                    <?php if($member_query->have_posts() ) : ?>
                        <section id="member_sec" aria-label="CICSS Team Members">
                            <h2>Core Team</h2>
                            <div class="member_group" id="member_core_group"> 
                            <?php while($member_query->have_posts() ) : $member_query->the_post(); ?>
                            <?php 
                                $memcat = get_field('category');
                                $role = get_field('department');
                                
                            ?>
                            
                            <?php if ($memcat=="core") : ?>
                                    <div class="member member_core">
                                        <div class="member_pic">
                                            <a href="<?php the_permalink(); ?>">
                                        <?php 
                                            if (has_post_thumbnail() ){
                                                the_post_thumbnail(); 
                                            } else { ?>
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?> /img/person-random.png" alt="<?php the_title(); ?>"/>
                                            <?php } ?>
                                            </a>
                                        </div>
                                        <div class="member_content">
                                        <h3><?php the_title(); ?></h3>
                                        <p class="member_role"><?php echo $role ?></p>
                                        <p class="member_descr"><?php the_content(); ?></p>
                                        </div>
                                    </div>
                            <?php endif; ?>
                            <?php endwhile; ?>
                            </div>
                            <h2>Our Interns</h2>
                            <div class="member_group" id="member_intern_group">
                            <?php while($member_query->have_posts() ) : $member_query->the_post(); ?>
                            <?php 
                                $memcat = get_field('category');
                                $role = get_field('department');
                                
                            ?>
                            
                            <?php if ($memcat=="intern") : ?>
                                    <div class="member member_intern">
                                        <div class="member_pic">
                                            <a href="<?php the_permalink(); ?>">
                                        <?php 
                                            if (has_post_thumbnail() ){
                                                the_post_thumbnail(); 
                                            } else { ?>
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/person-random.png" alt="<?php the_title(); ?>"/>
                                            <?php } ?>
                                            </a>
                                        </div>
                                        <div class="member_content">
                                        <h3><?php the_title(); ?></h3>
                                        <p class="member_role"><?php echo $role ?></p>
                                        </div>
                                    </div>
                            <?php endif; ?>
                            <?php endwhile; ?>
                            </div>
                            <h2>Our Advisory Board</h2>
                            <div class="member_group" id="member_advisory_group">
                            <?php while($member_query->have_posts() ) : $member_query->the_post(); ?>
                            <?php 
                                $memcat = get_field('category');
                                $role = get_field('department');
                                
                            ?>
                            
                            <?php if ($memcat=="board") : ?>
                                    <div class="member member_board">
                                        <div class="member_pic">
                                            <a href="<?php the_permalink(); ?>">
                                        <?php 
                                            if (has_post_thumbnail() ){
                                                the_post_thumbnail(); 
                                            } else { ?>
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/person-random.png" alt="<?php the_title(); ?>"/>
                                            <?php } ?>
                                            </a>
                                        </div>
                                        <div class="member_content">
                                        <h3><?php the_title(); ?></h3>
                                        <p class="member_role"><?php echo $role ?></p>
                                        </div>
                                    </div>                                 
                            <?php endif; ?>
                            <?php endwhile; ?>
                            </div>
                        </section>
                            <?php wp_reset_postdata(); ?>
                    
                        <?php else : ?>
                            <section id="member_sec" aria-label="CICSS Team Members"><p><?php _e('No members found.'); ?></p></section>
                    <?php endif; ?>
                        <?php if ( is_active_sidebar( 'about_widget' ) ) : ?>
                                <?php dynamic_sidebar( 'about_widget' ); ?>
                        <?php endif; ?>
				</main><!-- #main -->
			</div><!-- #primary -->
			<?php get_sidebar(); ?>
		</div><!-- .content -->
	</div><!-- .site -->

<?php get_footer(); ?>