<?PHP
if (isset($_GET['btnG'])) {
	session_start();
	$selected_radio = $_GET['sitesearch'];
	
	if ($selected_radio == 'cornell') {
		$search_terms = urlencode($_GET['s']);
		$URL="http://www.cornell.edu/search" . "?q=" . $search_terms . "&client=default_frontend&proxystylesheet=default_frontend";
		print $URL;
		header ("Location: $URL");
	}
}

?>

<?php get_header(); ?>
<div id="wrap" class="twocolumn-left">
<div id="main-navigation">
          <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
          <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
        </div>
        
<div id="content-wrap" class="twocolumn-left"> 
                
      <div id="header" class="twocolumn-left">
       <div class="header-meta">
         <h1><a href="<?php echo get_settings('home'); ?>/"><strong id="headerPageTitle">Cornell MRI Facility</strong></a></h1>
         <!-- <h2><?php bloginfo( 'description' ); ?></h2> -->
           </div>
       <img class="campaign-image" src="<?php header_image(); ?>" alt="" />
       
        </div>
        
      </div>
      <div id="content">
 <div id="main" class="twocolumn-left">
  <h2 class="page-title"><?php printf( __( 'Search Results for: %s' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
    <?php if (have_posts() && $_GET['s'] != "") : while (have_posts()) : the_post(); ?>
    
    <?php print_post_title() ?>
    <?php the_excerpt(__('Read more'));?>
    <!--
	<?php trackback_rdf(); ?>
	-->
    <?php endwhile; else: ?>
    <h3 class="entry-title"><?php _e( 'Nothing Found' ); ?></h3>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
  </div>


        <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
        <?php endif; ?>
      </div>
</div>
</div>
<?php get_footer(); ?>



  
     