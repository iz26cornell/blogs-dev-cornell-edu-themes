/* Tab UI
   ------------------------------------------- */   

/* Global Options -------------- */

/* Global Variables ------------ */
var tabset_count = 0;


/* -----------------------------------------------------------------------------------------
   Initialize Tab Sets
   -----------------------------------------------------------------------------------------
   - 
-------------------------------------------------------------------------------------------- */
function tabs() {
	
	jQuery(".tabset").each(function() {
		tabset_count++;
		window["tabset"+tabset_count] = jQuery(this).tabs();
	});

}


function tabsWithNav() {
	
	var $tabs = jQuery('#tabs-withnav').tabs();
	jQuery("#tabs-withnav .ui-tabs-panel").each(function(i) {
		var totalSize = jQuery("#tabs-withnav .ui-tabs-panel").size() - 1;
		if (i != totalSize) {
			next = i + 2;
			jQuery(this).append("<a href='#' class='next-tab mover' rel=" + next + ">Next Page</a>");
			
		}
		if (i != 0) {
			prev = i;
			jQuery(this).append("<a href='#' class='prev-tab mover' rel=" + prev + ">Prev Page</a>");
			
		}
	});
	
	jQuery('.next-tab, .prev-tab').click(function() {
		$tabs.tabs('select', jQuery(this).attr("rel"));
		return false;
		
	});
}