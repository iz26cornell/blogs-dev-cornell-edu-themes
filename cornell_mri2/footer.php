<!-- begin footer -->
<div id="footer">
  <div id="footer-content">
    <div id="footer-content-wrap">
	
      <div id="footer-right">
       
        <ul class="meta">
          <li>&copy;<?php echo date("Y");?> <a href="http://www.cornell.edu/">Cornell University</a></li>
          <?php wp_register(); ?>
          <li class="last">
            <?php wp_loginout(); ?>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div id="edublogs"><?php wp_footer();?></div><!-- edublogs -->
</body></html>