<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="distribution" content="global" />
<meta name="robots" content="follow, all" />
<meta name="language" content="en, sv" />
<title>
<?php wp_title(''); ?>
<?php if(wp_title('', false)) { echo ' |'; } ?>
 <?php bloginfo('name');
 	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	?>
</title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<!-- leave this for stats please -->
<link rel="Shortcut Icon" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_get_archives('type=monthly&format=link'); ?>
<?php wp_head(); ?>

<link rel="stylesheet" rev="stylesheet" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/styles/colorbox.css" media="all" />

<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/scripts/jquery.min.js"></script>
<style type="text/css" media="screen">
<!--
@import url( <?php bloginfo('stylesheet_url');
?> );
-->
</style>
<link rel="stylesheet" rev="stylesheet" media="print" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/styles/print.css" />

<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/scripts/colorbox/jquery.colorbox.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#imageCorral a').colorbox({rel:'gal',transition:"none",opacity:0.8});
	});
</script>

<script type="text/javascript">
	var slideSpeed = (<?php echo get_theme_mod( 'slider_speed', 'default_value' ); ?> * 1000);
	var slidePause = (<?php echo get_theme_mod( 'slider_pause', 'default_value' ); ?> * 1000);
</script>


</head>
<?php $body_classes = cornell_mri_body_classes();  ?>
<body <?php body_class($body_classes); ?>>

<?php include (TEMPLATEPATH . '/banner.php'); ?>

