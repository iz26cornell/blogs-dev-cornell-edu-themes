<?php
/**
 * Template Name: Homepage
 * @package WordPress
 * @subpackage Cornell
 */
get_header();  ?>


<div id="wrap">
	<div id="main-navigation">
		<?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
		<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
	</div>
	<!-- main navigation -->
	
	<div id="content-wrap" class="twocolumn-left">
		<div id="header" class="twocolumn-left">
			<div class="header-meta">
				<h1><a href="<?php echo get_settings('home'); ?>/"><strong id="headerPageTitle"><?php bloginfo('name'); ?></strong></a></h1>
				<h2><?php bloginfo( 'description' ); ?></h2>
			</div>
			<img class="campaign-image" src="<?php header_image(); ?>" alt="" />
		</div>
	</div>
	<div id="content">
    	<?php print cornell_theme_slider(); ?> 
		
	</div>
	
	<div class="clear"></div>
	
	<!-- The widgets are now stored on the bottom of the main page -->
	<div id="widgetsWrapper">
		<div id="home-left"><?php if ( is_active_sidebar( 'homepage-left' ) ) : ?>
			<?php dynamic_sidebar( 'homepage-left' ); ?>
			<?php endif; ?>
		</div>
		<div id="home-center"><?php if ( is_active_sidebar( 'sidebar-home' ) ) : ?>
			<?php dynamic_sidebar( 'sidebar-home' ); ?>
			<?php endif; ?>
		</div>
		<div id="home-right"><?php if ( is_active_sidebar( 'homepage-right' ) ) : ?>
			<?php dynamic_sidebar( 'homepage-right' ); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
