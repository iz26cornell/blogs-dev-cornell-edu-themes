<!-- The following div contains the Cornell University logo and search link -->
<div id="skipnav">
	<a href="#main">Skip to main content</a>
</div>
<div id="cu-identity" class="theme-red75">
	<div id="cu-logo">
		<a href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/cornell_identity/cu_logo_white45.gif" alt="Cornell University" /></a>
	</div>
	<div id="search-form">
			<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" >
            <div id="search-input">
            	<label for="search-form-query">SEARCH:</label>
     			<input type="text" value="" name="s" id="search-form-query" size="20" />
     			<input type="submit" id="search-form-submit" name="btnG" value="go" />
     		</div>
     		
            
            <div id="search-filters">
					<input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
					<label for="search-filters1">This Site</label>
				
					<input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
					<label for="search-filters2">Cornell</label>
					
					<a href="http://www.cornell.edu/search/">more options</a>
			</div>	
            </form>

            <!--<form action="http://www.cornell.edu/search/" method="get" enctype="application/x-www-form-urlencoded">
			<div id="search-input">
				<label for="search-form-query">SEARCH:</label>
				<input type="text" id="search-form-query" name="q" value="" size="20" />
				<input type="submit" id="search-form-submit" name="btnG" value="go" />
				<input type="hidden" name="output" value="xml_no_dtd" />
				<input type="hidden" name="sort" value="date:D:L:d1" />

				<input type="hidden" name="ie" value="UTF-8" />
				<input type="hidden" name="gsa_client" value="default_frontend" />
				<input type="hidden" name="oe" value="UTF-8" />
				<input type="hidden" name="site" value="default_collection" />
				<input type="hidden" name="proxystylesheet" value="default_frontend" />
				<input type="hidden" name="proxyreload" value="1"/>
                
			</div>

			<div id="search-filters">
					<input type="radio" id="search-filters1" name="sitesearch" value="www.cit.cornell.edu" checked="checked" />
					<label for="search-filters1">BOOM</label>
				
					<input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
					<label for="search-filters2">Cornell</label>
					
					<?php /*?><a href="http://www.cornell.edu/search/">more options</a><?php */?>
			</div>	
		</form>-->

	</div>
</div>
