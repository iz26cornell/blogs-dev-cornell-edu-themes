<?php

// Check if the user is accessing the admin section.
// If so, execute theme settings functions by including the admin.php file.

if ( function_exists('register_sidebar') )	

	register_sidebar( array(
		'name' => __( 'Header', 'Cornell' ),
		'id' => 'header-widget',
		'description' => __( 'Header area in banner', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="header-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Homepage (left)', 'Cornell' ),
		'id' => 'homepage-left',
		'description' => __( 'Sub-section on left side of homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="entry-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Homepage (right)', 'Cornell' ),
		'id' => 'homepage-right',
		'description' => __( 'Sub-section on right side of homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="entry-title">',
		'after_title' => '</h3>',
	) );
    
	register_sidebar( array(
		'name' => __( 'Sidebar (homepage)', 'Cornell' ),
		'id' => 'sidebar-home',
		'description' => __( 'The primary widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (homepage extra)', 'Cornell' ),
		'id' => 'sidebar-home-extra',
		'description' => __( 'Secondary widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (secondary)', 'Cornell' ),
		'id' => 'sidebar-1',
		'description' => __( 'The primary widget area for secondary pages.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	/*register_sidebar( array(
		'name' => __( 'Footer Links (all pages)', 'Cornell' ),
		'id' => 'footer-widget-area',
		'description' => __( 'A customizable menu of footer links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Text (all pages)', 'Cornell' ),
		'id' => 'footer-message',
		'description' => __( 'This content appears under the footer copyright and links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) ); */


add_action( 'after_setup_theme', 'Cornell_setup' );
if ( ! function_exists( 'Cornell_setup' ) ):
function Cornell_setup() {	
	
	// Your changeable header business starts here
	define( 'HEADER_TEXTCOLOR', '462406' );
	// No CSS, just an IMG call. The %s is a placeholder for the theme template directory URI.
	define( 'HEADER_IMAGE', '%s/images/project/home1.jpg' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'Cornell' ),
	) );

	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to Cornell_header_image_width and Cornell_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'Cornell_header_image_width', 960 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'Cornell_header_image_height', 100 ) );
	
	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 960 pixels wide by 160 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	// set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See Cornell_admin_header_style(), below.
	add_custom_image_header( 'Cornell_header_style', 'Cornell_admin_header_style', 'Cornell_admin_header_image' );

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	
	register_default_headers( array(
		'banner1' => array(
			'url' => '%s/images/project/home1.jpg',
			'thumbnail_url' => '%s/images/project/home1_t.jpg',
			'description' => __( 'Workday 1', 'Cornell' )
		),
		'banner2' => array(
			'url' => '%s/images/project/home2.jpg',
			'thumbnail_url' => '%s/images/project/home2_t.jpg',
			'description' => __( 'Workday 2', 'Cornell' )
		),
		'banner3' => array(
			'url' => '%s/images/project/home3.jpg',
			'thumbnail_url' => '%s/images/project/home3_t.jpg',
			'description' => __( 'Workday 3', 'Cornell' )
		),
		'banner4' => array(
			'url' => '%s/images/project/home4.jpg',
			'thumbnail_url' => '%s/images/project/home4_t.jpg',
			'description' => __( 'Workday 4', 'Cornell' )
		)
	) );
	
}
endif;

if ( ! function_exists( 'Cornell_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog
 *
 */
function Cornell_header_style() {

	// If no custom options for text are set, let's bail
	// get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
	if ( HEADER_TEXTCOLOR == get_header_textcolor() )
		return;
	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( 'blank' == get_header_textcolor() ) :
	?>
		#site-title,
		#site-description {
			position: absolute;
			left: -9000px;
		}
	<?php
		// If the user has set a custom color for the text use that
		else :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?> !important;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif;


if ( ! function_exists( 'Cornell_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 * Referenced via add_custom_image_header() in Cornell_setup().
 */
function Cornell_admin_header_style() {
?>
	<style type="text/css">
	.appearance_page_custom-header #headimg {
		background: #fff;
		border: none;
	}
	#headimg h1,
	#desc {
		font-family: Georgia, serif;
	}
	#headimg h1 {
		margin: 0;
		font-weight: normal;
		padding: 2px 0 0 0;
	}
	#headimg h1 a {
		font-size: 36px;
		line-height: 42px;
		text-decoration: none;
	}
	#desc {
		font-size: 18px;
		line-height: 31px;
		padding: 0 0 2px 0;
		font-style: italic;
	}
	<?php
		// If the user has set a custom color for the text use that
		if ( get_header_textcolor() != HEADER_TEXTCOLOR ) :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?>;
		}
	<?php endif; ?>
	#headimg img {
		max-width: 400px;
		padding-top: 5px;
	}
	</style>
<?php
}
endif;

if ( ! function_exists( 'Cornell_admin_header_image' ) ) :
/**
 * Custom header image markup displayed on the Appearance > Header admin panel.
 * Referenced via add_custom_image_header() in Cornell_setup().
 */
function Cornell_admin_header_image() { ?>
	<div id="headimg">
		<?php
		if ( 'blank' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) || '' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) )
			$style = ' style="display:none;"';
		else
			$style = ' style="color:#' . get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) . ';"';
		?>
		<h1><a id="name"<?php echo $style; ?> onclick="return false;" href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
		<div id="desc"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></div>
		<img src="<?php esc_url ( header_image() ); ?>" alt="" />
	</div>
<?php }
endif;

/*
 *  Returns the Cornell options with defaults as fallback
 */
function Cornell_get_theme_options() {
	$defaults = array(
		'color_scheme' => 'light',
		'theme_layout' => 'content-sidebar',
	);
	$options = get_option( 'Cornell_theme_options', $defaults );
	return $options;
}

/*
 *  Add section class to body tag
 */
add_filter('body_class','body_class_section');
function body_class_section ($classes) {
	global $wpdb, $post;
	if (is_page()) {
		if ($post->post_parent) {
			$parent = end(get_post_ancestors($current_page_id));
		}
		else {
			$parent = $post->ID;
		}
	$post_data = get_post($parent, ARRAY_A);
	$classes[] = 'section-' . $post_data['post_name'];
	}
	return $classes;
}
/*
 *  replace post title link if external link exists
 	add custom field "url_title" or "title_url" or "url1" and enter external url in value field
 */
function print_post_title() {
	global $post;
		$thePostID = $post->ID;
		$post_id = get_post($thePostID);
		$title = $post_id->post_title;
		$perm = get_permalink($post_id);
		$post_keys = array(); $post_val = array();
		$post_keys = get_post_custom_keys($thePostID);
	if (!empty($post_keys)) {
	foreach ($post_keys as $pkey) {
	if ($pkey=='url1' || $pkey=='title_url' || $pkey=='url_title') {
		$post_val = get_post_custom_values($pkey);
		}
	}
	if (empty($post_val)) {
		$link = $perm;
	} else {
		$link = $post_val[0];
		}
	} else {
		$link = $perm;
	}
	echo '<h3 class="entry-title"><a href="'.$link.'" rel="bookmark"; title="'.$title.'";>'.$title.'</a></h3>';
}

function new_excerpt_more( $more ) {
	return ' [ <a class="read-more" href="'. get_permalink($post->ID) . '"><em>Read More</em></a> ]';
}
add_filter('excerpt_more', 'new_excerpt_more');

/* slg55
 * Taken from a post by Spitzerg http://wordpress.org/support/topic/blank-search-sends-you-to-the-homepage */
add_filter( 'request', 'my_request_filter' );
function my_request_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 350, 100, true ); // 350 pixels wide by 100 pixels tall, crop mode

/* Breadcrumbs for pages */

function dimox_breadcrumbs() {

	/* === OPTIONS === */
	$text['home']     = 'Home'; // text for the 'Home' link
	$text['category'] = 'Archive by Category "%s"'; // text for a category page
	$text['search']   = 'Search Results for "%s" Query'; // text for a search results page
	$text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
	$text['author']   = 'Articles Posted by %s'; // text for an author page
	$text['404']      = 'Error 404'; // text for the 404 page

	$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
	$showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$delimiter   = ' &raquo; '; // delimiter between crumbs
	$before      = '<span class="current">'; // tag before the current crumb
	$after       = '</span>'; // tag after the current crumb
	/* === END OF OPTIONS === */

	global $post;
	$homeLink = get_bloginfo('url') . '/';
	$linkBefore = '<span typeof="v:Breadcrumb">';
	$linkAfter = '</span>';
	$linkAttr = ' rel="v:url" property="v:title"';
	$link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;

	if (is_home() || is_front_page()) {

		if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $text['home'] . '</a></div>';

	} else {

		echo '<div id="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, $homeLink, $text['home']) . $delimiter;

		if ( is_category() ) {
			$thisCat = get_category(get_query_var('cat'), false);
			if ($thisCat->parent != 0) {
				$cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
				$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
				$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
				echo $cats;
			}
			echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

		} elseif ( is_search() ) {
			echo $before . sprintf($text['search'], get_search_query()) . $after;

		} elseif ( is_day() ) {
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
			echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
			echo $before . get_the_time('d') . $after;

		} elseif ( is_month() ) {
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
			echo $before . get_the_time('F') . $after;

		} elseif ( is_year() ) {
			echo $before . get_the_time('Y') . $after;

		} elseif ( is_single() && !is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
				if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, $delimiter);
				if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
				$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
				$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
				echo $cats;
				if ($showCurrent == 1) echo $before . get_the_title() . $after;
			}

		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			echo $before . $post_type->labels->singular_name . $after;

		} elseif ( is_attachment() ) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			$cats = get_category_parents($cat, TRUE, $delimiter);
			$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
			$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
			echo $cats;
			printf($link, get_permalink($parent), $parent->post_title);
			if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

		} elseif ( is_page() && !$post->post_parent ) {
			if ($showCurrent == 1) echo $before . get_the_title() . $after;

		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			for ($i = 0; $i < count($breadcrumbs); $i++) {
				echo $breadcrumbs[$i];
				if ($i != count($breadcrumbs)-1) echo $delimiter;
			}
			if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

		} elseif ( is_tag() ) {
			echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

		} elseif ( is_author() ) {
	 		global $author;
			$userdata = get_userdata($author);
			echo $before . sprintf($text['author'], $userdata->display_name) . $after;

		} elseif ( is_404() ) {
			echo $before . $text['404'] . $after;
		}

		if ( get_query_var('paged') ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
			echo __('Page') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}

		echo '</div>';

	}
} // end dimox_breadcrumbs()

/**
 * Plugin Name: Display Posts Shortcode
 * Plugin URI: http://www.billerickson.net/shortcode-to-display-posts/
 * Description: Display a listing of posts using the [display-posts] shortcode
 * Version: 2.3
 * Author: Bill Erickson
 * Author URI: http://www.billerickson.net
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume 
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @package Display Posts
 * @version 2.2
 * @author Bill Erickson <bill@billerickson.net>
 * @copyright Copyright (c) 2011, Bill Erickson
 * @link http://www.billerickson.net/shortcode-to-display-posts/
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
 
 
/**
 * To Customize, use the following filters:
 *
 * `display_posts_shortcode_args`
 * For customizing the $args passed to WP_Query
 *
 * `display_posts_shortcode_output`
 * For customizing the output of individual posts.
 * Example: https://gist.github.com/1175575#file_display_posts_shortcode_output.php
 *
 * `display_posts_shortcode_wrapper_open` 
 * display_posts_shortcode_wrapper_close`
 * For customizing the outer markup of the whole listing. By default it is a <ul> but
 * can be changed to <ol> or <div> using the 'wrapper' attribute, or by using this filter.
 * Example: https://gist.github.com/1270278
 */

add_shortcode("include_post", "cwd_include_post");
add_filter('widget_text', 'do_shortcode'); // add text widget support
 
// Create the shortcode
add_shortcode( 'display-posts', 'be_display_posts_shortcode' );
function be_display_posts_shortcode( $atts ) {

	// Original Attributes, for filters
	$original_atts = $atts;

	// Pull in shortcode attributes and set defaults
	$atts = shortcode_atts( array(
		'author'              => '',
		'category'            => '',
		'date_format'         => 'n/j/Y',
		'id'                  => false,
		'ignore_sticky_posts' => false,
		'image_size'          => false,
		'include_content'     => false,
		'include_date'        => false,
		'include_tags'        => false,
		'include_excerpt'     => false,
		'meta_key'            => '',
		'no_posts_message'    => '',
		'offset'              => 0,
		'order'               => 'DESC',
		'orderby'             => 'date',
		'post_parent'         => false,
		'post_status'         => 'publish',
		'post_type'           => 'post',
		'posts_per_page'      => '10',
		'tag'                 => '',
		'tax_operator'        => 'IN',
		'tax_term'            => false,
		'taxonomy'            => false,
		'wrapper'             => 'ul',
	), $atts );

	$author = sanitize_text_field( $atts['author'] );
	$category = sanitize_text_field( $atts['category'] );
	$date_format = sanitize_text_field( $atts['date_format'] );
	$id = $atts['id']; // Sanitized later as an array of integers
	$ignore_sticky_posts = (bool) $atts['ignore_sticky_posts'];
	$image_size = sanitize_key( $atts['image_size'] );
	$include_content = (bool)$atts['include_content'];
	$include_date = (bool)$atts['include_date'];
	$include_tags = (bool)$atts['include_tags'];
	$include_excerpt = (bool)$atts['include_excerpt'];
	$meta_key = sanitize_text_field( $atts['meta_key'] );
	$no_posts_message = sanitize_text_field( $atts['no_posts_message'] );
	$offset = intval( $atts['offset'] );
	$order = sanitize_key( $atts['order'] );
	$orderby = sanitize_key( $atts['orderby'] );
	$post_parent = $atts['post_parent']; // Validated later, after check for 'current'
	$post_status = $atts['post_status']; // Validated later as one of a few values
	$post_type = sanitize_text_field( $atts['post_type'] );
	$posts_per_page = intval( $atts['posts_per_page'] );
	$tag = sanitize_text_field( $atts['tag'] );
	$tax_operator = $atts['tax_operator']; // Validated later as one of a few values
	$tax_term = sanitize_text_field( $atts['tax_term'] );
	$taxonomy = sanitize_key( $atts['taxonomy'] );
	$wrapper = sanitize_text_field( $atts['wrapper'] );

	
	// Set up initial query for post
	$args = array(
		'category_name'       => $category,
		'order'               => $order,
		'orderby'             => $orderby,
		'post_type'           => explode( ',', $post_type ),
		'posts_per_page'      => $posts_per_page,
		'tag'                 => $tag,
	);
	
	// Ignore Sticky Posts
	if( $ignore_sticky_posts )
		$args['ignore_sticky_posts'] = true;
	
	// Meta key (for ordering)
	if( !empty( $meta_key ) )
		$args['meta_key'] = $meta_key;
	
	// If Post IDs
	if( $id ) {
		$posts_in = array_map( 'intval', explode( ',', $id ) );
		$args['post__in'] = $posts_in;
	}
	
	// Post Author
	if( !empty( $author ) )
		$args['author_name'] = $author;
		
	// Offset
	if( !empty( $offset ) )
		$args['offset'] = $offset;
	
	// Post Status	
	$post_status = explode( ', ', $post_status );		
	$validated = array();
	$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
	foreach ( $post_status as $unvalidated )
		if ( in_array( $unvalidated, $available ) )
			$validated[] = $unvalidated;
	if( !empty( $validated ) )		
		$args['post_status'] = $validated;
	
	
	// If taxonomy attributes, create a taxonomy query
	if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {
	
		// Term string to array
		$tax_term = explode( ', ', $tax_term );
		
		// Validate operator
		if( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) )
			$tax_operator = 'IN';
					
		$tax_args = array(
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'slug',
					'terms'    => $tax_term,
					'operator' => $tax_operator
				)
			)
		);
		
		// Check for multiple taxonomy queries
		$count = 2;
		$more_tax_queries = false;
		while( 
			isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) && 
			isset( $original_atts['tax_' . $count . '_term'] ) && !empty( $original_atts['tax_' . $count . '_term'] ) 
		):
		
			// Sanitize values
			$more_tax_queries = true;
			$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
	 		$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
	 		$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts['tax_' . $count . '_operator'] : 'IN';
	 		$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
	 		
	 		$tax_args['tax_query'][] = array(
	 			'taxonomy' => $taxonomy,
	 			'field' => 'slug',
	 			'terms' => $terms,
	 			'operator' => $tax_operator
	 		);
	
			$count++;
			
		endwhile;
		
		if( $more_tax_queries ):
			$tax_relation = 'AND';
			if( isset( $original_atts['tax_relation'] ) && in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) ) )
				$tax_relation = $original_atts['tax_relation'];
			$args['tax_query']['relation'] = $tax_relation;
		endif;
		
		$args = array_merge( $args, $tax_args );
	}
	
	// If post parent attribute, set up parent
	if( $post_parent ) {
		if( 'current' == $post_parent ) {
			global $post;
			$post_parent = $post->ID;
		}
		$args['post_parent'] = intval( $post_parent );
	}
	
	// Set up html elements used to wrap the posts. 
	// Default is ul/li, but can also be ol/li and div/div
	$wrapper_options = array( 'ul', 'ol', 'div' );
	if( ! in_array( $wrapper, $wrapper_options ) )
		$wrapper = 'ul';
	$inner_wrapper = 'div' == $wrapper ? 'div' : 'li';

	
	$listing = new WP_Query( apply_filters( 'display_posts_shortcode_args', $args, $original_atts ) );
	if ( ! $listing->have_posts() )
		return apply_filters( 'display_posts_shortcode_no_results', wpautop( $no_posts_message ) );
		
	$inner = '';
	while ( $listing->have_posts() ): $listing->the_post(); global $post;
		
		$image = $date = $excerpt = $content = '';
		
		$title = '<a class="post-title" href="' . apply_filters( 'the_permalink', get_permalink() ) . '">' . apply_filters( 'the_title', get_the_title() ) . '</a>';
		
		if ( $image_size && has_post_thumbnail() )  
			$image = '<a class="post-image" href="' . get_permalink() . '">' . get_the_post_thumbnail( $post->ID, $image_size ) . '</a> ';
			
		if ( $include_date ) 
			$date = ' <span class="post-item-date">' . get_the_date( $date_format ) . '</span>';
		
		if( $include_tags )
			$taglist = '<div class="post-item-tags">' . get_the_tag_list('',' ','') . '</div>';
		
		if ( $include_excerpt ) 
			$excerpt = ' <span class="post-excerpt">' . get_the_excerpt() . '</span>';
			
		if( $include_content )
			$content = '<div class="post-item-content">' . apply_filters( 'the_content', get_the_content() ) . '</div>'; 
		
		
		$class = array( 'listing-item' );
		$class = apply_filters( 'display_posts_shortcode_post_class', $class, $post, $listing );
		$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $image . $title . $taglist . $date . $excerpt . $content . '</' . $inner_wrapper . '>';
		
		// If post is set to private, only show to logged in users
		if( 'private' == get_post_status( $post->ID ) && !current_user_can( 'read_private_posts' ) )
			$output = '';
		
		$inner .= apply_filters( 'display_posts_shortcode_output', $output, $original_atts, $image, $title, $date, $excerpt, $inner_wrapper, $content, $taglist, $class );
		
	endwhile; wp_reset_postdata();
	
	$open = apply_filters( 'display_posts_shortcode_wrapper_open', '<' . $wrapper . ' class="display-posts-listing iws-expander">', $original_atts );
	$close = apply_filters( 'display_posts_shortcode_wrapper_close', '</' . $wrapper . '>', $original_atts );
	$return = $open . $inner . $close;

	return $return;
}

function acroName($name) {
	$nameAry = explode($name, ' ');
	
	foreach($nameAry as $n) {
		$toReturn .= $n[0];
	}
	return strtoupper($toReturn);
}

// Addiing custom plugins
// require_once( 'plugins/bannerspace/bannerspace.php' );

/*add_action('after_setup_theme', 'my_load_plugin');
 
// This function loads the plugin.
function my_load_plugin() {
 
// Check to see if your plugin has already been loaded. This can be done in several
// ways - here are a few examples:
//
// Check for a class:
//	if (!class_exists('MyPluginClass')) {
//
// Check for a function:
//	if (!function_exists('my_plugin_function_name')) {
//
// Check for a constant:
//	if (!defined('MY_PLUGIN_CONSTANT')) {
 
	if (!class_exists('bannerspace_plugin_options')) {
 
		// load Social if not already loaded
		include_once('plugins/bannerspace/bannerspace.php');
 
	}
} */
// See more at: http://alexking.org/blog/2012/07/09/include-plugin-in-wordpress-theme#sthash.BkBXFrLm.dpuf

/******************************************/
// Add Note format option to TinyMCE editor
/******************************************/

/** 
 * Add Styles to editor for display on edit screens.
 */ 

add_action( 'after_setup_theme', 'cornell_edit_css' );
function cornell_edit_css()
{
	$stylesheet = 'lib/editor.css';
    add_editor_style($stylesheet);
}

/** 
 * Add "Styles" drop-down 
 */    
function cornell_enable_styles($buttons) {  
    array_unshift($buttons, 'styleselect');  
    return $buttons;  
}  
  
add_filter('mce_buttons_2', 'cornell_enable_styles');  
  
/** 
 * Add "Styles" drop-down content or classes 
 */    
	
function tuts_mcekit_editor_settings($settings) {  
    if (!empty($settings['theme_advanced_styles']))  
        $settings['theme_advanced_styles'] .= ';';      
    else  
        $settings['theme_advanced_styles'] = '';  
  
    /** 
     * Add styles in $classes array. 
     * The format for this setting is "Name to display=class-name;". 
     * More info: http://wiki.moxiecode.com/index.php/TinyMCE:Configuration/theme_advanced_styles 
     * 
     * To be allow translation of the class names, these can be set in a PHP array (to keep them 
     * readable) and then converted to TinyMCE's format. You will need to replace 'textdomain' with 
     * your theme's textdomain. 
     */  
    $classes = array(  
        __('None','textdomain') => '',
	    __('Note','textdomain') => 'cornell-note',
    );  
  
    $class_settings = '';  
    foreach ( $classes as $name => $value )  
        $class_settings .= "{$name}={$value};";  
  
    $settings['theme_advanced_styles'] .= trim($class_settings, '; ');  
    return $settings;  
}   
  
add_filter('tiny_mce_before_init', 'tuts_mcekit_editor_settings');

require_once( 'lib/slideshow.php' );

// Alter body classes
function Cornell_mri_body_classes() {
	
	$new_class = array();
	
	// var_dump($_SERVER['HTTP_USER_AGENT']); 
	 $browser = $_SERVER['HTTP_USER_AGENT'];
	 	
		// var_dump($browser);
	switch($browser) {
		case (preg_match('/mobile/i', $browser) ? true : false) :
			$new_class[] = 'mobile';
		break;
		
		case (preg_match('/windows/i', $browser) || preg_match('/macintosh/i', $browser) ? true : false) :
			$new_class[] = 'desktop';
			
			if(preg_match('/windows/i', $browser)) { $new_class[] = 'win'; }
			if(preg_match('/macintosh/i', $browser)) { $new_class[] = 'mac'; }
			
			switch($browser) {
				
				case (preg_match('/firefox/i', $browser) ? true : false) :
					$new_class[] = 'firefox';
				break;
				
				case (preg_match('/safari/i', $browser) || preg_match('/chrome/i', $browser) ? true : false) :
					$new_class[] = 'webkit';
					
					if(preg_match('/chrome/i', $browser)) { $new_class[] = 'chrome'; }else{ $new_class[] = 'safari'; }
				break;
				
				case (preg_match('/msie/i', $browser) ? true : false) :
					$new_class[] = 'ie';
					
					// Add IE version to body class
					if(preg_match('/MSIE 8.0/i', $browser)) { $new_class[] = 'eight'; }
					if(preg_match('/MSIE 9.0/i', $browser)) { $new_class[] = 'nine'; }
					if(preg_match('/MSIE 10.0/i', $browser)) { $new_class[] = 'ten'; }
				break;
				
			}
			
		break;
		
	}
	
		$return = implode(' ', $new_class);
	
  return $return;
}

// Add options to customize screen
function Cornell_customize_register( $wp_customize )
{
	// Add Cornell Section to the Customize screen
	$wp_customize->add_section('cornell_options',
									array(
   										'title' => __('Cornell Slider Options', 'Cornell'),
						   		   		'priority' => 1,
										'description' => 'Theme Options Cornell MRI Theme'
								   )
	);
	
	// Add Settings to Section
	$wp_customize->add_setting( 'slider_speed' , array(
    	'default' => 9,

	) );
	
	$wp_customize->add_control( 
					new WP_Customize_Control( $wp_customize, 'slider_pause', 
									array(
											'type' => 'text',
											'label'		=> __( 'Slider Speed (in seconds)', 'Cornell' ),
											'section'	=> 'cornell_options',
											'settings'  => 'slider_speed',
									)
					) 
	);
	
	$wp_customize->add_setting( 'slider_pause' , array(
    	'default' => 10,

	) );
	
	$wp_customize->add_control( 
					new WP_Customize_Control( $wp_customize, 'slider_speed', 
									array(
											'type' => 'text',
											'label'		=> __( 'Time (in seconds) of pause when user navigates to a slide', 'Cornell' ),
											'section'	=> 'cornell_options',
											'settings'  => 'slider_pause',
									)
					) 
	);

}
add_action( 'customize_register', 'Cornell_customize_register' );
?>