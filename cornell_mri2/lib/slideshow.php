<?php
/*
 *
 * Theme Slideshow
 * Creates custom slide content type and builds slideshow in theme layer.
 *
 */
 
 class cornell_slideshow 
 {
 	// Register custom post type
	function register_slides() {
		$labels = array(
    		'name' => 'Slides',
		    'singular_name' => 'Slide',
    		'add_new' => 'Add New',
	    	'add_new_item' => 'Add New Slide',
	    	'edit_item' => 'Edit Slide',
		    'new_item' => 'New Slide',
    		'all_items' => 'All Slides',
		    'view_item' => 'View Slide',
    		'search_items' => 'Search Slides',
		    'not_found' =>  'No slides found',
    		'not_found_in_trash' => 'No slides found in Trash', 
		    'parent_item_colon' => '',
    		'menu_name' => 'Slides'
   		);

		$args = array(
    		'labels' => $labels,
	    	'public' => true,
	    	'publicly_queryable' => true,
		    'show_ui' => true, 
    		'show_in_menu' => true, 
		    'query_var' => true,
    		'rewrite' => array( 'slug' => 'slide' ),
	    	'capability_type' => 'post',
		    'has_archive' => false, 
    		'hierarchical' => false,
	    	'menu_position' => 10,
	    	'supports' => array( 'title', 'editor', 'author', 'thumbnail')
		); 
   
	    register_post_type( 'slide', $args );
	
	 } // End register_slides function
 
	 function build_slides() {
		 /*
		  * This function builds the slide array to pass to the display function (cornell_theme_slider)
		  */
	 
		 // Set up vars to build output array
		 global $wpdb;
	 
		 // Set up output array. Keys: title, body, image, link, order, slide bg and classes
		 $slideOut = array();
	 
		 // Retrieve slide posts from DB
		 $slides = $wpdb->get_results(
	 							"select * from " .  $wpdb->posts .
								" where post_type = 'slide' and post_status = 'publish'", ARRAY_A);
	 
		foreach ( $slides as $s ) {
		
			$slideBld = array();
			$order = get_post_meta( $s['ID'], "cornell_slide_order", true);
				// Get orientation and set classes to style li
				$image_orientation = get_post_meta( $s['ID'], "cornell_slide_image_orientation", true);
				$text_align = get_post_meta( $s['ID'], "cornell_slide_text_alignment", true);
				$vert_text_align = get_post_meta( $s['ID'], "cornell_slide_vert_text_alignment", true);
		
			$slideBld['title'] = $s['post_title'];
			$slideBld['body'] = $s['post_content'];
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $s['ID'] ), "Full");
			$slideBld['image'] = $image[0];
			$slideBld['link'] = get_post_meta( $s['ID'], "cornell_slide_link", true);
		    $slideBld['order'] = get_post_meta( $s['ID'], "cornell_slide_order", true);
			// $slideBld['slide_bg'] = get_post_meta( $s['ID'], "slide_bg", true);
			$slideBld['classes'] = self::build_slideshow_classes($image_orientation, $text_align, $vert_text_align);
			
			do {
				$order++;
			} while ($slideOut[$order]);
			
			$slideOut[$order] = $slideBld;
		
			unset($slideBld);
		} // End slide foreach
			sort($slideOut);
			
function sortByOrder($a, $b) {
//Sort array by order value
    return $a['order'] - $b['order'];
}

usort($slideOut, 'sortByOrder');
						
			
		return $slideOut;
	 
	} // End build_slides function
	
	private function build_slideshow_classes($img, $text, $vert) {
		
		$classesOut = array();
		
		switch(strtolower($img)) {
			case 'background' :
				$classesOut['img'] = 'img-bg';
			break;
			case 'right' :
				$classesOut['img'] = 'img-right';
			break;
			case 'left':
			default:
				$classesOut['img'] = 'img-left';
			break;
		}
		
		switch(strtolower($text)) {
			case 'center' :
				$classesOut['txt'] = 'txt-center';
			break;
			case 'right' :
				$classesOut['txt'] = 'txt-right';
			break;
			case 'left':
			default:
				$classesOut['txt'] = 'txt-left';
			break;
		}
		
		switch(strtolower($vert)) {
			case 'middle' :
				$classesOut['vert'] = 'align-middle';
			break;
			case 'bottom' :
				$classesOut['vert'] = 'align-bottom';
			break;
			case 'top':
			default:
				$classesOut['vert'] = 'align-top';
			break;
		}
		
		return implode(' ', $classesOut);
	}
	
	function cornell_slide_fields_box() {
		add_meta_box( 'cornell-slide-opts', 'Slide Options', array( 'cornell_slideshow', 'cornell_slide_opts'), 'slide', 'side', 'low' );
		
		//  add_meta_box( $id, $title, $callback, $post_type, $context, $priority, $callback_args );
		
	} // End cornell_slide_fields function
	
	function cornell_slide_opts() {
		global $post;
		
		$link = get_post_meta($post->ID, 'cornell_slide_link', true);
		$order = get_post_meta($post->ID, 'cornell_slide_order', true);
		$image = get_post_meta($post->ID, 'cornell_slide_image_orientation', true);	
		$text_align = get_post_meta( $post->ID, "cornell_slide_text_alignment", true);
		$vert_text_align = get_post_meta( $post->ID, "cornell_slide_vert_text_alignment", true);
		
			// Set up option groups for the fields
			$orderOpts = range(0,99); // Range of order options
			$imageOpts = array('left', 'right', 'background');
			$textOpts = array('left', 'right', 'center');
			$vertOpts = array('top', 'bottom');
		
		echo '<p><label>Link: </label>
				<input name="cornell_slide_link" value="' . $link . '"  /></p>
			 <p><label>Order: </label>
			 	<select name="cornell_slide_order">'; // Begin order select
			
			foreach($orderOpts as $or) {
				echo '<option value="' . $or . '"';
					if($or == $order) { echo ' selected'; }
				echo '>' . $or . '</option>';
			}
			
		echo '</select></p>' . // End order select
			  '<p><label>Image Orientation: </label>
			  	<select name="cornell_slide_image_orientation">'; // Begin image orientation select
			
			foreach($imageOpts as $im) {
				echo '<option value="' . strtolower($im) . '"';
					if(strtolower($im) == strtolower($image)) { echo ' selected'; }
				echo '>' . ucfirst($im) . '</option>';
			}	
				
		echo '</select></p>'. // End image orientation select
			 '<p><label>Text Align: </label>
			  	<select name="cornell_slide_text_alignment">'; // Begin text align select
			
			foreach($textOpts as $tx) {
				echo '<option value="' . strtolower($tx) . '"';
					if(strtolower($tx) == strtolower($text_align)) { echo ' selected'; }
				echo '>' . ucfirst($tx) . '</option>';
			}	
				
		echo '</select></p>'. // End text align select
			 '<p><label>Vertical Align: </label>
			  	<select name="cornell_slide_vert_text_alignment">'; // Begin vertical align select
			
			foreach($vertOpts as $vt) {
				echo '<option value="' . strtolower($vt) . '"';
					if(strtolower($vt) == strtolower($vert_text_align)) { echo ' selected'; }
				echo '>' . ucfirst($vt) . '</option>';
			}	
				
		echo '</select></p>'; // End vertical align select
				
		
	} // End cornell slide opts.
	
	function cornell_slide_save($post_id, $post) {
		
		// If user can't edit the changes will not be saved.
		if ( !current_user_can( 'edit_post', $post_id->ID )) return $post->ID;
		
		if ( !wp_is_post_revision( $post_id ) ) {
			// Build slide meta array to pass to the save/update loop.
			$fieldsToDb = array();
				$fieldsToDb['cornell_slide_link'] = $_POST['cornell_slide_link']; // $fieldsToDb['cornell_slide_link'] = $_POST['cornell_slide_link'];
				$fieldsToDb['cornell_slide_order'] = $_POST['cornell_slide_order'];
				$fieldsToDb['cornell_slide_image_orientation'] = $_POST['cornell_slide_image_orientation'];
				$fieldsToDb['cornell_slide_text_alignment'] = $_POST['cornell_slide_text_alignment'];
				$fieldsToDb['cornell_slide_vert_text_alignment'] = $_POST['cornell_slide_vert_text_alignment'];
			
			foreach($fieldsToDb as $k => $v) {
			
				 if(!$v) { delete_post_meta($post_id, $k); continue; } // Delete value if the field has been cleared and move to next itme in array.
				if(get_post_meta($post->ID, $k, false)) { // If the custom field already has a value and that value has changed.
		            update_post_meta($post->ID, $k, $v);
	    	    } else { // If the custom field doesn't have a value
	        	    add_post_meta($post->ID, $k, $v);
		        }
	       
			
			} // End save loop
		} // End revision check
		
	} // End cornell_slide_save function
 
 } // End cornell_slideshow class
 
  // Register post type with init hook
  add_action( 'init', array('cornell_slideshow', 'register_slides') );
  
  // Adds meta boxes to custom content type with add_meta_box hook
  add_action( 'add_meta_boxes', array('cornell_slideshow', 'cornell_slide_fields_box') );
  
  // Tie in save function for slide content type.
  add_action('save_post', array('cornell_slideshow','cornell_slide_save'), 1, 2);
  
  // Function registers styles and JS for slideshow
  function cornell_slideshow_scripts() {
	   wp_register_script( 'cornell-slideshow-script', get_template_directory_uri() . '/lib/slideshow.js', array(), '1.0', true );
	   // wp_register_style( 'cornell-slideshow-style', get_template_directory_uri() . '/lib/slideshow.css', array(), '1.0', 'all');
       wp_enqueue_script( 'cornell-slideshow-script' );
	   // wp_enqueue_style( 'cornell-slideshow-style' );
	  
  } // End cornell_slideshow_scripts
  
  // Tie script function to the wp_enqueue_scripts action
  add_action( 'wp_enqueue_scripts', 'cornell_slideshow_scripts' );
  
  // Function instantiates slideshow class and produces HTML to output to theme.
  function cornell_theme_slider() {
	 	
		$slideshow = new cornell_slideshow;
		$slides = $slideshow->build_slides();
			
			$slideHTML = '<div id="cornell-slideshow-container">
							<div id="cornell_prev_slide" class="cornell_slide_arrows"><a href="#" onclick="cornellPrevNext(\'prev\'); return false;"></a></div>
							<div id="cornell_next_slide" class="cornell_slide_arrows"><a href="#" onclick="cornellPrevNext(\'next\'); return false;"></a></div>
							<ul class="cornell-theme-slider">';
				
				// Set beginning slide number.
				$slideNum = 0;
				
				// Begin menu html
				$menuHTML = '<ul class="cornell-slideshow-menu">';
				
			foreach( $slides as $s) {
				
				if($slideNum == 0) { $s['classes'] .= ' active'; }
				
				// Check for slide background color.  Set it if it's available
				if($s['slide_bg']) { $bg = 'style="background: ' . $s['slide_bg'] . '"'; }else{ $bg = ''; }
				$slideBld = '<li id="slide' . $slideNum . '" class="cornell-theme-slider-slide ' . $s['classes'] . '" ' . $bg . '>';
					// Add link to $slideBld if it exists, if not set it to #
					if($s['link']) { $slideBld .= '<a href="'. $s['link'] . '">'; }else{ $slideBld .= '<a href="#" onClick="return false;">'; }
					
						$slideBld .= '<div class="cornell-slide-img"><img src="' . $s['image'] . '" alt="' . $s['title'] . '" title="' . $s['title'] . '"></div>';
						$slideBld .= '<div class="cornell-slide-content">
										<div class="cornell-content-wrapper">
										<h3>' . $s['title'] . '</h3>';
						$slideBld .= '<p>' . $s['body'] . '</p></div>
									</div>'; // Closes content div
					
					$slideBld .= '</a>'; // Close link
					
					
				$slideBld .= '</li>'; //
				$slideHTML .= $slideBld;
				$menuHTML .= '<a href="#" onClick="showSlide(\'slide' . $slideNum . '\', this); return false;" id="slide' . $slideNum . 'Link"';
						if($slideNum == 0) { $menuHTML .= ' class="activeLink"'; }
				$menuHTML .= '><li></li></a>';	
					// Increment slide count
					$slideNum++;
					
			}
			
			$menuHTML .= '</ul>'; // Close menu list.
			
			
			$slideHTML .= '</ul>' . $menuHTML . '</div>'; // Close UL, add menu and close container div
		
		return $slideHTML;
	 
 	} // End cornell_theme_slider function
	
?>