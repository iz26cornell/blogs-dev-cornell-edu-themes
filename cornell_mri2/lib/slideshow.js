/* Javascript file to run Cornell EDU Blog Slideshow */

// window.slideSpeed and window.slidePause are set in header.php.

var $slideshow = window.setInterval( "slideSwitch()", window.slideSpeed );
var $timeOut = null;
	
	function slideSwitch() {
   		var $active = $('#cornell-slideshow-container > ul.cornell-theme-slider > li.cornell-theme-slider-slide:visible');
			if ( $active.length == 0 ) $active = $('#cornell-slideshow-container > ul.cornell-theme-slider > LI:last');
			if(!$($active).hasClass('active')) {	$($active).addClass('active'); 	}

		var $actId = $($active).attr('id');
		var $activeLink = $('#cornell-slideshow-container > ul.cornell-slideshow-menu #' + $actId + 'Link');
			if(!$($activeLink).hasClass('activeLink')) {	$($activeLink).addClass('activeLink'); 	}
   		if ( $active.length == 0 ) $active = $('#cornell-slideshow-container > ul.cornell-theme-slider > LI:last');

    	var $next = $($active).next('li').length ? $($active).next('li') : $('#cornell-slideshow-container ul LI:first');
		
		$($active).addClass('last-active');
			
			var $nextLink = '#' + $($next).attr('id') + 'Link';
			
			$($activeLink).animate({ opacity: 0 }, { duration: 750, queue: false, easing: 'swing', 
				complete: function() {
							$(this).removeClass('activeLink');
							$(this).animate({opacity: 1}, { duration: 750, queue: false, 
								complete: function() {		
											$($nextLink).delay(4500).animate({opacity:0},{ duration: 750, queue: false, easing: 'swing', 
												complete: function() {
															$(this).addClass('activeLink');
															$(this).animate({opacity: 1},{ duration: 750, queue: false})
												}});
								}});
				}});
			
			
			$($next).fadeIn({ duration: 3500, queue: false, easing: 'swing', 
				complete: function() { 
										$(this).addClass('active'); 
										
						  }});
			
			$($active).fadeOut({ duration: 3500, queue: false, easing: 'swing', 
				complete: function() { 
										$(this).removeClass('active last-active'); 
										// $($nextLink).delay(5500).addClass('activeLink');
						  }});
			
			// Switch link 
			
			
						

	} // End Slide Switch Function
	
	function showSlide($id, $t) {
		
		if($slideshow) { window.clearInterval($slideshow); }
		
		if($timeOut != null) {
			
			window.clearTimeout($timeOut); 
			$timeOut = window.setTimeout(function(){ $slideshow = window.setInterval( "slideSwitch()", window.slideSpeed ); $timeOut = null; }, window.slidePause);
			
		}else{
			 $timeOut = window.setTimeout(function(){ $slideshow = window.setInterval( "slideSwitch()", window.slideSpeed ); $timeOut = null; }, window.slidePause);
			
		}
		
		var $slides = $('#cornell-slideshow-container > ul.cornell-theme-slider > li.cornell-theme-slider-slide');
		var $slideLinks = $('#cornell-slideshow-container > ul.cornell-slideshow-menu > a');
		
		for(var $sl = 0; $sl < $slides.length; $sl++) {
			
			var $showLink = '#' + $id + 'Link';
			
			if($($slides[$sl]).attr("id") != $id) {
				// Stop, clear queue and set opacity to 1 for all other slides.
				$($slides[$sl]).stop();
				$($slideLinks[$sl]).stop();
				$($slides[$sl]).clearQueue();
				$($slideLinks[$sl]).clearQueue();
				$($slides[$sl]).css('opacity', 1);
				$($slideLinks[$sl]).css('opacity', 1);
					// Remove active classes from previous slide.
					if($($slides[$sl]).hasClass('active')) { $($slides[$sl]).removeClass('active'); }
					if($($slides[$sl]).hasClass('last-active')) { $($slides[$sl]).removeClass('last-active'); }
					if($($slideLinks[$sl]).hasClass('activeLink')) { $($slideLinks[$sl]).removeClass('activeLink'); }
				
				$($slides[$sl]).css('display', 'none');
			}else{
				// Show the slide that corresponds to the link clicked.
				$('#' + $id).css({display: 'block', opacity: 1}).addClass('active');
				$('#' + $t.id).addClass('activeLink');	
			}
		}
		
	}  // End Show Slide function
	
function cornellPrevNext($d) {
	
	if($slideshow) { window.clearInterval($slideshow); }
		
	if($timeOut != null) {
			window.clearTimeout($timeOut); 
			$timeOut = window.setTimeout(function(){ $slideshow = window.setInterval( "slideSwitch()", window.slideSpeed ); $timeOut = null; }, window.slidePause);
	}else{
			 $timeOut = window.setTimeout(function(){ $slideshow = window.setInterval( "slideSwitch()", window.slideSpeed ); $timeOut = null; }, window.slidePause);
	}
			
	var $active = $('#cornell-slideshow-container > ul.cornell-theme-slider > li.cornell-theme-slider-slide:visible');
	var $actId = $($active).attr('id');
	var $activeLink = $('#cornell-slideshow-container > ul.cornell-slideshow-menu #' + $actId + 'Link');
	var $next = $($active).next('li').length ? $($active).next() : $('#cornell-slideshow-container ul LI:first');
	var $slides = $('#cornell-slideshow-container > ul.cornell-theme-slider > li.cornell-theme-slider-slide');
	var $slideLinks = $('#cornell-slideshow-container > ul.cornell-slideshow-menu > a');
	
		// Set prev var based on slides length
		if($($active).prev('li').length > 0) {
			var $prev = $($active).prev('li');
		}else{
			var $prev = $('#cornell-slideshow-container > ul.cornell-theme-slider > LI:last');
		}
		
		// Set which slide to show based on input
		if($d == 'next') { $id = $($next).attr('id'); }else{ $id = $($prev).attr('id'); }				
						
		for(var $sl = 0; $sl < $slides.length; $sl++) {
	
			var $showLink = '#' + $id + 'Link';
			
			if($($slides[$sl]).attr("id") != $id) {
				// Stop, clear queue and set opacity to 1 for all other slides.
				$($slides[$sl]).stop();
				$($slideLinks[$sl]).stop();
				$($slides[$sl]).clearQueue();
				$($slideLinks[$sl]).clearQueue();
				$($slides[$sl]).css({opacity: 1, display: 'none'});
				$($slideLinks[$sl]).css('opacity', 1);
					// Remove active classes from previous slide.
					if($($slides[$sl]).hasClass('active')) { $($slides[$sl]).removeClass('active'); }
					if($($slides[$sl]).hasClass('last-active')) { $($slides[$sl]).removeClass('last-active'); }
					if($($slideLinks[$sl]).hasClass('activeLink')) { $($slideLinks[$sl]).removeClass('activeLink'); }
		
			}else{
				// Show the slide that corresponds to the link clicked.
				$('#' + $id).css({display: 'block', opacity: 1}).addClass('active');
				$('#' + $id + 'Link').addClass('activeLink');	
			}
		}		
} // End cornellPrevNext function