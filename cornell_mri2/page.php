<?php get_header(); ?>

<div id="wrap" class="twocolumn-left">
	<div id="main-navigation">
		<?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
		<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
	</div>
	<!-- main navigation --> 
	<div id="content-wrap" class="twocolumn-left">
		<div id="header" class="twocolumn-left">
			<div class="header-meta">
				<h1><a href="<?php echo get_settings('home'); ?>/"><strong id="headerPageTitle"><?php bloginfo('name'); ?></strong></a></h1>
				<h2><?php bloginfo( 'description' ); ?></h2>
			</div>
			<img class="campaign-image" src="<?php header_image(); ?>" alt="" />
		</div>
	</div>
	<div id="content">
		<div id="main" class="twocolumn-left">
			<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
            <?php $feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
            	
			
			<h2 class="page-title"><?php the_title(); ?></h2>
            
            <?php if($feat_image) { ?>
            	<div class="featuredImg"><img src="<?php echo $feat_image; ?>"  /></div>
            <?php } ?>
            
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				<?php the_content(__('Read more'));?>
				<!--
				<?php trackback_rdf(); ?>
				-->
			<?php endwhile; else: ?>
				<p>
					<?php _e('Sorry, no posts matched your criteria.'); ?>
				</p>
			<?php endif; ?>
			<?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
			<?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
		<div id="secondary">
			<!-- Start Pages Navigation -->
			<div id="pages" class="widget_pages">
            
            <?php 
			
				
					$listId = $post->ID;
					$title = $post->post_title;
					$link = '#';

					// Check to see if page has direct children.  If so emit a list of those children
					$children = get_pages('child_of='.$listId.'&parent='.$listId);
					
					// print '<h1>ID:' . $listId . '</h1>';
					if(count($children) != 0) {
					?>
				<h3>In this section:</h3>
                	
				<ul>
					<li class="page_item page-item-55 current_section_item">
						<a href="<?php print $link; ?>" <?php if($listId == $post->ID) { ?> onclick="return false;" <?php } ?>><?php echo $title; ?></a>
						<ul class="children">
							<?php wp_list_pages('title_li=&depth=1&child_of='.$listId.'&link_before=<span>&link_after=</span>'); ?>
						</ul>
					</li>
				</ul>
                
                <?php }elseif($post->post_parent != 0) { 
							$parentChild = get_pages('child_of='.$post->post_parent .'&parent='.$post->post_parent);
							
							if(count($parentChild) != 0) {
				?>	
                	<h3>In this section:</h3>
                		<ul>
							<li class="page_item page-item-55 current_section_item">
								<a href="<?php echo get_permalink($post->post_parent); ?>"><?php echo get_the_title($post->post_parent); ?></a>
								<ul class="children">
									<?php wp_list_pages('title_li=&depth=1&child_of='.$post->post_parent.'&link_before=<span>&link_after=</span>'); ?>
						</ul>
					</li>
				</ul>
                	<?php } ?>
                
                <?php } // child count check. ?>
			</div>
			<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
				<div id="sidebar-content">
					<?php // dynamic_sidebar( 'sidebar-1' ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
   