<?PHP
if (isset($_GET['btnG'])) {
	session_start();
	$selected_radio = $_GET['sitesearch'];
	
	if ($selected_radio == 'cornell') {
		$search_terms = urlencode($_GET['s']);
		$URL="http://www.cornell.edu/search" . "?q=" . $search_terms . "&client=default_frontend&proxystylesheet=default_frontend";
		print $URL;
		header ("Location: $URL");
	}
}
?>
<?php get_header(); ?>

      <div id="main">
        <div class="page-meta">
          <h2 class="page-title"><?php printf( __( 'Search Results for: "%s"' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
          <?php if (have_posts() && $_GET['s'] != "") : while (have_posts()) : the_post(); ?>
          <h3><a href="<?php the_permalink() ?>" rel="bookmark">
            <?php the_title(); ?>
            </a></h3>
          <?php the_excerpt(__('Read more'));?>
          <!--
	<?php trackback_rdf(); ?>
	-->
          <?php endwhile; else: ?>
          <h3 class="entry-title">
            <?php _e( 'Nothing Found' ); ?>
          </h3>
          <p>
            <?php _e('Sorry, no posts matched your criteria.'); ?>
          </p>
          <?php get_search_form(); ?>
          <?php endif; ?>
          <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
        </div>
      </div>
      <div id="secondary">
        <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
