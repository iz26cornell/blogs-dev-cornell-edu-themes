<?php
/**
 * @package WordPress
 * @subpackage Cornell
 */

get_header(); ?>

      <div id="main">
        <div class="page-meta">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <h2 class="page-title">
            <?php the_title(); ?>
          </h2>
          <?php the_content(__('Read more'));?>
          <!--
	<?php trackback_rdf(); ?>
	-->
          <?php endwhile; else: ?>
          <p>
            <?php _e('Sorry, no posts matched your criteria.'); ?>
          </p>
          <?php endif; ?>
          <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
          <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
        </div>
      </div>
      <div id="secondary">
        <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
