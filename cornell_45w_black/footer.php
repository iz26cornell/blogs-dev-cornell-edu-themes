<!-- begin footer -->
<hr />
<div id="footer">
  <!-- The footer-content div contains the Cornell University copyright -->
  <div id="footer-content">
        <p><span>&copy;<?php echo date("Y");?> <a href="http://www.cornell.edu/">Cornell University</a></span> Powered by <a href="http://edublogs.org/campus">Edublogs Campus</a> and running on <a href="http://blogs.cornell.edu">blogs.cornell.edu</a></p>
  </div>
</div>
<?php do_action('wp_footer'); ?>
</body></html>