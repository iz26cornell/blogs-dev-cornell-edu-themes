<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="distribution" content="global" />
<meta name="robots" content="follow, all" />
<meta name="language" content="en, sv" />
<title>
<?php wp_title(''); ?>
<?php if(wp_title('', false)) { echo ' :'; } ?>
 <?php bloginfo('name'); ?>
</title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<!-- leave this for stats please -->
<link rel="Shortcut Icon" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_get_archives('type=monthly&format=link'); ?>
<?php wp_head(); ?>

<?php if (function_exists('mobile_screen') ) { mobile_screen(); } ?>
</head>
<body <?php body_class(); ?>>
<?php include (TEMPLATEPATH . '/banner.php'); ?>
<div id="header">
<h1><em>CIO &amp; Vice President</em></h1>
<div id="navigation">
    <div class="navigation-wrap">
      <ul><?php
				 $bookmarks = get_bookmarks( array(									   
				'orderby'          => 'rating',
				'order'            => 'ASC',
				'category_name'    => 'cio',
				'hide_invisible'   => 1,
                          )); 

				// Loop through each bookmark and print formatted output
				$i = 1;
				$n = count($bookmarks);
				foreach ( $bookmarks as $bm ) {
					if ($i == 1) {
						printf('<li class="nav%s first">',$i);
					} elseif ($i == ($n - 1)) {
						printf('<li class="nav%s last">',$i);
					} else {
						printf('<li class="nav%s">',$i);
					}
					printf('<a href="%s">%s</a>', $bm->link_url, __($bm->link_name) );
					printf('</li>');
					$i++;
				}
				?>
	</ul>

    </div>
  </div>
   <div id="identity">
    <div id="campaign">
      <div id="section-title">
        <h2><span class="center-helper"><a href="<?php echo get_settings('home'); ?>/"><?php bloginfo('name'); ?></a></span></h2>
      </div>
    </div>
  </div>
</div>
<hr />
