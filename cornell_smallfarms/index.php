<?php get_header(); ?>
<div id="wrap">
<div id="content">
<div id="content-wrap">

  <div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
    <h2><a href="<?php the_permalink() ?>" rel="bookmark">
      <?php the_title(); ?>
      </a></h2>
      <?php the_content(__('Read more'));?>
    <div class="postmeta">
      <div class="postmetaleft">
      
        <p>
        <span class="avatar-box"><?php echo get_avatar( get_the_author_email(), '25' ); ?><br/><?php the_author_posts_link(); ?></span>
          category: 
          <?php the_category(', ') ?><br />
          posted: <?php the_time('m/j/y g:i A') ?> by  <?php the_author_posts_link(); ?><br />
          <?php if ('open' == $post->comment_status) : ?><?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?> <?php else : ?>
<?php endif; ?><span class="edit-post""><?php edit_post_link('edit', '', ''); ?></span>
        </p>
      </div>
    </div>

    <h2>Comments</h2>
    <?php comments_template(); // Get wp-comments.php template ?>
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php echo('<p class="post-nav">'); posts_nav_link(' ', __('<span class="post-nav-back">&laquo; Previous</span>'), __('<span class="post-nav-next">More &raquo;</span>')); echo('</p>'); ?>
  </div>
  <div id="secondary">
	
	<?php if ( get_post_meta($post->ID, 'Related Posts', true) ) : ?>
    <div class="color-box green">
		<div id="pages" class="widget_pages">
			<h3>Related Posts:</h3>
			<ul>
				<?php
					global $post;
					$postID = $post->ID;
					$related = get_post_meta($post->ID, 'Related Posts', true);
				?>
				<?php query_posts('posts_per_page=30&orderby=date&order=ASC&meta_key=Related Posts&meta_value=' . $related); ?>
					<ul id="category-listing">
					<li class="heading"><?php echo $related; ?>
					<ul class="children">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<li<?php if ($post->ID == $postID) { echo ' class="current"'; } ?>>
							<a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
						</li>
					<?php endwhile; else : ?>
					<?php endif; ?>
					</ul>
					</li>
					</ul>
				<?php wp_reset_query(); ?>
			</ul>

		</div>
	</div>
	<?php endif; ?>
	
	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	<?php endif; ?>
	</div>
  
</div>
</div>
</div>
<?php get_footer(); ?>