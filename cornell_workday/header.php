<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="distribution" content="global" />
<meta name="robots" content="follow, all" />
<meta name="language" content="en, sv" />
<title>
<?php wp_title(''); ?>
<?php if(wp_title('', false)) { echo ' |'; } ?>
 <?php bloginfo('name');
 	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	?>
</title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<!-- leave this for stats please -->
<link rel="Shortcut Icon" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_get_archives('type=monthly&format=link'); ?>
<?php wp_head(); ?>

<!-- iws js -->
<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/jquery-ui.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws_js/framework/iws_light.js"></script>
<script type="text/javascript">
	var js_path = "<?php echo get_stylesheet_directory_uri('template_directory'); ?>/iws_js/framework/";
	iws_init();
</script>

<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/colorbox/jquery.colorbox.js"></script>
<style type="text/css" media="screen">
<!--
@import url( <?php bloginfo('stylesheet_url');
?> );
-->
</style>
<link rel="stylesheet" rev="stylesheet" media="print" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/styles/print.css" />

<script>
			jQuery(document).ready(function(){
				//Examples of how to assign the ColorBox event to elements
				jQuery(".group1").colorbox({rel:'group1'});
				jQuery(".group2").colorbox({rel:'group2'});
				jQuery(".group3").colorbox({rel:'group3'});
				jQuery(".group5").colorbox({rel:'group5'});
				jQuery(".group6").colorbox({rel:'group6'});
				jQuery(".group7").colorbox({rel:'group7'});
				jQuery(".group8").colorbox({rel:'group8'});
				jQuery(".group9").colorbox({rel:'group9', slideshow:true});
				jQuery(".ajax").colorbox();
				jQuery(".youtube").colorbox({iframe:true, innerWidth:818, innerHeight:618});
				jQuery(".iframe").colorbox({iframe:true, innerWidth:1042, innerHeight:786});
				jQuery(".inline").colorbox({inline:true, width:"50%"});
				jQuery(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});
				
				//Example of preserving a JavaScript event for inline calls.
				jQuery("#click").click(function(){ 
					jQuery('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		</script>
</head>
<body <?php body_class(); ?>>

<?php include (TEMPLATEPATH . '/banner.php'); ?>

