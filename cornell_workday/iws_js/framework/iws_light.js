/* IWS Dynamic Components
   ------------------------------------------- */  

//var js_path = "/iws_js/framework/"; //moved to header

var iws_include = ["iws_popup.js",
				   "iws_tooltips.js",
				   "iws_expander.js",
				   "iws_tabs.js"];


/* load resources */
function iws_load() {
	jQuery.ajaxSetup({async: false});
	
	for (i=0;i<iws_include.length;i++) {
		jQuery.getScript(js_path+iws_include[i]);
	}
}

/* initialize components */
function iws_init() {
	iws_load();
	
	jQuery(document).ready(function(){
		popups();
		tooltips(100);
		expander();
		tabs();
		tabsWithNav();
	});
}