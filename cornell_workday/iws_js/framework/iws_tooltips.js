/* Tooltips 0.1b (ama39)
	-- added experimental support for padding-adjustments (3/30/12)
	-- fixed IE double tooltip bug (4/3/12)
   ------------------------------------------- */   

/* Global Options -------------- */
var tooltip_maxwidth = 280;
var tooltip_shadow = true;
var fadein_speed = 200;
var fadein_delay = 400;
var fadeout_speed = 100;
var fadeout_delay = 20;

/* Global Variables ------------ */
var tip_active = false;
var fade_active = false;
var tip_count = 0;
var current_tip = 0;
var xMouse = 0;
var yMouse = 0;
var bounds_x1 = 0;
var bounds_x2 = 0;
var bounds_y1 = 0;
var bounds_y2 = 0;

/* -----------------------------------------------------------------------------------------
   Initialize Tooltips
   -----------------------------------------------------------------------------------------
   - begin mouse tracker
   - creates the tooltip DIV node
   - sets up mouse events to control tooltip behavior
-------------------------------------------------------------------------------------------- */
function tooltips(custom_delay) {
	// Capture optional custom delay value ---------- 
	if (custom_delay) {
		fadein_delay = custom_delay;
	}
	
	// Track mouse coordinates ---------- 
	jQuery(document).mousemove(function(e) {
		xMouse = e.pageX;
		yMouse = e.pageY;
		if (tip_active && !fade_active) {
			if (xMouse < bounds_x1 || xMouse > bounds_x2 || yMouse < bounds_y1 || yMouse > bounds_y2) {
				//jQuery("#log").prepend("<div>out</div>");
				toolTipsDelayOut();
				fade_active = true;
			}
		}
	});
	
	// Click off tooltips ----------
	jQuery(document).mousedown(function() {
		jQuery("#tooltip").css("display","none");
		tip_active = false;
		current_tip = 0;
	});
	
	// Create #tooltip node ---------- 
	jQuery("body").append("<div id=\"tooltip\"></div>");
	jQuery("#tooltip").css({
		"position": "absolute",
		"display": "none",
		"cursor": "pointer"
	});
	if (tooltip_shadow) {
		jQuery("#tooltip").addClass("dropshadow"); // apply dropshadow preference		
	}
	
	// Setup hover events ----------
	jQuery(".tooltip").each(function() {
		tip_count++;
		jQuery(this).data("tipID",tip_count);
		jQuery(this).data("tiptext",jQuery(this).attr("title"));
		jQuery(this).removeAttr("title");
		
		jQuery(this).hover(function(e) {
			
			var tiptext = jQuery(this).data("tiptext");
			//jQuery("#log").prepend("<div>tipid: "+jQuery(this).data("tipID")+", current: "+current_tip+"</div>");
			if (tiptext != "" && tiptext != undefined && jQuery(this).data("tipID") != current_tip) {	
				
				jQuery("#tooltip").css("left","0");
				jQuery("#tooltip").html(tiptext);
				jQuery("#tooltip").css({
					"width": "auto",
					"display": "none"
				});
				
				// send bounds data ----------
				var position = jQuery(this).position();
				toolTipsGetElementBounds(
					position.left , 
					position.top , 
					jQuery(this).outerHeight() , 
					jQuery(this).outerWidth()
				);
				
				// activate tooltip ----------
				current_tip = jQuery(this).data("tipID");
				toolTipsDelayIn(this);
			}
		},
		function() {
			if (!tip_active) {
				clearTimeout(window.tip_delay);
				current_tip = 0;
			}
		});
	});
}

/* -----------------------------------------------------------------------------------------
   Tooltip UI Delay
   -----------------------------------------------------------------------------------------
   - delay the tooltip response on mouseover slightly
   - calculate size and position after the delay
-------------------------------------------------------------------------------------------- */
function toolTipsDelayIn(nodeID) {
	clearTimeout(window.tip_delay);
	tip_active = false;
	fade_active = false;
	window.tip_delay = setTimeout(function() {
		if (jQuery("#tooltip").width() > tooltip_maxwidth) {
			jQuery("#tooltip").css("width",(tooltip_maxwidth.toString()+"px"));
		}
		var position = jQuery(nodeID).position();
		jQuery("#tooltip").css({
			"top": yMouse - (jQuery("#tooltip").height()+13) ,
			"left": xMouse + 3
		}).fadeIn(fadein_speed);
		tip_active = true;
		
		// edge detection ----------
		var xTest = (xMouse + 3) + jQuery("#tooltip").width();
		var yTest = yMouse - (jQuery("#tooltip").height()+13);
		var xPadding = parseInt(jQuery("#tooltip").css("padding-left")) + parseInt(jQuery("#tooltip").css("padding-right"));
		var yPadding = parseInt(jQuery("#tooltip").css("padding-top")) + parseInt(jQuery("#tooltip").css("padding-bottom"));
		if (xTest + xPadding+2 > jQuery(window).width() + jQuery(window).scrollLeft()) {
			//jQuery("#log").append("<div>out of bounds: right</div>");
			jQuery("#tooltip").css("left", parseInt(jQuery("#tooltip").css("left")) - (jQuery("#tooltip").width() + xPadding + 6));
		}
		if (yTest + yPadding+2 < jQuery(window).scrollTop()) {
			//jQuery("#log").append("<div>out of bounds: top</div>");
			jQuery("#tooltip").css("top", parseInt(jQuery("#tooltip").css("top")) + (jQuery("#tooltip").height() + yPadding + 26));
		}
		
	}, fadein_delay);
}
function toolTipsDelayOut() {
	clearTimeout(window.tip_delay);
	window.tip_delay = setTimeout(function() {
		if (tip_active) {
			jQuery("#tooltip").fadeOut(fadeout_speed);
			tip_active = false;
			fade_active = false;
			current_tip = 0;
		}
	}, fadeout_delay);
}

/* -----------------------------------------------------------------------------------------
   Detect Element Bounds
   -----------------------------------------------------------------------------------------
   - makes some simple calculations to determin the hit area for an element
   - arguments: left, top, height, width
-------------------------------------------------------------------------------------------- */
function toolTipsGetElementBounds(x,y,h,w) {
	bounds_x1 = x;
	bounds_x2 = bounds_x1 + w;
	bounds_y1 = y;
	bounds_y2 = bounds_y1 + h;
}

