<?php
/**
 * Template Name: Homepage
 * @package WordPress
 * @subpackage Cornell
 */
get_header(); ?>


<div id="wrap">
<div id="main-navigation">
          <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
          <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
        </div>
        <!-- main navigation --> 
 <div id="content-wrap"> 
                
      <div id="header">
       <div class="header-meta">
         <h1><a href="<?php echo get_settings('home'); ?>/"><strong><?php bloginfo('name'); ?></strong></a></h1>
         <h2><?php bloginfo( 'description' ); ?></h2>
       </div>
       <img class="campaign-image" src="<?php header_image(); ?>" alt="" />
              
 <?php /*?> <?php if ( is_active_sidebar( 'sidebar-home' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-home' ); ?>
        <?php endif; ?> <?php */?>     
        
    </div>
      </div>
        <div id="content">
      <div id="main">
  
         <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php if ( is_front_page() ) { ?>
          <h3 class="entry-title"><?php the_title(); ?></h3><!-- removing title from homepage -->
          <?php } else { ?>
          <h1 class="entry-title">
            <?php /*?><?php the_title(); ?><?php */?><!-- removing title from homepage -->
          </h1>
          <?php } ?>
          <div class="entry-content">
            <?php the_content(); ?>
            <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
            <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
          </div>
          <!-- .entry-content --> 
        </div>
        <!-- #post-## -->
        <?php endwhile; ?>
        
  <div id="featured-posts">
<div class="category-left">
         <?php query_posts('cat=213&showposts=3'); ?> <!-- uncomment this line before sending to vendor-->
          <?php /*?><?php query_posts('category_name=featured&showposts=3'); ?><?php */?>  <!-- comment this line before sending to vendor -->      
          <h3 class="cat-name"><?php echo get_cat_name(213);?></h3>
    
          <?php $first = true; ?>          
           <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <div id="post-<?php the_ID(); ?>" <?php $post_nr = 'post-nr-'.($wp_query->current_post+1); post_class($post_nr); ?>>
         <?php /*?><h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3><?php */?>   		<?php if ( $first ): ?>
<?php the_post_thumbnail();?>
      <?php $first = false; ?>
    <?php endif; ?>                             
            <?php /*?> <div class="entry-date"><span class="month"><?php the_time('M') ?></span><span class="day"><?php the_time('d') ?></span><span class="year"><?php the_time('Y') ?></span></div> <?php */?>
            
             <?php if ( is_front_page() ) { ?>
             <?php print_post_title() ?>
             <?php } else { ?>
             <?php print_post_title() ?>
             <?php } ?>            
             </div>             
           <?php endwhile; ?>
           <p><span class="see-all-category-posts"><a href="<?php echo get_category_link(213); ?>">see all <?php echo get_cat_name(213);?></a></span></p>
 </div>          
 <div class="category-right">          
 		<?php query_posts('cat=71333&showposts=6'); ?> <!-- uncomment this line before sending to vendor-->
          <?php /*?><?php query_posts('category_name=featured&showposts=3'); ?><?php */?>  <!-- comment this line before sending to vendor -->
          <h3 class="cat-name"><?php echo get_cat_name(71333);?></h3>
          
           <?php $first = true; ?>
           <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>          
          <div id="post-<?php the_ID(); ?>" <?php $post_nr = 'post-nr-'.($wp_query->current_post+1); post_class($post_nr); ?>>
          <?php if ( $first ): ?>
<?php the_post_thumbnail();?>
      <?php $first = false; ?>
    <?php endif; ?> 
              <?php /*?><div class="entry-date"><span class="month"><?php the_time('M') ?></span><span class="day"><?php the_time('d') ?></span><span class="year"><?php the_time('Y') ?></span></div><?php */?><!-- calendar -->
             <?php if ( is_front_page() ) { ?>
             <div class="post-tn"><?php print_post_title() ?></div>
             <?php } else { ?>
             <?php print_post_title() ?>
             <?php } ?>
             </div><!-- #post-## -->
           <?php endwhile; ?>
           <p><span class="see-all-category-posts"><a href="<?php echo get_category_link(71333); ?>">see all <?php echo get_cat_name(71333);?></a></span></p>
         </div>
 </div>         
     
    <!--  <div id="secondary">    
    </div>-->
<div id="extra"><?php if ( is_active_sidebar( 'sidebar-home-extra' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-home-extra' ); ?>
        <?php endif; ?>
        </div>
        
   </div>      
  </div>
</div>
<?php get_footer(); ?>
