<?php get_header(); ?>
<div id="wrap" class="twocolumn-left">
<div id="main-navigation">
          <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
          <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
        </div>
        <div id="content-wrap" class="twocolumn-left"> 
                
      <div id="header" class="twocolumn-left">
       <div class="header-meta">
         <h1><a href="<?php echo get_settings('home'); ?>/"><strong><?php bloginfo('name'); ?></strong></a></h1>
         <h2><?php bloginfo( 'description' ); ?></h2>
         <h2 class="page-title"><?php the_category(', ') ?></h2>
       </div>
       <img class="campaign-image" src="<?php header_image(); ?>" alt="" />
      
      
 <?php /*?> <?php if ( is_active_sidebar( 'sidebar-home' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-home' ); ?>
        <?php endif; ?> <?php */?>     
        
    </div>
      </div>
<div id="content">
      
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
 <div id="main" class="twocolumn-left">
  <h2 class="page-title">
  
<?php if ( is_day() ) : ?>
				<?php printf( __( '<span>%s</span>' ), get_the_date() ); ?>
<?php elseif ( is_month() ) : ?>
				<?php printf( __( '<span>%s</span>' ), get_the_date( _x( 'F Y', 'monthly archives date format' ) ) ); ?>
<?php elseif ( is_year() ) : ?>
				<?php printf( __( '<span>%s</span>' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) ); ?>
<?php else : ?>
				<?php the_category(', ') ?>
<?php endif; ?>

  </h2>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="entry-date"><span class="month"><?php the_time('M') ?></span> <span class="day"><?php the_time('d') ?></span> <span class="year"><?php the_time('Y') ?></span></div>
    <?php print_post_title() ?>
    
    <?php the_excerpt(__('Read more'));?>
    <!--

	<?php trackback_rdf(); ?>

	-->
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
  </div>
  <div id="secondary">
	  <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
      <?php endif; ?>
  </div>
  
</div>
</div>

<?php get_footer(); ?>
