<?php get_header(); ?>
<div id="wrap" class="twocolumn-left">
<div id="main-navigation">
          <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
          <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
        </div>
        <div id="content-wrap" class="twocolumn-left"> 
                
      <div id="header" class="twocolumn-left">
       <div class="header-meta">
         <h1><a href="<?php echo get_settings('home'); ?>/"><strong><?php bloginfo('name'); ?></strong></a></h1>
         <h2><?php bloginfo( 'description' ); ?></h2>
         <h2 class="page-title"><?php the_category(', ') ?></h2>
       </div>
       <img class="campaign-image" src="<?php header_image(); ?>" alt="" />
      
      
 <?php /*?> <?php if ( is_active_sidebar( 'sidebar-home' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-home' ); ?>
        <?php endif; ?> <?php */?>     
        
    </div>
      </div>
<div id="content">

  <div id="main">
    <h2>Not Found, Error 404</h2>
    <p>The page you are looking for no longer exists.</p>
  </div>
  <div id="secondary">
	  <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
      <?php endif; ?>
  </div>
</div>
</div>
<!-- The main column ends  -->
</div>
<?php get_footer(); ?>
