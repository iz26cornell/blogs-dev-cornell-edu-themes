<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

	</div><!-- #main -->

	<footer id="colophon" role="contentinfo">

			<?php
				/* A sidebar in the footer? Yep. You can can customize
				 * your footer with three columns of widgets.
				 */
				if ( ! is_404() )
					get_sidebar( 'footer' );
			?>

			<div id="site-generator">
				<div style="float:left;">Powered by <a href="http://edublogs.org/campus">Edublogs Campus</a> and running on <a href="http://blogs.cornell.edu">blogs.cornell.edu</a></div><div style="float:right;">&copy; <?php $the_year = date("Y"); echo $the_year; ?> <a href="http://www.cornell.edu/">Cornell University</a></div>
                <div style="clear:both">Cornell Cooperative Extension is an equal opportunity, affirmative action educator and employer.</div>
			</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>