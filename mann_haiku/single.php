<?php get_header(); ?>

	<div id="content" class="widecolumn">
				
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<div class="navigation">
			<div class="alignleft"><?php previous_post_link(); ?></div>
			<div class="alignright"><?php next_post_link(); ?></div>
		</div>
	
		<div class="post">
		
			<p class="date"><?php the_time('l, F jS, Y') ?></p>
				
			<div class="entry">
				<?php the_content() ?>
			</div>
	
                        <p class="author">- <?php $cat = get_the_category(); if ($cat[1]->cat_name == "Author") { $cat = $cat[0]; } else {$cat = $cat[1];} $author = $cat->category_nicename; $author_bio = $cat->category_description; echo $cat->cat_name;
?></p>		
			<!-- <p class="author"><?php /* If this is a category archive */ if (is_category()) { ?>				
	- <?php $cat = get_the_category(); $cat = $cat[1]; $author_bio = $cat->category_description; echo $cat->cat_name; 
	} else { ?>- <?php $cat = get_the_category(); $cat = $cat[1]; echo '<a href="/category/author/' . $cat->category_nicename .'" title="Author\'s Archive">'.$cat->cat_name.'</a>'; }
?></p> -->
	
			<p class="postmetadata"><?php edit_post_link('Edit this entry', '', ''); ?></p>				

		</div>
	
	<?php endwhile; else: ?>
	
		<p>Sorry, no posts matched your criteria.</p>
	
<?php endif; ?>
	
	</div>
	
<?php get_sidebar(); ?>

<?php get_footer(); ?>
