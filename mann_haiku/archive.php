<?php get_header(); ?>

	<div id="content" class="narrowcolumn">

		<?php if (have_posts()) : ?>

		 <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
<?php /* If this is a category archive */ if (is_category()) { ?>				
		<h2 class="pagetitle">Archive for <?php echo single_cat_title(); ?></h2>
		
 	  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h2 class="pagetitle">Archive for <?php the_time('F jS, Y'); ?></h2>
		
	 <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h2 class="pagetitle">Archive for <?php the_time('F, Y'); ?></h2>

		<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h2 class="pagetitle">Archive for <?php the_time('Y'); ?></h2>
		
	  <?php /* If this is a search */ } elseif (is_search()) { ?>
		<h2 class="pagetitle">Search Results</h2>
		
	  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h2 class="pagetitle">Author Archive</h2>

		<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h2 class="pagetitle">Blog Archives</h2>

		<?php } ?>

		<?php while (have_posts()) : the_post(); ?>		
		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Previous'); ?></div>
			<div class="alignright"><?php previous_posts_link('Next &raquo;'); ?></div>
		</div>

		<div class="post">
		
			<p class="date"><?php the_time('l, F jS, Y') ?></p>
				
			<div class="entry">
				<?php the_content() ?>
			</div>
			
			<p class="author"><?php /* If this is a category archive */ if (is_category()) { ?>				
	- <?php $cat = get_the_category(); if ($cat[1]->cat_name == "Author") { $cat = $cat[0]; } else {$cat = $cat[1];} $author_bio = $cat->category_description; echo $cat->cat_name; 
	} else { ?>- <?php $cat = get_the_category(); if ($cat[1]->cat_name == "Author") { $cat = $cat[0]; } else {$cat = $cat[1];} echo '<a href="/category/author/' . $cat->category_nicename .'" title="Author\'s Archive">'.$cat->cat_name.'</a>'; }
?></p>
	
			<p class="postmetadata"><?php edit_post_link('Edit this entry', '', ''); ?></p>				

		</div>
	
		<?php endwhile; ?>
		
		<?php /* If this is a author archive and the author bio exists */ if (is_category() && $author_bio != '') { ?>
			<h3 class="author">Author Bio</h3>
			
			<div class="author-bio"><?php echo $author_bio; ?></div>
			
		<?php } ?>
	
	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<?php include (TEMPLATEPATH . '/searchform.php'); ?>

	<?php endif; ?>
		
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
