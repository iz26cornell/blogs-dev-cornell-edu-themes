<?php $author = 'test'; ?>
<?php get_header(); ?>

	<div id="content">

	<?php if (have_posts()) : ?>
		
		<?php while (have_posts()) : the_post(); ?>
				
			<div class="post" id="post-<?php the_ID(); ?>">
				<div class="entry">
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>
				
				<p class="author">- <?php $cat = get_the_category(); if ($cat[1]->cat_name == "Author") { $cat = $cat[0]; } else {$cat = $cat[1];} $author = $cat->category_nicename; $author_bio = $cat->category_description; echo $cat->cat_name;
?></p>
			</div>
			
			<div class="navigation">
				<div class="alignleft"><?php next_posts_link('&laquo; Previous') ?></div>
				<div class="alignright"><?php previous_posts_link('Next &raquo;') ?></div> 
			</div>
			
			<p class="date-home"><?php the_time('l, F jS, Y') ?></p>
	
		<?php endwhile; ?>
		
	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php include (TEMPLATEPATH . "/searchform.php"); ?>

	<?php endif; ?>

	</div>
 
<?php include('sidebar.php'); ?>

<?php get_footer(); ?>
