	<div id="sidebar">
		
			<!--
			<li>
				<?php include (TEMPLATEPATH . '/searchform.php'); ?>
			</li>
			-->
			
			<?php /* If this is a 404 page */ if (is_404()) { ?>
			<?php /* If this is a category archive */ } elseif (is_category()) { ?>
			<p>You are currently browsing the archive for <?php single_cat_title(''); ?>.</p>
			
			<?php /* If this is a yearly archive */ } elseif (is_day()) { ?>
			<p>You are currently browsing the <a href="<?php bloginfo('home'); ?>/"><?php echo bloginfo('name'); ?></a> weblog archives
			for the day <?php the_time('l, F jS, Y'); ?>.</p>
			
			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
			<p>You are currently browsing the monthly archive for <?php the_time('F, Y'); ?>.</p>

      <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
			<p>You are currently browsing the <a href="<?php bloginfo('home'); ?>/"><?php echo bloginfo('name'); ?></a> weblog archives
			for the year <?php the_time('Y'); ?>.</p>
			
		 <?php /* If this is a monthly archive */ } elseif (is_search()) { ?>
			<p>You have searched the <a href="<?php echo bloginfo('home'); ?>/"><?php echo bloginfo('name'); ?></a> weblog archives
			for <strong>'<?php echo wp_specialchars($s); ?>'</strong>. If you are unable to find anything in these search results, you can try one of these links.</p>

			<?php /* If this is a monthly archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
			<p>You are currently browsing the <a href="<?php echo bloginfo('home'); ?>/"><?php echo bloginfo('name'); ?></a> weblog archives.</p>

			<?php } ?>
			
			<ul>
				<li class="page_item"><a href="/" title="Home">Home</a></li>
				<?php wp_list_pages('exclude=56&title_li='); ?>
				<li class="page_item"><a href="/archives/" title="Archives">Archives</a></li>
				<li class="page_item"><a class="rss" href="http://feeds.feedburner.com/mannlibrary/dailyhaiku" title="Subscribe to the Daily Haiku RSS">Subscribe to our RSS Feed</a>
				<?php if ($author_bio != '') {
					echo ('<li class="page_item bump"><a href="/category/author/' . $author . '" title="Author Bio">Author Bio</a></li>');
					}
				?>
				<li class="page_item bump"><a href="http://mannlib.cornell.edu" title="Mann Library">Return to Mann Library</a></li>
			</ul>
			
			
			<?php global $user_ID;
				get_currentuserinfo();
				if ('' == $user_ID) {
					//no user logged in
				} else {
					echo
			'<ul>';
				echo wp_register();
				echo '<li>';
				echo wp_loginout();
				echo '</li>
			</ul>';
				}
			?>
			
	</div>

