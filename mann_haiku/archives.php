<?php
/*
Template Name: Archives
*/
?>

<?php get_header(); ?>

<div id="content" class="widecolumn">

<!-- <?php include (TEMPLATEPATH . '/searchform.php'); ?> -->

<div id="author-archives">
	<h2>Archives by Author</h2>
	<ul class="archives">
		 <?php wp_list_cats('sort_column=name&exclude=1,16&hierarchical=0'); ?>
	</ul>
</div>

<div id ="month-archives">
	<h2>Archives by Month</h2>
	<ul class="archives">
		<?php wp_get_archives('type=monthly'); ?>
	</ul>
</div>
	
</div>	

<?php get_sidebar(); ?>

<?php get_footer(); ?>
