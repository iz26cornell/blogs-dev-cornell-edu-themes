<?php
/*
 WARNING: This file is part of the core Genesis framework. DO NOT edit
 this file under any circumstances. Please do all modifications
 in the form of a child theme.
 */

/**
 * Handles primary sidebar structure.
 *
 * This file is a core Genesis file and should not be edited.
 *
 * @category Genesis
 * @package  Templates
 * @author   StudioPress
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     http://www.studiopress.com/themes/genesis
 */

?>

<?php if ( is_active_sidebar( 'sidebar-section' ) ) : ?>
    <div id="sidebar-section" class="sidebar widget-area">
    <?php
        genesis_before_sidebar_widget_area();
        dynamic_sidebar( 'Top Sidebar' );
        genesis_after_sidebar_widget_area();
    ?>
    </div>
<?php endif; ?>