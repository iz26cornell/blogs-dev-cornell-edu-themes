<?php
/** Start the engine */
require_once( get_template_directory() . '/lib/init.php' );

/** Child theme (do not remove) */
define( 'CHILD_THEME_NAME', 'Cornell UnivCom Theme' );
define( 'CHILD_THEME_URL', 'http://www.cit.cornell.edu' );

/** Add Viewport meta tag for mobile browsers */
add_action( 'genesis_meta', 'magazine_add_viewport_meta_tag' );
function magazine_add_viewport_meta_tag() {
    echo '<meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0"/>';
}

/** Change header size */
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'brunelleschi_header_image_width', 960 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'brunelleschi_header_image_height', 306 ) );

/** Create additional color style options */
add_theme_support( 'genesis-style-selector', array(
	'magazine-blue'		=> 'Blue',
	'magazine-green'	=> 'Green',
	'magazine-orange'	=> 'Orange',
	'magazine-purple'	=> 'Purple',
	'magazine-red'		=> 'Red',
	'magazine-teal'		=> 'Teal'
) );

$content_width = apply_filters( 'content_width', 610, 460, 910 );

/** Add support for structural wraps */
add_theme_support( 'genesis-structural-wraps', array(
	'header',
	'nav',
	'subnav',
	'inner',
	'footer-widgets',
	'footer'
) );

/** Add new image sizes */
add_image_size( 'home-bottom', 280, 150, TRUE );
add_image_size( 'slider', 600, 250, TRUE );
add_image_size( 'square', 120, 120, TRUE );
add_image_size( 'tabs', 580, 250, TRUE );
add_image_size( 'album-grid', 225, 150, true );

/** Add support for custom header */
add_theme_support( 'genesis-custom-header', array(
	'width' => 960,
	'height' => 115
) );

/** Add support for custom background */
add_theme_support( 'custom-background' );

/** Reposition the primary navigation */
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_before', 'genesis_do_nav' );

/** Add after post ad section */
add_action( 'genesis_after_post_content', 'magazine_after_post_ad', 9 ); 
function magazine_after_post_ad() {
    if ( is_single() && is_active_sidebar( 'after-post-ad' ) ) {
    echo '<div class="after-post-ad">';
	dynamic_sidebar( 'after-post-ad' );
	echo '</div><!-- end .after-post-ad -->';
	}
}

/** Add after content ad section */
add_action( 'genesis_before_footer', 'magazine_after_content_ad' ); 
function magazine_after_content_ad() {
    if ( is_active_sidebar( 'after-content-ad' ) ) {
    echo '<div class="after-content-ad">';
	dynamic_sidebar( 'after-content-ad' );
	echo '</div><!-- end .after-content-ad -->';
	}
}

/** Add support for 3-column footer widgets */
add_theme_support( 'genesis-footer-widgets', 3 );

/** Register widget areas */
genesis_register_sidebar( array(
	'id'				=> 'home-top',
	'name'			=> __( 'Home Top', 'magazine' ),
	'description'	=> __( 'This is the home top section.', 'magazine' ),
) );
genesis_register_sidebar( array(
	'id'				=> 'home-left',
	'name'			=> __( 'Home Left', 'magazine' ),
	'description'	=> __( 'This is the home left section.', 'magazine' ),
) );
genesis_register_sidebar( array(
	'id'				=> 'home-right',
	'name'			=> __( 'Home Right', 'magazine' ),
	'description'	=> __( 'This is the home right section.', 'magazine' ),
) );
genesis_register_sidebar( array(
	'id'				=> 'home-bottom',
	'name'			=> __( 'Home Bottom', 'magazine' ),
	'description'	=> __( 'This is the home bottom section.', 'magazine' ),
) );
genesis_register_sidebar( array(
	'id'				=> 'after-post-ad',
	'name'			=> __( 'After Post Ad', 'magazine' ),
	'description'	=> __( 'This is the after post ad section.', 'magazine' ),
) );
genesis_register_sidebar( array(
	'id'				=> 'after-content-ad',
	'name'			=> __( 'After Content Ad', 'magazine' ),
	'description'	=> __( 'This is the after content ad section.', 'magazine' ),
) );
/** Search mods */
function wpshock_search_filter( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', array('post','page') );
    }
    return $query;
}
add_filter('pre_get_posts','wpshock_search_filter');



/*
Plugin Name: Search Excerpt
Plugin URI: http://fucoder.com/code/search-excerpt/
Description: Modify <code>the_exceprt()</code> template code during search to return snippets containing the search phrase. Snippet extraction code stolen from <a href="http://drupal.org/">Drupal</a>'s search module. And patched by <a href="http://pobox.com/~jam/">Jam</a> to support Asian text.
Version: 1.2 $Rev$
Author: Scott Yang
Author URI: http://scott.yang.id.au/

*/

class SearchExcerpt {
    function get_content() {
        // Get the content of current post. We like to have the entire
        // content. If we call get_the_content() we'll only get the teaser +
        // page 1.
        global $post;
        
        // Password checking copied from
        // template-functions-post.php/get_the_content()
        // Search shouldn't match a passworded entry anyway.
        if (!empty($post->post_password) ) { // if there's a password
            if (stripslashes($_COOKIE['wp-postpass_'.COOKIEHASH]) != 
                $post->post_password ) 
            {      // and it doesn't match the cookie
                return get_the_password_form();
            }
        }

        return $post->post_content;
    }
    
    function get_query($text) {
        static $last = null;
        static $lastsplit = null;

        if ($last == $text)
            return $lastsplit;

        // The dot, underscore and dash are simply removed. This allows
        // meaningful search behaviour with acronyms and URLs.
        $text = preg_replace('/[._-]+/', '', $text);

        // Process words
        $words = explode(' ', $text);

        // Save last keyword result
        $last = $text;
        $lastsplit = $words;

        return $words;
    }

    function highlight_excerpt($keys, $text) {
        $text = strip_tags($text);

        for ($i = 0; $i < sizeof($keys); $i ++)
            $keys[$i] = preg_quote($keys[$i], '/');

        $workkeys = $keys;

        // Extract a fragment per keyword for at most 4 keywords.  First we
        // collect ranges of text around each keyword, starting/ending at
        // spaces.  If the sum of all fragments is too short, we look for
        // second occurrences.
        $ranges = array();
        $included = array();
        $length = 0;
        while ($length < 256 && count($workkeys)) {
            foreach ($workkeys as $k => $key) {
                if (strlen($key) == 0) {
                    unset($workkeys[$k]);
                    continue;
                }
                if ($length >= 256) {
                    break;
                }
                // Remember occurrence of key so we can skip over it if more
                // occurrences are desired.
                if (!isset($included[$key])) {
                    $included[$key] = 0;
                }

                // NOTE: extra parameter for preg_match requires PHP 4.3.3
                if (preg_match('/'.$key.'/iu', $text, $match, 
                               PREG_OFFSET_CAPTURE, $included[$key])) 
                {
                    $p = $match[0][1];
                    $success = 0;
                    if (($q = strpos($text, ' ', max(0, $p - 60))) !== false && 
                         $q < $p)
                    {
                        $end = substr($text, $p, 80);
                        if (($s = strrpos($end, ' ')) !== false && $s > 0) {
                            $ranges[$q] = $p + $s;
                            $length += $p + $s - $q;
                            $included[$key] = $p + 1;
                            $success = 1;
                        }
                    }

                    if (!$success) {
                        // for the case of asian text without whitespace
                        $q = _jamul_find_1stbyte($text, max(0, $p - 60));
                        $q = _jamul_find_delimiter($text, $q);
                        $s = _jamul_find_1stbyte_reverse($text, $p + 80, $p);
                        $s = _jamul_find_delimiter($text, $s);
                        if (($s >= $p) && ($q <= $p)) {
                            $ranges[$q] = $s;
                            $length += $s - $q;
                            $included[$key] = $p + 1;
                        } else {
                            unset($workkeys[$k]);
                        }
                    }
                } else {
                    unset($workkeys[$k]);
                }
            }
        }

        // If we didn't find anything, return the beginning.
        if (sizeof($ranges) == 0)
            return '<p>' . _jamul_truncate($text, 256) . '&nbsp;...</p>';

        // Sort the text ranges by starting position.
        ksort($ranges);

        // Now we collapse overlapping text ranges into one. The sorting makes
        // it O(n).
        $newranges = array();
        foreach ($ranges as $from2 => $to2) {
            if (!isset($from1)) {
                $from1 = $from2;
                $to1 = $to2;
                continue;
            }
            if ($from2 <= $to1) {
                $to1 = max($to1, $to2);
            } else {
                $newranges[$from1] = $to1;
                $from1 = $from2;
                $to1 = $to2;
            }
        }
        $newranges[$from1] = $to1;

        // Fetch text
        $out = array();
        foreach ($newranges as $from => $to)
            $out[] = substr($text, $from, $to - $from);

        $text = (isset($newranges[0]) ? '' : '...&nbsp;').
            implode('&nbsp;...&nbsp;', $out).'&nbsp;...';
        $text = preg_replace('/('.implode('|', $keys) .')/iu', 
                             '<strong class="search-excerpt">\0</strong>', 
                             $text);
        return "<p>$text</p>";
    }

    function the_excerpt($text) {
        static $filter_deactivated = false;
        global $more;
        global $wp_query;

        // If we are not in a search - simply return the text unmodified.
        if (!is_search())
            return $text;

        // Deactivating some of the excerpt text.
        if (!$filter_deactivated) {
            remove_filter('the_excerpt', 'wpautop');
            $filter_deactivated = true;
        }

        // Get the whole document, not just the teaser.
        $more = 1;
        $query = SearchExcerpt::get_query($wp_query->query_vars['s']);
        $content = SearchExcerpt::get_content();

        return SearchExcerpt::highlight_excerpt($query, $content);
    }
}

// The number of bytes used when WordPress looking around to find delimiters
// (either a whitespace or a point where ASCII and other character switched).
// This also represents the number of bytes of few characters.
define('_JAMUL_LEN_SEARCH', 15);

function _jamul_find_1stbyte($string, $pos=0, $stop=-1) {
    $len = strlen($string);
    if ($stop < 0 || $stop > $len) {
        $stop = $len;
    }
    for (; $pos < $stop; $pos++) {
        if ((ord($string[$pos]) < 0x80) || (ord($string[$pos]) >= 0xC0)) {
            break;      // find 1st byte of multi-byte characters.
        }
    }
    return $pos;
}

function _jamul_find_1stbyte_reverse($string, $pos=-1, $stop=0) {
    $len = strlen($string);
    if ($pos < 0 || $pos >= $len) {
        $pos = $len - 1;
    }
    for (; $pos >= $stop; $pos--) {
        if ((ord($string[$pos]) < 0x80) || (ord($string[$pos]) >= 0xC0)) {
            break;      // find 1st byte of multi-byte characters.
        }
    }
    return $pos;
}

function _jamul_find_delimiter($string, $pos=0, $min = -1, $max=-1) {
    $len = strlen($string);
    if ($pos == 0 || $pos < 0 || $pos >= $len) {
        return $pos;
    }
    if ($min < 0) {
        $min = max(0, $pos - _JAMUL_LEN_SEARCH);
    }
    if ($max < 0 || $max >= $len) {
        $max = min($len - 1, $pos + _JAMUL_LEN_SEARCH);
    }
    if (ord($string[$pos]) < 0x80) {
        // Found ASCII character at the trimming point.  So, trying
        // to find new trimming point around $pos.  New trimming point
        // should be on a whitespace or the transition from ASCII to
        // other character.
        $pos3 = -1;
        for ($pos2 = $pos; $pos2 <= $max; $pos2++) {
            if ($string[$pos2] == ' ') {
                break;
            } else if ($pos3 < 0 && ord($string[$pos2]) >= 0x80) {
                $pos3 = $pos2;
            }
        }
        if ($pos2 > $max && $pos3 >= 0) {
            $pos2 = $pos3;
        }
        if ($pos2 > $max) {
            $pos3 = -1;
            for ($pos2 = $pos; $pos2 >= $min; $pos2--) {
                if ($string[$pos2] == ' ') {
                    break;
                } else if ($pos3 < 0 && ord($string[$pos2]) >= 0x80) {
                    $pos3 = $pos2 + 1;
                }
            }
            if ($pos2 < $min && $pos3 >= 0) {
                $pos2 = $pos3;
            }
        }
        if ($pos2 <= $max && $pos2 >= $min) {
            $pos = $pos2;
        }
    } else if ((ord($string[$pos]) >= 0x80) || (ord($string[$pos]) < 0xC0)) {
        $pos = _jamul_find_1stbyte($string, $pos, $max);
    }
    return $pos;
}

function _jamul_truncate($string, $byte) {
    $len = strlen($string);
    if ($len <= $byte)
        return $string;
    $byte = _jamul_find_1stbyte_reverse($string, $byte);
    return substr($string, 0, $byte);
}

// Add with priority=5 to ensure that it gets executed before wp_trim_excerpt
// in default filters.
add_filter('get_the_excerpt', array('SearchExcerpt', 'the_excerpt'), 5);

/*
Plugin Name: Hierarchical Pages
Version: 1.6.1
Plugin URI: http://www.wlindley.com/website/hierpage/
Description: Adds sidebar widgets to display a context-based list of "nearby" pages, and to display nested categories.
Author: William Lindley
Author URI: http://www.wlindley.com/
Text Domain: hierarchical-pages
License: GPL2
*/

/*  Copyright 2007-2012 William Lindley (email : wlindley -at- wlindley -dot- com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if (!class_exists('SRCS_WP_Widget')) {
  class SRCS_WP_Widget extends WP_Widget
  {
    function form_html($instance) {
      $option_menu = $this->known_params(1);
      $tdom = 'hierarchical-pages';

      foreach (array_keys($option_menu) as $param) {
	$param_display[$param] = htmlspecialchars($instance[$param]);
      }

      foreach ($option_menu as $option_name => $option) {
	$checkval='';
	$desc = '';
	if (isset($option['desc']) && $option['desc'])
	  $desc = '<br /><small>' . __($option['desc'], $tdom) . '</small>';
	switch ($option['type']) {
	case 'checkbox':
	  if ($instance[$option_name]) // special HTML and override value
	    $checkval = 'checked="yes" ';
	  $param_display[$option_name] = 'yes';
	  break;
	case '':
	  $option['type'] = 'text';
	  break;
	}
	print '<p style="text-align:right;"><label for="' . $this->get_field_name($option_name) . '">' . 
	  __($option['title'], $tdom) . 
	  ' <input style="width: 200px;" id="' . $this->get_field_id($option_name) . 
	  '" name="' . $this->get_field_name($option_name) . 
	  "\" type=\"{$option['type']}\" {$checkval}value=\"{$param_display[$option_name]}\" /></label>$desc</p>";
      }
    }
  }
}

class HierPageWidget extends SRCS_WP_Widget
{
  /**
   * Declares the HierPageWidget class.
   *
   */
  function __construct(){
    $tdom = 'hierarchical-pages';
    $widget_ops = array('classname' => 'widget_hier_page',
			'description' => __( "Hierarchical Page Directory Widget", $tdom) );
    $control_ops = array('width' => 300, 'height' => 300);
    $this->WP_Widget('hierpage', __('Hierarchical Pages', $tdom), $widget_ops, $control_ops);
  }

  /**
   * Helper function
   *
   */
  function hierpages_list_pages($args = '') {
    global $post;
    global $wp_query;

    if ( !isset($args['echo']) )
      $args['echo'] = 1;

    $output = '';
    
    if(!isset($args['post_type']) || !$args['post_type'])
      $args['post_type'] = 'page';

    // Query pages.  NOTE: The array is sorted in alphabetical, or menu, order.
    $pages = & get_pages($args);

    $page_info = Array();

    if ( $pages ) {
      $current_post = $wp_query->get_queried_object_id();

      foreach ( $pages as $page ) {
	$page_info[$page->ID]['parent'] = $page->post_parent;
	$page_info[$page->post_parent]['children'][] = $page->ID;
      }

      // Display the front page?
      $front_page = -1; // assume no static front page
      if ('page' == get_option('show_on_front')) {
	$front_page = get_option('page_on_front');
	// Regard flag: always show front page?  Otherwise: Show front page only if it has children
	if (($args['show_home'] == 'yes') || (sizeof($page_info[$front_page]['children']))) {
	  $page_info[$front_page]['show'] = 1;	// always show front page
	}
      }
	
      // add all children of the root node, but only to single depth.
      if ($args['show_root'] == 'yes') {
	foreach ( $page_info[0]['children'] as $child ) {
	  if ($child != $front_page) {
	    $page_info[$child]['show'] = 1;
	  }
	}
      }
      
      if (is_post_type_hierarchical($args['post_type'])) {

	$page_count = 0;
	// show the current page's children, if any.
	if (isset($page_info[$current_post]['children']) && 
	    is_array($page_info[$current_post]['children'] )) {
	  foreach ( $page_info[$current_post]['children'] as $child ) {
	    $page_info[$child]['show'] = 1;
	    $page_count++;
	  }
	}

	$post_parent = $page_info[$current_post]['parent'];
	if ($post_parent && ($args['show_siblings'] == 'yes')) {
	  // if showing siblings, add the current page's parent's other children.
	  foreach ( $page_info[$post_parent]['children'] as $child ) {
	    if ($child != $front_page) {
	      $page_info[$child]['show'] = 1;
	      $page_count++;
	    }
	  }

	  // Also show parent node's siblings.
	  $post_grandparent = $page_info[$post_parent]['parent'];
	  if ($post_grandparent) {
	    foreach ( $page_info[$post_grandparent]['children'] as $child ) {
	      if ($child != $front_page) {
		$page_info[$child]['show'] = 1;
		$page_count++;
	      }
	    }
	  }
	}

	// add all ancestors of the current page.
	while ($post_parent) {
	  $page_info[$post_parent]['show'] = 1;
          // show that page's children, if any.                                                            
          if (is_array($page_info[$post_parent]['children'] )) {
            foreach ( $page_info[$post_parent]['children'] as $child ) {
              $page_info[$child]['show'] = 1;
	      $page_count++;
            }
          }
	  $post_parent = $page_info[$post_parent]['parent'];
	}

	if (($post->ID != $front_page) && ($page_count > 0) ) {
	  // The current page is always shown, unless
	  // 1. it is the static front page (see above), or
	  // 2. no other pages are displayed
	  $page_info[$post->ID]['show'] = 1;
	}

      }
      
      // Add pages that were selected
      $my_includes = Array();

      foreach ( $pages as $page ) {
	if (isset($page_info[$page->ID]['show']) && $page_info[$page->ID]['show']) {
	  $my_includes[] = $page->ID;
	}
      }
      if (isset($args['child_of']) && $args['child_of']) {
        $my_includes[] = $args['child_of'];
      }
      
      if (!empty($my_includes)) {
        // List pages, if any. Blank title_li suppresses unwanted elements.
        $output .= wp_list_pages( Array('title_li' => '',
					'sort_column' => $args['sort_column'],
					'sort_order' => $args['sort_order'],
					'include' => $my_includes,
                                        'post_type'=> $args['post_type'],
					'echo' => $args['echo']
                                        ) );
      }
    }

    $output = apply_filters('wp_list_pages', $output);
    
    if ( $args['echo'] )
      echo $output;
    else
      return $output;
  }

  /**
   * Displays the Widget
   *
   */
  function widget($args, $instance){

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $known_params = $this->known_params(0);
    foreach ($known_params as $param) {
      if (strlen($instance[$param])) {
	$page_options[$param] = $instance[$param];
      }
    }
      
    if (isset($instance['menu_order']) && $instance['menu_order'] == 'yes') { 
      // Deprecated, eliminated upon form display (see below)
      $page_options['sort_column']='menu_order,post_title';
    }

    $page_options['echo'] = 0;
    $output = $this->hierpages_list_pages($page_options);
    if (strlen($output)) {
      print $args['before_widget'];
      if ( $title )
	print "{$args['before_title']}{$title}{$args['after_title']}";
      print "<ul>{$output}</ul>{$args['after_widget']}";
    }
  }

  function known_params ($options = 0) {
    $option_menu = array('title' => array('title' => 'Title:'),
			 'show_siblings' => array('title' => 'Show siblings to the current page?',
						  'type' => 'checkbox'),
			 'show_root' => array('title' => 'Always show top-level pages?',
					      'type' => 'checkbox'),
			 'show_home' => array('title' => 'Show the static home page?',
					      'desc' => '(always shown if it has child pages)',
					      'type' => 'checkbox'),
			 'child_of' => array('title' => 'Root page ID:'),
			 'exclude' => array('title' => 'Exclude pages:',
					    'desc' => 'List of page IDs to exclude'),
			 'sort_column' => array('title' => 'Sort field:',
						'desc' => 'Comma-separated list: <em>post_title, menu_order, post_date, post_modified, ID, post_author, post_name</em>'),
			 'sort_order' => array('title' => 'Sort direction:',
					       'desc' => '(default: ASC)'),
			 'meta_key' => array('title' => 'Meta Key:'),
			 'meta_value' => array('title' => 'Meta-key Value:',
					     'desc' => 'for selecting pages by custom fields'),
			 'authors' => array('title' => 'Authors:'),
			 'post_status' => array('title' => 'Post status:',
						'desc' => '(default: publish)'),
			 'post_type' => array('title' => 'Post type:',
						'desc' => '(default: page)'),
			 );
    return ($options ? $option_menu : array_keys($option_menu));
  }

  /**
   * Saves the widget's settings.
   *
   */
  function update($new_instance, $old_instance){
    $instance = $old_instance;
    $known_params = $this->known_params();
    unset($instance['menu_order']);
    foreach ($known_params as $param) {
      $instance[$param] = strip_tags(stripslashes($new_instance[$param]));
    }
    $instance['sort_order'] = strtolower($instance['sort_order']) == 'desc'?'DESC':'ASC';
    return $instance;
  }

  /**
   * Creates the edit form for the widget.
   *
   */
  function form($instance){
    $instance = wp_parse_args( (array) $instance, array('title'=>'') );
    if ($instance['menu_order']) {
      $instance['sort_column'] = 'menu_order,post_title';
    }
    if (empty($instance['sort_column'])) {
      $instance['sort_column'] = 'post_title';
    }

    $this->form_html($instance);
  }

}// END class

/**
 * Register Hierarchical Pages widget.
 *
 * Calls 'widgets_init' action after the widget has been registered.
 */
function HierPageInit() {
  register_widget('HierPageWidget');
}

function HierPageLoad() {
  $plugin_dir = basename(dirname(__FILE__));
  load_plugin_textdomain('hierarchical-pages', false, $plugin_dir . '/languages');
}

  /*
   * Plugin Name: Hierarchical Categories (combined with Hierarchical Pages)
   * Plugin URI: http://www.wlindley.com/
   * Description: Adds a sidebar widget to display a context-based list of "nearby" categories.
   * Author: William Lindley
   * Author URI: http://www.wlindley.com/
   */

class HierCatWidget extends SRCS_WP_Widget
{
  /**
   * Declares our class.
   *
   */
  function __construct(){
    $tdom = 'hierarchical-pages';
    $widget_ops = array('classname' => 'widget_hier_cat', 'description' => __( "Hierarchical Category Widget", $tdom) );
    $control_ops = array('width' => 300, 'height' => 300);
    $this->WP_Widget('hiercat', __('Hierarchical Categories', $tdom), $widget_ops, $control_ops);
  }

  /**
   * Helper function
   *
   */
  function hiercat_list_cats($args) {
    global $post;
    global $wp_query;

    if ( !isset($args['echo']) )
      $args['echo'] = 1;
    $sort_column = $args['sort_column'];

    $output = '';

    // Query categories.
    $cats = & get_categories($args);
    if ($cats['errors']) {
      print "<pre>"; print_r($cats); print "</pre>";
      return;
    }
    $cat_info = Array();

    if ( !empty ($cats) ) {
      $current_cat = $wp_query->get_queried_object_id();

      foreach ( $cats as $cat ) {
	$cat_info[$cat->term_id]['parent'] = $cat->category_parent;
	$cat_info[$cat->category_parent]['children'][] = $cat->term_id;
      }

      // add all children of the root node, but only to single depth.
      foreach ( $cat_info[0]['children'] as $child ) {
	$cat_info[$child]['show'] = 1;
      }
      
      // If currently displaying a category, taxonomy, or tag; AND
      // if it is the same as the one in this widget...
      if ((is_category() || is_tax() || is_tag()) && 
	  ($args['taxonomy'] == get_queried_object()->taxonomy ) ) {
	// show the current category's children, if any.
	if (is_array($cat_info[$current_cat]['children'] )) {
	   foreach ( $cat_info[$current_cat]['children'] as $child ) {
	      $cat_info[$child]['show'] = 1;
	   }
	}

	$cat_parent = $cat_info[$current_cat]['parent'];
	if ($cat_parent && ($args['show_siblings'] == 'yes')) {
	  // if showing siblings, add the current category's parent's other children.
	  foreach ( $cat_info[$cat_parent]['children'] as $child ) {
	    $cat_info[$child]['show'] = 1;
	  }

	  // Also show parent node's siblings.
	  $cat_grandparent = $cat_info[$cat_parent]['parent'];
	  if ($cat_grandparent) {
	    foreach ( $cat_info[$cat_grandparent]['children'] as $child ) {
		$cat_info[$child]['show'] = 1;
	    }
	  }
	}
	
	// add all ancestors of the current category.
	while ($cat_parent) {
	  $cat_info[$cat_parent]['show'] = 1;
	  $cat_parent = $cat_info[$cat_parent]['parent'];
	}
      }
      
      $my_includes = Array();
      // Add categories that were selected
      foreach ( $cats as $cat ) {
	if ($cat_info[$cat->term_id]['show']) {
	  $my_includes[] =$cat->term_id;
	}
      }
      
      if (!empty($my_includes)) {
        // List categories, if any. Blank title_li suppresses unwanted elements.
        $qargs = Array('title_li' => '', 'hide_empty' => 0, 'include' => $my_includes,
                      'order' => $args['order'], 'orderby' => $args['orderby'],
                      'show_count' => $args['show_count']);
        if (!empty($args['taxonomy'])) {
          $qargs['taxonomy'] = $args['taxonomy'];
        }
        $output .= wp_list_categories( $qargs );
      }
    }

    $output = apply_filters('wp_list_categories', $output);
    
    if ( $args['echo'] )
      echo $output;
    else
      return $output;
  }

  function known_params ($options = 0) {
    $option_menu = array('title' => array('title' => 'Title:'),
			 'show_siblings' => array('title' => 'Show siblings to the current category?',
						  'type' => 'checkbox'),
			 'include' => array('title' => 'Include:',
					    'desc' => 'Comma-delimited list of category IDs, or blank for all'),
			 'exclude' => array('title' => 'Exclude:'),
			 'orderby' => array('title' => 'Sort field:',
					    'desc' => 'Enter one of: <em>name, count, term_group, slug</em> or a custom value. Default: name'),
			 'order' => array('title' => 'Sort direction:',
					  'desc' => '(default: ASC)'),
			 'child_of' => array('title' => 'Only display Categories below this ID'),
			 'hide_empty' => array('title' => 'Hide empty categories?', 
						  'type' => 'checkbox'),
			 'show_count' => array('title' => 'Show count of category entries?', 
						  'type' => 'checkbox'),
			 'taxonomy' => array('title' => 'Custom taxonomy:'),
			 );
    if ($options) {
      $taxons = get_taxonomies();
      $option_menu['taxonomy']['desc'] = 'Enter one of: <em>' . 
	implode(', ',array_keys($taxons)) . '</em> or blank for post categories.';
    }
    return ($options ? $option_menu : array_keys($option_menu));
  }
  /**
   * Displays the Widget
   *
   */
  function widget($args, $instance){
    $known_params = $this->known_params(0);
    foreach ($known_params as $param) {
      if (strlen($instance[$param]))
	$cat_options[$param] = $instance[$param];
    }
    $cat_options['title'] = apply_filters('widget_title', $cat_options['title']);
    // WordPress defaults to hiding: thus, always specify.
    $cat_options['hide_empty'] = $cat_options['hide_empty'] == 'yes' ? 1 : 0;
      
    print $args['before_widget'];
    if ( strlen($cat_options['title']) )
      print "{$args['before_title']}{$cat_options['title']}{$args['after_title']}";
    print "<ul>";

    $this->hiercat_list_cats($cat_options);
    print "</ul>{$args['after_widget']}";
  }

  /**
   * Saves the widget's settings.
   *
   */
  function update($new_instance, $old_instance){
    $instance = $old_instance;

    $known_params = $this->known_params();
    foreach ($known_params as $param) {
      $instance[$param] = strip_tags(stripslashes($new_instance[$param]));
    }
    return $instance;
  }

  /**
   * Creates the edit form for the widget.
   *
   */
  function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('title'=>'') );

    $this->form_html($instance);
  }

}// END class

/**
 * Register Hierarchical Categories widget.
 *
 * Calls 'widgets_init' action after the widget has been registered.
 */
function HierCatInit() {
  register_widget('HierCatWidget');
}

/*
 * Initialize both widgets
 */

add_action('widgets_init', 'HierCatInit');
add_action('plugins_loaded', 'HierPageLoad');
add_action('widgets_init', 'HierPageInit');

/**
 * Add featured image to post after title
 */
add_action( 'genesis_after_post_title', 'single_post_featured_image', 15 );

function single_post_featured_image() {
	
	if ( ! is_singular('post') )
		return;
	
	$img = genesis_get_image( array( 'format' => 'html', 'size' => 'medium', 'attr' => array( 'class' => 'post-image alignleft' ) ) );
	printf( '<a href="%s" title="%s">%s</a>', get_permalink(), the_title_attribute( 'echo=0' ), $img );
	}


/** Remove page/post/attachment titles */
add_action( 'get_header', 'child_remove_page_titles' );
function child_remove_page_titles() {
	if ( is_front_page() )
	remove_action( 'genesis_post_title', 'genesis_do_post_title' );
}

/* Remove post meta from posts */
add_action ( 'genesis_post_content' , 'wps_post_meta_check' );
function wps_post_meta_check() {
	if ( in_category('people') || in_category('news') || in_category('climate-smart-farmers') )
	remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
}

/* Remove post info from people posts */
add_filter( 'genesis_post_info', 'remove_post_info_from_category' );
function remove_post_info_from_category($post_info) {
	if ( is_category('people') )
		$post_info = '';
		return $post_info;  
}
/* Remove post info from farms posts */
add_filter( 'genesis_post_info', 'remove_post_info_from_farm_category' );
function remove_post_info_from_farm_category($post_info) {
	if ( is_category('climate-smart-farmers') )
		$post_info = '';
		return $post_info;  
}
/* Remove "by author" post info from news posts */
add_filter( 'genesis_post_info', 'change_post_info_for_category' );
function change_post_info_for_category($post_info) {
	if ( in_category('news') )
		$post_info = '[post_date]';
		return $post_info;  
}

/**
* show full content on people archives
*/
add_action( 'genesis_before_loop', 'sk_full_content_specific_category' );
 
function sk_full_content_specific_category() {
  if ( is_category('People') || is_category('Climate Smart Farmers') ) {
		remove_action( 'genesis_post_content', 'genesis_do_post_content' ); /* Pre-HTML5 */
		// remove_action( 'genesis_entry_content', 'genesis_do_post_content' ); /* HTML5 */
 
		add_action( 'genesis_post_content', 'sk_do_post_content' ); /* Pre-HTML5 */
		// add_action( 'genesis_entry_content', 'sk_do_post_content' ); /* HTML5 */
	}
}
 
function sk_do_post_content() {
	the_content();
}


//* Prefix author breadcrumb trail with ''
add_filter( 'genesis_breadcrumb_args', 'rvam_prefix_author_breadcrumb' );
function rvam_prefix_author_breadcrumb( $args ) {
 
  $args['labels']['category'] = '';
  return $args;
 
}

/**
 * Auto-generate taxonomy title for archive pages
 *
 * Will say "[Term] Archives"
 * 
 */
add_action( 'genesis_before_loop', 'ac_do_taxonomy_title_description', 20 );

function ac_do_taxonomy_title_description() {

	global $wp_query;

	if ( ! is_category() && ! is_tag() && ! is_tax() )
		return;

	if ( get_query_var( 'paged' ) >= 2 )
		return;

	$term = is_tax() ? get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ) : $wp_query->get_queried_object();

	if ( ! $term || ! isset( $term->meta ) )
		return;

	$headline = '';

	// If we have a headline already, then return, otherwise auto-generate
	if ( $term->meta['headline'] )
		return;
	else {
		$headline = sprintf( '<h1>%s</h1>', single_term_title( '', false ) );
		printf( '<div class="taxonomy-description">%s</div>', $headline );
	}

}

/*
Plugin Name: Allow Javascript in Text Widgets
Plugin URI: http://philipjohn.co.uk/#pj-better-multisite-text-widget
Description: Replaces the default text widget with one that allows Javascript so you can do basic things like add Google Ads to your sidebar without using other plugins.
Version: 0.3
Author: Philip John
Author URI: http://philipjohn.co.uk
License: GPL2
Network: true
Text Domain: allow-javascript-in-text-widgets
*/

/**
 * Text widget class
 */
class WP_Widget_Text_With_JS extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_text', 'description' => __('Arbitrary text or HTML', 'allow-javascript-in-text-widgets'));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('text', __('Text', 'allow-javascript-in-text-widgets'), $widget_ops, $control_ops);
	}

	function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters( 'widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
		$text = apply_filters( 'widget_text', $instance['text'], $instance );
		echo $before_widget;
		if ( !empty( $title ) ) { echo $before_title . $title . $after_title; } ?>
			<div class="textwidget"><?php echo $instance['filter'] ? wpautop($text) : $text; ?></div>
		<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['text'] =  $new_instance['text'];
		$instance['filter'] = isset($new_instance['filter']);
		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '' ) );
		$title = strip_tags($instance['title']);
		$text = esc_textarea($instance['text']);
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'allow-javascript-in-text-widgets'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>

		<p><input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs', 'allow-javascript-in-text-widgets'); ?></label></p>
<?php
	}
}
function wp_widget_text_with_js_init(){
	unregister_widget('WP_Widget_Text');
	register_widget('WP_Widget_Text_With_JS');
}
add_action('widgets_init', 'wp_widget_text_with_js_init', 1);


/**
 * Plugin Name: Display Posts Shortcode
 * Plugin URI: http://www.billerickson.net/shortcode-to-display-posts/
 * Description: Display a listing of posts using the [display-posts] shortcode
 * Version: 2.4
 * Author: Bill Erickson
 * Author URI: http://www.billerickson.net
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume 
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @package Display Posts
 * @version 2.4
 * @author Bill Erickson <bill@billerickson.net>
 * @copyright Copyright (c) 2011, Bill Erickson
 * @link http://www.billerickson.net/shortcode-to-display-posts/
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
 
 
/**
 * To Customize, use the following filters:
 *
 * `display_posts_shortcode_args`
 * For customizing the $args passed to WP_Query
 *
 * `display_posts_shortcode_output`
 * For customizing the output of individual posts.
 * Example: https://gist.github.com/1175575#file_display_posts_shortcode_output.php
 *
 * `display_posts_shortcode_wrapper_open` 
 * display_posts_shortcode_wrapper_close`
 * For customizing the outer markup of the whole listing. By default it is a <ul> but
 * can be changed to <ol> or <div> using the 'wrapper' attribute, or by using this filter.
 * Example: https://gist.github.com/1270278
 */ 
 
// Create the shortcode
add_shortcode( 'display-posts', 'be_display_posts_shortcode' );
function be_display_posts_shortcode( $atts ) {

	// Original Attributes, for filters
	$original_atts = $atts;

	// Pull in shortcode attributes and set defaults
	$atts = shortcode_atts( array(
		'title'              => '',
		'author'              => '',
		'category'            => '',
		'date_format'         => '(n/j/Y)',
		'display_posts_off'   => false,
		'exclude_current'     => false,
		'id'                  => false,
		'ignore_sticky_posts' => false,
		'image_size'          => false,
		'include_title'       => true,
		'include_author'      => false,
		'include_content'     => false,
		'include_date'        => false,
		'include_excerpt'     => false,
		'meta_key'            => '',
		'meta_value'          => '',
		'no_posts_message'    => '',
		'offset'              => 0,
		'order'               => 'DESC',
		'orderby'             => 'date',
		'post_parent'         => false,
		'post_status'         => 'publish',
		'post_type'           => 'post',
		'posts_per_page'      => '10',
		'tag'                 => '',
		'tax_operator'        => 'IN',
		'tax_term'            => false,
		'taxonomy'            => false,
		'wrapper'             => 'ul',
		'wrapper_class'       => 'display-posts-listing',
		'wrapper_id'          => false,
	), $atts, 'display-posts' );
	
	// End early if shortcode should be turned off
	if( $atts['display_posts_off'] )
		return;

	$shortcode_title = sanitize_text_field( $atts['title'] );
	$author = sanitize_text_field( $atts['author'] );
	$category = sanitize_text_field( $atts['category'] );
	$date_format = sanitize_text_field( $atts['date_format'] );
	$exclude_current = be_display_posts_bool( $atts['exclude_current'] );
	$id = $atts['id']; // Sanitized later as an array of integers
	$ignore_sticky_posts = be_display_posts_bool( $atts['ignore_sticky_posts'] );
	$image_size = sanitize_key( $atts['image_size'] );
	$include_title = be_display_posts_bool( $atts['include_title'] );
	$include_author = be_display_posts_bool( $atts['include_author'] );
	$include_content = be_display_posts_bool( $atts['include_content'] );
	$include_date = be_display_posts_bool( $atts['include_date'] );
	$include_excerpt = be_display_posts_bool( $atts['include_excerpt'] );
	$meta_key = sanitize_text_field( $atts['meta_key'] );
	$meta_value = sanitize_text_field( $atts['meta_value'] );
	$no_posts_message = sanitize_text_field( $atts['no_posts_message'] );
	$offset = intval( $atts['offset'] );
	$order = sanitize_key( $atts['order'] );
	$orderby = sanitize_key( $atts['orderby'] );
	$post_parent = $atts['post_parent']; // Validated later, after check for 'current'
	$post_status = $atts['post_status']; // Validated later as one of a few values
	$post_type = sanitize_text_field( $atts['post_type'] );
	$posts_per_page = intval( $atts['posts_per_page'] );
	$tag = sanitize_text_field( $atts['tag'] );
	$tax_operator = $atts['tax_operator']; // Validated later as one of a few values
	$tax_term = sanitize_text_field( $atts['tax_term'] );
	$taxonomy = sanitize_key( $atts['taxonomy'] );
	$wrapper = sanitize_text_field( $atts['wrapper'] );
	$wrapper_class = sanitize_html_class( $atts['wrapper_class'] );
	if( !empty( $wrapper_class ) )
		$wrapper_class = ' class="' . $wrapper_class . '"';
	$wrapper_id = sanitize_html_class( $atts['wrapper_id'] );
	if( !empty( $wrapper_id ) )
		$wrapper_id = ' id="' . $wrapper_id . '"';

	
	// Set up initial query for post
	$args = array(
		'category_name'       => $category,
		'order'               => $order,
		'orderby'             => $orderby,
		'post_type'           => explode( ',', $post_type ),
		'posts_per_page'      => $posts_per_page,
		'tag'                 => $tag,
	);
	
	// Ignore Sticky Posts
	if( $ignore_sticky_posts )
		$args['ignore_sticky_posts'] = true;
	
	// Meta key (for ordering)
	if( !empty( $meta_key ) )
		$args['meta_key'] = $meta_key;
	
	// Meta value (for simple meta queries)
	if( !empty( $meta_value ) )
		$args['meta_value'] = $meta_value;
		
	// If Post IDs
	if( $id ) {
		$posts_in = array_map( 'intval', explode( ',', $id ) );
		$args['post__in'] = $posts_in;
	}
	
	// If Exclude Current
	if( $exclude_current )
		$args['post__not_in'] = array( get_the_ID() );
	
	// Post Author
	if( !empty( $author ) )
		$args['author_name'] = $author;
		
	// Offset
	if( !empty( $offset ) )
		$args['offset'] = $offset;
	
	// Post Status	
	$post_status = explode( ', ', $post_status );		
	$validated = array();
	$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
	foreach ( $post_status as $unvalidated )
		if ( in_array( $unvalidated, $available ) )
			$validated[] = $unvalidated;
	if( !empty( $validated ) )		
		$args['post_status'] = $validated;
	
	
	// If taxonomy attributes, create a taxonomy query
	if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {
	
		// Term string to array
		$tax_term = explode( ', ', $tax_term );
		
		// Validate operator
		if( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) )
			$tax_operator = 'IN';
					
		$tax_args = array(
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'slug',
					'terms'    => $tax_term,
					'operator' => $tax_operator
				)
			)
		);
		
		// Check for multiple taxonomy queries
		$count = 2;
		$more_tax_queries = false;
		while( 
			isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) && 
			isset( $original_atts['tax_' . $count . '_term'] ) && !empty( $original_atts['tax_' . $count . '_term'] ) 
		):
		
			// Sanitize values
			$more_tax_queries = true;
			$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
	 		$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
	 		$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts['tax_' . $count . '_operator'] : 'IN';
	 		$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
	 		
	 		$tax_args['tax_query'][] = array(
	 			'taxonomy' => $taxonomy,
	 			'field' => 'slug',
	 			'terms' => $terms,
	 			'operator' => $tax_operator
	 		);
	
			$count++;
			
		endwhile;
		
		if( $more_tax_queries ):
			$tax_relation = 'AND';
			if( isset( $original_atts['tax_relation'] ) && in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) ) )
				$tax_relation = $original_atts['tax_relation'];
			$args['tax_query']['relation'] = $tax_relation;
		endif;
		
		$args = array_merge( $args, $tax_args );
	}
	
	// If post parent attribute, set up parent
	if( $post_parent ) {
		if( 'current' == $post_parent ) {
			global $post;
			$post_parent = get_the_ID();
		}
		$args['post_parent'] = intval( $post_parent );
	}
	
	// Set up html elements used to wrap the posts. 
	// Default is ul/li, but can also be ol/li and div/div
	$wrapper_options = array( 'ul', 'ol', 'div' );
	if( ! in_array( $wrapper, $wrapper_options ) )
		$wrapper = 'ul';
	$inner_wrapper = 'div' == $wrapper ? 'div' : 'li';

	
	$listing = new WP_Query( apply_filters( 'display_posts_shortcode_args', $args, $original_atts ) );
	if ( ! $listing->have_posts() )
		return apply_filters( 'display_posts_shortcode_no_results', wpautop( $no_posts_message ) );
		
	$inner = '';
	while ( $listing->have_posts() ): $listing->the_post(); global $post;
		
		$image = $date = $author = $excerpt = $content = '';
		
		$url = $_SERVER["REQUEST_URI"];
		$people = strpos($url, '/people/');
		$climate_smart_farmers = strpos($url, '/climate-smart-farmers/');
		$who_we_are = strpos($url, '/who-we-are/');

		if ( $people!==true || $who_we_are!==true || $climate_smart_farmers!==true )
			$title = '<a class="title" href="' . apply_filters( 'the_permalink', get_permalink() ) . '">' . get_the_title() . '</a>';

		if ( $image_size && has_post_thumbnail() && ($people!==true || $who_we_are!==true || $climate_smart_farmers!==true) )  
			$image = '<a class="image" href="' . get_permalink() . '">' . get_the_post_thumbnail( get_the_ID(), $image_size ) . '</a> ';
			
		if ( $people!==false || $who_we_are!==false || $climate_smart_farmers!==false )
			$title = '<span class="title">' . get_the_title() . '</span>';
			$image = get_the_post_thumbnail( get_the_ID(), $image_size );
		
		if ( $include_date ) 
			$date = ' <span class="date">' . get_the_date( $date_format ) . '</span>';
			
		if( $include_author )
			$author = apply_filters( 'display_posts_shortcode_author', ' <span class="author">by ' . get_the_author() . '</span>' );
		
		if ( $include_excerpt ) 
			$excerpt = ' <span class="excerpt-dash">-</span> <span class="excerpt">' . get_the_excerpt() . '</span>';
			
		if( $include_content ) {
			add_filter( 'shortcode_atts_display-posts', 'be_display_posts_off', 10, 3 );
			$content = '<div class="content">' . apply_filters( 'the_content', get_the_content() ) . '</div>'; 
			remove_filter( 'shortcode_atts_display-posts', 'be_display_posts_off', 10, 3 );
		}
		
		$class = array( 'listing-item' );
		$class = sanitize_html_class( apply_filters( 'display_posts_shortcode_post_class', $class, $post, $listing, $original_atts ) );
		$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $image . $title . $date . $author . $excerpt . $content . '</' . $inner_wrapper . '>';
		
		// If post is set to private, only show to logged in users
		if( 'private' == get_post_status( get_the_ID() ) && !current_user_can( 'read_private_posts' ) )
			$output = '';
		
		$inner .= apply_filters( 'display_posts_shortcode_output', $output, $original_atts, $image, $title, $date, $excerpt, $inner_wrapper, $content, $class );
		
	endwhile; wp_reset_postdata();
	
	$open = apply_filters( 'display_posts_shortcode_wrapper_open', '<' . $wrapper . $wrapper_class . $wrapper_id . '>', $original_atts );
	$close = apply_filters( 'display_posts_shortcode_wrapper_close', '</' . $wrapper . '>', $original_atts );
	
	$return = $open;

	if( $shortcode_title ) {

		$title_tag = apply_filters( 'display_posts_shortcode_title_tag', 'h2', $original_atts );

		$return .= '<' . $title_tag . ' class="display-posts-title">' . $shortcode_title . '</' . $title_tag . '>' . "\n";
	}

	$return .= $inner . $close;

	return $return;
}

/**
 * Turn off display posts shortcode 
 * If display full post content, any uses of [display-posts] are disabled
 *
 * @param array $out, returned shortcode values 
 * @param array $pairs, list of supported attributes and their defaults 
 * @param array $atts, original shortcode attributes 
 * @return array $out
 */
function be_display_posts_off( $out, $pairs, $atts ) {
	$out['display_posts_off'] = true;
	return $out;
}

/**
 * Convert string to boolean
 * because (bool) "false" == true
 *
 */
function be_display_posts_bool( $value ) {
	return !empty( $value ) && 'true' == $value ? true : false;
}

//adds new body class to pages
add_filter('body_class', 'add_category_class_single');

function add_category_class_single($classes){

		global $post;

		$post = get_post($post_id); 
		$slug = $post->post_name;
		$classes[] = $slug;

		return $classes;
}


/**
  *
  * Genesis Custom Header
  *
*/
 
remove_action('genesis_header', 'genesis_do_header');
remove_action('genesis_header', 'genesis_header_markup_open', 5);
remove_action('genesis_header', 'genesis_header_markup_close', 15);
function custom_header() {
  ?>
  <div id="header"><div class="wrap"></div></div>
  <?php }
 
add_action('genesis_header', 'custom_header');

/* Remove people category from search results */
function remove_categories_wp_search($query) {
if ($query->is_search) {
//$query->set('cat','-3,-12');
$query->set('cat','-300273,-388775');
}
return $query;
}
add_filter('pre_get_posts','remove_categories_wp_search');


/* Change search results page title */
add_filter( 'genesis_search_title_text', 'wp_search_title_text' );

function wp_search_title_text() {
    return 'Search Results for: ';
}

add_action( 'widgets_init', 'my_register_sidebars', 999 );

//Register Section Sidebar
function my_register_sidebars() {
	genesis_register_sidebar(
		array(
			'id'            => 'sidebar-section',
			'name'          => __( 'Top Sidebar', 'genesis' ),
			'description'   => __( 'This sidebar will always float on top of the Primary Sidebar on wide screens and above the main content on smaller screens (the Primary Sidebar will float below the main content on smaller screens).', 'genesis' ),
		)
	);
} 

//Add Section Sidebar to template
add_action( 'genesis_before_content', 'add_sidebar_section' );

function add_sidebar_section() {
	get_sidebar( 'section' );
}

?>