/* IWS Dynamic Components
   ------------------------------------------- */  

if (js_path == undefined) {
	var js_path = "js/";
}

var iws_include = ["iws_popup.js",
						 "iws_tooltips.js",
						 "iws_expander.js"];

/* load resources */
function iws_load() {
	jQuery.ajaxSetup({async: false});
	for (i=0;i<iws_include.length;i++) {
		jQuery.getScript(js_path+iws_include[i]);
	}
}

/* initialize components */
function iws_init() {
	//iws_load();
	
	// Headline Autoscale Variables
	var base_size;
	var base_width;
	var max_size = 72;
	var min_window_size = 0;
	
	// Window Size Tracking
	function resizeChecks() {
		
		// Restore Standard Main Navigation
		if (jQuery(window).width() >= 767) {
			jQuery('#search-form').removeAttr('style');
			jQuery('#nav').removeAttr('style');
			//jQuery('#header').removeAttr('style');
			jQuery('#mobile-nav').removeClass('open');
		}
		
		// Refresh Headline Autoscale 
		if (jQuery(window).width() > min_window_size) {
			jQuery('.autosize-header .home #identity-content h1').addClass('autoscale');
			var multiplier = jQuery('#identity-content').width() / base_width;
			if (multiplier > 0) {
				var new_size = base_size * multiplier;
				if (new_size > max_size) {
					new_size = max_size;
				}
				jQuery('.autosize-header .home #identity-content h1').css('font-size',new_size+'px');
			}
		}
		else {
			jQuery('.autosize-header .home #identity-content h1').removeAttr('style');
			jQuery('.autosize-header .home #identity-content h1').removeClass('autoscale');
		}
	}
	
	jQuery(window).load(function() {
		
		// Reinitialize Headline Autoscale (after remote webfonts load)
		jQuery('.autosize-header .home #identity-content h1').removeAttr('style');
		base_width = jQuery('.home #identity-content h1 span').width();
		resizeChecks();
		
		// Single-line News Headlines (move date to line two)
		jQuery('.home #news h3').each(function( index ) {
  			if (jQuery(this).next('h4.date').position().top - jQuery(this).position().top < 8) {
  				jQuery(this).find('a').append('<br />');
  			}
		});
	});
	
	jQuery(document).ready(function() {
		//popups();
		//tooltips(100);
		//expander();
		
		
		// Homepage Headline Autoscale
		base_size = parseInt(jQuery('.home #identity-content h1').css('font-size'));
		base_width = jQuery('.home #identity-content h1 span').width();
		jQuery(window).resize(resizeChecks);
		resizeChecks();		
		
		// Mobile Form
		jQuery(document).ready(function() {
			jQuery('#search_icon').click(function(){
				 jQuery('#search-form').slideToggle(100);
			});
		});

		// Mobile Navigation
		var nav_offset = jQuery('#mobile-nav').offset();
		
		jQuery('#nav').find('ul.menu').first().addClass('mobile-menu'); // standard
		//jQuery('#navigation h3').next('div').find('ul.menu').first().addClass('mobile-menu'); // drupal
		
		jQuery('#mobile-nav').addClass('mobile-menu');
		jQuery('#mobile-nav').click(function() {
			jQuery(this).toggleClass('open');
			//if (jQuery(window).width() <= 480) { 
				//jQuery('#header').slideToggle('fast'); 
			//}
			jQuery('#nav').slideToggle('fast'); // standard
			//jQuery(this).parent().next('div').find('ul.menu').slideToggle(200); //drupal
		});
		
		
		// Justified Navigation
		var nav_count = jQuery('.nav-centered #navigation ul.menu').first().find('li').length;
		if (nav_count > 0) {
			var nav_width = 100 / nav_count;
			jQuery('.nav-centered #navigation ul.menu').first().find('li').css( 'width',nav_width+'%');
		}
		
		
	});
}