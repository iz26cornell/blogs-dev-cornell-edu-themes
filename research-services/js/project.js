jQuery(document).ready(function($) {
	
	// Additional Photo Credits
	$('#program-pano .extra-caption').addClass('photo-credit');
	$('.photo-credit').each(function() {
		$(this).attr('tabindex','0').wrapInner('<div class="photo-credit-text off"></div>');
		$(this).append('<span class="photo-credit-icon zmdi zmdi-camera" aria-hidden="true"><span class="sr-only">Show Photo Credit</span></span>');
		$(this).find('.photo-credit-icon').hover(function() {
			$(this).prev('.photo-credit-text').removeClass('off');
		}, function() {
			$(this).prev('.photo-credit-text').addClass('off');
		});
	});
	
});