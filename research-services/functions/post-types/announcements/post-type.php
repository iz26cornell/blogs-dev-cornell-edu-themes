<?php

// Post Type: Announcements
function cptui_register_my_cpts_announcements() {

	$labels = [
		"name" => __( "Announcements", "cwd_base" ),
		"singular_name" => __( "Announcement", "cwd_base" ),
		"menu_name" => __( "Announcements", "cwd_base" ),
		"all_items" => __( "All Announcements", "cwd_base" ),
		"add_new" => __( "Add new", "cwd_base" ),
		"add_new_item" => __( "Add new Announcement", "cwd_base" ),
		"edit_item" => __( "Edit Announcement", "cwd_base" ),
		"new_item" => __( "New Announcement", "cwd_base" ),
		"view_item" => __( "View Announcement", "cwd_base" ),
		"view_items" => __( "View Announcements", "cwd_base" ),
		"search_items" => __( "Search Announcements", "cwd_base" ),
		"not_found" => __( "No Announcements found", "cwd_base" ),
		"not_found_in_trash" => __( "No Announcements found in trash", "cwd_base" ),
		"parent" => __( "Parent Announcement:", "cwd_base" ),
		"featured_image" => __( "Featured image for this Announcement", "cwd_base" ),
		"set_featured_image" => __( "Set featured image for this Announcement", "cwd_base" ),
		"remove_featured_image" => __( "Remove featured image for this Announcement", "cwd_base" ),
		"use_featured_image" => __( "Use as featured image for this Announcement", "cwd_base" ),
		"archives" => __( "Announcement archives", "cwd_base" ),
		"insert_into_item" => __( "Insert into Announcement", "cwd_base" ),
		"uploaded_to_this_item" => __( "Upload to this Announcement", "cwd_base" ),
		"filter_items_list" => __( "Filter Announcements list", "cwd_base" ),
		"items_list_navigation" => __( "Announcements list navigation", "cwd_base" ),
		"items_list" => __( "Announcements list", "cwd_base" ),
		"attributes" => __( "Announcements attributes", "cwd_base" ),
		"name_admin_bar" => __( "Announcement", "cwd_base" ),
		"item_published" => __( "Announcement published", "cwd_base" ),
		"item_published_privately" => __( "Announcement published privately.", "cwd_base" ),
		"item_reverted_to_draft" => __( "Announcement reverted to draft.", "cwd_base" ),
		"item_scheduled" => __( "Announcement scheduled", "cwd_base" ),
		"item_updated" => __( "Announcement updated.", "cwd_base" ),
		"parent_item_colon" => __( "Parent Announcement:", "cwd_base" ),
	];

	$args = [
		"label" => __( "Announcements", "cwd_base" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "announcements", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-megaphone",
		"supports" => [ 'title', 'editor', 'thumbnail', 'revisions', 'page-attributes' ],
		"taxonomies" => [ "announcement_categories", "announcement_tags" ],
		"show_in_graphql" => false,
	];

	register_post_type( "announcements", $args );
}

add_action( 'init', 'cptui_register_my_cpts_announcements' );

require_once 'custom-fields.php';
require_once 'custom-taxonomies.php';
require_once 'columns.php';