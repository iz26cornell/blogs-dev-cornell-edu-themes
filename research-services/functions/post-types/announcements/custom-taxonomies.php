<?php

// Custom taxonomies for announcements content type
if ( ! function_exists ( 'cptui_register_my_taxes_announcement_tags' ) ) {
	
	function cptui_register_my_taxes_announcement_tags() {

		$labels = [
			"name" => __( "Announcement tags", "cwd_base" ),
			"singular_name" => __( "Announcement tag", "cwd_base" ),
			"menu_name" => __( "Tags", "cwd_base" ),
			"all_items" => __( "All Announcement tags", "cwd_base" ),
			"edit_item" => __( "Edit Announcement tag", "cwd_base" ),
			"view_item" => __( "View Announcement tag", "cwd_base" ),
			"update_item" => __( "Update Announcement tag name", "cwd_base" ),
			"add_new_item" => __( "Add new Announcement tag", "cwd_base" ),
			"new_item_name" => __( "New Announcement tag name", "cwd_base" ),
			"parent_item" => __( "Parent Announcement tag", "cwd_base" ),
			"parent_item_colon" => __( "Parent Announcement tag:", "cwd_base" ),
			"search_items" => __( "Search Announcement tags", "cwd_base" ),
			"popular_items" => __( "Popular Announcement tags", "cwd_base" ),
			"separate_items_with_commas" => __( "Separate Announcement tags with commas", "cwd_base" ),
			"add_or_remove_items" => __( "Add or remove Announcement tags", "cwd_base" ),
			"choose_from_most_used" => __( "Choose from the most used Announcement tags", "cwd_base" ),
			"not_found" => __( "No Announcement tags found", "cwd_base" ),
			"no_terms" => __( "No Announcement tags", "cwd_base" ),
			"items_list_navigation" => __( "Announcement tags list navigation", "cwd_base" ),
			"items_list" => __( "Announcement tags list", "cwd_base" ),
			"back_to_items" => __( "Back to Announcement tags", "cwd_base" ),
		];


		$args = [
			"label" => __( "Announcement tags", "cwd_base" ),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'announcement_tags', 'with_front' => true, ],
			"show_admin_column" => true,
			"show_in_rest" => true,
			"rest_base" => "announcement_tags",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
			"show_in_graphql" => false,
			"meta_box_cb" => "post_categories_meta_box",
		];
		register_taxonomy( "announcement_tags", [ "announcements" ], $args );
	}
	add_action( 'init', 'cptui_register_my_taxes_announcement_tags' );
	
}

if ( ! function_exists ( 'cptui_register_my_taxes_announcement_categories' ) ) {
	
	function cptui_register_my_taxes_announcement_categories() {

		$labels = [
			"name" => __( "Announcement categories", "cwd_base" ),
			"singular_name" => __( "Announcement category", "cwd_base" ),
			"menu_name" => __( "Categories", "cwd_base" ),
			"all_items" => __( "All Announcement categories", "cwd_base" ),
			"edit_item" => __( "Edit Announcement category", "cwd_base" ),
			"view_item" => __( "View Announcement category", "cwd_base" ),
			"update_item" => __( "Update Announcement category name", "cwd_base" ),
			"add_new_item" => __( "Add new Announcement category", "cwd_base" ),
			"new_item_name" => __( "New Announcement category name", "cwd_base" ),
			"parent_item" => __( "Parent Announcement category", "cwd_base" ),
			"parent_item_colon" => __( "Parent Announcement category:", "cwd_base" ),
			"search_items" => __( "Search Announcement categories", "cwd_base" ),
			"popular_items" => __( "Popular Announcement categories", "cwd_base" ),
			"separate_items_with_commas" => __( "Separate Announcement categories with commas", "cwd_base" ),
			"add_or_remove_items" => __( "Add or remove Announcement categories", "cwd_base" ),
			"choose_from_most_used" => __( "Choose from the most used Announcement categories", "cwd_base" ),
			"not_found" => __( "No Announcement categories found", "cwd_base" ),
			"no_terms" => __( "No Announcement categories", "cwd_base" ),
			"items_list_navigation" => __( "Announcement categories list navigation", "cwd_base" ),
			"items_list" => __( "Announcement categories list", "cwd_base" ),
			"back_to_items" => __( "Back to Announcement categories", "cwd_base" ),
		];


		$args = [
			"label" => __( "Announcement categories", "cwd_base" ),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'announcement_categories', 'with_front' => true, ],
			"show_admin_column" => true,
			"show_in_rest" => true,
			"rest_base" => "announcement_categories",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
			"show_in_graphql" => false,
			"meta_box_cb" => "post_categories_meta_box",
		];
		register_taxonomy( "announcement_categories", [ "announcements" ], $args );
	}
	add_action( 'init', 'cptui_register_my_taxes_announcement_categories' );
	
}