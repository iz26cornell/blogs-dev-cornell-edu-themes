<?php

function announcements_columns( $columns ) {
    $columns = array(
        'cb'        => '<input type="checkbox" />',
        'title'     => 'Title',
        'make_sticky'  => 'Feature on front page?',
        'date'      =>  'Date'
    );
    return $columns;
}
add_filter( 'manage_announcements_posts_columns', 'announcements_columns' );

function announcements_column( $column, $post_id ) {
    if ( $column == 'make_sticky' ) {
        if(get_post_meta($post_id, 'make_sticky', true) == '1') {
            echo 'Yes';
        }
    }
}
add_action( 'manage_announcements_posts_custom_column', 'announcements_column', 10, 2 );