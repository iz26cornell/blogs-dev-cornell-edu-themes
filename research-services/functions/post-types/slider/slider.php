<?php

// Register post type and add custom fields
require_once 'post-type.php';
require_once 'custom-fields.php';

if ( ! function_exists ( 'slider' ) ) {

	function slider() {

		$sliderContainerId = 'slider-container';
		$sliderCaptionId = 'slider-caption';
		$photoCreditsId = 'photo-credits';

		function cwd_slider() {

			$slides = get_posts(array(
				'numberposts' => -1,
				'post_type' => 'slider',
				'meta_key' => 'slide_order',
				'orderby' => 'meta_value',
				'order' => 'ASC'
			));
			?>

			<script type="text/javascript">

				var image_array1 = [];
				var count = 0;

				<?php foreach ($slides as $slide) { ?>
				
					<?php $photocredits = Media_Credit::get_html(get_post_thumbnail_id($slide->ID)); ?>

					image_array1[count] = <?php
					  echo "['";
					  echo get_the_post_thumbnail_url($slide->ID, 'slider-image', true);
					  echo "','";
					  echo get_the_title($slide->ID);
					  echo "','";
					  echo get_field('text', $slide->ID);
					  echo "','";
					  if($photocredits && strpos($photocredits, 'href')) {
						  echo $photocredits;
					  } 
					  elseif($photocredits && !strpos($photocredits, 'href')) {
						  echo '&copy; ' . $photocredits;
					  } 
					  echo "','";
					  if (get_field('no_link', $slide->ID)) {
						echo '';
					  } else {
						  if (get_field('is_internal_link', $slide->ID)) {
							echo get_field('internal_link', $slide->ID);
						  } else {
							echo get_field('external_link', $slide->ID);
						  };
					  }
					  echo "','',''";
					  echo "]";
					?>

					count++;
				<?php } ?>

			</script>

		<?php 

		} 
		add_action('wp_footer', 'cwd_slider');

		wp_enqueue_script('cwd-slider-wp-js', get_template_directory_uri() . '/js/cwd_slider_wp.js' );	
		wp_enqueue_style('cwd-slider-wp-css', get_template_directory_uri() . '/css/cwd_slider_wp.css');

		?>

		<script type="text/javascript">
			jQuery(document).ready(function($) {
				// Reference: cwd_slider(div,caption,photocredits,time,speed,auto,random,height,path,bg,heading2)
				cwd_slider('.<?php echo $sliderContainerId ?>','.<?php echo $sliderCaptionId ?>','.<?php echo $photoCreditsId ?>',8,1,true,false,'','','','<div>');
			});
		</script>

	<?php 

	}
	add_action('wp_head', 'slider');
}