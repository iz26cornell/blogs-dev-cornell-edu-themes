<?php

function research_columns( $columns ) {
    $columns = array(
        'cb'        => '<input type="checkbox" />',
        'title'     => 'Title',
        'make_sticky'  => 'Feature on front page?',
        'research_tags'      =>  'Tags',
        'research_categories'      =>  'Categories',
        'date'      =>  'Date'
    );
    return $columns;
}
add_filter( 'manage_research_posts_columns', 'research_columns' );

function research_column( $column, $post_id ) {
    if ( $column == 'make_sticky' ) {
        if(get_post_meta($post_id, 'make_sticky', true) == '1') {
            echo 'Yes';
        }
    }
    if ( $column == 'research_tags' ) {
		$research_tags = get_the_terms( get_the_ID(), 'research_tags' ); 

		if($research_tags) {
			$count = count($research_tags);
			$i = 1;

			foreach($research_tags as $research_tag) { 
				echo '<a href="' . site_url() . '/research_tags/' . $research_tag->slug . '/">' . $research_tag->name . '</a>';
				if($i < $count) { 
					echo ', ';
				} 
				$i++;
			}
		}
    }
    if ( $column == 'research_categories' ) {
		$research_categories = get_the_terms( get_the_ID(), 'research_categories' ); 

		if($research_categories) {
			$count = count($research_categories);
			$i = 1;

			foreach($research_categories as $research_category) { 
				echo '<a href="' . site_url() . '/research_categories/' . $research_category->slug . '/">' . $research_category->name . '</a>';
				if($i < $count) { 
					echo ', ';
				} 
				$i++;
			}
		}
    }
}
add_action( 'manage_research_posts_custom_column', 'research_column', 10, 2 );