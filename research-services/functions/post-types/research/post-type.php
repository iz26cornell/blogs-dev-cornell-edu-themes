<?php

// Post Type: Research
function cptui_register_my_cpts_research() {
	$labels = [
		"name" => __( "Research", "cwd_base" ),
		"singular_name" => __( "Research", "cwd_base" ),
		"menu_name" => __( "Research", "cwd_base" ),
		"all_items" => __( "All Research", "cwd_base" ),
		"add_new" => __( "Add new", "cwd_base" ),
		"add_new_item" => __( "Add new Research", "cwd_base" ),
		"edit_item" => __( "Edit Research", "cwd_base" ),
		"new_item" => __( "New Research", "cwd_base" ),
		"view_item" => __( "View Research", "cwd_base" ),
		"view_items" => __( "View Research", "cwd_base" ),
		"search_items" => __( "Search Research", "cwd_base" ),
		"not_found" => __( "No Research found", "cwd_base" ),
		"not_found_in_trash" => __( "No Research found in trash", "cwd_base" ),
		"parent" => __( "Parent Person:", "cwd_base" ),
		"featured_image" => __( "Featured image for this Person", "cwd_base" ),
		"set_featured_image" => __( "Set featured image for this Person", "cwd_base" ),
		"remove_featured_image" => __( "Remove featured image for this Person", "cwd_base" ),
		"use_featured_image" => __( "Use as featured image for this Person", "cwd_base" ),
		"archives" => __( "Person archives", "cwd_base" ),
		"insert_into_item" => __( "Insert into Person", "cwd_base" ),
		"uploaded_to_this_item" => __( "Upload to this Person", "cwd_base" ),
		"filter_items_list" => __( "Filter Research list", "cwd_base" ),
		"items_list_navigation" => __( "Research list navigation", "cwd_base" ),
		"items_list" => __( "Research list", "cwd_base" ),
		"attributes" => __( "Research attributes", "cwd_base" ),
		"name_admin_bar" => __( "Person", "cwd_base" ),
		"item_published" => __( "Person published", "cwd_base" ),
		"item_published_privately" => __( "Person published privately.", "cwd_base" ),
		"item_reverted_to_draft" => __( "Person reverted to draft.", "cwd_base" ),
		"item_scheduled" => __( "Person scheduled", "cwd_base" ),
		"item_updated" => __( "Person updated.", "cwd_base" ),
		"parent_item_colon" => __( "Parent Person:", "cwd_base" ),
	];

	$args = [
		"label" => __( "Research", "cwd_base" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "research", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-edit-page",
		"supports" => [ "title", "editor", "thumbnail" ],
		"taxonomies" => [ "research_tags", "research_categories" ],
		'menu_position' => 6,
	];

	register_post_type( "research", $args );
}
add_action( 'init', 'cptui_register_my_cpts_research' );

require_once 'custom-fields.php';
require_once 'custom-taxonomies.php';
require_once 'columns.php';