<?php

// Custom taxonomies for research content type
if ( ! function_exists ( 'cptui_register_my_taxes_research_tags' ) ) {
	
	function cptui_register_my_taxes_research_tags() {

		$labels = [
			"name" => __( "Research tags", "cwd_base" ),
			"singular_name" => __( "Research tag", "cwd_base" ),
			"menu_name" => __( "Tags", "cwd_base" ),
			"all_items" => __( "All Research tags", "cwd_base" ),
			"edit_item" => __( "Edit Research tag", "cwd_base" ),
			"view_item" => __( "View Research tag", "cwd_base" ),
			"update_item" => __( "Update Research tag name", "cwd_base" ),
			"add_new_item" => __( "Add new Research tag", "cwd_base" ),
			"new_item_name" => __( "New Research tag name", "cwd_base" ),
			"parent_item" => __( "Parent Research tag", "cwd_base" ),
			"parent_item_colon" => __( "Parent Research tag:", "cwd_base" ),
			"search_items" => __( "Search Research tags", "cwd_base" ),
			"popular_items" => __( "Popular Research tags", "cwd_base" ),
			"separate_items_with_commas" => __( "Separate Research tags with commas", "cwd_base" ),
			"add_or_remove_items" => __( "Add or remove Research tags", "cwd_base" ),
			"choose_from_most_used" => __( "Choose from the most used Research tags", "cwd_base" ),
			"not_found" => __( "No Research tags found", "cwd_base" ),
			"no_terms" => __( "No Research tags", "cwd_base" ),
			"items_list_navigation" => __( "Research tags list navigation", "cwd_base" ),
			"items_list" => __( "Research tags list", "cwd_base" ),
			"back_to_items" => __( "Back to Research tags", "cwd_base" ),
		];


		$args = [
			"label" => __( "Research tags", "cwd_base" ),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'research_tags', 'with_front' => true, ],
			"show_admin_column" => true,
			"show_in_rest" => true,
			"rest_base" => "research_tags",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
			"show_in_graphql" => false,
			"meta_box_cb" => "post_categories_meta_box",
		];
		register_taxonomy( "research_tags", [ "research" ], $args );
	}
	add_action( 'init', 'cptui_register_my_taxes_research_tags' );
	
}

if ( ! function_exists ( 'cptui_register_my_taxes_research_categories' ) ) {
	
	function cptui_register_my_taxes_research_categories() {

		$labels = [
			"name" => __( "Research categories", "cwd_base" ),
			"singular_name" => __( "Research category", "cwd_base" ),
			"menu_name" => __( "Categories", "cwd_base" ),
			"all_items" => __( "All Research categories", "cwd_base" ),
			"edit_item" => __( "Edit Research category", "cwd_base" ),
			"view_item" => __( "View Research category", "cwd_base" ),
			"update_item" => __( "Update Research category name", "cwd_base" ),
			"add_new_item" => __( "Add new Research category", "cwd_base" ),
			"new_item_name" => __( "New Research category name", "cwd_base" ),
			"parent_item" => __( "Parent Research category", "cwd_base" ),
			"parent_item_colon" => __( "Parent Research category:", "cwd_base" ),
			"search_items" => __( "Search Research categories", "cwd_base" ),
			"popular_items" => __( "Popular Research categories", "cwd_base" ),
			"separate_items_with_commas" => __( "Separate Research categories with commas", "cwd_base" ),
			"add_or_remove_items" => __( "Add or remove Research categories", "cwd_base" ),
			"choose_from_most_used" => __( "Choose from the most used Research categories", "cwd_base" ),
			"not_found" => __( "No Research categories found", "cwd_base" ),
			"no_terms" => __( "No Research categories", "cwd_base" ),
			"items_list_navigation" => __( "Research categories list navigation", "cwd_base" ),
			"items_list" => __( "Research categories list", "cwd_base" ),
			"back_to_items" => __( "Back to Research categories", "cwd_base" ),
		];


		$args = [
			"label" => __( "Research categories", "cwd_base" ),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'research_categories', 'with_front' => true, ],
			"show_admin_column" => true,
			"show_in_rest" => true,
			"rest_base" => "research_categories",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
			"show_in_graphql" => false,
			"meta_box_cb" => "post_categories_meta_box",
		];
		register_taxonomy( "research_categories", [ "research" ], $args );
	}
	add_action( 'init', 'cptui_register_my_taxes_research_categories' );
	
}