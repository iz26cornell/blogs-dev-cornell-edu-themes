<?php

// Get the date depending on post type. Add more as needed. 
// Assumes the ACF date field is named 'custom_date'.
if ( ! function_exists( 'cwd_base_get_the_date' ) ) {
	
	function cwd_base_get_the_date() {
		
		if(get_field('publication_date')) { // News ?>

			<div class="subheading serif">
				<?php the_field('publication_date'); ?>
			</div>

		<?php }
		
		elseif(get_field('date')) { // Events ?>

			<div class="subheading serif">
				<?php the_field('date'); ?>
			</div>

		<?php }

		elseif(get_field('custom_date')) { // ACF datepicker ?>
			<div class="subheading serif">
				<?php the_field('custom_date'); ?>
			</div>
		<?php }

		else { ?>
			<div class="subheading serif">
				<?php echo get_the_date('F j, Y'); // Date (WP core) ?>
			</div>
		<?php }
		
	}
	
}