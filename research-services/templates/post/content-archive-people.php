<?php // People listings ?>

<div id="post-<?php the_ID(); ?>" <?php post_class('card flex-4'); ?>>

	<div class="group-image">

		<a href="<?php echo the_permalink(); ?>">

			<?php cwd_base_get_image(); ?>

			<div class="overlay">
				<h2 class="h3">
					<span class="deco">
						<?php the_title(); ?>
					</span>
				</h2>
			</div>

		</a>

	</div>

	<div class="group-fields">

		<?php //cwd_base_get_the_date(); ?>
		
		<?php if(get_field('professional_title')) { echo '<div class="meta">'; the_field('professional_title'); echo '</div>'; } ?>

		<?php if(get_field('department')) { echo '<div class="meta">'; the_field('department'); echo '</div>'; } ?>

		<p class="summary">
									
			<?php 
			
				$archive_options = get_field('archive_options', 'options');
				$excerpt_length = $archive_options['excerpt_length'];
			
				echo custom_excerpt($excerpt_length); // Characters
			
			?>
			
		</p>
			
		<?php $research_areas = get_field('research_areas'); ?>

		<?php if($research_areas) { ?>

			<div class="meta links">
								
				<?php $count = count($research_areas); ?>
				
				<?php $i = 0; ?>
				
				<?php foreach($research_areas as $research) :  ?>

					<?php $research_name = ucwords(str_replace('-', ' ', $research)); ?>
				
					<a href="<?php echo site_url('/research/').$research; ?>"><?php echo $research_name; ?></a>
				
					<?php $i++; ?>
				
					<?php if($i < $count) { echo ', '; } ?>

				<?php endforeach; wp_reset_postdata(); ?>
				
			</div>

		<?php } ?>

		<?php cwd_base_get_tag_options(); ?>

	</div>

</div>