<?php // The page content ?>

<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php if( is_front_page() ) {
	
		$args = array( 
			'post_type' => 'announcements',
			'posts_per_page' => 1,
			'meta_query' => array( array(
			   'key' => 'make_sticky',
			   'value' => '1',
			   'compare' => '=',
			),
		) ); 
		$query = new WP_Query($args);	 
	
	?>			
	
	<div class="wrapper<?php if($query->have_posts()) { echo ' flex-grid'; } ?>">

		<?php if ($query->have_posts()): while ($query->have_posts()) : $query->the_post(); ?>

			<div class="content-block cwd-component cwd-basic no-overlay announcement flex-5 tiles">

				<div class="header-with-button">
					<h2 class="h1"><?php the_title(); ?></h2>
				</div>

				<?php if(get_field('subtitle')) { ?>
					<div class="subtitle">
						<?php the_field('subtitle'); ?>
					</div>
				<?php } ?>

				<div class="cards">
					<div class="card flex-4">
						<div class="group-fields">
							<p class="summary"><?php echo custom_excerpt(180); ?></p>
						</div>
					</div>
				</div>

				<div class="buttons">
					<a href="<?php the_permalink(); ?>" class="link-button">
						<span>Read more</span>
						<span class="zmdi zmdi-arrow-right"></span>
					</a>
				</div>

			</div>			

		<?php endwhile; endif; wp_reset_query(); ?>

		<div class="entry-content flex-7">
			<?php get_template_part('templates/page/content', 'title'); ?>
			<?php the_content(); ?>
		</div>

	</div>
	
	<?php 
		} 
		else {
			the_content();
		}
	?>
	
	<?php // For paginated pages using the <!--next--> quicktag
		wp_link_pages(array(
			'before' => '<nav class="navigation"><ol class="cwd-pagination wp_link_pages">' . __('<span class="title">Pages:</span>'),
			'after' => '</ol></nav>',
			'next_or_number' => 'next_and_number',
			'nextpagelink' => __('&raquo;', 'cwd_base'),
			'previouspagelink' => __('&laquo;', 'cwd_base'),
			'pagelink' => '%'
		));
	?>
</section>