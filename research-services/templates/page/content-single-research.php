<?php // The research single page content ?>

<section id="post-<?php the_ID(); ?>" <?php post_class('cwd-component'); ?>>

	<figure class="image align-left">
		<?php cwd_base_get_image(); ?>
		<?php cwd_base_get_image_caption(); ?>
	</figure>

	<div class="custom-fields">
		
		<div class="summary">
			<?php the_content(); ?>
		</div>
	
		<?php $faculty = get_field('faculty'); ?>

		<?php if($faculty) { ?>

			<p class="meta links">

				<span class="label">Faculty: </span>

				<?php $count = count($faculty); ?>

				<?php $i = 0; ?>

				<?php foreach($faculty as $person) :  ?>

					<?php $person_name = ucwords(str_replace('-', ' ', $person)); ?>

					<a href="<?php echo site_url('/people/').$person; ?>"><?php echo $person_name; ?></a>

					<?php $i++; ?>

					<?php if($i < $count) { echo ', '; } ?>

				<?php endforeach; wp_reset_postdata(); ?>

			</p>

		<?php } ?>

		<?php cwd_base_get_tag_options(); ?>

	</div>
	
	<?php // For paginated pages using the <!--nextpage--> quicktag
		wp_link_pages(array(
			'before' => '<nav class="navigation"><ol class="cwd-pagination wp_link_pages">' . __('<span class="title">Pages:</span>'),
			'after' => '</ol></nav>',
			'next_or_number' => 'next_and_number',
			'nextpagelink' => __('&raquo;', 'cwd_base'),
			'previouspagelink' => __('&laquo;', 'cwd_base'),
			'pagelink' => '%'
		));
	?>
	
</section>