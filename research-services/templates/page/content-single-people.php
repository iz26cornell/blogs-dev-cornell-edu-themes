<?php // The people content ?>

<section id="post-<?php the_ID(); ?>" <?php post_class('cwd-component'); ?>>

	<figure class="image align-left">
		<?php cwd_base_get_image(); ?>
		<?php cwd_base_get_image_caption(); ?>
	</figure>

	<?php if(get_field('professional_title') || get_field('department') || get_field('research_areas')) { ?>
	
		<div class="custom-fields" class="clear">
			
			<?php if(get_field('professional_title')) { ?>
			
				<div id="professional_title">
					
					<span class="label">Title: </span>
			
					<span class="field">
						<?php the_field('professional_title'); ?>
					</span>
					
				</div>
			
			<?php } ?>
			
			<?php if(get_field('department')) { ?>
			
				<div id="department">
					
					<span class="label">Department: </span>

					<span class="field">
						<?php the_field('department'); ?>
					</span>
			
				</div>
			
			<?php } ?>
			
		</div>
	
	<?php } ?>
	
	<div class="summary">
		<?php the_content(); ?>
	</div>
			
	<?php $research_areas = get_field('research_areas'); ?>

	<?php if($research_areas) { ?>

		<p class="meta links">

			<span class="label">Research Areas: </span>

			<?php $count = count($research_areas); ?>

			<?php $i = 0; ?>

			<?php foreach($research_areas as $research) :  ?>

				<?php $research_name = ucwords(str_replace('-', ' ', $research)); ?>

				<a href="<?php echo site_url('/research/').$research; ?>"><?php echo $research_name; ?></a>

				<?php $i++; ?>

				<?php if($i < $count) { echo ', '; } ?>

			<?php endforeach; wp_reset_postdata(); ?>

		</p>

	<?php } ?>
				
	<?php cwd_base_get_tag_options(); ?>

	<?php // For paginated pages using the <!--nextpage--> quicktag
		wp_link_pages(array(
			'before' => '<nav class="navigation"><ol class="cwd-pagination wp_link_pages">' . __('<span class="title">Pages:</span>'),
			'after' => '</ol></nav>',
			'next_or_number' => 'next_and_number',
			'nextpagelink' => __('&raquo;', 'cwd_base'),
			'previouspagelink' => __('&laquo;', 'cwd_base'),
			'pagelink' => '%'
		));
	?>
	
</section>