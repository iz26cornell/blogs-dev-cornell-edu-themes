<?php get_header(); ?>
<div id="wrap">
<div id="content">
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
  <div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h2><a href="<?php the_permalink() ?>" rel="bookmark">
      <?php the_title(); ?>
      </a></h2>
    <?php the_excerpt(__('Read more'));?>
    <!--

	<?php trackback_rdf(); ?>

	-->
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
  </div>
  
</div>

</div>
<?php get_footer(); ?>
