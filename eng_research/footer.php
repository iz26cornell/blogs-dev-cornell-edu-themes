<!-- begin footer -->

<hr />
<div id="footer">
  <!-- The footer-content div contains the Cornell University copyright -->
  <div id="footer-content">
    <p><a href="http://www.engineering.cornell.edu">College of Engineering</a></p>
    <ul class="admin">
      <li>
        <?php wp_loginout(); ?>
      </li>
      <?php wp_register(); ?>
      <li><span class="blog-login"><?php wp_footer();?></span></li>
    </ul>
    <p class="copyrights"><span>&copy;<?php echo date("Y");?> <a href="http://www.cornell.edu/">Cornell University</a></span></p>
    
  </div>
</div>

</body></html>