
<div id="skipnav">
	<a href="#content">Skip to main content</a>
</div>

<hr />

<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity">
	<div id="cu-logo">
		<a id="insignia-link" href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/unit_signature_white.gif" alt="Cornell University" /></a>
		<div id="unit-signature-links">
			<a id="cornell-link" href="http://www.cornell.edu/">Cornell University</a>
			<a id="unit-link" href="http://www.engineering.cornell.edu/">College of Engineering</a>
		</div>
	</div>
	<div id="search-form">
			<form action="http://www.engineering.cornell.edu/search.cfm" method="get" enctype="application/x-www-form-urlencoded">
			<div id="search-input">
				<label for="search-form-query">SEARCH:</label>
				<input type="text" id="search-form-query" name="q" value="" size="20" />
				<input type="submit" id="search-form-submit" name="btnG" value="go" />
				<input type="hidden" name="output" value="xml_no_dtd" />
				<input type="hidden" name="sort" value="date:D:L:d1" />

				<input type="hidden" name="ie" value="UTF-8" />
				<input type="hidden" name="gsa_client" value="default_frontend" />
				<input type="hidden" name="oe" value="UTF-8" />
				<input type="hidden" name="site" value="default_collection" />
				<input type="hidden" name="proxystylesheet" value="default_frontend" />
				<input type="hidden" name="proxyreload" value="1"/>
                
			</div>

			<div id="search-filters">
					<input type="radio" id="search-filters1" name="sitesearch" value="www.engineering.cornell.edu" checked="checked" />
					<label for="search-filters1">This Site</label>
				
					<input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
					<label for="search-filters2">Cornell</label>
					
					<a href="http://www.cornell.edu/search/">more options</a>
			</div>	
		</form>

	</div>
</div>

<hr />