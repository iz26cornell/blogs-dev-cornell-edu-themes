<?php get_header(); ?>
<?php get_sidebar(); ?>

	<div id="main-body">

			<header class="page-header">
				<h1 class="page-title"><?php _e( 'Not found', 'nanooze' ); ?></h1>
			</header>

			<div class="page-wrapper">
				<div class="page-content">
					<h3><?php _e( 'It looks like nothing was found at this location...', 'nanooze' ); ?></h3>
					<h4 style="text-align:center;padding-right:30px;"><strong><?php _e( 'You are being redirected to the home page...', 'nanooze' ); ?></strong></h4>
				</div>
			</div>

	</div>
    
<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>