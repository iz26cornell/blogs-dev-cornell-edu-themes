<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
	<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<?php if (is_404()) { ?><meta http-equiv="refresh" content="5;URL=<?php echo site_url(); ?>" /><?php } ?>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/styles/ie.css" />
	<![endif]-->
  
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic|Rokkitt:400,700|Orbitron:500" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" media="print" href="<?php echo get_template_directory_uri(); ?>/styles/print.css" />
	<?php if (is_page('Blog')) { header( 'Location: ' . site_url() . '/articles/blog'); } ?>
	<?php if (is_page('Meet a Scientist')) { header( 'Location: ' . site_url() . '/articles/meet-a-scientist'); } ?>
	<?php if (is_page('Downloads')) { header( 'Location: ' . site_url() . '/articles/downloads'); } ?>
	<?php if (is_category('Articles')) { header( 'Location: ' . site_url() . '/articles'); } ?>

	<?php wp_head(); ?>

	<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/modernizr.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/framework/iws.js"></script>

</head>

<body <?php body_class($class); ?>>
<div class="flexible sidebar-tint">
<div id="skipnav"><a href="#content">Skip to main content</a></div>
<div id="wrap" class="home">
	<div id="header">
		<div id="header-band">
			<div id="identity">
				<div id="language">
					<label for="language-select">Language</label>
					<select id="language-select" name="language-select" onchange="location=this.options[this.selectedIndex].value;">
						<option selected value="http://blogs.cornell.edu/nanooze/">English</option>
						<option value="http://blogs.cornell.edu/nanoozeespanol/">Spanish</option>
						<option value="http://blogs.cornell.edu/nanoozeportuguese/">Portuguese</option>
					</select>
				</div>
				<h1 id="main-logo"><a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/images/project/nanooze_logo800.png" alt="Nanooze Magazine" /></a></h1>
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Site Tagline') ) : ?><?php endif; ?>
                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Main Nav') ) : ?><?php endif; ?>
			</div>
		</div>
	</div>

	<div id="midband-wrap" class="">
		<div id="midband"></div>
	</div>

	<div id="content-wrap">
    <div id="content">
		<div id="main">
			<div id="main-top"></div>