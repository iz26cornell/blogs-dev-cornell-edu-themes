<?php get_header(); ?>
<?php get_sidebar(); ?>

	<div id="main-body" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
				<h1 class="archive-title"><?php
					if ( is_day() ) :
						printf( __( 'Daily Archives: %s', 'nanooze' ), get_the_date() );
					elseif ( is_month() ) :
						printf( __( '%s', 'nanooze' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'nanooze' ) ) );
					elseif ( is_year() ) :
						printf( __( 'Posts from: %s', 'nanooze' ), get_the_date( _x( 'Y', 'yearly archives date format', 'nanooze' ) ) );
					else :
						_e( 'Archives', 'nanooze' );
					endif;
				?></h1>
			</header><!-- .archive-header -->
            
			<?php /* The loop */ ?>
			<?php $month = get_the_date('m'); $year = get_the_date('Y'); ?>
       		<?php //if(is_month()) query_posts( "cat=23&year=$year&monthnum=$month&order=ASC" ); ?><!-- for dev site -->
       		<?php //if(is_year()) query_posts( "cat=23&year=$year&order=ASC" ); ?>
       		<?php if(is_month()) query_posts( "cat=479&year=$year&monthnum=$month&order=ASC" ); ?><!-- for edublogs -->
       		<?php if(is_year()) query_posts( "cat=479&year=$year&order=ASC" ); ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php nanooze_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div>
	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>