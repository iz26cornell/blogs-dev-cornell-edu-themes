
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php if ( is_single() ) : ?>
			<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
            <h2 class="entry-title">
                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
            </h2>
		<?php endif; // is_single() ?>
		<?php if ( !is_category('Downloads') && !is_category('Meet a Scientist') && !is_category('Articles') && !in_category('Meet a Scientist') && !in_category('Articles') && !in_category('Downloads') ) : ?>
            <div id="entry-meta">
                <?php nanooze_entry_meta(); ?>
            </div>
		<?php endif; ?>
	</header><!-- .entry-header -->

 		<?php if ( has_post_thumbnail() && is_single() && in_category('Meet a Scientist') ) { ?>
		<div class="archive-meta">
				<?php the_post_thumbnail('full', array('class' => 'category-description-img')); ?>
                <p class="category-description"><?php the_post_thumbnail_caption(); ?></p>
                <hr class="thumb_hr">
		</div>
		<?php } ?>
		
		<?php if ( has_post_thumbnail() && is_single() && !in_category('Meet a Scientist') && has_post_thumbnail_caption() ) { ?>
				<?php $image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); $image_width = $image_data[1] + 6; ?>
                <div class="wp-caption alignleft entry-thumbnail" style="width:<?php echo $image_width; ?>px;padding:3px;"><?php the_post_thumbnail('full',array('class' => 'featured-img')); ?>
                <p class="wp-caption-text"><?php the_post_thumbnail_caption(); ?></p></div>
		<?php } ?>

		<?php if ( has_post_thumbnail() && is_single() && !in_category('Meet a Scientist') && !has_post_thumbnail_caption() ) { ?>
		<div class="entry-thumbnail">
			<?php the_post_thumbnail('full'); ?>
		</div>
		<?php } ?>


		<?php if ( is_search() || is_archive() || is_home() ) : ?>
        <div class="entry-summary">
            <?php the_excerpt_rereloaded(45, 'Read More...', '<a>,<i>,<em>,<span>', 'span', 'no'); ?>
        </div><!-- .entry-summary -->
        <?php else : ?>

	<div class="entry-content">
		<?php the_content( __( 'Read more <span class="meta-nav">&rarr;</span>', 'nanooze' ) ); ?>
        <?php if(in_category('Blog')) nanooze_tag_meta(); ?>
        <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'nanooze' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-meta">
		<?php if ( comments_open() && ! is_single() ) : ?>
			<div class="comments-link">
				<?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a comment', 'nanooze' ) . '</span>', __( 'One comment so far', 'nanooze' ), __( 'View all % comments', 'nanooze' ) ); ?>
			</div><!-- .comments-link -->
		<?php endif; // comments_open() ?>

		<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
			<?php get_template_part( 'author-bio' ); ?>
		<?php endif; ?>
	</footer><!-- .entry-meta -->
    
</article><!-- #post -->
