<?php get_header(); ?>
<?php get_sidebar(); ?>
           
	<div id="main-body" class="content-area">
		<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<?php if ( is_category( 'Blog' ) ) :  
                
                 	  echo '<h1 class="archive-title">Nanooze Blog</h1>';  
				 
				 	  elseif ( is_category( 'Meet a Scientist' ) ) :  
                              
                 	  echo '<h1 class="archive-title">Meet a Scientist</h1>';  
                    
				 	  elseif ( is_category( 'Downloads' ) ) :  
                              
                 	  echo '<h1 class="archive-title">Downloads</h1>';  
                    
				 	  else : ?>
                                       
                	<h1 class="archive-title"><?php printf( __( 'Articles: %s', 'nanooze' ), single_cat_title( '', false ) ); ?></h1>

				<?php endif; ?>
                
				<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
				<?php endif; ?>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
            	<?php $id = getCurrentCatID(); ?>
            	<?php //if ( is_category('Meet a Scientist') || cat_is_ancestor_of(17,$id) ) { ?>
            	<?php if (is_category('Meet a Scientist') || cat_is_ancestor_of(267497,$id) ) { ?><!--for edublogs-->
					<?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) the_post_thumbnail(array(150,150)); } else { ?>
					<?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) the_post_thumbnail(); } ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php nanooze_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>