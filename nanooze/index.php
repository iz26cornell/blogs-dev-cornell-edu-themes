<?php get_header(); ?>
<?php get_sidebar(); ?>

 	<div id="main-body">

        <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><a href="https://iws-lamp-dev.hosting.cornell.edu/wordpress/nanooze/" rel="v:url" property="v:title">Home</a> <span class="rarr">&rarr;</span> <span class="current">The Nanooze Blog</span></div>
        
               
		<?php query_posts( 'cat=19' ); ?>
        
		<?php if ( have_posts() ) : ?>
        <h1 class="entry-title">Nanooze Blog</h1>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php nanooze_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>