function iws_load() {
    $.ajaxSetup({
        async: false
    });
    for (i = 0; i < iws_include.length; i++) {
        $.getScript(js_path + iws_include[i])
    }
}

function iws_init() {
    function e() {
        if ($(window).width() >= 741) {
            $("#navigation ul").removeAttr("style");
            $("#navigation h3, #navigation h3 em").removeClass("open")
        }
    }
    iws_load();
    $(document).ready(function() {
        popups();
        tooltips(100);
        expander();
        $(window).resize(e);
        e();
        var t = $("#navigation h3 em").offset();
        $("#navigation").find("ul.menu").first().addClass("mobile-menu");
        $("#navigation h3 em").addClass("mobile-menu");
        $("#navigation h3 em").click(function() {
            $(this).toggleClass("open");
            $(this).parents("h3").first().toggleClass("open");
            $(this).parent().parent().find("ul.menu").slideToggle(200)
        });
        $("#language-select").bind("change", function() {
            var e = $(this).val();
            if (e) {
                window.location = e
            }
        })
    })
}

function resizeChecks() {
    if (jQuery(window).width() >= 741) {
        jQuery("#navigation ul").removeAttr("style");
        jQuery("#navigation h3, #navigation h3 em").removeClass("open")
    }
}

if (js_path == undefined) {
    var js_path = "js/framework/"
}

var iws_include = ["iws_popup.js", "iws_tooltips.js", "iws_expander.js"];

jQuery(document).ready(function() {
    jQuery(window).resize(resizeChecks);
	
    resizeChecks();
	
    jQuery("#navigation h3 em").click(function() {
        jQuery("#navigation h3").toggleClass("open");
        jQuery("#navigation ul.menu").slideToggle(200)
    })

    jQuery('.home #col2 .featured-post').prepend('<h2>What\'s <span class="sticker-new"><span>New</span></span></h2>');
	jQuery('.home #col2 .featured-post').attr('id', 'whatsnew');
	jQuery('.home #col2 .featured-posts').removeClass('featured-posts');
	jQuery('.home #col2 #whatsnew').removeClass('featured-post');
	jQuery('.home #col2 #whatsnew a img').removeClass('alignleft wp-post-image');
	jQuery('.home #col2 #whatsnew a img').addClass('dropshadow');

})
