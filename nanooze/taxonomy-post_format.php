<?php get_header(); ?>

	<div id="main-body">

		<?php if ( have_posts() ) : ?>
        
			<header class="archive-header">
				<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                <h1 class="archive-title"><?php printf( __( '%s Archives', 'nanooze' ), '<span>' . get_post_format_string( get_post_format() ) . '</span>' ); ?></h1>
			</header>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php nanooze_paging_nav(); ?>

			<?php else : ?>
                <?php get_template_part( 'content', 'none' ); ?>
            <?php endif; ?>

	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>