<?php
/**
 * @package WordPress
 * @subpackage Cornell
 * @since Cornell 1.0
 */

get_header(); ?>

		<div id="content-container">
			<div id="content" role="main">
			
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="nav-above" class="navigation">
					<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'Cornell' ) . '</span> %title' ); ?></div>
					<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'Cornell' ) . '</span>' ); ?></div>
				</div><!-- #nav-above -->

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1 class="entry-title"><?php the_title(); ?></h1>

					<div class="entry-meta">
						<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'Cornell' ), __( '1 Comment', 'Cornell' ), __( '% Comments', 'Cornell' ) ); ?></span>
						<?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="meta-sep">|</span> <span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-meta -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->

					<div class="entry-info">
						<?php Cornell_posted_in(); ?>
					</div><!-- .entry-info -->
				</div><!-- #post-## -->

				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'Cornell' ) . '</span> %title' ); ?></div>
					<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'Cornell' ) . '</span>' ); ?></div>
				</div><!-- #nav-below -->

				<?php comments_template( '', true ); ?>

			<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->
		</div><!-- #content-container -->

<?php get_sidebar(); ?>
	</div><!-- #content-box -->
</div><!-- #container -->
</div></div><!-- #wrap -->
<?php get_footer(); ?>