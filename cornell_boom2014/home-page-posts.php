<?php
/**
 * Template Name: Homepage with Posts
 * @package WordPress
 * @subpackage Cornell
 */

get_header(); ?>
		<div id="content-container">
			<div id="content" role="main">
            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( is_front_page() ) { ?>
						<!--<h2 class="entry-title"><?php the_title(); ?></h2>-->
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php } ?>

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

				<?php /*?> <?php comments_template( '', true ); ?> <?php */?>

			<?php endwhile; ?>
			
			<?php ?><?php /*query_posts('category_name=home_featured&showposts=3'); */?><?php ?>
            <?php query_posts('cat=31090&showposts=6'); ?>
            <h2 class="latest-title">Latest Posts</h2>

			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
					<!--<?php printf( __( ' %2$s ', 'Cornell' ),
						'meta-prep meta-prep-author',
						sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
							get_permalink(),
							esc_attr( get_the_time() ),
							get_the_date()
						)
					); ?>-->
                    <div class="entry-date"><span class="month"><?php the_time('M') ?></span><span class="day"><?php the_time('d') ?></span></div>
					<?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'Cornell' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
					<?php } ?>
					
                    <div class="post-meta">
					<?php if ( count( get_the_category() ) ) : ?>
                        Posted by <?php the_author_posts_link(); ?><span class="cat-links">
                            <?php printf( __( '<span class="%1$s">in</span> %2$s', 'Cornell' ), 'entry-info-prep entry-info-prep-cat-links', get_the_category_list( ', ' ) ); ?>
                        </span>
                    <?php endif; ?>
                    <span class="comments-link"><?php comments_popup_link( __( 'Comment', 'Cornell' ), __( '1 Comment', 'Cornell' ), __( '% Comments', 'Cornell' ) ); ?></span>
                    </div>
                    
                    
                    <div class="entry-content">
						<?php the_excerpt(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

				<?php /*?> <?php comments_template( '', true ); ?> <?php */?>

			<?php endwhile; ?>

			</div><!-- #content -->
		</div><!-- #content-container -->
			<?php if ( is_active_sidebar( 'fourth-widget-area' ) ) : ?>
				<div id="home-content-widgets" class="widget-area">
					<ul class="widget-list">
						<?php dynamic_sidebar( 'fourth-widget-area' ); ?>
					</ul>
				</div><!-- #fourth .widget-area -->
			<?php endif; ?>
      
	</div><!-- #content-box -->
</div><!-- #container -->
</div></div><!-- #wrap -->
<div id="home-extra-wrap">
  <div id="content-secondary">
    <ul class="widget-list1">
      <?php // A first featured sidebar for widgets. Cornell uses the secondary widget area for three column layouts.
			if ( ! dynamic_sidebar( 'secondary-widget-area1' ) ) : ?>
      <?php endif; ?>
    </ul>
    <ul class="widget-list2">
      <?php // A second sidebar for widgets. Cornell uses the secondary widget area for three column layouts.
			if ( ! dynamic_sidebar( 'secondary-widget-area2' ) ) : ?>
      <?php endif; ?>
    </ul>
  </div>
</div>
<!-- #extra widget area -->
<?php get_footer(); ?>