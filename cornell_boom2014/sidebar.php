<?php
/**
 * @package WordPress
 * @subpackage Cornell
 */
?>

		<?php
			/* If the current layout is a 3-column one with 2 sidebars on the right or left
			 * Cornell enables a "Feature Widget Area" that should span both sidebar columns
			 * and adds a containing div around the main sidebars for the content-sidebar-sidebar
			 * and sidebar-sidebar-layouts so the layout holds together with a short content area and long featured widget area
			 */
			$options = Cornell_get_theme_options();
			$current_layout = $options['theme_layout'];
			$feature_widget_area_layouts = array( 'content-sidebar-sidebar', 'sidebar-sidebar-content' );

			if ( in_array( $current_layout, $feature_widget_area_layouts ) ) :
		?>
		<div id="main-sidebars">
		<?php endif; // ends the check for the current layout that determines the #main-sidebars markup ?>

		<div id="primary" class="widget-area" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
			<ul class="widget-list">
			
			<?php // The primary sidebar used in all layouts
			if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

				<li id="search" class="widget-container widget_search">
					<h3 class="widget-title"><?php _e( 'Search It!', 'Cornell' ); ?></h3>
					<?php get_search_form(); ?>
				</li>

				<li class="widget-container">
					<h3 class="widget-title"><?php _e( 'Recent Entries', 'Cornell' ); ?></h3>
						<ul>
							<?php
							$recent_entries = new WP_Query();
							$recent_entries->query( 'order=DESC&posts_per_page=10' );

							while ($recent_entries->have_posts()) : $recent_entries->the_post();
								?>
								<li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
								<?php
							endwhile;
							?>
						</ul>
				</li>

				<li class="widget-container">
					<h3 class="widget-title"><?php _e( 'Links', 'Cornell' ); ?></h3>
						<ul>
							<?php wp_list_bookmarks( array( 'title_li' => '', 'categorize' => 0 ) ); ?>
						</ul>
				</li>

			<?php endif; // end primary widget area ?>
			
			
			<?php // Related Links (ama39 2/29/12) -----------------------------
				global $post_id;				
				if ( get_post_meta($post_id, 'Related Links', false) ) : // if one or more tags are present
					$mypages = array();
					$linkcount = 0;
					$maxlinks = 6;
					foreach( get_post_meta($post_id, 'Related Links', false) as $tag ) { // for each tag
						foreach( get_pages( array('meta_value' => $tag ) ) as $page_found ) { // add each page
							array_push($mypages, $page_found);
						}
					}
					if ( count($mypages) > 1 ) { // if more than one page is found (the first is always the current page), list them
				?>
				<li class="widget-container related-links">
					<h3 class="widget-title">Related Links</h3>
					<ul>
				<?php foreach( $mypages as $page ) { // list each page
					if ($page->ID != $post_id && $linkcount < $maxlinks) { // omit the current page ?>
						<li><a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a></li>
				<?php $linkcount++;
					} // end if
				} // end if ?>
					</ul>
				</li>
			
			<?php
				} // end if
				endif; // ------------------------------------------------------ ?>
			
			</ul>
			

		</div><!-- #primary .widget-area -->

		<?php
			// add a containing div around the main sidebars for the content-sidebar-sidebar and sidebar-sidebar-layouts
			// so the layout holds together with a short content area and long featured widget area
			if ( in_array( $current_layout, $feature_widget_area_layouts ) )
				echo '</div><!-- #main-sidebars -->';
		?>
