<?php get_header(); ?>
<div id="wrap">
<div id="content">
  <div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h2><a href="<?php the_permalink() ?>" rel="bookmark">
      <?php the_title(); ?>
      </a></h2>
      <?php the_content(__('Read more'));?>
    <div class="postmeta">
      <div class="postmetaleft">
        <p>
          <?php the_time('F j, Y'); ?>
          | category:
          <?php the_category(', ') ?>
        </p>
      </div>
      <div class="postmetaright">
        <p><?php if ('open' == $post->comment_status) : ?><?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?> <?php else : ?>&nbsp;
<?php endif; ?><span><?php edit_post_link('edit', '', ''); ?></span></p>
      </div>
    </div>
    <!--

	<?php trackback_rdf(); ?>

	-->
    <h2>Comments</h2>
    <?php comments_template(); // Get wp-comments.php template ?>
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
  </div>
  <?php include(TEMPLATEPATH."/r_sidebar.php");?>
</div>
<!-- The main column ends  -->
</div>
<?php get_footer(); ?>