<?php get_header(); ?>
<?php get_sidebar(); ?>

	<div id="main-body" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
				<h1 class="archive-title"><?php
					if ( is_day() ) :
						printf( __( 'Archivos Diarios: %s', 'nanooze' ), get_the_date() );
					elseif ( is_month() ) :
						printf( __( '%s', 'nanooze' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'nanooze' ) ) );
					elseif ( is_year() ) :
						printf( __( 'Mensajes de: %s', 'nanooze' ), get_the_date( _x( 'Y', 'yearly archives date format', 'nanooze' ) ) );
					else :
						_e( 'Archivos', 'nanooze' );
					endif;
				?></h1>
			</header><!-- .archive-header -->
            
       		<?php query_posts( array ( 'category_name' => 'blog', 'posts_per_page' => 20 ) ); ?>
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php nanooze_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div>
	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>