<?php get_header(); ?>
<?php get_sidebar(); ?>

	<div id="main-body">

			<header class="page-header">
				<h1 class="page-title"><?php _e( 'No se ha encontrado', 'nanooze' ); ?></h1>
			</header>

			<div class="page-wrapper">
				<div class="page-content">
					<h3><?php _e( 'Parece que no se encontr� nada en este lugar...', 'nanooze' ); ?></h3>
					<h4 style="text-align:center;padding-right:30px;"><strong><?php _e( 'Usted est� siendo redirigido a la p�gina de inicio...', 'nanooze' ); ?></strong></h4>
				</div>
			</div>

	</div>
    
<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>