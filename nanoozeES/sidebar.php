
<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>

	<div id="secondary-nav">

		<div id="section-navigation">

			<?php dynamic_sidebar( 'sidebar-1' ); ?>

		</div>

	</div>

<?php endif; ?>