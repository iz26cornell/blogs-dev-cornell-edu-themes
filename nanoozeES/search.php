<?php get_header(); ?>
<?php get_sidebar(); ?>

	<div id="main-body">

		 <?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                <h1 class="page-title"><?php printf( __( 'Resultados de la busqueda para: %s', 'nanooze' ), get_search_query() ); ?></h1>
			</header>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
					<?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) the_post_thumbnail(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php nanooze_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>