<?php get_header(); ?>
<?php get_sidebar(); ?>
           
	<div id="main-body" class="content-area">

		<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
		<?php if ( have_posts() ) : ?>
			<header class="archive-header">                                     
                <h1 class="archive-title">Nanooze Blog</h1>                
				<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
				<?php endif; ?>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<header class="entry-header">
                    <h2 class="entry-title">
                        <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                    </h2>
                    <div id="entry-meta">
                        <?php nanooze_entry_meta(); ?>
                    </div>
				</header><!-- .entry-header -->
				<?php the_content(); ?>
				<?php nanooze_tag_meta(); ?>
			<?php endwhile; ?>

			<?php nanooze_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

	</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>