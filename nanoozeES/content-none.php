<header class="page-header">
    <h1 class="page-title"><?php _e( 'Nada Encontrado', 'nanooze' ); ?></h1>
</header>

<div class="page-content">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

	<p><?php printf( __( 'Listo para publicar tu primer post? <a href="%1$s">Comience aqu�</a>.', 'nanooze' ), admin_url( 'post-new.php' ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

	<p><?php _e( 'Lo sentimos, pero no hay nada emparejado los t�rminos de b�squeda. Por favor, int�ntelo de nuevo con diferentes palabras clave.', 'nanooze' ); ?></p>
	<?php get_search_form(); ?>

	<?php elseif ( is_archive() ) : ?>

	<p><?php _e( 'Lo sentimos, no hay entradas en esta categoria.', 'nanooze' ); ?></p>

	<?php else : ?>

	<p><?php _e( 'Parece que no podemos encontrar lo que est�s buscando. Puede que la b�squeda puede ayudar.', 'nanooze' ); ?></p>
	<?php get_search_form(); ?>

	<?php endif; ?>
</div><!-- .page-content -->
