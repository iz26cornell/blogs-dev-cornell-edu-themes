<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<?php 

do_action( 'genesis_doctype' );
if ( is_front_page() ) {   //for edublogs theme
	echo '<title>' . get_bloginfo('name') . ' &mdash; ' . get_bloginfo('description') . '</title>';
}
else {
	do_action( 'genesis_title' );
}	
do_action( 'genesis_meta' );

/** search form configuration **/
	if (isset($_GET['btnG'])) {
		session_start();
		$selected_radio = $_GET['sitesearch'];
		
		if ($selected_radio == 'cornell') {
			$search_terms = urlencode($_GET['s']);
			$URL="http://www.cornell.edu/search" . "?q=" . $search_terms . "&client=default_frontend&proxystylesheet=default_frontend";
			print $URL;
			header ("Location: $URL");
		}
	}

/** we need this for plugins **/
wp_head(); 

?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/iws.js"></script>
<!-- Initialize -->
<script type="text/javascript">
	iws_init();
</script>

</head>
<body <?php body_class(); ?>>
<div id="skipnav"><a href="#content">Skip to main content</a></div>
<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity">
  <div class="cu-identity-wrap">
    <div id="cu-logo">
      <a href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/cu_logo_white45.gif" alt="Cornell University" id="cu_logo_white45" width="180" height="45" border="0" /></a>
    </div>
    
    <div id="cu-search">
      	<div id="mobile-nav"></div>
        <div id="search_icon">
        </div>
         <div id="search-form">
			<form method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" >
                <div id="search-filters">
                        <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
                        <label for="search-filters1">This Site</label>
                    
                        <input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
                        <label for="search-filters2">Cornell</label>
                </div>	
                <div id="search-input">
                    <input type="text" value="" name="s" id="search-form-query" size="26" />
                    <input type="submit" id="search-form-submit" name="btnG" value="go" />
                </div>              
            </form>
		 </div>
     </div>
     
  </div>
</div><!-- end identity -->

<?php
do_action( 'genesis_before' );
?>
<div id="wrap">
<?php
do_action( 'genesis_before_header' );
do_action( 'genesis_header' );
do_action( 'genesis_after_header' );

echo '<div id="inner">';
genesis_structural_wrap( 'inner' );
