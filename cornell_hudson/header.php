<?php
/**
 * The Header
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Cryout Creations
 * @subpackage parabola
 * @since parabola 0.5
 */
 ?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php  cryout_meta_hook(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/styles/style-mobile.css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/framework/iws.js"></script>

<!-- Initialize -->
<script type="text/javascript">
    var js_path = "<?php echo get_template_directory_uri(); ?>/js/framework/";
    iws_init();
</script>

<?php
 	cryout_header_hook();
	
	/** search form configuration **/
	if (isset($_GET['btnG'])) {
		session_start();
		$selected_radio = $_GET['sitesearch'];
		
		if ($selected_radio == 'cornell') {
			$search_terms = urlencode($_GET['s']);
			$URL="http://www.cornell.edu/search" . "?q=" . $search_terms . "&client=default_frontend&proxystylesheet=default_frontend";
			print $URL;
			header ("Location: $URL");
		}
	}
	
	wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="skipnav"><a href="#content">Skip to main content</a></div>
<div id="cu-search">
  <div class="cu-search-wrap">
     <div id="cu-search-form">
        <form method="get" id="cu-searchform" action="<?php echo home_url( '/' ); ?>" >
            <div id="cu-search-input">
                <label for="search-form-query">Search:</label>
                <input type="text" value="" name="s" id="cu-search-form-query" size="26" />
                <input type="submit" id="cu-search-form-submit" name="btnG" value="go" />
            </div>              
            <div id="search-filters">
                    <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
                    <label for="search-filters1">This Site</label>
                
                    <input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
                    <label for="search-filters2">Cornell</label>
            </div>	
        </form>
     </div>
  </div>
</div>
<div id="cu-identity">
  <div class="cu-identity-wrap">
    <div id="cu-logo">
      <a class="cals_logo" href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/CU-Insignia-White-120.jpg" alt="Cornell University" id="desktop_logo" width="120" height="120" border="0" /><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/theme_gray65.jpg" alt="Cornell University" id="mobile_logo" width="120" height="120" border="0" /></a>
    </div>
    <div id="search-button">
        <p><a href="#"></a></p>
    </div>
    <h1><a href="http://cals.cornell.edu/">College of Agriculture and Life Sciences</a></h1>
    <h2><a href="http://www.dnr.cornell.edu/">Department of Natural Resources</a></h2>
  </div>
</div>

<?php cryout_body_hook(); ?>

<div id="wrapper" class="hfeed">

<?php cryout_wrapper_hook(); ?>

<div id="header-full">

<header id="header">

<?php cryout_masthead_hook(); ?>

		<div id="masthead">

			<div id="branding" role="banner" >

				<?php cryout_branding_hook();?>
				<div style="clear:both;"></div>

			</div><!-- #branding -->

			<nav id="access" role="navigation">

				<?php cryout_access_hook();?>

			</nav><!-- #access -->

		</div><!-- #masthead -->

	<div style="clear:both;height:1px;width:1px;"> </div>

</header><!-- #header -->
</div><!-- #header-full -->
<div id="main">
	<div  id="forbottom" >
		<?php cryout_forbottom_hook(); ?>

		<div style="clear:both;"> </div>

		<?php cryout_breadcrumbs_hook();?>
