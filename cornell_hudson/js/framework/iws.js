/* IWS Dynamic Components
   ------------------------------------------- */  

if (js_path == undefined) {
	var js_path = "js/framework/";
}

var iws_include = ["iws_popup.js",
						 "iws_tooltips.js",
						 "iws_expander.js"];

/* load resources */
function iws_load() {
	$.ajaxSetup({async: false});
	for (i=0;i<iws_include.length;i++) {
		$.getScript(js_path+iws_include[i]);
	}
}

/* initialize components */
function iws_init() {
	//iws_load();
	
	// Headline Autoscale Variables
	var base_size;
	var base_width;
	var max_size = 72;
	var min_window_size = 0;
	
	// Window Size Tracking
	function resizeChecks() {
		
		// Restore Standard Main Navigation
		if ($(window).width() >= 741) {
			$('#navigation ul').removeAttr('style');
			$('#navigation h3 em').removeClass('open');
		}
		
		// Refresh Headline Autoscale 
		if ($(window).width() > min_window_size) {
			$('.autosize-header .home #identity-content h1').addClass('autoscale');
			var multiplier = $('#identity-content').width() / base_width;
			if (multiplier > 0) {
				var new_size = base_size * multiplier;
				if (new_size > max_size) {
					new_size = max_size;
				}
				$('.autosize-header .home #identity-content h1').css('font-size',new_size+'px');
			}
		}
		else {
			$('.autosize-header .home #identity-content h1').removeAttr('style');
			$('.autosize-header .home #identity-content h1').removeClass('autoscale');
		}
	}
	
	$(window).load(function() {
		
		// Reinitialize Headline Autoscale (after remote webfonts load)
		$('.autosize-header .home #identity-content h1').removeAttr('style');
		base_width = $('.home #identity-content h1 span').width();
		resizeChecks();
		
		// Single-line News Headlines (move date to line two)
		$('.home #news h3').each(function( index ) {
  			if ($(this).next('h4.date').position().top - $(this).position().top < 8) {
  				$(this).find('a').append('<br />');
  			}
		});
	});
	
	$(document).ready(function() {
		//popups();
		//tooltips(100);
		//expander();
		
		
		// Homepage Headline Autoscale
		base_size = parseInt($('.home #identity-content h1').css('font-size'));
		base_width = $('.home #identity-content h1 span').width();
		$(window).resize(resizeChecks);
		resizeChecks();		
		
		
		// Mobile Navigation
		var nav_offset = $('#navigation h3 em').offset();
		
		$('#navigation').find('ul.menu').first().addClass('mobile-menu'); // standard
		//$('#navigation h3').next('div').find('ul.menu').first().addClass('mobile-menu'); // drupal
		
		$('#navigation h3 em').addClass('mobile-menu');
		$('#navigation h3 em').click(function() {
			$(this).toggleClass('open');
			
			$(this).parent().parent().find('ul.menu').slideToggle(200); // standard
			//$(this).parent().next('div').find('ul.menu').slideToggle(200); //drupal
		});
		
		// Search
		$('#search-button').click(function (e) {
			e.preventDefault();
			$(this).toggleClass('open');
			$('#cu-search').slideToggle(200);
		});
		
		// Justified Navigation
		var nav_count = $('.nav-centered #navigation ul.menu').first().find('li').length;
		if (nav_count > 0) {
			var nav_width = 100 / nav_count;
			$('.nav-centered #navigation ul.menu').first().find('li').css( 'width',nav_width+'%');
		}
		
	});
}