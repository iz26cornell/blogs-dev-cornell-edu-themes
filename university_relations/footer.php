
<div class="site-footer">


  <?php if ( is_active_sidebar( 'sidebar-8' ) ) : ?>
  <div id="footer">
    <div class="footer-content">
      <div class="columns">
	      <?php dynamic_sidebar( 'sidebar-8' ); ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <div id="subfooter">
    <div class="footer-content">
      <div class="columns">
        <div class="col-item">
          <p><?php echo get_theme_mod( 'copyright_textbox', 'No copyright information has been saved yet.' ); ?></p>
        </div>
        <div class="col-item">
          <ul class="social-links">
            <?php 
				if ( get_theme_mod( 'facebook' ) ) {  
            		echo '<li class="facebook"> <a href="' . esc_url( get_theme_mod( 'facebook' ) ) . '"> <i class="fa fa-fw fa-facebook"> <span>Facebook</span> </i> </a> </li>'; }
				if ( get_theme_mod( 'twitter' ) ) {  
            		echo '<li class="twitter"> <a href="' . esc_url( get_theme_mod( 'twitter' ) ) . '"> <i class="fa fa-fw fa-twitter"> <span>Twitter</span> </i> </a> </li>'; }
				if ( get_theme_mod( 'google_plus' ) ) {  
            		echo '<li class="google_plus"> <a href="' . esc_url( get_theme_mod( 'google_plus' ) ) . '"> <i class="fa fa-fw fa-google-plus"> <span>Google Plus</span> </i> </a> </li>'; }
				if ( get_theme_mod( 'pinterest' ) ) {  
            		echo '<li class="pinterest"> <a href="' . esc_url( get_theme_mod( 'pinterest' ) ) . '"> <i class="fa fa-fw fa-pinterest"> <span>Pinterest</span> </i> </a> </li>'; }
				if ( get_theme_mod( 'tumblr' ) ) {  
            		echo '<li class="tumblr"> <a href="' . esc_url( get_theme_mod( 'tumblr' ) ) . '"> <i class="fa fa-fw fa-tumblr"> <span>Tumblr</span> </i> </a> </li>'; }
				if ( get_theme_mod( 'flickr' ) ) {  
            		echo '<li class="flickr"> <a href="' . esc_url( get_theme_mod( 'flickr' ) ) . '"> <i class="fa fa-fw fa-flickr"> <span>Flickr</span> </i> </a> </li>'; }
				if ( get_theme_mod( 'linkedin' ) ) {  
            		echo '<li class="linkedin"> <a href="' . esc_url( get_theme_mod( 'linkedin' ) ) . '"> <i class="fa fa-fw fa-linkedin"> <span>Linked In</span> </i> </a> </li>'; }
				if ( get_theme_mod( 'instagram' ) ) {  
            		echo '<li class="instagram"> <a href="' . esc_url( get_theme_mod( 'instagram' ) ) . '"> <i class="fa fa-fw fa-instagram"> <span>Instagram</span> </i> </a> </li>'; }
				if ( get_theme_mod( 'youtube' ) ) {  
            		echo '<li class="youtube"> <a href="' . esc_url( get_theme_mod( 'youtube' ) ) . '"> <i class="fa fa-fw fa-youtube"> <span>YouTube</span> </i> </a> </li>'; }
				if ( get_theme_mod( 'vimeo' ) ) {  
            		echo '<li class="vimeo"> <a href="' . esc_url( get_theme_mod( 'vimeo' ) ) . '"> <i class="fa fa-fw fa-vimeo-square"> <span>Vimeo</span> </i> </a> </li>'; }
			  ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

	<?php wp_footer(); ?>
</body>
</html>
