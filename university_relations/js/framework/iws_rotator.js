// user settings
var default_div = '#wrap'; // default background container
var default_caption_div = '#campaign-feature'; // default caption container
var image_path = 'images/rotator/'; // path to images (TODO: should be defined by the server)
var slide_time = 8; // time between transitions (in seconds)
var transition_speed = 1; // speed of cross-fade (in seconds)
var bg_color = '#2e2e2e'; // basic fill color behind images (may be visible briefly during page load)
var caption_height = '21em'; // must be enough height to accomodate the tallest caption text
var autoplay = true; // if true, the rotator will cycle through images on load (but will stop after user interaction)
var random_start = true; // if true, the rotator will start on a random slide (instead of always starting at 1)


// TODO: replace this with external data
var image_array = [['garcia.jpg','Ephrahim Garcia','College of Engineering','<strong>Breaking rules</strong> to <em>push <br />the limits</em> of <strong>imagination.</strong>','/brand/independent/faculty/garcia.cfm'],
                   ['baker.jpg','Jen Baker','&rsquo;12 MBA &rsquo;13 M.Eng.','<strong>Breaking rules</strong> to <br /><em>create new</em> <br /><strong>career paths.</strong>','/brand/independent/alumni/jen_baker.cfm'],
                   ['weber-shirk.jpg','Monroe Weber-Shirk','Senior Lecturer, Civil and Environmental Engineering','<strong>Breaking rules</strong> to <em>make <br />the world</em> a <strong>better place.</strong>','/brand/independent/faculty/monroe_weber_shirk.cfm'],
                   ['calderon.jpg','Brian Calderon','Ph.D. Student, Electrical and Computer Engineering', '<strong>Breaking rules</strong> to <em>inspire</em> <br />the <em>next generation</em> of <strong>engineers.</strong>','/brand/independent/students/brian-calderon.cfm'],
                   ['crowther.jpg','Georgia Crowther','&rsquo;14 Mechanical Engineering', '<strong>Breaking rules</strong> to <em>explore <br />new ways</em> to help <strong>humanity.</strong>','/brand/independent/students/crowther.cfm']];


// global variables
var current_slide = 0;
var slide_count = 0;
var starting_slide = 0;
var autoplaying = false;
var slide_interval;
var image_div;
var caption_div;
var caption_div_inner;

function rotator_init(div,caption) {
	image_div = div || default_div;
	caption_div = caption || default_caption_div;
	caption_div_inner = caption_div + '-caption';
	slide_count = image_array.length || 0;
	
	// lock the height
	$(caption_div).css('height',caption_height);
	
	// buffer layers
	$(image_div).prepend('<div class="slide-buffer" id="slide-buffer1" /><div class="slide-buffer" id="slide-buffer2" />');
	$(image_div).css('background',bg_color); // disable intrinsic background image
	
	// build image set and preload
	for (i=0;i<slide_count;i++) {
		$(image_div).append('<img class="image-loader" id="slide-image'+i+'" />');
		// slide data
		$('#slide-image'+i).data('loaded',false); // <- load status
		$('#slide-image'+i).data('name',image_array[i][1]); // <- name
		$('#slide-image'+i).data('title',image_array[i][2]); // <- title
		$('#slide-image'+i).data('caption',image_array[i][3]); // <- caption
		$('#slide-image'+i).data('link',image_array[i][4]); // <- article link
		// load image
		$('#slide-image'+i).attr('src',image_path+image_array[i][0]);
	}
	// tracking image preload
	$('.image-loader').load(function() {
		$(this).data('loaded',true);
	});
	// activate first slide when ready and start rotator
	if (random_start) { // random start
		starting_slide = Math.floor(Math.random() * slide_count);
		if (starting_slide > slide_count) {
			starting_slide = slide_count;
		}
		current_slide = starting_slide;
	}
	$('#slide-image'+starting_slide).load(function() {
		changeSlide(starting_slide,false);
		startRotator();
	});
}	

function startRotator() {
	
	// set up interval
	if (autoplay) {
		slide_interval = setInterval(slideTimer,(slide_time*1000));
	}
	$(image_div).addClass('animate');
	buildNav();
	
}
	

function slideTimer() {
	// find next slide
	var next_slide = current_slide + 1;
	if (next_slide >= slide_count) {
		next_slide = 0;
	}
	// activate next slide if loaded
	if ($('#slide-image'+next_slide).data('loaded')) {
		current_slide = next_slide;
		changeSlide(next_slide,true);
	}
	else {
		// either the image is still loading, or it was not found
		// TODO: handle slow load time and errors
		// for now, it will try again next interval
	}
	
}
	
	
function buildNav() {
	$(caption_div_inner).after('<div id="campaign-feature-nav"><h4 class="no-display">Slides</h4><ul></ul></div>');
	$('.image-loader').each(function(i){
		$('#campaign-feature-nav ul').first().append('<li><a href="#'+$(this).attr('id')+'">'+(i+1)+'<em class="no-display"> '+$(this).data('name')+'</em></a></li>');
	});
	$('#campaign-feature-nav li').eq(current_slide).addClass('active');
	
	$('#campaign-feature-nav li a').each(function(i){
		$(this).click(function(e){
			e.preventDefault();
			
			clearInterval(slide_interval);
			$(image_div).removeClass('animate');
			$('#slide-buffer2, ' + caption_div_inner).stop();
			changeSlide(i,false);
		});
	});

}

function changeSlide(slide,include_transition) {
	var c_speed = transition_speed * 1000;
	var c_quickspeed = transition_speed * 200;
	if (!include_transition) {
		c_speed = 0;
		c_quickspeed = 0;
	}
	$('#campaign-feature-nav li').removeClass('active');
	$('#campaign-feature-nav li').eq(slide).addClass('active');
	//$('#slide-buffer1').css('background-image',$('#slide-buffer2').css('background-image'));
	$(caption_div_inner).fadeOut(c_quickspeed,function() {
		$(this).find('a').first().attr('href',$('#slide-image'+slide).data('link')).html(
			'<h3><span class="campaign-link">'+$('#slide-image'+slide).data('caption')+'</span></h3>' +
			'<h4><span class="campaign-link">'+$('#slide-image'+slide).data('name')+'</span></h4>' +
			'<p><span class="campaign-link">'+$('#slide-image'+slide).data('title')+'</span></p>' +
			'<p class="campaign-more"><span class="campaign-link">read more</span></p></a>'
		); // TODO: make "read more" dynamic, perhaps with support for "view video"
		$(caption_div_inner).delay(c_quickspeed*2).fadeIn(c_quickspeed);
	});
	$('#slide-buffer2').hide().css('background-image','url('+$('#slide-image'+slide).attr('src')+')').fadeIn(c_speed, function() {
		$('#slide-buffer1').css('background-image',$('#slide-buffer2').css('background-image'));
	});
}



