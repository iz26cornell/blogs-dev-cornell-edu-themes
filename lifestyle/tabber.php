<div class="tabber">

	<?php 
		$i = 1;
		while($i < 7) { 
	?>
	<div class="tabbertab">
        <h2><?php $featcat = cat_id_to_name(get_theme_mod('tabber_'.$i)); echo $featcat; ?></h2>
		<?php $recent = new WP_Query("cat=".get_theme_mod('tabber_'.$i)."&showposts=1"); while($recent->have_posts()) : $recent->the_post();?> 
		<?php if( get_post_meta($post->ID, "thumb", true) ): ?>
			<a href="<?php the_permalink() ?>" rel="bookmark"><img class="thumb" src="<?php bloginfo('template_directory'); ?>/tools/timthumb.php?src=<?php echo get_post_meta($post->ID, "thumb", $single = true); ?>&amp;h=<?php echo get_theme_mod('tabber_thumb_height'); ?>&amp;w=<?php echo get_theme_mod('tabber_thumb_width'); ?>&amp;zc=1" alt="<?php the_title(); ?>" /></a>	
		<?php endif; ?>	
		<h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<p><?php the_time('F j, Y'); ?> &middot; <?php comments_popup_link(__('Leave a Comment', 'studiopress'), __('1 Comment', 'studiopress'), __('% Comments', 'studiopress')); ?>&nbsp;<?php edit_post_link('('.__('Edit', 'studiopress').')', '', ''); ?></p>
		<?php the_content_limit(600, __("[Read the full story]", 'studiopress')); ?>				
		<?php endwhile; ?>
        
        <div class="clear"></div>
	</div>
    <?php 
			$i++;
		} 
	?>
                        
</div>