<?php

	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'brunelleschi_header_image_width', 1160 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'brunelleschi_header_image_height', 268 ) );

/*
Plugin Name: RSS Reviews
Plugin URI: http://gregorypearcey.com/blog/tirpadvisor-plugin-wordpress
Description: RSS Revews from TripAdvisor
Version: 1.1 
Author: Gregory Pearcey
Author URI: http://gregorypearcey.com/
License: Creative Commons Attribution-ShareAlike 
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
*/

//Add the stylesheet into the header
wp_enqueue_style("rss.reviews",WP_PLUGIN_URL."/tripadvisor-rss-reviews/css/responsiveslides.css");

//Add the scripts in the footer
wp_enqueue_script("jquery");

wp_enqueue_script(
"rss.reviews", WP_PLUGIN_URL."/tripadvisor-rss-reviews/js/responsiveslides.js",
array("jquery"), "1.3.1",1);

wp_enqueue_script(
"rss.reviewssetup", WP_PLUGIN_URL."/tripadvisor-rss-reviews/js/rss-reviews.js",
array("jquery","rss.reviews"), "",1);

// Add Widget area
class rss_reviews extends WP_Widget {

	// constructor
	function __construct() {
        parent::WP_Widget(false, $name = __('TripAdvisor RSS Reviews', 'rss_reviews') );
    }

	// widget form creation
	function form($instance) {
	
	// Check values
	if( $instance) {
	     $title = esc_attr($instance['title']);
	     $text = esc_attr($instance['text']);
	     $amount = esc_textarea($instance['amount']);
	} else {
	     $title = '';
	     $text = '';
	     $amount = '';
	}
	?>
	
	<p>
	<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'rss_reviews'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
	</p>
	
	<p>
	<label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('TripAdvisor ID:', 'rss_reviews'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" type="text" value="<?php echo $text; ?>" />
	</p>
	
	<p>
	<label for="<?php echo $this->get_field_id('amount'); ?>"><?php _e('Amount of Reviews:', 'rss_reviews'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('amount'); ?>" name="<?php echo $this->get_field_name('amount'); ?>" type="text" value="<?php echo $amount; ?>" />
	</p>
	<?php
	}

	// update widget
	function update($new_instance, $old_instance) {
	      $instance = $old_instance;
	      // Fields
	      $instance['title'] = strip_tags($new_instance['title']);
	      $instance['text'] = strip_tags($new_instance['text']);
	      $instance['amount'] = strip_tags($new_instance['amount']);
	     return $instance;
	}

// display widget front end
	function widget($args, $instance) {
	   extract( $args );
	   // these are the widget options
	   $title = apply_filters('widget_title', $instance['title']);
	   $text = $instance['text'];
	   $amount = $instance['amount'];
	   echo $before_widget;
	   
	   if(function_exists('fetch_feed')) {
		// fetch feed items
		$rss = fetch_feed('http://www.tripadvisor.com/Feeds-'.$text.'-treviews.xml');
		if(!is_wp_error($rss)) : // error check
			$maxitems = $rss->get_item_quantity($amount); // number of items
			$rss_items = $rss->get_items(0, $maxitems);
		endif;   
	   
	   // Display the widget
	   echo '<div id="slider" class="widget-text wp_widget_plugin_box">';
	
	   // Check if title is set
	   if ( $title ) {
	      echo $before_title . $title . $after_title;
	   }
	   
	   echo '<ul class="rslides">';
	   if($maxitems == 0) echo '<p>Feed not available.</p>'; // if empty
	   else foreach ($rss_items as $item) : 

		echo '<li>';
			echo '<h5>';
				echo '<a href="'.$item->get_permalink().'"title="'.$item->get_date('j F Y @ g:i a').'">'.$item->get_title().'</a>';
			echo '</h5>';
			echo '<p><em>'.$item->get_description().'</em></p>';
		echo '</li>';
	
		endforeach; 
		echo '</ul>';
		echo '</div>';
	} 
	   echo $after_widget;
	}
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("rss_reviews");'));

//Short Code
function tripadvisor($atts, $content=null){  
  
    extract(shortcode_atts( array('id' => ''), $atts));  
	   
	   if(function_exists('fetch_feed')) {
		// fetch feed items
		$rss = fetch_feed('http://www.tripadvisor.com/Feeds-'.$id.'-treviews.xml');
		if(!is_wp_error($rss)) : // error check
			$maxitems = $rss->get_item_quantity($amount); // number of items
			$rss_items = $rss->get_items(0, $maxitems);
		endif;
	
	   // Check if title is set
	   if ( $title ) {
	      echo $before_title . $title . $after_title;
	   }
	   
	   echo '<ul class="nobullets">';
	   if($maxitems == 0) echo '<p>Feed not available.</p>'; // if empty
	   else foreach ($rss_items as $item) : 

		echo '<li>';
			echo '<h5>';
				echo '<a href="'.$item->get_permalink().'"title="'.$item->get_date('j F Y @ g:i a').'">'.$item->get_title().'</a>';
			echo '</h5>';
			echo '<p><em>'.$item->get_description().'</em></p>';
		echo '</li>';
		echo '<hr>';
	
		endforeach; 
		echo '</ul>';
	}  
  
}  
add_shortcode('tripadvisor', 'tripadvisor');

/*
Plugin Name: Allow Javascript in Text Widgets
Plugin URI: http://philipjohn.co.uk/#pj-better-multisite-text-widget
Description: Replaces the default text widget with one that allows Javascript so you can do basic things like add Google Ads to your sidebar without using other plugins.
Version: 0.3
Author: Philip John
Author URI: http://philipjohn.co.uk
License: GPL2
Network: true
Text Domain: allow-javascript-in-text-widgets
*/

/**
 * Text widget class
 */
class WP_Widget_Text_With_JS extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_text', 'description' => __('Arbitrary text or HTML', 'allow-javascript-in-text-widgets'));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('text', __('Text', 'allow-javascript-in-text-widgets'), $widget_ops, $control_ops);
	}

	function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters( 'widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
		$text = apply_filters( 'widget_text', $instance['text'], $instance );
		echo $before_widget;
		if ( !empty( $title ) ) { echo $before_title . $title . $after_title; } ?>
			<div class="textwidget"><?php echo $instance['filter'] ? wpautop($text) : $text; ?></div>
		<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['text'] =  $new_instance['text'];
		$instance['filter'] = isset($new_instance['filter']);
		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '' ) );
		$title = strip_tags($instance['title']);
		$text = esc_textarea($instance['text']);
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'allow-javascript-in-text-widgets'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>

		<p><input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs', 'allow-javascript-in-text-widgets'); ?></label></p>
<?php
	}
}
function wp_widget_text_with_js_init(){
	unregister_widget('WP_Widget_Text');
	register_widget('WP_Widget_Text_With_JS');
}
add_action('widgets_init', 'wp_widget_text_with_js_init', 1);
?>