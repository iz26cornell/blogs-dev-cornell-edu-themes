<div id="skipnav">
	<a href="#main">Skip to main content</a>
</div>
<div id="cu-identity" class="theme-gray45 header">
    <div class="container">
        <div id="cu-logo" class="cornellLogo columns">

            <a href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/cornell_identity/cu_logo_white45.gif" alt="Cornell University" /></a>
        </div>
        <div class="divSearchIcon" onclick="cornell.openSearchBar()">
            <img id="searchIconImg" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/magnifier.png" alt="search icon" class="imgSearchIcon" width="0" height="0"/>
        </div>
        <div id="search-form" class="searchBar">
            <form role="search" method="get" id="searchform" action="<?php echo home_url('/'); ?>" >
                <div id="search-input" class="searchInput">
                    <input type="text" value="" name="s" id="search-form-query" size="20" class="searchInpBox"/>
                    <input type="submit" id="search-form-submit" name="btnG" value="go" />
                </div>
                <div id="search-filters" class="searchFiter">
                <div class="filter">
                    <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
                    <label for="search-filters1">This Site</label>
                    </div>
                <div class="filter">
                    <input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
                    <label for="search-filters2">Cornell</label>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<div class="container">