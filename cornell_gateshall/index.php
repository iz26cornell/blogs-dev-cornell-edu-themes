<?php get_header(); ?>

<div id="wrap_index" class="bdyWrapper innerPagesWrapper">
    <div id="content" class="container">
        <!--        <div id="content-wrap">-->
        <div class="leftContent columns">
            <div id="header">
                <!-- GATES HALL LOGO -->
                <h1><a href="<?php echo get_settings('home'); ?>/">
				<strong>Gates Hall</strong>
				<!--<img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/logo.gif" alt="Logo" class="logo"/>-->
				</a>
				</h1>
                <!-- main navigation -->
                <div id="main-navigation">
                    <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'primary')); ?>
                </div>

            </div>
            <div class="divMobileDisplay">
                <div class="corbnellBannerForInnerPages">
                       <div class="exploreDiv" onclick="cornell.openMainNavigationLinks()">
                                          <img id="exploreSideArrow" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/arrow.png" alt="Arrow" class="exploreArrow"/>
                        <img id="exploreDownArrow" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/down_arrow.png" alt="Arrow" class="exploreDownArrow"/>
	  </div>
                </div>
                <?php
                    $home_page_post_id = 46;
                    $home_page_post = get_post($home_page_post_id, ARRAY_A);
                    $content_home = $home_page_post['post_content'];
                    echo $content_home;
                ?>
                </div>
                <div id="secondary" class="divSecondaryPageContent">
                <?php if (is_active_sidebar('sidebar-1')) : ?>
                <?php dynamic_sidebar('sidebar-1'); ?>
                <?php endif; ?>
                    </div>

                </div>
                <div class="rightContentPages columns">
                    <div class="divMobileContentWrapper">
                        <div id="main" class="pageMainDiv innerPageMainDiv">

                    <?php if (have_posts ()) : while (have_posts ()) : the_post(); ?>
                                <h2 class="page-title"><?php the_category(', ') ?></h2>
                                <div class="innerPageContentHeaderDiv">
                                    <div class="entry-date">
                                        <span class="month">
                                <?php the_time('M') ?>
                            </span>
                            <span class="day">
                                <?php the_time('d') ?>
                            </span>
                            <span class="year"><?php the_time('Y') ?></span></div>
                        <?php print_post_title() ?>
                            </div>        
                    <?php the_content(__('Read more')); ?>

                                <p class="see-all-category">See all in <?php the_category(', ') ?> category.</p>
                    <?php endwhile;
                            else: ?>
                                <p>
                        <?php _e('Sorry, no posts matched your criteria.'); ?>
                            </p>
                    <?php endif; ?>
                    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
            <!--                </div>-->
<?php get_footer(); ?>
