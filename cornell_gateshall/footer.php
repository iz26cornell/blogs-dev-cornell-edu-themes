<!-- begin footer -->
<div id="footer" class="container">
    <div id="footer-content">
        <div id="footer-content-wrap">
            <div id="footer-left">
                <?php if (is_active_sidebar('footer-message')) : ?>
                <?php dynamic_sidebar('footer-message'); ?>
                <?php endif; ?>
                </div>
                <div id="footer-right">
                <?php if (is_active_sidebar('footer-widget-area')) : ?>
                <?php dynamic_sidebar('footer-widget-area'); ?>
                <?php endif; ?>
                        <ul class="meta">
                            <li>&copy;<?php echo date("Y"); ?> <a href="http://www.cornell.edu/">Cornell University</a></li>
                    <?php wp_register(); ?>
                        <li class="last">
                        <?php wp_loginout(); ?>
                    </li>
                </ul>
            </div>
            
            
        </div>
    </div>

</div>



<div id="edublogs"><?php wp_footer(); ?></div><!-- edublogs -->

</div>

<!-- Included Scripts for Fluid video and common toggle functionality-->
<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/fluidvids.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/cornellScript.js" type="text/javascript"></script>
</body></html>