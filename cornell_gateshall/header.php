<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <head profile="http://gmpg.org/xfn/11"> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta content='yes' name='apple-mobile-web-app-capable'/>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="distribution" content="global" />
<meta name="robots" content="follow, all" />
<meta name="language" content="en, sv" />

<title>
<?php wp_title(''); ?>
<?php if(wp_title('', false)) { echo ' |'; } ?>
 <?php bloginfo('name');
 	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	?>
</title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<!-- leave this for stats please -->
<link rel="Shortcut Icon" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_get_archives('type=monthly&format=link'); ?>
<?php wp_head(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/colorbox/jquery.colorbox.js"></script>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fancybox/source/jquery.fancybox.css?v=2.0.4" type="text/css" media="screen" />
<!--<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script> -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/source/jquery.fancybox.pack.js?v=2.0.4"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-buttons.css?v=2.0.4" type="text/css" media="screen" />
<!--<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/source/helpers/jquery.fancybox-buttons.js?v=2.0.4"></script> -->
<!--<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/stylePrint.css" type="text/css" media="print" />-->

<?php if (function_exists('mobile_screen') ) { mobile_screen(); } ?>
<!--[if lte IE 8]>
	<link rel="stylesheet" rev="stylesheet" href="<?php echo home_url( '/' ); ?>wp-content/themes/cornell_smallfarms/styles/ie.css" media="all" />
<![endif]-->

<script>
			$(document).ready(function(){
				//Examples of how to assign the ColorBox event to elements
				$(".group1").colorbox({rel:'group1'});
				$(".group2").colorbox({rel:'group2'});
				$(".group3").colorbox({rel:'group3'});
				$(".group5").colorbox({rel:'group5'});
				$(".group6").colorbox({rel:'group6'});
				$(".group7").colorbox({rel:'group7'});
				$(".group8").colorbox({rel:'group8'});
				$(".group9").colorbox({rel:'group9', slideshow:true});
				$(".ajax").colorbox();
				$(".youtube").colorbox({iframe:true, innerWidth:818, innerHeight:618});
				$(".iframe").colorbox({iframe:true, innerWidth:1042, innerHeight:786});
				$(".inline").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});
				
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
				
				$(".myfancybox").fancybox({
		openEffect: 'none', closeEffect: 'none', nextEffect: 'none', prevEffect: 'none',
		nextMethod: 'zoomIn' , 
		preload: 3, openSpeed  : 150,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {}
		}
	});
	
	$(".myfancybox2").fancybox({
		type:'iframe',
autoScale:'false',
fitToView   : true,
		
		
	});
	
	
			});
		</script>
		
		
		
	<!--	<script type='text/javascript'>//<![CDATA[ 
$(function(){
(function ($, F) {
    
    // Opening animation - fly from the top
    F.transitions.dropIn = function() {
        var endPos = F._getPosition(true);

        endPos.top = (parseInt(endPos.top, 10) - 200) + 'px';
        endPos.opacity = 0;
        
        F.wrap.css(endPos).show().animate({
            top: '+=200px',
            opacity: 1
        }, {
            duration: F.current.openSpeed,
            complete: F._afterZoomIn
        });
    };

    // Closing animation - fly to the top
    F.transitions.dropOut = function() {
        F.wrap.removeClass('fancybox-opened').animate({
            top: '-=200px',
            opacity: 0
        }, {
            duration: F.current.closeSpeed,
            complete: F._afterZoomOut
        });
    };
    
    // Next gallery item - fly from left side to the center
    F.transitions.slideIn = function() {
        var endPos = F._getPosition(true);

        endPos.left = (parseInt(endPos.left, 10) - 200) + 'px';
        endPos.opacity = 0;
        
        F.wrap.css(endPos).show().animate({
            left: '+=200px',
            opacity: 1
        }, {
            duration: F.current.nextSpeed,
            complete: F._afterZoomIn
        });
    };
    
    // Current gallery item - fly from center to the right
    F.transitions.slideOut = function() {
        F.wrap.removeClass('fancybox-opened').animate({
            left: '+=200px',
            opacity: 0
        }, {
            duration: F.current.prevSpeed,
            complete: function () {
                $(this).trigger('onReset').remove();
            }
        });
    };

}(jQuery, jQuery.fancybox));

$(".fancybox")
    .attr('rel', 'gallery')
    .fancybox({
        openMethod : 'dropIn',
        openSpeed : 250,

        closeMethod : 'dropOut',
        closeSpeed : 150,
        
        nextMethod : 'slideIn',
        nextSpeed : 250,
        
        prevMethod : 'slideOut',
        prevSpeed : 250
    });
});//]]>  

</script> -->

		
		
		
		
		<!-- <style type="text/css">
		.fancybox-title {color:#000;font-size:1.2em;}
 .fancybox-next span {
  left: auto;
  right: 0px;
 }
 .fancybox-prev span {
  left: 0px;
 }
 .fancybox-nav span {
 visibility: visible;
}
</style> -->
</head>
<body <?php body_class(); ?>>

<?php include (TEMPLATEPATH . '/banner.php'); ?>

