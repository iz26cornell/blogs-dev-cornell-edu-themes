<?php
/**
 * Template Name: Homepage
 * @package WordPress
 * @subpackage Cornell
 */
get_header();
?>

<div id="wrap_Home" class="bdyWrapper">
	<div id="content" class="container">
		   <div class="leftContent columns">
			<div id="header">
				<h1><a href="<?php echo get_settings('home'); ?>/">
				<strong>Gates Hall</strong>
				<!--<img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/logo.gif" alt="Logo" class="logo"/>-->
				</a>
				</h1>
				<div id="main-navigation">
					<?php /* Main navigation menu.	If one isn't filled out, wp_nav_menu falls back to wp_page_menu.
					 *	The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
					<?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'primary')); ?>
				</div>
			</div>
			<div class="divMobileDisplay">
				<div class="cornellBanner">
					<div class="exploreDiv" onclick="cornell.openMainNavigationLinks()">
						<img id="exploreSideArrow" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/arrow.png" alt="Arrow" class="exploreArrow"  width="0" height="0"/>
                        <img id="exploreDownArrow" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/down_arrow.png" alt="Arrow" class="exploreDownArrow"  width="0" height="0"/>
	</div>

				</div>
				       

				<?php
					$home_page_post_id = 46;
					$home_page_post = get_post($home_page_post_id, ARRAY_A);
					$content_home = $home_page_post['post_content'];
					echo $content_home;
				?>
				</div>
				<div id="secondary">
				<?php if (is_active_sidebar('sidebar-home')) : ?>
				<?php dynamic_sidebar('sidebar-home'); ?>
				<?php endif; ?>

						<div id="featured-posts">

					<?php query_posts('cat=46247&showposts=3'); ?> 
						<h3 class="cat-name"><?php echo get_cat_name(46247); ?>	 <span class="see-all-category-posts"> 
						<a href="<?php echo get_category_link(46247); ?>">see all <?php echo get_cat_name(46247); ?></a></span></h3>
					<?php if (have_posts ())
							while (have_posts ()) : the_post(); ?>
								<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<div class="entry-date"><span class="month"><?php the_time('M') ?></span><span class="day"><?php the_time('d') ?></span><span class="year"><?php the_time('Y') ?></span></div>
						<?php if (is_front_page ()) {
						?>
						<?php print_post_title() ?>
						<?php } else {
						?>
						<?php print_post_title() ?>
						<?php } ?>
							</div><!-- #post-## -->
					<?php endwhile; ?>

					<?php query_posts('cat=213&showposts=3'); ?> 
							<h3 class="cat-name"><?php echo get_cat_name(213); ?>	 
							<span class="see-all-category-posts"> 
							<a href="<?php echo get_category_link(213); ?>">see all <?php echo get_cat_name(213); ?></a></span></h3>
					<?php if (have_posts ())
								while (have_posts ()) : the_post(); ?>

									<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
										<div class="entry-date"><span class="month"><?php the_time('M') ?></span><span class="day"><?php the_time('d') ?></span><span class="year"><?php the_time('Y') ?></span></div>
						<?php if (is_front_page ()) {
						?>
						<?php print_post_title() ?>
						<?php } else {
						?>
						<?php print_post_title() ?>
						<?php } ?>
								</div>
					<?php endwhile; ?>
							</div>

				<?php if (is_active_sidebar('sidebar-home-extra')) : ?>
				<?php dynamic_sidebar('sidebar-home-extra'); ?>
				<?php endif; ?>
								</div>
   
							</div>
							<div class="rightContent columns" style="background:url(<?php header_image(); ?>) no-repeat 0 0 ">
								<div class="rightContentWrapper">
									<div id="main">
								  <div class="page" style="display:none;">
								  
						<?php
									$home_page_post_id = 46;
									$home_page_post = get_post($home_page_post_id, ARRAY_A);
									$content_home = $home_page_post['post_content'];
									echo $content_home;
						?>
							   </div>

					<?php if (have_posts ())
										while (have_posts ()) : the_post(); ?>
											<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="display:none;">
						<?php if (is_front_page ()) {
						?>
						<?php the_title(); ?>
						<?php } else {
						?>
							<h1 class="entry-title">
								<?php the_title(); ?>
							</h1>
						<?php } ?>
							<div class="entry-content">
							<?php the_content(); ?>
							<?php wp_link_pages(array('before' => '<div class="page-link">' . __('Pages:', 'Cornell'), 'after' => '</div>')); ?>
							<?php edit_post_link(__('Edit', 'Cornell'), '<span class="edit-link">', '</span>'); ?>
										</div>
					
									</div>
			
					<?php endwhile; ?>

									</div>
								</div>
							</div>
						</div>
					</div>
<?php get_footer(); ?>
