<?php get_header(); ?>
<div id="wrap_archive" class="bdyWrapper innerPagesWrapper">
    <div id="content" class="container">
        <!--<div id="content-wrap">-->
        <div class="leftContent columns">
            <div id="header">
               <h1><a href="<?php echo get_settings('home'); ?>/">
				<strong>Gates Hall</strong>
				<!--<img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/logo.gif" alt="Logo" class="logo"/>-->
				</a>
				</h1>
                <?php /* bloginfo('name'); */ ?>
                <div id="main-navigation">
                    <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
                    <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'primary')); ?>
                </div>
                <!-- main navigation -->
            </div>
            <div class="divMobileDisplay">
                <div class="corbnellBannerForInnerPages">
                    <div class="exploreDiv" onclick="cornell.openMainNavigationLinks()">
                       <img id="exploreSideArrow" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/arrow.png" alt="Arrow" class="exploreArrow"/>
                        <img id="exploreDownArrow" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/down_arrow.png" alt="Arrow" class="exploreDownArrow"/>
	 </div>
                </div>
                <?php
                    $home_page_post_id = 46;
                    $home_page_post = get_post($home_page_post_id, ARRAY_A);
                    $content_home = $home_page_post['post_content'];
                    echo $content_home;
                ?>
                </div>
            <?php /* include(TEMPLATEPATH . "/r_sidebar.php"); */ ?>
                    <div id="secondary" class="divSecondaryPageContent">
                <?php if (is_active_sidebar('sidebar-1')) : ?>
                <?php dynamic_sidebar('sidebar-1'); ?>
                <?php endif; ?>
                    </div>

                </div>
                <div class="rightContentPages columns">
                    <div class="divMobileContentWrapper">
                        <div id="main" class="pageMainDiv innerPageMainDiv">
                            <h2 class="page-title">

                        <?php if (is_day ()) : ?>
                        <?php printf(__('<span>%s</span>'), get_the_date()); ?>
                        <?php elseif (is_month ()) : ?>
                        <?php printf(__('<span>%s</span>'), get_the_date(_x('F Y', 'monthly archives date format'))); ?>
                        <?php elseif (is_year ()) : ?>
                        <?php printf(__('<span>%s</span>'), get_the_date(_x('Y', 'yearly archives date format'))); ?>
                        <?php else : ?>
                        <?php the_category(', ') ?>
                        <?php endif; ?>

                                    </h2>

                    <?php if (have_posts ()) : while (have_posts ()) : the_post(); ?>
                                                <div class="postTitleNDate">
                                                    <div>
                                                        <div class="entry-date">
                                                            <span class="month"><?php the_time('M') ?></span>
                                                            <span class="day"><?php the_time('d') ?></span>
                                                            <span class="year"><?php the_time('Y') ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="postTitle">
                            <?php print_post_title() ?>
                                            </div>                    
                                        </div>
                                        <div class="innerPagesParaDiv">

                        <?php the_excerpt(__('Read more')); ?>
                                            </div>

                                            <!--

                    <?php trackback_rdf(); ?>

                                                                                                                                                                            	-->
                    <?php endwhile;
                                            else: ?>
                                                <p>
                        <?php _e('Sorry, no posts matched your criteria.'); ?>
                                            </p>
                    <?php endif; ?>
                    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
                                            </div>
                                        </div>
                                    </div>


                                    <!--</div>-->
                                </div>
                            </div>
<?php get_footer(); ?>
