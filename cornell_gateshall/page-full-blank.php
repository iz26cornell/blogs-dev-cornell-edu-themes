<?php
/**
 * Template Name: Full Width blank (for pop up)
 * @package WordPress
 * @subpackage Cornell
 */

get_header(); ?>
<div id="wrap" class="onecolumn blank">
<div id="content">
<div id="content-wrap">

  <div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php /*?><h2 class="page-title"><?php the_title(); ?></h2><?php */?>
    <?php the_content(__('Read more'));?>
    <!--
	<?php trackback_rdf(); ?>
	-->
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
  </div>
</div>
</div>
</div>
<?php get_footer(); ?>
