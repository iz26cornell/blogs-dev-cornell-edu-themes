<!-- begin r_sidebar -->

<div id="secondary">
  <div id="secondary-navigation">
  <!--<p class="admin-link"><a href="http://blogs.cornell.edu/wp-login.php">Blog Admin</a></p>-->
    <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
    <div id="pages">
      <h2>Pages</h2>
    <ul>
      <li><a href="<?php echo get_settings('home'); ?>">Home</a></li>
      <?php wp_list_pages('title_li=&depth=1'); ?>
    </ul>
  </div>
    <div id="Recent">
      <h2>Recently Written</h2>
      <ul>
        <?php get_archives('postbypost', 10); ?>
      </ul>
    </div>
    <div id="Categories">
      <h2>Categories</h2>
      <ul>
        <?php wp_list_cats('sort_column=name'); ?>
      </ul>
    </div>
    <div id="Archives">
      <h2>Monthly Archives</h2>
      <ul>
        <?php wp_get_archives('type=monthly'); ?>
      </ul>
    </div>
    <div id="Blogroll">
      <h2>Blogroll</h2>
      <ul>
        <?php get_links(-1, '<li>', '</li>', ' - '); ?>
      </ul>
    </div>
    <div id="Admin">
      <h2>Admin</h2>
      <ul>
        <?php wp_register(); ?>
        <li><?php wp_loginout(); ?></li>
        <?php wp_meta(); ?>
      </ul>
    </div>
    <?php endif; ?>
  </div>
  <div class="feeds"><a class="rss-entries" href="<?php bloginfo('rss2_url'); ?>">Entries</a> <a class="rss-comments" href="<?php bloginfo('comments_rss2_url'); ?>">Comments</a></div>
  <!-- <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. -->
</div>
<!-- end r_sidebar -->
