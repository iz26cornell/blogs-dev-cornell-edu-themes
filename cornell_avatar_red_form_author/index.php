<?php get_header(); ?>
<div id="wrap">
<div id="content">
  <div id="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h2><a href="<?php the_permalink() ?>" rel="bookmark">
      <?php the_title(); ?>
      </a></h2>
      <?php the_content(__('Read more'));?>
    <div class="postmeta">
        <div class="postmeta-author">
          <?php echo get_avatar( $id_or_email, $size = '76', $default = '<path_to_url>' ); ?>

          <p class=" author">by
<?php
global $post;
if ($post) {
	$custom_author =get_post_meta($post->ID, 'Name', true);
}
if ($custom_author) {
?>
            <strong><?php echo $custom_author; ?></strong>
<?php
} else {
?>
            <strong><?php the_author(); ?></strong>
<?php
}
?>
          </p>
          <p class="post-date"> <span class="month">
            <?php the_time(__('M','arclite')); ?>
            </span> <span class="day">
            <?php the_time(__('j','arclite')); ?>
            </span> </p>
          <p class="category"> category:
            <?php the_category(', ') ?>
          </p>
        </div>
        <div class="postmeta-comments">
          <p>This post currently has
            <?php comments_number('no responses','one response','% responses'); ?></p>
          <p class="leave-comment"><a href="<?php comments_link(); ?>">Leave Comment</a></p>
        </div>
      </div>
    <!--

	<?php trackback_rdf(); ?>

	-->
    <h2>Comments</h2>
    <?php comments_template(); // Get wp-comments.php template ?>
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
  </div>
  <?php include(TEMPLATEPATH."/r_sidebar.php");?>
</div>
<!-- The main column ends  -->
</div>
<?php get_footer(); ?>
