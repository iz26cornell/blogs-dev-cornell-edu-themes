<?php get_header(); ?>

<div id="wrap">
  <div id="content">
    <div id="main">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <h2><a href="<?php the_permalink() ?>" rel="bookmark">
        <?php the_title(); ?>
        </a></h2>
      <?php the_content(__('Read more'));?>
      <div class="postmeta">
        <div class="postmetaleft">
          <div class="post-date">
            <span class="month"><?php the_time(__('M','arclite')); ?></span>
            <span class="day"><?php the_time(__('j','arclite')); ?></span>
           </div>
          <p class="category_author"> category:
            <?php the_category(', ') ?>
            by
            <?php the_author(); ?>
          </p>
        </div>
        <div class="postmetaright">
          <p>
            <?php if ('open' == $post->comment_status) : ?>
            <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
            <?php else : ?>
            &nbsp;
            <?php endif; ?>
            <span>
            <?php edit_post_link('edit', '', ''); ?>
            </span></p>
        </div>
      </div>
      <!--

	<?php trackback_rdf(); ?>
	-->
      <?php endwhile; else: ?>
      <p>
        <?php _e('Sorry, no posts matched your criteria.'); ?>
      </p>
      <?php endif; ?>
      <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
    </div>
    <?php include(TEMPLATEPATH."/r_sidebar.php");?>
  </div>
  <!-- The main column ends  -->
</div>
<?php get_footer(); ?>
