<?php
/*
 WARNING: This file is part of the core Genesis framework. DO NOT edit
 this file under any circumstances. Please do all modifications
 in the form of a child theme.
 */

/**
 * Handles the header structure.
 *
 * This file is a core Genesis file and should not be edited.
 *
 * @category Genesis
 * @package  Templates
 * @author   StudioPress
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     http://www.studiopress.com/themes/genesis
 */

do_action( 'genesis_doctype' );
if ( is_front_page() ) {   //for edublogs theme
	echo '<title>' . get_bloginfo('name') . '</title>';
}
else {
	do_action( 'genesis_title' );
}	
do_action( 'genesis_meta' );

/** search form configuration **/
	if (isset($_GET['btnG'])) {
		session_start();
		$selected_radio = $_GET['sitesearch'];
		
		if ($selected_radio == 'cornell') {
			$search_terms = urlencode($_GET['s']);
			$URL="http://www.cornell.edu/search" . "?q=" . $search_terms . "&client=default_frontend&proxystylesheet=default_frontend";
			print $URL;
			header ("Location: $URL");
		}
	}
?>
<?php wp_head(); ?>

	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/iws.js"></script>
    
	<!-- Initialize -->
	<script type="text/javascript">
		iws_init();
	</script>

</head>
<body <?php body_class(); ?>>
<div id="skipnav"><a href="#content">Skip to main content</a></div>
<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity">
  <div class="cu-identity-wrap">
  
    <div id="cu-logo">
      <a class="cals_logo" href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/cals_logo_white.png" alt="Cornell University" id="cals_logo_white" width="434" height="60" border="0" /></a>
      <h1><a href="http://www.cornell.edu/">Cornell University</a></h1>
      <h2><a href="http://cals.cornell.edu/">College of Agriculture &amp; Life Sciences</a></h2>
    </div>
    <div id="cu-search">
      	<div id="mobile-nav"></div>
        <div id="search_icon">
        </div>
         <div id="search-form">
			<form method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" >
                <div id="search-input">
                    <label for="search-form-query">Search:</label>
                    <input type="text" value="" name="s" id="search-form-query" size="26" />
                    <input type="submit" id="search-form-submit" name="btnG" value="go" />
                </div>              
                <div id="search-filters">
                        <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
                        <label for="search-filters1">This Site</label>
                    
                        <input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
                        <label for="search-filters2">Cornell</label>
                </div>	
            </form>
		 </div>
     </div>
   </div>
  </div>

<div id="wrap">

<div id="cicca-identity">
	<div class="cicca-identity-wrap">
		<a href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a>
	</div>
</div>

<?php
do_action( 'genesis_before' );
do_action( 'genesis_before_header' );
do_action( 'genesis_header' );
do_action( 'genesis_after_header' );

echo '<div id="inner">';
genesis_structural_wrap( 'inner' );

?>