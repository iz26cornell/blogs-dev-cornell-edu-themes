<?php get_header(); ?>
<div id="wrap">
  <div id="content">  
   <?php 
   	// custom query to have less posts-per-page on the first page, while maintaining correct pagination
   	// wp_reset_query(); called at the end
   	$posts_page1 = 9;
   	$posts_pagex = 12;
   	global $query_string;
   	if ($paged > 1) {
   		query_posts( $query_string . '&offset=' . strval($posts_page1+($paged-2)*$posts_pagex) . '&posts_per_page=' . $posts_pagex );
   	}
   ?>
   <?php $i = 1; ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php if ($i==1 && $paged<2) : ?>
			<div class="post-first">
			<?php /*the_thumb('subfolder=feature&WIDTH=462&HEIGHT=308&showpost=1');*/ ?>
			<?php /*if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail(array(462,352)); }*/ ?>
			<a href="<?php the_permalink() ?>" rel="bookmark"><?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail('large'); } ?></a>
		<?php else : ?>
			 <div class="post-card">
			 <?php /*the_thumb('subfolder=loop&width=210&height=160&showpost=1');*/ ?>
			 <?php /*if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail(array(210,160)); }*/ ?>
			 <a href="<?php the_permalink() ?>" rel="bookmark"><?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail('medium'); } ?></a>
		<?php endif; ?>
		<h2><a href="<?php the_permalink() ?>" rel="bookmark">
        <?php the_title(); ?>
        </a></h2>
        <?php if ($i==1 && $paged<2) : ?>
			<?php the_excerpt(__('Read more')); the_time('F j, Y'); ?>
		<?php else : ?>
			 <p><?php echo get_post_meta($post->ID, 'tiny_excerpt', true); ?>
			 <span class="date"><?php the_time('M Y'); ?></span></p>
		<?php endif; ?>

		<?php if ($i==1 && $paged<2) : ?>
			<p class="post">
			<?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
			<span>
			<?php edit_post_link('edit', '', ''); ?>
			</span></p>
		<?php endif; ?>     
    </div>
	<?php $i++; ?>
    <?php trackback_rdf(); ?>
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; wp_reset_query(); ?>
    <div class="webring">
<?php /*    	
	<div class="back"><?php next_posts_link('&laquo; Previous Entries') ?></div>
	<div class="next"><?php previous_posts_link('Next Entries &raquo;') ?></div>
*/ ?>
	<div class="back"><?php previous_posts_link('&laquo; Next Entries') ?></div>
	<div class="next"><?php next_posts_link('Previous Entries &raquo;') ?></div>	
</div>

  </div>
</div>
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
<!-- The main column ends  -->
<?php get_footer(); ?>
