<!-- begin footer -->

<hr />
<div id="footer">
  <!-- The footer-content div contains the Cornell University copyright -->
  <div id="footer-content">
    <div id="footer-overlay">
      <p><span>&copy;<?php echo date("Y");?> <a href="http://www.cornell.edu/">Cornell University</a></span> Psst! Please don't use images on this blog to decide whether a mushroom is edible.</p>
      <ul id="meta">
        <?php wp_register(); ?>
        <li>
          <?php wp_loginout(); ?>
        </li>
        <?php wp_meta(); ?>
        </ul>
    </div>
  </div>
</div>
<?php do_action('wp_footer'); ?>
<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
	try {
		var pageTracker = _gat._getTracker("UA-8539160-1");
		pageTracker._trackPageview();
	} catch(err) {}
</script> 
</body></html>