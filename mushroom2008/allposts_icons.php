<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="posts-list">
  <!--th images-->
  <?php if ($post->image): ?>
  <div class="post-tn"> <a href="<?php the_permalink() ?>" rel="bookmark"><img src="<?php echo $post->image->getThumbnailHref(array('w=50','fltr[]=usm|30|0.5|3')) ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" /></a></div>
  <?php endif ?>
  <!--end images-->
</div>
<!--

	<?php trackback_rdf(); ?>

	-->
<?php endwhile; else: ?>
<p>
  <?php _e('Sorry, no posts matched your criteria.'); ?>
</p>
<?php endif; ?>
