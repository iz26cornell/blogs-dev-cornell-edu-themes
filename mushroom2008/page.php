<?php get_header(); ?>

<div id="wrap">
  <div id="content">
  
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h2>
      <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a>
    </h2>
    <?php the_content(__('Read more'));?>
    <!--

	<?php trackback_rdf(); ?>

	-->
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <!--<div class="webring">
      <div class="back">
        <?php next_posts_link('&laquo; Previous Entries') ?>
      </div>
      <div class="next">
        <?php previous_posts_link('Next Entries &raquo;') ?>
      </div>
    </div>-->
  </div>
  <!-- The main column ends  -->
</div>
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
<?php get_footer(); ?>
