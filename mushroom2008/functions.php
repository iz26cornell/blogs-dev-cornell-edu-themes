<?php

if ( function_exists('register_sidebar') )
	register_sidebar(array(
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
		'after_title' => "</h3>\n",
    ));
    register_sidebar( array(
		'name' => __( 'Footer Text (all pages)', 'Cornell' ),
		'id' => 'footer-message',
		'description' => __( 'This content appears in the footer above the "Subscribe" heading.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	 ));
	 register_sidebar( array(
		'name' => __( 'Footer Favorites (all pages)', 'Cornell' ),
		'id' => 'footer-favorites',
		'description' => __( 'This set of links appears in the footer, to the left of the "More Information" links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	 ));


/**
 * Sets the post excerpt length to 40 characters.
 */
function Cornell_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'Cornell_excerpt_length' );


/**
 *  Custom "Read More" link for homepage posts
 *
 */
function new_excerpt_more($more) {
	global $post;
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


add_theme_support( 'post-thumbnails' );

?>