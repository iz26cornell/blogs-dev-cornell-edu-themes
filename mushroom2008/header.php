<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="distribution" content="global" />
<meta name="microid" content="mailto+http:sha1:d99c74cc722ff83a8d8d7f565ba70f5ebaffbc58" />
<meta name="robots" content="follow, all" />
<meta name="language" content="en, sv" />
<title>
<?php wp_title(''); ?>
<?php if(wp_title('', false)) { echo ' :'; } ?>
<?php bloginfo('name'); ?>
<?php bloginfo('description'); ?>
</title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<!-- leave this for stats please -->
<link rel="Shortcut Icon" href="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="openid.server" href="http://openid.claimid.com/server" />
<link rel="openid.delegate" href="http://openid.claimid.com/kathiehodge" />
<?php wp_get_archives('type=monthly&format=link'); ?>
<?php wp_head(); ?>
<style type="text/css" media="screen">
<!--
@import url( <?php bloginfo('stylesheet_url'); ?> );
-->
</style>
<style type="text/css" media="print">
<!--
@import url( <?php echo get_stylesheet_directory_uri('template_directory'); ?>/styles/print.css);
-->
</style>
</head>
<body>
<?php include (TEMPLATEPATH . '/banner.php'); ?>
<div id="header">
  <div id="identity">
    <h1><a href="<?php echo get_settings('home'); ?>/"><span>
      <?php bloginfo('name'); ?>
      </span></a></h1>
    <ul>
      <li><a href="<?php echo get_settings('home'); ?>/">Home</a></li>
      <li><a href="<?php echo get_settings('home'); ?>/category/fungi/">Browse</a></li>
      <?php wp_list_pages('title_li=&depth=1'); ?>
    </ul>
    <?php include (TEMPLATEPATH . '/searchform.php'); ?>
  </div>
</div>
<hr />
