<?php get_header(); ?>

<div id="wrap">
  <div id="content">


    <!-- This loop applies only to the FIRST post -->
	<?php if ($paged < 2) {
	$my_query = new WP_Query('showposts=1');
	while ($my_query->have_posts()) : $my_query->the_post();
	$do_not_duplicate = $post->ID; ?>
	<div class="post-first">
	  <!--?php echo get_thumb('WIDTH=462&HEIGHT=308&showpost=1'); ?-->
      <?php $key='main_image'; $image_to_show = get_post_meta($post->ID, $key, true); 
		if (isset($image_to_show)) { print '<img src="' . $image_to_show . '" />'; } ?>
      <h2><a href="<?php the_permalink() ?>" rel="bookmark">
        <?php the_title(); ?>
        </a></h2>
      <?php the_excerpt(__('Read more'));?>
      <p class="cal">
        <?php the_time('F j, Y'); ?>
      </p>
      <p class="post">
        <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
        <span>
        <?php edit_post_link('edit', '', ''); ?>
        </span></p>
    </div>
	<?php endwhile; } ?>

    <!-- This loop applies to all posts EXCEPT the first -->
	<?php if (have_posts()) : while (have_posts()) : the_post();
		  if( $post->ID == $do_not_duplicate ) continue; update_post_caches($posts); ?>
    <div class="post-card">
	   <?php $checkimg = get_thumb_url(); print $checkimg; the_thumb('showpost=1'); ?>
      <h2><a href="<?php the_permalink() ?>" rel="bookmark">
        <?php the_title(); ?>
        </a></h2>
		<p><?php $key="tiny_excerpt"; echo get_post_meta($post->ID, $key, true); ?></p>
      <p class="cal">
        <?php the_time('F j, Y'); ?>
      </p>
    </div>
    <?php trackback_rdf(); ?>
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <div class="webring">
	<div class="back"><?php next_posts_link('&laquo; Previous Entries') ?></div>
	<div class="next"><?php previous_posts_link('Next Entries &raquo;') ?></div>
</div>

  </div>
</div>
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
<!-- The main column ends  -->
<?php get_footer(); ?>
