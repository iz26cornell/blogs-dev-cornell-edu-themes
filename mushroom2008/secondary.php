<!-- begin r_sidebar -->

<div id="secondary">
  <div id="secondary-navigation">
    <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
    <?php endif; ?>
	<?php get_sidebar(); ?>
  </div>
  <div class="feeds"><a class="rss-entries" href="<?php bloginfo('rss2_url'); ?>">Entries</a> <a class="rss-comments" href="<?php bloginfo('comments_rss2_url'); ?>">Comments</a></div>
  <!-- <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. -->
</div>
<!-- end r_sidebar -->
