<!-- begin r_sidebar -->

<div id="secondary">
<!---  
  <div id="main-navigation">
    <h2>Main Navigation</h2>
    <ul>
      <li><a href="<?php echo get_settings('home'); ?>">Home</a></li>
      <?php wp_list_pages('title_li=&depth=1'); ?>
    </ul>
  </div>
--->
  <div id="secondary-navigation">
    <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
<!---    
	<div id="Recent">
      <h2>Recently Written</h2>
      <ul>
        <?php get_archives('postbypost', 10); ?>
      </ul>
    </div> 
--->

	<div id="calendar">
      <h2>Calendar</h2>
      <ul>
        <?php get_calendar(); ?>
      </ul>
    </div> 

    <div id="categories">
      <h2>Categories</h2>
      <ul>
        <?php wp_list_cats('sort_column=name'); ?>
      </ul>
    </div>

    <div id="blogroll">
      <h2>Links</h2>
      <ul>
        <?php get_links(-1, '<li>', '</li>', ' - '); ?>
      </ul>
    </div>

    <div id="subscribe">
      <h2>Subscribe</h2>
		<form method="post" action="/redesign/maillist/index.php">
	     Enter your e-mail address to receive notifications for new posts<br><br>
  	   <input name="email" size="12" maxlength="36" type="text">
	     <input name="submit" value="sign up" type="submit">
	   </form>    
	</div>

    <div id="archives">
      <h2>Archives</h2>
      <ul>
        <?php wp_get_archives('type=monthly'); ?>
      </ul>
    </div>
    
    <div id="admin">
      <h2>Meta</h2>
      <ul>
        <?php wp_register(); ?>
        <li>
          <?php wp_loginout(); ?>
        </li>
        <?php wp_meta(); ?>
      </ul>
    </div>
    <?php endif; ?>
  </div>
  <div class="feeds"><a class="rss-entries" href="<?php bloginfo('rss2_url'); ?>">Entries</a> <a class="rss-comments" href="<?php bloginfo('comments_rss2_url'); ?>">Comments</a></div>
  <!-- <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. -->
</div>
<!-- end r_sidebar -->
