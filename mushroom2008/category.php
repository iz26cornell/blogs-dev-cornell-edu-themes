<?php get_header(); ?>

<div id="wrap">
  <div id="content">
  <div class="webring">
  		<?php /*
      <div class="back">
        <?php next_posts_link('&laquo; Previous Entries') ?>
      </div>
      <div class="next">
        <?php previous_posts_link('Next Entries &raquo;') ?>
      </div>
      */ ?>
			<div class="back"><?php previous_posts_link('&laquo; Next Entries') ?></div>
			<div class="next"><?php next_posts_link('Previous Entries &raquo;') ?></div>            
    </div>
  <h2><?php single_cat_title(); ?></h2>
    <p><?php echo category_description(); ?></p>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post-summary">
      <?php /*if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail(array(120,91)); }*/ ?>
      <a href="<?php the_permalink() ?>" rel="bookmark"><?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail('thumbnail'); } ?></a>
      <h3><a href="<?php the_permalink() ?>" rel="bookmark">
        <?php the_title(); ?>
        </a></h3>
      
        <?php the_excerpt(__('Read more'));?>
      
    </div>
    <!--
	<?php trackback_rdf(); ?>
	-->
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <div class="webring">
      <?php /*
      <div class="back">
        <?php next_posts_link('&laquo; Previous Entries'); ?>
      </div>
      <div class="next">
        <?php previous_posts_link('Next Entries &raquo;'); ?>
      </div>
      */ ?>
			<div class="back"><?php previous_posts_link('&laquo; Next Entries') ?></div>
			<div class="next"><?php next_posts_link('Previous Entries &raquo;') ?></div>
    </div>
  </div>
  <!-- The main column ends  -->
</div>
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
<?php get_footer(); ?>
