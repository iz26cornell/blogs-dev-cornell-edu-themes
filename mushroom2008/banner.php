<!-- The following div contains the Cornell University logo with unit signature -->
<div id="cu-identity-wrap">
	<div id="cu-identity-content">
		<a id="insignia-link" href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/cu_logo_unstyled.gif" alt="Cornell University" border="0" /></a>
		

	
	<!-- 
		The search-form div contains a form that allows the user to search 
		either pages or people within cornell.edu directly from the banner.
	-->
	<div id="search-form" class="search-45">
					<label for="search-form-query">SEARCH CORNELL:</label>
					<input type="text" id="search-form-query" name="q" value="Search Cornell" size="20" onblur="this.value = this.value || this.defaultValue;" onfocus="this.value == this.defaultValue && (this.value =''); this.select()">
					<input type="submit" id="search-form-submit" name="submit" value="go">
				</div>
	
	
	</div> <!-- cu-identity-content -->
</div> <!-- cu-identity-wrap -->

<hr class="banner-separator" /> 
