<!-- begin sidebar -->

<div id="secondary-wrap">
  <div id="secondary">
    <div id="secondary-subscribe">
      <div id="announcement">
        <?php /*if (function_exists('display_my_news_announcement')) { display_my_news_announcement(0); }*/ ?>
      <?php if ( is_active_sidebar( 'footer-message' ) ) : ?>
			<?php dynamic_sidebar( 'footer-message' ); ?>
		<?php endif; ?>
      </div>
      <div class="feeds">
      <h3>Subscribe</h3>
      <a class="rss-entries" href="<?php bloginfo('rss2_url'); ?>">Entries</a> <a class="rss-comments" href="<?php bloginfo('comments_rss2_url'); ?>">Comments</a>
        <form id="rss-email" action="http://www.feedburner.com/fb/a/emailverify" method="post" target="popupwindow" onsubmit="window.open('http://www.feedburner.com', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
          <p>Or subscribe by email by entering your address:</p>
          <input type="text" name="email"/>
          <input type="hidden" value="http://feeds.feedburner.com/~e?ffid=590209" name="url"/>
          <input type="hidden" value="Cornell Mushroom Blog" name="title"/>
          <input type="submit" value="Subscribe" />
        </form>
      </div>
    </div>
    
      <div id="secondary-listings">
        <div id="categories">
          <h3>Categories</h3>
          <ul>
            <?php wp_list_cats('sort_column=name'); ?>
          </ul>
        </div>
        <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
        <div id="text-1">
          <?php if ( is_active_sidebar( 'footer-favorites' ) ) : ?>
				<?php dynamic_sidebar( 'footer-favorites' ); ?>
			<?php endif; ?>
        </div>
        <div id="text-2">
          <h3>More Information</h3>
          <ul>
            <?php get_links(-1, '<li>', '</li>', ' - '); ?>
          </ul>
        </div>
        <?php endif; ?>
      </div>

    <!-- <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. -->
  </div>
</div>
<!-- end sidebar -->
