<?php get_header(); ?>

<div id="wrap">
  <div id="content">
  <h2><?php wp_title(); ?></h2>
  
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post-summary"><a href="<?php the_permalink() ?>" rel="bookmark"><?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail(array(120,91)); } ?></a>
      <h3><a href="<?php the_permalink() ?>" rel="bookmark">
        <?php the_title(); ?>
        </a></h3>
      <p>
        <?php the_excerpt(__('Read more'));?>
      </p>
    </div>
    <!--
	<?php trackback_rdf(); ?>
	-->
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <div class="webring">
      <div class="back">
        <?php next_posts_link('&laquo; Previous Entries') ?>
      </div>
      <div class="next">
        <?php previous_posts_link('Next Entries &raquo;') ?>
      </div>
    </div>
  </div>
  <!-- The main column ends  -->
</div>
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
<?php get_footer(); ?>
