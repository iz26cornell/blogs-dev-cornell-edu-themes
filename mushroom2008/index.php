<?php get_header(); ?>

<div id="wrap">
  <div id="content">
    <div class="webring">
<?php /*    	
      <div class="back">
        <?php previous_post_link('&laquo; %link') ?>
      </div>
      <div class="next">
        <?php next_post_link('%link &raquo;') ?>
      </div>
*/ ?>
      <div class="back">
        <?php next_post_link('&laquo; %link') ?>
      </div>
      <div class="next">
        <?php previous_post_link('%link &raquo;') ?>
      </div>      
    </div>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h2><a href="<?php the_permalink() ?>" rel="bookmark">
      <?php the_title(); ?>
      </a></h2>
    <div class="postmetacategory">
      <div class="category">
        <?php the_time('F j, Y'); ?>
        | category:
        <?php the_category(', ') ?>
      </div>
      <div class="print-link">
        <?php if(function_exists('pf_show_link')){echo pf_show_link();} ?>
      </div>
    </div>
    <div id="post-details">
      <?php the_content(__('Read more'));?>
    </div>
    <p>
      <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
      <span>
      <?php edit_post_link('edit', '', ''); ?>
      </span></p>
    <!--

	<?php trackback_rdf(); ?>

	-->
    <h2>Comments</h2>
    <?php comments_template(); // Get wp-comments.php template ?>
    <?php endwhile; else: ?>
    <p>
      <?php _e('Sorry, no posts matched your criteria.'); ?>
    </p>
    <?php endif; ?>
    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
  </div>
  <!-- The main column ends  -->
</div>
<?php include(TEMPLATEPATH."/r_sidebar.php");?>
<?php get_footer(); ?>
