<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Header Template
 *
 *
 * @file           header.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.3
 * @filesource     wp-content/themes/responsive/header.php
 * @link           http://codex.wordpress.org/Theme_Development#Document_Head_.28header.php.29
 * @since          available since Release 1.0
 */
?>
<!doctype html>
<!--[if !IE]>      <html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

<title><?php wp_title('&#124;', true, 'right'); ?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<style type="text/css" media="screen">
<!--
@import url( <?php bloginfo('stylesheet_url');
?> );
-->
</style>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
                 
<?php responsive_container(); // before container hook ?>
<div id="container" class="hfeed">


<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity">
	<div id="cu-logo">
		<a id="insignia-link" href="http://www.cornell.edu/"><img src="http://iws-lamp-dev.hosting.cornell.edu/wordpress/stan/files/2013/07/cornell.gif" alt="Cornell University" /></a>
    </div>
    
   
	<div id="search-form">
			<form action="http://www.cit.cornell.edu/search.cfm" method="get" enctype="application/x-www-form-urlencoded">
            
            <div id="search-filters">
					<input type="radio" id="search-filters1" name="sitesearch" value="www.cit.cornell.edu" checked="checked" />
					<label for="search-filters1">This Site</label>
				
					<input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
					<label for="search-filters2">Cornell</label>
					
			</div>	
            
			<div id="search-input">
				<label for="search-form-query"></label>
				<input type="text" id="search-form-query" name="q" value="" size="20" />
				<input type="submit" id="search-form-submit" name="btnG" value="go" />
				<input type="hidden" name="output" value="xml_no_dtd" />
				<input type="hidden" name="sort" value="date:D:L:d1" />

				<input type="hidden" name="ie" value="UTF-8" />
				<input type="hidden" name="gsa_client" value="default_frontend" />
				<input type="hidden" name="oe" value="UTF-8" />
				<input type="hidden" name="site" value="default_collection" />
				<input type="hidden" name="proxystylesheet" value="default_frontend" />
				<input type="hidden" name="proxyreload" value="1"/>
                
			</div>

			
		</form>

	</div>
    
   <!--
   <form action="http://www.cornell.edu/search" method="GET" name="gs" class="formaction">

        <input type="text" name="q" value="" size="40" maxlength="256" class="textform"/>
        
        <input type="submit" name="btnG" value="Go" class="submitform"/>
        
        <!-- SITE SEARCH: to search specific sites, add URL in 'value' below -->
 <!--       
        <input type="hidden" name="as_sitesearch" value="www.cornellx.cornell.edu" />
 
	</form>
    -->
   
</div>
       
    <?php responsive_header(); // before header hook ?>
    <div id="header">

		<?php responsive_header_top(); // before header content hook ?>
    
        <?php if (has_nav_menu('top-menu', 'responsive')) { ?>
	        <?php wp_nav_menu(array(
				    'container'       => '',
					'fallback_cb'	  =>  false,
					'menu_class'      => 'top-menu',
					'theme_location'  => 'top-menu')
					); 
				?>
        <?php } ?>
        
    <?php responsive_in_header(); // header hook ?>
   
	<?php if ( get_header_image() != '' ) : ?>
               
        <div id="logo">
            <!--<a href="< ?php echo home_url('/'); ?>"> -->
            	<div class="headerImage" style="background-image:url(<?php header_image(); ?>); width:<?php if(function_exists('get_custom_header')) { echo get_custom_header() -> width;} else { echo HEADER_IMAGE_WIDTH;} ?>px; height:<?php if(function_exists('get_custom_header')) { echo get_custom_header() -> height;} else { echo HEADER_IMAGE_HEIGHT;} ?>px; max-width: 100%;" alt="<?php bloginfo('name'); ?>">
            
					<?php if ( (get_bloginfo('name') != '')  && (get_bloginfo('description') != '')) : ?>
            			<div class="shade"><h1 class="description"><a href="<?php echo get_settings('home'); ?>/" style="font-weight: 100; font-family: Georgia, serif;"><?php bloginfo('name'); ?></a></h1> <h2 class="description" style="font-weight: 100; font-family: Georgia, serif; padding: .5em; font-style: italic; color:rgb(255, 243, 195);"> <?php bloginfo('description'); ?></h2></div>
            		<?php endif; // both title and tagline are present ?>
            
					<?php if ( ((get_bloginfo('name') != '')  && (get_bloginfo('description') == ''))  || ((get_bloginfo('name') == '')  && (get_bloginfo('description') != '')) ) : ?>
            			<div class="shade" style="height: 26%; top:9em;"><h1 class="description"><a href="<?php echo get_settings('home'); ?>/" style="font-weight: 100; font-family: Georgia, serif;"><?php bloginfo('name'); ?></a></h1> <h2 class="description" style="font-weight: 100; font-family: Georgia, serif; padding: .5em; font-style: italic; color:rgb(255, 243, 195);"> <?php bloginfo('description'); ?></h2></div>
            		<?php endif; // either title and tagline are present ?>
            
           		</div>
            <!--</a>-->
             <!-- <div><h1 class="description">< ?php bloginfo('description'); ?></h1></div> -->
        </div><!-- end of #logo -->
        
    <?php endif; // header image was removed ?>

    <?php if ( !get_header_image() ) : ?>
                
        <div id="logo">
            <span class="site-name"><a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></span>
            <div class="description"><?php bloginfo('description'); ?></div>
            <span class="site-description"><?php bloginfo('description'); ?></span>
        </div><!-- end of #logo -->  

    <?php endif; // header image was removed (again) ?>
    
    <?php get_sidebar('top'); ?>
				<?php wp_nav_menu(array(
				    'container'       => 'div',
						'container_class'	=> 'main-nav',
						'fallback_cb'	  =>  'responsive_fallback_menu',
						'theme_location'  => 'header-menu')
					); 
				?>
                
            <?php if (has_nav_menu('sub-header-menu', 'responsive')) { ?>
	            <?php wp_nav_menu(array(
				    'container'       => '',
					'menu_class'      => 'sub-header-menu',
					'theme_location'  => 'sub-header-menu')
					); 
				?>
            <?php } ?>

			<?php responsive_header_bottom(); // after header content hook ?>
 
    </div><!-- end of #header -->
    <?php responsive_header_end(); // after header container hook ?>
    
	<?php responsive_wrapper(); // before wrapper container hook ?>
    <div id="wrapper" class="clearfix">
		<?php responsive_wrapper_top(); // before wrapper content hook ?>
		<?php responsive_in_wrapper(); // wrapper hook ?>