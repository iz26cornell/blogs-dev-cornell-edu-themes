<?php
/*
 WARNING: This file is part of the core Genesis framework. DO NOT edit
 this file under any circumstances. Please do all modifications
 in the form of a child theme.
 */

/**
 * Handles the header structure.
 *
 * This file is a core Genesis file and should not be edited.
 *
 * @category Genesis
 * @package  Templates
 * @author   StudioPress
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     http://www.studiopress.com/themes/genesis
 */

do_action( 'genesis_doctype' );
if ( is_front_page() ) {
	echo '<title>' . get_bloginfo('name') . ' - Welcome</title>';
}
else {
	do_action( 'genesis_title' );
}	
do_action( 'genesis_meta' );

/*** search form configuration ***/
	if (isset($_GET['btnG'])) {
		session_start();
		$selected_radio = $_GET['sitesearch'];
		
		if ($selected_radio == 'cornell') {
			$search_terms = urlencode($_GET['s']);
			$URL="http://www.cornell.edu/search" . "?q=" . $search_terms . "&client=default_frontend&proxystylesheet=default_frontend";
			print $URL;
			header ("Location: $URL");
		}
	}

?>

<!--[if IE 9]>
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/ie9.css" />
<![endif]-->

<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
	<script type="text/javascript">
		// Mobile Form
		jQuery(document).ready(function() {
			jQuery('#search_icon').click(function(){
				 jQuery('#cu-identity').slideToggle(200);
				 jQuery("#search_icon").toggleClass('graybg');
			});
		});
		// Mobile Menu
		jQuery(document).ready(function() {
			jQuery('#mobile_menu').click(function() {
				jQuery('#access').slideToggle(200); 
				jQuery("#mobile_menu").toggleClass('graybg');
			});
		});
		// Restore Standard Main Navigation
	   window.onresize = function(event) {
			if (jQuery(window).width() >= 650) {
				jQuery('#access').removeAttr('style');
				jQuery('#mobile_menu').removeClass('graybg');

			}
	   }
    </script>
<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity">
  <div class="cu-identity-wrap">
    <div id="cu-search">
     <div id="search-form">
         <div id="search-form-wrapper">
            <form method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" >
                <div id="search-input">
                    <label for="search-form-query">SEARCH:</label>
                    <input type="text" value="" name="s" id="search-form-query" size="26" />
                    <input type="submit" id="search-form-submit" name="btnG" value="go" />
                </div>              
                <div id="search-filters">
                        <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
                        <label for="search-filters1">This Site</label>
                    
                        <input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
                        <label for="search-filters2">Cornell</label>
                        <a href="http://www.cornell.edu/search/" id="options">more options</a>
                </div>	
            </form>
         </div>    
      </div>
  </div>
  </div>
</div><!-- end identity -->
<?php
do_action( 'genesis_before' );
?>
<hr class="red5" />    
<div id="skipnav"><a href="#content">Skip to main content</a></div>
<div id="wrap">
    <div id="identity">
    	<!--<div id="mobile_menu"></div>-->
    	<div id="search_icon"></div>
        <h1 id="ratings_img"></h1>
        <a id="insignia_link" href="http://www.cornell.edu" title="Cornell University"><img alt="Cornell University" id="insignia_img" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/project/cu_insignia.png" /></a>
        <a id="cu_logo_link"  href="http://www.cornell.edu" title="Cornell University"></a>
    </div>
<?php
do_action( 'genesis_before_header' );
if(!is_front_page()) { do_action( 'genesis_header' ); }
do_action( 'genesis_after_header' ); ?>
<?php echo '<div id="inner">';
genesis_structural_wrap( 'inner' ); ?>
