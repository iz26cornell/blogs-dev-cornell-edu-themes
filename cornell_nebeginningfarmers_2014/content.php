<article id="post-<?php the_ID(); ?>" <?php post_class('col-item'); ?>>
	<header class="entry-header">
		<?php if ( is_single() ) : ?>
		<h2 class="entry-title"><?php the_title(); ?></h2>
		<?php else : ?>
		<h2 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h2>
		<?php endif; // is_single() ?>

		<?php if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ) : ?>
			<?php if ( is_single() ) : ?>
                <div class="entry-thumbnail">
                    <?php the_post_thumbnail('medium'); ?>
                </div>
            <?php elseif ( is_search() || is_archive() || is_category() ) : ?>
                <div class="entry-thumbnail">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
                </div>
            <?php else : ?>
                <div class="entry-thumbnail">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('featured_cropped'); ?></a>
                </div>
			<?php endif; ?>
		<?php endif; ?>

	</header><!-- .entry-header -->

	<?php if ( is_search() || is_home() || is_archive() ) : // Only display Excerpts for search, archive and blog pages ?>
	<div class="entry-summary">
		<p><?php echo excerpt(35); ?></p>
	</div><!-- .entry-summary -->
	<div class="entry-meta">
			<?php //nebeginningfarmers_entry_meta(); ?>
	</div><!-- .entry-meta -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'nebeginningfarmers' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'nebeginningfarmers' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-meta">
			<?php if ( ! is_search() ) { edit_post_link( __( 'Edit', 'nebeginningfarmers' ), '<span class="edit-link">', '</span>' ); } ?>
		<?php if ( comments_open() && ! is_single() ) : ?>
			<div class="comments-link">
				<?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a comment', 'nebeginningfarmers' ) . '</span>', __( 'One comment so far', 'nebeginningfarmers' ), __( 'View all % comments', 'nebeginningfarmers' ) ); ?>
			</div><!-- .comments-link -->
		<?php endif; // comments_open() ?>

		<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
			<?php get_template_part( 'author-bio' ); ?>
		<?php endif; ?>
	</footer><!-- .entry-meta -->
</article><!-- #post -->