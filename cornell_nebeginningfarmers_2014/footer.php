<div id="footer">
	<div id="footer-content">
    
		<div id="footer-content-wrap">
        
			<div id="footer-left">
                <ul>
                    <li>&copy;<?php echo date("Y");?> <a href="http://www.cornell.edu/">Cornell University</a></li>
                    <?php wp_register(); ?>
                    <li class="last"><?php wp_loginout(); ?></li>
                </ul>
			</div>
            
			<div id="footer-right">
				<?php if ( is_active_sidebar( 'sidebar-4' ) ) { dynamic_sidebar( 'sidebar-4' ); } ?>
			</div>
            
		</div>
        
        <div id="footer-message-wrap">
			<?php if ( is_active_sidebar( 'sidebar-5' ) ) { dynamic_sidebar( 'sidebar-5' ); } ?>
        </div>
        
	</div>
</div>


	<?php wp_footer(); ?>
    

</body>
</html>
