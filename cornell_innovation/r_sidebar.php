<!-- begin r_sidebar -->
<div id="secondary">
  <div id="secondary-navigation">
    <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
    <div id="pages">
      <h3>In this section:</h3>
    <ul>
      <li><a href="<?php echo get_settings('home'); ?>">Home</a></li>
      <?php wp_list_pages('title_li=&depth=1'); ?>
    </ul>
  </div>
    <?php endif; ?>
  </div>
</div>
<!-- end r_sidebar -->
