<article id="post-<?php the_ID(); ?>" <?php if ( get_option('blog_layout') == 'multicolumnblog' ) { post_class('col-item'); } else { post_class(); } ?>>
	<header class="entry-header">
		<?php if ( is_single() ) : ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h1>
		<?php endif; // is_single() ?>

		<?php if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ) : ?>
			<?php if ( is_single() ) : ?>
                <div class="entry-thumbnail">
                    <?php the_post_thumbnail('medium'); ?>
                </div>
            <?php elseif ( is_search() || is_archive() || is_category() || (get_option('blog_layout') == 'singlecolumnblog') ) : ?>
                <div class="entry-thumbnail">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
                </div>
            <?php else : ?>
                <div class="entry-thumbnail">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('featured_cropped'); ?></a>
                </div>
			<?php endif; ?>
		<?php endif; ?>

	</header><!-- .entry-header -->

	<?php if ( is_search() || is_home() || is_archive() ) : // Only display Excerpts for search, archive and blog pages ?>
	<div class="entry-summary">
		<p><?php echo excerpt(35); ?></p>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'cornell_base' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'cornell_base' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<?php if ( is_search() || is_home() || is_archive() || is_single() ) : // Only display post meta for search, archive and blog pages ?>
    <div class="entry-meta">
		<?php cornell_base_entry_meta() ?>
    </div>
	<?php endif; ?>
    
	<footer class="entry-meta">
			<?php // if ( ! is_search() ) { edit_post_link( __( 'Edit', 'cornell_base' ), '<span class="edit-link">', '</span>' ); } ?>
		<?php if ( comments_open() && is_single() ) : ?>
			<div class="comments-link">
				<?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a comment', 'cornell_base' ) . '</span>', __( 'One comment so far', 'cornell_base' ), __( 'View all % comments', 'cornell_base' ) ); ?>
			</div><!-- .comments-link -->
		<?php endif; // comments_open() ?>

		<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
			<?php get_template_part( 'author-bio' ); ?>
		<?php endif; ?>
	</footer><!-- .entry-meta -->
</article><!-- #post -->