/* IWS Dynamic Components
   ------------------------------------------- */  

if (js_path == undefined) {
	var js_path = "js/framework/";
}

var iws_include = ["iws_popup.js",
						 "iws_tooltips.js",
						 "iws_expander.js"];

/* load resources */
function iws_load() {
	$.ajaxSetup({async: false});
	for (i=0;i<iws_include.length;i++) {
		$.getScript(js_path+iws_include[i]);
	}
}


/* initialize components */
function iws_init() {
	iws_load();
	
	// Headline Autoscale Variables
	var base_size;
	var base_width;
	var max_size = 72;
	var min_window_size = 0;
	
	// Window Size Tracking
	function resizeChecks() {
		
		// Restore Standard Main Navigation

		if ($(window).width() > 959) {
			$('#navigation ul').removeAttr('style');
			$('#navigation h3 em').removeClass('open');
			$('#cu-overlay').removeClass('overlay-visible');
		}
		
		// Refresh Headline Autoscale 
		if ($(window).width() > min_window_size) {
			$('.autosize-header .home #identity-content h1').addClass('autoscale');
			var multiplier = $('#identity-content').width() / base_width;
			if (multiplier > 0) {
				var new_size = base_size * multiplier;
				if (new_size > max_size) {
					new_size = max_size;
				}
				$('.autosize-header .home #identity-content h1').css('font-size',new_size+'px');
			}
		}
		else {
			$('.autosize-header .home #identity-content h1').removeAttr('style');
			$('.autosize-header .home #identity-content h1').removeClass('autoscale');
		}
	}
	
	$(window).load(function() {
		
		// Reinitialize Headline Autoscale (after remote webfonts load)
		$('.autosize-header .home #identity-content h1').removeAttr('style');
		base_width = $('.home #identity-content h1 span').width();
		resizeChecks();
		
		// Single-line News Headlines (move date to line two)
		$('.home #news h3').each(function( index ) {
  			if ($(this).next('h4.date').position().top - $(this).position().top < 8) {
  				$(this).find('a').append('<br />');
  			}
		});
		
	});
	
	$(document).ready(function() {
		popups();
		tooltips(100);
		expander();
		$('#content').fitVids();
		$('.col-item').fitVids();
		
		// Homepage Headline Autoscale
		base_size = parseInt($('.home #identity-content h1').css('font-size'));
		base_width = $('.home #identity-content h1 span').width();
		$(window).resize(resizeChecks);
		resizeChecks();	
		
		
		//calculate number of elements and add to parent columns class
		
		if (!window.console) window.console = {};
		if (!window.console.log) window.console.log = function () { }; //make this function work on IE9

		$('.columns').each(function(){ 
			var col_count = $(this).children('.col-item').length;
			if (col_count > 4) {
				col_count = 3;}
				console.log('col_count');
				$(this).addClass(' columns-'+col_count);
			});
		//match column heights
			$(function() {
				if ($(window).width() > 650) {
					$('.columns .col-item').matchHeight();
				}
			});
			
		//Adjustments for Safari on Mac
			if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
				$('html').addClass('safari-mac'); 
			}
		
		//calculate number of links in top navigation
		/*$('#cu-identity #navigation ul.top.nav-menu').each(function(){ 
			var link_count = $(this).children('li').length;
			console.log('link_count');
			if (link_count > 5) {
				$('#cu-identity').addClass('collapse');
			}
		});*/


		// Mobile Navigation
		var nav_offset = $('#navigation h3 em').offset();
		
		$('#navigation').find('ul.nav-menu').first().addClass('mobile-menu'); // standard
		//$('#navigation h3').next('div').find('ul.menu').first().addClass('mobile-menu'); // drupal
				
		$('#navigation h3 em').addClass('mobile-menu');
		$('#navigation h3 em').click(function() {
			$(this).toggleClass('open');
			$('#cu-overlay').toggleClass('overlay-visible');
			
			if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
				$(this).parent().parent().find('ul.nav-menu').slideToggle(100); 
			}
			else {
				$(this).parent().parent().find('ul.nav-menu').slideToggle(200); 
			}
			//$(this).parent().next('div').find('ul.menu').slideToggle(200); //drupal
		});
				
		//mobile nav toggle submenu		
		$('#navigation .nav-arrow').click(function() {
			$(this).toggleClass('closed open');
			$(this).closest('li').find('.sub-menu').toggleClass('closed open').slideToggle(200); 
			$(this).closest('li').find('.sub-menu li ul').hide(); 
		});
								
		//Lists widgets toggle submenu		
		$('.widget_pages .nav-arrow').click(function() {
			$(this).toggleClass('closed open');
			$(this).closest('li').find('.children:first').slideToggle(200); //pages widget
		});
		$('.widget_nav_menu .nav-arrow').click(function() {
			$(this).toggleClass('closed open');
			$(this).closest('li').find('.sub-menu:first').slideToggle(200); //custom menu widget
			$(this).closest('li').find('.sub-menu li ul').hide(); //custom menu widget
		});
								
		$('.widget_hier_page .nav-arrow').css( 'pointer-events','none' ).css('padding','0'); //hier page widget
								
		// add banner body class
		if ( $('#cu-identity').hasClass('theme_gray45') ) {
				$('body').addClass('theme_gray_45');
		}	
		else if ( $('#cu-identity').hasClass('theme_red45') ) {
				$('body').addClass('theme_red_45');
		}		
		else if ( $('#cu-identity').hasClass('theme_white45') ) {
				$('body').addClass('theme_white_45');
		}
		else if ( $('#cu-identity').hasClass('theme_white75') ) {
				$('body').addClass('theme_white_75');
		}
		
			
		/* Mobile default pages widget navigation (dropdown) */
		
		$("<select id = 'responsive-main-nav-menu' onchange = 'javascript:window.location.replace(this.value);'></select>").appendTo(".widget_pages");
		
		// Create default option 
		var widgettitle = $('.widget_pages h3');
		$("<option />", {
		   "selected": "selected",
		   "class"   : "widget-pages-title",
		   "value"   : "",
		   "text"    : widgettitle.text()
		}).appendTo("#responsive-main-nav-menu");
				
		mainNavChildren(jQuery(".widget_pages > ul") , 0);
		
		function mainNavChildren(parent , level){
			jQuery(parent).children("li").each(function(i , obj){
				var label = "";
				for(var k = 0 ; k < level ; k++){
					label += "&nbsp;&nbsp;&nbsp;&nbsp;";
				}
				label += jQuery(obj).children("a").text();
				jQuery("#responsive-main-nav-menu").append("<option value = '" + jQuery(obj).children("a").attr("href") + "'>" + label + "</option>");
				
				if(jQuery(obj).children("ul").size() == 1){
					mainNavChildren(jQuery(obj).children("ul") , level + 1);
				}
			});
		}
	

		/* Mobile hierarchical pages widget navigation (dropdown)*/
		
		$("<select id = 'responsive-hier-nav-menu' onchange = 'javascript:window.location.replace(this.value);'></select>").appendTo(".widget_hier_page");
		
		// Create default option 
		var widgetHiertitle = $('.widget_hier_page h3');
		$("<option />", {
		   "selected": "selected",
		   "class"   : "widget-hier-pages-title",
		   "value"   : "",
		   "text"    : widgetHiertitle.text()
		}).appendTo("#responsive-hier-nav-menu");
				
		mainHierNavChildren(jQuery(".widget_hier_page > ul") , 0);
		
		function mainHierNavChildren(parent , level){
			jQuery(parent).children("li").each(function(i , obj){
				var label = "";
				for(var k = 0 ; k < level ; k++){
					label += "&nbsp;&nbsp;&nbsp;&nbsp;";
				}
				label += jQuery(obj).children("a").text();
				jQuery("#responsive-hier-nav-menu").append("<option value = '" + jQuery(obj).children("a").attr("href") + "'>" + label + "</option>");
				
				if(jQuery(obj).children("ul").size() == 1){
					mainHierNavChildren(jQuery(obj).children("ul") , level + 1);
				}
			});
		}
	

		// Justified Navigation
		var nav_count = $('.nav-centered #navigation ul.menu').first().find('li').length;
		if (nav_count > 0) {
			var nav_width = 100 / nav_count;
			$('.nav-centered #navigation ul.menu').first().find('li').css( 'width',nav_width+'%');
		}
		
		/* Detect Increased User Font Size
		var user_font_size = window.getComputedStyle(document.getElementsByTagName('html')[0],null).getPropertyValue('font-size');
		if (parseInt(user_font_size) > 16) {
			$('body').addClass('user-font-scaled');
		}*/
		
		// Homepage Tabs
		$('.home #subfooter .tab').click(function(e){
			e.preventDefault();
			$('.home #subfooter .tab').removeClass('active');
			$(this).addClass('active');
			$('.tab-content').css('visibility','hidden');
			if ( $(this).hasClass('tab-about') ) {
				$('#about').css('visibility','visible');
			}
			else if ( $(this).hasClass('tab-contact') ) {
				$('#contact').css('visibility','visible');
			}
			else if ( $(this).hasClass('tab-videos') ) {
				$('#videos').css('visibility','visible');
			}
		});
		
		// Search
		$('#search-box').hide();
		$('#search-button a').click(function(e) {
			e.preventDefault();
			$(this).toggleClass('open');
			$('#search-box').slideToggle(300);
		});
		
		// RWD Submenu
		if ( $('#section-navigation').has('li').length ) {
			$('#navigation li.active').append($('#section-navigation').clone());
		}
		
	});
}