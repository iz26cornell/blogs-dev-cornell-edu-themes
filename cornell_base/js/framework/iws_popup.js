/* Popup Launcher 0.2b (ama39)
	-- added close button and temporary "small" and "medium" popup options (4/4/12)
	-- fixed IE iframe caching bug (4/5/12)
	-- fixed missing Close Button on IMG popups (5/8/12)
	-- minor RWD update (8/6/13)
   ------------------------------------------- */   

/* Global Options -------------- */
var popup_shadow = true;
var popup_fadein_speed = 200;

/* Global Variables ------------ */
var popup_count = 0;
var popup_type = "none";


/* -----------------------------------------------------------------------------------------
   Initialize Tooltips
   -----------------------------------------------------------------------------------------
   - 
-------------------------------------------------------------------------------------------- */
function popups() {
	
	// Create #popup node ---------- 
	$("body").append("<div id=\"popup-background\"></div>");
	$("#popup-background").css({
		"position": "fixed",
		"display": "none",
		"width": "100%",
		"height": "100%",
		"left": "0",
		"top": "0",
		"z-index": "2000" // resolves conflict with jQuery tabs()
	})
	$("#popup-background").click(function(e) {
		$("#popup").hide();
		$("#popup-background").hide();
	});
	
	$("#wrap").append("<div id=\"popup\"></div>");
	$("#popup").css({
		"position": "fixed",
		"display": "none",
		"z-index": "2001" // resolves conflict with jQuery tabs()
	});
	if (popup_shadow) {
		$("#popup").addClass("dropshadow"); // apply dropshadow preference		
	}
	
	// Setup click events ----------
	$(".popup").each(function() {
		popup_count++;
		$(this).data("popupID",popup_count);
		
		var popup_content = $(this).attr("href");
		var popup_caption = $(this).attr("title");
		
		$(this).click(function(e) {
			
			e.preventDefault();
			
			if (popup_content != "" && popup_content != undefined) {	
				
				var popup_store = document.getElementById("popup-store");
				if (popup_store == null) {
					$("body").append("<div id=\"popup-store\"></div>");
					$("#popup-store").css("display","none");
				}
				if ($("#popup").children().size() > 0 && popup_type == "id") {
					$("#popup-store").append($("#popup").children());
				}
				$("#popup").empty();
				$("#popup").unbind();
				
				var filetype = popup_content.substr(popup_content.lastIndexOf(".")).toLowerCase();
				if (filetype == ".jpg" || filetype == ".jpeg" || filetype == ".gif" || filetype == ".png") {
					popup_type = "image";
					var img = new Image();
					img.onload = function() {
						$("#popup").html("<img style=\"max-height:95%\" id=\"popup-image\" src=\""+popup_content+"\" />");
						
						if (popup_caption != "" && popup_caption != undefined) {
							//$("#log").append("<div>caption detected</div>");
							$("#popup").append("<p class=\"caption\">"+popup_caption+"</p>");
						}
						
						$("#popup").css({
							"left": ($(window).width()/2 - $("#popup").width()/2).toString()+"px",
							"top": ($(window).height()/2 - $("#popup").height()/2).toString()+"px"
						}).click(function(e) {
							$("#popup").hide();
							$("#popup-background").hide();
						}).fadeIn(popup_fadein_speed);
						closeButton();
					};
					img.src = popup_content;
					
					$("#popup-background").show();
					
					// Loading message could go here (for slow connections)
					
				}
				else {
					if (popup_content.indexOf("#") == 0) {
						popup_type = "id";
						//$("#log").append("<div>anchor detected</div>");
						$("#popup").append($(popup_content).show()).css({
							"left": ($(window).width()/2 - $("#popup").width()/2).toString()+"px",
							"top": ($(window).height()/2 - $("#popup").height()/2).toString()+"px"
						}).fadeIn(popup_fadein_speed);
						
						$("#popup-background").show();
					}
					else {
						popup_type = "iframe";
												
						$("#popup").html("<iframe src=\"" + popup_content + "\" frameborder=\"0\" scrolling=\"auto\" />");
						$("#popup iframe").attr("src",$("#popup iframe").attr("src")); // clears IE iframe caching bug
						
						if ($(this).hasClass("popup-small")) {
							$("#popup iframe").width(400);
							$("#popup iframe").height(250);
						}
						else if ($(this).hasClass("popup-medium")) {
							$("#popup iframe").width(600);
							$("#popup iframe").height(400);
						}
						else {
							$("#popup iframe").width(parseInt($(window).width()*0.8)); // 80% window width (temporary default)
							$("#popup iframe").height(parseInt($(window).height()*0.7)); // 70% window height (temporary default)
						}
						
						$("#popup").css({
							"left": ($(window).width()/2 - $("#popup").width()/2).toString()+"px",
							"top": ($(window).height()/2 - $("#popup").height()/2).toString()+"px"
						}).fadeIn(popup_fadein_speed);
						
						$("#popup-background").show();
					}
					closeButton();
				}
			}
		});
	});
}

function closeButton() {
	// Add Close Button
	$("#popup").append("<div id=\"popup-close\"></div>");
	$("#popup-close").click(function(e) {
		$("#popup").hide();
		$("#popup-background").hide();
	});
	// Preload Rollover Image ("_on")
	var on_state = $("#popup-close").css("background-image").split("url(")[1].split(")")[0];
	var on_state_part1 = on_state.substr(0,on_state.lastIndexOf("."));
	var on_state_part2 = on_state.substr(on_state.lastIndexOf("."));
	$(new Image()).attr("src",on_state_part1+'_on'+on_state_part2);
}