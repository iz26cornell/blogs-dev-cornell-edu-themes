<?php
/**
 * The main template file
 */

get_header(); ?>

    <div id="content">
    
        <div id="main">
    
            <div id="main-top"></div>
            
            <div id="main-body" class="columns">
                <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                <?php if ( have_posts() ) : ?>
                    <?php /* The loop */ ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part( 'content' ); ?>
                    <?php endwhile; ?>
        
                    <?php cornell_base_paging_nav(); ?>
        
                <?php else : ?>
                    <?php get_template_part( 'content', 'none' ); ?>
                <?php endif; ?>
            </div><!-- end #main-body -->
            
            <div id="main-bottom"></div>
            
        </div><!-- end #main -->
        
    </div><!-- end #content -->
            

</div><!-- end #content-wrap -->
</div><!-- end #wrap -->


<?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
    <div class="colorband tint-one">
        <div class="colorband-content">
            <h2 class="section-title"><span><?php echo get_theme_mod('section_one'); ?></span></h2>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-5' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'sidebar-6' ) ) : ?>
    <div class="colorband tint-two">
        <div class="colorband-content">
            <h2 class="section-title"><span><?php echo get_theme_mod('section_two'); ?></span></h2>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-6' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ( is_active_sidebar( 'sidebar-7' ) ) : ?>
    <div class="colorband tint-three">
        <div class="colorband-content">
            <h2 class="section-title"><span><?php echo get_theme_mod('section_three'); ?></span></h2>
            <div class="columns">
            	<?php dynamic_sidebar( 'sidebar-7' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>


<?php get_footer(); ?>