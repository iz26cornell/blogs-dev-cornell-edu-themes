<?php

/**
 * Add support for a custom header image.
 */
function cornell_base_custom_header_setup() {
	$args = array(
		// Text color and image (empty to use none).
		'default-text-color'     => '2c6c8d',
		'default-image'          => '%s/images/headers/header_image_02.jpg',

		// Set height and width, with a maximum value for the width.
		'width'                  => 1280,
		'height'                 => 540,

		// Callbacks for styling the header and the admin preview.
		'wp-head-callback'       => 'cornell_base_header_style',
		'admin-head-callback'    => 'cornell_base_admin_header_style',
		'admin-preview-callback' => 'cornell_base_admin_header_image',
	);

	add_theme_support( 'custom-header', $args );
	
	 /*
	 * Default custom header
	 * %s is a placeholder for the theme template directory URI.
	 */
	register_default_headers( array(
		'header_image_01' => array(
			'url'           => '%s/images/headers/header_image_01.jpg',
			'thumbnail_url' => '%s/images/headers/header_image_01_thumbnail.png',
			'description'   => _x( 'header_image_01', 'header image description', 'cornell_base' )
		),
		'header_image_02' => array(
			'url'           => '%s/images/headers/header_image_02.jpg',
			'thumbnail_url' => '%s/images/headers/header_image_02_thumbnail.png',
			'description'   => _x( 'header_image_02', 'header image description', 'cornell_base' )
		),
		'header_image_03' => array(
			'url'           => '%s/images/headers/header_image_03.jpg',
			'thumbnail_url' => '%s/images/headers/header_image_03_thumbnail.png',
			'description'   => _x( 'header_image_03', 'header image description', 'cornell_base' )
		),
		'header_image_05' => array(
			'url'           => '%s/images/headers/header_image_05.jpg',
			'thumbnail_url' => '%s/images/headers/header_image_05_thumbnail.png',
			'description'   => _x( 'header_image_05', 'header image description', 'cornell_base' )
		),
		'header_image_06' => array(
			'url'           => '%s/images/headers/header_image_06.jpg',
			'thumbnail_url' => '%s/images/headers/header_image_06_thumbnail.png',
			'description'   => _x( 'header_image_06', 'header image description', 'cornell_base' )
		),
		'header_image_07' => array(
			'url'           => '%s/images/headers/header_image_07.jpg',
			'thumbnail_url' => '%s/images/headers/header_image_07_thumbnail.png',
			'description'   => _x( 'header_image_07', 'header image description', 'cornell_base' )
		),
		'header_image_09' => array(
			'url'           => '%s/images/headers/header_image_09.jpg',
			'thumbnail_url' => '%s/images/headers/header_image_09_thumbnail.png',
			'description'   => _x( 'header_image_09', 'header image description', 'cornell_base' )
		),
		'header_image_10' => array(
			'url'           => '%s/images/headers/header_image_10.jpg',
			'thumbnail_url' => '%s/images/headers/header_image_10_thumbnail.png',
			'description'   => _x( 'header_image_10', 'header image description', 'cornell_base' )
		),
		'header_image_11' => array(
			'url'           => '%s/images/headers/header_image_11.jpg',
			'thumbnail_url' => '%s/images/headers/header_image_11_thumbnail.png',
			'description'   => _x( 'header_image_11', 'header image description', 'cornell_base' )
		),
		'header_image_12' => array(
			'url'           => '%s/images/headers/header_image_12.jpg',
			'thumbnail_url' => '%s/images/headers/header_image_12_thumbnail.png',
			'description'   => _x( 'header_image_12', 'header image description', 'cornell_base' )
		),
	) );
	
}
add_action( 'after_setup_theme', 'cornell_base_custom_header_setup', 11 );

define('NO_HEADER_TEXT', true );

/**
 * Style the header image on the front end
 */
function cornell_base_header_style() {

	$header_image = get_header_image();
	$tagline = get_theme_mod('display_tagline');
	$dropdowns = get_theme_mod('show_subnav');

	?>
	<style type="text/css" id="cornell_base-header-css">
	<?php
		if ( ! empty( $header_image ) ) {
	?>
		#image-band {
			float: left;
			width: 100%;
			clear: both;
			position: relative;
			background: url(<?php header_image(); ?>) no-repeat 0 0;
			<?php //adjust the vertical position of default header images
				if ( is_front_page() ) { 
					if ( strpos( $header_image,'header_image_01' ) ) { echo 'background-position: 0 21.52%;'; } 
					if ( strpos( $header_image,'header_image_02' ) ) { echo 'background-position: 0 77.04%;'; } 
					if ( strpos( $header_image,'header_image_03' ) ) { echo 'background-position: 0 27.14%;'; } 
					if ( strpos( $header_image,'header_image_04' ) ) { echo 'background-position: 0 79.04%;'; } 
					if ( strpos( $header_image,'header_image_05' ) ) { echo 'background-position: 0 59.28%;'; } 
					if ( strpos( $header_image,'header_image_06' ) ) { echo 'background-position: 0 49.90%;'; } 
					if ( strpos( $header_image,'header_image_07' ) ) { echo 'background-position: 0 69.66%;'; } 
					if ( strpos( $header_image,'header_image_08' ) ) { echo 'background-position: 0 19.76%;'; } 
					if ( strpos( $header_image,'header_image_09' ) ) { echo 'background-position: 0 42.52%;'; } 
					if ( strpos( $header_image,'header_image_10' ) ) { echo 'background-position: 0 39.52%;'; } 
					if ( strpos( $header_image,'header_image_11' ) ) { echo 'background-position: 0 31.83%;'; } 
					if ( strpos( $header_image,'header_image_12' ) ) { echo 'background-position: 0 47.90%;'; } 
					if ( strpos( $header_image,'header_image_13' ) ) { echo 'background-position: 0 43.21%;'; } 
				}
				else {
					if ( strpos( $header_image,'header_image_01' ) ) { echo 'background-position: 0 59.18%;'; } 
					if ( strpos( $header_image,'header_image_02' ) ) { echo 'background-position: 0 53.11%;'; } 
					if ( strpos( $header_image,'header_image_03' ) ) { echo 'background-position: 0 31.59%;'; } 
					if ( strpos( $header_image,'header_image_04' ) ) { echo 'background-position: 0 60.18%;'; } 
					if ( strpos( $header_image,'header_image_05' ) ) { echo 'background-position: 0 69.25%;'; } 
					if ( strpos( $header_image,'header_image_06' ) ) { echo 'background-position: 0 41.35%;'; } 
					if ( strpos( $header_image,'header_image_07' ) ) { echo 'background-position: 0 44.04%;'; } 
					if ( strpos( $header_image,'header_image_08' ) ) { echo 'background-position: 0 22.52%;'; } 
					if ( strpos( $header_image,'header_image_09' ) ) { echo 'background-position: 0 47.73%;'; } 
					if ( strpos( $header_image,'header_image_10' ) ) { echo 'background-position: 0 44.04%;'; } 
					if ( strpos( $header_image,'header_image_11' ) ) { echo 'background-position: 0 46.73%;'; } 
					if ( strpos( $header_image,'header_image_12' ) ) { echo 'background-position: 0 44.70%;'; } 
				}
			?>
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover !important;
		}

  <?php } if ( $dropdowns == 1 ) { ?> /* end if */
			#header #navigation-bar #navigation .sub-menu {
				display: none !important;
			}
   <?php } ?>


	</style>
    
<?php
	
} //end function

/**
 * Style the header image displayed on the Appearance > Header admin panel
 *
 */
function cornell_base_admin_header_style() {
	$header_image = get_header_image();
?>
	<style type="text/css" id="cornell_base-admin-header-css">
	.appearance_page_custom-header #headimg {
		padding: 0 20px;
		border: none;
		-webkit-box-sizing: border-box;
		-moz-box-sizing:    border-box;
		box-sizing:         border-box;
		<?php
		if ( ! empty( $header_image ) ) {
			echo 'background: url(' . esc_url( $header_image ) . ') no-repeat scroll top; background-size: 1280px auto;';
		} 
		?>
	}
	#headimg .home-link {
		-webkit-box-sizing: border-box;
		-moz-box-sizing:    border-box;
		box-sizing:         border-box;
		margin: 0 auto;
		max-width: 1040px;
		<?php
		if ( ! empty( $header_image ) || display_header_text() ) {
			echo 'min-height: 440px;';
		} ?>
		width: 100%;
	}
	#headimg h1,
	#headimg h2 {
		position: absolute !important;
		clip: rect(1px 1px 1px 1px); /* IE7 */
		clip: rect(1px, 1px, 1px, 1px);
	}
	#headimg h1 {
		margin: 0;
		padding: 58px 0 10px;
	}
	</style>

<?php }

/**
 * Output markup to be displayed on the Appearance > Header admin panel.
 *
 * This callback overrides the default markup displayed there.
 *
 */
function cornell_base_admin_header_image() {
	?>
	<div id="headimg" style="background: url(<?php header_image(); ?>) no-repeat scroll top; background-size: 1600px auto;">
		<?php $style = ' style="color:#' . get_header_textcolor() . ';"'; ?>
		<div class="home-link">
			<h1 class="displaying-header-text"><a id="name"<?php echo $style; ?> onclick="return false;" href="#"><?php bloginfo( 'name' ); ?></a></h1>
			<h2 id="desc" class="displaying-header-text"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></h2>
		</div>
	</div>
<?php }



/**
 * Cornell Base setup.
 *
 */
add_action( 'after_setup_theme', 'cornell_base_setup' );

if ( ! function_exists( 'cornell_base_setup' ) ):

function cornell_base_setup() {

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Switches default core markup for search form, comment form,
	 * and comments to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menu( 'primary', __( 'Main Navigation', 'cornell_base' ) );
    register_nav_menu( 'top-menu',__( 'Top Navigation', 'cornell_base' ) );

	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 670, 250, true );


	// This theme uses its own gallery styles.
	//add_filter( 'use_default_gallery_style', '__return_false' );


	// check to see if default theme settings have been applied yet
	$the_theme_status = get_option( 'theme_setup_status' );
	
	// If the theme has not yet been used we want to run our default settings.
	if ( $the_theme_status !== '1' ) {

			// Setup Default WordPress settings
			$core_settings = array(
					'default_comment_status'                        => 0,
					'default_ping_status'                           => 0,
					'default_pingback_flag'                         => 0,
					'date_format'                                   => 'F j, Y'
			);

			$theme_mods = array(
					'custom_banner'                                 => 'theme_white75',                                                   
					'display_site_name'                             => 1,                                               
					'display_tagline'                               => 1,                                               
					'layout'                                        => 'sidebar_right',                                                  
					'section_one'                                   => 'Section One',                                                  
					'section_two'                                   => 'Section Two',                                                  
					'section_three'                                 => 'Section Three',                                                  
					'facebook'                                      => 'facebook.com',                                                  
					'twitter'                                       => 'twitter.com',                                                  
					'youtube'                                       => 'youtube.com',                                                  
					'footer_text_textbox'                           => '&copy; 2014 Cornell University'                                                  
			);

			foreach ( $core_settings as $k => $v ) {
					update_option( $k, $v );
			}
			foreach ( $theme_mods as $key => $value ) {
					set_theme_mod( $key, $value );
			}

			//create home page
			$home_page_title = 'Home';
			$home_page_content = 'Welcome to the Home page of your new theme!<br /><br /><ol><li>You can edit your home page content <a href="' . $baseUrl . 'wp-admin/post.php?post=4&action=edit">here.</a></li><li>Some default site options have been preset, but you can change them here: <a href="' . $baseUrl . 'wp-admin/customize.php">Customizable Options</a></li><li>Navigation: add some pages or posts, then create your custom navigation menus on the Menus page <a href="' . $baseUrl . 'wp-admin/nav-menus.php">here.</a></li></ol>';
			$home_page_template = '';
	
			$home_page_check = get_page_by_title($home_page_title);
			$home_page = array(
					'post_type' => 'page',
					'post_title' => $home_page_title,
					'post_content' => $home_page_content,
					'post_status' => 'publish',
					'post_author' => 1,
			);
			if(!isset($home_page_check->ID)){
					$home_page_id = wp_insert_post($home_page);
					if(!empty($home_page_template)){
							update_post_meta($home_page_id, '_wp_page_template', $home_page_template);
					}
			}
			
			//delete default sample page
			wp_delete_post( 2, true );

			//create sample page 1
			$sample_page_1_title = 'Sample Page 1';
			$sample_page_1_content = '';
			$sample_page_1_template = '';
	
			$sample_page_1_check = get_page_by_title($sample_page_1_title);
			$sample_page_1_ = array(
					'post_type' => 'page',
					'post_title' => $sample_page_1_title,
					'post_content' => $sample_page_1_content,
					'post_status' => 'publish',
					'post_author' => 1,
			);
			if(!isset($sample_page_1_check->ID)){
					$sample_page_1_id = wp_insert_post($sample_page_1_);
					if(!empty($sample_page_1_template)){
							update_post_meta($sample_page_1_id, '_wp_page_template', $sample_page_1_template);
					}
			}

			//create sample page 2
			$sample_page_2_title = 'Sample Page 2';
			$sample_page_2_content = '';
			$sample_page_2_template = '';
	
			$sample_page_2_check = get_page_by_title($sample_page_2_title);
			$sample_page_2_ = array(
					'post_type' => 'page',
					'post_title' => $sample_page_2_title,
					'post_content' => $sample_page_2_content,
					'post_status' => 'publish',
					'post_author' => 1,
			);
			if(!isset($sample_page_2_check->ID)){
					$sample_page_2_id = wp_insert_post($sample_page_2_);
					if(!empty($sample_page_2_template)){
							update_post_meta($sample_page_2_id, '_wp_page_template', $sample_page_2_template);
					}
			}

			//create sample page 3
			$sample_page_3_title = 'Sample Page 3';
			$sample_page_3_content = '';
			$sample_page_3_template = '';
	
			$sample_page_3_check = get_page_by_title($sample_page_3_title);
			$sample_page_3_ = array(
					'post_type' => 'page',
					'post_title' => $sample_page_3_title,
					'post_content' => $sample_page_3_content,
					'post_status' => 'publish',
					'post_author' => 1,
			);
			if(!isset($sample_page_3_check->ID)){
					$sample_page_3_id = wp_insert_post($sample_page_3_);
					if(!empty($sample_page_3_template)){
							update_post_meta($sample_page_3_id, '_wp_page_template', $sample_page_3_template);
					}
			}

			// Force layout on theme activation
			$home = get_page_by_title( 'Home' );
			update_option( 'page_on_front', $home->ID );
			update_option( 'show_on_front', 'page' );
			update_option( 'layout', 'sidebar_right' );
			
			// MAIN MENU /////////////////////////////
			$menu_name = 'Main Menu';

			//check if it exists
			$menu_exists = wp_get_nav_menu_object( $menu_name );
			
			if(!$menu_exists) {
			
				//create it, if it doesn't exist
				$menu_id = wp_create_nav_menu($menu_name);
				
				//add pages
				$homePage = wp_update_nav_menu_item($menu_id, 0, array(
					'menu-item-title' =>  __('Home'),
					'menu-item-url' => home_url( '/' ), 
					'menu-item-status' => 'publish',
					'menu-item-position' => 1
				));
				$samplePage = wp_update_nav_menu_item($menu_id, 0, array(
					'menu-item-title' =>  __('Sample Page 1'),
					'menu-item-url' => home_url( '/sample-page-1/' ), 
					'menu-item-status' => 'publish',
					'menu-item-position' => 2
				));
				
			}

			//create item and menu relationship for each item
			wp_set_object_terms($homePage, $menu_id, 'nav_menu');
			wp_set_object_terms($samplePage, $menu_id, 'nav_menu');
			
			//set menu location
			$locations = get_theme_mod('nav_menu_locations');
			$locations['primary'] = $menu_id;
			set_theme_mod( 'nav_menu_locations', $locations );


			// TOP MENU //////////////////////////////////////////
			$top_menu_name = 'Top Menu';

			//check if it exists
			$top_menu_exists = wp_get_nav_menu_object( $top_menu_name );
			
			if(!$top_menu_exists) {
			
				//create it, if it doesn't exist
				$top_menu_id = wp_create_nav_menu($top_menu_name);
				
				//add pages
				$samplePage1 = wp_update_nav_menu_item($top_menu_id, 0, array(
					'menu-item-title' =>  __('Sample Page 1'),
					'menu-item-url' => home_url( '/sample-page-1/' ), 
					'menu-item-status' => 'publish',
					'menu-item-position' => 1
				));
				$samplePage2 = wp_update_nav_menu_item($top_menu_id, 0, array(
					'menu-item-title' =>  __('Sample Page 2'),
					'menu-item-url' => home_url( '/sample-page-2/' ), 
					'menu-item-status' => 'publish',
					'menu-item-position' => 2
				));
				$samplePage3 = wp_update_nav_menu_item($top_menu_id, 0, array(
					'menu-item-title' =>  __('Sample Page 3'),
					'menu-item-url' => home_url( '/sample-page-3/' ), 
					'menu-item-status' => 'publish',
					'menu-item-position' => 3
				));
				
			}

			//create item and menu relationship for each item
			wp_set_object_terms($samplePage1, $top_menu_id, 'nav_menu');
			wp_set_object_terms($samplePage2, $top_menu_id, 'nav_menu');
			wp_set_object_terms($samplePage3, $top_menu_id, 'nav_menu');
			
			//set top menu location
			$location = get_theme_mod('nav_menu_locations');
			$location['top-menu'] = $top_menu_id;
			set_theme_mod( 'nav_menu_locations', $location );

			//Once done, we register our setting to make sure we don't duplicate everytime we activate.
			update_option( 'theme_setup_status', '1' );

	} 
	
}
endif;



/**
 * Enqueue scripts and styles for the front end.
 *
 */
function cornell_base_scripts_styles() {
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	// Loads our main stylesheet.
	wp_enqueue_style( 'cornell_base-style', get_stylesheet_uri(), array(), '2013-07-18' );

}
add_action( 'wp_enqueue_scripts', 'cornell_base_scripts_styles' );

/**
 * Filter the page title.
 *
 */
function cornell_base_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'cornell_base' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'cornell_base_wp_title', 10, 2 );

/**
 * Register widget areas.
 *
 */
function cornell_base_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'cornell_base' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears on posts and pages in the sidebar area.', 'cornell_base' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Secondary Sidebar', 'cornell_base' ),
		'id'            => 'sidebar-4',
		'description'   => __( 'Appears on posts and pages just below the primary sidebar.', 'cornell_base' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
  	register_sidebar( array(
		'name'          => __( 'Header Widget Area', 'cornell_base' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Appears in the main header image area. Replaces the page title.', 'cornell_base' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	
	register_sidebar( array(
		'name'          => __( 'Home Page Widgets', 'cornell_base' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears on the Home page within the main content area and below the text.', 'cornell_base' ),
		'before_widget' => '<div class="col-item">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Section One Widgets', 'cornell_base' ),
		'id'            => 'sidebar-5',
		'description'   => __( 'Appears above the footer. Change the titles of these sections using the Section Titles tab on the Customize page (under the Appearance menu).', 'cornell_base' ),
		'before_widget' => '<div class="col-item">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Section Two Widgets', 'cornell_base' ),
		'id'            => 'sidebar-6',
		'description'   => __( 'Appears above the footer. Change the titles of these sections using the Section Titles tab on the Customize page (under the Appearance menu).', 'cornell_base' ),
		'before_widget' => '<div class="col-item">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Section Three Widgets', 'cornell_base' ),
		'id'            => 'sidebar-7',
		'description'   => __( 'Appears above the footer. Change the titles of these sections using the Section Titles tab on the Customize page (under the Appearance menu).', 'cornell_base' ),
		'before_widget' => '<div class="col-item">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'cornell_base' ),
		'id'            => 'sidebar-8',
		'description'   => __( 'Appears in the footer on all posts and pages (above the footer text area).', 'cornell_base' ),
		'before_widget' => '<div class="col-item">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
}
add_action( 'widgets_init', 'cornell_base_widgets_init' );

if ( ! function_exists( 'cornell_base_paging_nav' ) ) :

/**
 * Display navigation to next/previous set of posts when applicable.
 *
 */
function cornell_base_paging_nav() {
	global $wp_query;

	// Don't print empty markup if there's only one page.
	if ( $wp_query->max_num_pages < 2 )
		return;
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'cornell_base' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'cornell_base' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'cornell_base' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'cornell_base_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
*
*/
function cornell_base_post_nav() {
	global $post;

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'cornell_base' ); ?></h1>
		<div class="nav-links">

			<?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'cornell_base' ) ); ?>
			<?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'cornell_base' ) ); ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->

	<?php
}
endif;

if ( ! function_exists( 'cornell_base_entry_meta' ) ) :
/**
 * Print HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own cornell_base_entry_meta() to override in a child theme.
 *
 */
 
function cornell_base_entry_meta() {
	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post"><i class="fa fa-thumb-tack"></i>' . __( 'Sticky', 'cornell_base' ) . '</span>';

	if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
		cornell_base_entry_date();

	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'cornell_base' ) );
	if ( $categories_list ) {
		echo '<span class="categories-links"><i class="fa fa-folder-open-o"></i>' . $categories_list . '</span>';
	}

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'cornell_base' ) );
	if ( $tag_list ) {
		echo '<span class="tags-links"><i class="fa fa-tags"></i>' . $tag_list . '</span>';
	}

	/* Post author
	if ( 'post' == get_post_type() ) {
		printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'cornell_base' ), get_the_author() ) ),
			get_the_author()
		);
	}*/
}
endif;

if ( ! function_exists( 'cornell_base_entry_date' ) ) :
/**
 * Print HTML with date information for current post.
 *
 * Create your own cornell_base_entry_date() to override in a child theme.
 *
 *
 * @param boolean $echo (optional) Whether to echo the date. Default true.
 * @return string The HTML-formatted post date.
 */
function cornell_base_entry_date( $echo = true ) {
	if ( has_post_format( array( 'chat', 'status' ) ) )
		$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'cornell_base' );
	else
		$format_prefix = '%2$s';

	$date = sprintf( '<span class="date"><i class="fa fa-calendar-o"></i><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'cornell_base' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);

	if ( $echo )
		echo $date;

	return $date;
}
endif;

if ( ! function_exists( 'cornell_base_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 *
 */
function cornell_base_the_attached_image() {
	/**
	 * Filter the image attachment size to use.
	 *
		 *
	 * @param array $size {
	 *     @type int The attachment height in pixels.
	 *     @type int The attachment width in pixels.
	 * }
	 */
	$attachment_size     = apply_filters( 'cornell_base_attachment_size', array( 724, 724 ) );
	$next_attachment_url = wp_get_attachment_url();
	$post                = get_post();

	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID'
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		// get the URL of the next image attachment...
		if ( $next_id )
			$next_attachment_url = get_attachment_link( $next_id );

		// or get the URL of the first image attachment.
		else
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
	}

	printf( '<a href="../cornell_base/%1$s" title="%2$s" rel="attachment">%3$s</a>',
		esc_url( $next_attachment_url ),
		the_title_attribute( array( 'echo' => false ) ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;

/**
 * Return the post URL.
 *
 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or
 * the first link found in the post content.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @return string The Link format URL.
 */
function cornell_base_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );

	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}


/**
 * Determines if the currently viewed page is
 * one of the blog pages, including the blog home page, archive, category/tag, author, or single
 * post pages.
 *
 * @return bool
 */
function is_blog_page() {
	global  $post;
	$posttype = get_post_type( $post );
	
	if ( ( is_home() || is_category() || is_single() || is_archive() || is_author() || is_tag() ) && ( $posttype == 'post' ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */

function cornell_base_customize_register( $wp_customize ) {

	//Add postMessage support for site title and description for the Customizer.



    //footer text section
    $wp_customize->add_section( 'footer_text_section', array( 'title' => 'Footer Text', 'description' => 'Appears in the footer.', 'priority' => 999, ) );
	$wp_customize->add_setting( 'footer_text_textbox', array( 'default' => '&copy; 2014 Cornell University', ) );
	$wp_customize->add_control( 'footer_text_textbox', array( 'label' => 'Footer text', 'section' => 'footer_text_section', 'type' => 'text', ) );
	
    //social icons section
	$wp_customize->add_section( 'social_icons_section', array( 'title' => 'Social Icons', 'description' => 'Enter the url for the social icons you wish to include in the footer.', 'priority' => 995, ) );
	//social icons settings
	$wp_customize->add_setting( 'facebook', array( 'default' => 'facebook.com', ) );
	$wp_customize->add_setting( 'twitter', array( 'default' => 'twitter.com', ) );
	$wp_customize->add_setting( 'google_plus' );
	$wp_customize->add_setting( 'pinterest' );
	$wp_customize->add_setting( 'tumblr' );
	$wp_customize->add_setting( 'flickr' );
	$wp_customize->add_setting( 'linkedin' );
	$wp_customize->add_setting( 'instagram' );
	$wp_customize->add_setting( 'youtube' );
	$wp_customize->add_setting( 'vimeo' );
	//social icons controls
	$wp_customize->add_control( 'facebook', array( 'label' => 'Facebook', 'section' => 'social_icons_section', 'type' => 'text', ) );
	$wp_customize->add_control( 'twitter', array( 'label' => 'Twitter', 'section' => 'social_icons_section', 'type' => 'text', ) );
	$wp_customize->add_control( 'google_plus', array( 'label' => 'Google Plus', 'section' => 'social_icons_section', 'type' => 'text', ) );
	$wp_customize->add_control( 'pinterest', array( 'label' => 'Pinterest', 'section' => 'social_icons_section', 'type' => 'text', ) );
	$wp_customize->add_control( 'tumblr', array( 'label' => 'Tumblr', 'section' => 'social_icons_section', 'type' => 'text', ) );
	$wp_customize->add_control( 'flickr', array( 'label' => 'Flickr', 'section' => 'social_icons_section', 'type' => 'text', ) );
	$wp_customize->add_control( 'linkedin', array( 'label' => 'Linked In', 'section' => 'social_icons_section', 'type' => 'text', ) );
	$wp_customize->add_control( 'instagram', array( 'label' => 'Instagram', 'section' => 'social_icons_section', 'type' => 'text', ) );
	$wp_customize->add_control( 'youtube', array( 'label' => 'Youtube', 'section' => 'social_icons_section', 'type' => 'text', ) );
	$wp_customize->add_control( 'vimeo', array( 'label' => 'Vimeo', 'section' => 'social_icons_section', 'type' => 'text', ) );
	
	//remove old colors section
	$wp_customize->remove_control('header_textcolor');
	
    //add new colors section
    $wp_customize->add_section( 'header_textcolor', array( 'title' => 'Colors', 'description' => 'Change the site name color.', 'priority' => 35, ) );

    //color settings
	$wp_customize->add_setting( 'site_name_color', array( 'default' => '#2C6C8C', 'sanitize_callback' => 'sanitize_hex_color', ) );

    //color controls
	$wp_customize->add_control( 'site_name_color', array( 'label' => 'Color:', 'section' => 'header_textcolor', 'type' => 'radio', 'choices' => array( '#2c6c8d' => 'Blue', '#b31b1b' => 'Red', '#222222' => 'Gray', ), ) );
	
    //add section title options
    $wp_customize->add_section( 'section_title', array( 'title' => 'Section Titles', 'description' => 'Change (or remove) the section titles which appear in the widget areas above the footer. Add widgets to these areas on the Widgets screen under the Appearance menu. ', 'priority' => 950, ) );

    //section title settings
	$wp_customize->add_setting( 'section_one', array( 'default' => 'Section One', ) );
	$wp_customize->add_setting( 'section_two', array( 'default' => 'Section Two', ) );
	$wp_customize->add_setting( 'section_three', array( 'default' => 'Section Three', ) );

    //section title controls
	$wp_customize->add_control( 'section_one', array( 'label' => 'Section One', 'section' => 'section_title', 'type' => 'text', ) );
	$wp_customize->add_control( 'section_two', array( 'label' => 'Section Two', 'section' => 'section_title', 'type' => 'text', ) );
	$wp_customize->add_control( 'section_three', array( 'label' => 'Section Three', 'section' => 'section_title', 'type' => 'text', ) );
	

    //add cornell banner section
    $wp_customize->add_section( 'cornell_banner', array( 'title' => 'Banner', 'description' => 'Change the identity banner at the top of the page.', 'priority' => 5, ) );

    //cornell banner settings
	$wp_customize->add_setting( 'custom_banner', array( 'default' => 'theme_white75', ) );

    //cornell banner controls
	$wp_customize->add_control( 'custom_banner', array( 'type' => 'radio', 'label' => 'Choose a banner style:', 'section' => 'cornell_banner', 
		'choices' => array( 'theme_gray45' => 'Gray (45px)', 'theme_red45' => 'Red (45px)', 'theme_white45' => 'White (45px)', 'theme_white75' => 'Large red insignia (120px)', ), ) );

    //main nav dropdown settings
	$wp_customize->add_setting( 'show_subnav', array( 'default' => 'false', ) );

    //main nav dropdown controls
	$wp_customize->add_control( 'show_subnav', array( 'label' => __( 'Hide main navigation dropdowns.' ), 'section'  => 'nav', 'type' => 'checkbox', ) );
			
	$wp_customize->get_section('header_image')->description = __( 'You can upload new header images on the Header page under the Appearance menu.' );
	
	//just remove it!
	//$wp_customize->remove_section('header_image');
	$wp_customize->remove_control('display_header_text');
	
	//site name and tagline
	$wp_customize->add_setting( 'display_site_name', array( 'default' => 'true', ) );
	$wp_customize->add_setting( 'display_tagline', array( 'default' => 'true', ) );
	
	$wp_customize->get_control( 'blogname' )->priority  = 0;
	$wp_customize->get_control( 'blogdescription' )->priority  = 10;
	
	$wp_customize->add_control( 'display_site_name', array( 'label' => __( 'Display title' ), 'section'  => 'title_tagline', 'priority' => 5, 'type' => 'checkbox', ) );
	$wp_customize->add_control( 'display_tagline', array( 'label' => __( 'Display tagline' ), 'section'  => 'title_tagline', 'priority' => 15, 'type' => 'checkbox', ) );
	
    //add layout section
    $wp_customize->add_section( 'layout_section', array( 'title' => 'Layout', 'description' => 'Change the layout of the page. This will be applied to all new pages on the site and pages which have not yet been overidden (on the Edit Page screen).', 'priority' => 40, ) );

    //cornell layout settings
	$wp_customize->add_setting( 
		'layout' , array(
		'type'       => 'option',
		'default'    => 'sidebar_right',
		'transport'  => 'postMessage',
	) );

	$wp_customize->add_setting( 
		'blog_layout' , array(
		'type'       => 'option',
		'default'    => 'multicolumnblog',
		'transport'  => 'postMessage',
	) );

    //cornell layout controls
	$wp_customize->add_control( 'layout', array(
		'label'      => __( 'Choose a layout:', 'cornell_base' ),
		'section'    => 'layout_section',
		'type'       => 'radio',
		'choices'    => array(
			'onecolumn'     => 'No sidebar',
			'sidebar_left'  => 'Left Sidebar',
			'sidebar_right' => 'Right Sidebar',
			),
	) );
	$wp_customize->add_control( 'blog_layout', array(
		'label'      => __( 'Layout for main blog page:', 'cornell_base' ),
		'section'    => 'layout_section',
		'type'       => 'radio',
		'choices'    => array(
			'singlecolumnblog' => 'Single Column',
			'multicolumnblog' => 'Multiple Column',
			),
	) );
	

	if ( $wp_customize->is_preview() && ! is_admin() )
    add_action( 'wp_footer', 'cornell_base_customize_preview', 21);
	
				
}
add_action( 'customize_register', 'cornell_base_customize_register' );

function cornell_base_customize_preview() {
    ?>
    <script type="text/javascript">
    ( function( $ ){
    wp.customize('layout',function( value ) {
        value.bind(function(to) {
            $('body').removeClass('sidebar_right');
            $('body').removeClass('sidebar_left');
            $('body').removeClass('onecolumn');
            $('body').removeClass('twocolumn');
            $('body').addClass(to);
			if( $('body').hasClass('sidebar_left') || $('body').hasClass('sidebar_right') ) {
				$('body').addClass('twocolumn');
				$('#secondary-nav').show();
				$('#secondary').show();
			}
        });
    });
	wp.customize( 'blog_layout', function( value ) {
		value.bind( function(to) {
            $('body').removeClass('singlecolumnblog');
            $('body').removeClass('multicolumnblog');
            $('body').addClass(to);
			if( $('body').hasClass('singlecolumnblog') ) {
				$('body').addClass('singlecolumnblog');
			}
			if( $('body').hasClass('multicolumnblog') ) {
				$('body').addClass('multicolumnblog');
			}
		} );
	} );
	
    } )( jQuery )
    </script>
    <?php 
} 
 
 /**
 * Extend the default WordPress body classes.
 *
 */
function cornell_base_body_class( $classes ) {
		
	$layout =  get_option( 'layout' ); //global or default layout
	$blog_layout =  get_option('blog_layout'); //global or default layout
	$page_layout =  get_post_meta( get_the_ID(), 'layout_option', true ); //get page-specific layout (does not work for the posts page -- see below)
	
	$posts_page_id = get_option( 'page_for_posts' ); //get the post_id for the main posts page
	$posts_page_layout =  get_post_meta( $posts_page_id, 'layout_option', true ); //posts page layout
	if ( $posts_page_layout == '' ) {
		$posts_page_layout = 'right_sidebar';
	}
	
	$front_page_id = get_option( 'page_on_front' ); //get the post_id for the front page
	$front_page_layout =  get_post_meta( $front_page_id, 'layout_option', true ); //front page layout
	
	$home_page_id = get_page_by_title( 'Home' );
	$home_page_layout =  get_post_meta( $home_page_id->ID, 'layout_option', true ); //home page layout
	
	//posts page layout
	if ( is_home() && is_front_page() && $posts_page_layout == 'no_sidebar' )
		$classes[] = 'onecolumn';
	if ( is_home() && is_front_page() && $posts_page_layout == 'left_sidebar' )
		$classes[] = 'twocolumn sidebar_left';
	if ( is_home() && is_front_page() && $posts_page_layout == 'right_sidebar' )
		$classes[] = 'twocolumn sidebar_right';

	if ( is_home() && !is_front_page() && $posts_page_layout == 'no_sidebar' )
		$classes[] = 'onecolumn';
	if ( is_home() && !is_front_page() && $posts_page_layout == 'left_sidebar' )
		$classes[] = 'twocolumn sidebar_left';
	if ( is_home() && !is_front_page() && $posts_page_layout == 'right_sidebar' )
		$classes[] = 'twocolumn sidebar_right';

	if ( $blog_layout == 'singlecolumnblog' )
		$classes[] = 'singlecolumnblog';
	if ( $blog_layout == 'multicolumnblog' )
		$classes[] = 'multicolumnblog';

	//regular page layout	
	if ( ($layout == 'onecolumn' || $page_layout == 'no_sidebar') && ($page_layout != 'right_sidebar') && ($page_layout != 'left_sidebar') && (!is_home()) )
		$classes[] = 'onecolumn';

	if ( ($layout == 'sidebar_left' || $page_layout == 'left_sidebar') && ($page_layout != 'no_sidebar') && ($page_layout != 'right_sidebar') && (!is_home()) )
		$classes[] = 'sidebar_left twocolumn';

	if ( ($layout == 'sidebar_right' || $page_layout == 'right_sidebar') && ($page_layout != 'no_sidebar') && ($page_layout != 'left_sidebar') && (!is_home()) )
		$classes[] = 'sidebar_right twocolumn';
		
	//search page layout	
	if ( $layout == 'onecolumn' && is_search() )
		$classes[] = 'onecolumn';

	if ( $layout == 'sidebar_left' && is_search() )
		$classes[] = 'sidebar_left twocolumn';

	if ( $layout == 'sidebar_right' && is_search() )
		$classes[] = 'sidebar_right twocolumn';
		
	//misc.
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	if ( ! is_front_page() )
		$classes[] = 'secondary';
		
	if ( ! is_active_sidebar( 'sidebar-3' ) && is_active_sidebar( 'sidebar-4' ) )
		$classes[] = 'secondary-top-border';
		
	if ( ! is_active_sidebar( 'sidebar-3' ) && ! is_active_sidebar( 'sidebar-4' ) )
		$classes[] = 'onecolumn';
		
	return $classes;
}
add_filter( 'body_class', 'cornell_base_body_class' );


/*
Plugin Name: Hierarchical Pages
Version: 1.6.1
Plugin URI: http://www.wlindley.com/website/hierpage/
Description: Adds sidebar widgets to display a context-based list of "nearby" pages, and to display nested categories.
*/


if (!class_exists('SRCS_WP_Widget')) {
  class SRCS_WP_Widget extends WP_Widget
  {
    function form_html($instance) {
      $option_menu = $this->known_params(1);
      $tdom = 'hierarchical-pages';

      foreach (array_keys($option_menu) as $param) {
	$param_display[$param] = htmlspecialchars($instance[$param]);
      }

      foreach ($option_menu as $option_name => $option) {
	$checkval='';
	$desc = '';
	if (isset($option['desc']) && $option['desc'])
	  $desc = '<br /><small>' . __($option['desc'], $tdom) . '</small>';
	switch ($option['type']) {
	case 'checkbox':
	  if ($instance[$option_name]) // special HTML and override value
	    $checkval = 'checked="yes" ';
	  $param_display[$option_name] = 'yes';
	  break;
	case '':
	  $option['type'] = 'text';
	  break;
	}
	print '<p style="text-align:right;"><label for="' . $this->get_field_name($option_name) . '">' . 
	  __($option['title'], $tdom) . 
	  ' <input style="width: 200px;" id="' . $this->get_field_id($option_name) . 
	  '" name="' . $this->get_field_name($option_name) . 
	  "\" type=\"{$option['type']}\" {$checkval}value=\"{$param_display[$option_name]}\" /></label>$desc</p>";
      }
    }
  }
}

class HierPageWidget extends SRCS_WP_Widget
{
  /**
   * Declares the HierPageWidget class.
   *
   */
  function __construct(){
    $tdom = 'hierarchical-pages';
    $widget_ops = array('classname' => 'widget_hier_page',
			'description' => __( "Makes collapsing hierarchical pages/taxonomy lists: top level; ancestors, children, and/or siblings of the current page.", $tdom) );
    $control_ops = array('width' => 300, 'height' => 300);
    $this->WP_Widget('hierpage', __('Hierarchical Pages', $tdom), $widget_ops, $control_ops);
  }

  /**
   * Helper function
   *
   */
  function hierpages_list_pages($args = '') {
    global $post;
    global $wp_query;

    if ( !isset($args['echo']) )
      $args['echo'] = 1;

    $output = '';
    
    if(!isset($args['post_type']) || !$args['post_type'])
      $args['post_type'] = 'page';

    // Query pages.  NOTE: The array is sorted in alphabetical, or menu, order.
    $pages = & get_pages($args);

    $page_info = Array();

    if ( $pages ) {
      $current_post = $wp_query->get_queried_object_id();

      foreach ( $pages as $page ) {
	$page_info[$page->ID]['parent'] = $page->post_parent;
	$page_info[$page->post_parent]['children'][] = $page->ID;
      }

      // Display the front page?
      $front_page = -1; // assume no static front page
      if ('page' == get_option('show_on_front')) {
	$front_page = get_option('page_on_front');
	// Regard flag: always show front page?  Otherwise: Show front page only if it has children
	if (($args['show_home'] == 'yes') || (sizeof($page_info[$front_page]['children']))) {
	  $page_info[$front_page]['show'] = 1;	// always show front page
	}
      }
	
      // add all children of the root node, but only to single depth.
      if ($args['show_root'] == 'yes') {
	foreach ( $page_info[0]['children'] as $child ) {
	  if ($child != $front_page) {
	    $page_info[$child]['show'] = 1;
	  }
	}
      }
      
      if (is_post_type_hierarchical($args['post_type'])) {

	$page_count = 0;
	// show the current page's children, if any.
	if (isset($page_info[$current_post]['children']) && 
	    is_array($page_info[$current_post]['children'] )) {
	  foreach ( $page_info[$current_post]['children'] as $child ) {
	    $page_info[$child]['show'] = 1;
	    $page_count++;
	  }
	}

	$post_parent = $page_info[$current_post]['parent'];
	if ($post_parent && ($args['show_siblings'] == 'yes')) {
	  // if showing siblings, add the current page's parent's other children.
	  foreach ( $page_info[$post_parent]['children'] as $child ) {
	    if ($child != $front_page) {
	      $page_info[$child]['show'] = 1;
	      $page_count++;
	    }
	  }

	  // Also show parent node's siblings.
	  $post_grandparent = $page_info[$post_parent]['parent'];
	  if ($post_grandparent) {
	    foreach ( $page_info[$post_grandparent]['children'] as $child ) {
	      if ($child != $front_page) {
		$page_info[$child]['show'] = 1;
		$page_count++;
	      }
	    }
	  }
	}

	// add all ancestors of the current page.
	while ($post_parent) {
	  $page_info[$post_parent]['show'] = 1;
          // show that page's children, if any.                                                            
          if (is_array($page_info[$post_parent]['children'] )) {
            foreach ( $page_info[$post_parent]['children'] as $child ) {
              $page_info[$child]['show'] = 1;
	      $page_count++;
            }
          }
	  $post_parent = $page_info[$post_parent]['parent'];
	}

	if (($post->ID != $front_page) && ($page_count > 0) ) {
	  // The current page is always shown, unless
	  // 1. it is the static front page (see above), or
	  // 2. no other pages are displayed
	  $page_info[$post->ID]['show'] = 1;
	}

      }
      
      // Add pages that were selected
      $my_includes = Array();

      foreach ( $pages as $page ) {
	if (isset($page_info[$page->ID]['show']) && $page_info[$page->ID]['show']) {
	  $my_includes[] = $page->ID;
	}
      }
      if (isset($args['child_of']) && $args['child_of']) {
        $my_includes[] = $args['child_of'];
      }
      
      if (!empty($my_includes)) {
        // List pages, if any. Blank title_li suppresses unwanted elements.
        $output .= wp_list_pages( Array('title_li' => '',
					'sort_column' => $args['sort_column'],
					'sort_order' => $args['sort_order'],
					'include' => $my_includes,
                    'post_type'=> $args['post_type'],
                    'walker'=> new Hier_Walker_Page(),
					'echo' => $args['echo']
                                        ) );
      }
    }

    $output = apply_filters('wp_list_pages', $output);
    
    if ( $args['echo'] )
      echo $output;
    else
      return $output;
  }

  /**
   * Displays the Widget
   *
   */
  function widget($args, $instance){

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $known_params = $this->known_params(0);
    foreach ($known_params as $param) {
      if (strlen($instance[$param])) {
	$page_options[$param] = $instance[$param];
      }
    }
      
    if (isset($instance['menu_order']) && $instance['menu_order'] == 'yes') { 
      // Deprecated, eliminated upon form display (see below)
      $page_options['sort_column']='menu_order,post_title';
    }

    $page_options['echo'] = 0;
    $output = $this->hierpages_list_pages($page_options);
    if (strlen($output)) {
      print $args['before_widget'];
      if ( $title )

	print "{$args['before_title']}{$title}{$args['after_title']}";
      print "<ul>{$output}</ul>{$args['after_widget']}";
    }
  }

  function known_params ($options = 0) {
    $option_menu = array('title' => array('title' => 'Title:'),
			 'show_siblings' => array('title' => 'Show siblings to the current page?',
						  'type' => 'checkbox'),
			 'show_root' => array('title' => 'Always show top-level pages?',
					      'type' => 'checkbox'),
			 'show_home' => array('title' => 'Show the static home page?',
					      'desc' => '(always shown if it has child pages)',
					      'type' => 'checkbox'),
			 'child_of' => array('title' => 'Root page ID:'),
			 'exclude' => array('title' => 'Exclude pages:',
					    'desc' => 'List of page IDs to exclude'),
			 'sort_column' => array('title' => 'Sort field:',
						'desc' => 'Comma-separated list: <em>post_title, menu_order, post_date, post_modified, ID, post_author, post_name</em>'),
			 'sort_order' => array('title' => 'Sort direction:',
					       'desc' => '(default: ASC)'),
			 'meta_key' => array('title' => 'Meta Key:'),
			 'meta_value' => array('title' => 'Meta-key Value:',
					     'desc' => 'for selecting pages by custom fields'),
			 'authors' => array('title' => 'Authors:'),
			 'post_status' => array('title' => 'Post status:',
						'desc' => '(default: publish)'),
			 'post_type' => array('title' => 'Post type:',
						'desc' => '(default: page)'),
			 );
    return ($options ? $option_menu : array_keys($option_menu));
  }

  /**
   * Saves the widget's settings.
   *
   */
  function update($new_instance, $old_instance){
    $instance = $old_instance;
    $known_params = $this->known_params();
    unset($instance['menu_order']);
    foreach ($known_params as $param) {
      $instance[$param] = strip_tags(stripslashes($new_instance[$param]));
    }
    $instance['sort_order'] = strtolower($instance['sort_order']) == 'desc'?'DESC':'ASC';
    return $instance;
  }

  /**
   * Creates the edit form for the widget.
   *
   */
  function form($instance){
    $instance = wp_parse_args( (array) $instance, array('title'=>'') );
    if ($instance['menu_order']) {
      $instance['sort_column'] = 'menu_order,post_title';
    }
    if (empty($instance['sort_column'])) {
      $instance['sort_column'] = 'post_title';
    }

    $this->form_html($instance);
  }

}// END class

/**
 * Register Hierarchical Pages widget.
 *
 * Calls 'widgets_init' action after the widget has been registered.
 */
function HierPageInit() {
  register_widget('HierPageWidget');
}

function HierPageLoad() {
  $plugin_dir = basename(dirname(__FILE__));
  load_plugin_textdomain('hierarchical-pages', false, $plugin_dir . '/languages');
}

function is_tree($pid) {      // $pid = The ID of the page we're looking for pages underneath
	global $post;         // load details about this page
	if(is_page()&&($post->post_parent==$pid||is_page($pid))) 
               return true;   // we're at the page or at a sub page
	else 
               return false;  // we're elsewhere
}
/**
 * Create HTML list of pages.
 *
 */
class Hier_Walker_Page extends Walker {
	
	var $tree_type = 'page';
	var $db_fields = array ('parent' => 'post_parent', 'id' => 'ID');

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class='children'>\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	function start_el( &$output, $page, $depth = 0, $args = array(), $current_page = 0 ) {
		if ( $depth )
			$indent = str_repeat("\t", $depth);
		else
			$indent = '';

		extract($args, EXTR_SKIP);
		$css_class = array('page_item', 'page-item-'.$page->ID);
		
		$children = get_pages('child_of=' . $page->ID);

		if( isset( $args['pages_with_children'][ $page->ID ] ) || $children )
			$css_class[] = 'page_item_has_children';

		if ( !empty($current_page) ) {
			$_current_page = get_post( $current_page );
			if ( in_array( $page->ID, $_current_page->ancestors ) )
				$css_class[] = 'current_page_ancestor';
			if ( $page->ID == $current_page )
				$css_class[] = 'current_page_item';
			elseif ( $_current_page && $page->ID == $_current_page->post_parent )
				$css_class[] = 'current_page_parent';
		} elseif ( $page->ID == get_option('page_for_posts') ) {
			$css_class[] = 'current_page_parent';
		}

		$css_class = implode( ' ', apply_filters( 'page_css_class', $css_class, $page, $depth, $args, $current_page ) );

		if ( '' === $page->post_title )
			$page->post_title = sprintf( __( '#%d (no title)' ), $page->ID );

		/** This filter is documented in wp-includes/post-template.php */
		$output .= $indent . '<li class="' . $css_class . '"><a href="' . get_permalink($page->ID) . '">' . $link_before . apply_filters( 'the_title', $page->post_title, $page->ID ) . $link_after . '</a><div class="nav-arrow closed"></div>';

		if ( !empty($show_date) ) {
			if ( 'modified' == $show_date )
				$time = $page->post_modified;
			else
				$time = $page->post_date;

			$output .= " " . mysql2date($date_format, $time);
		}
	}

	function end_el( &$output, $page, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}

}

  /*
   * Plugin Name: Hierarchical Categories (combined with Hierarchical Pages)
   * Plugin URI: http://www.wlindley.com/
   * Description: Adds a sidebar widget to display a context-based list of "nearby" categories.
   * Author: William Lindley
   * Author URI: http://www.wlindley.com/
   */

class HierCatWidget extends SRCS_WP_Widget
{
  /**
   * Declares our class.
   *
   */
  function __construct(){
    $tdom = 'hierarchical-pages';
    $widget_ops = array('classname' => 'widget_hier_cat', 'description' => __( "Makes collapsing hierarchical category/taxonomy lists: top level; ancestors, children, and/or siblings of the current category.", $tdom) );
    $control_ops = array('width' => 300, 'height' => 300);
    $this->WP_Widget('hiercat', __('Hierarchical Categories', $tdom), $widget_ops, $control_ops);
  }

  /**
   * Helper function
   *
   */
  function hiercat_list_cats($args) {
    global $post;
    global $wp_query;

    if ( !isset($args['echo']) )
      $args['echo'] = 1;
    $sort_column = $args['sort_column'];

    $output = '';

    // Query categories.
    $cats = & get_categories($args);
    if ($cats['errors']) {
      print "<pre>"; print_r($cats); print "</pre>";
      return;
    }
    $cat_info = Array();

    if ( !empty ($cats) ) {
      $current_cat = $wp_query->get_queried_object_id();

      foreach ( $cats as $cat ) {
	$cat_info[$cat->term_id]['parent'] = $cat->category_parent;
	$cat_info[$cat->category_parent]['children'][] = $cat->term_id;
      }

      // add all children of the root node, but only to single depth.
      foreach ( $cat_info[0]['children'] as $child ) {
	$cat_info[$child]['show'] = 1;
      }
      
      // If currently displaying a category, taxonomy, or tag; AND
      // if it is the same as the one in this widget...
      if ((is_category() || is_tax() || is_tag()) && 
	  ($args['taxonomy'] == get_queried_object()->taxonomy ) ) {
	// show the current category's children, if any.
	if (is_array($cat_info[$current_cat]['children'] )) {
	   foreach ( $cat_info[$current_cat]['children'] as $child ) {
	      $cat_info[$child]['show'] = 1;
	   }
	}

	$cat_parent = $cat_info[$current_cat]['parent'];
	if ($cat_parent && ($args['show_siblings'] == 'yes')) {
	  // if showing siblings, add the current category's parent's other children.
	  foreach ( $cat_info[$cat_parent]['children'] as $child ) {
	    $cat_info[$child]['show'] = 1;
	  }

	  // Also show parent node's siblings.
	  $cat_grandparent = $cat_info[$cat_parent]['parent'];
	  if ($cat_grandparent) {
	    foreach ( $cat_info[$cat_grandparent]['children'] as $child ) {
		$cat_info[$child]['show'] = 1;
	    }
	  }
	}
	
	// add all ancestors of the current category.
	while ($cat_parent) {
	  $cat_info[$cat_parent]['show'] = 1;
	  $cat_parent = $cat_info[$cat_parent]['parent'];
	}
      }
      
      $my_includes = Array();
      // Add categories that were selected
      foreach ( $cats as $cat ) {
	if ($cat_info[$cat->term_id]['show']) {
	  $my_includes[] =$cat->term_id;
	}
      }
      
      if (!empty($my_includes)) {
        // List categories, if any. Blank title_li suppresses unwanted elements.
        $qargs = Array('title_li' => '', 'hide_empty' => 0, 'include' => $my_includes,
                      'order' => $args['order'], 'orderby' => $args['orderby'],
                      'show_count' => $args['show_count']);
        if (!empty($args['taxonomy'])) {
          $qargs['taxonomy'] = $args['taxonomy'];
        }
        $output .= wp_list_categories( $qargs );
      }
    }

    $output = apply_filters('wp_list_categories', $output);
    
    if ( $args['echo'] )
      echo $output;
    else
      return $output;
  }

  function known_params ($options = 0) {
    $option_menu = array('title' => array('title' => 'Title:'),
			 'show_siblings' => array('title' => 'Show siblings to the current category?',
						  'type' => 'checkbox'),
			 'include' => array('title' => 'Include:',
					    'desc' => 'Comma-delimited list of category IDs, or blank for all'),
			 'exclude' => array('title' => 'Exclude:'),
			 'orderby' => array('title' => 'Sort field:',
					    'desc' => 'Enter one of: <em>name, count, term_group, slug</em> or a custom value. Default: name'),
			 'order' => array('title' => 'Sort direction:',
					  'desc' => '(default: ASC)'),
			 'child_of' => array('title' => 'Only display Categories below this ID'),
			 'hide_empty' => array('title' => 'Hide empty categories?', 
						  'type' => 'checkbox'),
			 'show_count' => array('title' => 'Show count of category entries?', 
						  'type' => 'checkbox'),
			 'taxonomy' => array('title' => 'Custom taxonomy:'),
			 );
    if ($options) {
      $taxons = get_taxonomies();
      $option_menu['taxonomy']['desc'] = 'Enter one of: <em>' . 
	implode(', ',array_keys($taxons)) . '</em> or blank for post categories.';
    }
    return ($options ? $option_menu : array_keys($option_menu));
  }
  /**
   * Displays the Widget
   *
   */
  function widget($args, $instance){
    $known_params = $this->known_params(0);
    foreach ($known_params as $param) {
      if (strlen($instance[$param]))
	$cat_options[$param] = $instance[$param];
    }
    $cat_options['title'] = apply_filters('widget_title', $cat_options['title']);
    // WordPress defaults to hiding: thus, always specify.
    $cat_options['hide_empty'] = $cat_options['hide_empty'] == 'yes' ? 1 : 0;
      
    print $args['before_widget'];
    if ( strlen($cat_options['title']) )
      print "{$args['before_title']}{$cat_options['title']}{$args['after_title']}";
    print "<ul>";

    $this->hiercat_list_cats($cat_options);
    print "</ul>{$args['after_widget']}";
  }

  /**
   * Saves the widget's settings.
   *
   */
  function update($new_instance, $old_instance){
    $instance = $old_instance;

    $known_params = $this->known_params();
    foreach ($known_params as $param) {
      $instance[$param] = strip_tags(stripslashes($new_instance[$param]));
    }
    return $instance;
  }

  /**
   * Creates the edit form for the widget.
   *
   */
  function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('title'=>'') );

    $this->form_html($instance);
  }

}// END class

/**
 * Register Hierarchical Categories widget.
 *
 * Calls 'widgets_init' action after the widget has been registered.
 */
function HierCatInit() {
  register_widget('HierCatWidget');
}

/*
 * Initialize both widgets
 */

add_action('widgets_init', 'HierCatInit');
add_action('plugins_loaded', 'HierPageLoad');
add_action('widgets_init', 'HierPageInit');



//custom nav menu
class Custom_Walker_Nav_Menu extends Walker {

	var $tree_type = array( 'post_type', 'taxonomy', 'custom' );
	var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<div class='nav-arrow closed'></div><ul class=\"sub-menu closed\">\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		/** This filter is documented in wp-includes/post-template.php */
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}

} 
function cornell_base_custom_walker( $args ) {
    return array_merge( $args, array(
        'walker' => new Custom_Walker_Nav_Menu(),
    ) );
}
add_filter( 'wp_nav_menu_args', 'cornell_base_custom_walker' );



//pages menu
class Pages_Widget_Walker extends Walker {
	var $tree_type = 'page';

	var $db_fields = array ('parent' => 'post_parent', 'id' => 'ID');

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class='children closed'>\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	function start_el( &$output, $page, $depth = 0, $args = array(), $current_page = 0 ) {
		if ( $depth )
			$indent = str_repeat("\t", $depth);
		else
			$indent = '';

		extract($args, EXTR_SKIP);
		$css_class = array('page_item', 'page-item-'.$page->ID);

		if( isset( $args['pages_with_children'][ $page->ID ] ) )
			$css_class[] = 'page_item_has_children';

		if ( !empty($current_page) ) {
			$_current_page = get_post( $current_page );
			if ( in_array( $page->ID, $_current_page->ancestors ) )
				$css_class[] = 'current_page_ancestor';
			if ( $page->ID == $current_page )
				$css_class[] = 'current_page_item';
			elseif ( $_current_page && $page->ID == $_current_page->post_parent )
				$css_class[] = 'current_page_parent';
		} elseif ( $page->ID == get_option('page_for_posts') ) {
			$css_class[] = 'current_page_parent';
		}

		$css_class = implode( ' ', apply_filters( 'page_css_class', $css_class, $page, $depth, $args, $current_page ) );

		if ( '' === $page->post_title )
			$page->post_title = sprintf( __( '#%d (no title)' ), $page->ID );

		/** This filter is documented in wp-includes/post-template.php */
		$output .= $indent . '<li class="' . $css_class . '"><a href="' . get_permalink($page->ID) . '">' . $link_before . apply_filters( 'the_title', $page->post_title, $page->ID ) . $link_after . '</a><div class="nav-arrow closed"></div>';

		if ( !empty($show_date) ) {
			if ( 'modified' == $show_date )
				$time = $page->post_modified;
			else
				$time = $page->post_date;

			$output .= " " . mysql2date($date_format, $time);
		}
	}

	function end_el( &$output, $page, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}

}

function pages_widget_custom_walker( $args ) {
    return array_merge( $args, array(
        'walker' => new Pages_Widget_Walker(),
    ) );
}
add_filter( 'widget_pages_args', 'pages_widget_custom_walker' );





//get theme menu name
function get_theme_menu_name( $theme_location ) {
	if( ! $theme_location ) return false;
 
	$theme_locations = get_nav_menu_locations();
	if( ! isset( $theme_locations[$theme_location] ) ) return false;
 
	$menu_obj = get_term( $theme_locations[$theme_location], 'nav_menu' );
	if( ! $menu_obj ) $menu_obj = false;
	if( ! isset( $menu_obj->name ) ) return false;
 
	return $menu_obj->name;
}


/**
 * Plugin Name: Display Posts Shortcode
 * Plugin URI: http://www.billerickson.net/shortcode-to-display-posts/
 * Description: Display a listing of posts using the [display-posts] shortcode
 * Version: 2.4
 */
 
 
/**
 * To Customize, use the following filters:
 *
 * `display_posts_shortcode_args`
 * For customizing the $args passed to WP_Query
 *
 * `display_posts_shortcode_output`
 * For customizing the output of individual posts.
 * Example: https://gist.github.com/1175575#file_display_posts_shortcode_output.php
 *
 * `display_posts_shortcode_wrapper_open` 
 * display_posts_shortcode_wrapper_close`
 * For customizing the outer markup of the whole listing. By default it is a <ul> but
 * can be changed to <ol> or <div> using the 'wrapper' attribute, or by using this filter.
 * Example: https://gist.github.com/1270278
 */ 
 
// Create the shortcode
add_shortcode( 'display-posts', 'be_display_posts_shortcode' );
function be_display_posts_shortcode( $atts ) {

	// Original Attributes, for filters
	$original_atts = $atts;

	// Pull in shortcode attributes and set defaults
	$atts = shortcode_atts( array(
		'title'              => '',
		'author'              => '',
		'category'            => '',
		'date_format'         => '(n/j/Y)',
		'display_posts_off'   => false,
		'exclude_current'     => false,
		'id'                  => false,
		'ignore_sticky_posts' => false,
		'image_size'          => false,
		'include_title'       => true,
		'include_author'      => false,
		'include_content'     => false,
		'include_date'        => false,
		'include_excerpt'     => false,
		'meta_key'            => '',
		'meta_value'          => '',
		'no_posts_message'    => '',
		'offset'              => 0,
		'order'               => 'DESC',
		'orderby'             => 'date',
		'post_parent'         => false,
		'post_status'         => 'publish',
		'post_type'           => 'post',
		'posts_per_page'      => '10',
		'tag'                 => '',
		'tax_operator'        => 'IN',
		'tax_term'            => false,
		'taxonomy'            => false,
		'wrapper'             => 'ul',
		'wrapper_class'       => 'display-posts-listing',
		'wrapper_id'          => false,
	), $atts, 'display-posts' );
	
	// End early if shortcode should be turned off
	if( $atts['display_posts_off'] )
		return;

	$shortcode_title = sanitize_text_field( $atts['title'] );
	$author = sanitize_text_field( $atts['author'] );
	$category = sanitize_text_field( $atts['category'] );
	$date_format = sanitize_text_field( $atts['date_format'] );
	$exclude_current = be_display_posts_bool( $atts['exclude_current'] );
	$id = $atts['id']; // Sanitized later as an array of integers
	$ignore_sticky_posts = be_display_posts_bool( $atts['ignore_sticky_posts'] );
	$image_size = sanitize_key( $atts['image_size'] );
	$include_title = be_display_posts_bool( $atts['include_title'] );
	$include_author = be_display_posts_bool( $atts['include_author'] );
	$include_content = be_display_posts_bool( $atts['include_content'] );
	$include_date = be_display_posts_bool( $atts['include_date'] );
	$include_excerpt = be_display_posts_bool( $atts['include_excerpt'] );
	$meta_key = sanitize_text_field( $atts['meta_key'] );
	$meta_value = sanitize_text_field( $atts['meta_value'] );
	$no_posts_message = sanitize_text_field( $atts['no_posts_message'] );
	$offset = intval( $atts['offset'] );
	$order = sanitize_key( $atts['order'] );
	$orderby = sanitize_key( $atts['orderby'] );
	$post_parent = $atts['post_parent']; // Validated later, after check for 'current'
	$post_status = $atts['post_status']; // Validated later as one of a few values
	$post_type = sanitize_text_field( $atts['post_type'] );
	$posts_per_page = intval( $atts['posts_per_page'] );
	$tag = sanitize_text_field( $atts['tag'] );
	$tax_operator = $atts['tax_operator']; // Validated later as one of a few values
	$tax_term = sanitize_text_field( $atts['tax_term'] );
	$taxonomy = sanitize_key( $atts['taxonomy'] );
	$wrapper = sanitize_text_field( $atts['wrapper'] );
	$wrapper_class = sanitize_html_class( $atts['wrapper_class'] );
	if( !empty( $wrapper_class ) )
		$wrapper_class = ' class="' . $wrapper_class . '"';
	$wrapper_id = sanitize_html_class( $atts['wrapper_id'] );
	if( !empty( $wrapper_id ) )
		$wrapper_id = ' id="' . $wrapper_id . '"';

	
	// Set up initial query for post
	$args = array(
		'category_name'       => $category,
		'order'               => $order,
		'orderby'             => $orderby,
		'post_type'           => explode( ',', $post_type ),
		'posts_per_page'      => $posts_per_page,
		'tag'                 => $tag,
	);
	
	// Ignore Sticky Posts
	if( $ignore_sticky_posts )
		$args['ignore_sticky_posts'] = true;
	
	// Meta key (for ordering)
	if( !empty( $meta_key ) )
		$args['meta_key'] = $meta_key;
	
	// Meta value (for simple meta queries)
	if( !empty( $meta_value ) )
		$args['meta_value'] = $meta_value;
		
	// If Post IDs
	if( $id ) {
		$posts_in = array_map( 'intval', explode( ',', $id ) );
		$args['post__in'] = $posts_in;
	}
	
	// If Exclude Current
	if( $exclude_current )
		$args['post__not_in'] = array( get_the_ID() );
	
	// Post Author
	if( !empty( $author ) )
		$args['author_name'] = $author;
		
	// Offset
	if( !empty( $offset ) )
		$args['offset'] = $offset;
	
	// Post Status	
	$post_status = explode( ', ', $post_status );		
	$validated = array();
	$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
	foreach ( $post_status as $unvalidated )
		if ( in_array( $unvalidated, $available ) )
			$validated[] = $unvalidated;
	if( !empty( $validated ) )		
		$args['post_status'] = $validated;
	
	
	// If taxonomy attributes, create a taxonomy query
	if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {
	
		// Term string to array
		$tax_term = explode( ', ', $tax_term );
		
		// Validate operator
		if( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) )
			$tax_operator = 'IN';
					
		$tax_args = array(
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'slug',
					'terms'    => $tax_term,
					'operator' => $tax_operator
				)
			)
		);
		
		// Check for multiple taxonomy queries
		$count = 2;
		$more_tax_queries = false;
		while( 
			isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) && 
			isset( $original_atts['tax_' . $count . '_term'] ) && !empty( $original_atts['tax_' . $count . '_term'] ) 
		):
		
			// Sanitize values
			$more_tax_queries = true;
			$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
	 		$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
	 		$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts['tax_' . $count . '_operator'] : 'IN';
	 		$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
	 		
	 		$tax_args['tax_query'][] = array(
	 			'taxonomy' => $taxonomy,
	 			'field' => 'slug',
	 			'terms' => $terms,
	 			'operator' => $tax_operator
	 		);
	
			$count++;
			
		endwhile;
		
		if( $more_tax_queries ):
			$tax_relation = 'AND';
			if( isset( $original_atts['tax_relation'] ) && in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) ) )
				$tax_relation = $original_atts['tax_relation'];
			$args['tax_query']['relation'] = $tax_relation;
		endif;
		
		$args = array_merge( $args, $tax_args );
	}
	
	// If post parent attribute, set up parent
	if( $post_parent ) {
		if( 'current' == $post_parent ) {
			global $post;
			$post_parent = get_the_ID();
		}
		$args['post_parent'] = intval( $post_parent );
	}
	
	// Set up html elements used to wrap the posts. 
	// Default is ul/li, but can also be ol/li and div/div
	$wrapper_options = array( 'ul', 'ol', 'div' );
	if( ! in_array( $wrapper, $wrapper_options ) )
		$wrapper = 'ul';
	$inner_wrapper = 'div' == $wrapper ? 'div' : 'li';

	
	$listing = new WP_Query( apply_filters( 'display_posts_shortcode_args', $args, $original_atts ) );
	if ( ! $listing->have_posts() )
		return apply_filters( 'display_posts_shortcode_no_results', wpautop( $no_posts_message ) );
		
	$inner = '';
	while ( $listing->have_posts() ): $listing->the_post(); global $post;
		
		$image = $date = $author = $excerpt = $content = '';
		
		if ( $include_title )
			$title = '<a class="title" href="' . apply_filters( 'the_permalink', get_permalink() ) . '">' . get_the_title() . '</a>';
		
		if ( $image_size && has_post_thumbnail() )  
			$image = '<a class="image" href="' . get_permalink() . '">' . get_the_post_thumbnail( get_the_ID(), $image_size ) . '</a> ';
			
		if ( $include_date ) 
			$date = ' <span class="date">' . get_the_date( $date_format ) . '</span>';
			
		if( $include_author )
			$author = apply_filters( 'display_posts_shortcode_author', ' <span class="author">by ' . get_the_author() . '</span>' );
		
		if ( $include_excerpt ) 
			$excerpt = ' <span class="excerpt-dash">-</span> <span class="excerpt">' . get_the_excerpt() . '</span>';
			
		if( $include_content ) {
			add_filter( 'shortcode_atts_display-posts', 'be_display_posts_off', 10, 3 );
			$content = '<div class="content">' . apply_filters( 'the_content', get_the_content() ) . '</div>'; 
			remove_filter( 'shortcode_atts_display-posts', 'be_display_posts_off', 10, 3 );
		}
		
		$class = array( 'listing-item' );
		$class = sanitize_html_class( apply_filters( 'display_posts_shortcode_post_class', $class, $post, $listing, $original_atts ) );
		$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $image . $title . $date . $author . $excerpt . $content . '</' . $inner_wrapper . '>';
		
		// If post is set to private, only show to logged in users
		if( 'private' == get_post_status( get_the_ID() ) && !current_user_can( 'read_private_posts' ) )
			$output = '';
		
		$inner .= apply_filters( 'display_posts_shortcode_output', $output, $original_atts, $image, $title, $date, $excerpt, $inner_wrapper, $content, $class );
		
	endwhile; wp_reset_postdata();
	
	$open = apply_filters( 'display_posts_shortcode_wrapper_open', '<' . $wrapper . $wrapper_class . $wrapper_id . '>', $original_atts );
	$close = apply_filters( 'display_posts_shortcode_wrapper_close', '</' . $wrapper . '>', $original_atts );
	
	$return = $open;

	if( $shortcode_title ) {

		$title_tag = apply_filters( 'display_posts_shortcode_title_tag', 'h2', $original_atts );

		$return .= '<' . $title_tag . ' class="display-posts-title">' . $shortcode_title . '</' . $title_tag . '>' . "\n";
	}

	$return .= $inner . $close;

	return $return;
}

/**
 * Turn off display posts shortcode 
 * If display full post content, any uses of [display-posts] are disabled
 *
 * @param array $out, returned shortcode values 
 * @param array $pairs, list of supported attributes and their defaults 
 * @param array $atts, original shortcode attributes 
 * @return array $out
 */
function be_display_posts_off( $out, $pairs, $atts ) {
	$out['display_posts_off'] = true;
	return $out;
}

/**
 * Convert string to boolean
 * because (bool) "false" == true
 *
 */
function be_display_posts_bool( $value ) {
	return !empty( $value ) && 'true' == $value ? true : false;
}


add_filter( 'template_include', 'onecolumn_page_template', 99 );

function onecolumn_page_template( $template ) {
	
	if ( isset( $_REQUEST['wp_customize'] ) ) {
		return $template;
	}
	else {
		$layout =  get_option( 'layout' ); //global or default layout
		$page_layout =  get_post_meta( get_the_ID(), 'layout_option', true ); //get page-specific layout (does not work for the posts page -- see below)
		
		$posts_page_id = get_option( 'page_for_posts' ); //get the post_id for the main posts page
		$posts_page_layout =  get_post_meta( $posts_page_id, 'layout_option', true ); //posts page layout
		
		$front_page_id = get_option( 'page_on_front' ); //get the post_id for the front page
		$front_page_layout =  get_post_meta( $front_page_id, 'layout_option', true ); //front page layout
		
		$home_page_id = get_page_by_title( 'Home' );
		$home_page_layout =  get_post_meta( $home_page_id->ID, 'layout_option', true ); //home page layout
		
	
		if ( ($layout == 'onecolumn' || $page_layout == 'no_sidebar') && ($page_layout != 'right_sidebar') && ($page_layout != 'left_sidebar') && (!is_search()) && (!is_home()) ) {
			$new_template = locate_template( array( 'onecolumn.php' ) );
			if ( '' != $new_template ) {
				return $new_template ;
			}
		}
		if ( is_home() && !is_front_page() && $posts_page_layout == 'no_sidebar' ) {
			$new_template = locate_template( array( 'onecolumn-blog.php' ) );
			if ( '' != $new_template ) {
				return $new_template ;
			}
		}
		if ( ($layout == 'onecolumn' || $page_layout == 'no_sidebar') && ($page_layout != 'right_sidebar') && ($page_layout != 'left_sidebar') && (!is_search()) && (!is_home()) ) {
			$new_template = locate_template( array( 'onecolumn.php' ) );
			if ( '' != $new_template ) {
				return $new_template ;
			}
		}
	
		if ( ! is_active_sidebar( 'sidebar-3' ) && ! is_active_sidebar( 'sidebar-4' ) ) {
			$new_template = locate_template( array( 'onecolumn.php' ) );
			if ( '' != $new_template ) {
				return $new_template ;
			}
		}
	
		return $template;
	
	}
	
}


/**
Plugin Name: TW Recent Posts Widget
Plugin URI: http://vuckovic.biz/wordpress-plugins/tw-recent-posts-widget
Description: TW Recent Posts Widget is advanced version of the WordPress Recent Posts widget allowing increased customization to display recent posts from category you define.
Author: Igor Vuckovic
Author URI: http://vuckovic.biz
Version: 1.0.3
*/

//	Set the wp-content and plugin urls/paths
if (! defined ( 'WP_CONTENT_URL' ))
	define ( 'WP_CONTENT_URL', get_option ( 'siteurl' ) . '/wp-content' );
if (! defined ( 'WP_CONTENT_DIR' ))
	define ( 'WP_CONTENT_DIR', ABSPATH . 'wp-content' );
if (! defined ( 'WP_PLUGIN_URL' ))
	define ( 'WP_PLUGIN_URL', WP_CONTENT_URL . '/plugins' );
if (! defined ( 'WP_PLUGIN_DIR' ))
	define ( 'WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins' );
	
class TW_Recent_Posts extends WP_Widget {
	
	//	@var string (The plugin version)		
	var $version = '1.0.3';
	//	@var string $localizationDomain (Domain used for localization)
	var $localizationDomain = 'tw-recent-posts';
	//	@var string $pluginurl (The url to this plugin)
	var $pluginurl = '';
	//	@var string $pluginpath (The path to this plugin)		
	var $pluginpath = '';
	
	//	PHP 4 Compatible Constructor
	function TW_Recent_Posts() {
		$this->__construct();
	}

	//	PHP 5 Constructor		
	function __construct() {
		$name = dirname ( plugin_basename ( __FILE__ ) );
		$this->pluginurl = WP_PLUGIN_URL . "/$name/";
		$this->pluginpath = WP_PLUGIN_DIR . "/$name/";
		add_action ( 'wp_print_styles', array (&$this, 'tw_recent_posts_css' ) );
		
		$widget_ops = array ('classname' => 'tw-recent-posts', 'description' => __ ( 'Show recent posts from selected category. Includes advanced options.', $this->localizationDomain ) );
		$this->WP_Widget ( 'tw-recent-posts', __ ( 'TW Recent Posts ', $this->localizationDomain ), $widget_ops );
	}
	
	function tw_recent_posts_css() {
		$name = "tw-recent-posts-widget.css";
		if (false !== @file_exists ( TEMPLATEPATH . "/$name" )) {
			$css = get_template_directory_uri () . "/$name";
		} else {
			$css = $this->pluginurl . $name;
		}
		wp_enqueue_style ( 'tw-recent-posts-widget', $css, false, $this->version, 'screen' );
	}
	
	private function truncate_post($amount, $echo = true, $allowed = '') {
		global $post;
		$postExcerpt = '';
		$postExcerpt = $post->post_excerpt;
		
		if ($postExcerpt != '') {
			if (strlen ( $postExcerpt ) <= $amount)
				$echo_out = '';
			else
				$echo_out = '...';
			
			$postExcerpt = strip_tags ( $postExcerpt, $allowed );
			if ($echo_out == '...')
				$postExcerpt = substr ( $postExcerpt, 0, strrpos ( substr ( $postExcerpt, 0, $amount ), ' ' ) );
			else
				$postExcerpt = substr ( $postExcerpt, 0, $amount );
			
			if ($echo)
				echo $postExcerpt . $echo_out;
			else
				return ($postExcerpt . $echo_out);
		} else {
			$truncate = $post->post_content;
			
			$truncate = preg_replace ( '@\[caption[^\]]*?\].*?\[\/caption]@si', '', $truncate );
			
			if (strlen ( $truncate ) <= $amount)
				$echo_out = '';
			else
				$echo_out = '...';
			
			$truncate = apply_filters ( 'the_content', $truncate );
			$truncate = preg_replace ( '@<script[^>]*?>.*?</script>@si', '', $truncate );
			$truncate = preg_replace ( '@<style[^>]*?>.*?</style>@si', '', $truncate );
			
			$truncate = strip_tags ( $truncate, $allowed );
			
			if ($echo_out == '...')
				$truncate = substr ( $truncate, 0, strrpos ( substr ( $truncate, 0, $amount ), ' ' ) );
			else
				$truncate = substr ( $truncate, 0, $amount );
			
			if ($echo)
				echo $truncate . $echo_out;
			else
				return ($truncate . $echo_out);
		}
	}
	
	function widget($args, $instance) {
		extract ( $args );
		$title = apply_filters ( 'title', isset ( $instance ['title'] ) ? esc_attr ( $instance ['title'] ) : '' );
		$category = apply_filters ( 'category', isset ( $instance ['category'] ) ? esc_attr ( $instance ['category'] ) : '' );
		$moretext = apply_filters ( 'moretext', isset ( $instance ['moretext'] ) ? esc_attr ( $instance ['moretext'] ) : '' );
		$count = apply_filters ( 'count', isset ( $instance ['count'] ) && is_numeric ( $instance ['count'] ) ? esc_attr ( $instance ['count'] ) : '' );
		$orderby = apply_filters ( 'orderby', isset ( $instance ['orderby'] ) ? $instance ['orderby'] : '' );
		$order = apply_filters ( 'order', isset ( $instance ['order'] ) ? $instance ['order'] : '' );
		$width = apply_filters ( 'width', isset ( $instance ['width'] ) && is_numeric ( $instance ['width'] ) ? $instance ['width'] : '60' );
		$height = apply_filters ( 'height', isset ( $instance ['height'] ) && is_numeric ( $instance ['height'] ) ? $instance ['height'] : '60' );
		$length = apply_filters ( 'length', isset ( $instance ['length'] ) && is_numeric ( $instance ['length'] ) ? $instance ['length'] : '100' );
		$show_post_title = apply_filters ( 'show_post_title', isset ( $instance ['show_post_title'] ) ? ( bool ) $instance ['show_post_title'] : false );
		$show_post_time = apply_filters ( 'show_post_time', isset ( $instance ['show_post_time'] ) ? ( bool ) $instance ['show_post_time'] : false );
		$show_post_thumb = apply_filters ( 'show_post_thumb', isset ( $instance ['show_post_thumb'] ) ? ( bool ) $instance ['show_post_thumb'] : ( bool ) false );
		$show_post_excerpt = apply_filters ( 'show_post_excerpt', isset ( $instance ['show_post_excerpt'] ) ? ( bool ) $instance ['show_post_excerpt'] : false );
		
		echo $before_widget;
		if (! empty ( $title ))
			echo $before_title . $title . $after_title;
?>

<div class="featured-posts textwidget">
<?php
$wp_query = new WP_Query( array('cat' => $category, 'posts_per_page' => $count, 'orderby' => $orderby, 'order' => $order, 'nopagging' => true));
while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
	<div class="featured-post">
	
	<?php if ($show_post_title) { ?>
		<h4><a href="<?php the_permalink() ?>" rel="bookmark"
	title="<?php the_title_attribute() ?>"><?php the_title() ?></a></h4>
	<?php } ?>

	<?php if ($show_post_time) { ?>
		<div class="post-time">
			<?php the_time ( get_option ( 'date_format' ) ); ?>
		</div>
	<?php } ?>
	
	<?php if ($show_post_thumb && has_post_thumbnail()) { ?>
		<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array($width,$height), array('title' => '', 'class' => 'alignleft')); ?></a>
	<?php } ?>
	
	<?php if ($show_post_excerpt) { ?>
		<div class="excerpt">
			<?php echo $this->truncate_post($length) . ($moretext != '') ? ' <a href="' . get_permalink () . '" class="read-more">' . $moretext . '</a>' : ''; ?>
		</div>
	<?php } ?>
	
		<div class="clear"></div>
	</div>
<?php
endwhile;
wp_reset_query();
wp_reset_postdata();
		?>
</div>
<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance) {
		return $new_instance;
	}
	
	function form($instance) {
		$title = isset ( $instance ['title'] ) ? esc_attr ( $instance ['title'] ) : '';
		$category = isset ( $instance ['category'] ) ? esc_attr ( $instance ['category'] ) : '';
		$moretext = isset ( $instance ['moretext'] ) ? esc_attr ( $instance ['moretext'] ) : 'more&raquo;';
		$count = isset ( $instance ['count'] ) && is_numeric ( $instance ['count'] ) ? esc_attr ( $instance ['count'] ) : '4';
		$orderby = isset ( $instance ['orderby'] ) ? $instance ['orderby'] : '';
		$order = isset ( $instance ['order'] ) ? $instance ['order'] : '';
		$width = isset ( $instance ['width'] ) && is_numeric ( $instance ['width'] ) ? $instance ['width'] : '60';
		$height = isset ( $instance ['height'] ) && is_numeric ( $instance ['height'] ) ? $instance ['height'] : '60';
		$length = isset ( $instance ['length'] ) && is_numeric ( $instance ['length'] ) ? $instance ['length'] : '100';
		$show_post_title = isset ( $instance ['show_post_title'] ) ? ( bool ) $instance ['show_post_title'] : false;
		$show_post_time = isset ( $instance ['show_post_time'] ) ? ( bool ) $instance ['show_post_time'] : false;
		$show_post_thumb = isset ( $instance ['show_post_thumb'] ) ? ( bool ) $instance ['show_post_thumb'] : false;
		$show_post_excerpt = isset ( $instance ['show_post_excerpt'] ) ? ( bool ) $instance ['show_post_excerpt'] : false;
?>

<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', $this->localizationDomain); ?> <input
	class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
	name="<?php echo $this->get_field_name('title'); ?>" type="text"
	value="<?php echo $title; ?>" /></label></p>

<p><label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category:', $this->localizationDomain); ?></label><select
	id="<?php echo $this->get_field_id('category'); ?>"
	name="<?php echo $this->get_field_name('category'); ?>">
	<?php 
	echo '<option value="0" ' .( '0' == $category ? 'selected="selected"' : '' ). '>'. __('All categories', $this->localizationDomain).'</option>';
	$cats = get_categories(array('hide_empty' => 0, 'name' => 'category', 'hierarchical' => true));
	foreach ($cats as $cat) {
		echo '<option value="' . $cat->term_id . '" ' .( $cat->term_id == $category ? 'selected="selected"' : '' ). '>' . $cat->name . '</option>';
	} ?>
	</select></p>

<p><label for="<?php echo $this->get_field_id('orderby'); ?>"><?php _e('Order by:', $this->localizationDomain); ?></label><select
	id="<?php echo $this->get_field_id('orderby'); ?>"
	name="<?php echo $this->get_field_name('orderby'); ?>">
	<option value="date"
		<?php echo 'date' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Date', $this->localizationDomain); ?></option>
	<option value="ID"
		<?php echo 'ID' == $orderby ? 'selected="selected"' : '' ?>><?php _e('ID', $this->localizationDomain); ?></option>
	<option value="title"
		<?php echo 'title' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Title', $this->localizationDomain); ?></option>
	<option value="author"
		<?php echo 'author' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Author', $this->localizationDomain); ?></option>
	<option value="comment_count"
		<?php echo 'comment_count' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Comment count', $this->localizationDomain); ?></option>
	<option value="rand"
		<?php echo 'rand' == $orderby ? 'selected="selected"' : '' ?>><?php _e('Random', $this->localizationDomain); ?></option>
</select></p>

<p><label for="<?php echo $this->get_field_id('order'); ?>"><?php _e('Order:', $this->localizationDomain); ?></label><select
	id="<?php echo $this->get_field_id('order'); ?>"
	name="<?php echo $this->get_field_name('order'); ?>">
	<option value="DESC"
		<?php echo 'DESC' == $order ? 'selected="selected"' : '' ?>><?php _e('DESC:', $this->localizationDomain); ?></option>
	<option value="ASC"
		<?php echo 'ASC' == $order ? 'selected="selected"' : '' ?>><?php _e('ASC:', $this->localizationDomain); ?></option>
</select></p>

<p><label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Number of posts to show:', $this->localizationDomain); ?> <input
	id="<?php echo $this->get_field_id('count'); ?>"
	name="<?php echo $this->get_field_name('count'); ?>" type="text"
	size="3" value="<?php echo $count; ?>" /></label></p>

<p><input id="<?php echo $this->get_field_id('show_post_title'); ?>"
	name="<?php echo $this->get_field_name('show_post_title'); ?>"
	type="checkbox" <?php checked($show_post_title); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_title'); ?>"><?php _e('Show post title', $this->localizationDomain); ?></label>
</p>

<p><input id="<?php echo $this->get_field_id('show_post_time'); ?>"
	name="<?php echo $this->get_field_name('show_post_time'); ?>"
	type="checkbox" <?php checked($show_post_time); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_time'); ?>"><?php _e('Show post time', $this->localizationDomain); ?></label>
</p>

<p><input id="<?php echo $this->get_field_id('show_post_thumb'); ?>"
	name="<?php echo $this->get_field_name('show_post_thumb'); ?>"
	type="checkbox" <?php checked($show_post_thumb); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_thumb'); ?>"><?php _e('Show post thumb', $this->localizationDomain); ?></label><br />
<small><?php _e('Thumbnail size (W-H):', $this->localizationDomain); ?></small>
<input type="text" size="3"
	name="<?php echo $this->get_field_name('width'); ?>"
	value="<?php echo $width; ?>" />px <input type="text" size="3"
	name="<?php echo $this->get_field_name('height'); ?>"
	value="<?php echo $height; ?>" />px</p>

<p><input id="<?php echo $this->get_field_id('show_post_excerpt'); ?>"
	name="<?php echo $this->get_field_name('show_post_excerpt'); ?>"
	type="checkbox" <?php checked($show_post_excerpt); ?> /> <label
	for="<?php echo $this->get_field_id('show_post_excerpt'); ?>"><?php _e('Show post excerpt', $this->localizationDomain); ?></label><br />
<small><?php _e('Post excerpt length (characters)', $this->localizationDomain); ?></small>
<input id="<?php echo $this->get_field_id('length'); ?>"
	name="<?php echo $this->get_field_name('length'); ?>" type="text"
	size="3" value="<?php echo $length; ?>" /><br />
<small><?php _e('Read more text', $this->localizationDomain); ?></small>
<input name="<?php echo $this->get_field_name('moretext'); ?>"
	type="text" size="12" value="<?php echo $moretext; ?>" /></p>

<?php 
    }
	
} // end class TW_Recent_Posts

add_action('widgets_init', create_function('', 'return register_widget("TW_Recent_Posts");'));


/*

Plugin Name: Search Excerpt
Plugin URI: http://fucoder.com/code/search-excerpt/
Description: Modify <code>the_exceprt()</code> template code during search to return snippets containing the search phrase. Snippet extraction code stolen from <a href="http://drupal.org/">Drupal</a>'s search module. And patched by <a href="http://pobox.com/~jam/">Jam</a> to support Asian text.
Version: 1.2 $Rev$
Author: Scott Yang
Author URI: http://scott.yang.id.au/

*/

class SearchExcerpt {
    function get_content() {
        // Get the content of current post. We like to have the entire
        // content. If we call get_the_content() we'll only get the teaser +
        // page 1.
        global $post;
        
        // Password checking copied from
        // template-functions-post.php/get_the_content()
        // Search shouldn't match a passworded entry anyway.
        if (!empty($post->post_password) ) { // if there's a password
            if (stripslashes($_COOKIE['wp-postpass_'.COOKIEHASH]) != 
                $post->post_password ) 
            {      // and it doesn't match the cookie
                return get_the_password_form();
            }
        }

        return $post->post_content;
    }
    
    function get_query($text) {
        static $last = null;
        static $lastsplit = null;

        if ($last == $text)
            return $lastsplit;

        // The dot, underscore and dash are simply removed. This allows
        // meaningful search behaviour with acronyms and URLs.
        $text = preg_replace('/[._-]+/', '', $text);

        // Process words
        $words = explode(' ', $text);

        // Save last keyword result
        $last = $text;
        $lastsplit = $words;

        return $words;
    }

    function highlight_excerpt($keys, $text) {
        $text = strip_tags($text);

        for ($i = 0; $i < sizeof($keys); $i ++)
            $keys[$i] = preg_quote($keys[$i], '/');

        $workkeys = $keys;

        // Extract a fragment per keyword for at most 4 keywords.  First we
        // collect ranges of text around each keyword, starting/ending at
        // spaces.  If the sum of all fragments is too short, we look for
        // second occurrences.
        $ranges = array();
        $included = array();
        $length = 0;
        while ($length < 256 && count($workkeys)) {
            foreach ($workkeys as $k => $key) {
                if (strlen($key) == 0) {
                    unset($workkeys[$k]);
                    continue;
                }
                if ($length >= 256) {
                    break;
                }
                // Remember occurrence of key so we can skip over it if more
                // occurrences are desired.
                if (!isset($included[$key])) {
                    $included[$key] = 0;
                }

                // NOTE: extra parameter for preg_match requires PHP 4.3.3
                if (preg_match('/'.$key.'/iu', $text, $match, 
                               PREG_OFFSET_CAPTURE, $included[$key])) 
                {
                    $p = $match[0][1];
                    $success = 0;
                    if (($q = strpos($text, ' ', max(0, $p - 60))) !== false && 
                         $q < $p)
                    {
                        $end = substr($text, $p, 80);
                        if (($s = strrpos($end, ' ')) !== false && $s > 0) {
                            $ranges[$q] = $p + $s;
                            $length += $p + $s - $q;
                            $included[$key] = $p + 1;
                            $success = 1;
                        }
                    }

                    if (!$success) {
                        // for the case of asian text without whitespace
                        $q = _jamul_find_1stbyte($text, max(0, $p - 60));
                        $q = _jamul_find_delimiter($text, $q);
                        $s = _jamul_find_1stbyte_reverse($text, $p + 80, $p);
                        $s = _jamul_find_delimiter($text, $s);
                        if (($s >= $p) && ($q <= $p)) {
                            $ranges[$q] = $s;
                            $length += $s - $q;
                            $included[$key] = $p + 1;
                        } else {
                            unset($workkeys[$k]);
                        }
                    }
                } else {
                    unset($workkeys[$k]);
                }
            }
        }

        // If we didn't find anything, return the beginning.
        if (sizeof($ranges) == 0)
            return '<p>' . _jamul_truncate($text, 256) . '&nbsp;...</p>';

        // Sort the text ranges by starting position.
        ksort($ranges);

        // Now we collapse overlapping text ranges into one. The sorting makes
        // it O(n).
        $newranges = array();
        foreach ($ranges as $from2 => $to2) {
            if (!isset($from1)) {
                $from1 = $from2;
                $to1 = $to2;
                continue;
            }
            if ($from2 <= $to1) {
                $to1 = max($to1, $to2);
            } else {
                $newranges[$from1] = $to1;
                $from1 = $from2;
                $to1 = $to2;
            }
        }
        $newranges[$from1] = $to1;

        // Fetch text
        $out = array();
        foreach ($newranges as $from => $to)
            $out[] = substr($text, $from, $to - $from);

        $text = (isset($newranges[0]) ? '' : '...&nbsp;').
            implode('&nbsp;...&nbsp;', $out).'&nbsp;...';
        $text = preg_replace('/('.implode('|', $keys) .')/iu', 
                             '<strong class="search-excerpt">\0</strong>', 
                             $text);
        return "<p>$text</p>";
    }

    function the_excerpt($text) {
        static $filter_deactivated = false;
        global $more;
        global $wp_query;

        // If we are not in a search - simply return the text unmodified.
        if (!is_search())
            return $text;

        // Deactivating some of the excerpt text.
        if (!$filter_deactivated) {
            remove_filter('the_excerpt', 'wpautop');
            $filter_deactivated = true;
        }

        // Get the whole document, not just the teaser.
        $more = 1;
        $query = SearchExcerpt::get_query($wp_query->query_vars['s']);
        $content = SearchExcerpt::get_content();

        return SearchExcerpt::highlight_excerpt($query, $content);
    }
}


//Dimox Breadcrumbs
function dimox_breadcrumbs() {  
  
  /* === OPTIONS === */  
  $text['home']   = 'Home'; // text for the 'Home' link  
  $text['category'] = 'Archive by Category "%s"'; // text for a category page  
  $text['search']  = 'Search Results for "%s" Query'; // text for a search results page  
  $text['tag']   = 'Posts Tagged "%s"'; // text for a tag page  
  $text['author']  = 'Articles Posted by %s'; // text for an author page  
  $text['404']   = 'Error 404'; // text for the 404 page  
  
  $show_current  = 1; // 1 - show current post/page/category title in breadcrumbs, 0 - don't show  
  $show_on_home  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show  
  $show_home_link = 1; // 1 - show the 'Home' link, 0 - don't show  
  $show_title   = 1; // 1 - show the title for the links, 0 - don't show  
  $delimiter   = ''; // delimiter between crumbs  
  $before     = '<span class="current">'; // tag before the current crumb  
  $after     = '</span>'; // tag after the current crumb  
  /* === END OF OPTIONS === */  
  
  global $post;  
  $home_link  = home_url('/');  
  $link_before = '<span typeof="v:Breadcrumb">';  
  $link_after  = '</span>';  
  $link_attr  = ' rel="v:url" property="v:title"';  
  $link     = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;  
  $parent_id  = $parent_id_2 = $post->post_parent;  
  $frontpage_id = get_option('page_on_front');  
  
  if (is_home() || is_front_page()) {  
  
    if ($show_on_home == 1) echo '<div class="breadcrumbs"><a href="' . $home_link . '">' . $text['home'] . '</a></div>';  
  
  } else {  
  
    echo '<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">';  
    if ($show_home_link == 1) {  
      echo '<a href="' . $home_link . '" rel="v:url" property="v:title">' . $text['home'] . '</a>';  
      if ($frontpage_id == 0 || $parent_id != $frontpage_id) echo $delimiter;  
    }  
  
    if ( is_category() ) {  
      $this_cat = get_category(get_query_var('cat'), false);  
      if ($this_cat->parent != 0) {  
        $cats = get_category_parents($this_cat->parent, TRUE, $delimiter);  
        if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);  
        $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);  
        $cats = str_replace('</a>', '</a>' . $link_after, $cats);  
        if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);  
        echo $cats;  
      }  
      if ($show_current == 1) echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;  
  
    } elseif ( is_search() ) {  
      echo $before . sprintf($text['search'], get_search_query()) . $after;  
  
    } elseif ( is_day() ) {  
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;  
      echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;  
      echo $before . get_the_time('d') . $after;  
  
    } elseif ( is_month() ) {  
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;  
      echo $before . get_the_time('F') . $after;  
  
    } elseif ( is_year() ) {  
      echo $before . get_the_time('Y') . $after;  
  
    } elseif ( is_single() && !is_attachment() ) {  
      if ( get_post_type() != 'post' ) {  
        $post_type = get_post_type_object(get_post_type());  
        $slug = $post_type->rewrite;  
        printf($link, $home_link . $slug['slug'] . '/', $post_type->labels->singular_name);  
        if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;  
      } else {  
        $cat = get_the_category(); $cat = $cat[0];  
        $cats = get_category_parents($cat, TRUE, $delimiter);  
        if ($show_current == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);  
        $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);  
        $cats = str_replace('</a>', '</a>' . $link_after, $cats);  
        if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);  
        echo $cats;  
        if ($show_current == 1) echo $before . get_the_title() . $after;  
      }  
  
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {  
      $post_type = get_post_type_object(get_post_type());  
      echo $before . $post_type->labels->singular_name . $after;  
  
    } elseif ( is_attachment() ) {  
      $parent = get_post($parent_id);  
      $cat = get_the_category($parent->ID); $cat = $cat[0];  
      if ($cat) {  
        $cats = get_category_parents($cat, TRUE, $delimiter);  
        $cats = str_replace('<a', $link_before . '<a' . $link_attr, $cats);  
        $cats = str_replace('</a>', '</a>' . $link_after, $cats);  
        if ($show_title == 0) $cats = preg_replace('/ title="(.*?)"/', '', $cats);  
        echo $cats;  
      }  
      printf($link, get_permalink($parent), $parent->post_title);  
      if ($show_current == 1) echo $delimiter . $before . get_the_title() . $after;  
  
    } elseif ( is_page() && !$parent_id ) {  
      if ($show_current == 1) echo $before . get_the_title() . $after;  
  
    } elseif ( is_page() && $parent_id ) {  
      if ($parent_id != $frontpage_id) {  
        $breadcrumbs = array();  
        while ($parent_id) {  
          $page = get_page($parent_id);  
          if ($parent_id != $frontpage_id) {  
            $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));  
          }  
          $parent_id = $page->post_parent;  
        }  
        $breadcrumbs = array_reverse($breadcrumbs);  
        for ($i = 0; $i < count($breadcrumbs); $i++) {  
          echo $breadcrumbs[$i];  
          if ($i != count($breadcrumbs)-1) echo $delimiter;  
        }  
      }  
      if ($show_current == 1) {  
        if ($show_home_link == 1 || ($parent_id_2 != 0 && $parent_id_2 != $frontpage_id)) echo $delimiter;  
        echo $before . get_the_title() . $after;  
      }  
  
    } elseif ( is_tag() ) {  
      echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;  
  
    } elseif ( is_author() ) {  
      global $author;  
      $userdata = get_userdata($author);  
      echo $before . sprintf($text['author'], $userdata->display_name) . $after;  
  
    } elseif ( is_404() ) {  
      echo $before . $text['404'] . $after;  
  
    } elseif ( has_post_format() && !is_singular() ) {  
      echo get_post_format_string( get_post_format() );  
    }  
  
    if ( get_query_var('paged') ) {  
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';  
      echo __('Page') . ' ' . get_query_var('paged');  
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';  
    }  
  
    echo '</div><!-- .breadcrumbs -->';  
  
  }  
} // end dimox_breadcrumbs()



//custom excerpts
function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);
      if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).' ...<br /><a class="moretag" href="'. get_permalink($post->ID) . '"> Read More<i class="fa fa-angle-right"></i></a>';
      } else {
        $excerpt = implode(" ",$excerpt);
      } 
      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
      return $excerpt;
    }

    function content($limit) {
      $content = explode(' ', get_the_content(), $limit);
      if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).' ...<br /><a class="moretag" href="'. get_permalink($post->ID) . '"> Read More<i class="fa fa-angle-right"></i></a>';
      } else {
        $content = implode(" ",$content);
      } 
      $content = preg_replace('/\[.+\]/','', $content);
      $content = apply_filters('the_content', $content); 
      $content = str_replace(']]>', ']]&gt;', $content);
      return $content;
}


/**
 * Adds a meta box to the side column on the Post and Page edit screens for choosing the layout.
 */
function custom_layout_add_meta_box() {

	$screens = array( 'post', 'page' );

	foreach ( $screens as $screen ) {

		add_meta_box(
			'custom_layout_sectionid',
			__( 'Layout Options', 'custom_layout_textdomain' ),
			'custom_layout_meta_box_callback',
			$screen, 'side', 'core'
		);
	}
}
add_action( 'add_meta_boxes', 'custom_layout_add_meta_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function custom_layout_meta_box_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'custom_layout_meta_box', 'custom_layout_meta_box_nonce' );
	$layout_post_meta = get_post_meta( get_the_ID() );
?>


<p>
		<?php 
			  if ( get_option('layout') == 'sidebar_left' )  { $default_layout = 'Left Sidebar'; }
			  elseif ( get_option('layout') == 'sidebar_right' )  { $default_layout = 'Right Sidebar'; }
			  elseif ( get_option('layout') == 'onecolumn' )  { $default_layout = 'No Sidebar'; } 
			  $baseUrl  = home_url('/');
			  ?>
			  
			  <p><?php echo 'Default layout is set to: '; ?><strong><?php echo $default_layout; ?></strong></p>
			  <p><?php echo 'You can override the default setting on a per page basis by selecting one of the options below. The default setting can be changed '; ?><a href="<?php echo $baseUrl . 'wp-admin/customize.php'; ?>">here</a></p>

    <div class="layout-row-content">
        <p style="margin: .6em 0;"><label for="left_sidebar">
            <input type="radio" name="layout_option" id="left_sidebar" value="left_sidebar" <?php if ( isset ( $layout_post_meta['layout_option'] ) ) checked( $layout_post_meta['layout_option'][0], 'left_sidebar' ); ?> />
            <?php _e( 'Left Sidebar', 'cornell_base_textdomain' )?><br />
        </label></p>
        <p style="margin: .6em 0;"><label for="right_sidebar">
            <input type="radio" name="layout_option" id="right_sidebar" value="right_sidebar" <?php if ( isset ( $layout_post_meta['layout_option'] ) ) checked( $layout_post_meta['layout_option'][0], 'right_sidebar' ); ?> />
            <?php _e( 'Right Sidebar', 'cornell_base_textdomain' )?><br />
        </label></p>
        <p style="margin: .6em 0;"><label for="no_sidebar">
            <input type="radio" name="layout_option" id="no_sidebar" value="no_sidebar" <?php if ( isset ( $layout_post_meta['layout_option'] ) ) checked( $layout_post_meta['layout_option'][0], 'no_sidebar' ); ?> />
            <?php _e( 'No Sidebar (Full width)', 'cornell_base_textdomain' )?>
        </label></p>
    </div> 
   
</p>




<?php }

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function custom_layout_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['custom_layout_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['custom_layout_meta_box_nonce'], 'custom_layout_meta_box' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}
	
	// Checks for input and saves if needed

	if( isset( $_POST[ 'layout_option' ] ) ) {
		update_post_meta( $post_id, 'layout_option', $_POST[ 'layout_option' ] );
	}
	
}
add_action( 'save_post', 'custom_layout_save_meta_box_data' );


// Custom filter function to modify default gallery shortcode output
function my_post_gallery( $output, $attr ) {

	// Initialize
	global $post, $wp_locale;

	// Gallery instance counter
	static $instance = 0;
	$instance++;

	// Validate the author's orderby attribute
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( ! $attr['orderby'] ) unset( $attr['orderby'] );
	}

	// Get attributes from shortcode
	extract( shortcode_atts( array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post->ID,
		'itemtag'    => 'dl',
		'icontag'    => 'dt',
		'captiontag' => 'dd',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => ''
	), $attr ) );

	// Initialize
	$id = intval( $id );
	$attachments = array();
	if ( $order == 'RAND' ) $orderby = 'none';

	if ( ! empty( $include ) ) {

		// Include attribute is present
		$include = preg_replace( '/[^0-9,]+/', '', $include );
		$_attachments = get_posts( array( 'include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );

		// Setup attachments array
		foreach ( $_attachments as $key => $val ) {
			$attachments[ $val->ID ] = $_attachments[ $key ];
		}

	} else if ( ! empty( $exclude ) ) {

		// Exclude attribute is present 
		$exclude = preg_replace( '/[^0-9,]+/', '', $exclude );

		// Setup attachments array
		$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );

	} else {
		// Setup attachments array
		$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
	}

	if ( empty( $attachments ) ) return '';

	// Filter gallery differently for feeds
	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment ) $output .= wp_get_attachment_link( $att_id, $size, true ) . "\n";
		return $output;
	}

	// Filter tags and attributes
	$itemtag = tag_escape( $itemtag );
	$captiontag = tag_escape( $captiontag );
	$columns = intval( $columns );
	$itemwidth = $columns > 0 ? floor( 100 / $columns ) : 100;
	$float = is_rtl() ? 'right' : 'left';
	$selector = "gallery-{$instance}";

	// Filter gallery CSS
	$output = apply_filters( 'gallery_style', "
		<style type='text/css'>
			#{$selector} {
				margin: auto;
			}
			#{$selector} .gallery-item {
				float: {$float};
				margin-top: 10px;
				text-align: center;
				width: {$itemwidth}%;
			}
			#{$selector} img {
				border: 2px solid #cfcfcf;
			}
			#{$selector} .gallery-caption {
				margin-left: 0;
			}
		</style>
		<!-- see gallery_shortcode() in wp-includes/media.php -->
		<div id='$selector' class='gallery galleryid-{$id}'>"
	);

	// Iterate through the attachments in this gallery instance
	$i = 0;
	foreach ( $attachments as $id => $attachment ) {

		// Attachment link
		$link = isset( $attr['link'] ) && 'file' == $attr['link'] ? wp_get_attachment_link( $id, $size, false, false ) : wp_get_attachment_link( $id, $size, true, false ); 

		// Start itemtag
		$output .= "<{$itemtag} class='gallery-item'>";

		// icontag
		$output .= "
		<{$icontag} class='gallery-icon'>
			$link
		</{$icontag}>";

		if ( $captiontag && trim( $attachment->post_excerpt ) ) {

			// captiontag
			$output .= "
			<{$captiontag} class='gallery-caption'>
				" . wptexturize($attachment->post_excerpt) . "
			</{$captiontag}>";

		}

		// End itemtag
		$output .= "</{$itemtag}>";

		// Line breaks by columns set
		if($columns > 0 && ++$i % $columns == 0) $output .= '<br style="clear: both">';

	}

	// End gallery output
	$output .= "
		<br style='clear: both;'>
	</div>\n";

	return $output;

}

// Apply filter to default gallery shortcode
add_filter( 'post_gallery', 'my_post_gallery', 10, 2 );



?>