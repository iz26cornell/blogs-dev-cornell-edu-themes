<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        
        <!-- CU Typekit fonts -->
        <script type="text/javascript" src="//use.typekit.net/gog6dck.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
        
        
        <link rel="stylesheet" type="text/css" media="print" href="<?php echo get_template_directory_uri(); ?>/style/print.css" />
        <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/style/ie.css" />
            <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
        <![endif]-->
        <!--[if gte IE 9]>
          <style type="text/css">
            .gradient {
               filter: none;
            }
          </style>
        <![endif]-->
        
        <script type="text/javascript">window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js">\x3C/script><script src="<?php echo get_template_directory_uri(); ?>/js/jquery-migrate-1.1.1.min.js">\x3C/script>')</script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/layout.engine.min.js"></script><!-- target specific browsers -->
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.matchHeight.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/framework/iws.js"></script>
        <!--<script src="<?php echo get_template_directory_uri(); ?>/js/framework/iws_rotator.js"></script>-->
        <!-- Initialize -->
        <script type="text/javascript">
			var js_path = "<?php echo get_template_directory_uri(); ?>/js/framework/";
            iws_init();
        </script>
        
        <?php         
			if (isset($_GET['btnG'])) {
					$selected_radio = $_GET['sitesearch'];
					
					if ($selected_radio == 'cornell') {
						$search_terms = urlencode($_GET['s']);
						$location = "http://www.cornell.edu/search/" . "?q=" . $search_terms;
						wp_redirect($location);
					}
			}
		?>

        <?php wp_head(); ?>
    
    </head>
    
    <body <?php body_class(); ?>>
    <div id="skipnav"><a href="#content">Skip to main content</a></div>
        <div id="search-box">
          <div id="search-box-content">
            <div id="cu-search" class="options">
              <form method="get" action="<?php echo home_url( '/' ); ?>">
                <div id="search-form">
                  <label for="search-form-query">SEARCH:</label>
                  <input type="hidden" name="cx" value="011895235015245787991:gf_zlivn_9i" />
                  <input type="hidden" name="cof" value="FORID:11" />
                  <input type="hidden" name="ie" value="UTF-8" />
                  <input type="text" id="search-form-query" name="s" value="" size="20" />
                  <input type="submit" id="search-form-submit" name="btnG" value="go" />
                  <div id="search-filters">
                    <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
                    <label for="search-filters1">This Site</label>
                    <input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
                    <label for="search-filters2">Cornell</label>
                    <a href="http://www.cornell.edu/search/">more options</a> </div>
                </div>
              </form>
            </div>
          </div><!-- #search-box-content -->
        </div><!-- #search-box -->

        <div id="cu-identity" class="<?php echo get_theme_mod('custom_banner'); if ( get_theme_mod('display_tagline') != 1 || get_bloginfo( 'description' ) == '' ) { echo ' no-tagline'; } else { echo ' tagline'; } ?>">
            <div id="cu-identity-content">
                <div id="cu-brand-wrapper">
                    <div id="cu-brand">
                        <?php if ( get_theme_mod('custom_banner') == 'theme_white75' ) : ?> <!-- large insignia layout -->
                        <div id="search-button">
                            <p><a href="#">Search This Site</a></p>
                        </div>
                        <div id="navigation-bar">
                            <div id="navigation-wrap">
                                <div id="navigation">
                                    <h3><em>menu</em></h3><!-- mobile drop down navigation -->
                                    <?php $menu1 = get_theme_menu_name( 'top-menu' ); $menu2 = get_theme_menu_name( 'primary' ); 
                                          if ($menu1 == $menu2) {  //in the [unlikely] event that the same menu is in both locations, only display one
                                            wp_nav_menu( array( 'theme_location' => 'top-menu', 'fallback_cb' => '', 'menu_class' => 'top nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) );
                                          }  
                                          else {   //otherwise display both
                                            wp_nav_menu( array( 'theme_location' => 'top-menu', 'fallback_cb' => '', 'menu_class' => 'top nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) ); 
                                            wp_nav_menu( array( 'theme_location' => 'primary', 'fallback_cb' => '', 'menu_class' => 'main nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) ); 
                                          } 
                                    ?>
                                </div>
                            </div>
                        </div><!-- #navigation-bar -->
                        <div id="cu-seal">&nbsp;<a href="http://www.cornell.edu/">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cornell_identity/<?php echo get_theme_mod('custom_banner'); ?>_white.png" alt="Cornell University" /></a>
                        </div>
                        <div id="custom-branding"> 
                            <h1><a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" style="color: <?php echo get_theme_mod('site_name_color'); ?>"><?php if ( get_theme_mod('display_site_name') == '1' ) { bloginfo( 'name' ); } ?></a></h1>
                            <h2 style="color: <?php echo get_theme_mod('tagline_color'); ?>"><?php if ( get_theme_mod('display_tagline') == '1' ) { bloginfo( 'description' ); } ?></h2>
                        </div>
                    </div><!-- #cu-brand -->
                </div><!-- #cu-brand-wrapper -->
                        <?php elseif ( get_theme_mod('custom_banner') != 'theme_white75' ) : ?> <!-- small banner layout -->
                        <div id="cu-seal"><a href="http://www.cornell.edu/">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cornell_identity/<?php echo get_theme_mod('custom_banner'); ?>_white.png" alt="Cornell University" /></a>
                        </div>
                        <div id="search-button">
                            <p><a href="#">Search This Site</a></p>
                        </div>
                        <div id="navigation-bar">
                            <div id="navigation-wrap">
                                <div id="navigation">
                                    <h3><em>menu</em></h3><!-- mobile drop down navigation -->
                                    <?php $menu1 = get_theme_menu_name( 'top-menu' ); $menu2 = get_theme_menu_name( 'primary' );
											  if ($menu1 == $menu2) {  //in the [unlikely] event that the same menu is in both locations, only display one
												wp_nav_menu( array( 'theme_location' => 'top-menu', 'fallback_cb' => '', 'menu_class' => 'top nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) );
											  }  
											  else {   //otherwise display both
												wp_nav_menu( array( 'theme_location' => 'top-menu', 'fallback_cb' => '', 'menu_class' => 'top nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) ); 
												wp_nav_menu( array( 'theme_location' => 'primary', 'fallback_cb' => '', 'menu_class' => 'main nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) ); 
											  }
                                    ?>
                                </div>
                            </div>
                        </div><!-- #navigation-bar -->
                    </div><!-- #cu-brand -->
                </div><!-- #cu-brand-wrapper -->

                <div id="custom-branding"> 
                    <h1><a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" style="color: <?php echo get_theme_mod('site_name_color'); ?>"><?php if ( get_theme_mod('display_site_name') == '1' ) { bloginfo( 'name' ); } ?></a></h1>
                    <h2 style="color: <?php echo get_theme_mod('tagline_color'); ?>"><?php if ( get_theme_mod('display_tagline') == '1' ) { bloginfo( 'description' ); } ?></h2>
               </div>
               
               <?php endif; ?> <!-- end banner layouts -->
               
            </div><!-- #cu-identity-content -->
        </div><!-- #cu-identity -->
        
		<div id="wrap">
            <div id="header">
                <div id="header-band">
                    <div id="identity">
                        <div id="navigation-bar">
                            <div id="navigation-wrap">
                                <div id="navigation">
                                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'fallback_cb' => '', 'menu_class' => 'main nav-menu', 'walker' => new Custom_Walker_Nav_Menu() ) ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="image-band" 
                    <?php 
                          $header_image = get_header_image(); 
                          echo 'class="'; 
                          if ( empty( $header_image ) ) { 
                            echo 'no-header-image'; 
                          } 
                          if ( !is_active_sidebar( 'sidebar-1' ) ) { 
                            echo ' no-header-widget'; 
                          } 
                          echo '"'; 
                    ?>>
                    <div class="image-meta">
                        <h3 class="image-subtitle">
                            <?php if ( is_active_sidebar( 'sidebar-1' ) && is_front_page() ) { //header widget ?>
                                <div id="tertiary" class="sidebar-container" role="complementary">
                                    <div class="sidebar-inner">
                                        <div class="widget-area">
                                            <?php dynamic_sidebar( 'sidebar-1' ); ?>
                                        </div><!-- .widget-area -->
                                    </div><!-- .sidebar-inner -->
                                </div><!-- #tertiary -->
                            <?php } ?>
                            
                            <?php if ( !is_search() && !is_home() && !is_front_page() && !is_archive() ) { //page title ?>
                            <a href="<?php echo empty( $post->post_parent ) ? get_permalink( $post->ID ) : get_permalink( $post->post_parent ); ?>" title="<?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?>"><span class="main_title"><?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?></span></a><?php } ?>
                            
                            <?php $blog_title = get_the_title( get_option('page_for_posts', true) ); ?>
                            <?php if ( is_home() && !is_front_page() ) { ?><span class="main_title"><?php echo $blog_title; ?></span><?php } //blog page title ?>
                            <?php if ( is_tag() ) { ?><span class="main_title"><?php printf( __( 'Posts Tagged: %s', 'cornell_base' ), single_tag_title( '', false ) ); ?></span><?php } //tag title ?>
                            <?php if ( is_archive() && !is_tag() ) { ?><span class="main_title"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></span><?php } //archive title ?>
                            <?php if ( is_search() ) { ?><span class="main_title"><?php printf( __( 'Search Results for: %s', 'cornell_base' ), get_search_query() ); ?></span><?php } //search page title ?>
                        </h3>
                    </div>
                    <div class="image-cover gradient"></div>
                    <div class="text-overlay gradient"></div>
                </div><!-- #image-band -->
                
            </div><!-- end #header -->
        
            <div id="midband-wrap" class="">
                <div id="midband"></div>
            </div>
	
			<div id="content-wrap">