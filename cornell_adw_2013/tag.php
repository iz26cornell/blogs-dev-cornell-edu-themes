<?php get_header(); ?>

<div id="content-wrap">
  <div class="campaign-wrap">
    <div class="campaign">
      <div id="extra">
        <h2 class="section-title">
          <?php single_tag_title(); ?>
        </h2>
      </div>
    </div>
  </div>
  <div id="content">
    <?php include(TEMPLATEPATH."/r_sidebar.php");?>
    <div id="main">
      <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
      <h2 class="page-title">
        <?php single_tag_title(); ?>
      </h2>
      <?php if ( is_active_sidebar( 'top-widget-area' ) ) : ?>
      <div class="top-widgets">
        <?php dynamic_sidebar( 'top-widget-area' ); ?>
      </div>
      <?php endif; ?>
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="<?php foreach( get_the_category() as $cat ) { echo $cat->slug . ' '; } ?>archive-item tags">
        <?php the_post_thumbnail();?>
        <?php print_post_title() ?>
        <?php the_excerpt(__('Read more'));?>
        <!--

	<?php trackback_rdf(); ?>

	--></div>
      <?php endwhile; else: ?>
      <p>
        <?php _e('Sorry, no posts matched your criteria.'); ?>
      </p>
      <?php endif; ?>
      <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
      
      <?php if ( is_active_sidebar( 'bottom-widget-area' ) ) : ?>
      <div class="bottom-widgets">
        <?php dynamic_sidebar( 'bottom-widget-area' ); ?>
      </div>
      <?php endif; ?>
    </div>
    <div id="secondary">
      <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
      <?php dynamic_sidebar( 'sidebar-1' ); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
