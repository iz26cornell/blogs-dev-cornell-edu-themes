<?php get_header(); ?>

<div id="content-wrap">
  <div class="campaign-wrap">
    <div class="campaign">
      <div id="extra">
        <h2 class="section-title">
          <?php the_category(', ') ?>
        </h2>
      </div>
    </div>
  </div>
  <div id="content">
    <div id="main">
      <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
      <?php print_post_title() ?>
      <?php if ( is_active_sidebar( 'top-widget-area' ) ) : ?>
      <div class="top-widgets">
        <?php dynamic_sidebar( 'top-widget-area' ); ?>
      </div>
      <?php endif; ?>
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <?php the_tags('<div class="post-item-tags">','  ',' </div>'); ?>
      <?php the_content(__('Read more'));?>
      <p class="see-all-category">See all in <?php the_category(', ') ?> category.</p>

      <?php endwhile; else: ?>
      <p>
        <?php _e('Sorry, no posts matched your criteria.'); ?>
      </p>
      <?php endif; ?>
      <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
      <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
      <?php if ( is_active_sidebar( 'bottom-widget-area' ) ) : ?>
      <div class="bottom-widgets">
        <?php dynamic_sidebar( 'bottom-widget-area' ); ?>
      </div>
      <?php endif; ?>
    </div>
    <div id="secondary">
      <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
      <?php dynamic_sidebar( 'sidebar-1' ); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
</div>
<?php get_footer(); ?>
