<?php
/**
 * Template Name: Homepage
 * @package WordPress
 * @subpackage Cornell
 */
get_header(); ?>

<div id="content-wrap">
  <div class="campaign-wrap">
    <div class="campaign"><img class="campaign-image" src="<?php header_image(); ?>" alt="" />
      <div id="extra">
        <?php if ( is_active_sidebar( 'sidebar-home-extra' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-home-extra' ); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div id="content">
    <div id="main">
      <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php if ( is_front_page() ) { ?>
        <h3 class="entry-title">
          <?php the_title(); ?>
        </h3>
        <!-- removing title from homepage -->
        <?php } else { ?>
        <h1 class="entry-title">
          <?php /*?><?php the_title(); ?><?php */?>
          <!-- removing title from homepage --> 
        </h1>
        <?php } ?>
        <div class="entry-content">
          <?php the_content(); ?>
          
			<?php if ( is_active_sidebar( 'widgets-home-bottom' ) ) : ?>
			<?php dynamic_sidebar( 'widgets-home-bottom' ); ?>
			<?php endif; ?>
      
          <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
          <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
        </div>
        <!-- .entry-content --> 
      </div>
      <!-- #post-## -->
      <?php endwhile; ?>
      
      <div id="container" class="clearfix">
        <?php /*?><?php query_posts('cat=6&orderby=rand&showposts=100'); ?><?php */?>
        <?php /*?><?php query_posts('category_name=professors-at-large&orderby=rand&showposts=1000'); ?><?php */?>
        <?php query_posts('cat=177481&orderby=rand&showposts=1000'); ?>
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        
        <div class="<?php foreach( get_the_category() as $cat ) { echo $cat->slug . ' '; } ?>element transition">
			<?php
			if ( has_post_thumbnail()) {
			echo '<a href="' . get_permalink($post->ID) . '" >';
			the_post_thumbnail();
			echo '</a>';
			}
			?>
          <?php if ( is_front_page() ) { ?>
          <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="hide-show"><span><?php the_title(); ?></span></a>
          <?php } else { ?>
          <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="hide-show"><span><?php the_title(); ?></span></a>
          <?php } ?>
        </div>
        <?php endwhile; ?>
      </div>
    </div>
    
    <div id="secondary">
      <?php if ( is_active_sidebar( 'sidebar-home' ) ) : ?>
      <?php dynamic_sidebar( 'sidebar-home' ); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
