<?php get_header(); ?>

<div id="content-wrap">
  <div class="campaign-wrap">
    <div class="campaign">
      <div id="extra">
        <h2 class="section-title">
          Error 404
        </h2>
      </div>
    </div>
  </div>
  <div id="content">
    <div id="main">
    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>

      <h2>Not Found, Error 404</h2>
      <?php if ( is_active_sidebar( 'top-widget-area' ) ) : ?>
        <div class="top-widgets">
        <?php dynamic_sidebar( 'top-widget-area' ); ?>
        </div>
        <?php endif; ?>
        
      <p>The page you are looking for no longer exists.</p>
      
      <?php if ( is_active_sidebar( 'bottom-widget-area' ) ) : ?>
      <div class="bottom-widgets">
        <?php dynamic_sidebar( 'bottom-widget-area' ); ?>
      </div>
      <?php endif; ?>
    </div>
    <div id="secondary">
      <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
      <?php dynamic_sidebar( 'sidebar-1' ); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
