<!-- The following div contains the Cornell University logo and search link -->
<div id="skipnav">
	<a href="#main">Skip to main content</a>
</div>
<div id="cu-identity" class="theme-white45">
	<div id="cu-logo">
		<a href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/cornell_identity/cu_logo_white45.gif" alt="Cornell University" /></a>
	
	<div id="search-form">
			<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" >
            <div id="search-input">
            	<label for="search-form-query">SEARCH:</label>
     			<input type="text" value="" name="s" id="search-form-query" size="20" />
     			<input type="submit" id="search-form-submit" name="btnG" value="go" />
     		</div>
            </form>

	</div></div>
</div>
