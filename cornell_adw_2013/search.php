<?PHP
if (isset($_GET['btnG'])) {
	session_start();
	$selected_radio = $_GET['sitesearch'];
	
	if ($selected_radio == 'cornell') {
		$search_terms = urlencode($_GET['s']);
		$URL="http://www.cornell.edu/search" . "?q=" . $search_terms . "&client=default_frontend&proxystylesheet=default_frontend";
		print $URL;
		header ("Location: $URL");
	}
}
?>
<?php get_header(); ?>

<div id="content-wrap">
  <div class="campaign-wrap">
    <div class="campaign">
      <div id="extra">
        <h2 class="section-title"> Search Results </h2>
      </div>
    </div>
  </div>
  <div id="content">
    <div id="main">
      <h2 class="page-title"><?php printf( __( 'Search Results for: %s' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
      <?php if ( is_active_sidebar( 'top-widget-area' ) ) : ?>
      <div class="top-widgets">
        <?php dynamic_sidebar( 'top-widget-area' ); ?>
      </div>
      <?php endif; ?>
      <?php if (have_posts() && $_GET['s'] != "") : while (have_posts()) : the_post(); ?>
      <?php print_post_title() ?>
      <?php the_excerpt(__('Read more'));?>
      <!--
	<?php trackback_rdf(); ?>
	-->
      <?php endwhile; else: ?>
      <h3 class="entry-title">
        <?php _e( 'Nothing Found' ); ?>
      </h3>
      <p>
        <?php _e('Sorry, no posts matched your criteria.'); ?>
      </p>
      <?php get_search_form(); ?>
      <?php endif; ?>
      <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
      <?php if ( is_active_sidebar( 'bottom-widget-area' ) ) : ?>
      <div class="bottom-widgets">
        <?php dynamic_sidebar( 'bottom-widget-area' ); ?>
      </div>
      <?php endif; ?>
    </div>
    <div id="secondary">
      <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
      <?php dynamic_sidebar( 'sidebar-1' ); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
