</div><!-- end wrap -->
<!-- begin footer -->
<div id="footer">
  <div id="footer-content">
    <div id="footer-content-wrap">
      <div id="footer-right">
       <?php if ( is_active_sidebar( 'footer-message' ) ) : ?>
        <?php dynamic_sidebar( 'footer-message' ); ?>
        <?php endif; ?>
      </div>
      <div id="footer-left">
      <p><a href="http://www.cornell.edu/">Cornell University</a> &copy;<?php echo date("Y");?></p>
        <?php if ( is_active_sidebar( 'footer-widget-area' ) ) : ?>
        <?php dynamic_sidebar( 'footer-widget-area' ); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<div id="edublogs"><?php wp_footer();?></div><!-- edublogs -->
</body></html>