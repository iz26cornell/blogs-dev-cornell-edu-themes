<?php
if ( function_exists('register_sidebar') )	
    
	register_sidebar( array(
		'name' => __( 'Sidebar (homepage)', 'Cornell' ),
		'id' => 'sidebar-home',
		'description' => __( 'The primary widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (homepage extra)', 'Cornell' ),
		'id' => 'sidebar-home-extra',
		'description' => __( 'Secondary widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (homepage bottom)', 'Cornell' ),
		'id' => 'widgets-home-bottom',
		'description' => __( 'Homepage bottom widget area.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (secondary)', 'Cornell' ),
		'id' => 'sidebar-1',
		'description' => __( 'The primary widget area for secondary pages.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Top page area (global)', 'Cornell' ),
		'id' => 'top-widget-area',
		'description' => __( 'Appears as the top of page content.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) ); 
	
	register_sidebar( array(
		'name' => __( 'Bottom page area (global)', 'Cornell' ),
		'id' => 'bottom-widget-area',
		'description' => __( 'Appears as the bottom of page content.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );


	register_sidebar( array(
		'name' => __( 'Footer Area Left (all pages)', 'Cornell' ),
		'id' => 'footer-widget-area',
		'description' => __( 'A customizable footer area on the left', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Area Right (all pages)', 'Cornell' ),
		'id' => 'footer-message',
		'description' => __( 'A customizable footer area on the right' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );


add_action( 'after_setup_theme', 'Cornell_setup' );
if ( ! function_exists( 'Cornell_setup' ) ):
function Cornell_setup() {	
	
	// Your changeable header business starts here
	define( 'HEADER_TEXTCOLOR', '462406' );
	// No CSS, just an IMG call. The %s is a placeholder for the theme template directory URI.
	define( 'HEADER_IMAGE', '%s/images/project/home1.jpg' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'Cornell' ),
	) );

	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to Cornell_header_image_width and Cornell_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'Cornell_header_image_width', 640 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'Cornell_header_image_height', 240 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 640 pixels wide by 240 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	// set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See Cornell_admin_header_style(), below.
	add_custom_image_header( 'Cornell_header_style', 'Cornell_admin_header_style', 'Cornell_admin_header_image' );

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	
	register_default_headers( array(
		'banner1' => array(
			'url' => '%s/images/project/home1.jpg',
			'thumbnail_url' => '%s/images/project/home1_t.jpg',
			'description' => __( 'ADW 1', 'Cornell' )
		),
		'banner2' => array(
			'url' => '%s/images/project/home2.jpg',
			'thumbnail_url' => '%s/images/project/home2_t.jpg',
			'description' => __( 'ADW 2', 'Cornell' )
		),
		'banner3' => array(
			'url' => '%s/images/project/home3.jpg',
			'thumbnail_url' => '%s/images/project/home3_t.jpg',
			'description' => __( 'ADW 3', 'Cornell' )
		),
		'banner4' => array(
			'url' => '%s/images/project/home4.jpg',
			'thumbnail_url' => '%s/images/project/home4_t.jpg',
			'description' => __( 'ADW 4', 'Cornell' )
		),
		'banner5' => array(
			'url' => '%s/images/project/home5.jpg',
			'thumbnail_url' => '%s/images/project/home5_t.jpg',
			'description' => __( 'ADW 5', 'Cornell' )
		),
		'banner6' => array(
			'url' => '%s/images/project/home6.jpg',
			'thumbnail_url' => '%s/images/project/home6_t.jpg',
			'description' => __( 'ADW 6', 'Cornell' )
		),
		'banner7' => array(
			'url' => '%s/images/project/home7.jpg',
			'thumbnail_url' => '%s/images/project/home7_t.jpg',
			'description' => __( 'ADW 7', 'Cornell' )
		)
	) );
	
}
endif;

if ( ! function_exists( 'Cornell_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog
 *
 */
function Cornell_header_style() {

	// If no custom options for text are set, let's bail
	// get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
	if ( HEADER_TEXTCOLOR == get_header_textcolor() )
		return;
	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( 'blank' == get_header_textcolor() ) :
	?>
		#site-title,
		#site-description {
			position: absolute;
			left: -9000px;
		}
	<?php
		// If the user has set a custom color for the text use that
		else :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?> !important;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif;


if ( ! function_exists( 'Cornell_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 * Referenced via add_custom_image_header() in Cornell_setup().
 */
function Cornell_admin_header_style() {
?>
	<style type="text/css">
	.appearance_page_custom-header #headimg {
		background: #fff;
		border: none;
	}
	#headimg h1,
	#desc {
		font-family: Georgia, serif;
	}
	#headimg h1 {
		margin: 0;
		font-weight: normal;
		padding: 2px 0 0 0;
	}
	#headimg h1 a {
		font-size: 36px;
		line-height: 42px;
		text-decoration: none;
	}
	#desc {
		font-size: 18px;
		line-height: 31px;
		padding: 0 0 2px 0;
		font-style: italic;
	}
	<?php
		// If the user has set a custom color for the text use that
		if ( get_header_textcolor() != HEADER_TEXTCOLOR ) :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?>;
		}
	<?php endif; ?>
	#headimg img {
		max-width: 400px;
		padding-top: 5px;
	}
	</style>
<?php
}
endif;

if ( ! function_exists( 'Cornell_admin_header_image' ) ) :
/**
 * Custom header image markup displayed on the Appearance > Header admin panel.
 * Referenced via add_custom_image_header() in Cornell_setup().
 */
function Cornell_admin_header_image() { ?>
	<div id="headimg">
		<?php
		if ( 'blank' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) || '' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) )
			$style = ' style="display:none;"';
		else
			$style = ' style="color:#' . get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) . ';"';
		?>
        <div id="desc"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></div>
		<h1><a id="name"<?php echo $style; ?> onclick="return false;" href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
		
		<img src="<?php esc_url ( header_image() ); ?>" alt="" />
	</div>
<?php }
endif;

/*
 *  Returns the Cornell options with defaults as fallback
 */
function Cornell_get_theme_options() {
	$defaults = array(
		'color_scheme' => 'light',
		'theme_layout' => 'content-sidebar',
	);
	$options = get_option( 'Cornell_theme_options', $defaults );
	return $options;
}

/*
 *  Add section class to body tag
 */
add_filter('body_class','body_class_section');
function body_class_section ($classes) {
	global $wpdb, $post;
	if (is_page()) {
		if ($post->post_parent) {
			$parent = end(get_post_ancestors($current_page_id));
		}
		else {
			$parent = $post->ID;
		}
	$post_data = get_post($parent, ARRAY_A);
	$classes[] = 'section-' . $post_data['post_name'];
	}
	return $classes;
}

// category id in body and post class
function category_id_class($classes) {
	global $post;
	foreach((get_the_category($post->ID)) as $category)
		$classes [] = 'cat-' . $category->cat_ID . '-id';
		return $classes;
}
add_filter('post_class', 'category_id_class');
add_filter('body_class', 'category_id_class');


/*
 *  show page top parent title
 	for use as section title by inserting <?php get_top_level_parent_title(); ?> on the template
 */
function get_top_level_parent_title() {
global $post;
	if ( empty($post->post_parent) )
		{ the_title(); }
	else {
		$ancestors = get_post_ancestors($post->ID);
		echo get_the_title(end($ancestors));
	}
}
/*
 *  replace post title link if external link exists
 	add custom field "url_title" or "title_url" or "url1" and enter external url in value field
 */
function print_post_title() {
	global $post;
		$thePostID = $post->ID;
		$post_id = get_post($thePostID);
		$title = $post_id->post_title;
		$perm = get_permalink($post_id);
		$post_keys = array(); $post_val = array();
		$post_keys = get_post_custom_keys($thePostID);
	if (!empty($post_keys)) {
	foreach ($post_keys as $pkey) {
	if ($pkey=='url1' || $pkey=='title_url' || $pkey=='url_title') {
		$post_val = get_post_custom_values($pkey);
		}
	}
	if (empty($post_val)) {
		$link = $perm;
	} else {
		$link = $post_val[0];
		}
	} else {
		$link = $perm;
	}
	echo '<h3 class="entry-title"><a href="'.$link.'" rel="bookmark"; title="'.$title.'";>'.$title.'</a></h3>';
}

function new_excerpt_more( $more ) {
	return ' ... <a class="read-more" href="'. get_permalink($post->ID) . '"><em>Read More</em></a>';
}
add_filter('excerpt_more', 'new_excerpt_more');


/* === Custom Breadcrumbs add <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
 to the theme === */
function dimox_breadcrumbs() {

	/* === OPTIONS === */
	$text['home']     = 'Home'; // text for the 'Home' link
	$text['category'] = 'Archive by Category "%s"'; // text for a category page
	$text['search']   = 'Search Results for "%s" Query'; // text for a search results page
	$text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
	$text['author']   = 'Articles Posted by %s'; // text for an author page
	$text['404']      = 'Error 404'; // text for the 404 page

	$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
	$showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$delimiter   = ' &raquo; '; // delimiter between crumbs
	$before      = '<span class="current">'; // tag before the current crumb
	$after       = '</span>'; // tag after the current crumb
	/* === END OF OPTIONS === */

	global $post;
	$homeLink = get_bloginfo('url') . '/';
	$linkBefore = '<span typeof="v:Breadcrumb">';
	$linkAfter = '</span>';
	$linkAttr = ' rel="v:url" property="v:title"';
	$link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;

	if (is_home() || is_front_page()) {

		if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $text['home'] . '</a></div>';

	} else {

		echo '<div id="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, $homeLink, $text['home']) . $delimiter;

		if ( is_category() ) {
			$thisCat = get_category(get_query_var('cat'), false);
			if ($thisCat->parent != 0) {
				$cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
				$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
				$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
				echo $cats;
			}
			echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

		} elseif ( is_search() ) {
			echo $before . sprintf($text['search'], get_search_query()) . $after;

		} elseif ( is_day() ) {
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
			echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
			echo $before . get_the_time('d') . $after;

		} elseif ( is_month() ) {
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
			echo $before . get_the_time('F') . $after;

		} elseif ( is_year() ) {
			echo $before . get_the_time('Y') . $after;

		} elseif ( is_single() && !is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
				if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, $delimiter);
				if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
				$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
				$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
				echo $cats;
				if ($showCurrent == 1) echo $before . get_the_title() . $after;
			}

		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			echo $before . $post_type->labels->singular_name . $after;

		} elseif ( is_attachment() ) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			$cats = get_category_parents($cat, TRUE, $delimiter);
			$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
			$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
			echo $cats;
			printf($link, get_permalink($parent), $parent->post_title);
			if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

		} elseif ( is_page() && !$post->post_parent ) {
			if ($showCurrent == 1) echo $before . get_the_title() . $after;

		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			for ($i = 0; $i < count($breadcrumbs); $i++) {
				echo $breadcrumbs[$i];
				if ($i != count($breadcrumbs)-1) echo $delimiter;
			}
			if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

		} elseif ( is_tag() ) {
			echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

		} elseif ( is_author() ) {
	 		global $author;
			$userdata = get_userdata($author);
			echo $before . sprintf($text['author'], $userdata->display_name) . $after;

		} elseif ( is_404() ) {
			echo $before . $text['404'] . $after;
		}

		if ( get_query_var('paged') ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
			echo __('Page') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}

		echo '</div>';

	}
} // end dimox_breadcrumbs()


/**
 * Plugin Name: Display Posts Shortcode
 * Plugin URI: http://www.billerickson.net/shortcode-to-display-posts/
 * Description: Display a listing of posts using the [display-posts] shortcode
 * Version: 2.3
 * Author: Bill Erickson
 * Author URI: http://www.billerickson.net
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume 
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @package Display Posts
 * @version 2.2
 * @author Bill Erickson <bill@billerickson.net>
 * @copyright Copyright (c) 2011, Bill Erickson
 * @link http://www.billerickson.net/shortcode-to-display-posts/
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */
 
 
/**
 * To Customize, use the following filters:
 *
 * `display_posts_shortcode_args`
 * For customizing the $args passed to WP_Query
 *
 * `display_posts_shortcode_output`
 * For customizing the output of individual posts.
 * Example: https://gist.github.com/1175575#file_display_posts_shortcode_output.php
 *
 * `display_posts_shortcode_wrapper_open` 
 * display_posts_shortcode_wrapper_close`
 * For customizing the outer markup of the whole listing. By default it is a <ul> but
 * can be changed to <ol> or <div> using the 'wrapper' attribute, or by using this filter.
 * Example: https://gist.github.com/1270278
 */

add_shortcode("include_post", "cwd_include_post");
add_filter('widget_text', 'do_shortcode'); // add text widget support
 
// Create the shortcode
add_shortcode( 'display-posts', 'be_display_posts_shortcode' );
function be_display_posts_shortcode( $atts ) {

	// Original Attributes, for filters
	$original_atts = $atts;

	// Pull in shortcode attributes and set defaults
	$atts = shortcode_atts( array(
		'author'              => '',
		'category'            => '',
		'date_format'         => 'n/j/Y',
		'id'                  => false,
		'ignore_sticky_posts' => false,
		'image_size'          => false,
		'include_content'     => false,
		'include_date'        => false,
		'include_tags'        => false,
		'include_excerpt'     => false,
		'meta_key'            => '',
		'no_posts_message'    => '',
		'offset'              => 0,
		'order'               => 'DESC',
		'orderby'             => 'date',
		'post_parent'         => false,
		'post_status'         => 'publish',
		'post_type'           => 'post',
		'posts_per_page'      => '10',
		'tag'                 => '',
		'tax_operator'        => 'IN',
		'tax_term'            => false,
		'taxonomy'            => false,
		'wrapper'             => 'ul',
	), $atts );

	$author = sanitize_text_field( $atts['author'] );
	$category = sanitize_text_field( $atts['category'] );
	$date_format = sanitize_text_field( $atts['date_format'] );
	$id = $atts['id']; // Sanitized later as an array of integers
	$ignore_sticky_posts = (bool) $atts['ignore_sticky_posts'];
	$image_size = sanitize_key( $atts['image_size'] );
	$include_content = (bool)$atts['include_content'];
	$include_date = (bool)$atts['include_date'];
	$include_tags = (bool)$atts['include_tags'];
	$include_excerpt = (bool)$atts['include_excerpt'];
	$meta_key = sanitize_text_field( $atts['meta_key'] );
	$no_posts_message = sanitize_text_field( $atts['no_posts_message'] );
	$offset = intval( $atts['offset'] );
	$order = sanitize_key( $atts['order'] );
	$orderby = sanitize_key( $atts['orderby'] );
	$post_parent = $atts['post_parent']; // Validated later, after check for 'current'
	$post_status = $atts['post_status']; // Validated later as one of a few values
	$post_type = sanitize_text_field( $atts['post_type'] );
	$posts_per_page = intval( $atts['posts_per_page'] );
	$tag = sanitize_text_field( $atts['tag'] );
	$tax_operator = $atts['tax_operator']; // Validated later as one of a few values
	$tax_term = sanitize_text_field( $atts['tax_term'] );
	$taxonomy = sanitize_key( $atts['taxonomy'] );
	$wrapper = sanitize_text_field( $atts['wrapper'] );

	
	// Set up initial query for post
	$args = array(
		'category_name'       => $category,
		'order'               => $order,
		'orderby'             => $orderby,
		'post_type'           => explode( ',', $post_type ),
		'posts_per_page'      => $posts_per_page,
		'tag'                 => $tag,
	);
	
	// Ignore Sticky Posts
	if( $ignore_sticky_posts )
		$args['ignore_sticky_posts'] = true;
	
	// Meta key (for ordering)
	if( !empty( $meta_key ) )
		$args['meta_key'] = $meta_key;
	
	// If Post IDs
	if( $id ) {
		$posts_in = array_map( 'intval', explode( ',', $id ) );
		$args['post__in'] = $posts_in;
	}
	
	// Post Author
	if( !empty( $author ) )
		$args['author_name'] = $author;
		
	// Offset
	if( !empty( $offset ) )
		$args['offset'] = $offset;
	
	// Post Status	
	$post_status = explode( ', ', $post_status );		
	$validated = array();
	$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
	foreach ( $post_status as $unvalidated )
		if ( in_array( $unvalidated, $available ) )
			$validated[] = $unvalidated;
	if( !empty( $validated ) )		
		$args['post_status'] = $validated;
	
	
	// If taxonomy attributes, create a taxonomy query
	if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {
	
		// Term string to array
		$tax_term = explode( ', ', $tax_term );
		
		// Validate operator
		if( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) )
			$tax_operator = 'IN';
					
		$tax_args = array(
			'tax_query' => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'slug',
					'terms'    => $tax_term,
					'operator' => $tax_operator
				)
			)
		);
		
		// Check for multiple taxonomy queries
		$count = 2;
		$more_tax_queries = false;
		while( 
			isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) && 
			isset( $original_atts['tax_' . $count . '_term'] ) && !empty( $original_atts['tax_' . $count . '_term'] ) 
		):
		
			// Sanitize values
			$more_tax_queries = true;
			$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
	 		$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
	 		$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts['tax_' . $count . '_operator'] : 'IN';
	 		$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
	 		
	 		$tax_args['tax_query'][] = array(
	 			'taxonomy' => $taxonomy,
	 			'field' => 'slug',
	 			'terms' => $terms,
	 			'operator' => $tax_operator
	 		);
	
			$count++;
			
		endwhile;
		
		if( $more_tax_queries ):
			$tax_relation = 'AND';
			if( isset( $original_atts['tax_relation'] ) && in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) ) )
				$tax_relation = $original_atts['tax_relation'];
			$args['tax_query']['relation'] = $tax_relation;
		endif;
		
		$args = array_merge( $args, $tax_args );
	}
	
	// If post parent attribute, set up parent
	if( $post_parent ) {
		if( 'current' == $post_parent ) {
			global $post;
			$post_parent = $post->ID;
		}
		$args['post_parent'] = intval( $post_parent );
	}
	
	// Set up html elements used to wrap the posts. 
	// Default is ul/li, but can also be ol/li and div/div
	$wrapper_options = array( 'ul', 'ol', 'div' );
	if( ! in_array( $wrapper, $wrapper_options ) )
		$wrapper = 'ul';
	$inner_wrapper = 'div' == $wrapper ? 'div' : 'li';

	
	$listing = new WP_Query( apply_filters( 'display_posts_shortcode_args', $args, $original_atts ) );
	if ( ! $listing->have_posts() )
		return apply_filters( 'display_posts_shortcode_no_results', wpautop( $no_posts_message ) );
		
	$inner = '';
	while ( $listing->have_posts() ): $listing->the_post(); global $post;
		
		$image = $date = $excerpt = $content = '';
		
		$title = '<a class="post-title" href="' . apply_filters( 'the_permalink', get_permalink() ) . '">' . apply_filters( 'the_title', get_the_title() ) . '</a>';
		
		if ( $image_size && has_post_thumbnail() )  
			$image = '<a class="post-image" href="' . get_permalink() . '">' . get_the_post_thumbnail( $post->ID, $image_size ) . '</a> ';
			
		if ( $include_date ) 
			$date = ' <span class="post-item-date">' . get_the_date( $date_format ) . '</span>';
		
		if( $include_tags )
			$taglist = '<div class="post-item-tags">' . get_the_tag_list('',' ','') . '</div>';
		
		if ( $include_excerpt ) 
			$excerpt = ' <span class="post-excerpt">' . get_the_excerpt() . '</span>';
			
		if( $include_content )
			$content = '<div class="post-item-content">' . apply_filters( 'the_content', get_the_content() ) . '</div>'; 
		
		
		$class = array( 'listing-item' );
		$class = apply_filters( 'display_posts_shortcode_post_class', $class, $post, $listing );
		$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $image . $title . $taglist . $date . $excerpt . $content . '</' . $inner_wrapper . '>';
		
		// If post is set to private, only show to logged in users
		if( 'private' == get_post_status( $post->ID ) && !current_user_can( 'read_private_posts' ) )
			$output = '';
		
		$inner .= apply_filters( 'display_posts_shortcode_output', $output, $original_atts, $image, $title, $date, $excerpt, $inner_wrapper, $content, $taglist, $class );
		
	endwhile; wp_reset_postdata();
	
	$open = apply_filters( 'display_posts_shortcode_wrapper_open', '<' . $wrapper . ' class="display-posts-listing">', $original_atts );
	$close = apply_filters( 'display_posts_shortcode_wrapper_close', '</' . $wrapper . '>', $original_atts );
	$return = $open . $inner . $close;

	return $return;
}

//Redirect to post when search query returns single result
add_action('template_redirect', 'single_result');
function single_result() {
	if (is_search()) {
		global $wp_query;
		if ($wp_query->post_count == 1) {
			wp_redirect( get_permalink( $wp_query->posts['0']->ID ) );
		}
	}
}
 
//** Resize large image on upload (Image will be resize according to the large size in your media settings)

function replace_uploaded_image($image_data) {
// if there is no large image : return
if (!isset($image_data['sizes']['large'])) return $image_data;

// path to the uploaded image and the large image
$upload_dir = wp_upload_dir();
$uploaded_image_location = $upload_dir['basedir'] . '/' .$image_data['file'];
$large_image_location = $upload_dir['path'] . '/'.$image_data['sizes']['large']['file'];

// delete the uploaded image
unlink($uploaded_image_location);

// rename the large image
rename($large_image_location,$uploaded_image_location);

// update image metadata and return them
$image_data['width'] = $image_data['sizes']['large']['width'];
$image_data['height'] = $image_data['sizes']['large']['height'];
unset($image_data['sizes']['large']);

return $image_data;
}
add_filter('wp_generate_attachment_metadata','replace_uploaded_image');


// Display Post Thumbnail Also In Edit Post and Page Overview

if ( !function_exists('fb_AddThumbColumn') && function_exists('add_theme_support') ) {
// for post and page
add_theme_support('post-thumbnails', array( 'post', 'page' ) );
function fb_AddThumbColumn($cols) {
	$cols['thumbnail'] = __('Thumbnail');
	return $cols;
}

function fb_AddThumbValue($column_name, $post_id) {
    $width = (int) 35;
    $height = (int) 35;
    if ( 'thumbnail' == $column_name ) {
        // thumbnail of WP 2.9
        $thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
        
        // image from gallery
        $attachments = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
        
        if ($thumbnail_id)
            $thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
        elseif ($attachments) {
            foreach ( $attachments as $attachment_id => $attachment ) {
            $thumb = wp_get_attachment_image( $attachment_id, array($width, $height), true );
        }
    }
	if ( isset($thumb) && $thumb ) { echo $thumb; }
	else { echo __('None'); }
	}
}

// for posts
add_filter( 'manage_posts_columns', 'fb_AddThumbColumn' );
add_action( 'manage_posts_custom_column', 'fb_AddThumbValue', 10, 2 );

// for pages
add_filter( 'manage_pages_columns', 'fb_AddThumbColumn' );
add_action( 'manage_pages_custom_column', 'fb_AddThumbValue', 10, 2 );
}


// Enable TinyMCE editor for post the_excerpt
function tinymce_excerpt_js(){ ?>  
<script type="text/javascript">  
    jQuery(document).ready( tinymce_excerpt );  
    function tinymce_excerpt() {  
    jQuery("#excerpt").addClass("mceEditor");  
    tinyMCE.execCommand("mceAddControl", false, "excerpt");  
    }  
</script>  
<?php }  
add_action( 'admin_head-post.php', 'tinymce_excerpt_js');  
add_action( 'admin_head-post-new.php', 'tinymce_excerpt_js');  
  
function tinymce_css(){ ?>  
<style type='text/css'>  
    #postexcerpt .inside{margin:0;padding:0;background:#fff;}  
    #postexcerpt .inside p{padding:0px 0px 5px 10px;}  
    #postexcerpt #excerpteditorcontainer { border-style: solid; padding: 0; }  
</style>  
<?php }  
add_action( 'admin_head-post.php', 'tinymce_css');  
add_action( 'admin_head-post-new.php', 'tinymce_css');


/*
Description: Adds sidebar widgets to display a context-based list of "nearby" pages, and to display nested categories.
*/

if (!class_exists('SRCS_WP_Widget')) {
  class SRCS_WP_Widget extends WP_Widget
  {
    function form_html($instance) {
      $option_menu = $this->known_params(1);
      foreach (array_keys($option_menu) as $param) {
	$param_display[$param] = htmlspecialchars($instance[$param]);
      }

      foreach ($option_menu as $option_name => $option) {
	$checkval='';
	$desc = '';
	if ($option['desc'])
	  $desc = "<br /><small>{$option['desc']}</small>";
	switch ($option['type']) {
	case 'checkbox':
	  if ($instance[$option_name]) // special HTML and override value
	    $checkval = 'checked="yes" ';
	  $param_display[$option_name] = 'yes';
	  break;
	case '':
	  $option['type'] = 'text';
	  break;
	}
	print '<p style="text-align:right;"><label for="' . $this->get_field_name($option_name) . '">' . 
	  __($option['title']) . 
	  ' <input style="width: 200px;" id="' . $this->get_field_id($option_name) . 
	  '" name="' . $this->get_field_name($option_name) . 
	  "\" type=\"{$option['type']}\" {$checkval}value=\"{$param_display[$option_name]}\" /></label>$desc</p>";
      }
    }
  }
}

class HierPageWidget extends SRCS_WP_Widget
{
  /**
   * Declares the HierPageWidget class.
   *
   */
  function __construct(){
    $widget_ops = array('classname' => 'widget_hier_page', 'description' => __( "Hierarchical Page Directory Widget") );
    $control_ops = array('width' => 300, 'height' => 300);
    $this->WP_Widget('hierpage', __('Hierarchical Pages'), $widget_ops, $control_ops);
  }

  /**
   * Helper function
   *
   */
  function hierpages_list_pages($args = '') {
    global $post;
    global $wp_query;

    if ( !isset($args['echo']) )
      $args['echo'] = 1;

    $output = '';
    
    if(!$args['post_type'])$args['post_type'] = 'page';

    // Query pages.  NOTE: The array is sorted in alphabetical, or menu, order.
    $pages = & get_pages($args);

    $page_info = Array();

    if ( $pages ) {
      $current_post = $wp_query->get_queried_object_id();

      foreach ( $pages as $page ) {
	$page_info[$page->ID]['parent'] = $page->post_parent;
	$page_info[$page->post_parent]['children'][] = $page->ID;
      }

      // Display the front page?
      $front_page = -1; // assume no static front page
      if ('page' == get_option('show_on_front')) {
	$front_page = get_option('page_on_front');
	// Regard flag: always show front page?  Otherwise: Show front page only if it has children
	if (($args['show_home'] == 'yes') || (sizeof($page_info[$front_page]['children']))) {
	  $page_info[$front_page]['show'] = 1;	// always show front page
	}
      }
	
      // add all children of the root node, but only to single depth.
      if ($args['show_root'] == 'yes') {
	foreach ( $page_info[0]['children'] as $child ) {
	  if ($child != $front_page) {
	    $page_info[$child]['show'] = 1;
	  }
	}
      }
      
      if (is_post_type_hierarchical($args['post_type'])) {
	if ($post->ID != $front_page ) {
	  // The current page is always shown, unless it is the static front page (see above)
	  $page_info[$post->ID]['show'] = 1;
	}

	// show the current page's children, if any.
	if (is_array($page_info[$current_post]['children'] )) {
	   foreach ( $page_info[$current_post]['children'] as $child ) {
	      $page_info[$child]['show'] = 1;
	   }
	}

	$post_parent = $page_info[$current_post]['parent'];
	if ($post_parent && ($args['show_siblings'] == 'yes')) {
	  // if showing siblings, add the current page's parent's other children.
	  foreach ( $page_info[$post_parent]['children'] as $child ) {
	    if ($child != $front_page) {
	      $page_info[$child]['show'] = 1;
	    }
	  }

	  // Also show parent node's siblings.
	  $post_grandparent = $page_info[$post_parent]['parent'];
	  if ($post_grandparent) {
	    foreach ( $page_info[$post_grandparent]['children'] as $child ) {
	      if ($child != $front_page) {
		$page_info[$child]['show'] = 1;
	      }
	    }
	  }
	}

	// add all ancestors of the current page.
	while ($post_parent) {
	  $page_info[$post_parent]['show'] = 1;
          // show that page's children, if any.                                                            
          if (is_array($page_info[$post_parent]['children'] )) {
            foreach ( $page_info[$post_parent]['children'] as $child ) {
              $page_info[$child]['show'] = 1;
            }
          }
	  $post_parent = $page_info[$post_parent]['parent'];
	}
      }
      
      // Add pages that were selected
      $my_includes = Array();

      foreach ( $pages as $page ) {
	if ($page_info[$page->ID]['show']) {
	  $my_includes[] = $page->ID;
	}
      }
      if ($args['child_of']) {
        $my_includes[] = $args['child_of'];
      }
      
      if (!empty($my_includes)) {
        // List pages, if any. Blank title_li suppresses unwanted elements.
        $output .= wp_list_pages( Array('title_li' => '',
					'sort_column' => $args['sort_column'],
					'sort_order' => $args['sort_order'],
					'include' => $my_includes,
                                        'post_type'=> $args['post_type']
                                        ) );
      }
    }

    $output = apply_filters('wp_list_pages', $output);
    
    if ( $args['echo'] )
      echo $output;
    else
      return $output;
  }

  /**
   * Displays the Widget
   *
   */
  function widget($args, $instance){

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $known_params = $this->known_params(0);
    foreach ($known_params as $param) {
      if (strlen($instance[$param])) {
	$page_options[$param] = $instance[$param];
      }
    }
      
    if ($instance['menu_order'] == 'yes') { // Deprecated, eliminated upon form display (see below)
      $page_options['sort_column']='menu_order,post_title';
    }

    print $args['before_widget'];
    if ( $title )
      print "{$args['before_title']}{$title}{$args['after_title']}";
    print '<ul>';
    $this->hierpages_list_pages($page_options);
    print "</ul>{$args['after_widget']}";
  }

  function known_params ($options = 0) {
    $option_menu = array('title' => array('title' => 'Title:'),
			 'show_siblings' => array('title' => 'Show siblings to the current page?',
						  'type' => 'checkbox'),
			 'show_root' => array('title' => 'Always show top-level pages?',
					      'type' => 'checkbox'),
			 'show_home' => array('title' => 'Show the static home page?',
					      'desc' => '(always shown if it has child pages)',
					      'type' => 'checkbox'),
			 'child_of' => array('title' => 'Root page ID:'),
			 'exclude' => array('title' => 'Exclude pages:',
					    'desc' => 'List of page IDs to exclude'),
			 'sort_column' => array('title' => 'Sort field:',
						'desc' => 'Comma-separated list: <em>post_title, menu_order, post_date, post_modified, ID, post_author, post_name</em>'),
			 'sort_order' => array('title' => 'Sort direction:',
					       'desc' => '(default: ASC)'),
			 'meta_key' => array('title' => 'Meta Key:'),
			 'meta_value' => array('title' => 'Meta-key Value:',
					     'desc' => 'for selecting pages by custom fields'),
			 'authors' => array('title' => 'Authors:'),
			 'post_status' => array('title' => 'Post status:',
						'desc' => '(default: publish)'),
			 'post_type' => array('title' => 'Post type:',
						'desc' => '(default: page)'),
			 );
    return ($options ? $option_menu : array_keys($option_menu));
  }

  /**
   * Saves the widget's settings.
   *
   */
  function update($new_instance, $old_instance){
    $instance = $old_instance;
    $known_params = $this->known_params();
    unset($instance['menu_order']);
    foreach ($known_params as $param) {
      $instance[$param] = strip_tags(stripslashes($new_instance[$param]));
    }
    $instance['sort_order'] = strtolower($instance['sort_order']) == 'desc'?'DESC':'ASC';
    return $instance;
  }

  /**
   * Creates the edit form for the widget.
   *
   */
  function form($instance){
    $instance = wp_parse_args( (array) $instance, array('title'=>'') );
    if ($instance['menu_order']) {
      $instance['sort_column'] = 'menu_order,post_title';
    }
    if (empty($instance['sort_column'])) {
      $instance['sort_column'] = 'post_title';
    }

    $this->form_html($instance);
  }

}// END class

/**
 * Register Hierarchical Pages widget.
 *
 * Calls 'widgets_init' action after the widget has been registered.
 */
function HierPageInit() {
  register_widget('HierPageWidget');
}

  /*
   * Hierarchical Categories (combined with Hierarchical Pages)
   */

class HierCatWidget extends SRCS_WP_Widget
{
  /**
   * Declares our class.
   *
   */
  function __construct(){
    $widget_ops = array('classname' => 'widget_hier_cat', 'description' => __( "Hierarchical Category Widget") );
    $control_ops = array('width' => 300, 'height' => 300);
    $this->WP_Widget('hiercat', __('Hierarchical Categories'), $widget_ops, $control_ops);
  }

  /**
   * Helper function
   *
   */
  function hiercat_list_cats($args) {
    global $post;
    global $wp_query;

    if ( !isset($args['echo']) )
      $args['echo'] = 1;
    $sort_column = $args['sort_column'];

    $output = '';

    // Query categories.
    $cats = & get_categories($args);
    if ($cats['errors']) {
      print "<pre>"; print_r($cats); print "</pre>";
      return;
    }
    $cat_info = Array();

    if ( !empty ($cats) ) {
      $current_cat = $wp_query->get_queried_object_id();

      foreach ( $cats as $cat ) {
	$cat_info[$cat->term_id]['parent'] = $cat->category_parent;
	$cat_info[$cat->category_parent]['children'][] = $cat->term_id;
      }

      // add all children of the root node, but only to single depth.
      foreach ( $cat_info[0]['children'] as $child ) {
	$cat_info[$child]['show'] = 1;
      }
      
      // If currently displaying a category, taxonomy, or tag; AND
      // if it is the same as the one in this widget...
      if ((is_category() || is_tax() || is_tag()) && 
	  ($args['taxonomy'] == get_queried_object()->taxonomy ) ) {
	// show the current category's children, if any.
	if (is_array($cat_info[$current_cat]['children'] )) {
	   foreach ( $cat_info[$current_cat]['children'] as $child ) {
	      $cat_info[$child]['show'] = 1;
	   }
	}

	$cat_parent = $cat_info[$current_cat]['parent'];
	if ($cat_parent && ($args['show_siblings'] == 'yes')) {
	  // if showing siblings, add the current category's parent's other children.
	  foreach ( $cat_info[$cat_parent]['children'] as $child ) {
	    $cat_info[$child]['show'] = 1;
	  }

	  # Also show parent node's siblings.
	  $cat_grandparent = $cat_info[$cat_parent]['parent'];
	  if ($cat_grandparent) {
	    foreach ( $cat_info[$cat_grandparent]['children'] as $child ) {
		$cat_info[$child]['show'] = 1;
	    }
	  }
	}
	
	// add all ancestors of the current category.
	while ($cat_parent) {
	  $cat_info[$cat_parent]['show'] = 1;
	  $cat_parent = $cat_info[$cat_parent]['parent'];
	}
      }
      
      $my_includes = Array();
      // Add categories that were selected
      foreach ( $cats as $cat ) {
	if ($cat_info[$cat->term_id]['show']) {
	  $my_includes[] =$cat->term_id;
	}
      }
      
      if (!empty($my_includes)) {
        // List categories, if any. Blank title_li suppresses unwanted elements.
        $qargs = Array('title_li' => '', 'hide_empty' => 0, 'include' => $my_includes,
                      'order' => $args['order'], 'orderby' => $args['orderby'],
                      'show_count' => $args['show_count']);
        if (!empty($args['taxonomy'])) {
          $qargs['taxonomy'] = $args['taxonomy'];
        }
        $output .= wp_list_categories( $qargs );
      }
    }

    $output = apply_filters('wp_list_categories', $output);
    
    if ( $args['echo'] )
      echo $output;
    else
      return $output;
  }

  function known_params ($options = 0) {
    $option_menu = array('title' => array('title' => 'Title:'),
			 'show_siblings' => array('title' => 'Show siblings to the current category?',
						  'type' => 'checkbox'),
			 'include' => array('title' => 'Include:',
					    'desc' => 'Comma-delimited list of category IDs, or blank for all'),
			 'exclude' => array('title' => 'Exclude:'),
			 'orderby' => array('title' => 'Sort field:',
					    'desc' => 'Enter one of: <em>name, count, term_group, slug</em> or a custom value. Default: name'),
			 'order' => array('title' => 'Sort direction:',
					  'desc' => '(default: ASC)'),
			 'child_of' => array('title' => 'Only display Categories below this ID'),
			 'hide_empty' => array('title' => 'Hide empty categories?', 
						  'type' => 'checkbox'),
			 'show_count' => array('title' => 'Show count of category entries?', 
						  'type' => 'checkbox'),
			 'taxonomy' => array('title' => 'Custom taxonomy:'),
			 );
    if ($options) {
      $taxons = get_taxonomies();
      $option_menu['taxonomy']['desc'] = 'Enter one of: <em>' . 
	implode(', ',array_keys($taxons)) . '</em> or blank for post categories.';
    }
    return ($options ? $option_menu : array_keys($option_menu));
  }
  /**
   * Displays the Widget
   *
   */
  function widget($args, $instance){
    $known_params = $this->known_params(0);
    foreach ($known_params as $param) {
      if (strlen($instance[$param]))
	$cat_options[$param] = $instance[$param];
    }
    $cat_options['title'] = apply_filters('widget_title', $cat_options['title']);
    // WordPress defaults to hiding: thus, always specify.
    $cat_options['hide_empty'] = $cat_options['hide_empty'] == 'yes' ? 1 : 0;
      
    print $args['before_widget'];
    if ( strlen($cat_options['title']) )
      print "{$args['before_title']}{$cat_options['title']}{$args['after_title']}";
    print "<ul>";

    $this->hiercat_list_cats($cat_options);
    print "</ul>{$after_widget}";
  }

  /**
   * Saves the widget's settings.
   *
   */
  function update($new_instance, $old_instance){
    $instance = $old_instance;

    $known_params = $this->known_params();
    foreach ($known_params as $param) {
      $instance[$param] = strip_tags(stripslashes($new_instance[$param]));
    }
    return $instance;
  }

  /**
   * Creates the edit form for the widget.
   *
   */
  function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('title'=>'') );

    $this->form_html($instance);
  }

}// END class

/**
 * Register Hierarchical Categories widget.
 *
 * Calls 'widgets_init' action after the widget has been registered.
 */
function HierCatInit() {
  register_widget('HierCatWidget');
}

/*
 * Initialize both widgets
 */

add_action('widgets_init', 'HierCatInit');
add_action('widgets_init', 'HierPageInit');

//


/* slg55 
 * Taken from a post by Spitzerg http://wordpress.org/support/topic/blank-search-sends-you-to-the-homepage */
add_filter( 'request', 'my_request_filter' );
function my_request_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 100, 80, true ); // 100 pixels wide by 80 pixels tall, crop mode
?>