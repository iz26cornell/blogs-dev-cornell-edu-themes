<?php
/**
 * Template Name: Professors
 * @package WordPress
 * @subpackage Cornell
 */
get_header(); ?>

<div id="content-wrap" class="professors-listing">
  <div class="campaign-wrap">
    <div class="campaign">
      <div id="extra">
        <h2 class="section-title">
          <?php get_top_level_parent_title(); ?>
        </h2>
      </div>
    </div>
  </div>
  <div id="content">
    <div id="main">
      <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
      <h2 class="page-title">
        <?php the_title(); ?>
      </h2>
      <?php if ( is_active_sidebar( 'top-widget-area' ) ) : ?>
      <div class="top-widgets">
        <?php dynamic_sidebar( 'top-widget-area' ); ?>
      </div>
      <?php endif; ?>
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <?php the_content(__('Read more'));?>
		<?php if ( is_active_sidebar( 'widgets-home-bottom' ) ) : ?>
		<?php dynamic_sidebar( 'widgets-home-bottom' ); ?>
		<?php endif; ?>
      <!--
	<?php trackback_rdf(); ?>
	-->
      <?php endwhile; else: ?>
      <p>
        <?php _e('Sorry, no posts matched your criteria.'); ?>
      </p>
      <?php endif; ?>
      <p>
        <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
        <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
      </p>
      <div id="container" class="clearfix">
        <?php /*?><?php query_posts('cat=6&orderby=rand&showposts=100'); ?><?php */?>
        <?php /*?><?php query_posts('category_name=professors-at-large&orderby=rand&showposts=1000'); ?><?php */?>
        <?php query_posts('cat=177481&orderby=rand&showposts=1000'); ?>
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div class="<?php foreach( get_the_category() as $cat ) { echo $cat->slug . ' '; } ?>element transition">
          <?php
			if ( has_post_thumbnail()) {
			echo '<a href="' . get_permalink($post->ID) . '" >';
			the_post_thumbnail();
			echo '</a>';
			}
			?>
          <?php if ( is_front_page() ) { ?>
          <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="hide-show"><span>
          <?php the_title(); ?>
          </span></a>
          <?php } else { ?>
          <?php print_post_title() ?>
          <?php the_excerpt(__('Read more'));?>
          <?php } ?>
        </div>
        <?php endwhile; ?>
      </div>
      <?php if ( is_active_sidebar( 'bottom-widget-area' ) ) : ?>
      <div class="bottom-widgets">
        <?php dynamic_sidebar( 'bottom-widget-area' ); ?>
      </div>
      <?php endif; ?>
    </div>
    <div id="secondary">
      <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
      <?php dynamic_sidebar( 'sidebar-1' ); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
</div>
<?php get_footer(); ?>
