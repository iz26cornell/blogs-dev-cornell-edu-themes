<?php
/**
 * Template Name: People
 * @package WordPress
 * @subpackage Cornell
 */
get_header(); ?>

<?php get_sidebar(); ?>

<div id="main-body">

	<div id="people-listing" class="isotope">
                

		<?php /* The faculty-staff loop */ ?>
       	<?php //query_posts( array ( 'cat' => '8', 'posts_per_page' => 100 ) ); ?>
       	<?php query_posts( array ( 'cat' => '272397', 'posts_per_page' => 100 ) ); ?>
        <h2 class="faculty-staff">Faculty & Staff</h2>
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div class="post faculty-staff <?php echo the_slug(); ?>">
			<?php if ( has_post_thumbnail() ) { the_post_thumbnail('thumbnail', array('class' => 'people-photo')); } else { ?>
            <img width="150" height="150" src="http://blogs.cornell.edu/mellonurbanism/files/2013/11/placeholder-154gwgi.jpg" class="people-photo wp-post-image" alt="No Photo Available">
            <?php } ?>
            <div class="people-content">
		    <div class="readmore-expand">
              <h3><?php the_title(); ?></h3>
		      <?php the_content();?>
            </div>
		    </div>
        </div>
        <?php endwhile; ?>

	
		<?php /* The steering-committee loop */ ?>
		<?php //query_posts( array ( 'cat' => '9', 'posts_per_page' => 100 ) ); ?>
       	<?php query_posts( array ( 'cat' => '270714', 'posts_per_page' => 100 ) ); ?>
        <h2 class="steering-committee">Steering Committee</h2>
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div class="post steering-committee <?php echo the_slug(); ?>">
			<?php if ( has_post_thumbnail() ) { the_post_thumbnail('thumbnail', array('class' => 'people-photo')); } else { ?>
            <img width="150" height="150" src="http://blogs.cornell.edu/mellonurbanism/files/2013/11/placeholder-154gwgi.jpg" class="people-photo wp-post-image" alt="No Photo Available">
            <?php } ?>
            <div class="people-content">
		    <div class="readmore-expand">
              <h3><?php the_title(); ?></h3>
		      <?php the_content();?>
            </div>
		    </div>
        </div>
        <?php endwhile; ?>


		<?php /* The students loop */ ?>
       	<?php //query_posts( array ( 'cat' => '11', 'posts_per_page' => 100 ) ); ?>
       	<?php query_posts( array ( 'cat' => '1569', 'posts_per_page' => 100 ) ); ?>
        <h2 class="students">Students</h2>
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div class="post students <?php echo the_slug(); ?>">
			<?php if ( has_post_thumbnail() ) { the_post_thumbnail('thumbnail', array('class' => 'people-photo')); } else { ?>
            <img width="150" height="150" src="http://blogs.cornell.edu/mellonurbanism/files/2013/11/placeholder-154gwgi.jpg" class="people-photo wp-post-image" alt="No Photo Available">
            <?php } ?>
            <div class="people-content">
		    <div class="readmore-expand">
              <h3><?php the_title(); ?></h3>
		      <?php the_content();?>
            </div>
		    </div>
        </div>
        <?php endwhile; ?>


	</div>
    
</div>

<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>