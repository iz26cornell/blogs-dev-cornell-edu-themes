<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>

	<div id="secondary-nav">

		<div id="section-navigation">

			<?php dynamic_sidebar( 'sidebar-1' ); ?>

			<?php if(is_page('People')) : ?>

				<?php //query_posts( array ( 'cat' => '10', 'posts_per_page' => 100 ) ); ?>
				<?php query_posts( array ( 'cat' => '233', 'posts_per_page' => 100 ) ); ?>

                <aside id="people-widget" class="widget widget_text">			
                    <div class="textwidget">
                        <h3 id="people-ui-filter2-label">All People</h3>
                        <ul class="people-ui-filter2 people-ui-scroll">
                            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                            	<li class="<?php foreach( get_the_category() as $cat ) { echo $cat->slug . ' '; } ?>"><a href="#" data-filter=".<?php echo the_slug(); ?>"><?php the_title(); ?></a></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </aside>

			<?php endif; ?>
            
		</div>

	</div>

<?php endif; ?>