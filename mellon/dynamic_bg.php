<?php /*** Random Background Images

http://www.thought-after.com/2006/05/26/css-random-background-image-rotation/

This file will display a random background image to any element targetted through a CSS id.

1) Place this file in the same location as your sites main stylesheet file.

2) In the head section of each page place the following @import statement.

@import url(/path/to/dynamic_css.php)

NOTE: the import must occur after all other style sheet links, imports and declarations to avoid this dynamic style being over-written.

3) Create a folder to hold the images to be used for the random backgrounds.

4) Assign values to the following variables to complete the setup

$imgFolder : the path from the sites root to the image folder created at 3 above example:

$imgFolder = "/images/random-images/";

$element : the css ID of the element to apply the background image to example:

$element = "header";

5)    That's it!! ***/

// Set up
$imgFolder = "/wp-content/blogs.dir/42/files/backgrounds/"; // the path from the sites root to the image folder created at 3 above
$element = "content-wrap"; // the css ID of the element to apply the background image to

// That's it!! Nothing below this line needs to be changed
$img = null;

// build up the path to the image folder
if (substr($imgFolder,0,1) != '/') { $imgFolder = '/'.$imgFolder; }
if (substr($imgFolder,-1) != '/') { $imgFolder = $imgFolder.'/'; }
$path = $_SERVER['DOCUMENT_ROOT'] . $imgFolder;

// populate an array to hold valid file type extensions
$extList = array('gif','jpg','jpeg','png');

// create an array to hold the list of image files
$fileList = array();

?>