<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>

	<div id="secondary">

		<div class="secondary-section">

			<?php dynamic_sidebar( 'sidebar-2' ); ?>

		</div><!-- .secondary-section -->

	</div><!-- #secondary -->

<?php endif; ?>