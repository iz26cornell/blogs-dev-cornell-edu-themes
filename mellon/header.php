<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>        
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" media="print" href="<?php echo get_template_directory_uri(); ?>/style/print.css" />
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico?v=2" />
	<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/style/ie.css" />
	<![endif]-->
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
    
	<script>window.jQuery || document.write('<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/jquery-1.9.1.min.js">\x3C/script><script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/jquery-migrate-1.1.1.min.js">\x3C/script>')</script>

	<?php wp_head(); ?>
    
	<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/modernizr.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/framework/iws.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/jquery.isotope.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/readmore.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/js/people.js"></script>
	<?php include 'dynamic_css.php'; ?>

	<!-- Initialize 
	<script type="text/javascript">
		var js_path = '<?php echo get_stylesheet_directory_uri('template_directory'); ?>' + '/js/framework/';
		iws_init();
	</script>-->

</head>

<body <?php body_class(); ?>>
<div class="flexible nav-centered">
<div id="skipnav"><a href="#content">Skip to main content</a></div>
<div id="cu-identity" class="theme-white45">
<div id="cu-identity-content">
<div id="cu-logo">
	<a href="http://www.cornell.edu"></a>
</div>

<div id="cu-search" class="options">
	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
		<div id="search-form">
				<input type="radio" name="sitesearch" id="search-filters1" value="thissite" checked="checked">
				<label for="search-filters1">This Site</label>
				<input type="radio" name="sitesearch" id="search-filters2" value="cornell">
				<label for="search-filters2">Cornell</label>
				<input type="text" id="search-form-query" name="s" value="Search" size="20" onBlur="this.value = this.value || this.defaultValue;" onFocus="this.value == this.defaultValue &amp;&amp; (this.value =''); this.select()">
				<input type="submit" id="search-form-submit" name="btnG" value="go">
		</div>
	</form>
</div>

</div></div>

<div id="wrap">
	<div id="header">
    
		<div id="header-band">
			<div id="identity">
				<div id="identity-content">
				</div>
				<div id="campaign-feature"></div>
			</div>
		</div>

		<div id="navigation-bar">
			<div id="navigation-wrap">
			<div id="navigation">
				<h3><em>menu</em></h3>
				<div id="header-text">
                	<div class="site-title"><a href="<?php echo get_settings('home'); ?>" class="home-link"><?php bloginfo('name'); ?></a></div>
                    <div class="site-tagline"><a href="<?php echo get_settings('home'); ?>" class="home-link"><?php bloginfo('description'); ?></a></div>
                </div>
				<div id="mobile-header-text">
                	<div class="site-title"><a href="<?php echo get_settings('home'); ?>" class="home-link"><?php bloginfo('name'); ?></a></div>
                    <div class="site-tagline"><a href="<?php echo get_settings('home'); ?>" class="home-link"><?php bloginfo('description'); ?></a></div>
                </div>
				<?php wp_nav_menu(); ?>
			</div>
			</div>
		</div>
        
	</div>
	
	<div id="midband-wrap" class="">
		<div id="midband"></div>
	</div>
	
	<div id="content-wrap">
	<div id="content" <?php if(is_page('Home')) echo 'class="top-padding-50"'; ?>>
		<div id="main">
			
			<div id="main-top"></div>