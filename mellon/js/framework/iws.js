/* IWS Dynamic Components
   ------------------------------------------- */  

if (js_path == undefined) {
	var js_path = "js/framework/";
}

var iws_include = ["iws_popup.js",
						 "iws_tooltips.js",
						 "iws_expander.js"];

/* load resources */
function iws_load() {
	$.ajaxSetup({async: false});
	for (i=0;i<iws_include.length;i++) {
		$.getScript(js_path+iws_include[i]);
	}
}

/* initialize components */
function iws_init() {
	iws_load();
	
	// Headline Autoscale Variables
	var base_size;
	var base_width;
	var max_size = 72;
	var min_window_size = 0;
	
	// Window Size Tracking
	function resizeChecks() {
		
		// Restore Standard Main Navigation
		if ($(window).width() >= 741) {
			$('#navigation ul').removeAttr('style');
			$('#navigation h3 em').removeClass('open');
		}
		
		// Refresh Headline Autoscale 
		if ($(window).width() > min_window_size) {
			$('.autosize-header .home #identity-content h1').addClass('autoscale');
			var multiplier = $('#identity-content').width() / base_width;
			if (multiplier > 0) {
				var new_size = base_size * multiplier;
				if (new_size > max_size) {
					new_size = max_size;
				}
				$('.autosize-header .home #identity-content h1').css('font-size',new_size+'px');
			}
		}
		else {
			$('.autosize-header .home #identity-content h1').removeAttr('style');
			$('.autosize-header .home #identity-content h1').removeClass('autoscale');
		}
	}
	
	$(window).load(function() {
		
		// Reinitialize Headline Autoscale (after remote webfonts load)
		$('.autosize-header .home #identity-content h1').removeAttr('style');
		base_width = $('.home #identity-content h1 span').width();
		resizeChecks();
		
		// Single-line News Headlines (move date to line two)
		$('.home #news h3').each(function( index ) {
  			if ($(this).next('h4.date').position().top - $(this).position().top < 8) {
  				$(this).find('a').append('<br />');
  			}
		});
	});
	
	$(document).ready(function() {
		popups();
		tooltips(100);
		expander();
		
		
		// Homepage Headline Autoscale
		base_size = parseInt($('.home #identity-content h1').css('font-size'));
		base_width = $('.home #identity-content h1 span').width();
		$(window).resize(resizeChecks);
		resizeChecks();		
		
		
		// Mobile Navigation
		var nav_offset = $('#navigation h3 em').offset();
		
		$('#navigation').find('ul.menu').first().addClass('mobile-menu'); // standard
		//$('#navigation h3').next('div').find('ul.menu').first().addClass('mobile-menu'); // drupal
		
		$('#navigation h3 em').addClass('mobile-menu');
		$('#navigation h3 em').click(function() {
			$(this).toggleClass('open');
			
			$(this).parent().parent().find('ul.menu').slideToggle(200); // standard
			//$(this).parent().next('div').find('ul.menu').slideToggle(200); //drupal
		});
		
		
		// Justified Navigation
		var nav_count = $('.nav-centered #navigation ul.menu').first().find('li').length;
		if (nav_count > 0) {
			var nav_width = 100 / nav_count;
			$('.nav-centered #navigation ul.menu').first().find('li').css( 'width',nav_width+'%');
		}
		
		
/*		
		// Isotope People Page
		
		var $container = $('#people-listing');
		//var $container_list = $('.people-ui-filter2');
		$container.isotope({
  			//transformsEnabled: false,
  			resizable: false,
  			masonry: { columnWidth: $container.width() / 5 }
		});
		$(window).smartresize(updateIsotope);
		
		$('.people-ui-filter1 a').click(function(e) {
			
			e.preventDefault();
			closeAllReadMore();
			$('.people-ui-filter1 a').removeClass('active');
			$(this).addClass('active');
			
			
			var selector = $(this).attr('data-filter');
			$container.isotope({ filter: selector });
			if (selector == '*') {
				$('.people-ui-filter2 li').show();
			}
			else {
				$('.people-ui-filter2 li').hide();
				$('.people-ui-filter2 li'+selector).show();
			}
			
			return false;
			
		});
		
		$('.people-ui-filter2 a').click(function(e) {
			
			e.preventDefault();
			closeAllReadMore();
			
			$('.readmore-expand').readmore({
				speed: 0,
				maxHeight: 133,
				afterToggle: updateIsotope
			});
			var selector = $(this).attr('data-filter');
			triggerReadMore(selector.split('.')[1]);
			$container.isotope({ filter: selector });
			return false;
			
		});
		
		
		$('.readmore-expand').readmore({
			speed: 0,
			maxHeight: 133,
			afterToggle: updateIsotope
		});
		
		
		function updateIsotope() {
			$container.isotope({
				masonry: { columnWidth: $container.width() / 5 }
			});
		}
		function closeAllReadMore() {
			$('.readmore-js-toggle').each(function() {
				if ($(this).text() == "Close") {
					$(this).trigger('click');
				}
			});
		}
		function triggerReadMore(key) {
			$('.isotope-item').each(function() {
				if ($(this).hasClass(key)) {
					if ($(this).find('.readmore-js-toggle').text() == "Read More") {
						$(this).find('.readmore-js-toggle').trigger('click');
					}
				}
			});
		}
		
		updateIsotope();
		
*/		
		
	});
}
/* -------------------------------------------- */


			// Mobile Navigation
		jQuery(document).ready(function() {
			jQuery(window).resize(resizeChecks);
				resizeChecks();
			jQuery('#navigation h3 em').click(function(){
				 jQuery('#navigation h3').toggleClass('open');
				 jQuery('#navigation ul.menu').slideToggle(200);
			});
		});		

		function resizeChecks() {
			// Restore Standard Main Navigation
			if (jQuery(window).width() >= 741) {
				jQuery('#navigation ul').removeAttr('style');
				jQuery('#navigation h3, #navigation h3 em').removeClass('open');
			}
			
		}

		// Home caption
		jQuery(document).ready(function() {
				jQuery(window).resize(resizeChecks2);
				resizeChecks2();
				jQuery('#home-caption-mobile').click(function(){
					 jQuery('#home-caption-mobile .textwidget').slideToggle(200);
				});
		});		

		function resizeChecks2() {
			if (jQuery(window).width() >= 620) {
				jQuery('#home-caption-mobile .textwidget').removeAttr('style');
			}
			
		}