<?php
/**
 * Template Name: Homepage
 * @package WordPress
 * @subpackage Cornell
 */
get_header(); ?>

<div id="wrap" class="twocolumn-left">
  <div id="content">
    <div id="content-wrap">
      <div id="header"> 
            <h1><a href="<?php echo get_settings('home'); ?>/"><strong><?php bloginfo('name'); ?></strong></a></h1>
            <?php if ( is_active_sidebar( 'sidebar-home-extra' ) ) : ?>
        	<?php dynamic_sidebar( 'sidebar-home-extra' ); ?>
        	<?php endif; ?>
      </div>
        <div id="main-navigation">
          <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
          <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
        </div>
        <!-- main navigation --> 
      
      <div id="main">
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php if ( is_front_page() ) { ?>
          
          <?php } else { ?>
          <h1 class="entry-title">
            <?php the_title(); ?>
          </h1>
          <?php } ?>
          <div class="entry-content">
            <?php the_content(); ?>
            <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
            <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
          </div>
          <!-- .entry-content --> 
        </div>
        <!-- #post-## -->
        <?php endwhile; ?>
        
        <div id="featured-posts">
         <?php query_posts('cat=73390&showposts=3'); ?> <!-- uncomment this line before sending to vendor-->
          <?php /*?><?php query_posts('category_name=featured&showposts=3'); ?><?php */?>  <!-- comment this line before sending to vendor -->      
          <h3 class="cat-name"><a href="<?php echo get_category_link(73390); ?>"><?php echo get_cat_name(73390);?></a></h3>
           <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
           
           <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
             <?php if ( is_front_page() ) { ?>
             <?php print_post_title() ?>
             <?php } else { ?>
             <?php print_post_title() ?>
             <?php } ?> 
             </div><!-- #post-## -->
           <?php endwhile; ?>
           
         </div>
   
      </div>
      <div id="secondary">
        <?php if ( is_active_sidebar( 'sidebar-home' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-home' ); ?>
        <?php endif; ?>     
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
