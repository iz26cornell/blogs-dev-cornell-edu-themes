<?php
if ( function_exists('register_sidebar') )	
    
	register_sidebar( array(
		'name' => __( 'Sidebar (homepage)', 'Cornell' ),
		'id' => 'sidebar-home',
		'description' => __( 'The primary widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (homepage extra)', 'Cornell' ),
		'id' => 'sidebar-home-extra',
		'description' => __( 'Secondary widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (secondary default)', 'Cornell' ),
		'id' => 'sidebar-1',
		'description' => __( 'The primary widget area for secondary pages.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (about section)', 'Cornell' ),
		'id' => 'sidebar-about',
		'description' => __( 'Appears on about section pages', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (apply section)', 'Cornell' ),
		'id' => 'sidebar-apply',
		'description' => __( 'Appears on apply section pages', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (news section)', 'Cornell' ),
		'id' => 'sidebar-news',
		'description' => __( 'Appears on news section pages', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Links (all pages)', 'Cornell' ),
		'id' => 'footer-widget-area',
		'description' => __( 'A customizable menu of footer links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Text (all pages)', 'Cornell' ),
		'id' => 'footer-message',
		'description' => __( 'This content appears under the footer copyright and links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );


add_action( 'after_setup_theme', 'Cornell_setup' );
if ( ! function_exists( 'Cornell_setup' ) ):
function Cornell_setup() {	
	
	// Your changeable header business starts here
	define( 'HEADER_TEXTCOLOR', '462406' );
	// No CSS, just an IMG call. The %s is a placeholder for the theme template directory URI.
	define( 'HEADER_IMAGE', '%s/images/project/home1.jpg' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'Cornell' ),
	) );

	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to Cornell_header_image_width and Cornell_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'Cornell_header_image_width', 625 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'Cornell_header_image_height', 1000 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 960 pixels wide by 160 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	//set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See Cornell_admin_header_style(), below.
	add_custom_image_header( 'Cornell_header_style', 'Cornell_admin_header_style', 'Cornell_admin_header_image' );

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	
	register_default_headers( array(
		'banner1' => array(
			'url' => '%s/images/project/home1.jpg',
			'thumbnail_url' => '%s/images/project/home1_t.jpg',
			'description' => __( 'Header 1', 'Cornell' )
		),
		'banner2' => array(
			'url' => '%s/images/project/home2.jpg',
			'thumbnail_url' => '%s/images/project/home2_t.jpg',
			'description' => __( 'Header 2', 'Cornell' )
		),
		'banner3' => array(
			'url' => '%s/images/project/home3.jpg',
			'thumbnail_url' => '%s/images/project/home3_t.jpg',
			'description' => __( 'Header 3', 'Cornell' )
		)
	) );
	
}
endif;

if ( ! function_exists( 'Cornell_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog
 *
 */
function Cornell_header_style() {

	// If no custom options for text are set, let's bail
	// get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
	if ( HEADER_TEXTCOLOR == get_header_textcolor() )
		return;
	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( 'blank' == get_header_textcolor() ) :
	?>
		#site-title,
		#site-description {
			position: absolute;
			left: -9000px;
		}
	<?php
		// If the user has set a custom color for the text use that
		else :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?> !important;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif;


if ( ! function_exists( 'Cornell_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 * Referenced via add_custom_image_header() in Cornell_setup().
 */
function Cornell_admin_header_style() {
?>
	<style type="text/css">
	.appearance_page_custom-header #headimg {
		background: #fff;
		border: none;
	}
	#headimg h1,
	#desc {
		font-family: Georgia, serif;
	}
	#headimg h1 {
		margin: 0;
		font-weight: normal;
		padding: 2px 0 0 0;
	}
	#headimg h1 a {
		font-size: 36px;
		line-height: 42px;
		text-decoration: none;
	}
	#desc {
		font-size: 18px;
		line-height: 31px;
		padding: 0 0 2px 0;
		font-style: italic;
	}
	<?php
		// If the user has set a custom color for the text use that
		if ( get_header_textcolor() != HEADER_TEXTCOLOR ) :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?>;
		}
	<?php endif; ?>
	#headimg img {
		max-width: 400px;
		padding-top: 5px;
	}
	</style>
<?php
}
endif;

if ( ! function_exists( 'Cornell_admin_header_image' ) ) :
/**
 * Custom header image markup displayed on the Appearance > Header admin panel.
 * Referenced via add_custom_image_header() in Cornell_setup().
 */
function Cornell_admin_header_image() { ?>
	<div id="headimg">
		<?php
		if ( 'blank' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) || '' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) )
			$style = ' style="display:none;"';
		else
			$style = ' style="color:#' . get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) . ';"';
		?>
		<h1><a id="name"<?php echo $style; ?> onclick="return false;" href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
		<div id="desc"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></div>
		<img src="<?php esc_url ( header_image() ); ?>" alt="" />
	</div>
<?php }
endif;

/*
 *  Returns the Cornell options with defaults as fallback
 */
function Cornell_get_theme_options() {
	$defaults = array(
		'color_scheme' => 'light',
		'theme_layout' => 'content-sidebar',
	);
	$options = get_option( 'Cornell_theme_options', $defaults );
	return $options;
}

/*
 *  Add section class to body tag
 */
add_filter('body_class','body_class_section');
function body_class_section ($classes) {
	global $wpdb, $post;
	if (is_page()) {
		if ($post->post_parent) {
			$parent = end(get_post_ancestors($current_page_id));
		}
		else {
			$parent = $post->ID;
		}
	$post_data = get_post($parent, ARRAY_A);
	$classes[] = 'section-' . $post_data['post_name'];
	}
	return $classes;
}
/*
 *  replace post title link if external link exists
 	add custom field "url_title" or "title_url" or "url1" and enter external url in value field
 */
function print_post_title() {
	global $post;
		$thePostID = $post->ID;
		$post_id = get_post($thePostID);
		$title = $post_id->post_title;
		$perm = get_permalink($post_id);
		$post_keys = array(); $post_val = array();
		$post_keys = get_post_custom_keys($thePostID);
	if (!empty($post_keys)) {
	foreach ($post_keys as $pkey) {
	if ($pkey=='url1' || $pkey=='title_url' || $pkey=='url_title') {
		$post_val = get_post_custom_values($pkey);
		}
	}
	if (empty($post_val)) {
		$link = $perm;
	} else {
		$link = $post_val[0];
		}
	} else {
		$link = $perm;
	}
	echo '<h3 class="entry-title"><a href="'.$link.'" rel="bookmark"; title="'.$title.'";>'.$title.'</a></h3>';
}

function new_excerpt_more( $more ) {
	return ' [ <a class="read-more" href="'. get_permalink($post->ID) . '"><em>Read More</em></a> ]';
}
add_filter('excerpt_more', 'new_excerpt_more');

/* slg55
 * Taken from a post by Spitzerg http://wordpress.org/support/topic/blank-search-sends-you-to-the-homepage */
add_filter( 'request', 'my_request_filter' );
function my_request_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 40, 40, true ); // 40 pixels wide by 40 pixels tall, crop mode
?>