<!-- The following div contains the Cornell University logo and search link -->
<div id="skipnav">
	<a href="#main">Skip to main content</a>
</div>
<div id="cu-identity" class="theme-gray45">
	<div id="cu-logo">
		<a href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/cornell_identity/cu_logo_white45.gif" alt="Cornell University" /></a>
	</div>
	<div id="search-form">
		<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" >
        <div id="search-filters">
                    <div class="filter">
                      <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
                      <label for="search-filters1">This Site</label>
                    </div>
					<div class="filter">
					<input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
					<label for="search-filters2">Cornell</label>	
                    </div>
		</div>
		<div id="search-input">
				<input type="text" value="" name="s" id="search-form-query" size="20" />
     			<input type="submit" id="search-form-submit" name="btnG" value="go" />
		</div>
		</form>
	</div>
</div>
