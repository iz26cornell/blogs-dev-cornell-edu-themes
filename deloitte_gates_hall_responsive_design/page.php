<?php
/**
 * @package WordPress
 * @subpackage Cornell
 */
get_header();
?>
<div id="wrap_page" class="bdyWrapper innerPagesWrapper">
    <div id="content" class="container">
        <!--<div id="content-wrap">-->
        <div class="leftContent columns">
            <div id="header">
                <h1><a href="<?php echo get_settings('home'); ?>/">
				<strong>Gates Hall</strong>
				<!--<img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/logo.gif" alt="Logo" class="logo"/>-->
				</a>
				</h1>
                <!--                <div class="logo">
                                </div>-->
                <div id="main-navigation">
                    <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
                    <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'primary')); ?>
                </div>
                <!-- main navigation -->
            </div>
            <div class="divMobileDisplay">
                <div class="corbnellBannerForInnerPages">
                    <div class="exploreDiv" onclick="cornell.openMainNavigationLinks()">
                                          <img id="exploreSideArrow" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/arrow.png" alt="Arrow" class="exploreArrow"/>
                        <img id="exploreDownArrow" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/down_arrow.png" alt="Arrow" class="exploreDownArrow"/>
	  </div>
                </div>
                 <?php
                    $home_page_post_id = 46;
                    $home_page_post = get_post($home_page_post_id, ARRAY_A);
                    $content_home = $home_page_post['post_content'];
                    echo $content_home;
                ?>
            </div>
            <div class="divMobileContentWrapper">
                <div id="secondary" class="divSecondaryPageContent">
                    <?php if (is_active_sidebar('sidebar-1')) : ?>
                    <?php dynamic_sidebar('sidebar-1'); ?>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="rightContentPages columns">
                <!--                    <div class="rightContentWrapper">-->
                <div class="divMobileContentWrapper">    
                    <div id="main" class="pageMainDiv">
                    <?php if (have_posts ()) : while (have_posts ()) : the_post(); ?>
                                <h2 class="page-title"><?php the_title(); ?></h2>
                    <?php the_content(__('Read more')); ?>
                    <?php wp_link_pages(); ?>
                                <!--
                    <?php trackback_rdf(); ?>
                                                            	-->
                    <?php endwhile;
                            else: ?>
                                <p>
                        <?php _e('Sorry, no posts matched your criteria.'); ?>
                            </p>
                    <?php endif; ?>
                    <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
                    <?php edit_post_link(__('Edit', 'Cornell'), '<span class="edit-link">', '</span>'); ?>
                            </div>
                        </div>
                        <!--                        </div>-->
                    </div>


                    <!--</div>-->
                </div>
            </div>
<?php get_footer(); ?>
