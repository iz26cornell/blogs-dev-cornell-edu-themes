<?PHP
if (isset($_GET['btnG'])) {
    session_start();
    $selected_radio = $_GET['sitesearch'];

    if ($selected_radio == 'cornell') {
        $search_terms = urlencode($_GET['s']);
        $URL = "http://www.cornell.edu/search" . "?q=" . $search_terms . "&client=default_frontend&proxystylesheet=default_frontend";
        print $URL;
        header("Location: $URL");
    }
}
?>

<?php get_header(); ?>
<div id="wrap_search" class="bdyWrapper innerPagesWrapper">
    <div id="content" class="container">
        <div class="leftContent columns">
            <div id="header">
                <h1><a href="<?php echo get_settings('home'); ?>/">
				<strong>Gates Hall</strong>
				<!--<img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/logo.gif" alt="Logo" class="logo"/>-->
				</a>
				</h1>
                <div id="main-navigation">
                    <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
                    <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'primary')); ?>
                </div>
                <!-- main navigation -->
            </div>
            <div class="divMobileDisplay">
                <div class="corbnellBannerForInnerPages">
                    <div class="exploreDiv" onclick="cornell.openMainNavigationLinks()">
                       <img id="exploreSideArrow" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/arrow.png" alt="Arrow" class="exploreArrow"/>
                        <img id="exploreDownArrow" src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/layout/down_arrow.png" alt="Arrow" class="exploreDownArrow"/>
	</div>
                </div>
                <?php
                    $home_page_post_id = 46;
                    $home_page_post = get_post($home_page_post_id, ARRAY_A);
                    $content_home = $home_page_post['post_content'];
                    echo $content_home;
                ?>
                </div>

                <div id="secondary" class="divSecondaryPageContent">
                <?php if (is_active_sidebar('sidebar-1')) : ?>
                <?php dynamic_sidebar('sidebar-1'); ?>
                <?php endif; ?>
                    </div>
                </div>
                <div class="rightContentPages columns">
                    <div class="divMobileContentWrapper">
                        <div id="main" class="pageMainDiv innerPageMainDiv">
                            <h2 class="page-title"><?php printf(__('Search Results for: %s'), '<span>' . get_search_query() . '</span>'); ?></h2>
                    <?php if (have_posts() && $_GET['s'] != "") : while (have_posts ()) : the_post(); ?>

                    <?php print_post_title() ?>
                            <div class="readMore">
                    <?php the_excerpt(__('Read more')); ?>
                                </div>
                                <!--
                    <?php trackback_rdf(); ?>
                                                	-->
                    <?php endwhile;
                            else: ?>
                                <h3 class="entry-title"><?php _e('Nothing Found'); ?></h3>
                                <p>
                        <?php _e('Sorry, no posts matched your criteria.'); ?>
                            </p>
                            <div id="innerSearchForm">
                    <?php get_search_form(); ?>
                    </div>
                    <?php endif; ?>
                    <?php /* posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); */ ?>
                    <!-- Added classes for buttons type styling in Mobile devices-->
                        <div class="navigation">
                            <div class="alignleft previousBtn"><span><?php previous_posts_link('&laquo; go back') ?></span></div>
                            <div class="alignright backBtn"><span><?php next_posts_link('keep looking &raquo;', '') ?></span></div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
