<?php
/**
 * The Header
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Cryout Creations
 * @subpackage parabola
 * @since parabola 0.5
 */
 ?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php  cryout_meta_hook(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
 	cryout_header_hook();
	wp_head(); ?>
</head>

<body class="<?php body_class(); ?> no-js flexible">
<!-- Cornell Identity Banner -->
	<div id="cu-identity" class="home theme-gray45">
		<div id="cu-identity-content">
			<div id="cu-logo">
				<a href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/cornell_identity/cu_logo_white45.gif" alt="Cornell University" /></a>
			</div>
			<div id="cu-search" class="options">
				<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
					<div id="search-form" class="no-label">
						<div id="search-input">
							<label for="search-form-query">Search:</label>
							<input type="text" id="search-form-query" name="s" value="Search" size="20" onBlur="this.value = this.value || this.defaultValue;" onFocus="this.value == this.defaultValue && (this.value =''); this.select()" />
							<input type="submit" id="search-form-submit" name="submit" value="go" />
						</div>
						<div id="search-filters">
							<input type="radio" name="sitesearch" id="search-filters1" value="thissite" checked="checked" />
							<label for="search-filters1">This Site</label>
							<input type="radio" name="sitesearch" id="search-filters2" value="cornell" />
							<label for="search-filters2">Cornell</label>
						</div>	
					</div>
				</form>
			</div>
		</div>
	</div>

<?php cryout_body_hook(); ?>

<div id="wrapper" class="hfeed">

<?php cryout_wrapper_hook(); ?>


<div id="header-full">





<header id="header">
<div id="bme">
<a title="Department of Biomedical Engineering" href="http://www.bme.cornell.edu/"></a>
</div>
<div id="cbe">
<a title="School of Chemical and Biomeolecular Engineering " href="http://www.cbe.cornell.edu/"></a>
</div>

<?php cryout_masthead_hook(); ?>

		<div id="masthead">

			<div id="branding" role="banner" >
				
				<?php cryout_branding_hook();?>
				<div style="clear:both;"></div>
			
			</div><!-- #branding -->

			<nav id="access" role="navigation">

				<?php cryout_access_hook();?>

			</nav><!-- #access -->

		</div><!-- #masthead -->

	<div style="clear:both;height:1px;width:1px;"> </div>

</header><!-- #header -->
</div><!-- #header-full -->
<div id="main">
	<div  id="forbottom" >
		<?php cryout_forbottom_hook(); ?>

		<div style="clear:both;"> </div>

		<?php cryout_breadcrumbs_hook();?>
