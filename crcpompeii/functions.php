<?php

// Enqueue styles and scripts
function crcpompeii_enqueue_styles_and_scripts() {
    if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-styles', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
    }
    wp_enqueue_style( 'theme-styles', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'crcpompeii_enqueue_styles_and_scripts', 11 );

function crcpompeii_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Sidebar', 'twentysixteen' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'crcpompeii_widgets_init', 15 );