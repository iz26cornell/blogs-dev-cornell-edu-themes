<?php get_header(); ?>

<div id="wrap">
  <div id="content" class="widecolumn">
    <div id="main">
      <div class="webring">
        <div class="previous">
          <?php previous_post_link('<strong>%link</strong>'); ?>
        </div>
        <div class="next">
          <?php next_post_link('<strong>%link</strong>'); ?>
        </div>
      </div>
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <h2><a href="<?php the_permalink() ?>" rel="bookmark">
        <?php the_title(); ?>
        </a></h2>
      <div class="postmeta">
        <div class="postmetaleft">
          <div class="post-date">
            <span class="month"><?php the_time(__('M','arclite')); ?></span>
            <span class="day"><?php the_time(__('j','arclite')); ?></span>
           </div>

          <p class="category_author"> category:
            <?php the_category(', ') ?>
            by
            <?php the_author(); ?>
          </p>
        </div>
        <div class="postmetaright">
          <p>
            <?php if ('open' == $post->comment_status) : ?>
            <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
            <?php else : ?>
            &nbsp;
            <?php endif; ?>
            <span>
            <?php edit_post_link('edit', '', ''); ?>
            </span></p>
        </div>
      </div>
      <?php the_content(__('Read more'));?>
      <!--
	<?php trackback_rdf(); ?>
	-->
      <?php endwhile; else: ?>
      <p>
        <?php _e('Sorry, no posts matched your criteria.'); ?>
      </p>
      <?php endif; ?>
      <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
    </div>
  </div>
  <!-- The main column ends  -->
</div>
<?php get_footer(); ?>
