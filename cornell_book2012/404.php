<?php get_header(); ?>
<div id="wrap" class="twocolumn-left">
<div id="content">
<div id="content-wrap">
      <div id="header">
        <div id="main-navigation">
          <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
          <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
        </div>
        <!-- main navigation --> 
      </div>
  <div id="main">
  <div id="campaign-home">
        <h1><a href="<?php echo get_settings('home'); ?>/"><strong><?php bloginfo('name'); ?></strong></a></h1>
        <img src="<?php header_image(); ?>" alt="" />
   </div>
  <div class="page-meta">
    <h2>Not Found, Error 404</h2>
    <p>The page you are looking for no longer exists.</p>
  </div>
  </div>
  <div id="secondary">
	  <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
      <?php endif; ?>
  </div>
</div>
</div>
<!-- The main column ends  -->
</div>
<?php get_footer(); ?>
