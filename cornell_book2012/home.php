<?php
/**
 * Template Name: Homepage
 * @package WordPress
 * @subpackage Cornell
 */
get_header(); ?>

<div id="wrap" class="twocolumn-left">
  <div id="content">
    <div id="content-wrap">
      <div id="header">
        <div id="main-navigation">
          <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
          <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
        </div>
        <!-- main navigation --> 
      </div>
      <div id="main">
        <div id="campaign-home">
        <h1><a href="<?php echo get_settings('home'); ?>/"><strong><?php bloginfo('name'); ?></strong></a></h1>
        <img src="<?php header_image(); ?>" alt="" />
        </div>
        <div class="page-meta">
          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php if ( is_front_page() ) { ?>
            <?php /*?><h2 class="entry-title"><?php the_title(); ?></h2><?php */?><!-- removing title from homepage -->
            <?php } else { ?>
            <h1 class="entry-title">
              <?php the_title(); ?>
            </h1>
            <?php } ?>
            <div class="entry-content">
              <?php the_content(); ?>
              <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'Cornell' ), 'after' => '</div>' ) ); ?>
              <?php edit_post_link( __( 'Edit', 'Cornell' ), '<span class="edit-link">', '</span>' ); ?>
            </div>
            <!-- .entry-content --> 
          </div>
          <!-- #post-## -->
          <?php endwhile; ?>
        </div>
   
      </div>
      <div id="secondary">
        <?php if ( is_active_sidebar( 'sidebar-home' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-home' ); ?>
        <?php endif; ?>
         <div id="featured-posts">
         <?php query_posts('cat=46247&showposts=3'); ?> <!-- uncomment this line before sending to vendor-->
          <?php /*?><?php query_posts('category_name=featured&showposts=3'); ?><?php */?>  <!-- comment this line before sending to vendor -->
           <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
           <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
             <div class="entry-date"><span class="month"><?php the_time('M') ?></span><span class="day"><?php the_time('d') ?></span></div>
             <?php if ( is_front_page() ) { ?>
             <h3 class="entry-title"><?php the_title(); ?></h3>
             <?php } else { ?>
             <h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'Cornell' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
             <?php } ?>
             </div><!-- #post-## -->
           <?php endwhile; ?>
         </div>
         
         <?php if ( is_active_sidebar( 'sidebar-home-extra' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-home-extra' ); ?>
        <?php endif; ?>
            
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
