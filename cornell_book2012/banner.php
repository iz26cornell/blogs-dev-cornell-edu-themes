<div id="skipnav"> <a href="#main">Skip to main content</a> </div>
<hr />
<!-- The following div contains the Cornell University logo and search link -->
<div id="cu-identity" class="theme-black75">
  <div id="cu-logo-unit75"> <a id="insignia-link" href="http://www.cornell.edu/"><img src="<?php echo get_stylesheet_directory_uri('template_directory'); ?>/images/cornell_identity/cu_logo_white45.gif" alt="Cornell University" /></a>
    <div id="unit-signature-links"> <a id="cornell-link" href="http://www.cornell.edu/">Cornell University</a> <a id="unit-link" href="<?php echo get_settings('home'); ?>/">New Student Reading Project</a> </div>
  </div>
  <div id="search-form">
    <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" >
      <div id="search-input">
        <label for="search-form-query">SEARCH:</label>
        <input type="text" value="<?php echo trim( get_search_query() ); ?>" name="s" id="search-form-query" size="20" />
        <input type="submit" id="search-form-submit" name="btnG" value="go" />
      </div>
      <div id="search-filters">
        <input type="radio" id="search-filters1" name="sitesearch" value="thissite" checked="checked" />
        <label for="search-filters1">New Student Reading Project</label>
        <input type="radio" id="search-filters2" name="sitesearch" value="cornell" />
        <label for="search-filters2">Cornell</label>
        <a href="http://www.cornell.edu/search/">more options</a> </div>
    </form>
  </div>
</div>
