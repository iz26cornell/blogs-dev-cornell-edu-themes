<?php get_header(); ?>

<div id="wrap">
  <div id="content">
    <div id="content-wrap">
            <div id="header"> 
        <div id="main-navigation">
          <?php /* Main navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
          <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
        </div>
        <!-- main navigation --> 
      </div>
      <div id="main">
      <div id="campaign-home">
        <h1><a href="<?php echo get_settings('home'); ?>/"><strong><?php bloginfo('name'); ?></strong></a></h1>
        <img src="<?php header_image(); ?>" alt="" />
        </div>
      <div class="page-meta">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <h2 class="page-title">
          <?php the_title(); ?>
        </h2>
        <?php the_content(__('Read more'));?>
        <div class="postmeta clear-both">
          <div class="postmetaleft">
            <p><!--<span class="avatar-box"><?php echo get_avatar( get_the_author_email(), '25' ); ?> <?php the_author_posts_link(); ?></span>--> 
              category: <?php the_category(', ') ?>
              <br />  
              posted: <?php the_time('m/j/y g:i A') ?> by <?php the_author_posts_link(); ?>
              <br />  
              <?php if ('open' == $post->comment_status) : ?><?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?><?php else : ?><?php endif; ?>  
              <span class="edit-post"><?php edit_post_link('edit', '', ''); ?></span>  
              </p>
            </div>
        </div>
        <?php trackback_rdf(); ?>
        <h3 class="comments-title clear-both">Comments</h3>
        <?php comments_template(); // Get wp-comments.php template ?>
        <?php endwhile; else: ?>
        <p>
          <?php _e('Sorry, no posts matched your criteria.'); ?>
        </p>
        <?php endif; ?>
        <?php posts_nav_link(' &#8212; ', __('&laquo; go back'), __('keep looking &raquo;')); ?>
      </div>
      </div>
      <div id="secondary">
        <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
