<?php

function mobile_screen() {
	$useragent=$_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) 
	{
	
		//we ARE a mobile device
		
		/*
		//let's take care of any cookie changes from when user clicks
		if (isset($_GET["killMobileSession"])) { $_SESSION['iws_mobileSession'] = ""; $_SESSION["iws_amMobileDevice"] = "TRUE"; }
		//if (isset($_GET["killMobileSession"])) { unset($_SESSION['iws_mobileSession']); $_SESSION["iws_amMobileDevice"] = "TRUE"; }//setcookie("mobileSession", "", time()-3600); }
		if (isset($_GET["startMobileSession"])) { $_SESSION["iws_mobileSession"] = "TRUE"; }//setcookie("mobileSession", "TRUE"); }
		//echo "\niws_mobileSession:".$_SESSION["iws_mobileSession"]."\n";
		//echo "iws_amMobileDevice:".$_SESSION["iws_amMobileDevice"]."\n";
		
		//Are we already in a mobile session?
		//if (!isset($_SESSION["iws_mobileSession"])) echo "Session is not set\n";
		//if(isset($_SESSION["iws_amMobileDevice"])) echo "I am a mobile device\n";
		if(isset($_SESSION["iws_amMobileDevice"]) && $_SESSION["iws_amMobileDevice"] != "" && $_SESSION["iws_mobileSession"] === "")
		{
			//Then they switched to regular web version
			echo "<link rel=\"stylesheet\" href=\""; echo bloginfo('stylesheet_directory'); echo "/style.css\" type=\"text/css\" media=\"screen\" />";
		}
		else 
		{
			//set session
			$_SESSION["iws_amMobileDevice"] = "TRUE"; // setcookie("amMobileDevice","TRUE");
			$_SESSION["iws_mobileSession"] = "TRUE"; //setcookie("mobileSession","TRUE");
			//echo $_SESSION["iws_amMobileDevice"];
			echo "<meta name=\"viewport\" content=\"width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\"/>";
			echo "<link rel=\"stylesheet\" href=\""; echo bloginfo('stylesheet_directory'); echo "/styles/mobile.css\" type=\"text/css\" />";
		}*/
	
		/* Mobile CSS DISABLED ************************************************************ */
		
		//echo "<meta name=\"viewport\" content=\"width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\"/>";
		//echo "<link rel=\"stylesheet\" href=\""; echo bloginfo('stylesheet_directory'); echo "/styles/mobile.css\" type=\"text/css\" />";
		
		echo "<link rel=\"stylesheet\" href=\""; echo bloginfo('stylesheet_directory'); echo "/style.css\" type=\"text/css\" media=\"screen\" />";
		
		/* ******************************************************************************** */
	} 
	else {
		// then we're NOT on a mobile device
		echo "<link rel=\"stylesheet\" href=\""; echo bloginfo('stylesheet_directory'); echo "/style.css\" type=\"text/css\" media=\"screen\" />";
	}	
}


if ( function_exists('register_sidebar') )	
    
	register_sidebar( array(
		'name' => __( 'Sidebar (homepage)', 'Cornell' ),
		'id' => 'sidebar-home',
		'description' => __( 'The primary widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (homepage extra)', 'Cornell' ),
		'id' => 'sidebar-home-extra',
		'description' => __( 'Secondary widget area for the homepage.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar (secondary)', 'Cornell' ),
		'id' => 'sidebar-1',
		'description' => __( 'The primary widget area for secondary pages.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	/*
	register_sidebar( array(
		'name' => __( 'Side Navigation (secondary)', 'Cornell' ),
		'id' => 'sidenav-widget-area',
		'description' => __( 'Appears as the topmost element of the sidebar on secondary pages.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	*/

	register_sidebar( array(
		'name' => __( 'Footer Links (all pages)', 'Cornell' ),
		'id' => 'footer-widget-area',
		'description' => __( 'A customizable menu of footer links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	/*register_sidebar( array(
		'name' => __( 'Footer Links bottom (all pages)', 'Cornell' ),
		'id' => 'footer-widget-area2',
		'description' => __( 'This content appears under the footer top links.', 'Cornell' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );*/


add_action( 'after_setup_theme', 'Cornell_setup' );
if ( ! function_exists( 'Cornell_setup' ) ):
function Cornell_setup() {	
	
	// Your changeable header business starts here
	define( 'HEADER_TEXTCOLOR', '83ADE7' );
	// No CSS, just an IMG call. The %s is a placeholder for the theme template directory URI.
	define( 'HEADER_IMAGE', '%s/images/project/home1.gif' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'Cornell' ),
	) );

	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to Cornell_header_image_width and Cornell_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'Cornell_header_image_width', 700 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'Cornell_header_image_height', 325 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 960 pixels wide by 160 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See Cornell_admin_header_style(), below.
	add_custom_image_header( 'Cornell_header_style', 'Cornell_admin_header_style', 'Cornell_admin_header_image' );

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	
	register_default_headers( array(
		'banner1' => array(
			'url' => '%s/images/project/home1.jpg',
			'thumbnail_url' => '%s/images/project/home1_t.jpg',
			'description' => __( 'Book 1', 'Cornell' )
		),
		'banner2' => array(
			'url' => '%s/images/project/home2.jpg',
			'thumbnail_url' => '%s/images/project/home2_t.jpg',
			'description' => __( 'Book 2', 'Cornell' )
		),
		'banner3' => array(
			'url' => '%s/images/project/home3.jpg',
			'thumbnail_url' => '%s/images/project/home3_t.jpg',
			'description' => __( 'Book 3', 'Cornell' )
		),
		'banner4' => array(
			'url' => '%s/images/project/home4.jpg',
			'thumbnail_url' => '%s/images/project/home4_t.jpg',
			'description' => __( 'Book 4', 'Cornell' )
		)
	) );
	
}
endif;

if ( ! function_exists( 'Cornell_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog
 *
 */
function Cornell_header_style() {

	// If no custom options for text are set, let's bail
	// get_header_textcolor() options: HEADER_TEXTCOLOR is default, hide text (returns 'blank') or any hex value
	if ( HEADER_TEXTCOLOR == get_header_textcolor() )
		return;
	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( 'blank' == get_header_textcolor() ) :
	?>
		#site-title,
		#site-description {
			position: absolute;
			left: -9000px;
		}
	<?php
		// If the user has set a custom color for the text use that
		else :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?> !important;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif;


if ( ! function_exists( 'Cornell_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 * Referenced via add_custom_image_header() in Cornell_setup().
 */
function Cornell_admin_header_style() {
?>
	<style type="text/css">
	.appearance_page_custom-header #headimg {
		background: #000;
		border: none;
	}
	#headimg h1,
	#desc {
		font-family: Georgia, serif;
	}
	#headimg h1 {
		margin: 0;
		font-weight: normal;
		padding: 2px 0 0 0;
	}
	#headimg h1 a {
		font-size: 36px;
		line-height: 42px;
		text-decoration: none;
	}
	#desc {
		font-size: 18px;
		line-height: 31px;
		padding: 0 0 2px 0;
		font-style: italic;
	}
	<?php
		// If the user has set a custom color for the text use that
		if ( get_header_textcolor() != HEADER_TEXTCOLOR ) :
	?>
		#site-title a,
		#site-description {
			color: #<?php echo get_header_textcolor(); ?>;
		}
	<?php endif; ?>
	#headimg img {
		max-width: 400px;
		padding-top: 5px;
	}
	</style>
<?php
}
endif;

if ( ! function_exists( 'Cornell_admin_header_image' ) ) :
/**
 * Custom header image markup displayed on the Appearance > Header admin panel.
 * Referenced via add_custom_image_header() in Cornell_setup().
 */
function Cornell_admin_header_image() { ?>
	<div id="headimg">
		<?php
		if ( 'blank' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) || '' == get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) )
			$style = ' style="display:none;"';
		else
			$style = ' style="color:#' . get_theme_mod( 'header_textcolor', HEADER_TEXTCOLOR ) . ';"';
		?>
		<h1><a id="name"<?php echo $style; ?> onclick="return false;" href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
		<div id="desc"<?php echo $style; ?>><?php bloginfo( 'description' ); ?></div>
		<img src="<?php esc_url ( header_image() ); ?>" alt="" />
	</div>
<?php }
endif;

/*
 *  Returns the Cornell options with defaults as fallback
 */
function Cornell_get_theme_options() {
	$defaults = array(
		'color_scheme' => 'light',
		'theme_layout' => 'content-sidebar',
	);
	$options = get_option( 'Cornell_theme_options', $defaults );
	return $options;
}

/*
 *  Add section class to body tag
 */
add_filter('body_class','body_class_section');
function body_class_section ($classes) {
	global $wpdb, $post;
	if (is_page()) {
		if ($post->post_parent) {
			$parent = end(get_post_ancestors($current_page_id));
		}
		else {
			$parent = $post->ID;
		}
	$post_data = get_post($parent, ARRAY_A);
	$classes[] = 'section-' . $post_data['post_name'];
	}
	return $classes;
}

/* Taken from a post by Spitzerg http://wordpress.org/support/topic/blank-search-sends-you-to-the-homepage */
add_filter( 'request', 'my_request_filter' );
function my_request_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}


?>